model dpp {
  class Philosopher {
    signature {
      attr left : Fork;
      attr right : Fork;

      // reception ack();
    }

    behaviour {
      states {
        initial Initial;
        simple Thinking;
        simple Taking;
        simple Eating;
      }

      transitions {
        Initial -> Thinking;
        Thinking -> Taking {
          trigger wait;
          effect left.take(this);
        }
        Taking -> Eating {
          trigger wait;
          effect right.take(this);
        }
        Eating -> Thinking {
          trigger wait;
          effect right.release(this); left.release(this);
        }
      }
    }
  }

  class Fork {
    signature {
      operation take(phil : Philosopher);
      operation release(phil : Philosopher);
    }

    behaviour {
      states {
        initial Initial;
        simple Free {
          defer release;
        }
        simple Taken {
          defer take;
        }
      }

      transitions {
        Initial -> Free;
        Free -> Taken {
          trigger take;
        }
        Taken -> Free {
          trigger release;
        }
      }
    }
  }

  collaboration two {
    object fork1 : Fork {
    }
    object fork2 : Fork {
    }
    object phil1 : Philosopher {
      left = fork1;
      right = fork2;
    }
    object phil2 : Philosopher {
      left = fork2;
      right = fork1;
    }

    assertion not_deadlocking {
      G not deadlock;
    }
  }

  collaboration four {
    object fork1 : Fork {
    }
    object fork2 : Fork {
    }
    object fork3 : Fork {
    }
    object fork4 : Fork {
    }
    object phil1 : Philosopher {
      left = fork1;
      right = fork2;
    }
    object phil2 : Philosopher {
      left = fork2;
      right = fork3;
    }
    object phil3 : Philosopher {
      left = fork3;
      right = fork4;
    }
    object phil4 : Philosopher {
      left = fork4;
      right = fork1;
    }

    assertion not_deadlocking {
      AG not deadlock;
    }

    assertion not_deadlocking_LTL {
      G not deadlock;
    }

    assertion all_taken_LTL {
      G not (phil1.inState(Taking) and phil2.inState(Taking) and phil3.inState(Taking) and phil4.inState(Taking) and
             fork1.inState(Taken) and fork2.inState(Taken) and fork3.inState(Taken) and fork4.inState(Taken));
    }

    assertion all_taken {
      EF phil1.inState(Taking) and phil2.inState(Taking) and phil3.inState(Taking) and phil4.inState(Taking) and
         fork1.inState(Taken) and fork2.inState(Taken) and fork3.inState(Taken) and fork4.inState(Taken);
    }
  }

  collaboration five {
    object fork1 : Fork {
    }
    object fork2 : Fork {
    }
    object fork3 : Fork {
    }
    object fork4 : Fork {
    }
    object fork5 : Fork {
    }
    object phil1 : Philosopher {
      left = fork1;
      right = fork2;
    }
    object phil2 : Philosopher {
      left = fork2;
      right = fork3;
    }
    object phil3 : Philosopher {
      left = fork3;
      right = fork4;
    }
    object phil4 : Philosopher {
      left = fork4;
      right = fork5;
    }
    object phil5 : Philosopher {
      left = fork5;
      right = fork1;
    }

    assertion not_deadlocking {
      G not deadlock;
    }

    assertion all_taken_LTL {
      G not (phil1.inState(Taking) and phil2.inState(Taking) and phil3.inState(Taking) and phil4.inState(Taking) and phil5.inState(Taking) and
             fork1.inState(Taken) and fork2.inState(Taken) and fork3.inState(Taken) and fork4.inState(Taken) and fork5.inState(Taken));
    }

    assertion all_taken {
      EF phil1.inState(Taking) and phil2.inState(Taking) and phil3.inState(Taking) and phil4.inState(Taking) and phil5.inState(Taking) and
         fork1.inState(Taken) and fork2.inState(Taken) and fork3.inState(Taken) and fork4.inState(Taken) and fork5.inState(Taken);
    }
  }

  collaboration six {
    object fork1 : Fork {
    }
    object fork2 : Fork {
    }
    object fork3 : Fork {
    }
    object fork4 : Fork {
    }
    object fork5 : Fork {
    }
    object fork6 : Fork {
    }
    object phil1 : Philosopher {
      left = fork1;
      right = fork2;
    }
    object phil2 : Philosopher {
      left = fork2;
      right = fork3;
    }
    object phil3 : Philosopher {
      left = fork3;
      right = fork4;
    }
    object phil4 : Philosopher {
      left = fork4;
      right = fork5;
    }
    object phil5 : Philosopher {
      left = fork5;
      right = fork6;
    }
    object phil6 : Philosopher {
      left = fork6;
      right = fork1;
    }

    assertion not_deadlocking {
      G not deadlock;
    }
  }

  collaboration seven {
    object fork1 : Fork {
    }
    object fork2 : Fork {
    }
    object fork3 : Fork {
    }
    object fork4 : Fork {
    }
    object fork5 : Fork {
    }
    object fork6 : Fork {
    }
    object fork7 : Fork {
    }
    object phil1 : Philosopher {
      left = fork1;
      right = fork2;
    }
    object phil2 : Philosopher {
      left = fork2;
      right = fork3;
    }
    object phil3 : Philosopher {
      left = fork3;
      right = fork4;
    }
    object phil4 : Philosopher {
      left = fork4;
      right = fork5;
    }
    object phil5 : Philosopher {
      left = fork5;
      right = fork6;
    }
    object phil6 : Philosopher {
      left = fork6;
      right = fork7;
    }
    object phil7 : Philosopher {
      left = fork7;
      right = fork1;
    }

    assertion not_deadlocking {
      G not deadlock;
    }
  }
}
