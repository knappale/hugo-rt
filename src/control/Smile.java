package control;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UClass;
import uml.UModel;
import uml.smile.SMachine;
import uml.statemachine.UStateMachine;
import util.Message;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class Smile extends Output {
  static void execute(Model model, String baseName) {
    UModel uModel = model.getModel();

    Message.log("Translating UML state machines in `" + uModel.getName() + "' into Smile...");
    StringBuilder contentsBuilder = new StringBuilder();
    String postfix = "";
    for (UClass uClass : uModel.getClasses()) {
      UStateMachine uStateMachine = uClass.getStateMachine();
      if (uStateMachine == null)
        continue;

      contentsBuilder.append(postfix);
      SMachine sMachine = uStateMachine.getMachine(uModel.getProperties());
      contentsBuilder.append(sMachine.declaration());
      postfix = "\n\n";
    }

    String fileName = baseName;
    if (fileName.equals(""))
      fileName = model.extractFileName(".smile");

    Output.writeFile("Smile state machines", fileName, contentsBuilder.toString());
  }
}
