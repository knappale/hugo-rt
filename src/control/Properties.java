package control;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNull;

import util.properties.BooleanProperty;
import util.properties.IntegerProperty;
import util.properties.Property;
import util.properties.PropertyException;
import util.properties.PropertyGroup;
import util.properties.StringProperty;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Properties {
  private static @NonNull Properties DEFAULT = new Properties();

  @NonNull IntegerProperty networkCapacity;
  @NonNull IntegerProperty maxDelay;
  @NonNull BooleanProperty blocking;
  @NonNull IntegerProperty externalQueueCapacity;
  @NonNull IntegerProperty internalQueueCapacity;
  @NonNull IntegerProperty deferredQueueCapacity;
  @NonNull IntegerProperty intMin;
  @NonNull IntegerProperty intMax;
  @NonNull BooleanProperty mutex;
  @NonNull BooleanProperty smileBased;
  @NonNull BooleanProperty fixedOrderRegions;
  @NonNull BooleanProperty fixedOrderTransitionFiring;
  @NonNull BooleanProperty phaseBased;
  @NonNull BooleanProperty eventBased;
  @NonNull BooleanProperty cycleBased;
  @NonNull BooleanProperty doubleChecks;
  @NonNull StringProperty packageName;

  private List<Property<@NonNull ?>> properties = new ArrayList<>();
  private List<PropertyGroup> propertyGroups = new ArrayList<>();

  private Properties() {
    this.properties.add(this.networkCapacity =
      new IntegerProperty("networkCapacity", "capacity of the network", "n", 2) {
        public void check(@NonNull Integer value) throws PropertyException {
          if (!(value >= 0))
            throw new PropertyException("Network capacity must not be negative");
        }
      });

    this.properties.add(this.maxDelay =
      new IntegerProperty("maxDelay", "maximum delay of the network (untimed if negative)", "d", 10) {
        public void check(@NonNull Integer value) throws PropertyException {
        }
      });

    this.properties.add(this.externalQueueCapacity =
      new IntegerProperty("externalQueueCapacity", "external queue capacity", "", 5) {
        public void check(@NonNull Integer value) throws PropertyException {
          if (!(value >= 0))
            throw new PropertyException("External queue capacity must be at least one");
        }
      });

    this.properties.add(internalQueueCapacity =
      new IntegerProperty("internalQueueCapacity", "internal queue capacity", "", 2) {
        public void check(@NonNull Integer value) throws PropertyException {
          if (!(value >= 0))
            throw new PropertyException("Internal queue capacity must be at least one");
        }
      });

    this.properties.add(this.deferredQueueCapacity =
      new IntegerProperty("deferredQueueCapacity", "deferred queue capacity", "", 2) {
        public void check(@NonNull Integer value) throws PropertyException {
          if (!(value >= 0))
            throw new PropertyException("Deferred queue capacity must be at least one");
        }
      });

    this.properties.add(this.blocking =
      new BooleanProperty("blocking", "blocking queues", "k", false));

    this.properties.add(this.intMin =
      new IntegerProperty("minInt", "lower integer bound", "", -32768) {
        public void check(@NonNull Integer value) throws PropertyException {
        }
      });

    this.properties.add(this.intMax =
      new IntegerProperty("maxInt", "upper integer bound", "", 32767) {
        public void check(@NonNull Integer value) throws PropertyException {
        }
      });

    this.properties.add(mutex =
      new BooleanProperty("mutex", "include mutex", "x", false));

    this.properties.add(this.smileBased =
      new BooleanProperty("smileMachine", "use Smile-based state machine translation", "m", true));

    this.properties.add(this.fixedOrderRegions =
      new BooleanProperty("fixedOrderRegions", "deterministic ordering of regions", "r", true));

    this.properties.add(this.fixedOrderTransitionFiring =
      new BooleanProperty("fixedOrderTransitionFiring", "deterministic ordering of transitions", "f", true));

    this.properties.add(this.phaseBased =
      new BooleanProperty("phaseBased", "use phase-based interaction translation", "p", false));

    this.properties.add(this.eventBased =
      new BooleanProperty("eventBased", "use event-based state machine translation", "e", true));

    this.properties.add(this.cycleBased =
      new BooleanProperty("cycleBased", "use cycle-based state machine translation", "y", false));

    this.properties.add(this.doubleChecks =
      new BooleanProperty("doubleChecks", "add double checks for completion and time events", "dc", false));

    this.properties.add(this.packageName =
      new StringProperty("packageName", "name of the Java package for code generation", "", "generated") {
        public void check(@NonNull String value) throws PropertyException {
        }
      });

    this.propertyGroups.add(
      new PropertyGroup("queueCapacity", "capacities of the external, internal, deferred queues", "q",
                        externalQueueCapacity, internalQueueCapacity, deferredQueueCapacity) {
        public void check(List<Object> values) throws PropertyException {
        }
      });

    this.propertyGroups.add(
      new PropertyGroup("intBounds", "integer lower and upper bound", "b",
                        intMin, intMax) {
        public void check(List<Object> values) throws PropertyException {
          if (!((Integer)values.get(0) < (Integer)values.get(1)))
            throw new PropertyException("Integer lower bound must be less than integer upper bound");
        }
      });

    this.propertyGroups.add(
      new PropertyGroup("smile", "Smile machine translation", "",
                        smileBased, eventBased, cycleBased) {
        public void check(List<Object> values) throws PropertyException {
          var smileBased = ((Boolean)values.get(0)).booleanValue();
          var eventBased  = ((Boolean)values.get(1)).booleanValue();
          var cycleBased  = ((Boolean)values.get(2)).booleanValue();
          if (!eventBased && cycleBased)
            throw new PropertyException("Event-based translation needed for cycle splitting");
          if (!smileBased && eventBased)
            throw new PropertyException("Smile-based translation needed for event splitting");
        }
      });
  }

  /**
   * Set the default properties.
   *
   * @param properties new default properties
   */
  public static void setDefault(@NonNull Properties properties) {
    DEFAULT = properties;
  }

  /**
   * @return the default properties
   */
  public static @NonNull Properties getDefault() {
    return DEFAULT;
  }

  /**
   * Commit changes to properties
   *
   * @throws PropertyException if some consistency check fails
   */
  public void commit() throws PropertyException {
    for (PropertyGroup propertyGroup : this.propertyGroups)
      propertyGroup.commit();
    for (Property<@NonNull ?> property : this.properties)
      property.commit();
  }

  /**
   * Abort changes to properties
   */
  public void abort() {
    for (PropertyGroup propertyGroup : this.propertyGroups)
      propertyGroup.abort();
    for (Property<@NonNull ?> property : this.properties)
      property.abort();
  }

  /**
   * @return a copy of this set of properties
   */
  public @NonNull Properties getCopy() {
    Properties copiedProperties = new Properties();
    try {
      for (Property<@NonNull ?> property : this.properties)
        copiedProperties.setPropertyByName(property.getName() + "=" + property.getValue().toString());
      copiedProperties.commit();
    }
    catch (PropertyException pe)
    {
    }
    return copiedProperties;
  }

  /**
   * Yield all properties in this property set that differ in value from the
   * corresponding property in the other set of properties.
   *
   * @param other another set of properties
   * @return list of differing properties
   */
  List<Property<@NonNull ?>> getDiffering(Properties other) {
    List<Property<@NonNull ?>> result = new ArrayList<>();

    if (other == null || other.properties == null)
      return this.properties;

    for (Property<@NonNull ?> property : this.properties) {
      Property<@NonNull ?> otherProperty = other.getPropertyByName(property.getName());
      if (otherProperty == null || !(property.equals(otherProperty)))
        result.add(property);
    }

    return result;  
  }

  /**
   * @return network capacity
   */
  public int getNetworkCapacity() {
    return this.networkCapacity.getValue();
  }

  /**
   * @return maximum delay of the network
   */
  public int getMaxDelay() {
    return this.maxDelay.getValue();
  }

  /**
   * @return external queue capacity
   */
  public int getExternalQueueCapacity() {
    return this.externalQueueCapacity.getValue();
  }

  /**
   * @return internal queue capacity
   */
  public int getInternalQueueCapacity() {
    return this.internalQueueCapacity.getValue();
  }

  /**
   * @return deferred queue capacity
   */
  public int getDeferredQueueCapacity() {
    return this.deferredQueueCapacity.getValue();
  }

  /**
   * @return whether the queues block
   */
  public boolean isBlocking() {
    return this.blocking.getValue();
  }

  /**
   * @return integer maximum
   */
  public int getIntMax() {
    return this.intMax.getValue();
  }

  /**
   * @return integer minimum
   */
  public int getIntMin() {
    return this.intMin.getValue();
  }

  /**
   * @return whether a mutex should be used
   */
  public boolean isMutex() {
    return this.mutex.getValue();
  }

  /**
   * @return whether a Smile-based translation scheme is to be used
   */
  public boolean isSmileBased() {
    return this.smileBased.getValue();
  }

  /**
   * @return whether a phase-based translation scheme for interactions should be used
   */
  public boolean isPhaseBased() {
    return this.phaseBased.getValue();
  }

  /**
   * @return whether the order of regions is fixed
   */
  public boolean isFixedOrderRegions() {
    return this.fixedOrderRegions.getValue();
  }

  /**
   * @return whether the order of firing transitions is fixed
   */
  public boolean isFixedOrderTransitionFiring() {
    return this.fixedOrderTransitionFiring.getValue();
  }

  /**
   * @return whether an event-based translation scheme for state machines should be used
   */
  public boolean isEventBased() {
    return this.eventBased.getValue();
  }

  /**
   * @return whether an event-based translation scheme for state machines should be used
   */
  public boolean isCycleBased() {
    return this.cycleBased.getValue();
  }

  /**
   * @return whether double checks for completion and time events should be performed
   */
  public boolean doDoubleChecks() {
    return this.doubleChecks.getValue();
  }

  /**
   * @return the name of Java package for code generation
   */
  @NonNull public String getJavaPackageName() {
    return this.packageName.getValue();
  }

  /**
   * Find a property by name.
   *
   * @param name name of the property
   * @return property with the name, or {@code null} if none is found
   */
  Property<@NonNull ?> getPropertyByName(@NonNull String name) {
    for (Property<@NonNull ?> property : this.properties) {
      if (property.matchesByName(name))
        return property;
    }
    return null;
  }

  /**
   * Set a named property.
   *
   * @param name name of the property
   * @param valueString value string to be set
   * @return whether the named property has been recognized
   * @throws PropertyException if name matched for some property, but value string could not be parsed
   */
  public boolean setPropertyByName(@NonNull String name, @NonNull String valueString) throws PropertyException {
    for (Property<@NonNull ?> property : this.properties) {
      if (property.parseByName(name + "=" + valueString)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Set a named property.
   * 
   * @param argument name property string
   * @return whether the named property has been recognized
   * @throws PropertyException if name matched for some property, but value string could not be parsed
   */
  public boolean setPropertyByName(@NonNull String argument) throws PropertyException {
    for (Property<@NonNull ?> property : this.properties) {
      if (property.parseByName(argument)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Set a named property as an option (starting with "-").
   * 
   * @param argument option property string
   * @return whether the named property has been recognized
   * @throws PropertyException if name matched for some property, but value string could not be parsed
   */
  public boolean setPropertyByOption(@NonNull String argument) throws PropertyException {
    for (Property<@NonNull ?> property : this.properties) {
      if (property.parseByOption(argument))
        return true;
    }
    for (PropertyGroup propertyGroup : this.propertyGroups) {
      if (propertyGroup.parseByOption(argument))
        return true;
    }
    return false;
  }

  /**
   * @return usage string
   */
  public String usage() {
    StringBuffer result = new StringBuffer();
    String postfix = "";
    for (Property<@NonNull ?> property : this.properties) {
      result.append(postfix);
      result.append(property.optionString("  "));
      postfix = "\n";
    }
    if (!postfix.equals(""))
      postfix = postfix + "\n";
    for (PropertyGroup propertyGroup : this.propertyGroups) {
      result.append(postfix);
      result.append(propertyGroup.optionString("  "));
      postfix = "\n";
    }
    return result.toString();
  }

  /**
   * Declaration string for the set of properties in this set of properties
   * which differ in their values from the corresponding properties
   * in another set of properties.
   * 
   * @param other another set of properties
   * @param prefix prefix per line (for, e.g., indentation)
   * @return declaration string with every line prefixed by prefix
   */
  public String declarationOfDiffering(Properties other, String prefix) {
    StringBuffer result = new StringBuffer();

    result.append(prefix);

    List<Property<@NonNull ?>> differings = this.getDiffering(other);
    if (differings == null || differings.size() == 0)
      return result.toString();

    String nextPrefix = prefix + "  ";
    result.append("properties {\n");
    for (Property<@NonNull ?> property : differings) {
      result.append(nextPrefix);
      result.append(property.declaration());
      result.append(";\n");
    }
    result.append(prefix);
    result.append("}");
    return result.toString();
  }

  /**
   * Declaration string for this set of properties
   * 
   * @param prefix prefix per line (for, e.g., indentation)
   * @return declaration string with every line prefixed by prefix
   */
  public String declaration(String prefix) {
    StringBuffer result = new StringBuffer();
    String nextPrefix = prefix + "  ";
    result.append(prefix);
    result.append("properties {\n");
    for (Property<@NonNull ?> property : this.properties) {
      result.append(nextPrefix);
      result.append(property.declaration());
      result.append(";\n");
    }
    result.append(prefix);
    result.append("}");
    return result.toString();
  }

  /**
   * Declaration string for this set of properties
   * 
   * @return declaration string
   */
  public String declaration() {
    return this.declaration("");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.properties);
  }
 
  @Override
  public boolean equals(Object object) {
    if (object == null)
      return false;
    try {
      Properties other = (Properties)object;
      return Objects.equals(this.properties, other.properties);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
}
