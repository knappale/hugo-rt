package control;


/**
 * @author knapp
 *
 */
public class UTE extends Output {
  private UTE() {
  }

  static void execute(Model model, String fileName) {
    if (fileName.equals(""))
      fileName = model.extractFileName(".ute");

    writeFile("UTE model", fileName, model.getModel().declaration());
  }  
}
