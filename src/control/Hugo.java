package control;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;

import uml.UModelException;
import util.Message;
import util.properties.PropertyException;


/**
 * HUGO/RT
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @version 0.9b
 */
public class Hugo {
  private static final String VERSION = "0.9b (2024-03-09)";

  private static void version() {
    Message.log("Hugo/RT " + VERSION);
  }

  // Inputs
  private static Model inputModel = null;
  private static Optional<@NonNull Trail> inputTrail = Optional.empty();

  // Option defaults
  private static control.@NonNull Properties properties = control.Properties.getDefault();

  private static Optional<@NonNull String> uteModelBaseName = Optional.empty();
  private static Optional<@NonNull String> smileComplexBaseName = Optional.empty();
  private static Optional<@NonNull String> idaBaseName = Optional.empty();
  private static Optional<@NonNull String> promelaSystemBaseName = Optional.empty();
  private static Optional<@NonNull String> uppaalSystemBaseName = Optional.empty();
  private static Optional<@NonNull String> javaCodeBaseDir = Optional.empty();
  private static Optional<@NonNull String> cppCodeBaseDir = Optional.empty();
  private static Optional<@NonNull String> arduinoCodeBaseDir = Optional.empty();
  private static Optional<@NonNull String> systemCBaseDir = Optional.empty();
  private static Optional<@NonNull String> umlTrailBaseName = Optional.empty();
  private static Optional<@NonNull String> dotBaseName = Optional.empty();
  private static Optional<@NonNull String> uppaalDotBaseName = Optional.empty();
  private static Optional<@NonNull String> idaDotBaseName = Optional.empty();

  private static void usage() {
    Message.usage("Usage: hugort [options] [trail[" + Trail.getExtensionOptions() + "]] [model[" + Model.getExtensionOptions() + "]]");
    Message.usage("");
    Message.usage("  -o, --output[uppaal|promela|smile|ute|dot|trail][=<filename>]");
    Message.usage("      output UPPAAL (default), Promela, Smile, UTE, or Dot system or");
    Message.usage("             UML trail to <filename>");
    Message.usage("  -o, --output(java|cpp|arduino|systemc)[=<basedir>]");
    Message.usage("      output Java, C++, Arduino C++, or SystemC code to <basedir>");
    Message.usage("  -c, --collaboration[=<name>]");
    Message.usage("      create objects for collaboration");
    Message.usage("  -a, --assertion[=<name>]");
    Message.usage("      translate assertion (repeat option for several)");
    Message.usage("  -i, --interaction[=<name>]");
    Message.usage("      create observer for interaction");
    Message.usage("  -t, --test[=<name>]");
    Message.usage("      create tester for test case");
    Message.usage("");
    Message.usage(properties.usage());
    Message.usage("");
    Message.usage("  -v, --verbose");
    Message.usage("      verbose");
    Message.usage("  -s, --silent");
    Message.usage("      silent (but showing warnings and errors)");
    Message.usage("  -V, --version");
    Message.usage("      show version and exit");
    Message.usage("  -h, --help");
    Message.usage("      show this help and exit");
    Message.usage("  -D, --debug");
    Message.usage("      debugging mode");
    Message.usage("");
    Message.usage("Example: hugort -n=3 -m --outputuppaal -osmile=model.smile model.xmi");
  }

  private static String getNamedString(String argument, String longForm, String shortForm) {
    String argumentLowerCase = new String(argument).toLowerCase();
    if (argumentLowerCase.equals("--" + longForm) || argument.equals("-" + shortForm) ||
        argumentLowerCase.startsWith("--" + longForm + "=") || argument.startsWith("-" + shortForm + "=")) {
      String argumentString = (argumentLowerCase.startsWith("--" + longForm) ? argument.substring(("--" + longForm).length()) : argument.substring(("-" + shortForm).length()));
      argumentString = (argumentString.indexOf("=") == -1 ? "" : argumentString.substring(argumentString.indexOf("=") + 1));
      return argumentString;
    }
    return null;
  }

  private static Model parseArguments(@NonNull String[] args) {
    Optional<@NonNull String> collaborationName = Optional.empty();
    Optional<@NonNull String> interactionName = Optional.empty();
    Optional<@NonNull String> testName = Optional.empty();
    List<@NonNull String> assertionNames = new ArrayList<>();

    int argNum = 0;
    while (argNum < args.length && args[argNum].startsWith("-")) {
      String argument = args[argNum];
      String argumentLowerCase = new String(argument).toLowerCase();
      String tmpString = null;

      try {
        if (properties.setPropertyByOption(argument)) {
          argNum++;
          continue;
        }
      }
      catch (PropertyException pe) {
        Message.warning("" + pe.getMessage());
        argNum++;
        continue;
      }

      tmpString = getNamedString(argument, "collaboration", "c");
      if (tmpString != null) {
        collaborationName = Optional.of(tmpString);
        argNum++;
        continue;
      }

      tmpString = getNamedString(argument, "interaction", "i");
      if (tmpString != null) {
        interactionName = Optional.of(tmpString);
        argNum++;
        continue;
      }

      tmpString = getNamedString(argument, "test", "t");
      if (tmpString != null) {
        testName = Optional.of(tmpString);
        argNum++;
        continue;
      }

      tmpString = getNamedString(argument, "assertion", "a");
      if (tmpString != null) {
        assertionNames.add(tmpString);
        argNum++;
        continue;
      }

      if (argumentLowerCase.startsWith("--output") || argument.startsWith("-o")) {
        argNum++;
        String outputString = (argumentLowerCase.startsWith("--output") ? argument.substring("--output".length()) : argument.substring("-o".length()));
        String outputFormatString = (outputString.indexOf("=") == -1 ? outputString : outputString.substring(0, outputString.indexOf("=")));
        String outputFileString = (outputString.indexOf("=") == -1 ? "" : outputString.substring(outputString.indexOf("=") + 1));
        assert (outputFileString != null);

        if (outputFormatString.equals("ute")) {
          uteModelBaseName = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("smile")) {
          smileComplexBaseName = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("ida")) {
          idaBaseName = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("promela")) {
          promelaSystemBaseName = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("uppaal") || outputFormatString.equals("")) {
          uppaalSystemBaseName = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("java")) {
          javaCodeBaseDir = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("cpp")) {
          cppCodeBaseDir = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("arduino")) {
          arduinoCodeBaseDir = Optional.of(outputFileString);
          try {
            properties.setPropertyByName("cycleBased=true");
          }
          catch (PropertyException pe) {
            Message.warning("" + pe.getMessage());
          }
          continue;
        }
        if (outputFormatString.equals("systemc")) {
          systemCBaseDir = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("dot")) {
          dotBaseName = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("uppaaldot")) {
          uppaalDotBaseName = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("idadot")) {
          idaDotBaseName = Optional.of(outputFileString);
          continue;
        }
        if (outputFormatString.equals("trail")) {
          umlTrailBaseName = Optional.of(outputFileString);
          continue;
        }
        Message.warning("Unknown output format `" + outputFormatString + "', no output from this option");
        continue;
      }
      
      if (argumentLowerCase.equals("--debug") || argument.equals("-D")) {
        argNum++;
        Message.debug = true;
        continue;
      }

      if (argumentLowerCase.equals("--verbose") || argument.equals("-v")) {
        argNum++;
        Message.verbosity = Message.MAXVERBOSITY;
        continue;
      }

      if (argumentLowerCase.equals("--silent") || argument.equals("-s")) {
        argNum++;
        Message.verbosity = Message.MINVERBOSITY;
        continue;
      }

      if (argumentLowerCase.equals("--version") || argument.equals("-V")) {
        Hugo.version();
        java.lang.System.exit(0);
      }

      if (argumentLowerCase.equals("--help") || argument.equals("-h")) {
        Hugo.version();
        Message.usage("");
        Hugo.usage();
        java.lang.System.exit(0);
      }

      Message.warning("Unknown option: " + argument);
      argNum++;
    }

    try {
      properties.commit();
      Properties.setDefault(properties);
    }
    catch (PropertyException pe) {
      Message.warning(pe.getMessage());
    }

    if (argNum >= args.length ||
        args[argNum].equals("-")) {
      inputModel = Model.create("-", collaborationName, interactionName, testName, assertionNames);
      return inputModel;
    }

    if (argNum < args.length - 1)
      inputTrail = Optional.of(Trail.create(args[argNum++]));

    if (umlTrailBaseName.isPresent() && !inputTrail.isPresent()) {
      Message.error("No input trail for generating UML trail given");
      System.exit(-1);
    }

    inputModel = Model.create(args[argNum], collaborationName, interactionName, testName, assertionNames);

    if (argNum < args.length - 1)
      Message.warning("Model file name is not the last argument, ignoring remaining arguments");

    return inputModel;
  }

  public static void main(@NonNull String[] args) {
    // Read in default properties and file names
    parseArguments(args);
    version();

    Model model = inputModel;
    assert (model != null) : "@AssumeAssertion(nullness)";
    Optional<@NonNull Trail> trail = inputTrail;

    // Outputs
    try {
      boolean executed = false;

      if (uteModelBaseName.isPresent()) {
        UTE.execute(model, uteModelBaseName.get());
        executed = true;
      }
      
      if (dotBaseName.isPresent()) {
        Dot.execute(model, dotBaseName.get());
        executed = true;
      }

      if (smileComplexBaseName.isPresent()) {
        Smile.execute(model, smileComplexBaseName.get());
        executed = true;
      }

      if (idaBaseName.isPresent() ||
          idaDotBaseName.isPresent()) {
        Ida.execute(model, idaBaseName, idaDotBaseName);
        executed = true;
      }

      if (promelaSystemBaseName.isPresent() ||
          (umlTrailBaseName.isPresent() && trail.isPresent() && trail.get().isSpin())) {
        Promela.execute(model, promelaSystemBaseName,
                        trail, umlTrailBaseName);
        executed = true;
      }

      if (uppaalSystemBaseName.isPresent() ||
          (trail.isPresent() && trail.get().isUPPAAL()) ||
          uppaalDotBaseName.isPresent()) {
        UPPAAL.execute(model, uppaalSystemBaseName, uppaalDotBaseName,
                       trail, umlTrailBaseName);
        executed = true;
      }

      if (javaCodeBaseDir.isPresent()) {
        Java.execute(model, javaCodeBaseDir.get());
        executed = true;
      }

      if (cppCodeBaseDir.isPresent()) {
        Cpp.execute(model, cppCodeBaseDir.get());
        executed = true;
      }

      if (arduinoCodeBaseDir.isPresent()) {
        Arduino.execute(model, arduinoCodeBaseDir.get());
        executed = true;
      }

      if (systemCBaseDir.isPresent()) {
        SystemC.execute(model, systemCBaseDir.get());
        executed = true;
      }

      // Just check the model syntactically
      if (!executed) {
        model.getModel();
      }
    }
    catch (UModelException ume) {
      Message.error(ume.getMessage());
      System.exit(-1);
    }
    catch (Throwable t) {
      StringWriter stackTraceWriter = new StringWriter();
      t.printStackTrace(new PrintWriter(stackTraceWriter, true));
      Message.fatalError("Uncaught exception: " + (t.getMessage() != null ? t.getMessage() + "\n" : "") + stackTraceWriter.toString());
      System.exit(-1);
    }
  }
}
