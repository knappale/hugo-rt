package control;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UCollaboration;
import uml.UModel;
import util.Message;

import static util.Formatter.quoted;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class Java extends Output {
  static void execute(Model model, String baseName) {
    UModel uModel = model.getModel();
    UCollaboration uCollaboration = model.getCollaboration();

    Message.log("Translating collaboration " + quoted(uCollaboration.getName()) + " UML model " + quoted(uModel.getName()) + " into Java code...");
    javacode.JSystem javaCode = translation.uml2java.UMLTranslator.translate(uCollaboration);

    String baseDirName = baseName;
    if (baseDirName.equals(""))
      baseDirName = model.extractFileName("");
    if (!baseDirName.endsWith(fileSeparator))
      baseDirName = baseDirName + fileSeparator;
    
    String generatedDirName = baseDirName + "generated" + fileSeparator;
    createDirectory(generatedDirName);

    for (javacode.JClass jclass : javaCode.getJClasses()) {
      String fileName = (new String(jclass.getQualifiedName()).replace('.', fileSeparator.charAt(0))) + ".java";
      // The qualified name already contains "generated"
      writeFile("Java code", baseDirName + fileName, jclass.declaration());
    }
  }  
}
