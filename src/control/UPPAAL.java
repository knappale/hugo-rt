package control;

import java.util.List;
import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;

import uml.UClass;
import uml.UCollaboration;
import uml.UContext;
import uml.UModel;
import uml.UModelException;
import uml.interaction.UInteraction;
import uml.ocl.OTCTLConstraint;
import uml.run.URun;
import uml.statemachine.UEvent;
import uml.statemachine.UStateMachine;
import uml.statemachine.UTransition;
import uppaal.TSystem;
import uppaal.run.TSystemState;
import util.Message;

import static util.Formatter.quoted;


public class UPPAAL extends Output {
  /**
   * Additional checks for translation to UPPAAL
   * 
   * @param uModel a UML model
   * @throws UModelException if an error is detected
   */
  static void check(@NonNull UModel uModel) throws UModelException {
    for (UClass uClass : uModel.getClasses()) {
      UStateMachine uStateMachine = uClass.getStateMachine();
      if (uStateMachine == null)
        continue;
      String stateMachineContext = "In state machine for class " + quoted(uClass.getName());
      
      for (UTransition uTransition : uStateMachine.getTransitions()) {
        UEvent uEvent = uTransition.getTrigger();
        UModelException uModelException = uEvent.new Cases<UModelException>().otherwise(() -> null).
            time((uState, lower, upper) -> {
                @NonNull UContext context = uEvent.getTimeContext();
                if (!lower.isConstant(context))
                  return new UModelException(stateMachineContext + ", lower time bound " + quoted(lower) + " is not a constant");
                if (!upper.isConstant(context))
                  return new UModelException(stateMachineContext + ", upper time bound " + quoted(upper) + " is not a constant");
                return null;
            }).
            apply();
        if (uModelException != null)
          throw uModelException;
      }
    }
  }

  static void execute(Model model, Optional<@NonNull String> baseName, Optional<@NonNull String> dotBaseName,
                      Optional<@NonNull Trail> trail, Optional<@NonNull String> uTrailBaseName) throws UModelException {
    UModel uModel = model.getModel();
    check(uModel);

    UCollaboration uCollaboration = model.getCollaboration();
    UInteraction uInteraction = model.getInteraction(uCollaboration);
    List<@NonNull OTCTLConstraint> tctlAssertions = model.getTCTLAssertions(uCollaboration);

    Message.log(translationMessage("UPPAAL", uCollaboration, uInteraction, null, tctlAssertions));
    TSystem system = translation.uml2uppaal.UMLTranslator.translate(uCollaboration, uInteraction, tctlAssertions);

    if (baseName.isPresent()) {
      String systemFileName = baseName.get();
      if (systemFileName.equals(""))
        systemFileName = model.extractFileName(".xml");

      String contents = (systemFileName.endsWith(".xta") ? system.declaration() : system.declarationXML());
      Output.writeFile("UPPAAL system", systemFileName, contents);

      if (!system.getQueries().isEmpty()) {
        String queriesFileName = systemFileName;
        if (queriesFileName.endsWith(".xml"))
          queriesFileName = queriesFileName.substring(0, queriesFileName.length() - ".xml".length()) + ".q";
        else {
          if (queriesFileName.endsWith(".xta"))
            queriesFileName = queriesFileName.substring(0, queriesFileName.length() - ".xta".length()) + ".q";
          else
            queriesFileName += ".q";
        }

        contents = "";
        for (uppaal.TQuery query : system.getQueries()) {
          contents += query.declaration();
          contents += "\n";
        }
        Output.writeFile("UPPAAL queries", queriesFileName, contents);
      }
    }

    if (trail.isPresent() && trail.get().isUPPAAL()) {
      String uTrailFileName = (uTrailBaseName.isPresent() ? uTrailBaseName.get() : "");
      if (uTrailFileName.equals(""))
        uTrailFileName = model.extractFileName(".utl");

      try {
        Message.log("Parsing UPPAAL trail from `" + trail.get().getFileName() + "'...");
        uppaal.run.parser.TrailReader trailReader = new uppaal.run.parser.TrailReader(trail.get().getInputStream(), system);
        List<TSystemState> uTrail = trailReader.getTrail();
        URun uRun = translation.uml2uppaal.UMLTranslator.reconstruct(uTrail, uCollaboration, uInteraction);
        Output.writeFile("trail", uTrailFileName, uRun.declaration());
      }
      catch (uppaal.run.parser.TrailException ge) {
        Message.error(ge.getMessage());
        java.lang.System.exit(-1);
      }
    }

    if (uTrailBaseName.isPresent() && !trail.isPresent())
      Message.warning("UML trail requested, but no trail file given.  Skipping this request");

    if (dotBaseName.isPresent()) {
      String dotFileName = dotBaseName.get();
      if (dotFileName.equals(""))
        dotFileName = model.extractFileName(".dot");

      Message.log("Translating UPPAAL templates into Dot...");
      translation.uppaal2dot.DotFormatter dotFormatter = new translation.uppaal2dot.DotFormatter();
      system.accept(dotFormatter);
      Output.writeFile("dot", dotFileName, dotFormatter.declaration());
    }
  }
}
