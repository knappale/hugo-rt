package control;

import org.eclipse.jdt.annotation.NonNull;

import util.Message;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class SystemC extends Output {
  static void execute(@NonNull Model model, @NonNull String baseName) {
    uml.@NonNull UCollaboration uCollaboration = model.getCollaboration();

    Message.log("Translating UML model `" + uCollaboration.getModel().getName() + "' into SystemC ...");
    translation.uml2systemc.UMLTranslator translator = translation.uml2systemc.UMLTranslator.translate(uCollaboration);

    @NonNull String basePath = baseName;
    if (basePath.equals(""))
      basePath = model.extractFileName("");
    if (!basePath.endsWith(fileSeparator))
      basePath = basePath + fileSeparator;

    @NonNull String sourcePath = "src" + fileSeparator;
    @NonNull String basedSourcePath = basePath + sourcePath;
    createDirectory(basedSourcePath); 

    @NonNull String includePath = "include" + fileSeparator;
    @NonNull String basedIncludePath = basePath + includePath;
    createDirectory(basedIncludePath); 

    @NonNull String relativeIncludePath = ".." + fileSeparator + includePath;

    @NonNull String objectsPath = "o" + fileSeparator;
    @NonNull String basedObjectsPath = basePath + objectsPath;
    createDirectory(basedObjectsPath); 

    for (cpp.CClass cClass : translator.getCPPCode().getCClasses()) {
      writeFile("SystemC", basedSourcePath + cClass.getImplementationFileName(), "#include \"" + relativeIncludePath + cClass.getDeclarationFileName() + "\"\n\n" +
                                                                                 cClass.getDefinitions());
      writeFile("SystemC", basedIncludePath + cClass.getDeclarationFileName(), "#include \"sc_eventqueue.h\"\n" + 
                                                                               "#include <systemc.h>\n" +
                                                                               cClass.getDeclaration());
    }
    writeFile("SystemC", basedSourcePath + "SystemCController.cpp", translator.getController());
    writeFile("SystemC", basedIncludePath + "sc_eventqueue.h", translator.getEventQueue());
    writeFile("SystemC", basePath + "Makefile", translator.getMakefile());
  }
}
