package control;

import java.util.List;
import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;

import promela.PSystem;
import promela.run.PSystemState;
import uml.UCollaboration;
import uml.interaction.UInteraction;
import uml.ocl.OLTLConstraint;
import uml.testcase.UTestCase;
import util.Message;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 *
 */
public class Promela extends Output {
  static void execute(@NonNull Model model, Optional<@NonNull String> baseName,
                      Optional<@NonNull Trail> trail, Optional<@NonNull String> umlTrailBaseName) {
    @NonNull UCollaboration uCollaboration = model.getCollaboration();
    UInteraction uInteraction = model.getInteraction(uCollaboration);
    UTestCase uTestCase = model.getTestCase(uCollaboration);
    @NonNull List<@NonNull OLTLConstraint> ltlAssertions = model.getLTLAssertions(uCollaboration);

    Message.log(translationMessage("Promela", uCollaboration, uInteraction, uTestCase, ltlAssertions));
    PSystem system = translation.uml2promela.UMLTranslator.translate(uCollaboration, uInteraction, uTestCase, ltlAssertions);

    if (baseName.isPresent()) {
      @NonNull String systemFileName = baseName.get();
      if (systemFileName.equals(""))
        systemFileName = model.extractFileName(".pml");

      Output.writeFile("Promela system", systemFileName, system.code());
      if (system.hasSpecification()) {
        var specification = system.getSpecification(); assert specification != null;
        @NonNull String specificationFileName = systemFileName + ".ltl";
        Output.writeFile("Promela specification", specificationFileName, specification.code());
      }
    }
        
    if (trail.isPresent() && trail.get().isSpin()) {
      @NonNull String umlTrailFileName = (umlTrailBaseName.isPresent() ? umlTrailBaseName.get() : "");
      if (umlTrailFileName.equals(""))
        umlTrailFileName = model.extractFileName(".utl");

      List<@NonNull PSystemState> pTrail = null;
      try {
        Message.log("Parsing Spin trail from `" + trail.get().getFileName() + "'...");
        promela.run.parser.TrailReader trailReader = new promela.run.parser.TrailReader(trail.get().getInputStream(), system);
        pTrail = trailReader.getTrail();
      }
      catch (promela.run.parser.TrailException ge) {
        Message.error(ge.getMessage());
        java.lang.System.exit(-1);
      }
      assert (pTrail != null);

      Message.log("Reconstructing UML trail from `" + trail.get().getFileName() + "'...");
      uml.run.URun umlRun = translation.uml2promela.UMLTranslator.reconstruct(pTrail, uCollaboration, uInteraction, uTestCase);
      Output.writeFile("trail", umlTrailFileName, umlRun.declaration());
    }

    if (umlTrailBaseName.isPresent() && !trail.isPresent())
      Message.warning("UML trail requested, but no trail file given.  Skipping this request");
  }
}
