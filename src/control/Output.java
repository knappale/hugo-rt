package control;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import uml.UCollaboration;
import uml.interaction.UInteraction;
import uml.ocl.OConstraint;
import uml.testcase.UTestCase;

import static util.Formatter.quoted;
import util.Message;

/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 *
 */
public class Output {
  static @NonNull String fileSeparator = "/";
  static {
    String _fileSeparator = System.getProperty("file.separator");
    if (_fileSeparator != null && !_fileSeparator.equals(""))
      fileSeparator = _fileSeparator;
  }

  static String translationMessage(@NonNull String name, UCollaboration uCollaboration, UInteraction uInteraction, UTestCase uTestCase, @NonNull List<? extends @NonNull OConstraint> oAssertions) {
    StringBuffer result = new StringBuffer();
    String separator = "";
    result.append("Translating collaboration ");
    result.append(quoted(uCollaboration.getName()));
    result.append(" ");
    if (uInteraction != null) {
      result.append("with interaction ");
      result.append(quoted(uInteraction.getName()));
      result.append(" ");
      separator = "and ";
    }
    if (uTestCase != null) {
      result.append(separator);
      result.append("with test case ");
      result.append(quoted(uTestCase.getName()));
      result.append(" ");
      separator = "and ";
    }
    if (!oAssertions.isEmpty()) {
      result.append(separator);
      if (oAssertions.size() > 1)
        result.append("with assertions ");
      else
        result.append("with assertion ");
      String prefix = "";
      for (uml.ocl.OConstraint assertion : oAssertions) {
        result.append(prefix);
        result.append(quoted(assertion.getName()));
        prefix = ", ";
      }
      result.append(" ");
    }
    result.append("in UML model ");
    result.append(quoted(uCollaboration.getModel().getName()));
    result.append(" into ");
    result.append(name);
    result.append("...");
    return result.toString();
  }

  static void createDirectory(@NonNull String dirName) {
    File dir = new java.io.File(dirName);
    if (dir.exists())
      return;

    Message.log("Creating directory " + quoted(dirName) + "...");
    if (!dir.mkdirs()) {
      Message.error("Cannot create directory " + quoted(dirName));
      System.exit(-1);
    }
  }

  static void writeFile(@NonNull String message, String fileName, String contents) {
    try (FileWriter output = new FileWriter(fileName)) {
      Message.log("Writing " + message + " to " + quoted(fileName) + "...");
      output.write(contents);
      output.close();
    }
    catch (IOException ioe) {
      Message.error("Cannot write " + message + " to " + quoted(fileName));
      System.exit(-1);
    }
  }
}
