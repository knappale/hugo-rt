package control;

import util.Message;

import static util.Formatter.quoted;


/**
 * @author knapp
 *
 */
public class Dot extends Output {
  static void execute(Model model, String fileName) {
    uml.UModel uModel = model.getModel();
    
    Message.log("Translating UML state machines in " + quoted(uModel.getName()) + " into Dot ...");
    String contents = translation.uml2dot.Model.translate(uModel);

    Output.writeFile("dot", fileName, contents);
  }  
}
