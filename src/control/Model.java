package control;

import java.io.BufferedInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.eclipse.jdt.annotation.NonNull;

import uml.UCollaboration;
import uml.UModel;
import uml.UModelException;
import uml.interaction.UInteraction;
import uml.ocl.OConstraint;
import uml.ocl.OLTLConstraint;
import uml.ocl.OLTLFormula;
import uml.ocl.OTCTLConstraint;
import uml.ocl.OTCTLFormula;
import uml.testcase.UTestCase;
import util.Message;

import static util.Formatter.quoted;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class Model {
  static String fileSeparator = null;
  static {
    fileSeparator = System.getProperty("file.separator");
    if (fileSeparator == null || fileSeparator.equals(""))
      fileSeparator = "/";
  }

  private static final @NonNull List<@NonNull String> unzippedExtensions = new ArrayList<>();
  static {
    unzippedExtensions.add(".xmi");
    unzippedExtensions.add(".xml");
    unzippedExtensions.add(".uml");
    unzippedExtensions.add(".ute");
  }
  private static final @NonNull List<@NonNull String> zippedExtensions = new ArrayList<>();
  static {
    zippedExtensions.add(".zargo");
    zippedExtensions.add(".zip");
  }

  private static @NonNull List<@NonNull String> getUnzippedExtensions() {
    return unzippedExtensions;
  }

  private static @NonNull List<@NonNull String> getZippedExtensions() {
    return zippedExtensions;
  }

  private static @NonNull List<@NonNull String> getExtensions() {
    @NonNull List<@NonNull String> modelExtensions = new ArrayList<>();
    modelExtensions.addAll(getUnzippedExtensions());
    modelExtensions.addAll(getZippedExtensions());
    return modelExtensions;
  }

  static @NonNull String getExtensionOptions() {
    StringBuffer result = new StringBuffer();
    String postfix = "";
    for (String modelExtension : getExtensions()) {
      result.append(postfix);
      result.append(modelExtension);
      postfix = "|";
    }
    String resultString = result.toString();
    assert (resultString != null);
    return resultString;
  }

  private static @NonNull String getUnzippedExtensionOptions() {
    StringBuffer result = new StringBuffer();
    String postfix = "";
    for (String modelExtension : getExtensions()) {
      result.append(postfix);
      result.append(modelExtension);
      postfix = "|";
    }
    String resultString = result.toString();
    assert (resultString != null);
    return resultString;
  }

  private @NonNull String fileName = "";
  private BufferedInputStream inputStream = null;

  private Optional<@NonNull String> collaborationName = Optional.empty();
  private Optional<@NonNull String> interactionName = Optional.empty();
  private Optional<@NonNull String> testName = Optional.empty();
  private List<@NonNull String> assertionNames = new ArrayList<>();

  private UModel uModel;
  
  private Model() {
  }

  static @NonNull Model create(@NonNull String arg,
                               Optional<@NonNull String> collaborationName,
                               Optional<@NonNull String> interactionName,
                               Optional<@NonNull String> testName,
                               List<@NonNull String> assertionNames) {
    @NonNull Model instance = new Model();
    
    if (arg.equals("-")) {
      instance.fileName = "stdin";
      instance.inputStream = new BufferedInputStream(java.lang.System.in);
      return instance;
    }

    instance.fileName = arg;
    try {
      String extension = "";
      for (String e : getExtensions()) {
        if (instance.fileName.endsWith(e)) {
          if (!new File(instance.fileName).exists()) {
            Message.error("Model file " + instance.fileName + " does not exist");
            System.exit(-1);
          }
          extension = e;
          break;
        }
      }
      if (extension.equals("")) {
        for (String e : getExtensions()) {
          if (new File(instance.fileName + e).exists()) {
            extension = e;
            instance.fileName += extension;
            break;
          }
        }
        if (extension.equals("")) {
          Message.error("Model file " + instance.fileName + "(" + getExtensionOptions() + ") does not exist");
          System.exit(-1);
        }
      }

      if (getUnzippedExtensions().contains(extension)) {
        instance.inputStream = new BufferedInputStream(new java.io.FileInputStream(instance.fileName));
      }
      else {
        ZipFile zipFile = new ZipFile(instance.fileName);
        boolean foundEntry = false;
        for (Enumeration<? extends ZipEntry> entries = zipFile.entries(); !foundEntry && entries.hasMoreElements();) {
          ZipEntry entry = entries.nextElement();
          for (String e : getUnzippedExtensions()) {
            if (entry.getName().endsWith(e)) {
              instance.inputStream = new BufferedInputStream(zipFile.getInputStream(entry));
              foundEntry = true;
              break;
            }
          }
        }
        if (!foundEntry) {
          zipFile.close();
          Message.error("Model file " + instance.fileName + " does not contain an (" + getUnzippedExtensionOptions() + ") entry");
          System.exit(-1);
        }
      }
    }
    catch (java.io.IOException ioe) {
      Message.error("Cannot read file " + quoted(instance.fileName));
      System.exit(-1);
    }

    instance.collaborationName = collaborationName;
    instance.interactionName = interactionName;
    instance.testName = testName;
    instance.assertionNames = assertionNames;

    return instance;
  }

  @NonNull String getFileName() {
    return this.fileName;
  }

  BufferedInputStream getInputStream() {
    return this.inputStream;
  }

  @NonNull String extractFileName(String extension) {
    String result = this.fileName;
    for (String modelExtension : getExtensions()) {
      if (result.endsWith(modelExtension)) {
        result = result.substring(0, result.length()-modelExtension.length()) + extension;
        break;
      }
    }
    while (result.indexOf(fileSeparator) != -1)
      result = result.substring(result.indexOf(fileSeparator)+1);
    if (!(result.endsWith(extension)))
      result += extension;
    return result;
  }
  
  @NonNull UModel getModel() {
    UModel uModel = this.uModel;
    if (uModel != null)
      return uModel;

    try {
      Message.log("Parsing UML model from " + quoted(this.getFileName()) + "...");
      uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(this.getInputStream());
      Message.log("Checking UML model " + quoted(modelReader.getModelName()) + "...");
      uModel = modelReader.getModel();
    }
    catch (UModelException ge) {
      Message.error(ge.getMessage());
      System.exit(-1);
    }

    if (uModel == null) {
      Message.fatalError("No model found.");
      System.exit(-1);
    }
    assert (uModel != null);
    this.uModel = uModel;
    return uModel;
  }

  @NonNull UCollaboration getCollaboration() {
    UCollaboration uCollaboration = null;

    // If translation of a collaboration was requested
    if (this.collaborationName.isPresent()) {
      // If a collaboration has been named properly
      if (!this.collaborationName.get().equals("")) {
        for (UCollaboration c : this.getModel().getCollaborations()) {
          if (c.getName().equals(this.collaborationName.get())) {
            uCollaboration = c;
            break;
          }
        }
        if (uCollaboration == null) {
          if (!this.getModel().getCollaborations().isEmpty()) {
            uCollaboration = this.getModel().getCollaborations().iterator().next();
            Message.warning("No collaboration named " + quoted(this.collaborationName.get()) + " found, using collaboration " + quoted(uCollaboration.getName()));
          }
          else {
            uCollaboration = this.getModel().getEmptyUCollaboration();
            Message.warning("No collaboration named " + quoted(this.collaborationName.get()) + " found, using empty collaboration");
          }
        }
      }
      // Some collaboration would be enough
      else {
        // There is a collaboration
        if (!this.getModel().getCollaborations().isEmpty()) {
          uCollaboration = this.getModel().getCollaborations().iterator().next();
          if (this.getModel().getCollaborations().size() > 1)
            Message.warning("Choosing arbitrarily collaboration named " + quoted(uCollaboration.getName()));
        }
        // There is none, so fix it
        else {
          uCollaboration = this.getModel().getEmptyUCollaboration();
          Message.warning("No collaboration found, using empty collaboration");
        }
      }
    }
    // No collaboration has been requested, thus the empty one is used
    else {
      uCollaboration = this.getModel().getEmptyUCollaboration();
      Message.info("No collaboration requested, using empty collaboration");
    }

    assert (uCollaboration != null);
    return uCollaboration;
  }

  UInteraction getInteraction(@NonNull UCollaboration uCollaboration) {
    UInteraction uInteraction = null;

    // If translation of an interaction was requested
    if (this.interactionName.isPresent()) {
      // If an interaction has been named properly
      if (!this.interactionName.get().equals("")) {
        for (UInteraction i : uCollaboration.getInteractions()) {
          if (i.getName().equals(this.interactionName.get()))
            uInteraction = i;
        }
        if (uInteraction == null) {
          if (!uCollaboration.getInteractions().isEmpty()) {
            uInteraction = uCollaboration.getInteractions().iterator().next();
            Message.warning("No interaction named " + quoted(this.interactionName.get()) + " found, using interaction " + quoted(uInteraction.getName()));
          }
          else {
            uInteraction = uCollaboration.getEmptyInteraction();
            Message.warning("No interaction named " + quoted(this.interactionName.get()) + " found, using empty interaction");
          }
        }
      }
      // Some interaction would be enough
      else {
        // There is an interaction
        if (!uCollaboration.getInteractions().isEmpty()) {
          uInteraction = uCollaboration.getInteractions().iterator().next();
          if (uCollaboration.getInteractions().size() > 1)
            Message.warning("Choosing arbitrarily interaction named " + quoted(uInteraction.getName()) + "");
        }
        // There is none, so fix it
        else {
          uInteraction = uCollaboration.getEmptyInteraction();
          Message.warning("No interaction found, using empty interaction");
        }
      }
    }

    return uInteraction;
  }
  
  UTestCase getTestCase(@NonNull UCollaboration uCollaboration) {
    UTestCase uTestCase = null;

    // If translation of an interaction was requested
    if (this.testName.isPresent()) {
      // If an interaction has been named properly
      if (!this.testName.get().equals("")) {
        for (UTestCase t : uCollaboration.getTestCases()) {
          if (t.getName().equals(this.testName.get()))
            uTestCase = t;
        }
        if (uTestCase == null) {
          if (!uCollaboration.getTestCases().isEmpty()) {
            uTestCase = uCollaboration.getTestCases().iterator().next();
            Message.warning("No test case named " + quoted(this.testName.get()) + " found, using test case " + quoted(uTestCase.getName()) + "");
          }
          else {
            uTestCase = uCollaboration.getEmptyTestCase();
            Message.warning("No test case named " + quoted(this.testName.get()) + " found, using empty test case");
          }
        }
      }
      // Some interaction would be enough
      else {
        // There is an interaction
        if (!uCollaboration.getTestCases().isEmpty())
          uTestCase = uCollaboration.getTestCases().iterator().next();
        // There is none, so fix it
        else {
          uTestCase = uCollaboration.getEmptyTestCase();
          Message.warning("No test case found in collaboration " + quoted(uCollaboration.getName()) + ", using empty test case");
        }
      }
    }

    return uTestCase;
  }
  
  @NonNull List<@NonNull OTCTLConstraint> getTCTLAssertions(@NonNull UCollaboration collaboration) {
    @NonNull List<@NonNull OTCTLConstraint> tctlAssertions = new ArrayList<>();

    // If translation of assertions was requested
    if (!assertionNames.isEmpty()) {
      for (String assertionName : assertionNames) {
        OTCTLConstraint assertion = null;
        // If an assertion has been named properly
        if (!assertionName.equals("")) {
          for (OConstraint a : collaboration.getConstraints()) {
            if (a.getName().equals(assertionName) && (a instanceof OTCTLConstraint))
              assertion = (OTCTLConstraint)a;
          }
          if (assertion == null) {
            Message.warning("No TCTL assertion named " + quoted(assertionName) + " found");
          }
          else {
            tctlAssertions.add(assertion);
          }
        }
      }

      // Some assertion would be enough
      OTCTLConstraint assertion = null;
      if (assertionNames.size() == 1 && assertionNames.contains("") && tctlAssertions.isEmpty()) {
        for (OConstraint a : collaboration.getConstraints()) {
          if (a instanceof OTCTLConstraint)
            assertion = (OTCTLConstraint)a;
        }
        // There is an assertion
        if (assertion != null) {
          tctlAssertions.add(assertion);
        }
        // There is none, so fix it
        else {
          assertion = OTCTLConstraint.ag("no_deadlock", OTCTLFormula.neg(OTCTLFormula.deadlock()));
          collaboration.addConstraint(assertion);
          tctlAssertions.add(assertion);
          Message.warning("No TCTL assertion found, using " + quoted(assertion));
        }
      }
    }

    return tctlAssertions;
  }

  @NonNull List<@NonNull OLTLConstraint> getLTLAssertions(@NonNull UCollaboration collaboration) {
    @NonNull List<@NonNull OLTLConstraint> ltlAssertions = new ArrayList<>();

    // If translation of assertions was requested
    if (!this.assertionNames.isEmpty()) {
      for (String assertionName : this.assertionNames) {
        OLTLConstraint assertion = null;
        // If an assertion has been named properly
        if (!assertionName.equals("")) {
          for (OConstraint a : collaboration.getConstraints()) {
            if (a.getName().equals(assertionName) && (a instanceof OLTLConstraint))
              assertion = (OLTLConstraint)a;
          }
          if (assertion == null) {
            Message.warning("No LTL assertion named " + quoted(assertionName) + " found");
          }
          else {
            ltlAssertions.add(assertion);
          }
        }
      }

      // Some assertion would be enough
      OLTLConstraint assertion = null;
      if (assertionNames.size() == 1 && this.assertionNames.contains("") && ltlAssertions.isEmpty()) {
        for (OConstraint a : collaboration.getConstraints()) {
          if (a instanceof OLTLConstraint)
            assertion = (OLTLConstraint)a;
        }
        // There is an assertion
        if (assertion != null) {
          ltlAssertions.add(assertion);
        }
        // There is none, so fix it
        else {
          assertion = new OLTLConstraint("simple", OLTLFormula.trueConst());
          collaboration.addConstraint(assertion);
          ltlAssertions.add(assertion);
          Message.warning("No LTL assertion found, using " + quoted(assertion));
        }
      }
    }

    return ltlAssertions;
  }
}
