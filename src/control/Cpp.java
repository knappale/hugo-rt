package control;

import org.eclipse.jdt.annotation.NonNull;

import util.Message;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class Cpp extends Output {
  static void execute(@NonNull Model model, @NonNull String baseName) {
    uml.@NonNull UCollaboration uCollaboration = model.getCollaboration();

    Message.log("Translating UML model `" + uCollaboration.getModel().getName() + "' into C++ ...");
    cpp.CSystem cSystem = translation.uml2cpp.UMLTranslator.translate(uCollaboration);

    @NonNull String basePath = baseName;
    if (basePath.equals(""))
      basePath = model.extractFileName("");
    if (!basePath.endsWith(fileSeparator))
      basePath = basePath + fileSeparator;

    @NonNull String sourcePath = "src" + fileSeparator;
    @NonNull String basedSourcePath = basePath + sourcePath;
    createDirectory(basedSourcePath);

    @NonNull String includePath = "include" + fileSeparator;
    @NonNull String basedIncludePath = basePath + includePath;
    createDirectory(basedIncludePath);

    @NonNull String relativeIncludePath = ".." + fileSeparator + includePath;

    for (cpp.CClass cClass : cSystem.getCClasses()) {
      writeFile("C++", basedSourcePath + cClass.getImplementationFileName(), "#include \"" + relativeIncludePath + cClass.getDeclarationFileName() + "\"\n\n" + cClass.getDefinitions());
      writeFile("C++", basedIncludePath + cClass.getDeclarationFileName(), cSystem.getHeaderFile(cClass, relativeIncludePath));
    }
    if (!cSystem.getMethods().isEmpty()) {
      String systemName = uCollaboration.getName();
      writeFile("C++", basedSourcePath + systemName + ".cpp", cSystem.getSystemFile(relativeIncludePath));
    }
  }
}
