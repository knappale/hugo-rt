package control;

import java.io.IOException;

import org.eclipse.jdt.annotation.NonNull;

import util.Message;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class Arduino extends Output {
  static void execute(@NonNull Model model, @NonNull String baseName) throws IOException {
    var uCollaboration = model.getCollaboration();

    Message.log("Translating UML model `" + uCollaboration.getModel().getName() + "' into C++/Arduino ...");
    var cSystem = translation.uml2cpp.arduino.UMLTranslator.translate(uCollaboration);

    @NonNull String basePath = baseName;
    if (basePath.equals(""))
      basePath = model.extractFileName("");
    if (!basePath.endsWith(fileSeparator))
      basePath = basePath + fileSeparator + uCollaboration.getName() + fileSeparator;
    createDirectory(basePath);
   
    @NonNull String sourcePath = "src" + fileSeparator;
    @NonNull String basedSourcePath = basePath + sourcePath;
    createDirectory(basedSourcePath);

    @NonNull String includePath = "include" + fileSeparator;
    @NonNull String basedIncludePath = basePath + includePath;
    createDirectory(basedIncludePath);
    @NonNull String relativeIncludePath = ".." + fileSeparator + "include" + fileSeparator;

    @NonNull String externalFileName = "external.hh";

    for (cpp.CClass cClass : cSystem.getCClasses()) {
      writeFile("C++", basedSourcePath + cClass.getImplementationFileName(), "#include \"" + relativeIncludePath + cClass.getDeclarationFileName() + "\"\n" +
                                                                             "#include \"" + relativeIncludePath + externalFileName + "\"\n\n" + cClass.getDefinitions());
      writeFile("C++", basedIncludePath + cClass.getDeclarationFileName(), cSystem.getHeaderFile(cClass, ""));
    }
    if (!cSystem.getMethods().isEmpty()) {
      String systemName = uCollaboration.getName();
      writeFile("C++", basePath + systemName + ".ino", "#include \"" + includePath + externalFileName + "\"\n\n" + cSystem.getSystemFile(includePath));
    }
  }
}
