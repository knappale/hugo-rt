package control;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;

import util.Message;

/**
 * @author knapp
 *
 */
public class Ida extends Output {
  private Ida() {
  }

  static void execute(@NonNull Model model, Optional<@NonNull String> baseName, Optional<@NonNull String> dotBaseName) {
    uml.@NonNull UCollaboration uCollaboration = model.getCollaboration();
    uml.interaction.UInteraction uInteraction = model.getInteraction(uCollaboration);
    if (uInteraction == null) {
      Message.warning("No interaction for translation to Ida given.  Skipping this request");
      return;
    }

    Message.log("Translating UML interaction `" + uInteraction.getName() + "' into Ida...");
    uml.ida.IAutomaton idaAutomaton = uInteraction.getAutomaton();

    if (baseName != null && baseName.isPresent()) {
      @NonNull String idaFileName = baseName.get();
      if (idaFileName.equals("")) {
        if (!uInteraction.getName().equals(""))
          idaFileName = uInteraction.getName() + ".ida";
        else
          idaFileName = model.extractFileName(".ida");
      }

      String contents = idaAutomaton.declaration();
      writeFile("Ida automaton", baseName.get(), contents);
    }

    if (dotBaseName != null && dotBaseName.isPresent()) {
      @NonNull String idaDotFileName = dotBaseName.get();
      if (idaDotFileName.equals(""))
        idaDotFileName = model.extractFileName(".dot");

      Message.log("Translating Ida automaton for `" + uInteraction.getName() + "' into Dot...");
      String contents = translation.ida2dot.Translator.translate(idaAutomaton);
      writeFile("dot", idaDotFileName, contents);
    }
  }  
}
