package control;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import util.Message;

/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 *
 */
public class Trail {
  private static final @NonNull List<@NonNull String> extensions = new ArrayList<@NonNull String>();
  static {
    extensions.add(".tl");
  }

  static @NonNull List<@NonNull String> getExtensions() {
    return extensions;
  }

  static @NonNull String getExtensionOptions() {
    StringBuffer result = new StringBuffer();
    String postfix = "";
    for (String extension : getExtensions()) {
      result.append(postfix);
      result.append(extension);
      postfix = "|";
    }
    String resultString = result.toString();
    assert (resultString != null);
    return resultString;
  }


  private @NonNull String fileName = "";
  private BufferedInputStream inputStream;
  private boolean isSpin = false;
  private boolean isUPPAAL = false;

  private Trail() {
  }

  static @NonNull Trail create(@NonNull String arg) {
    @NonNull Trail instance = new Trail();

    instance.fileName = arg;
    try {
      String extension = "";
      for (String e : getExtensions()) {
        if (instance.fileName.endsWith(e)) {
          if (!new File(instance.fileName).exists()) {
            Message.error("Trail file " + instance.fileName + " does not exist");
            System.exit(-1);
          }
          extension = e;
          break;
        }
      }
      if (extension.equals("")) {
        for (String e : getExtensions()) {
          if (new File(instance.fileName + e).exists()) {
            extension = e;
            instance.fileName += extension;
            break;
          }
        }
        if (extension.equals("")) {
          Message.error("Trail file " + instance.fileName + "(" + getExtensionOptions() + ") does not exist");
          System.exit(-1);
        }
      }

      instance.inputStream = new BufferedInputStream(new java.io.FileInputStream(instance.fileName));
      final int lookaheadNum = 1 << 18; // -AK seems to be the minimum possible, when working with marks
      try {
        // Set a mark that will be valid up to reading lookaheadNum
        // times such that we can reset to start of stream
        instance.inputStream.mark(lookaheadNum);
        BufferedReader modelReader = new BufferedReader(new InputStreamReader(instance.inputStream));
        char[] lookaheadChars = new char[lookaheadNum];
        for (int i = 0; i < lookaheadNum; i++) {
          int c = modelReader.read();
          // End of stream reached
          if (c == -1)
            break;
          lookaheadChars[i] = (char)c;
        }
        String lookahead = new String(lookaheadChars);
        if (lookahead.indexOf(":init:") >= 0)
          instance.isSpin = true;
        else
          instance.isUPPAAL = true;
        instance.inputStream.reset();
      }
      catch (IOException ioe) {
        util.Message.debug().info("Exception: " + ioe);
      }
    }
    catch (java.io.IOException ioe) {
      Message.error("Cannot read file `" + instance.fileName + "'");
      System.exit(-1);
    }

    return instance;
  }

  @NonNull String getFileName() {
    return this.fileName;
  }

  BufferedInputStream getInputStream() {
    return this.inputStream;
  }

  boolean isSpin() {
    return this.isSpin;
  }

  boolean isUPPAAL() {
    return this.isUPPAAL;
  }
}
