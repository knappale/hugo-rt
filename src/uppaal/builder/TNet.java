package uppaal.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uppaal.TAnnotation;
import uppaal.TState;
import util.Formatter;

/**
 * Net of UPPAAL annotations
 *
 * An annotation net represents alternative paths of annotations
 * that are thought to connect a single entry point with a single exit
 * point.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class TNet {
  public static interface Visitor<T> {
    public T onTransition(List<TAnnotation> annotations);
    public T onTransition(List<TAnnotation> annotations, TState target);
    public T onSequence(List<TNet> nets);
    public T onChoice(List<TNet> nets);
    public T onSubTemplate(TSubTemplate subTemplate);
  }

  public abstract <T> T accept(Visitor<T> visitor);

  public static class Transition extends TNet {
    private @Nullable TState target;
    private List<TAnnotation> annotations = new ArrayList<>();

    /**
     * Create a transition annotation net from a sequence of annotations.
     *
     * @param annotations some annotations
     */
    private Transition(@NonNull TAnnotation ... annotations) {
      this.annotations.addAll(Arrays.asList(annotations));
    }

    /**
     * Create a transition annotation net from a sequence of annotations.
     *
     * @param annotations some annotations
     */
    private Transition(List<TAnnotation> annotations) {
      this.annotations.addAll(annotations);
    }

    /**
     * Create a transition annotation net to a given state from a sequence
     * of annotations.
     * 
     * @param target goto state 
     * @param annotations some annotations
     */
    private Transition(TState target, @NonNull TAnnotation ... annotations) {
      this(annotations);
      this.target = target;
    }

    /**
     * Add an annotation to this transition annotation net.
     */
    public Transition add(TAnnotation annotation) {
      this.annotations.add(annotation);
      return this;
    }

    /**
     * Add a list of annotations to this transition annotation net.
     */
    public Transition add(List<TAnnotation> annotations) {
      this.annotations.addAll(annotations);
      return this;
    }

    @Override
    public <T> T accept(TNet.Visitor<T> visitor) {
      TState target = this.target;
      if (target == null)
        return visitor.onTransition(this.annotations);
      else
        return visitor.onTransition(this.annotations, target);
    }

    @Override
    public String toString() {
      var resultBuilder = new StringBuilder();
      resultBuilder.append("Transition [sequence = ");
      resultBuilder.append(Formatter.list(this.annotations));
      resultBuilder.append(", target = ");
      var target = this.target;
      if (target != null)
        resultBuilder.append(target.getName());
      else
        resultBuilder.append("null");
      resultBuilder.append("]");
      return resultBuilder.toString();
    }
  }

  public static Transition trans(@NonNull TAnnotation ... annotations) {
    return new Transition(annotations);
  }

  public static Transition trans(TState target, @NonNull TAnnotation ... annotations) {
    return new Transition(target, annotations);
  }

  public static class Sequence extends TNet {
    private List<TNet> nets = new ArrayList<>();

    /**
     * Create an annotation net sequence from a sequence of annotation nets.
     *
     * An empty annotation net sequence represents the fact that there is no
     * path of annotation nets, i.e., {@code skip}.
     */
    private Sequence(@NonNull TNet ... nets) {
      this.nets.addAll(Arrays.asList(nets));
    }

    /**
     * Add an annotation to this annotation net sequence.
     *
     * @param annotation an annotation
     * @return the extended annotation net sequence
     */
    public Sequence add(TAnnotation annotation) {
      this.nets.add(new Transition(annotation));
      return this;
    }

    /**
     * Add a list of annotations to this annotation net sequence.
     *
     * @param annotation an annotation
     * @return the extended annotation net sequence
     */
    public Sequence add(List<TAnnotation> annotations) {
      this.nets.add(new Transition(annotations));
      return this;
    }

    /**
     * Add an annotation net to this annotation net sequence.
     *
     * @param annotationNet an annotation net
     * @return the extended annotation net sequence
     */
    public Sequence add(TNet annotationNet) {
      this.nets.add(annotationNet);
      return this;
    }

    @Override
    public <T> T accept(TNet.Visitor<T> visitor) {
      return visitor.onSequence(nets);
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("Sequence [sequence = ");
      resultBuilder.append(Formatter.list(this.nets));
      resultBuilder.append("]");
      return resultBuilder.toString();
    }
  }

  public static Sequence seq(@NonNull TNet ... nets) {
    return new Sequence(nets);
  }

  public static class Choice extends TNet {
    private List<TNet> nets = new ArrayList<>();

    /**
     * Create an annotation net choice from a list of alternatives.
     *
     * An empty annotation net sequence represents the fact that there is no
     * choice of annotation nets, i.e., {@code skip}.
     */
    public Choice(@NonNull TNet ... nets) {
      this.nets.addAll(Arrays.asList(nets));
    }

    /**
     * Add an alternative annotation path for a single annotation to
     * annotation matrix.
     *
     * An empty annotation net sequence represents the fact that there is no
     * choice of annotation nets, i.e., {@code skip}.
     *
     * @param annotationNet an annotation net
     */
    public Choice add(TNet annotationNet) {
      this.nets.add(annotationNet);
      return this;
    }

    @Override
    public <T> T accept(TNet.Visitor<T> visitor) {
      return visitor.onChoice(this.nets);
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("Choice [nets = ");
      resultBuilder.append(Formatter.list(this.nets));
      resultBuilder.append("]");
      return resultBuilder.toString();
    }
  }

  public static Choice choice(@NonNull TNet ... nets) {
    return new Choice(nets);
  }

  public static class SubTemplate extends TNet {
    private TSubTemplate subTemplate;
  
    /**
     * Create an annotation net from a sub-process.
     * 
     * @param subProcess a (UPPAAL) sub-process
     */
    public SubTemplate(TSubTemplate subTemplate) {
      this.subTemplate = subTemplate;
    }
 
    @Override
    public <T> T accept(TNet.Visitor<T> visitor) {
      return visitor.onSubTemplate(this.subTemplate);
    }
  }

  public static SubTemplate sub(TSubTemplate subTemplate) {
    return new SubTemplate(subTemplate);
  }
}
