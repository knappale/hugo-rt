package uppaal.builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uppaal.TAnnotation;
import uppaal.TClock;
import uppaal.TConstant;
import uppaal.TGuard;
import uppaal.TInvariant;
import uppaal.TState;
import uppaal.TSynchronisation;
import uppaal.TTemplate;
import uppaal.TTransition;
import uppaal.TVariable;
import uppaal.TUpdate;

import static util.Objects.requireNonNull;


/**
 * Contains data of an UPPAAL sub-template.
 *
 * A sub-template shows an entry state and an optional exit state for
 * sequential concatenation.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TSubTemplate {
  private List<TConstant> constants = new ArrayList<>();
  private List<TVariable> variables = new ArrayList<>();
  private List<TClock> clocks = new ArrayList<>();
  private List<TState> states = new ArrayList<>();
  private List<TTransition> transitions = new ArrayList<>();
  /** @invariant entry \in states */
  private TState entry;
  private @Nullable TState exit;

  // Caching
  private Map<TState, Set<TTransition>> incomings = new HashMap<>();
  private Map<TState, Set<TTransition>> outgoings = new HashMap<>();

  public static interface StateProducer extends TTemplate.StateProducer {
    public TState getEntryState(boolean isCommitted, boolean isUrgent);
    public TState getExitState();
    public Collection<TVariable> getCommunicationVariables();
  }

  /**
   * Create an UPPAAL sub-process.
   *
   * An empty {@code exit} state means that this sub-process has no exit at all.
   * In particular, sequentially composing such a sub-process with another
   * sub-process breaks the sequential control flow.
   *
   * @param entry sub-process' entry state
   * @param exit sub-process' exit state
   */
  public TSubTemplate(TState entry, @Nullable TState exit) {
    this.addState(entry);
    this.entry = entry;
    if (exit != null) {
      this.addState(exit);
      this.exit = exit;
    }
  }

  /**
   * Add a constant to sub-process.
   *
   * @param tConstant UPPAAL constant
   */
  public void addConstant(TConstant tConstant) {
    if (!this.constants.contains(tConstant))
      this.constants.add(tConstant);
  }

  /**
   * Add a collection of constants to sub-process.
   *
   * @param tConstants collection of UPPAAL constants
   */
  public void addConstants(Collection<TConstant> tConstants) {
    for (TConstant tConstant : tConstants) {
      this.addConstant(tConstant);
    }
  }

  /**
   * @return sub-process's constants
   */
  public Collection<TConstant> getConstants() {
    return this.constants;
  }

  /**
   * Add a variable to sub-process.
   *
   * @param tVariable UPPAAL variable
   */
  public void addVariable(TVariable tVariable) {
    if (!this.variables.contains(tVariable))
      this.variables.add(tVariable);
  }

  /**
   * Add a collection of variables to sub-process.
   *
   * @param tVariables collection of UPPAAL variables
   */
  public void addVariables(Collection<TVariable> tVariables) {
    for (TVariable tVariable : tVariables) {
      this.addVariable(tVariable);
    }
  }

  /**
   * @return sub-process's variables
   */
  public Collection<TVariable> getVariables() {
    return this.variables;
  }

  /**
   * Add a clock to sub-process.
   *
   * @param tClock UPPAAL clock
   */
  public void addClock(TClock tClock) {
    if (!this.clocks.contains(tClock))
      this.clocks.add(tClock);
  }

  /**
   * Add a collection of clocks to sub-process.
   *
   * @param tClocks collection of UPPAAL clocks
   */
  public void addClocks(Collection<TClock> tClocks) {
    for (TClock tClock : tClocks) {
      this.addClock(tClock);
    }
  }

  /**
   * @return sub-process's clocks
   */
  public Collection<TClock> getClocks() {
    return this.clocks;
  }

  /**
   * Add a state from state data to sub-process.
   *
   * @param name state's name
   * @return added state
   */
  public TState addState(String name) {
    TState tState = new TState(name);
    return this.addState(tState);
  }

  /**
   * Add a state from state data to sub-process.
   *
   * @param name state's name
   * @param tInvariants a list of state's invariants
   * @return added state
   */
  public TState addState(String name, List<TInvariant> tInvariants) {
    TState tState = new TState(name);
    tState.addInvariants(tInvariants);
    return this.addState(tState);
  }

  /**
   * Add a state to sub-process.
   *
   * @param tState UPPAAL state
   * @return added state
   */
  private TState addState(TState tState) {
    if (!this.states.contains(tState)) {
      this.states.add(tState);
      this.incomings.put(tState, new HashSet<>());
      this.outgoings.put(tState, new HashSet<>());
    }
    return tState;
  }

  /**
   * Add a list of states to sub-process.
   *
   * @param tStates collection of UPPAAL states
   */
  private void addStates(List<TState> tStates) {
    for (TState tState : tStates) {
      this.addState(tState);
    }
  }

  /**
   * Remove a state from sub-process's states.
   *
   * @param tState UPPAAL state
   */
  private void removeState(TState tState) {
    this.removeTransitions(this.getIncomings(tState));
    this.incomings.remove(tState);
    this.removeTransitions(this.getOutgoings(tState));
    this.outgoings.remove(tState);
    this.states.remove(tState);
  }

  /**
   * Change a state of this sub-process.
   *
   * @param oldState old state
   * @param newState new state
   * @return sub-process old state replaced by new state
   * @precondition this.states.contains(oldState)
   */
  private void changeState(TState oldState, TState newState) {
    if (oldState.equals(newState))
      return;
    addState(newState);
    List<TTransition> removeTransitions = new ArrayList<>();
    List<TTransition> addTransitions = new ArrayList<>();
    for (TTransition transition : this.getTransitions()) {
      if (transition.getSource().equals(oldState) ||
          transition.getTarget().equals(oldState)) {
        TTransition newTransition = exchange(transition, oldState, newState, oldState, newState);
        removeTransitions.add(transition);
        addTransitions.add(newTransition);
      }
    }
    this.removeTransitions(removeTransitions);
    this.addTransitions(addTransitions);
    removeState(oldState);
    if (oldState.equals(this.entry))
      this.entry = newState;
    if (oldState.equals(this.exit))
      this.exit = newState;
  }

  /**
   * @return sub-process's states
   */
  public List<TState> getStates() {
    return this.states;
  }

  /**
   * Add a transition from transition data to sub-process.
   *
   * @param source transition's source state
   * @param target transition's target state
   * @param guard transitions's guard, or {@code null}
   * @param sync transition's synchronisation, or {@code null}
   * @param updates list of transition's updates
   * @return added transition
   */
  public TTransition addTransition(TState source, TState target, @Nullable TGuard guard, @Nullable TSynchronisation sync, List<TUpdate> updates) {
    TTransition transition = new TTransition(source, target, guard, sync, (updates == null ? new ArrayList<>() : updates));
    return this.addTransition(transition);
  }

  /**
   * Add a transition from transition data to sub-process.
   *
   * @param source transition's source state
   * @param target transition's target state
   * @param guards transitions's collection of guards
   * @param sync transition's synchronisation, or {@code null}
   * @param updates list of transition's updates
   * @return added transition
   */
  public TTransition addTransition(TState source, TState target, List<TGuard> guards, @Nullable TSynchronisation sync, List<TUpdate> updates) {
    TTransition transition = new TTransition(source, target, guards, sync, (updates == null ? new ArrayList<>() : updates));
    return this.addTransition(transition);
  }

  /**
   * Add a transition to sub-process.
   *
   * @param transition UPPAAL transition
   * @return added transition
   */
  public TTransition addTransition(TTransition transition) {
    if (this.transitions.contains(transition))
      return transition;

    TState source = transition.getSource();
    this.addState(source);
    TState target = transition.getTarget();
    this.addState(target);

    this.transitions.add(transition);

    Set<TTransition> sourceOutgoings = new HashSet<>(this.outgoings.get(source));
    sourceOutgoings.add(transition);
    this.outgoings.put(source, sourceOutgoings);
    Set<TTransition> targetIncomings = new HashSet<>(this.incomings.get(target));
    targetIncomings.add(transition);
    this.incomings.put(target, targetIncomings);

    return transition;
  }

  /**
   * Add a collection of transitions to sub-process
   *
   * @param transitions collection of UPPAAL transitions
   */
  private void addTransitions(Collection<TTransition> transitions) {
    for (TTransition transition : transitions) {
      this.addTransition(transition);
    }
  }

  /**
   * Remove a transition from sub-process's transitions
   *
   * @param transition UPPAAL transition
   */
  private void removeTransition(TTransition transition) {
    TState source = transition.getSource();
    Set<TTransition> sourceOutgoings = new HashSet<>(this.outgoings.get(source));
    sourceOutgoings.remove(transition);
    this.outgoings.put(source, sourceOutgoings);
    TState target = transition.getTarget();
    Set<TTransition> targetIncomings = new HashSet<>(this.incomings.get(target));
    targetIncomings.remove(transition);
    this.incomings.put(target, targetIncomings);

    this.transitions.remove(transition);
  }

  /**
   * Remove a collection of transitions from sub-process's transitions
   *
   * @param transitions collection of UPPAAL transition
   */
  private void removeTransitions(Collection<TTransition> transitions) {
    for (TTransition transition : transitions) {
      this.removeTransition(transition);
    }
  }

  /**
   * @return sub-process's transitions
   */
  public Collection<TTransition> getTransitions() {
    return this.transitions;
  }

  /**
   * Add a sub-process to this sub-process
   *
   * @param subProcess another sub-process
   */
  public void addSubProcess(TSubTemplate subProcess) {
    this.constants.addAll(subProcess.getConstants());
    this.variables.addAll(subProcess.getVariables());
    this.clocks.addAll(subProcess.getClocks());
    this.addStates(subProcess.getStates());
    this.addTransitions(subProcess.getTransitions());
  }

  /**
   * Change the entry state of this sub-process.
   *
   * @param newEntry new entry state
   * @return sub-process with entry set to newEntry
   * @pre newEntry != null
   */
  private void changeEntry(TState newEntry) {
    this.changeState(this.getEntry(), newEntry);
  }

  /**
   * @return sub-process's entry state
   */
  public TState getEntry() {
    return this.entry;
  }

  /**
   * Change the exit state of this sub-process.
   *
   * @param newExit new exit state
   * @return sub-process with exit set to newExit, if there has been
   * an exit state
   * @pre this.getExit() == null || newExit != null
   */
  private void changeExit(TState newExit) {
    TState exit = this.exit;
    if (exit == null)
      return;
    this.changeState(exit, newExit);
  }

  /**
   * @return sub-process's exit state, or {@code null} if there is no exit
   */
  public @Nullable TState getExit() {
    return this.exit;
  }

  /**
   * Determine set of incoming transitions of a state.
   *
   * @param state a state of the sub-process
   * @return set of incoming transitions of state
   */
  private Set<TTransition> getIncomings(TState state) {
    Set<TTransition> incomings = this.incomings.get(state);
    if (incomings != null)
      return incomings;
    return new HashSet<>();
  }

  /**
   * Determine set of outgoing transitions of a state.
   *
   * @param state a state of the sub-process
   * @return set of outgoing transitions of state
   */
  private Set<TTransition> getOutgoings(TState state) {
    Set<TTransition> outgoings = this.outgoings.get(state);
    if (outgoings != null)
      return outgoings;
    return new HashSet<>();
  }

  /**
   * Optimise a sub-process on a single state.
   *
   * <UL>
   * <LI>Committed states c occurring in the form
   *   c1 -> c -> x
   * may be superfluous.
   * <LI>Non-initial, non-entry states without incoming transitions are
   * superfluous.
   * <LI>Only assignments to variables in {@code comms} are taken to communicate
   * between processes, such that updates not affecting {@code comms} can be
   * merged.
   * </UL>
   *
   * TODO (AK160409) needs more comment; perhaps some further optimisation can be done
   */
  private boolean optimiseState(TState state, Collection<TVariable> comms) {
    // Do not change the entry and an initial location
    if (state.isInitial() || getEntry().equals(state))
      return false;

    Set<TTransition> incomings = getIncomings(state);
    Set<TTransition> outgoings = getOutgoings(state);

    // Eliminate unreachable states
    if (incomings.size() == 0) {
      removeTransitions(outgoings);
      removeState(state);
      return true;
    }

    // Otherwise, only eliminate chained committed locations ...
    if (!state.isCommitted())
      return false;

    // ... that show a single incoming and a single outgoing transition ...
    if (!(incomings.size() == 1 && outgoings.size() == 1))
      return false;
    TTransition incomingTransition = incomings.iterator().next();
    TTransition outgoingTransition = outgoings.iterator().next();

    // ... where the incoming transition originates either in a committed or in
    // the initial location
    if (!incomingTransition.getSource().isCommitted() && !incomingTransition.getSource().isInitial())
      return false;

    // No optimisation if there is a synchronisation on the incoming transition
    // and the outgoing transition shows some update mentioning a variable in {@code comms}
    // as the incoming transition may transfer data
    if (incomingTransition.getSynchronisation() != null &&
        (!outgoingTransition.getGuards().isEmpty() ||
         outgoingTransition.getSynchronisation() != null ||
         outgoingTransition.getUpdates().stream().anyMatch(update -> update.affects(comms))))
      return false;

    // If there are no guards and no synchronisation on the outgoing transition,
    // it only consists of assigns to variables apart from {@code comms} and may
    // therefore be merged with the incoming transition
    if (outgoingTransition.getGuards().isEmpty() && outgoingTransition.getSynchronisation() == null) {
      incomingTransition.addUpdates(outgoingTransition.getUpdates());
      TTransition newTransition = exchange(incomingTransition, incomingTransition.getSource(), incomingTransition.getSource(), incomingTransition.getTarget(), outgoingTransition.getTarget());
      removeTransition(incomingTransition);
      addTransition(newTransition);
      removeTransition(outgoingTransition);
      removeState(state);
      return true;
    }

    // If there are no updates (and no synchronisation) on the incoming
    // transition, the outgoing transition may be merged with the incoming
    // transition, if the outgoing transition has no receiving transition
    // (whence the choice would be committed).
    TSynchronisation outgoingSynchronisation = outgoingTransition.getSynchronisation();
    if (incomingTransition.getUpdates().isEmpty() && !(outgoingSynchronisation != null && outgoingSynchronisation.isReceive())) {
      incomingTransition.addGuards(outgoingTransition.getGuards());
      if (outgoingSynchronisation != null)
        incomingTransition.setSynchronisation(outgoingSynchronisation);
      incomingTransition.addUpdates(outgoingTransition.getUpdates());
      TTransition newTransition = exchange(incomingTransition, incomingTransition.getSource(), incomingTransition.getSource(), incomingTransition.getTarget(), outgoingTransition.getTarget());
      removeTransition(incomingTransition);
      addTransition(newTransition);
      removeTransition(outgoingTransition);
      removeState(state);
      return true;
    }

    return false;
  }

  /**
   * Optimise a sub-process.
   * 
   * Only assignments to variables in {@code comms} are taken to communicate
   * between processes, such that updates not affecting {@code comms} can be
   * merged.
   *
   * Caveat: This is a global optimisation routine.  If it is applied to a
   * sub-process in the mid-way of construction, unpredictable behaviour may
   * result.  In particular, after calling optimise, no guarantees are given
   * with respect to exported state and transition handles.
   */
  public void optimise(Collection<TVariable> comms) {
    boolean changed = true;
    while (changed) {
      changed = false;
      // The optimisation may change the set of states and the sets of incoming
      // and outgoing transitions; thus after each modification the process is
      // started over again.
      for (TState state : this.getStates()) {
        changed = optimiseState(state, comms);
        if (changed)
          break;
      }
    }
  }

  /**
   * Compose two sub-processes sequentially into a new sub-process.
   *
   * The right process is prioritised: It keeps its entry, its exit, and all its
   * states and transitions.  The entry of the composed sub-process is the left
   * entry, if the entry and the exit state of the left sub-process are
   * different; otherwise the entry of the right process.
   *
   * The states of the new sub-process all come from either the left or the
   * right sub-process, but only the transitions from the right sub-process are
   * kept.
   *
   * If the left sub-process has a non-committed entry, so will have the
   * composed sub-process.
   *
   * @param left left sub-process
   * @param right right sub-process
   * @param mergings map mapping old states to new states (result)
   * @return new sub-process containing first the left then the right
   * process
   */
  public static TSubTemplate seq(TSubTemplate left, TSubTemplate right, Map<TState, TState> mergings) {
    TState entry = (left.getEntry().equals(left.getExit()) ? right.getEntry() : left.getEntry());
    TState exit = right.getExit();
    TSubTemplate merged = new TSubTemplate(entry, exit);
    merged.addVariables(left.getVariables());
    merged.addClocks(left.getClocks());
    merged.addConstants(left.getConstants());
    merged.addVariables(right.getVariables());
    merged.addClocks(right.getClocks());
    merged.addConstants(right.getConstants());

    for (TState leftState : left.getStates()) {
      if (!leftState.equals(left.getExit()))
        merged.addState(leftState);
    }
    for (TTransition leftTransition : left.getTransitions()) {
      merged.addTransition(exchange(leftTransition, left.getEntry(), entry, left.getExit(), right.getEntry()));
    }
    for (TState rightState : right.getStates()) {
      merged.addState(rightState);
    }
    for (TTransition rightTransition : right.getTransitions()) {
      merged.addTransition(rightTransition);
    }

    // Commitments
    if (!left.getEntry().isCommitted())
      entry.setCommitted(false);

    // Mergings
    if (!left.getEntry().equals(entry))
      mergings.put(left.getEntry(), entry);
    TState leftExit = left.getExit();
    if (leftExit != null && !leftExit.equals(right.getEntry()))
      mergings.put(leftExit, right.getEntry());
    return merged;
  }

  /**
   * Compose a guard sub-process and a guarded sub-process into a new
   * sub-process.
   *
   * If the guard sub-process has a non-committed entry, so will have the
   * composed branch sub-process.
   *
   * @param guard guard sub-process
   * @param guarded guarded sub-process
   * @param mergings map mapping old states to new states (result)
   * @return new sub-process guarding the guarded sub-process by the
   * guard sub-process
   */
  public static TSubTemplate branch(TSubTemplate guard, TSubTemplate guarded, Map<TState, TState> mergings) {
    return seq(guard, guarded, mergings);
  }

  /**
   * Compose a list of branch sub-processes into a sub-process which chooses
   * some branch.
   *
   * If some branch sub-process has a non-committed entry, so will have the
   * composed choice sub-process.  If some branch sub-process has a
   * non-committed exit, so will have the composed choice sub-process.
   *
   * @param branches a list of branch sub-processes
   * @param mergings map mapping old states to new states (result)
   * @return new sub-process choosing between the branch sub-processes
   */
  public static TSubTemplate choice(List<TSubTemplate> branches, Map<TState, TState> mergings) {
    TSubTemplate merged = null;

    // Determine whether there is a branch that has a proper exit state
    int exitingBranch = -1;
    for (int i = 0; i < branches.size(); i++) {
      TSubTemplate branch = branches.get(i);
      if (branch.getExit() != null) {
        exitingBranch = i;
        break;
      }
    }

    // If there is a branch with an exit state, some such exit state
    // is chosen as the exit state of the composed choice sub-process
    if (exitingBranch >= 0) {
      TState entry = branches.get(exitingBranch).getEntry();
      TState exit = requireNonNull(branches.get(exitingBranch).getExit());
      merged = new TSubTemplate(entry, exit);
      for (int i = 0; i < branches.size(); i++) {
        TSubTemplate branch = branches.get(i);
        if (i == exitingBranch) {
          for (TState branchState : branch.getStates()) {
            merged.addState(branchState);
          }
          for (TTransition branchTransition : branch.getTransitions()) {
            merged.addTransition(branchTransition);
          }
        }
        else {
          for (TState branchState : branch.getStates()) {
            if (!branchState.equals(branch.getEntry()) && !branchState.equals(branch.getExit()))
              merged.addState(branchState);
          }
          for (TTransition branchTransition : branch.getTransitions()) {
            merged.addTransition(exchange(branchTransition, branch.getEntry(), entry, branch.getExit(), exit));
          }

          // Mergings
          if (!branch.getEntry().equals(entry))
            mergings.put(branch.getEntry(), entry);
          TState branchExit = branch.getExit();
          if (branchExit != null && exit != null && !branchExit.equals(exit))
            mergings.put(branchExit, exit);
        }
      }
    }
    else {
      // Since no branch shows an exit state the composed choice
      // sub-process will have no exit state either
      TState entry = branches.get(0).getEntry();
      merged = new TSubTemplate(entry, null);
      for (int i = 0; i < branches.size(); i++) {
        TSubTemplate branch = branches.get(i);
        for (TState branchState : branch.getStates()) {
          if (!branchState.equals(branch.getEntry()))
            merged.addState(branchState);
        }
        for (TTransition branchTransition : branch.getTransitions()) {
          merged.addTransition(exchange(branchTransition, branch.getEntry(), entry, branch.getExit(), branch.getExit()));
        }

        // Mergings
        if (!branch.getEntry().equals(entry))
          mergings.put(branch.getEntry(), entry);
      }
    }

    // Commitments
    for (TSubTemplate branch : branches) {
      if (!branch.getEntry().isCommitted())
        merged.getEntry().setCommitted(false);
      TState branchExit = branch.getExit();
      TState mergedExit = merged.getExit();
      if (mergedExit != null && branchExit != null && !branchExit.isCommitted())
        mergedExit.setCommitted(false);
      merged.addVariables(branch.getVariables());
      merged.addClocks(branch.getClocks());
      merged.addConstants(branch.getConstants());
    }

    return merged;
  }

  /**
   * Compose a list of branch sub-processes into a sub-process which repeatedly
   * chooses some branch.
   *
   * Every branch sub-process may break to an outside exit state.
   *
   * If some branch sub-process has a non-committed entry, so will have the
   * composed loop sub-process.  If some branch sub-process has a non-committed
   * exit, so will have the composed loop sub-process.
   *
   * @param branches a list of branch sub-processes
   * @param exit break exit state
   * @param mergings map mapping old states to new states (result)
   * @return new sub-process choosing between the branch sub-processes
   */
  public static TSubTemplate loop(List<TSubTemplate> branches, TState exit, Map<TState, TState> mergings) {
    Iterator<TSubTemplate> branchesIt = branches.iterator();
    TSubTemplate firstBranch = branchesIt.next();
    TState entry = firstBranch.getEntry();
    TSubTemplate merged = new TSubTemplate(entry, exit);

    for (TState firstBranchState : firstBranch.getStates()) {
      if (!firstBranchState.equals(firstBranch.getExit()))
        merged.addState(firstBranchState);
    }
    for (TTransition firstBranchTransition : firstBranch.getTransitions()) {
      TState firstBranchExit = firstBranch.getExit();
      merged.addTransition(exchange(firstBranchTransition, entry, entry, firstBranchExit, entry));
      if (firstBranchExit != null && !firstBranchExit.equals(entry))
        mergings.put(firstBranchExit, entry);
    }
    while (branchesIt.hasNext()) {
      TSubTemplate nextBranch = branchesIt.next();
      for (TState nextBranchState : nextBranch.getStates()) {
        if (!nextBranchState.equals(nextBranch.getEntry()) && !nextBranchState.equals(nextBranch.getExit()))
          merged.addState(nextBranchState);
      }
      for (TTransition nextBranchTransition : nextBranch.getTransitions()) {
        TState nextBranchExit = nextBranch.getExit();
        merged.addTransition(exchange(nextBranchTransition, nextBranch.getEntry(), entry, nextBranchExit, entry));
        if (!nextBranch.getEntry().equals(entry))
          mergings.put(nextBranch.getEntry(), entry);
        if (nextBranchExit != null && !nextBranchExit.equals(entry))
          mergings.put(nextBranchExit, entry);
      }
    }

    // Commitments
    for (branchesIt = branches.iterator(); branchesIt.hasNext();) {
      TSubTemplate branch = branchesIt.next();
      if (!branch.getEntry().isCommitted())
        entry.setCommitted(false);
      TState mergedExit = merged.getExit();
      TState branchExit = branch.getExit();
      if (mergedExit != null && branchExit != null && !branchExit.isCommitted())
        mergedExit.setCommitted(false);
      merged.addVariables(branch.getVariables());
      merged.addClocks(branch.getClocks());
      merged.addConstants(branch.getConstants());
    }

    return merged;
  }

  /**
   * Compute a sub-process from a given sub-process with a set of
   * states mapped to other states.
   *
   * A map determines which state becomes which new state.  The states
   * in the map that are mapped to some other state will not belong to
   * the resulting sub-process.
   *
   * @param subProcess a sub-process
   * @param mapping a map from state to state
   * @return new sub-process from given sub-process with states identified
   */
  public static TSubTemplate map(TSubTemplate subProcess, Map<TState, TState> mapping) {
    TState entry = mapping.get(subProcess.getEntry());
    if (entry == null)
      entry = subProcess.getEntry();
    TState exit = (subProcess.exit != null ? mapping.get(subProcess.getExit()) : null);
    if (exit == null)
      exit = subProcess.getExit();
    TSubTemplate merged = new TSubTemplate(entry, exit);
    merged.addVariables(subProcess.getVariables());
    merged.addClocks(subProcess.getClocks());
    merged.addConstants(subProcess.getConstants());
    for (TState state : subProcess.getStates()) {
      TState mappedState = mapping.get(state);
      if (mappedState == null)
        merged.addState(state);
      else
        merged.addState(mappedState);
    }
    for (TTransition transition : subProcess.getTransitions()) {
      TState newSource = mapping.get(transition.getSource());
      if (newSource == null)
        newSource = transition.getSource();
      TState newTarget = mapping.get(transition.getTarget());
      if (newTarget == null)
        newTarget = transition.getTarget();
      merged.addTransition(exchange(transition, transition.getSource(), newSource, transition.getTarget(), newTarget));
    }
    return merged;
  }

  /**
   * Compute a new transition from a given transition with source and target
   * state exchanged.
   *
   * @param transition given transition
   * @param oldEntry a state to be exchanged
   * @param newEntry exchange partner for {@code oldEntry}
   * @param oldExit a state to be exchanged
   * @param newExit exchange partner for {@code oldExit}
   * @return new transition from given transition with oldEntry
   * replaced by newEntry, oldExit by newExit
   */
  public static TTransition exchange(TTransition transition, TState oldEntry, TState newEntry, @Nullable TState oldExit, @Nullable TState newExit) {
    TState newSource = transition.getSource();
    if (newSource.equals(oldEntry))
      newSource = newEntry;
    if (newSource.equals(oldExit) && newExit != null)
      newSource = newExit;

    TState newTarget = transition.getTarget();
    if (newTarget.equals(oldEntry))
      newTarget = newEntry;
    if (newTarget.equals(oldExit) && newExit != null)
      newTarget = newExit;

    return new TTransition(newSource, newTarget, transition.getGuards(), transition.getSynchronisation(), transition.getUpdates());
  }

  /**
   * Decoration of a transition by annotations.
   */
  private static final class TransitionDecorator implements TAnnotation.Visitor {
    private TTransition tTransition;

    TransitionDecorator(TTransition tTransition) {
      this.tTransition = tTransition;
    }

    void process(List<TAnnotation> tAnnotations) {
      for (TAnnotation tAnnotation : tAnnotations)
        tAnnotation.accept(this);
    }

    public void onGuard(TGuard guard) {
      this.tTransition.addGuard(guard);
    }

    public void onSynchronisation(TSynchronisation synchronisation) {
      this.tTransition.setSynchronisation(synchronisation);
    }

    public void onUpdate(TUpdate update) {
      this.tTransition.addUpdate(update);
    }
  }

  /**
   * Translation of an annotation net.
   *
   * Annotation nets are either atomic sequences, sequences, or
   * choices.  The latter two consist of annotation nets, whereas an
   * atomic sequence consists of annotations.  With the help of the
   * atomic sequence translator annotation nets are translated into
   * sub-processes.
   */
  private static final class AnnotationNetTranslator implements TNet.Visitor<TSubTemplate> {
    private TTemplate.StateProducer stateProducer;

    AnnotationNetTranslator(TTemplate.StateProducer stateProducer) {
      this.stateProducer = stateProducer;
    }

    TSubTemplate process(TNet annotationNet) {
      return annotationNet.accept(this);
    }

    /**
     * Translation of an annotation net atomic sequence.
     *
     * An annotation net atomic sequence consists of annotations, i.e.,
     * guards, synchronisations, and assigns.  All annotations of such a
     * sequence are turned into annotations of a single transition.  In
     * particular, if there are several synchronisations only the last
     * synchronisation of the sequence will appear as a label on the
     * transition.
     *
     * Note that the outcoming transition will only have a non-committed
     * source state if the last synchronisation of the atomic sequence
     * is receiving.
     */
    private TSubTemplate atomicSequence(List<TAnnotation> annotations, @Nullable TState gotoState) {
      TState entry = this.stateProducer.getNewState();
      TState exit = (gotoState == null ? this.stateProducer.getNewState() : null);
      TState target = (gotoState != null ? gotoState : requireNonNull(exit));
      TSubTemplate subProcess = new TSubTemplate(entry, exit);
      if (gotoState != null)
        subProcess.addState(gotoState);
      TTransition transition = new TTransition(entry, target);
      subProcess.addTransition(transition);
      new TransitionDecorator(transition).process(annotations);
      TSynchronisation transitionSynchronisation = transition.getSynchronisation();
      if (transitionSynchronisation == null || (transitionSynchronisation != null && transitionSynchronisation.isSend()))
        transition.getSource().setCommitted(true);
      return subProcess;
    }

    public TSubTemplate onTransition(List<TAnnotation> annotations) {
      return atomicSequence(annotations, null);
    }

    public TSubTemplate onTransition(List<TAnnotation> annotations, TState gotoState) {
      return atomicSequence(annotations, gotoState);
    }

    public TSubTemplate onSequence(List<TNet> annotationNets) {
      if (annotationNets == null || annotationNets.size() == 0) {
        //-AK The empty annotationNets list formerly introduced a break in the
        // control flow, i.e., has been as below, without introducing a
        // transition between entry and exit state.  Is this necessary?
        // State entry = stateProducer.getNewState();
        // State exit = stateProducer.getNewState();
        // SubProcess subProcess = new SubProcess(entry, exit);
        // result(subProcess);
        return atomicSequence(new ArrayList<>(), null);
      }

      Iterator<TNet> annotationNetsIt = annotationNets.iterator();
      TNet annotationNet = annotationNetsIt.next();
      TSubTemplate tSubTemplate = process(annotationNet);
      while (annotationNetsIt.hasNext()) {
        annotationNet = annotationNetsIt.next();
        TSubTemplate leftSubProcess = tSubTemplate;
        TSubTemplate rightSubProcess = process(annotationNet);

        // State intermediateState = stateProducer.getNewState();
        // if (rightSubProcess.getEntry().isCommitted())
        //  intermediateState.setCommitted(true);
        TState intermediateState = rightSubProcess.getEntry();
        leftSubProcess.changeExit(intermediateState);
        // rightSubProcess.changeEntry(intermediateState);
        tSubTemplate = TSubTemplate.seq(leftSubProcess, rightSubProcess, new HashMap<>());
      }
      return tSubTemplate;
    }

    public TSubTemplate onChoice(List<TNet> annotationNets) {
      if (annotationNets == null || annotationNets.size() == 0)
        return atomicSequence(new ArrayList<>(), null);
      
      List<TSubTemplate> subProcesses = new ArrayList<>();
      for (TNet annotationNet : annotationNets) {
        subProcesses.add(process(annotationNet));
      }
      return TSubTemplate.choice(subProcesses, new HashMap<>());
    }
    
    public TSubTemplate onSubTemplate(TSubTemplate tSubTemplate) {
      return tSubTemplate;
    }
  }

  /**
   * Create a sub-process from an annotation net.
   *
   * @param annotationNet to be translated into a sub-process
   * @param stateProducer for intermediate states
   * @return sub-process for annotation net
   */
  public static TSubTemplate annotationNet(TNet annotationNet, TSubTemplate.StateProducer stateProducer) {
    var subTemplate = (new AnnotationNetTranslator(stateProducer)).process(annotationNet);
    var entry = subTemplate.getEntry();
    subTemplate.changeEntry(stateProducer.getEntryState(entry.isCommitted(), entry.isUrgent()));
    var exit = subTemplate.exit;
    if (exit != null)
      subTemplate.changeExit(stateProducer.getExitState());
    subTemplate.optimise(stateProducer.getCommunicationVariables());
    return subTemplate;
  }

  @Override
  public String toString() {
    TTemplate template = new TTemplate("");
    template.addSubProcess(this);
    return template.declaration();
  }
}
