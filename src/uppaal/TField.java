package uppaal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * UPPAAL field
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class TField implements TDeclaration {
  private TDataType type;
  private String name = "";
    
  public TField(TDataType type, String name) {
    this.type = type;
    this.name = name;
  }
  
  /**
   * @return field's name
   */
  public String getName(){
    return this.name;
  }

  /**
   * @return field's type
   */
  public TDataType getType(){
    return this.type;
  }

  /**
   * UPPAAL field (i.e., variable or constant) access.
   *
   * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
   */
  public static abstract class Access extends TExpression {
    private TField field;
    private List<Accessor> accessors = new ArrayList<>();

    protected Access(TField field) {
      super(TExpression.Kind.ACCESS);
      this.field = field;
    }

    protected Access(Access access, Accessor accessor) {
      super(TExpression.Kind.ACCESS);
      this.field = access.field;
      this.accessors.addAll(access.accessors);
      this.accessors.add(accessor);
    }

    public abstract Access access(Accessor accessor, @NonNull Accessor ... remainingAccessors);

    @Override
    public Set<TDeclaration> getDependencies() {
      var dependencies = this.field.getDependencies();
      for (var accessor : this.accessors)
        dependencies = accessor.getDependencies(dependencies);
      return dependencies;
    }

    @Override
    public String expression() {
      var expression = this.field.getName();
      for (var accessor : this.accessors)
        expression = accessor.expression(expression);
      return expression;
    }

    /**
     * @return the type of the expression accessed
     */
    public TDataType getType() {
      var type = this.field.getType();
      for (var accessor : this.accessors)
        type = accessor.getType(type);
      return type;
    }

    /**
     * @return the value expression accessed
     */
    private TExpression getValue() {
      var value = this.field.getInitialValue();
      for (var accessor : this.accessors)
        value = accessor.getValue(value);
      return value;
    }

    /**
     * @return field access' underlying field
     */
    public TField getField() {
      return this.field;
    }

    @Override
    public int getIntValue() {
      return this.getValue().getIntValue();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.field, this.accessors);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Access other = (Access)object;
        return Objects.equals(this.field, other.field) &&
               Objects.equals(this.accessors, other.accessors);
      }
      catch (ClassCastException cce) {
      }
      return false;
    }

    @Override
    public String toString() {
      return this.expression();
    }
  }

  /**
   * Create a field access to this field.
   *
   * @return a field access
   */
  public abstract Access access();

  public interface Accessor {
    /**
     * Determine the resulting type when applying this accessor to {@code inner}.
     *
     * @param inner a type representing what is accessed
     * @return type after applying this accessor to {@code inner}
     */
    public abstract TDataType getType(TDataType inner);

    /**
     * Determine the resulting value expression when applying this accessor to {@code inner}.
     *
     * @param inner a value expression representing what is accessed
     * @return value expression after applying this accessor to {@code inner}
     */
    public abstract TExpression getValue(TExpression inner);

    /**
     * @return the declarations this accessor depends on
     */
    public abstract Set<TDeclaration> getDependencies(Set<TDeclaration> inner);

    /**
     * Determine the decorated {@code inner} string according to this accessor.
     *
     * @param inner a string representing what is accessed
     * @return decorated {@code inner}
     */
    public abstract String expression(String inner);
  }

  public interface ArrayAccessor extends Accessor {
    /**
     * @return the integer value of this array accessor
     */
    public abstract int getIntValue();

    /**
     * @return the declarations this array accessor depends on
     */
    public abstract Set<TDeclaration> getDependencies();

    /**
     * @return the UPPAAL string representation of this array accessor
     */
    public abstract String expression();

    @Override
    public default TDataType getType(TDataType inner) {
      return inner.getType(this.getIntValue());
    }

    @Override
    public default TExpression getValue(TExpression inner) {
      return inner.getValue(this.getIntValue());
    }

    @Override
    public default Set<TDeclaration> getDependencies(Set<TDeclaration> inner) {
      var dependencies = new HashSet<>(inner);
      dependencies.addAll(this.getDependencies());
      return dependencies;
    }

    @Override
    public default String expression(String inner) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(inner);
      resultBuilder.append("[");
      resultBuilder.append(this.expression());
      resultBuilder.append("]");
      return resultBuilder.toString();
    }
  }

  public static class StructAccessor implements Accessor {
    private String fieldName;

    StructAccessor(String fieldName) {
      this.fieldName = fieldName;
    }

    @Override
    public TDataType getType(TDataType inner) {
      return inner.getType(fieldName);
    }

    @Override
    public TExpression getValue(TExpression inner) {
      return inner.getValue(fieldName);
    }

    @Override
    public Set<TDeclaration> getDependencies(Set<TDeclaration> inner) {
      return new HashSet<>(inner);
    }

    @Override
    public String expression(String inner) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(inner);
      resultBuilder.append(".");
      resultBuilder.append(fieldName);
      return resultBuilder.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.fieldName);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;

      try {
        StructAccessor other = (StructAccessor)object;
        return Objects.equals(this.fieldName, other.fieldName);
      }
      catch (ClassCastException cce) {
      }
      return false;
    }
  }

  /**
   * Create access to this field by a sequence of accessors.
   *
   * @param accessor the first accessor
   * @param remainingAccessors the remaining accessors
   * @return access to this field by first {@code accessor}, then {@code remainingAccessors}
   */
  public Access access(Accessor accessor, @NonNull Accessor ... remainingAccessors) {
    return this.access().access(accessor, remainingAccessors);
  }

  /**
   * @return the value expression with which this field has been initialised
   */
  public abstract TExpression getInitialValue();
  
  /**
   * @return set of fields this field (transitively) depends on
   */
  public Set<TDeclaration> getDependencies() {
    return this.type.getDependencies();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.type, this.name);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;

    try {
      TField other = (TField)object;
      return Objects.equals(this.type, other.type) &&
             Objects.equals(this.name, other.name);
    }
    catch (ClassCastException cce) {
    }
    return false;
  }
}
