package uppaal;

import java.util.*;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * UPPAAL block
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TBlock {
  private List<TVariable> declarations = new ArrayList<>();
  private List<TStatement> statements = new ArrayList<>();

  protected TBlock() {
  }

  public static TBlock block(@NonNull TStatement ... statements) {
    TBlock b = new TBlock();
    for (TStatement statement : statements)
      b.statements.add(statement);
    return b;
  }

  public static TBlock block(List<TStatement> statements) {
    TBlock b = new TBlock();
    b.statements.addAll(statements);
    return b;
  }

  public static TBlock block(List<TVariable> declarations, @NonNull TStatement ... statements) {
    TBlock b = new TBlock();
    b.declarations.addAll(declarations);
    for (TStatement statement : statements) {
      b.statements.add(statement);
    }
    return b;
  }

  public static TBlock block(List<TVariable> declarations, List<TStatement> statements) {
    TBlock b = new TBlock();
    b.declarations.addAll(declarations);
    b.statements.addAll(statements);
    return b;
  }

  public String statement(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append("{\n");

    for (TVariable declaration : this.declarations) {
      resultBuilder.append(nextPrefix);
      resultBuilder.append(declaration.declaration());
      resultBuilder.append("\n");
    }
    if (!this.declarations.isEmpty())
      resultBuilder.append("\n");

    for (TStatement statement : this.statements) {
      resultBuilder.append(nextPrefix);
      resultBuilder.append(statement.statement(nextPrefix));
      resultBuilder.append("\n");
    }

    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
