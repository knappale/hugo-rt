package uppaal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class TOpaque extends TBlock {
  private List<String> opaques;

  public TOpaque(@Nullable List<String> opaquesOrNull) {
    List<String> opaques = new ArrayList<>();
    if (opaquesOrNull != null)
      opaques.addAll(opaquesOrNull);
    while (opaques.size() > 0 && "".equals(opaques.get(0).trim()))
      opaques = opaques.subList(1, opaques.size());
    while (opaques.size() > 0 && "".equals(opaques.get(opaques.size()-1).trim()))
      opaques = opaques.subList(0, opaques.size()-1);
    this.opaques = opaques;
  }

  public TOpaque(String opaque) {
    this(Arrays.asList(opaque.split("\n")));
  }

  @Override
  public String statement(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append("{\n");
    for (var opaque : this.opaques) {
      resultBuilder.append(nextPrefix);
      resultBuilder.append(opaque);
      resultBuilder.append("\n");
    }

    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
