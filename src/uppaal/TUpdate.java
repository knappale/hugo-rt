package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;

import java.util.Collection;


/**
 * UPPAAL update
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public interface TUpdate extends TAnnotation, TStatement {
  public boolean affects(Collection<TVariable> variables);
}
