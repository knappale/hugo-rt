package uppaal.run.parser;

import java.io.BufferedInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import uppaal.TSystem;
import uppaal.run.TSystemState;


/**
 * Reads data from a UPPAAL trail-file.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class TrailReader {
  private List<TSystemState> trail;

  public TrailReader(BufferedInputStream trailInputStream, TSystem system) throws TrailException {
    try {
      trail = uppaal.run.parser.UppaalParser.trail(trailInputStream, system);
    }
    catch (Exception e) {
      StringWriter stackTraceWriter = new StringWriter();
      e.printStackTrace(new PrintWriter(stackTraceWriter, true));
      throw new TrailException("Trail parsing failed: " + e.getMessage());
    }
    if (trail == null)
      throw new TrailException("No trail found in input");
  }

  public List<TSystemState> getTrail() {
    return trail;
  }
}
