package uppaal.run;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uppaal.TClock;
import uppaal.TProcess;
import uppaal.TState;
import uppaal.TVariable;


/**
 * UPPAAL process state
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TProcessState {
  private TProcess process;
  private @Nullable TState state;
  private TValuation valuation = new TValuation();
  
  public TProcessState(TProcess process, @Nullable TState state) {
    this.process = process;
    this.state = state;
  }
  
  public void addEntry(TValuationEntry entry) {
    this.valuation.addEntry(entry);
  }
  
  public TProcess getProcess() {
    return this.process;
  }

  public @Nullable TState getState() {
    return this.state;
  }

  public int getValue(TVariable.Access variableAccess) {
    return this.valuation.getValue(variableAccess);
  }

  public int getValue(TClock.Access clockAccess) {
    return this.valuation.getValue(clockAccess);
  }

  @Override
  public String toString() {
    var resultBuilder = new StringBuilder();
    resultBuilder.append("ProcessState[process = ");
    resultBuilder.append(this.process.getName());
    var state = this.state;
    if (state != null) {
      resultBuilder.append(", state = ");
      resultBuilder.append(state.getName());
    }
    resultBuilder.append(", valuation = ");
    resultBuilder.append(this.valuation);
    return resultBuilder.toString();
  }
}
