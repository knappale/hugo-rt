package uppaal.run;

import uppaal.TClock;
import uppaal.TExpression;
import uppaal.TVariable;


/**
 * UPPAAL valuation entry
 *
 * @author Alexander Knapp (<A HREF="mailto:knapp@pst.ifi.lmu.de">knapp@pst.ifi.lmu.de</A>)
 */
public class TValuationEntry {
  private TVariable.Access variableAccess = null;
  private TClock.Access clockAccess = null;
  private TExpression value;
  
  private TValuationEntry() {
  }
  
  public static TValuationEntry variable(TVariable.Access variableAccess, TExpression value) {
    TValuationEntry e = new TValuationEntry();
    e.variableAccess = variableAccess;
    e.value = value;
    return e;
  }

  public static TValuationEntry clock(TClock.Access clockAccess, TExpression value) {
    TValuationEntry e = new TValuationEntry();
    e.clockAccess = clockAccess;
    e.value = value;
    return e;
  }
  
  public boolean matches(TVariable.Access otherVariableAccess) {
    var thisVariableAccess = this.variableAccess;
    return thisVariableAccess != null && thisVariableAccess.equals(otherVariableAccess);
  }

  public boolean matches(TClock.Access otherClockAccess) {
    var thisClockAccess = this.clockAccess;
    return thisClockAccess != null && thisClockAccess.equals(otherClockAccess);
  }

  public int getValue() {
    return this.value.getIntValue();
  }

  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    var variableAccess = this.variableAccess;
    if (variableAccess != null)
      resultBuilder.append(this.variableAccess.expression());
    else {
      var clockAccess = this.clockAccess;
      if (clockAccess != null)
        resultBuilder.append(this.clockAccess.expression());
    }
    resultBuilder.append(" = ");
    resultBuilder.append(this.value.expression());
    return resultBuilder.toString();
  }
}
