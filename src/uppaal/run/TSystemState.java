package uppaal.run;

import java.util.ArrayList;
import java.util.List;

import uppaal.TClock;
import uppaal.TProcess;
import uppaal.TVariable;


/**
 * UPPAAL system state
 *
 * @author Alexander Knapp (<A HREF="mailto:knapp@pst.ifi.lmu.de">knapp@pst.ifi.lmu.de</A>)
 */
public class TSystemState {
  private TValuation valuation = new TValuation();
  private List<TProcessState> processStates = new ArrayList<TProcessState>();
  
  public TSystemState() {
  }
  
  public void addProcessState(TProcessState processState) {
    this.processStates.add(processState);
  }

  public TProcessState getProcessState(TProcess process) {
    for (TProcessState processState : this.processStates) {
      if (processState.getProcess().equals(process))
        return processState;
    }
    return null;
  }

  public void addEntry(TValuationEntry entry) {
    this.valuation.addEntry(entry);
  }

  public int getValue(TVariable.Access variableAccess) {
    return valuation.getValue(variableAccess);
  }

  public int getValue(TClock.Access clockAccess) {
    return valuation.getValue(clockAccess);
  }

  public String toString() {
    var resultBuilder = new StringBuilder();
    resultBuilder.append("SystemState [valuation = ");
    resultBuilder.append(this.valuation);
    resultBuilder.append(", processStates = ");
    resultBuilder.append(util.Formatter.separated(this.processStates, ", "));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
