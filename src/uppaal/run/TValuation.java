package uppaal.run;

import uppaal.TVariable;

import java.util.ArrayList;
import java.util.List;

import uppaal.TClock;

import static util.Formatter.quoted;


/**
 * UPPAAL valuation
 *
 * @author Alexander Knapp (<A HREF="mailto:knapp@pst.ifi.lmu.de">knapp@pst.ifi.lmu.de</A>)
 */
public class TValuation {
  private List<TValuationEntry> valuation = new ArrayList<TValuationEntry>();
  
  public TValuation() {
  }
  
  public void addEntry(TValuationEntry entry) {
    this.valuation.add(entry);
  }
  
  public int getValue(TVariable.Access variableAccess) {
    for (TValuationEntry entry : this.valuation) {
      if (entry.matches(variableAccess))
        return entry.getValue();
    }
    util.Message.debug().info("No value found for variable access " + quoted(variableAccess.expression()) + " in valuation " + this + ".");
    return 0;
  }
 
  public int getValue(TClock.Access clockAccess) {
    for (TValuationEntry entry : this.valuation) {
      if (entry.matches(clockAccess))
        return entry.getValue();
    }
    util.Message.debug().info("No value found for clock access " + quoted(clockAccess.expression()) + " in valuation " + this + ".");
    return 0;
  }

  public String toString() {
    return util.Formatter.set(this.valuation);
  }
}
