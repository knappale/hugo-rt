package uppaal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * Abstract container for UPPAAL systems and templates.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 *
 */
@NonNullByDefault
public abstract class TFrame {
  private List<TChannel> channels = new ArrayList<>();
  private List<TClock> clocks = new ArrayList<>();
  private List<TDataType.Def> typeDefs = new ArrayList<>();
  private List<TVariable> variables = new ArrayList<>();
  private List<TConstant> constants = new ArrayList<>();
  private List<TFunction> functions = new ArrayList<>();

  /**
   * Add a (global) clock to this frame.
   *
   * @param clock UPPAAL clock
   */
  public void addClock(TClock clock) {
    this.clocks.add(clock);
  }

  /**
   * Add a channel to this frame.
   *
   * @param channel UPPAAL channel
   */
  public void addChannel(TChannel channel) {
    this.channels.add(channel);
  }

  /**
   * Add a collection of channels to this frame.
   *
   * @param channels collection of UPPAAL channels
   */
  public void addChannels(Collection<TChannel> channels) {
    this.channels.addAll(channels);
  }

  /**
   * Add a collection of (global) clocks to this frame.
   *
   * @param clocks collection of UPPAAL clocks
   */
  public void addClocks(Collection<TClock> clocks) {
    this.clocks.addAll(clocks);
  }

  /**
   * @return frame's clocks
   */
  public List<TClock> getClocks() {
    return this.clocks;
  }

  /**
   * Add a type definition to this frame.
   *
   * @param typeDef a UPPAAL type definition
   */
  public void addTypeDef(TDataType.Def typeDef) {
    this.typeDefs.add(typeDef);
  }

  /**
   * Add a variable to this frame.
   *
   * @param variable UPPAAL variable
   */
  public void addVariable(TVariable variable) {
    this.variables.add(variable);
  }

  /**
   * Add a collection of variables to this frame.
   *
   * @param variables collection of UPPAAL variables
   */
  public void addVariables(Collection<TVariable> variables) {
    this.variables.addAll(variables);
  }

  /**
   * @return system's variables
   */
  public List<TVariable> getVariables() {
    return this.variables;
  }

  /**
   * Add a constant to this frame.
   *
   * @param constant UPPAAL constant
   */
  public void addConstant(TConstant constant) {
    List<TConstant> tConstants = new ArrayList<TConstant>(this.constants);
    tConstants.add(constant);
    this.constants = util.Lists.topologicalSort(tConstants,
                                                 new Comparator<TConstant>() {
                                                   public int compare(TConstant c1, TConstant c2) {
                                                     if (c1.dependsOn(c2))
                                                       return 1;
                                                     if (c2.dependsOn(c1))
                                                       return -1;
                                                     return 0;
                                                   }
                                                 });
  }

  /**
   * Add a collection of  constants to this frame.
   *
   * @param constants collection of UPPAAL constants
   */
  public void addConstants(Collection<TConstant> constants) {
    for (TConstant constant : constants)
      this.addConstant(constant);
  }

  /**
   * Add a function to this frame.
   *
   * @param function an UPPAAL function declaration
   */
  public void addFunction(TFunction function){
    this.functions.add(function);
  }

  /**
   * Add a collection of functions to this frame.
   *
   * @param functions collection of UPPAAL functions
   */
  public void addFunctions(Collection<TFunction> functions) {
      this.functions.addAll(functions);
  }

  /**
   * @return frame's representation
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    boolean pending = false;

    // order matters
    if (!this.channels.isEmpty()) {
      pending = true;
      for (var channel : this.channels) {
        resultBuilder.append(prefix);
        resultBuilder.append(channel.declaration());
        resultBuilder.append("\n");
      }
    }

    if (!this.typeDefs.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      pending = true;
      for (var typeDef : this.typeDefs) {
        resultBuilder.append(prefix);
        resultBuilder.append(typeDef.declaration());
        resultBuilder.append("\n");
      }
    }

    if (!this.constants.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      pending = true;
      for (var constant : this.constants) {
        resultBuilder.append(prefix);
        resultBuilder.append(constant.declaration());
        resultBuilder.append("\n");
      }
    }

    if (!this.variables.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      pending = true;
      for (var variable : this.variables) {
        resultBuilder.append(prefix);
        resultBuilder.append(variable.declaration());
        resultBuilder.append("\n");
      }
    }

    if (!this.clocks.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      pending = true;
      for (var clock : this.clocks) {
        resultBuilder.append(prefix);
        resultBuilder.append(clock.declaration());
        resultBuilder.append("\n");
      }
    }

    if (!this.functions.isEmpty()) {
      for (var function : this.functions) {
        if (pending)
          resultBuilder.append("\n");
        resultBuilder.append(prefix);
        resultBuilder.append(function.declaration(prefix));
        resultBuilder.append("\n");
        pending = true;
      }
    }

    return resultBuilder.toString();
  }
}
