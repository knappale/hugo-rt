package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * UPPAAL loop
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TLoop implements TStatement {
  private TVariable.@Nullable Assign before;
  private TExpression condition;
  private TVariable.@Nullable Assign after;
  private TBlock block;

  private TLoop(TExpression condition, TBlock block) {
    this.condition = condition;
    this.block = block;
  }

  public static TLoop loop(TExpression condition, TBlock block) {
    return new TLoop(condition, block);
  }

  public static TLoop loop(TVariable.Assign before, TExpression condition, TBlock block, TVariable.Assign after) {
    TLoop l = new TLoop(condition, block);
    l.before = before;
    l.after = after;
    return l;
  }

  @Override
  public String statement(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    if (this.before != null || this.after != null) {
      resultBuilder.append("for (");
      if (this.before != null)
        resultBuilder.append(this.before.expression());
      resultBuilder.append("; ");
      resultBuilder.append(this.condition.expression());
      resultBuilder.append("; ");
      if (this.after != null)
        resultBuilder.append(this.after.expression());
    }
    else {
      resultBuilder.append("while (");
      resultBuilder.append(this.condition.expression());
    }
    resultBuilder.append(") ");
    resultBuilder.append(this.block.statement(prefix));

    return resultBuilder.toString();
  }
}
