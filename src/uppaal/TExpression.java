package uppaal;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Pair;

import static util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toList;


/**
 * UPPAAL expression.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TExpression implements TGuard, TArgument, TField.ArrayAccessor {
  static enum Kind {
    VALUE,
    ARRAY,
    STRUCT,
    ACCESS,
    CONDITIONAL,
    UNARY,
    BINARY,
    EXISTS;
  }

  private boolean hasPrecedence(TExpression other) {
    if ((this.kind == Kind.UNARY || this.kind == Kind.BINARY) &&
        (other.kind == Kind.UNARY || other.kind == Kind.BINARY))
      return TOperator.precedes(requireNonNull(this.op), requireNonNull(other.op));
    if (this.kind == Kind.EXISTS)
      return false;
    return true;
  }

  private Kind kind;
  private @Nullable TExpression guard;
  private @Nullable TVariable boundVariable;
  private @Nullable TExpression lexpr;
  private @Nullable TOperator op;
  private @Nullable TExpression rexpr;

  protected TExpression(Kind kind) {
    this.kind = kind;
  }

  public static class BooleanValue extends TExpression {
    private static final BooleanValue TRUE = new BooleanValue(true);
    private static final BooleanValue FALSE = new BooleanValue(false);

    private boolean value;

    BooleanValue(boolean value) {
      super(Kind.VALUE);
      this.value = value;
    }

    @Override
    public TDataType getType() {
      return TDataType.boolType();
    }

    @Override
    public int getIntValue() {
      return this.value ? 1 : 0;
    }

    @Override
    public String expression() {
      return "" + this.value;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.value);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        BooleanValue other = (BooleanValue)object;
        return this.value == other.value;
      }
      catch (ClassCastException cce) {
        return false;
      }
    }
  }

  public static class IntegerValue extends TExpression {
    private int value;

    IntegerValue(int value) {
      super(Kind.VALUE);
      this.value = value;
    }

    @Override
    public TDataType getType() {
      return TDataType.intType();
    }

    @Override
    public int getIntValue() {
      return this.value;
    }

    @Override
    public String expression() {
      return "" + this.value;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.value);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        IntegerValue other = (IntegerValue)object;
        return this.value == other.value;
      }
      catch (ClassCastException cce) {
        return false;
      }
    }
  }

  @SuppressWarnings("unused")
  public TExpression getValue(int offset) {
    assert false : "not applicable";
    return falseConst();
  }

  @SuppressWarnings("unused")
  public TExpression getValue(String fieldName) {
    assert false : "not applicable";
    return falseConst();
  }

  public static class Array extends TExpression {
    private List<TExpression> elements = new ArrayList<>();

    Array(List<TExpression> elements) {
      super(Kind.ARRAY);
      this.elements.addAll(elements);
    }

    @Override
    public TDataType getType() {
      return TDataType.array(this.elements.get(0).getType(), this.elements.size());
    }

    public TExpression getValue(int offset) {
      return this.elements.get(offset);
    }

    @Override
    public String expression() {
      return Formatter.array(this.elements, e -> e.expression());
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.elements);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Array other = (Array)object;
        return Objects.equals(this.elements, other.elements);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }
  }

  public static Array array(List<TExpression> elements) {
    return new Array(elements);
  }

  public static class Struct extends TExpression {
    private List<Pair<String, TExpression>> fields = new ArrayList<>();

    Struct(List<Pair<String, TExpression>> fields) {
      super(Kind.STRUCT);
      this.fields.addAll(fields);
    }

    @Override
    public TDataType getType() {
      return TDataType.struct(this.fields.stream().map(f -> new Pair<String, TDataType>(f.getFirst(), f.getSecond().getType())).collect(toList()));
    }

    @Override
    public TExpression getValue(String fieldName) {
      var maybeValue = this.fields.stream().filter(f -> f.getFirst().equals(fieldName)).map(f -> f.getSecond()).findAny();
      if (maybeValue.isPresent())
        return maybeValue.get();
      assert false : "not applicable";
      return falseConst();
    }

    @Override
    public String expression() {
      return Formatter.array(this.fields, f -> f.getSecond().expression());
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.fields);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Struct other = (Struct)object;
        return Objects.equals(this.fields, other.fields);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }
  }

  public static Struct struct(List<Pair<String, TExpression>> fields) {
    return new Struct(fields);
  }

  /**
   * Create an expression representing a boolean.
   */
  public static TExpression.BooleanValue boolConst(boolean bool) {
    return (bool ? BooleanValue.TRUE : BooleanValue.FALSE);
  }

  /**
   * Create an expression representing true.
   */
  public static TExpression.BooleanValue trueConst() {
    return BooleanValue.TRUE;
  }

  /**
   * Create an expression representing false.
   */
  public static TExpression.BooleanValue falseConst() {
    return BooleanValue.FALSE;
  }

  /**
   * Create an expression representing an integer.
   */
  public static TExpression.IntegerValue intConst(int num) {
    return new IntegerValue(num);
  }

  /**
   * Create an expression representing an existential quantification
   */
  public static TExpression exists(TVariable boundVariable, TExpression expression) {
    TExpression e = new TExpression(Kind.EXISTS);
    e.boundVariable = boundVariable;
    e.lexpr = expression;
    return e;
  }

  /**
   * Create an expression representing a negation.
   */
  public static TExpression neg(TExpression expr) {
    if (expr.equals(trueConst()))
      return falseConst();
    if (expr.equals(falseConst()))
      return trueConst();
    if (expr.kind == Kind.UNARY && expr.op == TOperator.UNEG)
      return requireNonNull(expr.lexpr);
    if (expr.kind == Kind.BINARY) {
      switch (requireNonNull(expr.op)) {
        case LT:
          return TExpression.relational(requireNonNull(expr.lexpr), TOperator.GEQ, requireNonNull(expr.rexpr));
        case LEQ:
          return TExpression.relational(requireNonNull(expr.lexpr), TOperator.GT, requireNonNull(expr.rexpr));
        case EQ:
          return TExpression.relational(requireNonNull(expr.lexpr), TOperator.NEQ, requireNonNull(expr.rexpr));
        case NEQ:
          return TExpression.relational(requireNonNull(expr.lexpr), TOperator.EQ, requireNonNull(expr.rexpr));
        case GEQ:
          return TExpression.relational(requireNonNull(expr.lexpr), TOperator.LT, requireNonNull(expr.rexpr));
        case GT:
          return TExpression.relational(requireNonNull(expr.lexpr), TOperator.LEQ, requireNonNull(expr.rexpr));
        //$CASES-OMITTED$
        default:
      }
    }
    TExpression e = new TExpression(Kind.UNARY);
    e.op = TOperator.UNEG;
    e.lexpr = expr;
    return e;
  }

  /**
   * Create an expression representing a conjunction.
   */
  public static TExpression and(TExpression lexpr, TExpression rexpr) {
    if (lexpr.equals(falseConst()))
      return falseConst();
    if (rexpr.equals(falseConst()))
      return falseConst();
    if (lexpr.equals(trueConst()))
      return rexpr;
    if (rexpr.equals(trueConst()))
      return lexpr;
    TExpression e = new TExpression(Kind.BINARY);
    e.lexpr = lexpr;
    e.op = TOperator.AND;
    e.rexpr = rexpr;
    return e;
  }

  /**
   * Create an expression representing a conjunction.
   */
  public static TExpression and(@NonNull TExpression ... expressions) {
    TExpression conjunction = trueConst();
    for (var expression : expressions)
      conjunction = and(conjunction, expression);
    return conjunction;
  }

  /**
   * Create an expression representing a disjunction.
   */
  public static TExpression or(TExpression lexpr, TExpression rexpr) {
    if (lexpr.equals(trueConst()))
      return trueConst();
    if (rexpr.equals(trueConst()))
      return trueConst();
    if (lexpr.equals(falseConst()))
      return rexpr;
    if (rexpr.equals(falseConst()))
      return lexpr;
    TExpression e = new TExpression(Kind.BINARY);
    e.lexpr = lexpr;
    e.op = TOperator.OR;
    e.rexpr = rexpr;
    return e;
  }

  /**
   * Create an expression representing a conditional.
   */
  public static TExpression conditional(TExpression guard, TExpression lexpr, TExpression rexpr) {
    TExpression e = new TExpression(Kind.CONDITIONAL);
    e.guard = guard;
    e.lexpr = lexpr;
    e.rexpr = rexpr;
    return e;
  }

  /**
   * Create an expression representing a unary minus.
   */
  public static TExpression minus(TExpression expr) {
    if (expr.kind == Kind.UNARY && expr.op == TOperator.UMINUS)
      return requireNonNull(expr.lexpr);

    TExpression e = new TExpression(Kind.UNARY);
    e.op = TOperator.UMINUS;
    e.lexpr = expr;
    return e;
  }

  /**
   * Create an expression representing an arithmetical
   * expression.
   */
  public static TExpression arithmetical(TExpression lexpr, TOperator op, TExpression rexpr) {
    TExpression e = new TExpression(Kind.BINARY);
    e.lexpr = lexpr;
    e.op = op;
    e.rexpr = rexpr;
    return e;
  }

  public static TExpression arithmetical(TExpression lexpr, TOperator op, int rint) {
    return arithmetical(lexpr, op, TExpression.intConst(rint));
  }

  public static TExpression arithmetical(int lint, TOperator op, TExpression rexpr) {
    return arithmetical(TExpression.intConst(lint), op, rexpr);
  }

  /**
   * Create an expression representing a relational expression.
   */
  public static TExpression relational(TExpression lexpr, TOperator op, TExpression rexpr) {
    TExpression e = new TExpression(Kind.BINARY);
    e.lexpr = lexpr;
    e.op = op;
    e.rexpr = rexpr;
    return e;
  }

  /**
   * Create an expression representing a less-than.
   */
  public static TExpression lt(TExpression lexpr, TExpression rexpr) {
    return relational(lexpr, TOperator.LT, rexpr);
  }

  /**
   * Create an expression representing a less-than-or-equal
   */
  public static TExpression leq(TExpression lexpr, TExpression rexpr) {
    return relational(lexpr, TOperator.LEQ, rexpr);
  }

  /**
   * Create an expression representing an equation.
   */
  public static TExpression eq(TExpression lexpr, TExpression rexpr) {
    return relational(lexpr, TOperator.EQ, rexpr);
  }

  /**
   * Create an expression representing an inequation.
   */
  public static TExpression neq(TExpression lexpr, TExpression rexpr) {
    return relational(lexpr, TOperator.NEQ, rexpr);
  }

  /**
   * Create an expression representing a greater-than-or equal.
   */
  public static TExpression geq(TExpression lexpr, TExpression rexpr) {
    return relational(lexpr, TOperator.GEQ, rexpr);
  }

  /**
   * Create an expression representing a greater-than.
   */
  public static TExpression gt(TExpression lexpr, TExpression rexpr) {
    return relational(lexpr, TOperator.GT, rexpr);
  }

  /**
   * @return expression's integer value
   */
  @Override
  public int getIntValue() {
    switch (this.kind) {
      case CONDITIONAL:
        return (requireNonNull(this.guard).getIntValue() != 0 ? requireNonNull(this.lexpr).getIntValue() : requireNonNull(this.rexpr).getIntValue());

      case UNARY:
        return requireNonNull(this.op).getIntValue(requireNonNull(this.lexpr).getIntValue());

      case BINARY:
        return requireNonNull(this.op).getIntValue(requireNonNull(this.lexpr).getIntValue(),
                                                   requireNonNull(this.rexpr).getIntValue());

      //$CASES-OMITTED$
      default:
    }

    assert false : "not applicable";
    return 0;
  }

  /**
   * @return set of declarations this expression depends on
   */
  @Override
  public Set<TDeclaration> getDependencies() {
    Set<TDeclaration> dependencies = new HashSet<>();
    var lexpr = this.lexpr;
    if (lexpr != null)
      dependencies.addAll(lexpr.getDependencies());
    var rexpr = this.rexpr;
    if (rexpr != null)
      dependencies.addAll(rexpr.getDependencies());
    return dependencies;
  }

  /**
   * @return whether this expression takes precedence over Operator.AND
   */
  public boolean hasAndPrecedence() {
    return    (this.kind != Kind.UNARY) && (this.kind != Kind.BINARY)
           || (TOperator.precedes(requireNonNull(this.op), TOperator.AND));
  }

  /**
   * Accept an annotation visitor.
   */
  public void accept(TAnnotation.Visitor visitor) {
    visitor.onGuard(this);
  }

  /**
   * @return expression's data type
   */
  public TDataType getType() {
    switch (this.kind) {
      case CONDITIONAL:
      case UNARY:
      case BINARY:
        return requireNonNull(this.lexpr).getType();

      case EXISTS:
        return TDataType.boolType();

      //$CASES-OMITTED$
      default:
    }

    assert false : "not applicable for " + this;
    return TDataType.intType();
  }

  @Override
  public TDataType getType(TDataType inner) {
    return inner.getType(this.getIntValue());
  }

  /**
   * @return expression representation
   */
  public String expression() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case CONDITIONAL: {
        resultBuilder.append("(");
        resultBuilder.append(requireNonNull(this.guard).expression());
        resultBuilder.append(" ? ");
        resultBuilder.append(requireNonNull(this.lexpr).expression());
        resultBuilder.append(" : ");
        resultBuilder.append(requireNonNull(this.rexpr).expression());
        resultBuilder.append(")");
        return resultBuilder.toString();
      }

      case UNARY: {
        resultBuilder.append(requireNonNull(this.op).getOpName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.lexpr), e -> e.hasPrecedence(this), e -> e.expression()));
        return resultBuilder.toString();
      }

      case BINARY: {
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.lexpr), e -> e.hasPrecedence(this), e -> e.expression()));
        resultBuilder.append(requireNonNull(this.op).getOpName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.rexpr), e -> e.hasPrecedence(this), e -> e.expression()));
        return resultBuilder.toString();
      }

      case EXISTS: {
        TVariable boundVariable = requireNonNull(this.boundVariable);
        resultBuilder.append("(exists (");
        resultBuilder.append(boundVariable.getName());
        resultBuilder.append(" : ");
        resultBuilder.append(boundVariable.getType().getName());
        resultBuilder.append(") ");
        resultBuilder.append(requireNonNull(this.lexpr).expression());
        resultBuilder.append(")");
        return resultBuilder.toString();
      }

      //$CASES-OMITTED$
      default:
    }

    assert false : "not applicable";
    return resultBuilder.toString();
  }

  /**
   * @return expression annotation representation
   */
  public String annotation() {
    return this.expression();
  }

  /**
   * @return expression argument representation
   */
  public String argument() {
    return this.expression();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.guard,
                        this.lexpr,
                        this.rexpr);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      TExpression other = (TExpression)object;
      return this.kind == other.kind &&
             Objects.equals(this.guard, other.guard) &&
             Objects.equals(this.lexpr, other.lexpr) &&
             this.op == other.op &&
             Objects.equals(this.rexpr, other.rexpr);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Expression [");
    resultBuilder.append(this.expression());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
