package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * UPPAAL process argument
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public interface TArgument {
  /**
   * @return argument representation
   */
  public String argument();
}
