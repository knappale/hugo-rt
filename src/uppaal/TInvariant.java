package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * UPPAAL invariant
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public interface TInvariant extends TGuard {
}
