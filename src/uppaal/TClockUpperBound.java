package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import java.util.Objects;

import static util.Objects.requireNonNull;


/**
 * UPPAAL clock condition for upper bounds on clocks
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TClockUpperBound implements TInvariant {
  static enum Kind {
    SIMPLE,
    DIFFERENCE;
  }

  private Kind kind;
  private TClock.Access leftClock;
  private TClock.@Nullable Access rightClock = null;
  private @Nullable TOperator relOp = null;
  private @Nullable TExpression bound = null;

  private TClockUpperBound(Kind kind, TClock.Access tClockAccess) {
    this.kind = kind;
    this.leftClock = tClockAccess;
  }

  /**
   * Create a clock condition for comparing a clock with an upper bound.
   *
   * @param tClockAccess a clock access
   * @param relOp a relational operator
   * @param bound a bound expression
   * @return a clock condition
   */
  private static TClockUpperBound simple(TClock.Access tClockAccess, TOperator relOp, TExpression bound) {
    TClockUpperBound i = new TClockUpperBound(Kind.SIMPLE, tClockAccess);
    i.relOp = relOp;
    i.bound = bound;
    return i;
  }

  /**
   * Create a clock condition for a clock bounded from above (inclusive).
   *
   * @param tClockAccess a clock access
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockUpperBound leq(TClock.Access tClockAccess, TExpression bound) {
    return TClockUpperBound.simple(tClockAccess, TOperator.LEQ, bound);
  }

  /**
   * Create a clock condition for a clock bounded from above (exclusive).
   *
   * @param tClockAccess a clock access
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockUpperBound lt(TClock.Access tClockAccess, TExpression bound) {
    return TClockUpperBound.simple(tClockAccess, TOperator.LT, bound);
  }

  /**
   * Create a clock condition for comparing the difference between two clocks
   * with an upper bound.
   *
   * @param leftClock a clock to be subtracted from
   * @param rightClock a clock to be subtracted
   * @param relOp a relational operator
   * @param bound a bound expression
   * @return a clock condition
   */
  private static TClockUpperBound difference(TClock.Access leftClock, TClock.Access rightClock, TOperator relOp, TExpression bound) {
    TClockUpperBound i = new TClockUpperBound(Kind.DIFFERENCE, leftClock);
    i.rightClock = rightClock;
    i.relOp = relOp;
    i.bound = bound;
    return i;
  }

  /**
   * Create a clock condition for comparing the difference between two clocks w.r.t.
   * less-or-equal to a bound.
   *
   * @param leftClock a clock to be subtracted from
   * @param rightClock a clock to be subtracted
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockUpperBound leq(TClock.Access leftClock, TClock.Access rightClock, TExpression bound) {
    return TClockUpperBound.difference(leftClock, rightClock, TOperator.LEQ, bound);
  }

  /**
   * Create a clock condition for comparing the difference between two clocks w.r.t.
   * less-than to a bound.
   *
   * @param leftClock a clock to be subtracted from
   * @param rightClock a clock to be subtracted
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockUpperBound lt(TClock.Access leftClock, TClock.Access rightClock, TExpression bound) {
    return TClockUpperBound.difference(leftClock, rightClock, TOperator.LT, bound);
  }

  @Override
  public String annotation() {
    StringBuilder resultBuilder = new StringBuilder();
   
    switch (this.kind) {
      case SIMPLE: {
        resultBuilder.append(this.leftClock.expression());
        resultBuilder.append(requireNonNull(this.relOp).getOpName());
        resultBuilder.append(requireNonNull(this.bound).expression());
        break;
      }
    
      case DIFFERENCE: {
        resultBuilder.append(this.leftClock.expression());
        resultBuilder.append(TOperator.SUB.getOpName());
        resultBuilder.append(requireNonNull(this.rightClock).expression());
        resultBuilder.append(requireNonNull(this.relOp).getOpName());
        resultBuilder.append(requireNonNull(this.bound).expression());
        break;
      }
    }

    return resultBuilder.toString();
  }

  @Override
  public boolean hasAndPrecedence() {
    return true;
  }

  @Override
  public void accept(TAnnotation.Visitor visitor) {
    visitor.onGuard(this);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.kind,
                        this.leftClock,
                        this.rightClock,
                        this.relOp,
                        this.bound);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      TClockUpperBound other = (TClockUpperBound)object;
      return this.kind == other.kind &&
             Objects.equals(this.leftClock, other.leftClock) &&
             Objects.equals(this.rightClock, other.rightClock) &&
             this.relOp == other.relOp &&
             Objects.equals(this.bound, other.bound);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("TClockUpperBound [");
    resultBuilder.append(this.annotation());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
