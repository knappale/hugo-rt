package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;

import java.util.ArrayList;
import java.util.List;


/**
 * UPPAAL process
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TProcess {
  private String name;
  private TTemplate tTemplate;
  private List<TArgument> tArguments = new ArrayList<>();
    
  /**
   * Create a process.
   *
   * @param name name of the process
   * @param template underlying template
   */
  public TProcess(String name, TTemplate template) {
    this.name = name;
    this.tTemplate = template;
  }
 
  /**
   * @return process's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return process's template
   */
  public TTemplate getTemplate() {
    return this.tTemplate;
  }

  /**
   * Add an argument to this process.
   *
   * @param tArgument UPPAAL argument
   */
  public void addArgument(TArgument tArgument) {
    this.tArguments.add(tArgument);
  }

  /**
   * Add a collection of arguments to this process.
   *
   * @param tArguments collection of UPPAAL parameters
   */
  public void addArguments(List<TArgument> tArguments) {
    for (TArgument argument : tArguments) {
      addArgument(argument);
    }
  }

  /**
   * @return whether process shows arguments
   */
  public boolean hasArguments() {
    return this.tArguments.size() > 0;
  }

  /**
   * @return process's arguments
   */
  public List<TArgument> getArguments() {
    return this.tArguments;
  }

  /**
   * @return process's representation
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.name);
    resultBuilder.append(" := ");
    resultBuilder.append(this.tTemplate.getName());
    resultBuilder.append(Formatter.tuple(this.tArguments, tArgument -> tArgument.argument()));
    resultBuilder.append(";");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Process [name = ");
    resultBuilder.append(this.name);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
