package uppaal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;

import static java.util.stream.Collectors.toList;

/**
 * UPPAAL function
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TFunction {
  private TDataType returnType;
  private String name;
  private List<TVariable> parameters = new ArrayList<>();
  private TBlock block;

  private TFunction(TDataType returnType, String name, List<TVariable> parameters, TBlock block) {
    this.returnType = returnType;
    this.name = name;
    this.parameters.addAll(parameters);
    this.block = block;
  }

  public static TFunction decl(String name, List<TVariable> parameters, TBlock block) {
    return new TFunction(TDataType.voidType(), name, parameters, block);
  }

  public static TFunction decl(TDataType returnType, String name, List<TVariable> parameters, TBlock block) {
    return new TFunction(returnType, name, parameters, block);
  }

  public String getName() {
    return this.name;
  }

  public static class Call implements TUpdate {
    private TFunction function;
    private List<TExpression> arguments = new ArrayList<>();

    Call(TFunction function) {
      this.function = function;
    }

    Call(TFunction function, List<TExpression> arguments) {
      this.function = function;
      this.arguments.addAll(arguments);
    }

    @Override
    public boolean affects(Collection<TVariable> variables) {
      // TODO (AK211007) Check whether some variable is changed in the function body
      return true;
    }

    @Override
    public void accept(TAnnotation.Visitor visitor) {
      visitor.onUpdate(this);
    }

    @Override
    public String annotation() {
      StringBuilder resultBuilder = new StringBuilder();

      resultBuilder.append(function.getName());
      resultBuilder.append("(");
      resultBuilder.append(Formatter.separated(this.arguments, argument -> argument.expression(), ", "));
      resultBuilder.append(")");
    
      return resultBuilder.toString();
    }

    @Override
    public String statement(String prefix) {
      StringBuilder resultBuilder = new StringBuilder();

      resultBuilder.append(this.annotation());
      resultBuilder.append(";");
 
      return resultBuilder.toString();
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("Call [");
      resultBuilder.append(this.annotation());
      resultBuilder.append("]");
      return resultBuilder.toString();
    }
  }

  public Call call() {
    return new Call(this);
  }

  public Call call(List<TExpression> arguments) {
    return new Call(this, arguments);
  }

  public Call call(TExpression argument, @NonNull TExpression ... remainingArguments) {
    return new Call(this, Stream.concat(Stream.of(argument), Arrays.asList(remainingArguments).stream()).collect(toList()));
  }

  public Call call(int intArgument, @NonNull Integer ... remainingIntArguments) {
    return new Call(this, Stream.concat(Stream.of(TExpression.intConst(intArgument)),
                                        Arrays.asList(remainingIntArguments).stream().map(i -> TExpression.intConst(i))).collect(toList()));
  }

  public static class Return implements TStatement {
    private @Nullable TExpression expression;

    Return() {
    }

    Return(TExpression expression) {
      this.expression = expression;
    }

    @Override
    public String statement(String prefix) {
      StringBuilder resultBuilder = new StringBuilder();

      resultBuilder.append("return");
      var expression = this.expression;
      if (expression != null) {
        resultBuilder.append(" ");
        resultBuilder.append(expression.expression());
      }
      resultBuilder.append(";");
 
      return resultBuilder.toString();
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("Return [");
      resultBuilder.append(this.statement(""));
      resultBuilder.append("]");
      return resultBuilder.toString();
    }
  }

  public static Return ret() {
    return new Return();
  }

  public static Return ret(TExpression expression) {
    return new Return(expression);
  }

  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.returnType.getName());
    resultBuilder.append(" ");
    resultBuilder.append(this.name);
    resultBuilder.append("(");
    resultBuilder.append(Formatter.separated(this.parameters, parameter -> parameter.parameter(), ", "));
    resultBuilder.append(") ");
    resultBuilder.append(this.block.statement(prefix));

    return resultBuilder.toString();
  }

  public String declaration() {
    return this.declaration("");
  }
}
