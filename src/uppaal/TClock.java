package uppaal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static java.util.stream.Collectors.toList;


/**
 * UPPAAL clock
 */
@NonNullByDefault
public class TClock implements TDeclaration {
  private String name;
  private List<TExpression> lengths = new ArrayList<>();
    
  public TClock(String name, @NonNull TExpression ... lengths) {
    this.name = name;
    for (TExpression length : lengths)
      this.lengths.add(length);
  }
  
  /**
   * @return clock's name
   */
  public String getName(){
    return name;
  }

  /**
   * UPPAAL clock access
   *
   * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
   */
  public static class Access implements TArgument {
    private TClock clock;
    private List<TExpression> offsets = new ArrayList<>();

    /**
     * Create a clock access.
     *
     * @param clock a clock
     * @param offsets list of offsets
     * @return a clock access
     */
    Access(TClock clock, List<TExpression> offsets) {
      this.clock = clock;
      this.offsets.addAll(offsets);
    }

    /**
     * Create a clock access to this clock access.
     *
     * @param offsets list of offsets
     * @return a clock access
     */
    public Access access(TExpression offset, @NonNull TExpression ... offsets) {
      List<TExpression> offsetsList = new ArrayList<>(this.offsets);
      offsetsList.add(offset);
      offsetsList.addAll(Arrays.asList(offsets));
      return new Access(this.clock, offsetsList);
    }

    /**
     * Create a clock access to this clock access.
     *
     * @param offsets list of constant offsets
     * @return a clock access
     */
    public Access access(int intOffset, @NonNull Integer ... intOffsets) {
      List<TExpression> offsetsList = new ArrayList<>(this.offsets);
      offsetsList.add(TExpression.intConst(intOffset));
      offsetsList.addAll(Arrays.asList(intOffsets).stream().map(i -> TExpression.intConst(i)).collect(toList()));
      return new Access(this.clock, offsetsList);
    }

    /**
     * @return a clock assignment resetting this clock access
     */
    public Assign reset() {
      return new Assign(this);
    }

    /**
     * @return clock access representation
     */
    public String expression() {
      StringBuilder resultBuilder = new StringBuilder();

      resultBuilder.append(this.clock.getName());
      for (TExpression offset : this.offsets) {
        resultBuilder.append("[");
        resultBuilder.append(offset.expression());
        resultBuilder.append("]");
      }

      return resultBuilder.toString();
    }

    @Override
    public String argument() {
      return this.expression();
    }
  }

  /**
   * @return a clock access
   */
  public Access access() {
    return new Access(this, new ArrayList<>());
  }

  /**
   * Create a clock access to this clock.
   *
   * @param offsets list of offsets
   * @return a clock access
   */
  public Access access(TExpression offset, @NonNull TExpression ... offsets) {
    return this.access().access(offset, offsets);
  }

  /**
   * Create a clock access to this clock.
   *
   * @param offsets list of constant offsets
   * @return a clock access
   */
  public Access access(int intOffset, @NonNull Integer ... intOffsets) {
    return this.access().access(intOffset, intOffsets);
  }

  /**
   * UPPAAL clock assignment
   * 
   * TODO (AK121222) Also other integer expressions can be used for clock assignments
   * 
   * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
   */
  public static class Assign implements TUpdate {
    private Access access;

    /**
     * Create a clock assignment for resetting a clock accessed by some expression.
     *
     * @param access a clock access
     * @return clock assignment for resetting a clock accessed by some expression
     */
    Assign(Access access) {
      this.access = access;
    }

    @Override
    public boolean affects(Collection<TVariable> variables) {
      return false;
    }

    @Override
    public void accept(TAnnotation.Visitor visitor) {
      visitor.onUpdate(this);
    }

    @Override
    public String annotation() {
      StringBuilder resultBuilder = new StringBuilder();

      resultBuilder.append(access.expression());
      resultBuilder.append(" = 0");

      return resultBuilder.toString();
    }

    @Override
    public String statement(String prefix) {
      StringBuilder resultBuilder = new StringBuilder();

      resultBuilder.append(this.annotation());
      resultBuilder.append(";");

      return resultBuilder.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.access);
    }
 
    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Assign other = (Assign)object;
        return Objects.equals(this.access, other.access);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("TClock.Assign [");
      resultBuilder.append(this.annotation());
      resultBuilder.append("]");
      return resultBuilder.toString();
    }
  }

  @Override
  public Set<TDeclaration> getDependencies() {
    return new HashSet<>();
  }

  @Override
  public String parameter() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("clock &amp;");
    resultBuilder.append(this.name);
    for (TExpression length : this.lengths) {
      resultBuilder.append("[");
      resultBuilder.append(length.expression());
      resultBuilder.append("]");
    }

    return resultBuilder.toString();
  }

  @Override
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("clock ");
    resultBuilder.append(this.name);
    for (TExpression length : this.lengths) {
      resultBuilder.append("[");
      resultBuilder.append(length.expression());
      resultBuilder.append("]");
    }
    resultBuilder.append(";");

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name);
  }

  @Override
  public String toString(){
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Clock [");
    resultBuilder.append(this.name);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
