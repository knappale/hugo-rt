package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * UPPAAL state
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TState {
  private String name = "";
  private boolean initial = false;
  private boolean committed = false;
  private boolean urgent = false;
  private List<TInvariant> invariants = new ArrayList<>();
  private @Nullable TProcess process = null;

  /**
   * Create a new state.
   * 
   * @param name state's name
   */
  public TState(String name) {
    this.name = name;
  }

  /**
   * Create a new state with the indication whether it's committed.
   * 
   * @param name state's name
   * @param committed whether the state is committed
   */
  public TState(String name, boolean committed) {
    this.name = name;
    this.committed = committed;
  }

  /**
   * Set state's owning process.
   * 
   * @param process UPPAAL process
   */
  public void setProcess(TProcess process) {
    this.process = process;
  }

  /**
   * @return state's owning process
   */
  public @Nullable TProcess getProcess() {
    return this.process;
  }

  /**
   * @return state's name
   */
  public String getName() {
    return name;
  }

  /**
   * @return whether state is initial
   */
  public boolean isInitial() {
    return initial;
  }

  /**
   * modify state's initiality
   */
  public void setInitial(boolean initial) {
    this.initial = initial;
  }

  /**
   * @return whether state is committed
   */
  public boolean isCommitted() {
    return committed;
  }

  /**
   * modify state's commitment
   */
  public void setCommitted(boolean committed) {
    this.committed = committed;
  }

  /**
   * @return whether state is urgent
   */
  public boolean isUrgent() {
    return urgent;
  }

  /**
   * modify state's urgency
   */
  public void setUrgent(boolean urgent) {
    this.urgent = urgent;
  }

  /**
   * @return whether state has invariants
   */
  public boolean hasInvariants() {
    return (!this.invariants.isEmpty());
  }

  /**
   * Add an invariant.
   *
   * @param tInvariant an invariant
   */
  public void addInvariant(TInvariant tInvariant) {
    this.invariants.add(tInvariant);
  }

  /**
   * Add a collection of invariants.
   *
   * @param tInvariants a collection of invariants
   */
  public void addInvariants(Collection<TInvariant> tInvariants) {
    for (TInvariant tInvariant : tInvariants)
      this.addInvariant(tInvariant);
  }

  /**
   * Accept a visitor.
   *
   * @param visitor a visitor
   */
  public void accept(TSystem.Visitor visitor) {
    visitor.onState(this.getName());
  }

  /**
   * @param indent: indentation
   * @return state representation in TA-format
   */
  public String declaration(String indent) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(indent);
    resultBuilder.append(this.getName());

    if (this.hasInvariants()) {
      String nextIndent = indent;
      for (int i = 0; i < (this.getName() + " { ").length(); i++)
        nextIndent += " ";
      resultBuilder.append(" { ");
      boolean singleInvariant = (this.invariants.size() == 1);
      String postfix = "";
      for (TInvariant invariant : this.invariants) {
        resultBuilder.append(postfix);
        if (!singleInvariant && !invariant.hasAndPrecedence()) {
          resultBuilder.append("(");
          resultBuilder.append(invariant.annotation());
          resultBuilder.append(")");
        }
        else
          resultBuilder.append(invariant.annotation());
        postfix = TOperator.AND.getOpName() + "\n" + nextIndent;
      }
      resultBuilder.append(" }");
    }
    return resultBuilder.toString();
  }

  /**
   * @return state representation
   */
  public String declaration() {
    return this.declaration("");
  }

  /**
   * @return state representation in XML-format
   * TODO Does the location id have to be unique over all states?
   */
  public String declarationXML() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("<location id=\"");
    resultBuilder.append(getName());
    resultBuilder.append("\">\n");

    resultBuilder.append("<name>");
    resultBuilder.append(getName());
    resultBuilder.append("</name>\n");
    
    if (this.hasInvariants()) {
      resultBuilder.append("<label kind=\"invariant\">");

      String postfix = "";
      boolean singleInvariant = (this.invariants.size() == 1);
      for (TInvariant invariant : this.invariants) {
        resultBuilder.append(postfix);
        if (!singleInvariant && !invariant.hasAndPrecedence()) {
          resultBuilder.append("(");
          resultBuilder.append(Formatter.toXML(invariant.annotation()));
          resultBuilder.append(")");
        }
        else
          resultBuilder.append(Formatter.toXML(invariant.annotation()));
        postfix = TOperator.AND.getOpNameXML();
      }
      resultBuilder.append("</label>\n");
    }

    if (isCommitted()) {
      resultBuilder.append("<committed/>\n");
    }
    if (isUrgent()) {
      resultBuilder.append("<urgent/>\n");
    }

    resultBuilder.append("</location>");
    return resultBuilder.toString();
  }

  /**
   * @return state representation as a string
   */
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("State [name = ");
    resultBuilder.append(this.name);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
