package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * UPPAAL statement
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public interface TStatement {
  /**
   * @return statement representation
   */
  public String statement(String prefix);
}
