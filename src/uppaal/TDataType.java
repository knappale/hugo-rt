package uppaal;

import static java.util.stream.Collectors.toList;
import static util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Pair;


/**
 * UPPAAL data type
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class TDataType {
  private static final Void VOID = new Void();
  private static final Int INT = new Int(null, null);

  public static class Void extends TDataType {
    Void() {
    }

    @Override
    public String getNameDeclaration(String name) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("void");
      if (name != "") {
        resultBuilder.append(" ");
        resultBuilder.append(name);
      }
      return resultBuilder.toString();
    }

    @Override
    public int hashCode() {
      return 0;
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      return (object instanceof Void);
    }
  }

  /**
   * Create a data type representing void.
   *
   * @return UPPAAL data type
   */
  public static Void voidType() {
    return VOID;
  }

  public static class Int extends TDataType {
    private static int UPPAAL_MIN = -32768;
    private static int UPPAAL_MAX = 32767;
    private TExpression lower = TExpression.intConst(UPPAAL_MIN);
    private TExpression upper = TExpression.intConst(UPPAAL_MAX);

    Int(@Nullable TExpression lower, @Nullable TExpression upper) {
      if (lower != null)
        this.lower = lower;
      if (upper != null)
        this.upper = upper;
    }

    @Override
    public TExpression getDefaultValue() {
      int lowerInt = this.lower.getIntValue();
      int upperInt = this.upper.getIntValue();
      if (upperInt < 0 || 0 < lowerInt)
        return this.lower;
      return TExpression.intConst(0);
    }

    @Override
    public boolean isNumeric() {
      return true;
    }

    @Override
    public int maxValue() {
      return this.upper.getIntValue();
    }

    @Override
    public Set<TDeclaration> getDependencies() {
      Set<TDeclaration> dependencies = new HashSet<>();
      dependencies.addAll(this.lower.getDependencies());
      dependencies.addAll(this.upper.getDependencies());
      return dependencies;
    }

    @Override
    public String getNameDeclaration(String name) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("int");
      // Only show range restriction if it exists and deviates from UPPAAL's default
      if (this.lower.getIntValue() != UPPAAL_MIN || this.upper.getIntValue() != UPPAAL_MAX) {
        resultBuilder.append("[");
        resultBuilder.append(this.lower.expression());
        resultBuilder.append(", ");
        resultBuilder.append(this.upper.expression());
        resultBuilder.append("]");
      }
      if (!name.equals("")) {
        resultBuilder.append(" ");
        resultBuilder.append(name);
      }
      return resultBuilder.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.lower, this.upper);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Int other = (Int)object;
        return Objects.equals(this.lower, other.lower) &&
               Objects.equals(this.upper, other.upper);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }
  }

  /**
   * Create a data type representing integer.
   *
   * @return UPPAAL data type
   */
  public static Int intType() {
    return INT;
  }

  /**
   * Create a data type representing boolean.
   *
   * @return UPPAAL data type
   */
  public static Int boolType() {
    return new Int(TExpression.intConst(0), TExpression.intConst(1));
  }

  /**
   * Create a data type representing a range sub-type of integer.
   *
   * @param lower lower bound
   * @param upper upper bound
   * @return UPPAAL data type
   */
  public static Int range(TExpression lower, TExpression upper) {
    return new Int(lower, upper);
  }

  /**
   * Create a data type representing a range sub-type of integer.
   *
   * @param lower lower bound
   * @param upper upper bound
   * @return UPPAAL data type
   */
  public static Int range(int lower, int upper) {
    return range(TExpression.intConst(lower), TExpression.intConst(upper));
  }

  /**
   * Create a data type representing a range sub-type of integer, the
   * lower bound set to 0.
   *
   * @param upper upper bound
   * @return UPPAAL data type
   */
  public static Int range(TExpression upper) {
    return range(TExpression.intConst(0), upper);
  }

  /**
   * Create a data type representing a range sub-type of integer, the
   * lower bound set to 0.
   *
   * @param upper upper bound
   * @return UPPAAL data type
   */
  public static Int range(int upper) {
    return range(TExpression.intConst(upper));
  }

  public static class Array extends TDataType {
    private TDataType underlying;
    private List<TExpression> lengths = new ArrayList<>();

    Array(TDataType underlying, List<TExpression> lengths) {
      this.underlying = underlying;
      this.lengths.addAll(lengths);
    }

    @Override
    public TExpression getDefaultValue() {
      var defaultValue = this.getArray(this.underlying.getDefaultValue(), this.lengths.get(0).getIntValue());
      for (int i = 1; i < this.lengths.size(); i++)
        defaultValue = this.getArray(defaultValue, this.lengths.get(i).getIntValue());
      return defaultValue;
    }

    private TExpression getArray(TExpression value, int length) {
      var values = new ArrayList<TExpression>();
      for (int i = 0; i < length; i++)
        values.add(value);
      return TExpression.array(values);
    }
      
    @Override
    public TDataType getType(int offset) {
      return this.underlying;
    }

    @Override
    public Set<TDeclaration> getDependencies() {
      Set<TDeclaration> dependencies = new HashSet<>();
      for (var length : this.lengths)
        dependencies.addAll(length.getDependencies());
      return dependencies;
    }

    @Override
    public String getNameDeclaration(String name) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(requireNonNull(this.underlying).getNameDeclaration(name));
      for (TExpression length : this.lengths) {
        resultBuilder.append("[");
        resultBuilder.append(length.expression());
        resultBuilder.append("]");
      }
      return resultBuilder.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.underlying, this.lengths);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Array other = (Array)object;
        return Objects.equals(this.underlying, other.underlying) &&
               Objects.equals(this.lengths, other.lengths);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }
  }

  /**
   * Create a data type representing an array.
   *
   * @param type underlying type
   * @param length array length
   * @return UPPAAL data type
   */
  public static Array array(TDataType type, TExpression length) {
    return new Array(type, Arrays.asList(length));
  }

  /**
   * Create a data type representing an array.
   *
   * @param type underlying type
   * @param length array length
   * @return UPPAAL data type
   */
  public static Array array(TDataType type, int length, @NonNull Integer ... remainingLengths) {
    var lengths = new ArrayList<TExpression>();
    lengths.add(TExpression.intConst(length));
    lengths.addAll(Arrays.asList(remainingLengths).stream().map(l -> TExpression.intConst(l)).collect(toList()));
    return new Array(type, lengths);
  }

  /**
   * Create a data type representing an array.
   *
   * @param type underlying type
   * @param length array length
   * @return UPPAAL data type
   */
  public static Array array(TDataType type, @NonNull TExpression ... lengths) {
    return new Array(type, Arrays.asList(lengths));
  }

  public static class Struct extends TDataType {
    private List<Pair<String, TDataType>> fields = new ArrayList<>();

    Struct(List<Pair<String, TDataType>> fields) {
      this.fields.addAll(fields);
    }

    @Override
    @SuppressWarnings("null")
    public TDataType getType(String fieldName) {
      return this.fields.stream().filter(f -> f.getFirst().equals(fieldName)).map(f -> f.getSecond()).findAny().orElse(voidType());
    }

    @Override
    public TExpression getDefaultValue() {
      return TExpression.struct(this.fields.stream().map(f -> new Pair<String, TExpression>(f.getFirst(), f.getSecond().getDefaultValue())).collect(toList()));
    }

    @Override
    public Set<TDeclaration> getDependencies() {
      Set<TDeclaration> dependencies = new HashSet<>();
      for (var field : this.fields)
        dependencies.addAll(field.getSecond().getDependencies());
      return dependencies;
    }

    @Override
    public String getNameDeclaration(String name) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("struct { ");
      resultBuilder.append(util.Formatter.separated(this.fields, f -> f.getSecond().getNameDeclaration(f.getFirst().toString()) + ";", " "));
      resultBuilder.append(" }");
      return resultBuilder.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.fields);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Struct other = (Struct)object;
        return Objects.equals(this.fields, other.fields);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }
  }

  /**
   * Create a data type representing a record.
   *
   * @param fields a list of pairs of names and data types
   * @return UPPAAL data type
   */
  public static Struct struct(List<Pair<String, TDataType>> fields) {
    return new Struct(fields);
  }

  public static class Def extends TDataType {
    private String name;
    private TDataType underlying;

    Def(String name, TDataType underlying) {
      this.name = name;
      this.underlying = underlying;
    }

    @Override
    public TExpression getDefaultValue() {
      return this.underlying.getDefaultValue();
    }

    /**
     * @return data type representation
     */
    public String declaration() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("typedef ");
      resultBuilder.append(this.underlying.getNameDeclaration(""));
      resultBuilder.append(" ");
      resultBuilder.append(this.name);
      resultBuilder.append(";");
      return resultBuilder.toString();
    }

    @Override
    public Set<TDeclaration> getDependencies() {
      return this.underlying.getDependencies();
    }

    @Override
    public String getNameDeclaration(String name) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(this.name);
      if (!name.equals("")) {
        resultBuilder.append(" ");
        resultBuilder.append(name);
      }
      return resultBuilder.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.name, this.underlying);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Def other = (Def)object;
        return Objects.equals(this.name, other.name) &&
               Objects.equals(this.underlying, other.underlying);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }
  }

  /**
   * Create a data type representing a type definition.
   *
   * @param fields a map of names to data types
   * @return UPPAAL data type
   */
  public static TDataType.Def typeDef(String name, TDataType underlying) {
    return new Def(name, underlying);
  }

  /**
   * @return data type of this data type at {@code offset}
   */
  @SuppressWarnings("unused")
  public TDataType getType(int offset) {
    assert false : "not applicable";
    return voidType();
  }

  /**
   * @return data type of this data type at {@code fieldName}
   */
  @SuppressWarnings("unused")
  public TDataType getType(String fieldName) {
    assert false : "not applicable";
    return voidType();
  }

  /**
   * @return this data type's default value
   */
  public TExpression getDefaultValue() {
    assert false : "not applicable";
    return TExpression.falseConst();
  }

  /**
   * Determine whether this data type is numeric, i.e., an integer range.
   * (Booleans are seen as the integer range [0..1]).
   *
   * @return whether data type is numeric
   */
  public boolean isNumeric() {
    return false;
  }

  /**
   * Determine the maximum value that can be stored in this data type.
   *
   * @return data type's maximum value
   */
  public int maxValue() {
    assert false : "not applicable";
    return -1;
  }

  /**
   * @return set of declarations this data type (transitively) depends on
   */
  public Set<TDeclaration> getDependencies() {
    return new HashSet<>();
  }

  /**
   * @return constant/variable (also as parameter) declaration of {@code name} for this data type
   */
  public abstract String getNameDeclaration(String name);

  /**
   * @return this data type's name
   */
  public String getName() {
    return this.getNameDeclaration("");
  }

  @Override
  public String toString() {
    return this.getName();
  }
}
