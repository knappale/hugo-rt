package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * UPPAAL guard
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public interface TGuard extends TAnnotation {
  /**
   * @return whether this guard takes precedence over Operator.AND
   */
  public boolean hasAndPrecedence();
}
