package uppaal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * UPPAAL channel
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TChannel implements TDeclaration {
  private String name;
  private List<TExpression> lengths = new ArrayList<>();
  private boolean urgent;

  public TChannel(String name, @NonNull TExpression ... lengths) {
    this.name = name;
    for (TExpression length : lengths)
      this.lengths.add(length);
    this.urgent = false;
  }

  public TChannel(boolean urgent, String name, @NonNull TExpression ... lengths) {
    this.name = name;
    this.urgent = urgent;
    for (TExpression length : lengths)
      this.lengths.add(length);
  }

  /**
   * @return channel's name
   */
  public String getName() {
    return this.name;
  }

  @Override
  public Set<TDeclaration> getDependencies() {
    return new HashSet<TDeclaration>();
  }

  /**
   * UPPAAL channel access
   *
   * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
   * @since 2013-05-30
   */
  public static class Access implements TArgument {
    private TChannel channel;
    private List<TExpression> offsets = new ArrayList<>();

    /**
     * Create channel access.
     *
     * @param channel a channel
     * @param offsets a list of offset expressions
     */
    Access(TChannel channel, List<TExpression> offsets) {
      this.channel = channel;
      if (offsets != null)
        this.offsets.addAll(offsets);
    }

    /**
     * Create a channel access.
     *
     * @param channel a channel
     * @param offsets list of offsets
     */
    Access(TChannel channel, @NonNull TExpression ... offsets) {
      this.channel = channel;
      for (TExpression offset : offsets)
        this.offsets.add(offset);
    }

    /**
     * @return sending synchronisation on this channel access
     */
    public TSynchronisation send() {
      return TSynchronisation.send(this);
    }

    /**
     * @return receiving synchronisation on this channel access
     */
    public TSynchronisation receive() {
      return TSynchronisation.receive(this);
    }

    /**
     * @return channel access representation
     */
    public String expression() {
      StringBuilder resultBuilder = new StringBuilder();

      resultBuilder.append(this.channel.getName());
      for (TExpression offset : this.offsets) {
        resultBuilder.append("[");
        resultBuilder.append(offset.expression());
        resultBuilder.append("]");
      }

      return resultBuilder.toString();
    }

    @Override
    public String argument() {
      return this.expression();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.channel,
                          this.offsets);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Access other = (Access)object;
        return Objects.equals(this.channel, other.channel) &&
               Objects.equals(this.offsets, other.offsets);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("ChannelAccess [");
      resultBuilder.append(this.expression());
      resultBuilder.append("]");
      return resultBuilder.toString();
    }
  }

  /**
   * @param offsets a list of offsets
   * @return a channel access to this channel with offsets
   */
  public Access access(@NonNull TExpression ... offsets) {
    return new Access(this, offsets);
  }

  @Override
  public String parameter() {
    StringBuilder resultBuilder = new StringBuilder();

    if (this.urgent)
      resultBuilder.append("urgent ");
    resultBuilder.append("chan &");
    resultBuilder.append(this.getName());
    for (TExpression length : this.lengths) {
      resultBuilder.append("[");
      resultBuilder.append(length.expression());
      resultBuilder.append("]");
    }

    return resultBuilder.toString();
  }

  @Override
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    if (this.urgent)
      resultBuilder.append("urgent ");
    resultBuilder.append("chan ");
    resultBuilder.append(this.getName());
    for (TExpression length : this.lengths) {
      resultBuilder.append("[");
      resultBuilder.append(length.expression());
      resultBuilder.append("]");
    }
    resultBuilder.append(";");

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name);
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Channel [");
    resultBuilder.append(this.declaration());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
