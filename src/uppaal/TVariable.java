package uppaal;

import java.util.Collection;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static util.Objects.requireNonNull;

/**
 * UPPAAL variable
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TVariable extends TField {
  private @Nullable TExpression initialValue;

  /**
   * Create a variable.
   *
   * @param type variable's type
   * @param name variable's name
   */
  public TVariable(TDataType type, String name) {
    super(type, name);
  }

  /**
   * Create an initialised variable.
   *
   * @param type variable's type
   * @param name variable's name
   * @param initialValue variable's initial value
   */
  public TVariable(TDataType type, String name, TExpression initialValue) {
    super(type, name);
    this.initialValue = initialValue;
  }

  /**
   * Create an integer variable.
   *
   * @param name variable's name
   */
  public TVariable(String name) {
    super(TDataType.intType(), name);
  }

  /**
   * Create an integer array variable.
   * 
   * @param name variable's name
   * @param length array's length expression
   */
  public TVariable(String name, TExpression length) {
    super(TDataType.array(TDataType.intType(), length), name);
  }

  @Override
  public TExpression getInitialValue() {
    assert false : "not applicable";
    return TExpression.falseConst();
  }

  public static class Access extends TField.Access {
    Access(TVariable variable) {
      super(variable);
    }

    Access(Access access, TField.Accessor accessor) {
      super(access, accessor);
    }

    @Override
    public Access access(Accessor accessor, @NonNull Accessor... remainingAccessors) {
      var access = new Access(this, accessor);
      for (var remainingAccessor : remainingAccessors)
        access = new Access(access, remainingAccessor);
      return access;
    }

    public Access access(String fieldName) {
      return this.access(new StructAccessor(fieldName));
    }

    public Access access(int offset) {
      return this.access(TExpression.intConst(offset));
    }

    /**
     * An assignment for this variable access with an expression.
     *
     * @param expression an expression
     * @return an assignment for this variable access with the expression
     */
    public Assign assign(TExpression expression) {
      return Assign.assign(this, expression);
    }

    /**
     * An assignment for this variable access with a boolean.
     *
     * @param bool a boolean
     * @return an assignment for this variable access with {@code bool}
     */
    public Assign assign(boolean bool) {
      return this.assign(TExpression.boolConst(bool));
    }

    /**
     * An assignment for this variable access with an integer.
     *
     * @param integer an integer
     * @return an assignment for this variable access with {@code integer}
     */
    public Assign assign(int integer) {
      return this.assign(TExpression.intConst(integer));
    }

    /**
     * @return variable access' underlying variable
     */
    public TVariable getVariable() {
      return (TVariable) this.getField();
    }

    /**
     * An increment for this variable access.
     *
     * @param expression an expression
     * @return an increment for this variable access
     */
    public Assign inc() {
      return Assign.inc(this);
    }

    /**
     * A decrement for this variable access.
     *
     * @param expression an expression
     * @return a decrement for this variable access
     */
    public Assign dec() {
      return Assign.dec(this);
    }
  }

  @Override
  public Access access() {
    return new Access(this);
  }

  @Override
  public Access access(Accessor accessor, @NonNull Accessor... remainingAccessors) {
    return this.access().access(accessor, remainingAccessors);
  }

  /**
   * UPPAAL variable assignment
   * 
   * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
   */
  public static class Assign implements TUpdate {
    static enum Kind {
      EXPRESSION, POSTINC, POSTDEC;
    }

    private Kind kind;
    private Access access;
    private @Nullable TExpression expression;

    private Assign(Kind kind, Access access) {
      this.kind = kind;
      this.access = access;
    }

    /**
     * Create a variable assignment to a variable access with an expression.
     *
     * @param lhs a variable access
     * @param rhs an expression
     * @return a variable assignment to the variable access with the expression
     */
    static Assign assign(Access lhs, TExpression rhs) {
      Assign a = new Assign(Kind.EXPRESSION, lhs);
      a.expression = rhs;
      return a;
    }

    /**
     * Create a variable increment for a variable access.
     *
     * @param access a variable access
     * @return a variable increment for the variable access
     */
    static Assign inc(Access access) {
      return new Assign(Kind.POSTINC, access);
    }

    /**
     * Create a variable decrement for a variable access.
     *
     * @param access a variable access
     * @return a variable decrement for the variable access
     */
    static Assign dec(Access access) {
      return new Assign(Kind.POSTDEC, access);
    }

    @Override
    public boolean affects(Collection<TVariable> variables) {
      return variables.contains(this.access.getVariable());
    }

    @Override
    public void accept(TAnnotation.Visitor visitor) {
      visitor.onUpdate(this);
    }

    /**
     * @return variable assignment expression representation
     */
    public String expression() {
      StringBuilder resultBuilder = new StringBuilder();

      switch (this.kind) {
        case EXPRESSION:
          resultBuilder.append(this.access.expression());
          resultBuilder.append(" = ");
          resultBuilder.append(requireNonNull(this.expression).expression());
          break;

        case POSTINC:
          resultBuilder.append(this.access.expression());
          resultBuilder.append("++");
          break;

        case POSTDEC:
          resultBuilder.append(this.access.expression());
          resultBuilder.append("--");
          break;
      }

      return resultBuilder.toString();
    }

    @Override
    public String annotation() {
      return this.expression();
    }

    @Override
    public String statement(String prefix) {
      StringBuilder resultBuilder = new StringBuilder();

      resultBuilder.append(this.annotation());
      resultBuilder.append(";");

      return resultBuilder.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.access, this.expression);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Assign other = (Assign) object;
        return Objects.equals(this.access, other.access) && Objects.equals(this.expression, other.expression);
      } catch (ClassCastException cce) {
        return false;
      }
    }

    /**
     * @return variable assignment representation as a string
     */
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("TVariable.Assign [");
      resultBuilder.append(this.annotation());
      resultBuilder.append("]");
      return resultBuilder.toString();
    }
  }

  @Override
  public String parameter() {
    return this.getType().getNameDeclaration(this.getName());
  }

  @Override
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.getType().getNameDeclaration(this.getName()));
    var initialValue = this.initialValue;
    if (initialValue != null) {
      resultBuilder.append(" := ");
      resultBuilder.append(initialValue.expression());
    }
    resultBuilder.append(";");

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getType(), this.getName());
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Variable [");
    resultBuilder.append(this.declaration());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
