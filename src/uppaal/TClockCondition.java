package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import java.util.Objects;

import static util.Objects.requireNonNull;


/**
 * UPPAAL clock condition for lower bounds on clocks
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TClockCondition implements TGuard {
  static enum Kind {
    SIMPLE,
    COMPARISON,
    DIFFERENCE;
  }

  private Kind kind;
  private TClock.Access clock;
  private TClock.@Nullable Access otherClock;
  private @Nullable TOperator relOp;
  private @Nullable TExpression bound;

  private TClockCondition(Kind kind, TClock.Access clock) {
    this.kind = kind;
    this.clock = clock;
  }

  /**
   * Create a clock condition for a clock compared to a bound.
   *
   * @param clock a clock
   * @param relOp a relational operator
   * @param bound a bound expression
   * @return a clock condition
   */
  private static TClockCondition simple(TClock.Access clock, TOperator relOp, TExpression bound) {
    TClockCondition i = new TClockCondition(Kind.SIMPLE, clock);
    i.relOp = relOp;
    i.bound = bound;
    return i;
  }

  /**
   * Create a clock condition for a clock bounded from above (inclusive).
   *
   * @param clock a clock
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockCondition leq(TClock.Access clock, TExpression bound) {
    return TClockCondition.simple(clock, TOperator.LEQ, bound);
  }

  /**
   * Create a clock condition for a clock bounded from above (exclusive).
   *
   * @param clock a clock
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockCondition lt(TClock.Access clock, TExpression bound) {
    return TClockCondition.simple(clock, TOperator.LT, bound);
  }

  /**
   * Create a clock condition for a clock bounded from below (inclusive).
   *
   * @param clock a clock
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockCondition geq(TClock.Access clock, TExpression bound) {
    return TClockCondition.simple(clock, TOperator.GEQ, bound);
  }

  /**
   * Create a clock condition for a clock bounded from below (exclusive).
   *
   * @param clock a clock
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockCondition gt(TClock.Access clock, TExpression bound) {
    return TClockCondition.simple(clock, TOperator.GT, bound);
  }

  /**
   * Create a clock condition for a clock bounded from below (inclusive).
   *
   * @param bound a bound expression
   * @param clock a clock
   * @return a clock condition
   */
  public static TClockCondition leq(TExpression bound, TClock.Access clock) {
    return TClockCondition.simple(clock, TOperator.GEQ, bound);
  }

  /**
   * Create a clock condition for a clock bounded from below (exclusive).
   *
   * @param bound a bound expression
   * @param clock a clock
   * @return a clock condition
   */
  public static TClockCondition lt(TExpression bound, TClock.Access clock) {
    return TClockCondition.simple(clock, TOperator.GT, bound);
  }

  /**
   * Create a clock condition for a clock bounded from above (inclusive).
   *
   * @param bound a bound expression
   * @param clock a clock
   * @return a clock condition
   */
  public static TClockCondition geq(TExpression bound, TClock.Access clock) {
    return TClockCondition.simple(clock, TOperator.LEQ, bound);
  }

  /**
   * Create a clock condition for a clock bounded from above (exclusive).
   *
   * @param bound a bound expression
   * @param clock a clock
   * @return a clock condition
   */
  public static TClockCondition gt(TExpression bound, TClock.Access clock) {
    return TClockCondition.simple(clock, TOperator.LT, bound);
  }

  /**
   * Create a clock condition for comparing two clocks.
   *
   * @param clock a clock
   * @param relOp a relational operator
   * @param otherClock another clock
   * @return a clock condition
   */
  private static TClockCondition comparison(TClock.Access clock, TOperator relOp, TClock.Access otherClock) {
    TClockCondition i = new TClockCondition(Kind.COMPARISON, clock);
    i.relOp = relOp;
    i.clock = otherClock;
    return i;
  }

  /**
   * Create a clock condition for comparing two clocks w.r.t. less-or-equal.
   *
   * @param clock a clock
   * @param otherClock another clock
   * @return a clock condition
   */
  public static TClockCondition leq(TClock.Access clock, TClock.Access otherClock) {
    return TClockCondition.comparison(clock, TOperator.LEQ, otherClock);
  }

  /**
   * Create a clock condition for comparing two clocks w.r.t. less-than.
   *
   * @param clock a clock
   * @param otherClock another clock
   * @return a clock condition
   */
  public static TClockCondition lt(TClock.Access clock, TClock.Access otherClock) {
    return TClockCondition.comparison(clock, TOperator.LT, otherClock);
  }

  /**
   * Create a clock condition for comparing two clocks w.r.t. greater-or-equal.
   *
   * @param clock a clock
   * @param otherClock another clock
   * @return a clock condition
   */
  public static TClockCondition geq(TClock.Access clock, TClock.Access otherClock) {
    return TClockCondition.comparison(clock, TOperator.GEQ, otherClock);
  }

  /**
   * Create a clock condition for comparing two clocks w.r.t. greater-than.
   *
   * @param clock a clock
   * @param otherClock another clock
   * @return a clock condition
   */
  public static TClockCondition gt(TClock.Access clock, TClock.Access otherClock) {
    return TClockCondition.comparison(clock, TOperator.GT, otherClock);
  }

  /**
   * Create a clock condition for comparing the difference between two clocks
   * with a bound.
   *
   * @param clock a clock to be subtracted from
   * @param otherClock a clock to be subtracted
   * @param relOp a relational operator
   * @param bound a bound expression
   * @return a clock condition
   */
  private static TClockCondition difference(TClock.Access clock, TClock.Access otherClock, TOperator relOp, TExpression bound) {
    TClockCondition i = new TClockCondition(Kind.DIFFERENCE, clock);
    i.otherClock = otherClock;
    i.relOp = relOp;
    i.bound = bound;
    return i;
  }

  /**
   * Create a clock condition for comparing the difference between two clocks w.r.t.
   * less-or-equal to a bound.
   *
   * @param clock a clock to be subtracted from
   * @param otherClock a clock to be subtracted
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockCondition leq(TClock.Access clock, TClock.Access otherClock, TExpression bound) {
    return TClockCondition.difference(clock, otherClock, TOperator.LEQ, bound);
  }

  /**
   * Create a new clock condition for comparing the difference between two clocks w.r.t.
   * less-than to a bound.
   *
   * @param clock a clock to be subtracted from
   * @param otherClock a clock to be subtracted
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockCondition lt(TClock.Access clock, TClock.Access otherClock, TExpression bound) {
    return TClockCondition.difference(clock, otherClock, TOperator.LT, bound);
  }

  /**
   * Create a clock condition for comparing the difference between two clocks w.r.t.
   * greater-or-equal to a bound.
   *
   * @param clock a clock to be subtracted from
   * @param otherClock a clock to be subtracted
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockCondition geq(TClock.Access clock, TClock.Access otherClock, TExpression bound) {
    return TClockCondition.difference(clock, otherClock, TOperator.GEQ, bound);
  }

  /**
   * Create a new clock condition for comparing the difference between two clocks w.r.t.
   * greater-than to a bound.
   *
   * @param clock a clock to be subtracted from
   * @param otherClock a clock to be subtracted
   * @param bound a bound expression
   * @return a clock condition
   */
  public static TClockCondition gt(TClock.Access clock, TClock.Access otherClock, TExpression bound) {
    return TClockCondition.difference(clock, otherClock, TOperator.GT, bound);
  }

  /**
   * Create a clock condition for comparing the difference between two clocks w.r.t.
   * less-or-equal to a bound.
   *
   * @param bound a bound expression
   * @param clock a clock to be subtracted from
   * @param otherClock a clock to be subtracted
   * @return a clock condition
   */
  public static TClockCondition leq(TExpression bound, TClock.Access clock, TClock.Access otherClock) {
    return TClockCondition.difference(clock, otherClock, TOperator.GEQ, bound);
  }

  /**
   * Create a new clock condition for comparing the difference between two clocks w.r.t.
   * less-than to a bound.
   *
   * @param bound a bound expression
   * @param clock a clock to be subtracted from
   * @param otherClock a clock to be subtracted
   * @return a clock condition
   */
  public static TClockCondition lt(TExpression bound, TClock.Access clock, TClock.Access otherClock) {
    return TClockCondition.difference(clock, otherClock, TOperator.GT, bound);
  }

  /**
   * Create a clock condition for comparing the difference between two clocks w.r.t.
   * greater-or-equal to a bound.
   *
   * @param bound a bound expression
   * @param clock a clock to be subtracted from
   * @param otherClock a clock to be subtracted
   * @return a clock condition
   */
  public static TClockCondition geq(TExpression bound, TClock.Access clock, TClock.Access otherClock) {
    return TClockCondition.difference(clock, otherClock, TOperator.LEQ, bound);
  }

  /**
   * Create a new clock condition for comparing the difference between two clocks w.r.t.
   * greater-than to a bound.
   *
   * @param bound a bound expression
   * @param clock a clock to be subtracted from
   * @param otherClock a clock to be subtracted
   * @return a clock condition
   */
  public static TClockCondition gt(TExpression bound, TClock.Access clock, TClock.Access otherClock) {
    return TClockCondition.difference(clock, otherClock, TOperator.LT, bound);
  }

  /**
   * @return whether this clock condition takes precedence over Operator.AND
   */
  public boolean hasAndPrecedence() {
    return true;
  }

  /**
   * @return clock condition representation in XML-format
   */
  public String annotation() {
    StringBuilder resultBuilder = new StringBuilder();
   
    switch (this.kind) {
      case SIMPLE: {
        resultBuilder.append(this.clock.expression());
        resultBuilder.append(requireNonNull(this.relOp).getOpName());
        resultBuilder.append(requireNonNull(this.bound).expression());
        break;
      }
    
      case COMPARISON: {
        resultBuilder.append(this.clock.expression());
        resultBuilder.append(requireNonNull(this.relOp).getOpName());
        resultBuilder.append(requireNonNull(this.otherClock).expression());
        break;
      }

      case DIFFERENCE: {
        resultBuilder.append(this.clock.expression());
        resultBuilder.append(TOperator.SUB.getOpName());
        resultBuilder.append(requireNonNull(this.otherClock).expression());
        resultBuilder.append(requireNonNull(this.relOp).getOpName());
        resultBuilder.append(requireNonNull(this.bound).expression());
        break;
      }
    }

    return resultBuilder.toString();
  }

  @Override
  public void accept(TAnnotation.Visitor visitor) {
    visitor.onGuard(this);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.kind,
                        this.clock,
                        this.otherClock,
                        this.relOp,
                        this.bound);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      TClockCondition other = (TClockCondition)object;
      return this.kind == other.kind &&
             Objects.equals(this.clock, other.clock) &&
             Objects.equals(this.otherClock, other.otherClock) &&
             this.relOp == other.relOp &&
             Objects.equals(this.bound, other.bound);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("TClockCondition [");
    resultBuilder.append(this.annotation());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
