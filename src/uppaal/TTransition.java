package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static util.Objects.requireNonNull;


/**
 * UPPAAL transition
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TTransition {
  private TState source;
  private TState target;
  private List<TGuard> guards = new ArrayList<>();
  private @Nullable TSynchronisation sync;
  private List<TUpdate> updates = new ArrayList<>();

  /**
   * Create an UPPAAL transition.
   *
   * @param source source state
   * @param target target state
   */
  public TTransition(TState source, TState target) {
    this.source = source;
    this.target = target;
  }
 
  /**
   * Create an UPPAAL transition.
   *
   * @param source source state
   * @param target target state
   * @param guard guard
   * @param sync synchronisation
   * @param updates list of updates
   */
  public TTransition(TState source, TState target, @Nullable TGuard guard, @Nullable TSynchronisation sync, List<TUpdate> updates) {
    this.source = source;
    this.target = target;
    if (guard != null)
      this.addGuard(guard);
    this.sync = sync;
    this.updates = updates;
  }

  /**
   * Create an UPPAAL transition.
   *
   * @param source source state
   * @param target target state
   * @param guards collection of guards
   * @param sync synchronisation
   * @param updates list of updates
   */
  public TTransition(TState source, TState target, List<TGuard> guards, @Nullable TSynchronisation sync, List<TUpdate> updates) {
    this.source = source;
    this.target = target;
    this.addGuards(guards);
    this.sync = sync;
    this.updates = updates;
  }

  /**
   * Create an UPPAAL transition from an existing transition.
   *
   * @param transition an already existing transition
   */
  public TTransition(TTransition transition) {
    this.source = transition.source;
    this.target = transition.target;
    this.addGuards(transition.guards);
    this.sync = transition.sync;
    this.addUpdates(transition.updates);
  }

  /**
   * @return transition's source state
   */
  public TState getSource() {
    return this.source;
  }

  /**
   * Change transition's source state
   */
  public void setSource(TState source) {
    this.source = source;
  }

  /**
   * @return transition's target state
   */
  public TState getTarget(){
    return this.target;
  }

  /**
   * Change transition's target state
   */
  public void setTarget(TState target) {
    this.target = target;
  }

  /**
   * Add a guard to transition's guards
   *
   * @param guard a guard
   */
  public void addGuard(TGuard guard) {
    this.guards.add(guard);
  }

  /**
   * Add a collection of guards to transition's guards
   *
   * @param guards a list of guards
   */
  public void addGuards(List<TGuard> guards) {
    for (TGuard guard : guards)
      this.addGuard(guard);
  }

  /**
   * @return whether transition shows guards
   */
  public boolean hasGuards() {
    return (!this.guards.isEmpty());
  }

  /**
   * @return transition's guards
   */
  public List<TGuard> getGuards() {
    return this.guards;
  }

  /**
   * Modify transition's synchronisation
   */
  public void setSynchronisation(@Nullable TSynchronisation sync) {
    this.sync = sync;
  }
    
  /**
   * @return whether transition shows a synchronisation
   */
  public boolean hasSynchronisation() {
    return (this.sync != null);
  }
   
  /**
   * @return transition's synchronisation
   */
  public @Nullable TSynchronisation getSynchronisation() {
    return this.sync;
  }
    
  /**
   * Add an update to transition's updates
   */
  public void addUpdate(TUpdate update) {
    this.updates.add(update);
  }

  /**
   * Add a collection of updates to transition's updates
   */
  public void addUpdates(Collection<TUpdate> updates) {
    for (TUpdate update : updates)
      this.addUpdate(update);
  }
    
  /**
   * @return whether transition shows updates
   */
  public boolean hasUpdates() {
    return !this.updates.isEmpty();
  }

  /**
   * @return transition's updates
   */
  public List<TUpdate> getUpdates() {
    return this.updates;
  }
    
  /**
   * Accept a system visitor.
   */
  public void accept(TSystem.Visitor visitor) {
    visitor.onTransition(this.getSource(), this.getTarget());
  }

  /**
   * @param indent indentation
   * @return transition representation in TA-format
   */
  public String declaration(String indent) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(indent);
    resultBuilder.append(this.source.getName() + " -> " + this.target.getName() + " {\n");

    String nextIndent = indent + "  ";

    // Order matters: guard - sync - assignment
    if (hasGuards()) {
      resultBuilder.append(nextIndent);
      resultBuilder.append("guard ");

      boolean singleGuard = (this.getGuards().size() == 1);
      String postfix = "";
      for (TGuard guard : this.getGuards()) {
        resultBuilder.append(postfix);
        if (!singleGuard && !guard.hasAndPrecedence()) {
          resultBuilder.append("(");
          resultBuilder.append(guard.annotation());
          resultBuilder.append(")");
        }
        else
          resultBuilder.append(guard.annotation());
        postfix = TOperator.AND.getOpName() + "\n" + nextIndent + "      ";
      }
      resultBuilder.append(";\n");
    }
        
    if (hasSynchronisation()) {
      resultBuilder.append(nextIndent);

      resultBuilder.append("sync ");
      resultBuilder.append(requireNonNull(this.getSynchronisation()).annotation());
      resultBuilder.append(";\n");
    }

    if (hasUpdates()) {
      resultBuilder.append(nextIndent);
      resultBuilder.append("assign ");

      String postfix = "";
      for (TUpdate update : this.updates) {
        resultBuilder.append(postfix);
        resultBuilder.append(update.annotation());
        postfix = ",\n" + nextIndent + "       ";
      }
      resultBuilder.append(";\n");
    }

    resultBuilder.append(indent);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  /**
   * @return transition representation
   */
  public String declaration() {
    return this.declaration("");
  }

  /**
   * @return transition representation in XML-format
   */
  public String declarationXML() {
    StringBuilder resultBuilder = new StringBuilder();
    String sep = "";

    resultBuilder.append("<transition>");

    resultBuilder.append("<source ref=\"");
    resultBuilder.append(source.getName());
    resultBuilder.append("\"/>\n");

    resultBuilder.append("<target ref=\"");
    resultBuilder.append(target.getName());
    resultBuilder.append("\"/>\n");

    if (hasGuards()) {
      resultBuilder.append("<label kind=\"guard\">");
      sep = "";
      boolean singleGuard = (this.getGuards().size() == 1);
      for (TGuard guard : this.getGuards()) {
        resultBuilder.append(sep);
        if (!singleGuard && !guard.hasAndPrecedence()) {
          resultBuilder.append("(");
          resultBuilder.append(Formatter.toXML(guard.annotation()));
          resultBuilder.append(")");
        }
        else
          resultBuilder.append(Formatter.toXML(guard.annotation()));
        sep = TOperator.AND.getOpName();
      }
      resultBuilder.append("</label>\n");
    }

    if (hasSynchronisation()) {
      resultBuilder.append("<label kind=\"synchronisation\">");
      resultBuilder.append(requireNonNull(this.getSynchronisation()).annotation());
      resultBuilder.append("</label>\n");
    }

    if (hasUpdates()) {
      resultBuilder.append("<label kind=\"assignment\">");
      sep = "";
      for (TUpdate assign : this.getUpdates()) {
        resultBuilder.append(sep);
        resultBuilder.append(Formatter.toXML(assign.annotation()));
        sep = ", ";
      }
      resultBuilder.append("</label>\n");
    }

    resultBuilder.append("</transition>");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Transition [source = ");
    resultBuilder.append(this.getSource());
    resultBuilder.append(", target = ");
    resultBuilder.append(this.getTarget());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
