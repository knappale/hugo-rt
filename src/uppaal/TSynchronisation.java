package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import java.util.Objects;


/**
 * UPPAAL synchronisation
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TSynchronisation implements TAnnotation {
  static enum Direction {
    SEND,
    RECEIVE;

    public String toString() {
      switch (this) {
        case SEND:
          return "!";
        case RECEIVE:
          return "?";
        default:
          return "[unknown]";
      }
    }
  }

  private TChannel.Access access;
  private Direction direction;

  private TSynchronisation(TChannel.Access access, Direction direction) {
    this.access = access;
    this.direction = direction;
  }

  /**
   * Create a sending synchronisation.
   * 
   * @param access synchronisation channel access
   * @return sending synchronisation
   */
  static TSynchronisation send(TChannel.Access access) {
    return new TSynchronisation(access, Direction.SEND);
  }

  /**
   * Create a receiving synchronisation.
   * 
   * @param access synchronisation channel access
   * @return receiving synchronisation
   */
  static TSynchronisation receive(TChannel.Access access) {
    return new TSynchronisation(access, Direction.RECEIVE);
  }

  /**
   * Determine whether the direction of the synchronisation is sending
   *
   * @return whether synchronisation is sending
   */
  public boolean isSend() {
    return this.direction == Direction.SEND;
  }

  /**
   * Determine whether the direction of the synchronisation is receiving
   *
   * @return whether synchronisation is sending
   */
  public boolean isReceive() {
    return this.direction == Direction.RECEIVE;
  }

  @Override
  public void accept(TAnnotation.Visitor visitor) {
    visitor.onSynchronisation(this);
  }

  @Override
  public String annotation() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.access.expression());
    resultBuilder.append(this.direction);

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.access);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      TSynchronisation other = (TSynchronisation)object;
      return Objects.equals(this.access, other.access) &&
             this.direction == other.direction;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Synchronisation [");
    resultBuilder.append(this.annotation());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
