package uppaal;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * UPPAAL constant
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TConstant extends TField {
  private TExpression value;

  public TConstant(TDataType type, String name, TExpression value) {
    super(type, name);
    this.value = value;
  }

  public TConstant(String name, TExpression value) {
    super(value.getType(), name);
    this.value = value;
  }

  @Override
  public TExpression getInitialValue() {
    return this.value;
  }

  public static class Access extends TField.Access {
    Access(TConstant constant) {
      super(constant);
    }

    Access(Access access, TField.Accessor accessor) {
      super(access, accessor);
    }

    @Override
    public Access access(Accessor accessor, @NonNull Accessor ... remainingAccessors) {
      var access = new Access(this, accessor);
      for (var remainingAccessor : remainingAccessors)
        access = new Access(access, remainingAccessor);
      return access;
    }
  }

  @Override
  public Access access() {
    return new Access(this);
  }

  /**
   * @return whether this constant depends on another constant
   */
  public boolean dependsOn(TConstant other) {
    return this.getDependencies(new HashSet<>()).contains(other);
  }

  /**
   * @return set of declarations this constant (transitively) depends on,
   * excluding the set visited
   */
  private Set<TDeclaration> getDependencies(Set<TDeclaration> visited) {
    visited.add(this);
    Set<TDeclaration> dependencies = new HashSet<>();

    dependencies.addAll(super.getDependencies());
    dependencies.addAll(this.value.getDependencies());

    for (TDeclaration dependence : dependencies) {
      if (!visited.contains(dependence))
	dependencies.addAll(dependence.getDependencies());
    }

    return dependencies;
  }

  @Override
  public String parameter() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("const ");
    resultBuilder.append(this.getType().getNameDeclaration(this.getName()));
    return resultBuilder.toString();
  }

  @Override
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("const ");
    resultBuilder.append(this.getType().getNameDeclaration(this.getName()));
    resultBuilder.append(" = ");
    resultBuilder.append(this.value.expression());
    resultBuilder.append(";");

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.value);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;

    try {
      TConstant other = (TConstant)object;
      return super.equals(other) &&
             Objects.equals(this.value, other.value);
    }
    catch (ClassCastException cce) {
    }
    return false;
  }
  
  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("Constant [type = ");
    resultBuilder.append(this.getType());
    resultBuilder.append(", name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append(", value = ");
    resultBuilder.append(this.value);
    resultBuilder.append("]");

    return resultBuilder.toString();
  }
}
