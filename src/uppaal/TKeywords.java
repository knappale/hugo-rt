package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;

import java.util.HashSet;
import java.util.Set;

import static util.Objects.requireNonNull;


/**
 * UPPAAL keywords
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class TKeywords {
  // Reserved keywords
  public static final String CHAN = "chan";
  public static final String CLOCK = "clock";
  public static final String BOOL = "bool";
  public static final String INT = "int";
  public static final String COMMIT = "commit";
  public static final String CONST = "const";
  public static final String URGENT = "urgent";
  public static final String BROADCAST = "broadcast";
  public static final String HIDE = "hide";
  public static final String INIT = "init";
  public static final String INVARIANT = "invariant";
  public static final String LOCATION = "location";
  public static final String PROCESS = "process";
  public static final String RATE = "rate";
  public static final String STATE = "state";
  public static final String GUARD = "guard";
  public static final String SYNC = "sync";
  public static final String ASSIGN = "assign";
  public static final String SYSTEM = "system";
  public static final String TRANS = "trans";
  public static final String DEADLOCK = "deadlock";
  public static final String AND = "and";
  public static final String OR = "or";
  public static final String NOT = "not";
  public static final String IMPLY = "imply";
  public static final String TRUE = "true";
  public static final String FALSE = "false";
  public static final String QUIT = "quit";

  // Reserved but unused keywords
  public static final String FOR = "for";
  public static final String WHILE = "while";
  public static final String DO = "do";
  public static final String BREAK = "break";
  public static final String CONTINUE = "continue";
  public static final String SWITCH = "switch";
  public static final String IF = "if";
  public static final String ELSE = "else";
  public static final String CASE = "case";
  public static final String DEFAULT = "default";
  public static final String RETURN = "return";
  public static final String TYPEDEF = "typedef";
  public static final String STRUCT = "struct";
  public static final String BEFORE_UPDATE = "before_update";
  public static final String AFTER_UPDATE = "after_update";

  private static String[] keywords = {
    CHAN, CLOCK, BOOL, INT, COMMIT, CONST, URGENT, BROADCAST, HIDE, INIT,
    INVARIANT, LOCATION, PROCESS, RATE, STATE, GUARD, SYNC, ASSIGN, SYSTEM,
    TRANS, DEADLOCK, AND, OR, NOT, IMPLY, TRUE, FALSE, QUIT, FOR, WHILE,
    DO, BREAK, CONTINUE, SWITCH, IF, ELSE, CASE, DEFAULT, RETURN, TYPEDEF,
    STRUCT, BEFORE_UPDATE, AFTER_UPDATE };

  public static boolean isKeyword(String word) {
    for (int i = 0; i < keywords.length; i++) {
      if (requireNonNull(keywords[i]).equals(word))
        return true;
    }
    return false;
  }

  public static Set<String> getKeywords() {
    Set<String> result = new HashSet<>();
    for (int i = 0; i < keywords.length; i++)
      result.add(requireNonNull(keywords[i]));
    return result;
  }
}
