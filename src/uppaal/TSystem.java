package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * UPPAAL system
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TSystem extends TFrame {
  private List<TTemplate> templates = new ArrayList<>();
  private List<TProcess> processes = new ArrayList<>();
  private List<TQuery> queries = new ArrayList<>();

  public interface Visitor {
    public void onSystem(List<TTemplate> templates);
    public void onTemplate(String name, List<TState> states, List<TTransition> transitions);
    public void onState(String name);
    public void onTransition(TState source, TState target);
  }
  
  /**
   * Create a new system.
   */    
  public TSystem() {
  }

  /**
   * Add a template process to this system.
   *
   * @param template UPPAAL template process
   */
  public void addTemplate(TTemplate template) {
    templates.add(template);
  }

  /**
   * Add a collection of template processes to this system.
   *
   * @param templates collection of UPPAAL template processes
   */
  public void addTemplates(Collection<TTemplate> templates) {
    for (TTemplate template : templates)
      this.addTemplate(template);
  }

  /**
   * Add a process to this system.
   *
   * @param process UPPAAL process
   */
  public void addProcess(TProcess process) {
    processes.add(process);
  }

  /**
   * Add a collection of processes to this system.
   *
   * @param processes collection of UPPAAL processes
   */
  public void addProcesses(Collection<TProcess> processes) {
    for (TProcess process : processes)
      this.addProcess(process);
  }

  /**
   * @return system's processes
   */
  public List<TProcess> getProcesses() {
    return this.processes;
  }

  /**
   * Add a query to this system.
   *
   * @param query UPPAAL query
   */
  public void addQuery(TQuery query) {
    this.queries.add(query);
  }

  /**
   * @return system's queries
   */
  public List<TQuery> getQueries() {
    return this.queries;
  }

  /**
   * Accept a visitor.
   *
   * @param visitor system visitor
   */
  public void accept(Visitor visitor) {
    visitor.onSystem(this.templates);
  }

  /**
   * @return system's representation
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    boolean pending = false;

    var frameDeclarations = super.declaration("");
    if (!frameDeclarations.isEmpty())
      pending = true;
    resultBuilder.append(frameDeclarations);

    if (!this.templates.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      pending = true;
      String sep = "";
      for (var template : this.templates) {
        resultBuilder.append(sep);
        resultBuilder.append(template.declaration());
        sep = "\n\n";
      }
      resultBuilder.append("\n");
    }

    if (!this.processes.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      pending = true;

      String sep = "";
      for (var process : this.processes) {
        resultBuilder.append(sep);
        resultBuilder.append(process.declaration());
        sep = "\n";
      }
      resultBuilder.append("\n\n");

      sep = "system ";
      for (var process : this.processes) {
        resultBuilder.append(sep);
        resultBuilder.append(process.getName());
        sep = ",\n       ";
      }
      resultBuilder.append(";\n\n");
    }
    
    return resultBuilder.toString();
  }

  /**
   * @return system's representation in XML format
   */
  public String declarationXML() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("<nta>\n");

    resultBuilder.append("<declaration>\n");
    resultBuilder.append(Formatter.toXML(super.declaration("")));
    resultBuilder.append("</declaration>\n");

    for (TTemplate template : this.templates) {
      resultBuilder.append(template.declarationXML());
      resultBuilder.append("\n");
    }

    if (!this.processes.isEmpty()) {
      resultBuilder.append("<system>\n");
      for (TProcess process : this.processes) {
        resultBuilder.append(process.declaration());
        resultBuilder.append("\n");
      }

      String sep = "system ";
      for (TProcess process : this.processes) {
        resultBuilder.append(sep);
        resultBuilder.append(process.getName());
        sep = ", ";
      }
      resultBuilder.append(";\n");
      resultBuilder.append("</system>\n");
    }
    
    resultBuilder.append("</nta>");

    return resultBuilder.toString();
  }
}
