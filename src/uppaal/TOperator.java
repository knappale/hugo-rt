package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * UPPAAL operators
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public enum TOperator {
  UMINUS,
  ADD,
  SUB,
  MULT,
  DIV,
  MOD,
  LT,
  LEQ,
  EQ,
  NEQ,
  GEQ,
  GT,
  TRUE,
  FALSE,
  UNEG,
  AND,
  OR,
  LNEG,
  LAND,
  LOR,
  LIMPLY,
  LDEADLOCK;

  public String getOpName() {
    switch (this) {
      case UMINUS: return "-";
      case ADD: return "+";
      case SUB: return "-";
      case MULT: return "*";
      case DIV: return "/";
      case MOD: return "%";
      case LT: return " < ";
      case LEQ: return " <= ";
      case EQ: return " == ";
      case NEQ: return " != ";
      case GEQ: return " >= ";
      case GT: return " > ";
      case TRUE: return "true";
      case FALSE: return "false";
      case UNEG: return "!";
      case AND: return " && ";
      case OR: return " || ";
      case LNEG: return "not ";
      case LAND: return " and ";
      case LOR: return " or ";
      case LIMPLY: return " imply ";
      case LDEADLOCK: return "deadlock";
      default: return "[unknown]";
    }
  }

  public String getOpNameXML() {
    switch (this) {
      case UMINUS: return "-";
      case ADD: return "+";
      case SUB: return "-";
      case MULT: return "*";
      case DIV: return "/";
      case MOD: return "%";
      case LT: return " &lt; ";
      case LEQ: return " &lt;= ";
      case EQ: return " == ";
      case NEQ: return " != ";
      case GEQ: return " &gt;= ";
      case GT: return " &gt; ";
      case TRUE: return "true";
      case FALSE: return "false";
      case UNEG: return "!";
      case AND: return " &amp;&amp; ";
      case OR: return " || ";
      case LNEG: return "not ";
      case LAND: return " and ";
      case LOR: return " or ";
      case LIMPLY: return " imply ";
      case LDEADLOCK: return "deadlock";
      default: return "[unknown]";
    }
  }

  public static boolean precedes(TOperator op1, TOperator op2) {
    return (op1.getOpPrecedence() > op2.getOpPrecedence()) ||
           (op1 == op2 && op1.isAssociative());
  }

  private int getOpPrecedence() {
    switch (this) {
      case TRUE:
      case FALSE:
        return 24;
      case UMINUS:
      case UNEG:
        return 22;
      case MULT:
      case DIV:
      case MOD:
        return 20;
      case SUB:
        return 19;
      case ADD:
        return 18;
      case LT:
      case LEQ:
      case GEQ:
      case GT:
        return 16;
      case EQ:
      case NEQ:
        return 14;
      case AND:
      case OR:
        return 10;
      case LDEADLOCK:
        return 8;
      case LNEG:
        return 6;
      case LAND:
      case LOR:
        return 4;
      case LIMPLY:
        return 2;
      default:
        return 0;
    }
  }

  public boolean isAssociative() {
    switch (this) {
      case UMINUS:
      case UNEG:
      case MULT:
      case ADD:
      case AND:
      case OR:
      case LNEG:
      case LAND:
      case LOR:
        return true;
      //$CASES-OMITTED$
      default:
        return false;
    }
  }

  public int getIntValue(int value) {
    switch (this) {
      case UMINUS:
        return -value;
      case UNEG:
        return (value != 0 ? 0 : 1);
      //$CASES-OMITTED$
      default:
        return 0;
    }
  }

  public int getIntValue(int lvalue, int rvalue) {
    switch (this) {
      case ADD:
        return lvalue+rvalue;
      case SUB:
        return lvalue-rvalue;
      case MULT:
        return lvalue*rvalue;
      case DIV:
        return (rvalue == 0 ? 0 : lvalue/rvalue);
      case MOD:
        return (rvalue == 0 ? 0 : lvalue%rvalue);
      case LT:
        return (lvalue < rvalue ? 0 : 1);
      case LEQ:
        return (lvalue <= rvalue ? 0 : 1);
      case EQ:
        return (lvalue == rvalue ? 0 : 1);
      case NEQ:
        return (lvalue != rvalue ? 0 : 1);
      case GEQ:
        return (lvalue >= rvalue ? 0 : 1);
      case GT:
        return (lvalue > rvalue ? 0 : 1);
      case AND:
        return ((lvalue != 0) && (rvalue != 0) ? 0 : 1);
      case OR:
        return ((lvalue != 0) || (rvalue != 0) ? 0 : 1);
      //$CASES-OMITTED$
      default:
        return 0;
    }
  }
}
