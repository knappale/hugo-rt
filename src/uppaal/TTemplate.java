package uppaal;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import uppaal.builder.TSubTemplate;
import util.Formatter;


/**
 * UPPAAL process template
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TTemplate extends TFrame {
  private String name;
  private List<TParameter> tParameters = new ArrayList<>();
  private List<TState> tStates = new ArrayList<>();   
  private List<TTransition> tTransitions = new ArrayList<>();

  public static interface StateProducer {
    public TState getNewState();
  }

  /**
   * Create an UPPAAL process template.
   *
   * @param name name of the process template
   */
  public TTemplate(String name) {
    this.name = name;
  }
 
  /**
   * @return process template's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Add a parameter to this process template.
   *
   * @param tParameter UPPAAL parameter
   */
  public void addParameter(TParameter tParameter) {
    this.tParameters.add(tParameter);
  }

  /**
   * Add a collection of parameters to this process.
   *
   * @param tParameters collection of UPPAAL parameters
   */
  public void addParameters(Collection<TParameter> tParameters) {
    for (TParameter tParameter : tParameters)
      this.addParameter(tParameter);
  }

  /**
   * @return process's parameters
   */
  public List<TParameter> getParameters() {
    return this.tParameters;
  }

  /**
   * Add a state to this process.
   *
   * @param tState an UPPAAL state
   */
  public void addState(TState tState) {
    if (!this.tStates.contains(tState))
      this.tStates.add(tState);
  }
    
  /**
   * Add a collection of states to this process.
   *
   * @param tStates collection of UPPAAL states
   */
  public void addStates(Collection<TState> tStates) {
    for (TState tState : tStates)
      this.addState(tState);
  }

  /**
   * @return process's states
   */
  public List<TState> getStates() {
    return this.tStates;
  }

  /**
   * Add a transition to this process.
   *
   * @param tTransition an UPPAAL transition
   */
  public void addTransition(TTransition tTransition) {
    if (!this.tTransitions.contains(tTransition))
      this.tTransitions.add(tTransition);
  }

  /**
   * Add a collection of transitions to this process.
   *
   * @param transitions collection of UPPAAL transitions
   */
  public void addTransitions(Collection<TTransition> transitions) {
    for (TTransition transition : transitions) {
      addTransition(transition);
    }
  }

  /**
   * Add a sub-process to this process
   *
   * @param subProcess a sub-process
   */
  public void addSubProcess(TSubTemplate subProcess) {
    this.addConstants(subProcess.getConstants());
    this.addVariables(subProcess.getVariables());
    this.addClocks(subProcess.getClocks());
    this.addStates(subProcess.getStates());
    this.addTransitions(subProcess.getTransitions());
  }

  /**
   * @return process's initial state, or {@code null} if there is none
   */
  public @Nullable TState getInitialState() {
    for (TState tState : this.tStates) {
      if (tState.isInitial())
	return tState;
    }
    return null;
  }

  /**
   * @return process's committed states
   */
  public List<TState> getCommittedStates() {
    List<TState> committedTStates = new ArrayList<>();
    for (TState tState : this.tStates) {
      if (tState.isCommitted())
	committedTStates.add(tState);
    }
    return committedTStates;
  }

  /**
   * @return process's urgent states
   */
  public List<TState> getUrgentStates() {
    List<TState> urgentTStates = new ArrayList<>();
    for (TState tState : this.tStates) {
      if (tState.isUrgent())
        urgentTStates.add(tState);
    }
    return urgentTStates;
  }

  /**
   * Accept a visitor.
   *
   * @param visitor UPPAAL visitor
   */
  public void accept(TSystem.Visitor visitor) {
    visitor.onTemplate(this.getName(), this.getStates(), this.tTransitions);
  }

  /**
   * @return process's representation in TA-format
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("process ");
    resultBuilder.append(getName());
    resultBuilder.append(Formatter.tuple(this.tParameters, tParameter -> tParameter.parameter()));
    resultBuilder.append(" {\n");

    var frameDeclarations = super.declaration("  ");
    var pending = !frameDeclarations.isEmpty();
    resultBuilder.append(frameDeclarations);

    if (!this.tStates.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      pending = true;
      resultBuilder.append("  state\n\n");
      @NonNull TState[] statesArray = this.tStates.toArray(new @NonNull TState[0]);
      Arrays.sort(statesArray, new Comparator<TState>() {
                                 public int compare(TState s1, TState s2) {
                                   return (s1.getName().compareTo(s2.getName()));
                                 }
                               });
      List<TState> states = Arrays.asList(statesArray);
      String sep = "";
      for (TState tState : states) {
        resultBuilder.append(sep);
        resultBuilder.append(tState.declaration("  "));
        sep = ",\n";
      }
      resultBuilder.append(";\n");
    }

    if (!this.getCommittedStates().isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      pending = true;
      resultBuilder.append("  commit ");
      @NonNull TState[] committedStatesArray = this.getCommittedStates().toArray(new @NonNull TState[0]);
      Arrays.sort(committedStatesArray, new Comparator<TState>() {
                                          public int compare(TState s1, TState s2) {
                                            return (s1.getName().compareTo(s2.getName()));
                                          }
                                        });
      List<TState> committedStates = Arrays.asList(committedStatesArray);
      String sep = "";
      for (TState committedTState : committedStates) {
        resultBuilder.append(sep);
        resultBuilder.append(committedTState.getName());
        sep = ",\n         ";
      }
      resultBuilder.append(";\n");
    }

    if (!this.getUrgentStates().isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      pending = true;
      resultBuilder.append("  urgent ");
      @NonNull TState[] urgentStatesArray = this.getUrgentStates().toArray(new @NonNull TState[0]);
      Arrays.sort(urgentStatesArray, new Comparator<TState>() {
                                       public int compare(TState s1, TState s2) {
                                         return (s1.getName().compareTo(s2.getName()));
                                       }
                                     });
      List<TState> urgentStates = Arrays.asList(urgentStatesArray);
      String sep = "";
      for (TState urgentTState : urgentStates) {
        resultBuilder.append(sep);
        resultBuilder.append(urgentTState.getName());
        sep = ",\n         ";
      }
      resultBuilder.append(";\n");
    }

    TState initialState = getInitialState();
    if (initialState != null) {
      if (pending)
        resultBuilder.append("\n");
      pending = true;
      resultBuilder.append("  init ");
      resultBuilder.append(initialState.getName());
      resultBuilder.append(";\n");
    }

    if (!this.tTransitions.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      resultBuilder.append("  trans\n\n");
      String sep = "";
      for (TTransition tTransition : this.tTransitions) {
        resultBuilder.append(sep);
        resultBuilder.append(tTransition.declaration("  "));
        sep = ",\n";
      }
      resultBuilder.append(";\n");
    }
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  /**
   * @return process's representation
   */
  public String declarationXML() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("<template>\n");
    resultBuilder.append("<name>");
    resultBuilder.append(getName());
    resultBuilder.append("</name>\n");
    if (!this.tParameters.isEmpty()) {
      resultBuilder.append("<parameter>\n");
      String sep = "";
      for (TParameter parameter : getParameters()) {
        resultBuilder.append(sep);
        resultBuilder.append(Formatter.toXML(parameter.parameter()));
        sep = ",\n";
      }
      resultBuilder.append("\n");
      resultBuilder.append("</parameter>\n");
    }

    resultBuilder.append("<declaration>\n");
    resultBuilder.append(Formatter.toXML(super.declaration("")));
    resultBuilder.append("</declaration>\n");

    if (!this.tStates.isEmpty()) {
      @NonNull TState[] statesArray = this.tStates.toArray(new @NonNull TState[0]);
      Arrays.sort(statesArray, new Comparator<TState>() {
        public int compare(TState s1, TState s2) {
          return (s1.getName().compareTo(s2.getName()));
        }
      });
      for (TState tState : Arrays.asList(statesArray)) {
        resultBuilder.append(tState.declarationXML());
        resultBuilder.append("\n");
      }
    }

    TState initialState = getInitialState();
    if (initialState != null) {
      resultBuilder.append("<init ref=\"");
      resultBuilder.append(initialState.getName());
      resultBuilder.append("\"/>\n");
    }

    for (TTransition transition : this.tTransitions) {
      resultBuilder.append(transition.declarationXML());
      resultBuilder.append("\n");
    }

    resultBuilder.append("</template>");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Template [name =");
    resultBuilder.append(this.name);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
