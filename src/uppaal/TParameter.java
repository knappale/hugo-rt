package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * UPPAAL process template parameter
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public interface TParameter {
  /**
   * @return parameter's name
   */
  public String getName();

  /**
   * @return parameter representation
   */
  public String parameter();
}
