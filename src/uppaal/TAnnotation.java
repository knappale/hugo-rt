package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Annotation of a UPPAAL transition
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public interface TAnnotation {
  public interface Visitor {
    public void onSynchronisation(TSynchronisation synchronisation);
    public void onGuard(TGuard guard);
    public void onUpdate(TUpdate update);
  }

  /**
   * @return annotation representation
   */
  public String annotation();

  /**
   * Accept a visitor.
   *
   * @param visitor an annotation visitor
   */
  public void accept(Visitor visitor);
}
