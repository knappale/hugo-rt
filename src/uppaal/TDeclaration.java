package uppaal;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * UPPAAL declaration
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public interface TDeclaration extends TParameter {
  /**
   * @return declaration representation
   */
  public String declaration();

  /**
   * @return set of declarations this declaration depends on
   */
  public Set<TDeclaration> getDependencies();
}
