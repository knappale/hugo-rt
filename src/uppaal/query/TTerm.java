package uppaal.query;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uppaal.TClock;
import uppaal.TDeclaration;
import uppaal.TField;
import uppaal.TOperator;
import uppaal.TProcess;
import uppaal.TState;

import static util.Objects.requireNonNull;
import static util.Formatter.parenthesised;


/**
 * UPPAAL query formula term
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TTerm implements TField.ArrayAccessor {
  enum Kind {
    BOOLEAN,
    INTEGER,
    ACCESS,
    STATE,
    CLOCK,
    UNARY,
    BINARY;
  }

  boolean hasPrecedence(TTerm other) {
    if ((this.kind == Kind.UNARY || this.kind == Kind.BINARY) &&
        (other.kind == Kind.UNARY || other.kind == Kind.BINARY))
      return TOperator.precedes(requireNonNull(this.op), requireNonNull(other.op));
    return true;
  }

  private static final TTerm TRUETERM;
  private static final TTerm FALSETERM;
  static {
    TRUETERM = new TTerm(Kind.BOOLEAN);
    TRUETERM.op = TOperator.TRUE;
    FALSETERM = new TTerm(Kind.BOOLEAN);
    FALSETERM.op = TOperator.FALSE;
  }

  private Kind kind;
  private @Nullable TProcess process = null;
  private TField.@Nullable Access access = null;
  private @Nullable TState state = null;
  private @Nullable TClock clock = null;
  private int num = 0;
  private @Nullable TTerm leftTerm = null;
  private @Nullable TOperator op;
  private @Nullable TTerm rightTerm = null;

  private TTerm(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create a term which represents a boolean constant.
   *
   * @param booleanConstant boolean constant
   * @return a term
   */
  public static TTerm boolConst(boolean booleanConstant) {
    return (booleanConstant ? TRUETERM : FALSETERM);
  }

  /**
   * Create a term which represents <CODE>true</CODE>.
   *
   * @return a term
   */
  public static TTerm trueConst() {
    return TRUETERM;
  }

  /**
   * Create a term which represents <CODE>false</CODE>.
   *
   * @return a term
   */
  public static TTerm falseConst() {
    return FALSETERM;
  }

  /**
   * Create a term representing an integer constant.
   *
   * @param process constant's owning process
   * @param constant constant
   * @return a term
   */
  public static TTerm intConst(int num) {
    TTerm e = new TTerm(Kind.INTEGER);
    e.num = num;
    return e;
  }

  /**
   * Create a term representing a global field access.
   *
   * @param access field access
   * @return a term
   */
  public static TTerm field(TField.Access access) {
    TTerm e = new TTerm(Kind.ACCESS);
    e.process = null;
    e.access = access;
    return e;
  }

  /**
   * Create a term representing a process' field access.
   *
   * @param process field's owning process
   * @param access field access
   * @return a term
   */
  public static TTerm field(TProcess process, TField.Access access) {
    TTerm e = new TTerm(Kind.ACCESS);
    e.process = process;
    e.access = access;
    return e;
  }

  /**
   * Create a term representing a process' state access.
   *
   * @param process process
   * @param access field
   * @return a term
   */
  public static TTerm state(TProcess process, TState state) {
    TTerm e = new TTerm(Kind.STATE);
    e.process = process;
    e.state = state;
    return e;
  }

  /**
   * Create a term representing a clock.
   *
   * @param process clock's owning process
   * @param clock clock
   * @return a term
   */
  public static TTerm clock(TProcess process, TClock clock) {
    TTerm e = new TTerm(Kind.CLOCK);
    e.process = process;
    e.clock = clock;
    return e;
  }

  /**
   * Create a term representing a unary operation.
   *
   * @pre (op == TOperator.UMINUS || op == TOperator.UNEG) && term != null
   */
  public static TTerm unary(TOperator op, TTerm term) {
    if (op == TOperator.UNEG && term.op == TOperator.UNEG)
      return requireNonNull(term.leftTerm);
    if (op == TOperator.UMINUS && term.op == TOperator.UMINUS)
      return requireNonNull(term.leftTerm);
    TTerm e = new TTerm(Kind.UNARY);
    e.op = op;
    e.leftTerm = term;
    return e;
  }

  /**
   * Create a term representing a negation.
   *
   * @param term term
   * @return a term
   */
  public static TTerm neg(TTerm term) {
    return TTerm.unary(TOperator.UNEG, term);
  }

  /**
   * Create a term representing an arithmetical term.
   *
   * @param leftTerm left term
   * @param op binary operation
   * @param rightTerm right term
   */
  public static TTerm arithmetical(TTerm leftTerm, TOperator op, TTerm rightTerm) {
    TTerm e = new TTerm(Kind.BINARY);
    e.leftTerm = leftTerm;
    e.op = op;
    e.rightTerm = rightTerm;
    return e;
  }

  /**
   * Create a term representing a relational term.
   *
   * @param leftTerm left term
   * @param op binary relation
   * @param rightTerm right term
   */
  public static TTerm relational(TTerm leftTerm, TOperator op, TTerm rightTerm) {
    TTerm e = new TTerm(Kind.BINARY);
    e.leftTerm = leftTerm;
    e.op = op;
    e.rightTerm = rightTerm;
    return e;
  }

  /**
   * Create a term representing an equational term.
   *
   * @param leftTerm left term
   * @param rightTerm right term
   */
  public static TTerm eq(TTerm leftTerm, TTerm rightTerm) {
    return relational(leftTerm, TOperator.EQ, rightTerm);
  }

  /**
   * Create a term representing an inequational term.
   *
   * @param leftTerm left term
   * @param rightTerm right term
   */
  public static TTerm neq(TTerm leftTerm, TTerm rightTerm) {
    return relational(leftTerm, TOperator.NEQ, rightTerm);
  }

  /**
   * Create a term representing a junctional term.
   *
   * @param leftTerm left term
   * @param op binary relation
   * @param rightTerm right term
   */
  public static TTerm junctional(TTerm leftTerm, TOperator op, TTerm rightTerm) {
    if (op == TOperator.AND && leftTerm.equals(trueConst()))
      return rightTerm;
    if (op == TOperator.AND && rightTerm.equals(trueConst()))
      return leftTerm;
    if (op == TOperator.OR && leftTerm.equals(falseConst()))
      return rightTerm;
    if (op == TOperator.OR && rightTerm.equals(falseConst()))
      return leftTerm;
    TTerm e = new TTerm(Kind.BINARY);
    e.leftTerm = leftTerm;
    e.op = op;
    e.rightTerm = rightTerm;
    return e;
  }

  /**
   * Create a term representing a conjunctive term.
   *
   * @param leftTerm left term
   * @param rightTerm right term
   */
  public static TTerm and(TTerm leftTerm, TTerm rightTerm) {
    return junctional(leftTerm, TOperator.AND, rightTerm);
  }

  /**
   * Create a term representing a disjunctive term.
   *
   * @param leftTerm left term
   * @param rightTerm right term
   */
  public static TTerm or(TTerm leftTerm, TTerm rightTerm) {
    return junctional(leftTerm, TOperator.OR, rightTerm);
  }

  @Override
  public int getIntValue() {
    switch (this.kind) {
      case BOOLEAN:
        return requireNonNull(this.op).equals(TOperator.TRUE) ? 1 : 0;

      case INTEGER:
        return this.num;

      case UNARY:
        return requireNonNull(this.op).getIntValue(requireNonNull(this.leftTerm).getIntValue());

      case BINARY:
        return requireNonNull(this.op).getIntValue(requireNonNull(this.leftTerm).getIntValue(),
                                                   requireNonNull(this.rightTerm).getIntValue());

      //$CASES-OMITTED$
      default:
    }

    return 0;
  }

  /**
   * @return set of declarations this term depends on
   */
  @Override
  public Set<TDeclaration> getDependencies() {
    Set<TDeclaration> dependencies = new HashSet<>();
    var leftTerm = this.leftTerm;
    if (leftTerm != null)
      dependencies.addAll(leftTerm.getDependencies());
    var rightTerm = this.rightTerm;
    if (rightTerm != null)
      dependencies.addAll(rightTerm.getDependencies());
    return dependencies;
  }

  /**
   * UPPAAL currently does not support query formulae in XML format,
   * so this is not in use.  If this feature should become available,
   * rename declaration() to declarationTA() to be consistent.
   * In particular, operators like "<" or ">" do not
   * have to be escaped as XML-entities "&lt;" or "&gt;".
   *
   * @return term representation in XML-format
   * @TODO UPPAAL now supports queries in XML
   */
  @Override
  public String expression() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
    case BOOLEAN: {
      resultBuilder.append(requireNonNull(this.op).getOpName());
      return resultBuilder.toString();
    }

    case INTEGER: {
      resultBuilder.append(this.num);
      return resultBuilder.toString();
    }

    case ACCESS: {
      if (process != null) {
        resultBuilder.append(this.process.getName());
        resultBuilder.append(".");
      }
      resultBuilder.append(requireNonNull(this.access).expression());
      if (leftTerm != null) {
        resultBuilder.append("[");
        resultBuilder.append(requireNonNull(this.leftTerm).expression());
        resultBuilder.append("]");        
      }
      return resultBuilder.toString();
    }

    case STATE: {
      if (process != null) {
        resultBuilder.append(this.process.getName());
        resultBuilder.append(".");
      }
      resultBuilder.append(requireNonNull(this.state).getName());
      return resultBuilder.toString();
    }

    case CLOCK: {
      if (process != null) {
        resultBuilder.append(this.process.getName());
        resultBuilder.append(".");
      }
      resultBuilder.append(requireNonNull(this.clock).getName());
      return resultBuilder.toString();
    }

    case UNARY: {
      resultBuilder.append(requireNonNull(this.op).getOpName());
      resultBuilder.append(parenthesised(requireNonNull(this.leftTerm), e -> e.hasPrecedence(this), e -> e.expression()));
      return resultBuilder.toString();
    }

    case BINARY:
      resultBuilder.append(parenthesised(requireNonNull(this.leftTerm), e -> e.hasPrecedence(this), e -> e.expression()));
      resultBuilder.append(requireNonNull(this.op).getOpName());
      resultBuilder.append(parenthesised(requireNonNull(this.rightTerm), e -> e.hasPrecedence(this), e -> e.expression()));
      return resultBuilder.toString();
    }

    return resultBuilder.toString();
  }

  /**
   * @return term's hash code
   */
  public int hashCode() {
    return Objects.hash(this.process,
                        this.access,
                        this.state,
                        this.clock,
                        this.leftTerm,
                        this.rightTerm);
  }
 
  /**
   * @return whether object is equal to this term
   */
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      TTerm other = (TTerm)object;
      return this.kind == other.kind &&
             Objects.equals(this.process, other.process) &&
             Objects.equals(this.access, other.access) &&
             Objects.equals(this.state, other.state) &&
             Objects.equals(this.clock, other.clock) &&
             Objects.equals(this.leftTerm, other.leftTerm) &&
             Objects.equals(this.rightTerm, other.rightTerm) &&
             this.op == other.op &&
             this.num == other.num;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  /**
   * @return term representation as a string
   */
  public String toString() {
    return "Term " + this.expression();
  }
}
