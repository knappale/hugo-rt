package uppaal.query;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uppaal.TOperator;
import uppaal.TProcess;
import uppaal.TState;
import util.Formatter;

import static util.Objects.requireNonNull;


/**
 * UPPAAL query state formula
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TFormula {
  enum Kind {
    TERM,
    DEADLOCK,
    UNARY,
    JUNCTIONAL;
  }

  private boolean hasPrecedence(TFormula other) {
    if ((this.kind == Kind.UNARY || this.kind == Kind.JUNCTIONAL) &&
        (other.kind == Kind.UNARY || other.kind == Kind.JUNCTIONAL))
      return TOperator.precedes(requireNonNull(this.op), requireNonNull(other.op));      
    return true;
  }

  private static final TFormula TRUEFORMULA;
  private static final TFormula FALSEFORMULA;
  static {
    TRUEFORMULA = new TFormula(Kind.TERM);
    TRUEFORMULA.term = TTerm.trueConst();
    FALSEFORMULA = new TFormula(Kind.TERM);
    FALSEFORMULA.term = TTerm.falseConst();
  }

  private Kind kind;
  private @Nullable TProcess process = null;
  private @Nullable TState state = null;
  private @Nullable TTerm term = null;
  private @Nullable TFormula left = null;
  private @Nullable TOperator op = null;
  private @Nullable TFormula right = null;

  private TFormula(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create a formula representing <CODE>false</CODE>.
   *
   * @return a formula
   */
  public static TFormula falseConst() {
    return FALSEFORMULA;
  }

  /**
   * Create a formula representing <CODE>true</CODE>.
   *
   * @return a formula
   */
  public static TFormula trueConst() {
    return TRUEFORMULA;
  }

  /**
   * Create a formula representing a term.
   *
   * @return a formula
   */
  public static TFormula term(TTerm term) {
    TFormula f = new TFormula(Kind.TERM);
    f.term = term;
    return f;
  }

  /**
   * Create a formula representing <CODE>deadlock</CODE>.
   *
   * @return a formula
   */
  public static TFormula deadlock() {
    TFormula f = new TFormula(Kind.DEADLOCK);
    return f;
  }

  /**
   * Create a formula representing a negation.
   *
   * @param formula a query formula
   * @return a formula
   */
  public static TFormula neg(TFormula formula) {
    TFormula f = new TFormula(Kind.UNARY);
    f.op = TOperator.LNEG;
    f.left = formula;
    return f;
  }

  /**
   * Create a formula representing a junction.
   *
   * @param left a query formula
   * @param op junctional operator
   * @param right a query formula
   * @return a formula
   */
  public static TFormula junctional(TFormula left, TOperator op, TFormula right) {
    switch (op) {
      case LAND:
        if (left.equals(trueConst()))
          return right;
        if (right.equals(trueConst()))
          return left;
        if (left.equals(falseConst()))
          return falseConst();
        if (right.equals(falseConst()))
          return falseConst();
        break;
      case LOR:
        if (left.equals(falseConst()))
          return right;
        if (right.equals(falseConst()))
          return left;
        if (left.equals(trueConst()))
          return trueConst();
        if (right.equals(trueConst()))
          return trueConst();
        break;
      case LIMPLY:
        if (left.equals(falseConst()))
          return trueConst();
        if (left.equals(trueConst()))
          return right;
        break;
      //$CASES-OMITTED$
      default:
    }
    TFormula f = new TFormula(Kind.JUNCTIONAL);
    f.left = left;
    f.op = op;
    f.right = right;
    return f;
  }

  /**
   * Create a formula representing a conjunction
   *
   * @param left a query formula
   * @param right a query formula
   * @return a formula
   */
  public static TFormula and(TFormula left, TFormula right) {
    return junctional(left, TOperator.LAND, right);
  }

  /**
   * Create a formula representing a disjunction
   *
   * @param left a query formula
   * @param right a query formula
   * @return a formula
   */
  public static TFormula or(TFormula left, TFormula right) {
    return junctional(left, TOperator.LOR, right);
  }

  /**
   * Create a formula representing an implication
   *
   * @param left a query formula
   * @param right a query formula
   * @return a formula
   */
  public static TFormula imply(TFormula left, TFormula right) {
    return junctional(left, TOperator.LIMPLY, right);
  }

  /**
   * UPPAAL currently does not support query formulae in XML format,
   * so this is not in use.  If this feature should become available,
   * rename declaration() to declarationTA() to be consistent.
   * In particular, operators like "<" or ">" do not
   * have to be escaped as XML-entities "&lt;" or "&gt;".
   *
   * @return formula representation in XML-format
   *
   * TODO (AK130525) UPPAAL now supports queries in XML
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case TERM: {
        resultBuilder.append(requireNonNull(this.term).expression());
        return resultBuilder.toString();
      }

      case DEADLOCK: {
        resultBuilder.append("deadlock");
        return resultBuilder.toString();
      }
    
      case UNARY: {
        resultBuilder.append(requireNonNull(this.op).getOpName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.left), f -> f.hasPrecedence(this), TFormula::declaration));
        return resultBuilder.toString();        
      }

      case JUNCTIONAL: {
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.left), f -> f.hasPrecedence(this), TFormula::declaration));
        resultBuilder.append(requireNonNull(this.op).getOpName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.right), f -> f.hasPrecedence(this), TFormula::declaration));
        return resultBuilder.toString();
      }
    }

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.process,
                        this.state,
                        this.left,
	                this.right,
                        this.term);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      TFormula other = (TFormula)object;
      return (this.kind == other.kind &&
              Objects.equals(this.process, other.process) &&
              Objects.equals(this.state, other.state) &&
              Objects.equals(this.left, other.left) &&
              Objects.equals(this.right, other.right) &&
              Objects.equals(this.term, other.term) &&
              this.op == other.op);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return "Formula " + this.declaration();
  }
}
