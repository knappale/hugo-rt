package uppaal;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uppaal.query.TFormula;


/**
 * UPPAAL query
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TQuery {
  enum Modality {
    AG,
    AF,
    EG,
    EF;

    public String getName() {
      switch (this) {
        case AG: return "A[]";
        case AF: return "A<>";
        case EG: return "E[]";
        case EF: return "E<>";
        default: return "[unknown]";
      }
    }
  }

  private Modality modality;
  private TFormula formula;

  private TQuery(Modality modality, TFormula formula) {
    this.modality = modality;
    this.formula = formula;
  }

  /**
   * Create a query which represents an <CODE>AG</CODE> formula
   *
   * @param formula UPPAAL formula
   * @return an <CODE>AG</CODE> constraint
   */
  public static TQuery ag(TFormula formula) {
    return new TQuery(Modality.AG, formula);
  }

  /**
   * Create a query which represents an <CODE>AF</CODE> formula
   *
   * @param formula UPPAAL formula
   * @return an <CODE>AF</CODE> constraint
   */
  public static TQuery af(TFormula formula) {
    return new TQuery(Modality.AF, formula);
  }

  /**
   * Create a query which represents an <CODE>EG</CODE> formula
   *
   * @param formula UPPAAL formula
   * @return an <CODE>EG</CODE> constraint
   */
  public static TQuery eg(TFormula formula) {
    return new TQuery(Modality.EG, formula);
  }

  /**
   * Create a query which represents an <CODE>EF</CODE> formula
   *
   * @param formula UPPAAL formula
   * @return an <CODE>EF</CODE> constraint
   */
  public static TQuery ef(TFormula formula) {
    return new TQuery(Modality.EF, formula);
  }

  /**
   * @return query representation in Q-format
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.modality.getName());
    resultBuilder.append(" ");
    resultBuilder.append(this.formula.declaration());

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.modality,
                        this.formula);
  }
 
  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      TQuery other = (TQuery)object;
      return this.modality == other.modality &&
             Objects.equals(this.formula, other.formula);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return "Query " + this.declaration();
  }
}
