package uppaal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * UPPAAL conditional
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class TConditional implements TStatement {
  private TExpression condition;
  private TBlock thenBranch;
  private @Nullable TBlock elseBranch = null;

  private TConditional(TExpression condition, TBlock thenBranch) {
    this.condition = condition;
    this.thenBranch = thenBranch;
  }

  public static TConditional ifthen(TExpression condition, TBlock thenBranch) {
    TConditional c = new TConditional(condition, thenBranch);
    return c;
  }

  public static TConditional ifthenelse(TExpression condition, TBlock thenBranch, TBlock elseBranch) {
    TConditional c = new TConditional(condition, thenBranch);
    c.elseBranch = elseBranch;
    return c;
  }

  @Override
  public String statement(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("if (");
    resultBuilder.append(this.condition.expression());
    resultBuilder.append(") ");
    resultBuilder.append(this.thenBranch.statement(prefix));
    TBlock elseBranch = this.elseBranch;
    if (elseBranch != null) {
      resultBuilder.append("\n");
      resultBuilder.append(prefix);
      resultBuilder.append("else ");
      resultBuilder.append(elseBranch.statement(prefix));
    }

    return resultBuilder.toString();
  }
}
