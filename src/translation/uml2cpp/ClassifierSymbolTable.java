package translation.uml2cpp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cpp.CAttribute;
import cpp.CClass;
import cpp.CCode;
import cpp.CEnum;
import cpp.CKeywords;
import cpp.CMethod;
import cpp.CParameter;
import cpp.CPointer;
import cpp.CSystem;
import cpp.CVariableDeclaration;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UClassifier;
import uml.UParameter;
import uml.UReception;
import uml.UType;
import uml.smile.SConstant;
import uml.smile.SPrimitive;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UCompoundTransition;

import static util.Objects.requireNonNull;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class ClassifierSymbolTable extends SymbolTable {
  private static final String DEFAULTCLASSVARIABLEVISIBILITY = CKeywords.PUBLIC;
  public static final String EVENTPOOLNAME = "ep";
  static final String ARGSARRAYNAME = "args";
  public static final String INITMETHODNAME = "init";
  public static final String TERMMETHODNAME = "hasterminated";
  public static final String HANDLEMETHODNAME = "handle";
  public static final String HANDLEDMETHODNAME = "handled";
  static final String RUNMETHODNAME = "run";

  private ModelSymbolTable modelSymbolTable;
  private UClassifier uClassifier;
  private control.Properties properties;

  private Map<UAttribute, CAttribute> attributes = new HashMap<>();
  private Map<CAttribute, String> attributeSetterNames = new HashMap<>();
  private Map<SVariable, CVariableDeclaration> variables = new HashMap<>();
  private @Nullable CMethod initMethod = null;
  private Map<UBehavioural, CMethod> methods = new HashMap<>();
  private Map<UEvent, String> eventMethodNames = new HashMap<>();
  private Map<UVertex, String> stateNames = new HashMap<>();
  private Map<UCompoundTransition, String> transitionNames = new HashMap<>();
  private Map<UEvent, String> eventNames = new HashMap<>();
  private IdentityHashMap<SPrimitive, String> acknowledgementNames = new IdentityHashMap<>();
  private IdentityHashMap<SPrimitive, String> resumptionMethodNames = new IdentityHashMap<>();
  
  private Set<String> variableNames = new HashSet<>();
  private Set<String> methodNames = new HashSet<>();
  private Set<String> attributeNames = new HashSet<>();

  /**
   * Create a new classifier symbol table.
   * 
   * @param uClassifier a UML classifier
   * @return class symbol table for {@link uClassifier}
   */
  ClassifierSymbolTable(UClassifier uClassifier, ModelSymbolTable modelSymbolTable) {
    super(uClassifier.getModel());
    this.uClassifier = uClassifier;
    this.modelSymbolTable = modelSymbolTable;
    this.properties = modelSymbolTable.getProperties();

    this.attributeNames.add(EVENTPOOLNAME);
    // this.methodNames.add(INITMETHODNAME);
    this.methodNames.add(TERMMETHODNAME);
    this.methodNames.add(HANDLEMETHODNAME);
    this.methodNames.add(HANDLEDMETHODNAME);
    this.methodNames.add(RUNMETHODNAME);
  }

  /**
   * Determine the UML classifier of this classifier symbol table.
   * 
   * @return classifier symbol table's underlying UML classifier
   */
  UClassifier getUClassifier() {
    return this.uClassifier;
  }

  /**
   * Determine the UML state machine of this classifier symbol table.
   * 
   * @return classifier symbol table's underlying UML state machine or {@code null} if
   *         no state machine underlies this symbol table
   */
  @Nullable UStateMachine getUStateMachine() {
    try {
      UClass uClass = (UClass)this.uClassifier;
      return uClass.getStateMachine();
    }
    catch (ClassCastException cce) {
      return null;
    }
  }

  @Override
  public control.Properties getProperties() {
    return this.properties;
  }

  /**
   * @return classifier symbol table's underlying C++ class
   */
  public CClass getCClass() {
    return this.modelSymbolTable.getCClass(this.uClassifier);
  }

  /**
   * @return the C++ system that this classifier symbol table works on
   */
  CSystem getCSystem() {
    return this.modelSymbolTable.getCSystem();
  }

  @Override
  public String getTypeName(UType uType) {
    return this.modelSymbolTable.getTypeName(uType);
  }

  /**
   * Determine a C++ expression for accessing the event queue of the C++ class
   * of this classifier symbol table.
   * 
   * @return C++ expression for accessing classifier symbol table's underlying C++ class' event queue
   */
  public String getEventQueueAccess() {
    return EVENTPOOLNAME;
  }

  /**
   * Determine a C++ expression for accessing the current event.
   * 
   * @return C++ expression for accessing the current event
   */
  String getEventAccess() {
    return this.modelSymbolTable.getEventAccess();
  }

  /**
   * Determine the C++ access expression to a UML parameter.
   *
   * @param uParameter a UML parameter
   * @return Java access expression to {@link uParameter}
   */
  String getParameterAccess(UParameter uParameter) {
    if (this.getProperties().isEventBased())
      return this.getParameterName(uParameter.getName());
    return this.getUnboxing(uParameter.getType(), this.getEventAccess() + "->" + "getarg(" + uParameter.getNumber() + ")");
  }

  /**
   * Determine the C++ name for a Smile variable.
   *
   * @param sVariable a Smile variable
   * @return the C++ name for {@link sVariable}
   */
  public String getVariableName(SVariable sVariable) {
    CVariableDeclaration cVariable = this.variables.get(sVariable);
    if (cVariable != null)
      return cVariable.getName();

    String name = uniqueVariableName(sVariable.getName());
    String type = (sVariable.isFlag()) ? CKeywords.BOOL : CKeywords.INT;
    String value = (sVariable.isFlag()) ? CKeywords.FALSE : "0";
    cVariable = new CVariableDeclaration(name, type, value);
    this.variables.put(sVariable, cVariable);
    return cVariable.getName();
  }

  /**
   * Determine the C++ name for a Smile constant.
   *
   * @param sConstant a Smile constant
   * @return the C++ name for {@link sConstant}
   */
  public String getConstantName(SConstant sConstant) {
    return sConstant.new Cases<String>().
      empty(() -> "0").
      vertex(uState -> this.getStateName(uState)).
      transition(uTransition -> this.getTransitionName(uTransition)).
      event(uEvent -> this.getEventName(uEvent)).
      apply();
  }

  /**
   * Determine the C++ access expression to a UML attribute of the underlying UML classifier.
   *
   * @param uAttribute a UML attribute
   * @return C++ access expression to {@link uAttribute}
   */
  String getAttributeAccess(UAttribute uAttribute) {
    assert uAttribute.getOwner().equals(this.uClassifier);

    CAttribute cAttribute = createAttribute(uAttribute);
    if (uAttribute.isStatic())
      return this.modelSymbolTable.getClassifierSymbolTable(uAttribute.getOwner()).getCClass().getName() + "::" + cAttribute.getName();
    else
      return CKeywords.THIS + "->" + cAttribute.getName();
  }
  
  /**
   * Create a C++ attribute representing a UML attribute of the underlying UML classifier.
   *
   * Also the infrastructure for this attribute, i.e., a getter and a setter,
   * are created.
   * 
   * @param uAttribute a UML attribute
   * @return a C++ attribute for {@link uAttribute}
   */
  public CAttribute createAttribute(UAttribute uAttribute) {
    assert uAttribute.getOwner().equals(this.uClassifier);

    CAttribute cAttribute = this.attributes.get(uAttribute);
    if (cAttribute != null)
      return cAttribute;

    String name = uniqueAttributeName(uAttribute.getName());
    String type = this.getTypeName(uAttribute.getType());
    String initialValue = null;
    if (uAttribute.isArray()) {
      initialValue = CKeywords.NEW + " " + type;
      initialValue += "{ ";
      initialValue += UMLTranslator.translateExpressions(uAttribute.getInitialValues(), uAttribute.getInitialValuesContext(), this);
      initialValue += " }";
    }
    else {
      if (uAttribute.hasInitialValues())
        initialValue = UMLTranslator.translateExpression(uAttribute.getInitialValue(0), uAttribute.getInitialValuesContext(), this);
      else
        initialValue = UMLTranslator.translateExpression(uAttribute.getType().getInitialValue(), uAttribute.getInitialValuesContext(), this);
    }

    // Set modifiers
    String modifiers = "";
    if (uAttribute.isStatic()) {
      modifiers = DEFAULTCLASSVARIABLEVISIBILITY;
      modifiers += " " + CKeywords.STATIC;
      if (uAttribute.isConstant())
        modifiers += " " + CKeywords.CONST;
    }
    if (uAttribute.getType().isDataType()) {
      cAttribute = new CAttribute(name, type, requireNonNull(initialValue), modifiers);
      // if (!uAttribute.isStatic() || uAttribute.isConstant())
      //   this.getCClass().getConstructor().addBlockStatement(new CCode(name + " = " + initialValue + ";"));
    }
    else 
      cAttribute = new CPointer(name, type, modifiers);

    // Add attribute to C++ class
    this.getCClass().addAttribute(cAttribute);
    this.attributes.put(uAttribute, cAttribute);

    // Create setter
    if (!uAttribute.isFinal()) {
      String setterName = uniqueMethodName("set" + name.substring(0, 1).toUpperCase() + name.substring(1, name.length()));
      this.attributeSetterNames.put(cAttribute, setterName);

      CParameter setterParameter = new CParameter(name, type + (cAttribute instanceof CPointer ? "*" : ""));
      CMethod setter = new CMethod(setterName, CKeywords.VOID, Arrays.asList(setterParameter), CKeywords.PUBLIC);
      setter.addBlockStatement(new CCode(this.getAttributeAccess(uAttribute) + " = " + setterParameter.getName() + ";"));
      this.getCClass().addMethod(setter);
    }

    // Create getter
    String getterName = uniqueMethodName("get" + name.substring(0, 1).toUpperCase() + name.substring(1, name.length()));
    CMethod getter = new CMethod(getterName, type + (cAttribute instanceof CPointer ? "*" : ""), new ArrayList<>(), CKeywords.PUBLIC);
    getter.addBlockStatement(new CCode("return " + this.getAttributeAccess(uAttribute) + ";"));
    this.getCClass().addMethod(getter);

    return cAttribute;
  }

  /**
   * Determine the name of the C++ setter for a UML attribute of the underlying classifier.
   *
   * @param uAttribute a UML attribute of the underlying classifier
   * @return the method name for the setter of {@code uAttribute} or {@code null} if the attribute is final
   */
  public @Nullable String getAttributeSetterName(UAttribute uAttribute) {
    CAttribute cAttribute = createAttribute(uAttribute);
    return this.attributeSetterNames.get(cAttribute);
  }

  /**
   * Determine the init method of this classifier.
   *
   * If there is an operation with name {@code INITMETHODNAME}, with an empty parameter list,
   * and with void return type, this is taken to be the init method; otherwise, another
   * method is created.
   *
   * @return the classifier's init method
   */
  public CMethod getInitMethod() {
    var initMethod = this.initMethod;
    if (initMethod != null)
      return initMethod;
    for (var uOperation : this.uClassifier.getOperations())
      if (uOperation.getName().equals(INITMETHODNAME) && uOperation.getType().equals(UType.function(new ArrayList<>(), UType.simple(this.getModel().getVoid()))))
        initMethod = createMethod(uOperation);
    initMethod = new CMethod(uniqueMethodName(INITMETHODNAME), CKeywords.VOID, new ArrayList<>(), CKeywords.PUBLIC);
    return this.initMethod = initMethod;
  }

  /**
   * Determine the name of the C++ method for a UML behavioural.
   *
   * @param uBehavioural a UML behavioural
   * @return the method name for {@code uBehavioural}
   */
  String getMethodName(UBehavioural uBehavioural) {
    if (!uBehavioural.getOwner().equals(this.uClassifier))
      return this.modelSymbolTable.getMethodName(uBehavioural);

    return this.createMethod(uBehavioural).getName();
  }

  /**
   * Create a C++ method for a UML behavioural of the underlying classifier.
   *
   * @param uBehavioural a UML behavioural
   * @return the C++ method for handling an external invocation of {@code uBehavioural}
   * @pre uBehavioural.getOwner().equals(this.uClassifier)
   */
  public CMethod createMethod(UBehavioural uBehavioural) {
    assert uBehavioural.getOwner().equals(this.uClassifier);

    CMethod cMethod = this.methods.get(uBehavioural);
    if (cMethod != null)
      return cMethod;

    String name = uniqueMethodName(uBehavioural.getName());

    var isCycleBasedOperation = this.getProperties().isCycleBased() && this.getUStateMachine() != null && !uBehavioural.isStatic() && uBehavioural.cases(o -> true, r -> false);
    List<CParameter> parameters = new ArrayList<>();
    if (isCycleBasedOperation) {
      var cParameterName = "sndep";
      parameters.add(new CParameter(cParameterName, ModelSymbolTable.EVENTPOOLCLASSNAME + "*"));
    }
    for (UParameter uParameter : uBehavioural.getParameters()) {
      String cParameterName = this.getParameterName(uParameter.getName());
      String cParameterType = this.getTypeName(uParameter.getType());
      parameters.add(new CParameter(cParameterName, cParameterType));
    }

    String modifiers = CKeywords.PUBLIC;
    if (uBehavioural.isStatic())
      modifiers += " " + CKeywords.STATIC;
    cMethod = new CMethod(name, CKeywords.VOID, parameters, modifiers);
    this.getCClass().addMethod(cMethod);
    this.methods.put(uBehavioural, cMethod);

    var cppMethodCode = uBehavioural.getMethodCode("C++");
    if (cppMethodCode != null) {
      for (var cppMethodCodeLine : cppMethodCode)
        cMethod.addBlockStatement(new CCode(cppMethodCodeLine));
    }
    if (this.getUStateMachine() != null && !uBehavioural.isStatic()) {
      StringBuilder argsBuilder = new StringBuilder(CKeywords.VOID + " *" + ARGSARRAYNAME);
      argsBuilder.append("[");
      argsBuilder.append(parameters.size());
      argsBuilder.append("] = {");
      String sep = "";
      for (UParameter uParameter : uBehavioural.getParameters()) {
        argsBuilder.append(sep);
        argsBuilder.append("(" + CKeywords.VOID + " *)");
        argsBuilder.append(this.getParameterName(uParameter.getName()));
        sep = ", ";
      }
      argsBuilder.append(" };");
      cMethod.addBlockStatement(new CCode(argsBuilder.toString()));

      StringBuilder callBuilder = new StringBuilder(this.getEventQueueAccess());
      callBuilder.append("->");
      callBuilder.append(uBehavioural instanceof UReception ? "insertsignal" : "insertcall");
      callBuilder.append("(");
      if (isCycleBasedOperation) {
        callBuilder.append(parameters.get(0).getName());
        callBuilder.append(", ");
      }
      callBuilder.append(this.getEventName(UEvent.behavioural(uBehavioural)));
      callBuilder.append(", ");
      callBuilder.append(parameters.size());
      callBuilder.append(", ");
      callBuilder.append(ARGSARRAYNAME);
      callBuilder.append(");");
      cMethod.addBlockStatement(new CCode(callBuilder.toString()));
    }
    
    return cMethod;
  }

  /**
   * Get the name of the internal method for handling a UML event.
   *
   * @param uEvent a UML event
   * @return the name of the internal method handling {@link uEvent}
   */
  String getEventMethodName(UEvent uEvent) {
    String eventMethodName = this.eventMethodNames.get(uEvent);
    if (eventMethodName != null)
      return eventMethodName;

    eventMethodName = uniqueMethodName(uEvent.getName());
    this.eventMethodNames.put(uEvent, eventMethodName);
    return eventMethodName;
  }

  /**
   * Get the name for the resumption method after a Smile primitive.
   *
   * @param sPrimitive a Smile primitive
   * @return the name for resuming after {@code sPrimitive}
   */
  String getResumptionMethodName(SPrimitive sPrimitive) {
    var resumptionName = this.resumptionMethodNames.get(sPrimitive);
    if (resumptionName != null)
      return resumptionName;

    resumptionName = uniqueMethodName("resume_" + sPrimitive.toString());
    this.resumptionMethodNames.put(sPrimitive, resumptionName);
    return resumptionName;
  }

  /**
   * Determine the enumeration type for holding all literals
   * for a given UML type like events, states, or transitions.
   *
   * @param name UML type name
   * @return the enumeration type for {@link name}
   */
  private CEnum getCEnum(String name) {
    CEnum cEnum = this.getCClass().getEnumerations().stream().filter(e -> e.getName().equals(name)).findAny().orElse(null);
    if (cEnum != null)
      return cEnum;

    cEnum = new CEnum(name, CKeywords.PRIVATE);
    this.getCClass().addEnumerationType(cEnum);
    return cEnum;
  }

  /**
   * Determine the C++ name of a UML event.
   *
   * @param uEvent a UML event
   * @return the C++ name for {@link uEvent}
   */
  public String getEventName(UEvent uEvent) {
    String eventName = this.eventNames.get(uEvent);
    if (eventName != null)
      return eventName;

    CEnum eventsEnum = this.getCEnum("events");
    eventName = newName(uEvent.getName().toUpperCase(), eventsEnum.getLiterals());
    if (uEvent.isCompletion()) {
      int number = (int)this.eventNames.keySet().stream().filter(UEvent::isCompletion).count()+1;
      eventsEnum.addLiteral(eventName, number);
      String stateName = this.getStateName(uEvent.getState());
      this.getCEnum("states").addLiteral(stateName, number);
    }
    else
      eventsEnum.addLiteral(eventName);
    this.eventNames.put(uEvent, eventName);
    return eventName;
  }

  /**
   * Get the name for "acknowledging" a Smile primitive.
   *
   * @param sPrimitive a Smile primitive
   * @return the name for acknowledging {@code sPrimitive}
   */
  String getAcknowledgementName(SPrimitive sPrimitive) {
    var acknowledgementName = this.acknowledgementNames.get(sPrimitive);
    if (acknowledgementName != null)
      return acknowledgementName;

    CEnum eventsEnum = this.getCEnum("events");
    acknowledgementName = newName("ack" + sPrimitive.toString().toUpperCase(), eventsEnum.getLiterals());
    eventsEnum.addLiteral(acknowledgementName);
    this.acknowledgementNames.put(sPrimitive, acknowledgementName);
    return acknowledgementName;
  }

  /**
   * Determine the C++ name for a UML state (vertex).
   *
   * @param uVertex a UML vertex
   * @return a name for {@link uVertex}
   */
  public String getStateName(UVertex uVertex) {
    String stateName = this.stateNames.get(uVertex);
    if (stateName != null)
      return stateName;

    stateName = newName(uVertex.getIdentifier().toString().toUpperCase(), this.stateNames.values());
    this.getCEnum("states").addLiteral(stateName);
    this.stateNames.put(uVertex, stateName);
    return stateName;
  }

  /**
   * Determine the C++ name for a UML (compound) transition.
   *
   * @param uTransition a UML compound transition
   * @return a name for {@code uTransition}
   */
  private String getTransitionName(UCompoundTransition uTransition) {
    String transitionName = this.transitionNames.get(uTransition);
    if (transitionName != null)
      return transitionName;

    transitionName = newName(uTransition.getName(), this.transitionNames.values());
    this.getCEnum("transitions").addLiteral(transitionName);
    this.transitionNames.put(uTransition, transitionName);
    return transitionName;
  }
  
  /**
   * Determine the collection of collected variable declarations.
   * 
   * @return a collection of C++ variable declarations
   */
  public Collection<CVariableDeclaration> getVariableDeclarations() {
    return this.variables.values();
  }

  /**
   * Determine the C++ name for a parameter name.
   * 
   * @param parameterName a parameter name
   * @return the C++ parameter name
   */
  String getParameterName(String parameterName) {
    if (parameterName.equals(""))
      parameterName = "p";
    return this.newName(parameterName, CKeywords.getKeywords());
  }

  /**
   * Determine a unique name for an attribute name.
   * 
   * @param attributeName an attribute name
   * @return a unique attribute name
   */
  private String uniqueAttributeName(String attributeName) {
    if (attributeName.equals(""))
      attributeName = "a";
    String uniqueAttributeName = this.newName(attributeName, this.attributeNames);
    this.attributeNames.add(uniqueAttributeName);
    return uniqueAttributeName;
  }

  /**
   * Determine a unique name for a method name.
   * 
   * @param methodName a method name
   * @return a unique method name
   */
  private String uniqueMethodName(String methodName) {
    if (methodName.equals(""))
      methodName = "m";
    String uniqueMethodName = this.newName(methodName, this.methodNames);
    this.methodNames.add(uniqueMethodName);
    return uniqueMethodName;
  }

  /**
   * Determine a unique name for a variable name.
   * 
   * @param variableName a variable name
   * @return a unique variable name
   */
  private String uniqueVariableName(String variableName) {
    if (variableName.equals(""))
      variableName = "v";
    String uniqueVariableName = this.newName(variableName, this.variableNames);
    this.variableNames.add(uniqueVariableName);
    return uniqueVariableName;
  }
}
