package translation.uml2cpp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import cpp.CBlock;
import cpp.CBlockStatement;
import cpp.CBranch;
import cpp.CBreak;
import cpp.CCase;
import cpp.CClass;
import cpp.CCode;
import cpp.CIfElse;
import cpp.CIfs;
import cpp.CKeywords;
import cpp.CLoop;
import cpp.CMethod;
import cpp.CParameter;
import cpp.CPointer;
import cpp.CSequence;
import cpp.CSwitch;
import cpp.CVariableDeclaration;
import uml.UAction;
import uml.UContext;
import uml.UExpression;
import uml.UParameter;
import uml.smile.SBranch;
import uml.smile.SGuard;
import uml.smile.SLiteral;
import uml.smile.SMachine;
import uml.smile.SPrimitive;
import uml.smile.SStatement;
import uml.smile.SValue;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import util.Formatter;
import util.Pair;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;


/**
 * Translate Smile machine into Java code.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class SmileTranslator {
  /**
   * Translate a Smile machine.
   * 
   * The state machine method is added to the Java class of the class symbol
   * table.
   * 
   * @param uMachine a Smile machine
   * @param symbolTable a class symbol table
   */
  public static void translate(SMachine sMachine, ClassifierSymbolTable symbolTable) {
    CClass cClass = symbolTable.getCClass();
    cClass.addAttribute(new CPointer(ClassifierSymbolTable.EVENTPOOLNAME, ModelSymbolTable.EVENTPOOLCLASSNAME, CKeywords.PUBLIC));

    cClass.addMethods(getInitMethods(sMachine, symbolTable));
    if (symbolTable.getProperties().isEventBased()) {
      for (UEvent uEvent : sMachine.getStateMachine().getEvents())
        cClass.addMethods(getEventMethods(uEvent, sMachine, symbolTable));
    }
    cClass.addMethod(getEventHandlingMethod(sMachine, symbolTable));
    cClass.addMethod(getTerminatedMethod(sMachine, symbolTable));
    cClass.addAttributes(symbolTable.getVariableDeclarations().stream().map(CVariableDeclaration::getAttribute).collect(toSet()));
  }

  /**
   * Create C++ methods for initialising a Smile machine.
   * 
   * @param sMachine a Smile Machine
   * @param symbolTable {@code sMachine}'s underlying classifier symbol table
   * @return C++ method for initialisation
   */
  private static Set<CMethod> getInitMethods(SMachine sMachine, ClassifierSymbolTable symbolTable) {
    var initMethods = new HashSet<CMethod>();
    var method = symbolTable.getInitMethod();
    initMethods.add(method);
    if (symbolTable.getProperties().isCycleBased())
      initMethods.addAll(getCyclesMethods(method, sMachine.getInitialisationCycles(), symbolTable));
    else
      method.getBody().addBlockStatement(translateStatement(sMachine.getInitialisation(), symbolTable));
    return initMethods;
  }

  /**
   * Create C++ methods for the internal handling of a UML event.
   * 
   * @param uEvent a UML event
   * @param sMachine a Smile Machine
   * @param symbolTable {@code sMachine}'s underlying classifier symbol table
   * @return C++ methods for {@link uEvent}
   */
  private static Set<CMethod> getEventMethods(UEvent uEvent, SMachine sMachine, ClassifierSymbolTable symbolTable) {
    var eventMethods = new HashSet<CMethod>();
    var uStateMachine = symbolTable.getUStateMachine(); assert uStateMachine != null;
    var parameters = uEvent.getParameters(uStateMachine).stream().
                                map(uParameter -> new CParameter(symbolTable.getParameterName(uParameter.getName()), symbolTable.getTypeName(uParameter.getType()))).collect(toList());

    var method = new CMethod(symbolTable.getEventMethodName(uEvent), CKeywords.VOID, parameters, CKeywords.PRIVATE);
    eventMethods.add(method);
    if (symbolTable.getProperties().isCycleBased())
      eventMethods.addAll(getCyclesMethods(method, sMachine.getReactionCycles(uEvent), symbolTable));
    else
      method.getBody().addBlockStatement(translateStatement(sMachine.getReaction(uEvent), symbolTable));
    return eventMethods;
  }

  private static Set<CMethod> getCyclesMethods(CMethod method, Pair<SStatement, IdentityHashMap<SPrimitive, SStatement>> cycles, ClassifierSymbolTable symbolTable) {
    var cyclesMethods = new HashSet<CMethod>();
    method.getBody().addBlockStatement(translateStatement(cycles.getFirst(), symbolTable));
    var resumptionsMap = cycles.getSecond();
    for (var suspender : resumptionsMap.keySet()) {
      var resumption = resumptionsMap.get(suspender); assert resumption != null;
      var resumptionMethod = new CMethod(symbolTable.getResumptionMethodName(suspender), CKeywords.VOID, method.getParameters(), CKeywords.PRIVATE);
      resumptionMethod.getBody().addBlockStatement(translateStatement(resumption, symbolTable));
      cyclesMethods.add(resumptionMethod);
    }
    return cyclesMethods;
  }

  private static CCode getLocalMethodCall(String name, List<UParameter> uParameters, ClassifierSymbolTable symbolTable) {
    StringBuilder callBuilder = new StringBuilder();
    callBuilder.append(CKeywords.THIS);
    callBuilder.append("->");
    callBuilder.append(name);
    callBuilder.append("(");
    String separator = "";
    for (UParameter uParameter : uParameters) {
      callBuilder.append(separator);
      callBuilder.append(symbolTable.getUnboxing(uParameter.getType(), symbolTable.getEventAccess() + "->getarg(" + uParameter.getNumber() + ")"));
      separator = ", ";
    }
    callBuilder.append(");");
    return new CCode(callBuilder.toString());
  }

  private static List<CCase> getCyclesHandlingMethods(IdentityHashMap<SPrimitive, SStatement> resumptionsMap, List<UParameter> uParameters, ClassifierSymbolTable symbolTable) {
    List<CCase> cases = new ArrayList<>();
    for (var suspender : resumptionsMap.keySet()) {
      var resumption = resumptionsMap.get(suspender); assert resumption != null;
      cases.add(new CCase(symbolTable.getAcknowledgementName(suspender),
                          new CSequence(getLocalMethodCall(symbolTable.getResumptionMethodName(suspender), uParameters, symbolTable), new CBreak())));
    }
    return cases;
  }

  private static CMethod getEventHandlingMethod(SMachine sMachine, ClassifierSymbolTable symbolTable) {
    CMethod handleMethod = new CMethod(ClassifierSymbolTable.HANDLEMETHODNAME, CKeywords.VOID, Arrays.asList(new CParameter(ModelSymbolTable.CURRENTEVENTNAME, ModelSymbolTable.EVENTCLASSNAME + "* ")), CKeywords.PUBLIC);
    if (!symbolTable.getProperties().isEventBased()) {
      handleMethod.addBlockStatement(translateStatement(sMachine.getReaction(), symbolTable));
      return handleMethod;
    }

    List<CCase> handleCases = new ArrayList<>();
    for (UEvent uEvent : sMachine.getStateMachine().getEvents()) {
      var uParameters = uEvent.getParameters(sMachine.getStateMachine());
      handleCases.add(new CCase(symbolTable.getEventName(uEvent),
                                new CSequence(getLocalMethodCall(symbolTable.getEventMethodName(uEvent), uParameters, symbolTable), new CBreak())));
    }
    if (symbolTable.getProperties().isCycleBased()) {
      for (UEvent uEvent : sMachine.getStateMachine().getEvents())
        handleCases.addAll(getCyclesHandlingMethods(sMachine.getReactionCycles(uEvent).getSecond(), uEvent.getParameters(sMachine.getStateMachine()), symbolTable));
      handleCases.addAll(getCyclesHandlingMethods(sMachine.getInitialisationCycles().getSecond(), new ArrayList<>(), symbolTable));
    }
    handleMethod.getBody().addBlockStatement(new CSwitch(symbolTable.getEventAccess() + "->getid()", handleCases, null));
    return handleMethod;
  }

  private static CMethod getTerminatedMethod(SMachine sMachine, ClassifierSymbolTable symbolTable) {
    CMethod termMethod = new CMethod(ClassifierSymbolTable.TERMMETHODNAME, CKeywords.BOOL, new ArrayList<>(), CKeywords.PUBLIC);
    termMethod.getBody().addBlockStatement(new CCode(CKeywords.RETURN + " " + translateExpression(sMachine.getTerminated(), symbolTable) + ";"));
    return termMethod;
  }

  /**
   * Translator for Smile statements.
   * 
   * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
   */
  private static class StatementTranslator implements SStatement.Visitor<CBlockStatement> {
    private ClassifierSymbolTable symbolTable;

    public StatementTranslator(ClassifierSymbolTable symbolTable) {
      this.symbolTable = symbolTable;
    }

    /**
     * Translate a Smile statement.
     * 
     * @param statement a Smile statement
     */
    public CBlockStatement translate(SStatement sStatement) {
      if (this.symbolTable.getProperties().isCycleBased() && sStatement instanceof SPrimitive && ((SPrimitive)sStatement).suspended() != null) {
        // Register the acknowledgement event that is waited for next
        var cSequence = new CSequence();
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.append(this.symbolTable.getEventQueueAccess());
        resultBuilder.append("->waitforacknowledgement(");
        resultBuilder.append(this.symbolTable.getAcknowledgementName((SPrimitive)sStatement));
        resultBuilder.append(");");
        cSequence.addBlockStatement(new CCode(resultBuilder.toString()));
        cSequence.addBlockStatement(sStatement.accept(this));
        return cSequence;
      }
      return sStatement.accept(this);
    }
    
    @Override
    public CBlockStatement onSkip() {
      return new CSequence();
    }

    @Override
    public CBlockStatement onAtomic(SStatement sStatement) {
      return translate(sStatement);
    }

    @Override
    public CBlockStatement onSequential(SStatement sLeft, SStatement sRight) {
      return new CSequence(translate(sLeft), translate(sRight));
    }

    @Override
    public CBlockStatement onChoice(List<SBranch> sBranches, @Nullable SStatement sElseStm) {
      List<CBranch> cBranches = new ArrayList<>();
      for (SBranch sBranch : sBranches) {
        CBranch cBranch = this.onBranch(sBranch.getGuard(), sBranch.getEffect());
        if (!cBranch.hasEmptyStatement())
          cBranches.add(cBranch);
      }
      CBranch cElseBranch = null;
      if (sElseStm != null)
        cElseBranch = translate(sElseStm).asBranch();
      return new CIfs(cBranches, cElseBranch);
    }

    @Override
    public CBlockStatement onLoop(List<SBranch> sBranches, @Nullable SStatement sElseStm) {
      CLoop cLoop = new CLoop();
      for (SBranch sBranch : sBranches)
        cLoop.addBranch(this.onBranch(sBranch.getGuard(), sBranch.getEffect()));
      if (sElseStm != null)
        cLoop.addBranch(new CBranch().addBlockStatement(translate(sElseStm)));
      return cLoop;
    }

    @Override
    public CBranch onBranch(SGuard sGuard, SStatement sStatement) {
      return new CBranch(translateExpression(sGuard, this.symbolTable)).addBlockStatement(translate(sStatement));
    }

    @Override
    public CBlockStatement onBreak() {
      return new CBreak();
    }

    @Override
    public CBlockStatement onAssignment(SVariable sVariable, SValue sValue) {
      return new CCode(this.symbolTable.getVariableName(sVariable) + " = " + translateValue(sValue, this.symbolTable) + ";");
    }

    @Override
    public CBlockStatement onAssignment(SVariable sVariable, SVariable otherSVariable) {
      return new CCode(this.symbolTable.getVariableName(sVariable) + " = " + this.symbolTable.getVariableName(otherSVariable) + ";");
    }

    @Override
    public CBlockStatement onExternal(UAction external, UContext context) {
      return UMLTranslator.translateAction(external, context, this.symbolTable);
    }

    @Override
    public CBlockStatement onSuccess() {
      return new CSequence(new CCode(this.symbolTable.getEventQueueAccess() + "->log(\"Normal termination of state machine\");"),
                           new CCode("return;"));
    }

    @Override
    public CBlockStatement onFail() {
      return new CSequence(new CCode(this.symbolTable.getEventQueueAccess() + "->log(\"Abnormal termination of state machine\");"),
                           new CCode("exit(-1);"));
    }

    @Override
    public CBlockStatement onAwait(SGuard sExpression) {
      return new CIfElse(translateExpression(SGuard.neg(sExpression), this.symbolTable),
                     new CBlock(new CCode(this.symbolTable.getEventQueueAccess() + "->log(\"Abnormal termination of state machine\");"),
                                new CCode("exit(-1);")),
                     null);
    }

    @Override
    public CBlockStatement onInitialisation() {
      return new CSequence();
    }

    @Override
    public CBlockStatement onFetch(Set<UState> waitStates) {
      return new CCode(this.symbolTable.getEventAccess() + " = " + this.symbolTable.getEventQueueAccess() + "->fetch();");
    }

    @Override
    public CBlockStatement onAcknowledge() {
      if (this.symbolTable.getProperties().isCycleBased())
        return new CCode(this.symbolTable.getEventQueueAccess() + "->sendacknowledgement();");
      return new CCode(this.symbolTable.getEventQueueAccess() + "->acknowledge();");
    }

    @Override
    public CBlockStatement onDefer() {
      return new CCode(this.symbolTable.getEventQueueAccess() + "->defer();");
    }

    @Override
    public CBlockStatement onChosen() {
      @Nullable UStateMachine uStateMachine = this.symbolTable.getUStateMachine();
      if (uStateMachine == null || uStateMachine.getDeferrableEvents().isEmpty())
        return new CSequence();
      return new CCode(this.symbolTable.getEventQueueAccess() + "->chosen();");
    }

    @Override
    public CBlockStatement onComplete(boolean keep, UState uState) {
      if (keep)
        return new CCode(this.symbolTable.getEventQueueAccess() + "->complete(" + this.symbolTable.getStateName(uState) + ");");
      return new CCode(this.symbolTable.getEventQueueAccess() + "->insertcompletion(" + this.symbolTable.getEventName(UEvent.completion(uState)) + ");");
    }

    @Override
    public CBlockStatement onUncomplete(UState uState) {
      return new CCode(this.symbolTable.getEventQueueAccess() + "->uncomplete(" + this.symbolTable.getStateName(uState) +");");
    }

    @Override
    public CBlockStatement onStartTimer(boolean keep, UEvent uEvent) {
      String low = UMLTranslator.translateExpression(uEvent.getTimeLowExpression(), uEvent.getTimeContext(), this.symbolTable);
      String high = UMLTranslator.translateExpression(uEvent.getTimeHighExpression(), uEvent.getTimeContext(), this.symbolTable);
      return new CCode(this.symbolTable.getEventQueueAccess() + "->starttimer(" + this.symbolTable.getEventName(uEvent) + ", " + low + ", " + high + ");");
    }

    @Override
    public CBlockStatement onStopTimer(UEvent uEvent) {
      return new CCode(this.symbolTable.getEventQueueAccess() + "->stoptimer(" + this.symbolTable.getEventName(uEvent) + ");");
    }
  }

  private static CBlockStatement translateStatement(SStatement statement, ClassifierSymbolTable symbolTable) {
    return new StatementTranslator(symbolTable).translate(statement);
  }

  /**
   * Translator for Smile expressions.
   * 
   * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
   */
  private static class ExpressionTranslator implements SGuard.Visitor<String>, SLiteral.Visitor<String> {
    private ClassifierSymbolTable symbolTable;

    ExpressionTranslator(ClassifierSymbolTable symbolTable) {
      this.symbolTable = symbolTable;
    }

    /**
     * Translate a Smile expression.
     * 
     * @param sExpression a Smile expression
     * @return C++ code string representation of {@link sExpression}
     */
    public String translate(SGuard sExpression) {
      return sExpression.accept(this);
    }

    @Override
    public String onBooleanConstant(boolean booleanConstant) {
      return booleanConstant ? "true" : "false";
    }

    @Override
    public String onEq(boolean positive, SVariable sVariable, Set<SValue> sValues) {
      if (sValues.isEmpty())
        return positive ? "false" : "true";

      Function<SValue, @Nullable String> eqFun = sValue -> this.symbolTable.getVariableName(sVariable) + " == " + translateValue(sValue, this.symbolTable);
      String eq = Formatter.separated(sValues, eqFun, " || ");
      return positive ? (sValues.size() > 1 ? Formatter.parenthesised(eq) : eq) : "!(" + eq + ")";
    }

    @Override
    public String onMatch(boolean positive, Set<UEvent> uEvents) {
      if (uEvents.isEmpty())
        return positive ? "false" : "true";

      Function<UEvent, @Nullable String> matchFun = uEvent -> this.symbolTable.getEventAccess() + "->match(" + this.symbolTable.getEventName(uEvent) + ")";
      String match = Formatter.separated(uEvents, matchFun, " || ");
      if (uEvents.size() > 1)
        match = Formatter.parenthesised(match);
      return positive ? (uEvents.size() > 1 ? Formatter.parenthesised(match) : match) : "!(" + match + ")";
    }

    @Override
    public String onIsCompleted(boolean positive, UState uState) {
      String isCompleted = this.symbolTable.getEventQueueAccess() + "->iscompleted(" + this.symbolTable.getEventName(UEvent.completion(uState)) + ")";
      return positive ? isCompleted : "!(" + isCompleted + ")";
    }

    @Override
    public String onIsTimedOut(boolean positive, UEvent event) {
      String isTimedOut = this.symbolTable.getEventQueueAccess() + "->istimedout(" + this.symbolTable.getEventName(event) + ")";
      return positive ? isTimedOut : "!(" + isTimedOut + ")";
    }

    @Override
    public String onExternal(boolean positive, UExpression uExpression, UContext context) {
      String external = UMLTranslator.translateExpression(uExpression, context, this.symbolTable);
      return positive ? external : "!(" + external + ")";
    }

    @Override
    public String onLiteral(SLiteral literal) {
      return literal.accept(this);
    }

    @Override
    public String onAnd(SGuard leftExpression, SGuard rightExpression) {
      StringBuilder resultBuilder = new StringBuilder("(");
      resultBuilder.append(translate(leftExpression));
      resultBuilder.append(" && ");
      resultBuilder.append(translate(rightExpression));
      return resultBuilder.append(")").toString();
    }


    @Override
    public String onOr(SGuard leftExpression, SGuard rightExpression) {
      StringBuilder resultBuilder = new StringBuilder("(");
      resultBuilder.append(translate(leftExpression));
      resultBuilder.append(" || ");
      resultBuilder.append(translate(rightExpression));
      return resultBuilder.append(")").toString();
    }
  }

  private static String translateExpression(SGuard sExpression, ClassifierSymbolTable symbolTable) {
    return new ExpressionTranslator(symbolTable).translate(sExpression);
  }

  private static String translateValue(SValue sValue, ClassifierSymbolTable symbolTable) {
    return sValue.new Cases<String>().booleanConstant(booleanConstant -> booleanConstant ? "true" : "false").
                                      constant(sConstant -> symbolTable.getConstantName(sConstant)).
                                      apply();
  }
}
