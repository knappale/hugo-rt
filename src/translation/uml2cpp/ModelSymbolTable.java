package translation.uml2cpp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import cpp.CClass;
import cpp.CKeywords;
import cpp.CSystem;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UClassifier;
import uml.UCollaboration;
import uml.UType;

import static util.Objects.requireNonNull;


@NonNullByDefault
public class ModelSymbolTable extends SymbolTable {
  public static final String EVENTCLASSNAME = "runtime::event";
  public static final String EVENTPOOLCLASSNAME = "runtime::eventpool";
  public static final String CURRENTEVENTNAME = "current";

  private UCollaboration uCollaboration;
  private CSystem cSystem;

  private Map<UClassifier, CClass> cClasses = new HashMap<>();
  private Map<UClassifier, ClassifierSymbolTable> classSymbolTables = new HashMap<>();
  private Set<String> classNames = new HashSet<>();

  public ModelSymbolTable(UCollaboration uCollaboration, CSystem cSystem) {
    super(uCollaboration.getModel());
    this.uCollaboration = uCollaboration;
    this.cSystem = cSystem;
    this.classNames.addAll(CKeywords.getKeywords());
  }

  @Override
  control.Properties getProperties() {
    return this.uCollaboration.getProperties();
  }

  /**
   * @return the C++ system that this model symbol table works on
   */
  CSystem getCSystem() {
    return this.cSystem;
  }

  /**
   * Determine a C++ expression for accessing the current event.
   * 
   * @return C++ expression for accessing the current event
   */
  public String getEventAccess() {
    return CURRENTEVENTNAME;
  }

  /**
   * Determine the classifier symbol table for a UML classifier.
   *
   * @param uClassifier a UML classifier
   * @return the classifier symbol table for {@link uClassifier}
   */
  public ClassifierSymbolTable getClassifierSymbolTable(UClassifier uClassifier) {
    ClassifierSymbolTable classSymbolTable = this.classSymbolTables.get(uClassifier);
    if (classSymbolTable == null) {
      classSymbolTable = new ClassifierSymbolTable(uClassifier, this);
      this.classSymbolTables.put(uClassifier, classSymbolTable);
    }
    return classSymbolTable;
  }

  /**
   * Determine the C++ class for a UML classifier.
   *
   * @param uClassifier a UML classifier
   * @return the Java C++ for {@line uClassifier}
   */
  public CClass getCClass(UClassifier uClassifier) {
    CClass cClass = this.cClasses.get(uClassifier);
    if (cClass != null)
      return cClass;
      
    String className = uniqueClassName(uClassifier.getName());
    cClass = new CClass(className);
    this.cClasses.put(uClassifier, cClass);
    return cClass;
  }

  /**
   * Get the name of the C++ type representing a UML type.
   * 
   * @param uType a UML type
   * @return name of the C++ type for {@link uType}
   */
  public String getTypeName(UType uType) {
    String cTypeName = "";
    if (uType.getUnderlyingType().isDataType()) {
      if (this.getModel().getIntegerType().subsumes(uType.getUnderlyingType()))
        cTypeName = CKeywords.INT;
      else {
        if (this.getModel().getBooleanType().subsumes(uType.getUnderlyingType()))
          cTypeName = CKeywords.BOOL;
        else {
          if (this.getModel().getStringType().subsumes(uType.getUnderlyingType()))
            cTypeName = CKeywords.STRING;
        }
      }
    }
    else
      cTypeName = this.getClassifierSymbolTable(requireNonNull((UClass)uType.getUnderlyingType().getClassifier())).getCClass().getName();
    if (uType.isArray())
      cTypeName += "[]";
    return cTypeName;
  }

  /**
   * Get an access expression to the C++ attribute representing a UML attribute.
   * 
   * @param uAttribute a UML attribute
   * @return a C++ access expression to the C++ attribute for {@code uAttribute}
   */
  public String getAttributeAccess(UAttribute uAttribute) {
    return this.getClassifierSymbolTable(uAttribute.getOwner()).getAttributeAccess(uAttribute);
  }

  /**
   * Get the name of the C++ method representing a UML behavioural.
   * 
   * @param uBehavioural a UML behavioural
   * @return name of the C++ method for {@code uBehavioural}
   */
  public String getMethodName(UBehavioural uBehavioural) {
    return this.getClassifierSymbolTable(uBehavioural.getOwner()).getMethodName(uBehavioural);
  }

  /**
   * Determine a unique name for a clas name.
   * 
   * @param className a class name
   * @return a unique class name
   */
  private String uniqueClassName(String className) {
    if (className.equals(""))
      className = "c";
    String uniqueClassName = this.newName(className, this.classNames);
    this.classNames.add(uniqueClassName);
    return uniqueClassName;
  }
}
