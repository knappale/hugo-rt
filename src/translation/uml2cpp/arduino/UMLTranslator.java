package translation.uml2cpp.arduino;

import java.util.Arrays;

import cpp.CBlock;
import cpp.CBlockStatement;
import cpp.CClass;
import cpp.CCode;
import cpp.CIfElse;
import cpp.CKeywords;
import cpp.CMethod;
import cpp.CSequence;
import cpp.CSystem;
import cpp.CVariableDeclaration;
import translation.uml2cpp.ClassifierSymbolTable;
import translation.uml2cpp.ModelSymbolTable;
import uml.UClass;
import uml.UCollaboration;
import uml.statemachine.semantics.UCompletionTree;
import uml.statemachine.semantics.UTimerTree;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class UMLTranslator {
  public static CSystem translate(UCollaboration uCollaboration) {
    var uModel = uCollaboration.getModel();
    var cSystem = new CSystem();
    var modelSymbolTable = new ModelSymbolTable(uCollaboration, cSystem);

    // Translate each class of the model
    for (var uClass : uModel.getClasses())
      cSystem.addClass(translateClass(uClass, modelSymbolTable));

    if (!uCollaboration.isEmpty()) {
      // Translate collaboration
      for (var uObject : uCollaboration.getObjects()) {
        var uClass = uObject.getC1ass();
        var classifierSymbolTable = modelSymbolTable.getClassifierSymbolTable(uClass);
        var cClassName = classifierSymbolTable.getCClass().getName();
        cSystem.addVariable(new CVariableDeclaration(uObject.getName(), cClassName + "*", CKeywords.NULLPTR));
      }
      cSystem.addVariable(new CVariableDeclaration(ModelSymbolTable.CURRENTEVENTNAME, ModelSymbolTable.EVENTCLASSNAME + "*", CKeywords.NULLPTR));
      cSystem.addMethod(setupMethod(uCollaboration, modelSymbolTable));
      cSystem.addMethod(loopMethod(uCollaboration, modelSymbolTable));
    }

    return cSystem;
  }

  public static CClass translateClass(UClass uClass, ModelSymbolTable symbolTable) {
    var classifierSymbolTable = symbolTable.getClassifierSymbolTable(uClass);

    // Translation of attributes (including getters and setters)
    for (var uAttribute : uClass.getAttributes())
      classifierSymbolTable.createAttribute(uAttribute);

    // Translation of receptions
    for (var uReception : uClass.getReceptions())
      classifierSymbolTable.createMethod(uReception);

    // Translation of operations
    for (var uOperation : uClass.getOperations())
      classifierSymbolTable.createMethod(uOperation);

    var cClass = classifierSymbolTable.getCClass();

    // Translation of state machine
    var uStateMachine = uClass.getStateMachine();
    if (uStateMachine != null) {
      var sMachine = uStateMachine.getMachine(classifierSymbolTable.getProperties());
      translation.uml2cpp.SmileTranslator.translate(sMachine, classifierSymbolTable);

      cClass.addHeader("eventpool");
      cClass.getConstructor().addBlockStatement(new CCode(classifierSymbolTable.getEventQueueAccess() + " = " + CKeywords.NEW + " " + ModelSymbolTable.EVENTPOOLCLASSNAME + "(" +
          uClass.getMaxParameters() + ", " +
          classifierSymbolTable.getProperties().getExternalQueueCapacity() + ", " +
          classifierSymbolTable.getProperties().getDeferredQueueCapacity() + ", " +
          classifierSymbolTable.getProperties().getInternalQueueCapacity() + ", " +
          UCompletionTree.create(uStateMachine).getMaxCompletionsNumber() + ", " +
          UTimerTree.create(uStateMachine).getMaxTimersNumber() + ");"));
    }

    return cClass; 
  }

  public static CMethod setupMethod(UCollaboration uCollaboration, ModelSymbolTable modelSymbolTable) {
    var cSetup = new CMethod("setup", CKeywords.VOID, Arrays.asList(), CKeywords.PUBLIC);

    // Create objects
    for (var uObject : uCollaboration.getObjects()) {
      var uClass = uObject.getC1ass();
      var classifierSymbolTable = modelSymbolTable.getClassifierSymbolTable(uClass);
      var cClassName = classifierSymbolTable.getCClass().getName();
      cSetup.getBody().addBlockStatement(new CCode(uObject.getName() + " = " + CKeywords.NEW + " " + cClassName + "();"));
    }

    // Initialise object configuration
    for (var uObject : uCollaboration.getObjects()) {
      var uClass = uObject.getC1ass();
      var classifierSymbolTable = modelSymbolTable.getClassifierSymbolTable(uClass);
      for (var uSlot : uObject.getSlots()) {
        var uAttribute = uSlot.getAttribute();
        var setterName = classifierSymbolTable.getAttributeSetterName(uAttribute);
        if (setterName == null)
          continue;

        var valueBuilder = new StringBuilder();
        if (uAttribute.isArray()) {
          valueBuilder.append(CKeywords.NEW);
          valueBuilder.append(" ");
          valueBuilder.append(classifierSymbolTable.getTypeName(uAttribute.getType()));
          valueBuilder.append("{ ");
        }
        valueBuilder.append(translation.uml2cpp.UMLTranslator.translateExpressions(uSlot.getValues(), uSlot.getValuesContext(), classifierSymbolTable));
        if (uAttribute.isArray())
          valueBuilder.append(" }");
        
        cSetup.getBody().addBlockStatement(new CCode(uObject.getName() + "->" + setterName + "(" + valueBuilder.toString() + ");"));
      }
    }
    cSetup.getBody().addBlockStatement(new CCode(""));
   
    // Initialise state machines
    for (var uObject : uCollaboration.getObjects()) {
      if (uObject.getC1ass().getStateMachine() != null)
        cSetup.getBody().addBlockStatement(new CCode(uObject.getName() + "->" + ClassifierSymbolTable.INITMETHODNAME + "();"));
    }

    return cSetup;
  }

  public static CMethod loopMethod(UCollaboration uCollaboration, ModelSymbolTable modelSymbolTable) {
    var cLoop = new CMethod("loop", CKeywords.VOID, Arrays.asList(), CKeywords.PUBLIC);

    CBlockStatement sep = null;
    // Round-robin for all objects
    for (var uObject : uCollaboration.getObjects()) {
      var uClass = uObject.getC1ass();
      var uStateMachine = uClass.getStateMachine();
      if (uStateMachine == null)
        continue;
      if (sep != null)
        cLoop.getBody().addBlockStatement(sep);
      var cStep = new CSequence(new CCode(modelSymbolTable.getEventAccess() + " = " + uObject.getName() + "->" + ClassifierSymbolTable.EVENTPOOLNAME + "->poll();"),
                                new CIfElse(modelSymbolTable.getEventAccess() + " != " + CKeywords.NULLPTR,
                                            new CBlock(new CCode(uObject.getName() + "->" + ClassifierSymbolTable.HANDLEMETHODNAME + "(" + modelSymbolTable.getEventAccess() + ");"),
                                                       new CCode(uObject.getName() + "->" + ClassifierSymbolTable.EVENTPOOLNAME + "->" + ClassifierSymbolTable.HANDLEDMETHODNAME + "();")),
                                            null));
      var classifierSymbolTable = modelSymbolTable.getClassifierSymbolTable(uClass);
      var sMachine = uStateMachine.getMachine(classifierSymbolTable.getProperties());
      if (!sMachine.getTerminated().isFalse())
        cLoop.getBody().addBlockStatement(new CIfElse("!" + uObject.getName() + "->" + ClassifierSymbolTable.TERMMETHODNAME + "()", cStep.asStatement(), null));
      else
        cLoop.getBody().addBlockStatement(cStep);
      sep = new CCode("");
    }
 
    return cLoop;
  }
}
