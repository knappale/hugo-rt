package translation.uml2cpp;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import cpp.CClass;
import cpp.CCode;
import cpp.CIfElse;
import cpp.CKeywords;
import cpp.CMethod;
import cpp.CParameter;
import cpp.CSystem;
import cpp.CWhile;
import cpp.CSequence;
import uml.UAction;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UCollaboration;
import uml.UContext;
import uml.UExpression;
import uml.UModel;
import uml.UObject;
import uml.UOperation;
import uml.UOperator;
import uml.UReception;
import uml.USlot;
import uml.UStatual;
import uml.smile.SMachine;
import uml.statemachine.UStateMachine;
import util.Formatter;

import static java.util.stream.Collectors.toList;


public class UMLTranslator {
  public static CSystem translate(UCollaboration uCollaboration) {
    UModel uModel = uCollaboration.getModel();
    CSystem cSystem = new CSystem();
    ModelSymbolTable modelSymbolTable = new ModelSymbolTable(uCollaboration, cSystem);

    // Translate each class of the model
    for (UClass uClass : uModel.getClasses())
      cSystem.addClass(translateClass(uClass, modelSymbolTable));

    // Translate collaboration
    if (!uCollaboration.isEmpty())
      cSystem.addMethod(translateCollaboration(uCollaboration, modelSymbolTable));

    return cSystem;
  }

  @NonNullByDefault
  public static CClass translateClass(UClass uClass, ModelSymbolTable symbolTable) {
    ClassifierSymbolTable classifierSymbolTable = symbolTable.getClassifierSymbolTable(uClass);

    // Translation of attributes (including getters and setters)
    for (UAttribute uAttribute : uClass.getAttributes())
      classifierSymbolTable.createAttribute(uAttribute);

    // Translation of receptions
    for (UReception uReception : uClass.getReceptions())
      classifierSymbolTable.createMethod(uReception);

    // Translation of operations
    for (UOperation uOperation : uClass.getOperations())
      classifierSymbolTable.createMethod(uOperation);

    CClass cClass = classifierSymbolTable.getCClass();

    // Translation of state machine
    UStateMachine uStateMachine = uClass.getStateMachine();
    if (uStateMachine != null) {
      SMachine sMachine = uStateMachine.getMachine(classifierSymbolTable.getProperties());
      SmileTranslator.translate(sMachine, classifierSymbolTable);

      cClass.addHeader("eventpool");
      cClass.getConstructor().addBlockStatement(new CCode(classifierSymbolTable.getEventQueueAccess() + " = " + CKeywords.NEW + " " + ModelSymbolTable.EVENTPOOLCLASSNAME + "();"));
      cClass.addMethod(runMethod(sMachine, classifierSymbolTable));
    }

    return cClass; 
  }

  @NonNullByDefault
  public static CMethod translateCollaboration(UCollaboration uCollaboration, ModelSymbolTable modelSymbolTable) {
    CMethod cMain = new CMethod("main", CKeywords.INT, Arrays.asList(new CParameter("argc", CKeywords.INT), new CParameter("argv", CKeywords.CHAR + "**")), CKeywords.PUBLIC);

    // Create objects
    for (UObject uObject : uCollaboration.getObjects()) {
      UClass uClass = uObject.getC1ass();
      ClassifierSymbolTable classifierSymbolTable = modelSymbolTable.getClassifierSymbolTable(uClass);
      String cClassName = classifierSymbolTable.getCClass().getName();
      cMain.getBody().addBlockStatement(new CCode(cClassName + "* " + uObject.getName() + " = " + CKeywords.NEW + " " + cClassName + "();"));
    }

    // Initialise objects
    for (UObject uObject : uCollaboration.getObjects()) {
      UClass uClass = uObject.getC1ass();
      ClassifierSymbolTable classifierSymbolTable = modelSymbolTable.getClassifierSymbolTable(uClass);
      for (USlot uSlot : uObject.getSlots()) {
        UAttribute uAttribute = uSlot.getAttribute();
        String setterName = classifierSymbolTable.getAttributeSetterName(uAttribute);
        if (setterName == null)
          continue;

        StringBuilder valueBuilder = new StringBuilder();
        if (uAttribute.isArray()) {
          valueBuilder.append(CKeywords.NEW);
          valueBuilder.append(" ");
          valueBuilder.append(classifierSymbolTable.getTypeName(uAttribute.getType()));
          valueBuilder.append("{ ");
        }
        valueBuilder.append(translateExpressions(uSlot.getValues(), uSlot.getValuesContext(), classifierSymbolTable));
        if (uAttribute.isArray())
          valueBuilder.append(" }");
        
        cMain.getBody().addBlockStatement(new CCode(uObject.getName() + "->" + setterName + "(" + valueBuilder.toString() + ");"));
      }
    }
    cMain.getBody().addBlockStatement(new CCode(""));
   
    // Create and start object threads
    for (UObject uObject : uCollaboration.getObjects()) {
      UClass uClass = uObject.getC1ass();
      ClassifierSymbolTable classifierSymbolTable = modelSymbolTable.getClassifierSymbolTable(uClass);
      String cClassName = classifierSymbolTable.getCClass().getName();
      if (uClass.getStateMachine() != null) {
        cMain.getBody().addBlockStatement(new CCode("std::thread " + uObject.getName() + "_thd(&" + cClassName + "::" + ClassifierSymbolTable.RUNMETHODNAME + ", " + uObject.getName() + ");"));
      }
    }
    cMain.getBody().addBlockStatement(new CCode(""));

    // Wait for all object threads to join
    for (UObject uObject : uCollaboration.getObjects()) {
      if (uObject.getC1ass().getStateMachine() != null)
        cMain.getBody().addBlockStatement(new CCode(uObject.getName() + "_thd.join();"));
    }
    cMain.getBody().addBlockStatement(new CCode(""));

    cMain.getBody().addBlockStatement(new CCode("return 0;"));
    return cMain;
  }

  @NonNullByDefault
  public static CMethod runMethod(SMachine sMachine, ClassifierSymbolTable symbolTable) {
    CMethod runMethod = new CMethod(ClassifierSymbolTable.RUNMETHODNAME, CKeywords.VOID, Arrays.asList(), CKeywords.PUBLIC);
    runMethod.getBody().addBlockStatement(new CCode(ModelSymbolTable.EVENTCLASSNAME + "* " + symbolTable.getEventAccess() + " = " + CKeywords.NULLPTR + ";"));
    runMethod.getBody().addBlockStatement(new CCode(""));
    runMethod.getBody().addBlockStatement(new CCode(CKeywords.THIS + "->" + ClassifierSymbolTable.INITMETHODNAME + "();"));
    CSequence main = new CSequence();
    if (!sMachine.getTerminated().isFalse())
      main.addBlockStatement(new CIfElse(CKeywords.THIS + "->" + ClassifierSymbolTable.TERMMETHODNAME + "()", new CCode(CKeywords.RETURN + ";"), null));
    main.addBlockStatement(new CCode(symbolTable.getEventAccess() + " = " + symbolTable.getEventQueueAccess() + "->fetch();"));
    main.addBlockStatement(new CCode(CKeywords.THIS + "->" + ClassifierSymbolTable.HANDLEMETHODNAME + "(" + symbolTable.getEventAccess() + ");"));
    runMethod.getBody().addBlockStatement(new CWhile(CKeywords.TRUE, main.asStatement()));
    return runMethod;
  }

  @NonNullByDefault
  private static class ActionTranslator implements UAction.InContextVisitor<CSequence> {
    private ClassifierSymbolTable symbolTable;
    private UContext uContext;

    /**
     * Create a new UML action translator.
     * 
     * @param symbolTable class symbol table to use
     * @param uContext UML Context for this action
     */
    public ActionTranslator(ClassifierSymbolTable symbolTable, UContext uContext) {
      this.symbolTable = symbolTable;
      this.uContext = uContext;
    }

    /**
     * Translate a UML action into a C++ statement sequence.
     * 
     * @param uAction action to be translated
     * @return C++ statement sequence that represents {@code uAction}
     */
    public CSequence translate(UAction uAction) {
      return uAction.accept(this);
    }

    /**
     * Determine the underlying context of this UML action translator.
     * 
     * @returns UML action translator's underlying context
     */
    public @NonNull UContext getContext() {
      return this.uContext;
    }

    @Override
    public CSequence onSkip() {
      return new CSequence();
    }

    @Override
    public CSequence onChoice(UAction left, UAction right) {
      return translate(left);
    }

    @Override
    public CSequence onParallel(UAction left, UAction right) {
      return translate(UAction.par(left, right).getInterleavingNormalForm());
    }

    @Override
    public CSequence onSequential(UAction left, UAction right) {
      CSequence cSequence = translate(left);
      cSequence.addBlockStatement(translate(right));
      return cSequence;
    }

    @Override
    public CSequence onConditional(UExpression condition, UAction left, UAction right) {
      CSequence cSequence = new CSequence();
      String cCondition = UMLTranslator.translateExpression(condition, this.uContext, this.symbolTable);
      CSequence cLeft = translate(left);
      CSequence cRight = null;
      if (right != null && !right.isSkip())
        cRight = translate(right);
      cSequence.addBlockStatement(new CIfElse(cCondition, cLeft.asStatement(), cRight != null ? cRight.asStatement() : null));
      return cSequence;
    }

    @Override
    public CSequence onAssertion(UExpression expression) {
      CSequence cSequence = new CSequence();
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("assert(");
      resultBuilder.append(new ExpressionTranslator(this.symbolTable, this.uContext).translate(expression));
      resultBuilder.append(");");
      cSequence.addBlockStatement(new CCode(resultBuilder.toString()));
      return cSequence;
    }

    @Override
    public CSequence onSelfInvocation(UBehavioural uBehavioural, List<UExpression> parameters) {
      var target = uBehavioural.isStatic() ? (uBehavioural.getOwner().getName() + "::") : (CKeywords.THIS + "->");
      return this.translateBehaviouralCall(target, uBehavioural, parameters);
    }

    @Override
    public CSequence onInvocation(UStatual statual, UBehavioural uBehavioural, List<UExpression> parameters) {
      var target = this.translateStatual(statual) + "->";
      return this.translateBehaviouralCall(target, uBehavioural, parameters);
    }

    @Override
    public CSequence onArrayInvocation(UAttribute attribute, UExpression offset, UBehavioural uBehavioural, List<UExpression> parameters) {
      var target = "(" + this.translateStatual(attribute) + "[" + translateExpression(offset) + "])->";
      return this.translateBehaviouralCall(target, uBehavioural, parameters);
    }

    @Override
    public CSequence onAssignment(UAttribute attribute, UExpression expression) {
      CSequence cSequence = new CSequence();
      cSequence.addBlockStatement(new CCode(translateStatual(attribute) + " = " + translateExpression(expression) + ";"));
      return cSequence;
    }

    @Override
    public CSequence onArrayAssignment(UAttribute attribute, UExpression offset, UExpression expression) {
      CSequence cSequence = new CSequence();
      cSequence.addBlockStatement(new CCode(translateStatual(attribute) + "[" + translateExpression(offset) + "] = " + translateExpression(expression) + ";"));
      return cSequence;
    }

    @Override
    public CSequence onPost(UAttribute attribute, UAction.PostKind kind) {
      CSequence cSequence = new CSequence();
      cSequence.addBlockStatement(new CCode(translateStatual(attribute) + ((kind == UAction.PostKind.DEC) ? "--;" : "++;")));
      return cSequence;
    }

    @Override
    public CSequence onArrayPost(UAttribute attribute, UExpression offset, UAction.PostKind kind) {
      CSequence cSequence = new CSequence();
      cSequence.addBlockStatement(new CCode(translateStatual(attribute) + "[" + translateExpression(offset) + "]" + ((kind == UAction.PostKind.DEC) ? "--;" : "++;")));
      return cSequence;
    }

    /**
     * Translate a UML expression.
     * 
     * @param expression UML expression
     * @return Java code for <CODE>expression</CODE>
     */
    private String translateExpression(UExpression uExpression) {
      return new ExpressionTranslator(this.symbolTable, this.uContext).translate(uExpression);
    }

    /**
     * Translate a UML statual.
     * 
     * @param statual UML statual
     * @return Java code for <CODE>statual</CODE>
     */
    private String translateStatual(UStatual uStatual) {
      return new ExpressionTranslator(this.symbolTable, this.uContext).onStatual(uStatual);
    }

    /**
     * Translate a UML behavioural call.
     * 
     * @param target call target string
     * @param uBehavioural a UML behavioural
     * @param uArguments a list of UML argument expression
     * @return Java code for call to <CODE>uBehavioural</CODE> with <CODE>uArguments</CODE>
     */
    private CSequence translateBehaviouralCall(String target, UBehavioural uBehavioural, List<UExpression> arguments) {
      var cSequence = new CSequence();
      StringBuilder resultBuilder = new StringBuilder();
      /*
      var uAction = this.uAction;
      if (this.symbolTable.getProperties().isCycleBased() && uBehavioural.cases(o -> true, r -> false) && !uBehavioural.isStatic() && uAction != null) {
        // Register the acknowledgement event that is waited for next
        resultBuilder.append(this.symbolTable.getEventQueueAccess());
        resultBuilder.append("->waitforacknowledgement(");
        resultBuilder.append(this.symbolTable.getAcknowledgementName(uAction));
        resultBuilder.append(");");
        cSequence.addBlockStatement(new CCode(resultBuilder.toString()));
      }
      resultBuilder = new StringBuilder();
      */
      resultBuilder.append(target);
      resultBuilder.append(this.symbolTable.getMethodName(uBehavioural));
      if (this.symbolTable.getProperties().isCycleBased() && uBehavioural.cases(o -> true, r -> false) && !uBehavioural.isStatic())
        resultBuilder.append(Formatter.tuple(Stream.concat(Stream.of(this.symbolTable.getEventQueueAccess()), arguments.stream().map(argument -> this.translateExpression(argument))).collect(toList())));
      else
        resultBuilder.append(Formatter.tuple(arguments.stream().map(argument -> this.translateExpression(argument)).collect(toList())));
      resultBuilder.append(";");
      cSequence.addBlockStatement(new CCode(resultBuilder.toString()));
      return cSequence;
    }
  }

  /**
   * Translate a UML action into a C++ statement sequence.
   * 
   * @param uAction a UML action
   * @param uContext a UML context
   * @param symbolTable a class symbol table
   * @return C++ statement sequence for {@code uAction}
   */
  @NonNullByDefault
  public static CSequence translateAction(UAction action, UContext uContext, ClassifierSymbolTable symbolTable) {
    return new ActionTranslator(symbolTable, uContext).translate(action);
  }

  /**
   * Translator for UML expressions.
   */
  private static class ExpressionTranslator implements UExpression.InContextVisitor<@NonNull String> {
    private ClassifierSymbolTable symbolTable;
    private @NonNull UContext uContext;

    /**
     * Constructor
     * 
     * @param symbolTable ModelSymbolTable of UML Class
     * @param uContext UML Context
     */
    public ExpressionTranslator(ClassifierSymbolTable symbolTable, @NonNull UContext uContext) {
      this.symbolTable = symbolTable;
      this.uContext = uContext;
    }

    /**
     * Translate a UML expression into a Java expression string.
     *
     * @param uExpression UML expression
     * @return Java expression string for <CODE>uExpression</CODE>
     */
    private @NonNull String translate(UExpression uExpression) {
      return uExpression.accept(this);
    }

    /**
     * Returns the UML context.
     * 
     * @return UML context
     */
    public UContext getContext() {
      return uContext;
    }

    public @NonNull String onBooleanConstant(boolean booleanConstant) {
      return booleanConstant ? CKeywords.TRUE : CKeywords.FALSE;
    }

    public @NonNull String onIntegerConstant(int integerConstant) {
      return Integer.toString(integerConstant);
    }

    public @NonNull String onStringConstant(String stringConstant) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("\"");
      resultBuilder.append(stringConstant);
      resultBuilder.append("\"");
      return resultBuilder.toString();
    }

    public @NonNull String onNull() {
      return CKeywords.NULLPTR;
    }

    public @NonNull String onThis() {
      return CKeywords.THIS;
    }

    /**
     * Translate UML statual.
     * 
     * If the statual is a class attribute, this method gets the real name from
     * the translator's model system table. Otherwise it is assumed that the
     * statual is a parameter, which was delivered by a reception or operation.
     * In this case, the method gets the value of the parameter of the current
     * event.
     * 
     * @param statual UML attribute or parameter of an operation or reception
     */
    public @NonNull String onStatual(UStatual uStatual) {
      return uStatual.cases(uAttribute -> { return this.symbolTable.getAttributeAccess(uAttribute); },
                            uParameter -> { return this.symbolTable.getParameterAccess(uParameter); },
                            uObject -> { return uObject.getName(); });
    }

    /**
     * Translate an access to an array attribute
     */
    public @NonNull String onArray(UAttribute attribute, UExpression offset) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(onStatual(attribute));
      resultBuilder.append("[");
      resultBuilder.append(translate(offset));
      resultBuilder.append("]");
      return resultBuilder.toString();
    }

    /**
     * Translate conditional expression.
     */
    public @NonNull String onConditional(UExpression conditionExpression, UExpression trueExpression, UExpression falseExpression) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("((");
      resultBuilder.append(translate(conditionExpression));
      resultBuilder.append(") ? ");
      resultBuilder.append(translate(trueExpression));
      resultBuilder.append(" : ");
      resultBuilder.append(translate(falseExpression));
      resultBuilder.append(")");
      return resultBuilder.toString();
    }

    /**
     * Translates unary expression
     */
    public @NonNull String onUnary(UOperator unary, UExpression expression) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("(");
      resultBuilder.append(unary.getName());
      resultBuilder.append(translate(expression));
      resultBuilder.append(")");
      return resultBuilder.toString();
    }

    /**
     * Translates a arithmetical operation between two expressions
     */
    public @NonNull String onArithmetical(UExpression leftExpression, UOperator arithmetical, UExpression rightExpression) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("(");
      resultBuilder.append(translate(leftExpression));
      resultBuilder.append(arithmetical.getName());
      resultBuilder.append(translate(rightExpression));
      resultBuilder.append(")");
      return resultBuilder.toString();
    }

    /**
     * Translates a relational operation between two expressions
     */
    public @NonNull String onRelational(UExpression leftExpression, UOperator relational, UExpression rightExpression) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("(");
      resultBuilder.append(translate(leftExpression));
      resultBuilder.append(relational.getName());
      resultBuilder.append(translate(rightExpression));
      resultBuilder.append(")");
      return resultBuilder.toString();
    }

    /**
     * Translates a junctional operation between two expressions
     */
    public @NonNull String onJunctional(UExpression leftExpression, UOperator junctional, UExpression rightExpression) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("(");
      resultBuilder.append(translate(leftExpression));
      resultBuilder.append(junctional.getName());
      resultBuilder.append(translate(rightExpression));
      resultBuilder.append(")");
      return resultBuilder.toString();
    }
  }

  /**
   * Translate a UML expression into a C++ expression string.
   * 
   * @param uExpression a UML expression
   * @param uContext a UML context
   * @param symbolTable a class symbol table
   * @return C++ expression string for {@code uExpression}
   */
  public static @NonNull String translateExpression(UExpression uExpression, @NonNull UContext uContext, ClassifierSymbolTable symbolTable) {
    return new ExpressionTranslator(symbolTable, uContext).translate(uExpression);
  }

  /**
   * Translate a list of UML expressions into a comma separated C++ expression string.
   * 
   * @param uExpressions a list of UML expressions
   * @param uContext a UML context
   * @param symbolTable a class symbol table
   * @return comma separated C++ expression string for {@code uExpressions}
   */
  public static @NonNull String translateExpressions(@NonNull List<@NonNull UExpression> uExpressions, @NonNull UContext uContext, ClassifierSymbolTable symbolTable) {
    StringBuilder resultBuilder = new StringBuilder();
    String sep = "";
    for (UExpression uExpression : uExpressions) {
      resultBuilder.append(sep);
      resultBuilder.append(translateExpression(uExpression, uContext, symbolTable));
      sep = ", ";
    }
    return resultBuilder.toString();
  }
}
