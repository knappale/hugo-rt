package translation.uml2java;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import javacode.JBlock;
import javacode.JBlockStatement;
import javacode.JClass;
import javacode.JCode;
import javacode.JIfElse;
import javacode.JKeywords;
import javacode.JMethod;
import javacode.JParameter;
import javacode.JSequence;
import javacode.JStatement;
import javacode.JSystem;
import javacode.JTryCatch;
import uml.UAction;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UCollaboration;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import uml.UOperation;
import uml.UOperator;
import uml.UReception;
import uml.USlot;
import uml.UStatual;
import uml.statemachine.UStateMachine;
import util.Formatter;

import static java.util.stream.Collectors.toList;


/**
 * Translator for UML elements, classes, attributes, expressions and actions.
 * 
 * @author <a href="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</a>
 */
public class UMLTranslator {
  /**
   * Create Java code for a UML collaboration.
   * 
   * @param uCollaboration a UML collaboration
   * @return Java code for {@code uCollaboration}
   */
  @NonNullByDefault
  public static JSystem translate(UCollaboration uCollaboration) {
    ModelSymbolTable modelSymbolTable = new ModelSymbolTable(uCollaboration);
    JSystem jSystem = new JSystem();

    // Translate each class of the model
    for (UClass uClass : uCollaboration.getModel().getClasses())
      jSystem.addClass(translateClass(uClass, modelSymbolTable));

    // Translate collaboration
    if (!uCollaboration.isEmpty()) {
      var jClass = modelSymbolTable.getJClass(uCollaboration);
      jClass.addMethod(translateCollaboration(uCollaboration, modelSymbolTable));
      jSystem.addClass(jClass);
    }

    return jSystem;
  }

  @NonNullByDefault
  public static JClass translateClass(UClass uClass, ModelSymbolTable modelSymbolTable) {
    ClassifierSymbolTable classifierSymbolTable = modelSymbolTable.getClassifierSymbolTable(uClass);

    // Translation of attributes (including getters and setters)
    for (UAttribute uAttribute : uClass.getAttributes())
      classifierSymbolTable.createAttribute(uAttribute);

    // Translation of receptions
    for (UReception uReception : uClass.getReceptions())
      classifierSymbolTable.createMethod(uReception);

    // Translation of operations
    for (UOperation uOperation : uClass.getOperations())
      classifierSymbolTable.createMethod(uOperation);

    // Translation of state machine
    UStateMachine uStateMachine = uClass.getStateMachine();
    if (uStateMachine != null)
      SmileTranslator.translate(uStateMachine.getMachine(classifierSymbolTable.getProperties()), classifierSymbolTable);

    return classifierSymbolTable.getJClass();
  }

  @NonNullByDefault
  public static JMethod translateCollaboration(UCollaboration uCollaboration, ModelSymbolTable modelSymbolTable) {
    var jBlock = new JBlock();

    // Create objects
    for (UObject uObject : uCollaboration.getObjects()) {
      UClass uClass = uObject.getC1ass();
      ClassifierSymbolTable classifierSymbolTable = modelSymbolTable.getClassifierSymbolTable(uClass);
      String jClassName = classifierSymbolTable.getJClass().getName();
      jBlock.addBlockStatement(new JCode(jClassName + " " + uObject.getName() + " = " + JKeywords.NEW + " " + jClassName + "();"));
    }

    // Initialise objects
    for (UObject uObject : uCollaboration.getObjects()) {
      UClass uClass = uObject.getC1ass();
      ClassifierSymbolTable classifierSymbolTable = modelSymbolTable.getClassifierSymbolTable(uClass);
      for (USlot uSlot : uObject.getSlots()) {
        UAttribute uAttribute = uSlot.getAttribute();
        String setterName = classifierSymbolTable.getAttributeSetterName(uAttribute);
        if (setterName == null)
          continue;

        StringBuilder valueBuilder = new StringBuilder();
        if (uAttribute.isArray()) {
          valueBuilder.append(JKeywords.NEW);
          valueBuilder.append(" ");
          valueBuilder.append(classifierSymbolTable.getTypeName(uAttribute.getType()));
          valueBuilder.append("{ ");
        }
        valueBuilder.append(translateExpressions(uSlot.getValues(), uSlot.getValuesContext(), classifierSymbolTable));
        if (uAttribute.isArray())
          valueBuilder.append(" }");
        
        jBlock.addBlockStatement(new JCode(uObject.getName() + "." + setterName + "(" + valueBuilder.toString() + ");"));
      }
    }
    jBlock.addBlockStatement(new JCode(""));
   
    // Create object threads
    for (UObject uObject : uCollaboration.getObjects()) {
      if (uObject.getC1ass().getStateMachine() != null)
        jBlock.addBlockStatement(new JCode("Thread " + uObject.getName() + "Thd = " + JKeywords.NEW + " Thread(" + uObject.getName() + ");"));
    }
    // Start object threads
    for (UObject uObject : uCollaboration.getObjects()) {
      if (uObject.getC1ass().getStateMachine() != null)
        jBlock.addBlockStatement(new JCode(uObject.getName() + "Thd.start();"));
    }
    jBlock.addBlockStatement(new JCode(""));

    // Wait for all object threads to join
    var tryJBlock = new JBlock();
    for (UObject uObject : uCollaboration.getObjects()) {
      if (uObject.getC1ass().getStateMachine() != null)
        tryJBlock.addBlockStatement(new JCode(uObject.getName() + "Thd.join();"));
    }
    jBlock.addBlockStatement(new JTryCatch(tryJBlock, "InterruptedException ie"));

    // jBlock.addBlockStatement(new JCode(""));
    // jBlock.addBlockStatement(new JCode("return 0;"));
    return new JMethod("main", JKeywords.PUBLIC + " " + JKeywords.STATIC, JKeywords.VOID, Arrays.asList(new JParameter("args", "String[]")), jBlock);
  }

  /**
   * Translator for UML actions.
   */
  @NonNullByDefault
  private static class ActionTranslator implements UAction.InContextVisitor<JBlockStatement> {
    private ClassifierSymbolTable symbolTable;
    private UContext uContext;

    /**
     * Create a new UML action translator.
     * 
     * @param symbolTable class symbol table to use
     * @param uContext context for this action
     */
    public ActionTranslator(ClassifierSymbolTable symbolTable, UContext uContext) {
      this.symbolTable = symbolTable;
      this.uContext = uContext;
    }

    /**
     * Translate a UML action into a Java block statement.
     * 
     * @param uAction action to be translated
     * @return Java block statement that represents {@code uAction}
     */
    public JBlockStatement translate(UAction uAction) {
      return uAction.accept(this);
    }

    @Override
    public @NonNull UContext getContext() {
      return this.uContext;
    }

    @Override
    public JSequence onSkip() {
      return new JSequence();
    }

    @Override
    public JBlockStatement onChoice(UAction left, UAction right) {
      return translate(left);
    }

    @Override
    public JBlockStatement onParallel(UAction left, UAction right) {
      return translate(UAction.par(left, right).getInterleavingNormalForm());
    }

    @Override
    public JBlockStatement onSequential(UAction left, UAction right) {
      JSequence jSequence = new JSequence();
      jSequence.addBlockStatement(translate(left));
      jSequence.addBlockStatement(translate(right));
      return jSequence;
    }

    @Override
    public JBlockStatement onConditional(UExpression condition, UAction left, UAction right) {
      String jCondition = translateExpression(condition);
      JStatement jLeft = translate(left).getStatement();
      @Nullable JStatement jRight = (right != null && !right.isSkip()) ? translate(right).getStatement() : null;
      return new JIfElse(jCondition, jLeft, jRight);
    }

    @Override
    public JBlockStatement onAssertion(UExpression uExpression) {
      JSequence jSequence = new JSequence();
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("assert(");
      resultBuilder.append(translateExpression(uExpression));
      resultBuilder.append(");");
      jSequence.addBlockStatement(new JCode(resultBuilder.toString()));
      return jSequence;
    }

    @Override
    public JBlockStatement onSelfInvocation(UBehavioural uBehavioural, List<UExpression> parameters) {
      var target = uBehavioural.isStatic() ? uBehavioural.getOwner().getName() : JKeywords.THIS;
      return this.translateBehaviouralCall(target, uBehavioural, parameters);
    }

    @Override
    public JBlockStatement onInvocation(UStatual uStatual, UBehavioural uBehavioural, List<UExpression> parameters) {
      var target = this.translateStatual(uStatual);
      return this.translateBehaviouralCall(target, uBehavioural, parameters);
    }

    @Override
    public JBlockStatement onArrayInvocation(UAttribute uAttribute, UExpression offset, UBehavioural uBehavioural, List<UExpression> parameters) {
      var target = this.translateStatual(uAttribute) + "[" + translateExpression(offset) + "]";
      return this.translateBehaviouralCall(target, uBehavioural, parameters);
    }

    @Override
    public JBlockStatement onAssignment(UAttribute uAttribute, UExpression uExpression) {
      JSequence jSequence = new JSequence();
      jSequence.addBlockStatement(new JCode(translateStatual(uAttribute) + " = " + translateExpression(uExpression) + ";"));
      return jSequence;
    }

    @Override
    public JBlockStatement onArrayAssignment(UAttribute uAttribute, UExpression offset, UExpression uExpression) {
      JSequence jSequence = new JSequence();
      jSequence.addBlockStatement(new JCode(translateStatual(uAttribute) + "[" + translateExpression(offset) + "] = " + translateExpression(uExpression) + ";"));
      return jSequence;
    }

    @Override
    public JBlockStatement onPost(UAttribute uAttribute, UAction.PostKind kind) {
      JSequence jSequence = new JSequence();
      jSequence.addBlockStatement(new JCode(translateStatual(uAttribute) + ((kind == UAction.PostKind.DEC) ? "--;" : "++;")));
      return jSequence;
    }

    @Override
    public JBlockStatement onArrayPost(UAttribute uAttribute, UExpression offset, UAction.PostKind kind) {
      JSequence jSequence = new JSequence();
      jSequence.addBlockStatement(new JCode(translateStatual(uAttribute) + "[" + translateExpression(offset) + "]" + ((kind == UAction.PostKind.DEC) ? "--;" : "++;")));
      return jSequence;
    }

    /**
     * Translate a UML expression.
     * 
     * @param expression UML expression
     * @return Java code for {@code expression}
     */
    private String translateExpression(UExpression uExpression) {
      return new ExpressionTranslator(this.symbolTable, this.uContext).translate(uExpression);
    }

    /**
     * Translate a UML statual.
     * 
     * @param uStatual UML statual
     * @return Java code for {@code uStatual}
     */
    private String translateStatual(UStatual uStatual) {
      return new ExpressionTranslator(this.symbolTable, this.uContext).translate(uStatual);
    }

    /**
     * Translate a UML behavioural call.
     * 
     * @param target call target string
     * @param uBehavioural a UML behavioural
     * @param arguments a list of UML argument expression
     * @return Java code for call to {@code uBehavioural} with {@code arguments}
     */
    private JSequence translateBehaviouralCall(String target, UBehavioural uBehavioural, List<UExpression> arguments) {
      var jSequence = new JSequence();
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(target);
      resultBuilder.append(".");
      resultBuilder.append(this.symbolTable.getMethodName(uBehavioural));
      if (this.symbolTable.getProperties().isCycleBased() && uBehavioural.cases(o -> true, r -> false) && !uBehavioural.isStatic())
        resultBuilder.append(Formatter.tuple(Stream.concat(Stream.of(this.symbolTable.getEventQueueAccess()), arguments.stream().map(argument -> this.translateExpression(argument))).collect(toList())));
      else
        resultBuilder.append(Formatter.tuple(arguments.stream().map(argument -> this.translateExpression(argument)).collect(toList())));
      resultBuilder.append(";");
      jSequence.addBlockStatement(new JCode(resultBuilder.toString()));
      return jSequence;
    }
  }

  /**
   * Translate a UML action into a Java block statement.
   * 
   * @param action a UML action
   * @param context a UML context
   * @param symbolTable a class symbol table
   * @return Java block statement for {@code action}
   */
  @NonNullByDefault
  public static JBlockStatement translateAction(UAction action, UContext context, ClassifierSymbolTable symbolTable) {
    return new ActionTranslator(symbolTable, context).translate(action);
  }

  /**
   * Translator for UML expressions.
   */
  private static class ExpressionTranslator implements UExpression.InContextVisitor<@Nullable Void> {
    private ClassifierSymbolTable symbolTable;
    private @NonNull UContext uContext;
    private @NonNull StringBuilder resultBuilder;

    /**
     * Constructor
     * 
     * @param symbolTable classifier symbol table
     * @param uContext context
     */
    public ExpressionTranslator(ClassifierSymbolTable symbolTable, @NonNull UContext uContext) {
      this.resultBuilder = new StringBuilder();
      this.symbolTable = symbolTable;
      this.uContext = uContext;
    }

    /**
     * Translate a UML expression into a Java expression string.
     *
     * @param uExpression UML expression
     * @return Java expression string for {@code uExpression}
     */
    private @NonNull String translate(UExpression uExpression) {
      uExpression.accept(this);
      return this.resultBuilder.toString();
    }

    /**
     * Translate a UML statual into a Java expression string.
     *
     * @param uStatual UML statual
     * @return Java expression string for {@code uStatual}
     */
    private @NonNull String translate(@NonNull UStatual uStatual) {
      this.onStatual(uStatual);
      return this.resultBuilder.toString();
    }

    @Override
    public UContext getContext() {
      return uContext;
    }

    @Override
    public @Nullable Void onBooleanConstant(boolean booleanConstant) {
      if (booleanConstant)
        resultBuilder.append("true");
      else
        resultBuilder.append("false");
      return null;
    }

    @Override
    public @Nullable Void onIntegerConstant(int integerConstant) {
      resultBuilder.append(integerConstant);
      return null;
    }

    @Override
    public @Nullable Void onStringConstant(String stringConstant) {
      resultBuilder.append("\"");
      resultBuilder.append(stringConstant);
      resultBuilder.append("\"");
      return null;
    }

    @Override
    public @Nullable Void onNull() {
      resultBuilder.append("null");
      return null;
    }

    @Override
    public @Nullable Void onThis() {
      resultBuilder.append("this");
      return null;
    }

    /**
     * Translate UML statual.
     * 
     * If the statual is a class attribute, this method gets the real name from
     * the translator's model system table. Otherwise it is assumed that the
     * statual is a parameter, which was delivered by a reception or operation.
     * In this case, the method gets the value of the parameter of the current
     * event.
     * 
     * @param statual UML attribute or parameter of an operation or reception
     */
    @Override
    public @Nullable Void onStatual(UStatual uStatual) {
      uStatual.cases(uAttribute -> resultBuilder.append(symbolTable.getAttributeAccess(uAttribute)),
                     uParameter -> resultBuilder.append(symbolTable.getParameterAccess(uParameter)),
                     uObject -> resultBuilder.append(uObject.getName()));
      return null;
    }

    @Override
    public @Nullable Void onArray(UAttribute attribute, UExpression offset) {
      onStatual(attribute);
      resultBuilder.append("[");
      translate(offset);
      resultBuilder.append("]");
      return null;
    }

    @Override
    public @Nullable Void onConditional(UExpression conditionExpression, UExpression trueExpression, UExpression falseExpression) {
      resultBuilder.append("((");
      translate(conditionExpression);
      resultBuilder.append(") ? ");
      translate(trueExpression);
      resultBuilder.append(" : ");
      translate(falseExpression);
      resultBuilder.append(")");
      return null;
    }

    @Override
    public @Nullable Void onUnary(UOperator unary, UExpression expression) {
      resultBuilder.append("(");
      resultBuilder.append(unary.getName());
      translate(expression);
      resultBuilder.append(")");
      return null;
    }

    @Override
    public @Nullable Void onArithmetical(UExpression leftExpression, UOperator arithmetical, UExpression rightExpression) {
      resultBuilder.append("(");
      translate(leftExpression);
      resultBuilder.append(arithmetical.getName());
      translate(rightExpression);
      resultBuilder.append(")");
      return null;
    }

    @Override
    public @Nullable Void onRelational(UExpression leftExpression, UOperator relational, UExpression rightExpression) {
      resultBuilder.append("(");
      translate(leftExpression);
      resultBuilder.append(relational.getName());
      translate(rightExpression);
      resultBuilder.append(")");
      return null;
    }

    @Override
    public @Nullable Void onJunctional(UExpression leftExpression, UOperator junctional, UExpression rightExpression) {
      resultBuilder.append("(");
      translate(leftExpression);
      resultBuilder.append(junctional.getName());
      translate(rightExpression);
      resultBuilder.append(")");
      return null;
    }
  }

  /**
   * Translate a UML expression into a Java expression string.
   * 
   * @param uExpression a UML expression
   * @param context a UML context
   * @param symbolTable a class symbol table
   * @return Java expression string for <CODE>expression</CODE>
   */
  public static @NonNull String translateExpression(UExpression uExpression, @NonNull UContext context, ClassifierSymbolTable symbolTable) {
    return new ExpressionTranslator(symbolTable, context).translate(uExpression);
  }

  /**
   * Translate a list of UML expressions into a comma separated Java expression string.
   * 
   * @param uExpressions a list of UML expressions
   * @param uContext a UML context
   * @param symbolTable a class symbol table
   * @return comma separated Java expression string for {@code uExpressions}
   */
  public static @NonNull String translateExpressions(@NonNull List<@NonNull UExpression> uExpressions, @NonNull UContext uContext, ClassifierSymbolTable symbolTable) {
    StringBuilder resultBuilder = new StringBuilder();
    String sep = "";
    for (UExpression uExpression : uExpressions) {
      resultBuilder.append(sep);
      resultBuilder.append(translateExpression(uExpression, uContext, symbolTable));
      sep = ", ";
    }
    return resultBuilder.toString();
  }
}
