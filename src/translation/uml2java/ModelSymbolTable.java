package translation.uml2java;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import javacode.JClass;
import javacode.JKeywords;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UClassifier;
import uml.UCollaboration;
import uml.UElement;
import uml.UType;

import static util.Objects.requireNonNull;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
class ModelSymbolTable extends SymbolTable {
  private static final String DEFAULTCLASSVISIBILITY = JKeywords.PUBLIC;
  
  private UCollaboration uCollaboration;

  private Map<UElement, JClass> javaClasses = new HashMap<>();
  private Map<UClassifier, ClassifierSymbolTable> classifierSymbolTables = new HashMap<>();
  private Set<String> classNames = new HashSet<String>();

  ModelSymbolTable(UCollaboration uCollaboration) {
    super(uCollaboration.getModel());
    this.uCollaboration = uCollaboration;
    this.classNames.addAll(JKeywords.getKeywords());
  }

  @Override
  control.Properties getProperties() {
    return this.uCollaboration.getProperties();
  }

  /**
   * Determine the classifier symbol table for a UML classifier.
   *
   * @param uClassifier a UML classifier
   * @return the classifier symbol table for {@link uClassifier}
   */
  ClassifierSymbolTable getClassifierSymbolTable(UClassifier uClassifier) {
    ClassifierSymbolTable classSymbolTable = this.classifierSymbolTables.get(uClassifier);
    if (classSymbolTable != null)
      return classSymbolTable;

    classSymbolTable = new ClassifierSymbolTable(uClassifier, this);
    this.classifierSymbolTables.put(uClassifier, classSymbolTable);
    return classSymbolTable;
  }

  /**
   * Determine the Java class for a UML classifier.
   *
   * @param uClassifier a UML classifier
   * @return the Java class for {@line uClassifier}
   */
  JClass getJClass(UClassifier uClassifier) {
    JClass jClass = this.javaClasses.get(uClassifier);
    if (jClass != null)
      return jClass;

    String className = uniqueClassName(uClassifier.getName());
    jClass = new JClass(this.getProperties().getJavaPackageName(), className, DEFAULTCLASSVISIBILITY);
    this.javaClasses.put(uClassifier, jClass);
    return jClass;
  }

  /**
   * Determine the Java class for a UML collaboration.
   *
   * @param uCollaboration a UML collaboration
   * @return the Java class for {@line uCollaboration}
   */
  JClass getJClass(UCollaboration uCollaboration) {
    JClass jClass = this.javaClasses.get(uCollaboration);
    if (jClass != null)
      return jClass;

    String className = uniqueClassName(uCollaboration.getName());
    jClass = new JClass(this.getProperties().getJavaPackageName(), className, DEFAULTCLASSVISIBILITY);
    this.javaClasses.put(uCollaboration, jClass);
    return jClass;
  }

  /**
   * Get the name of the Java type representing a UML type.
   * 
   * @param uType a UML type
   * @return name of the Java type for {@link uType}
   */
  String getTypeName(UType uType) {
    String jTypeName = "";
    if (uType.getUnderlyingType().isDataType()) {
      if (this.getModel().getIntegerType().subsumes(uType.getUnderlyingType()))
        jTypeName = JKeywords.INT;
      else {
        if (this.getModel().getBooleanType().subsumes(uType.getUnderlyingType()))
          jTypeName = JKeywords.BOOLEAN;
        else {
          if (this.getModel().getBooleanType().subsumes(uType.getUnderlyingType()))
            jTypeName = "String";
        }
      }
    }
    else
      jTypeName = this.getClassifierSymbolTable(requireNonNull((UClass)uType.getUnderlyingType().getClassifier())).getJClass().getName();
    if (uType.isArray())
      jTypeName += "[]";
    return jTypeName;
  }

  /**
   * Get an access expression to the Java attribute representing a UML attribute.
   * 
   * @param uAttribute a UML attribute
   * @return a Java access expression to the Java attribute for {@link uAttribute}
   */
  String getAttributeAccess(UAttribute uAttribute) {
    return this.getClassifierSymbolTable(uAttribute.getOwner()).getAttributeAccess(uAttribute);
  }
  
  /**
   * Get the name of the Java method representing a UML behavioural.
   * 
   * @param uBehavioural a UML behavioural
   * @return name of the Java method for {@code uBehavioural}
   */
  String getMethodName(UBehavioural uBehavioural) {
    return this.getClassifierSymbolTable(uBehavioural.getOwner()).getMethodName(uBehavioural);
  }

  /**
   * Determine a unique name for a class name.
   * 
   * @param className a class name
   * @return a unique class name
   */
  private String uniqueClassName(String className) {
    if (className.equals(""))
      className = "C";
    else {
      if (className.length() == 1)
        className = className.toUpperCase();
      else
        className = className.substring(0, 1).toUpperCase() + className.substring(1);
    }
    String uniqueClassName = this.newName(className, this.classNames);
    this.classNames.add(uniqueClassName);
    return uniqueClassName;
  }
}
