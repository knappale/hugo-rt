package translation.uml2java;

import java.util.Collection;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import javacode.JKeywords;
import uml.UModel;
import uml.UType;


/**
 * @author <A HREF="mailto:Max.Raba@ifi.lmu.de">Max Raba</A>
 * @author <A HREF="knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
abstract class SymbolTable {
  private UModel uModel;

  SymbolTable(UModel uModel) {
    this.uModel = uModel;
  }

  /**
   * @return symbol table's underlying model
   */
  UModel getModel() {
    return this.uModel;
  }

  /**
   * @return symbol table's underlying properties
   */
  control.Properties getProperties() {
    return this.uModel.getProperties();
  }

  /**
   * Get the name of the Java type representing a UML type.
   * 
   * @param uType a UML type
   * @return name of the Java type for {@code uType}
   */
  abstract String getTypeName(UType uType);

  String getBoxing(UType uType, @Nullable String uExpression) {
    if (uExpression == null)
      uExpression = "";

    String boxingClass = "";
    if (uType.isDataType()) {
      if (this.getModel().getIntegerType().subsumes(uType))
        boxingClass = JKeywords.INTEGERCLASS;
      else {
        if (this.getModel().getBooleanType().subsumes(uType))
          boxingClass = JKeywords.BOOLEANCLASS;
      }
    }

    if (boxingClass.equals(""))
      return uExpression;
    else
      return JKeywords.NEW + " " + boxingClass + "(" + uExpression + ")";
  }

  String getUnboxing(UType uType, @Nullable String uExpression) {
    if (uExpression == null)
      uExpression = "";

    String unboxingClass = "";
    String unboxingMethod = "";
    if (uType.isDataType()) {
      if (this.getModel().getIntegerType().subsumes(uType)) {
        unboxingClass = JKeywords.INTEGERCLASS;
        unboxingMethod = "intValue";
      }
      else {
        if (this.getModel().getBooleanType().subsumes(uType)) {
          unboxingClass = JKeywords.BOOLEANCLASS;
          unboxingMethod = "booleanValue";
        }
      }
    }
    else
      unboxingClass = this.getTypeName(uType);

    if (unboxingMethod.equals(""))
      return "(" + unboxingClass + ")" + uExpression;
    else
      return "((" + unboxingClass + ")" + uExpression + ")." + unboxingMethod + "()";
  }

  /**
   * Determine a new name based on a name, but not occurring in a set of names.
   * 
   * @param name a name
   * @param names a collection of names
   * @return a new name based on {@link name} not occurring in {@link names}
   */
  protected String newName(String variableName, Collection<String> names) {
    char[] characters = variableName.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    String newName = new String(characters);
    if (newName.equals(""))
      newName = "n";
    if (names.contains(newName)) {
      String tmpName;
      int counter = 0;
      do {
        tmpName = newName + counter++;
      } while (names.contains(tmpName));
      newName = tmpName;
    }
    return newName;
  }
}
