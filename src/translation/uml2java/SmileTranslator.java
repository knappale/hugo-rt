package translation.uml2java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import javacode.JAttribute;
import javacode.JBlock;
import javacode.JBlockStatement;
import javacode.JBranch;
import javacode.JBreak;
import javacode.JCase;
import javacode.JClass;
import javacode.JCode;
import javacode.JCycle;
import javacode.JGoto;
import javacode.JIfElse;
import javacode.JIfs;
import javacode.JKeywords;
import javacode.JLabel;
import javacode.JLoop;
import javacode.JMethod;
import javacode.JParameter;
import javacode.JReturn;
import javacode.JSequence;
import javacode.JSwitch;
import javacode.JVariableDeclaration;
import javacode.JWhile;
import uml.UAction;
import uml.UContext;
import uml.UExpression;
import uml.UParameter;
import uml.smile.SBranch;
import uml.smile.SBranching;
import uml.smile.SGuard;
import uml.smile.SLiteral;
import uml.smile.SMachine;
import uml.smile.SPrimitive;
import uml.smile.SStatement;
import uml.smile.SValue;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import util.Formatter;
import util.Pair;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;


/**
 * Translate Smile machine into Java code.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class SmileTranslator {
  /**
   * Translate a Smile machine.
   * 
   * The state machine method is added to the Java class of the class
   * symbol table.
   * 
   * @param sMachine a Smile machine
   * @param symbolTable a class symbol table
   */
  public static void translate(SMachine sMachine, ClassifierSymbolTable symbolTable) {
    JClass jClass = symbolTable.getJClass();
    jClass.addImplementedInterface("Runnable");

    // Attributes
    jClass.addAttribute(new JAttribute(ClassifierSymbolTable.EVENTQUEUENAME, ClassifierSymbolTable.EVENTQUEUECLASSNAME, "", JKeywords.PRIVATE));

    // Constructor
    JBlock constructorBody = new JBlock();
    Set<UState> waitStates = sMachine.getStateMachine().getWaitStates();
    if (!waitStates.isEmpty()) {
      var waitStateEvents = "waitStateEvents";
      constructorBody.addBlockStatement(new JCode("java.util.Set<Integer> " + waitStateEvents + " = new java.util.HashSet<>();"));
      for (UState waitState : sMachine.getStateMachine().getWaitStates())
        constructorBody.addBlockStatement(new JCode(waitStateEvents + ".add((int)" + symbolTable.getEventName(UEvent.completion(waitState)) + ");"));
      constructorBody.addBlockStatement(new JCode(symbolTable.getEventQueueAccess() + " = new " + ClassifierSymbolTable.EVENTQUEUECLASSNAME + "(" + waitStateEvents + ");"));
    }
    else {
      constructorBody.addBlockStatement(new JCode(symbolTable.getEventQueueAccess() + " = new " + ClassifierSymbolTable.EVENTQUEUECLASSNAME + "();"));
    }
    jClass.addConstructor(JKeywords.PUBLIC, new ArrayList<>(), constructorBody);

    // Methods
    JMethod startMethod = new JMethod(ClassifierSymbolTable.STARTMETHODNAME, JKeywords.PUBLIC, JKeywords.VOID, new ArrayList<>());
    startMethod.getBody().addBlockStatement(new JCode("(new Thread(this)).start();"));
    jClass.addMethod(startMethod);

    jClass.addMethods(getInitMethods(sMachine, symbolTable));
    if (symbolTable.getProperties().isEventBased()) {
      for (UEvent uEvent : sMachine.getStateMachine().getEvents())
        jClass.addMethods(getEventMethods(uEvent, sMachine, symbolTable));
      jClass.addMethod(getEventBasedRunMethod(sMachine, symbolTable));
      jClass.addAttributes(symbolTable.getVariableDeclarations().stream().map(JVariableDeclaration::getAttribute).collect(toSet()));
    }
    else {
      jClass.addMethod(getMachineBasedRunMethod(sMachine, symbolTable));
    }
    jClass.addAttributes(symbolTable.getConstantDeclarations().stream().map(JVariableDeclaration::getClassAttribute).collect(toSet()));
  }

  /**
   * Create Java methods for initialising a Smile machine.
   * 
   * @param sMachine a Smile Machine
   * @param symbolTable {@code sMachine}'s underlying classifier symbol table
   * @return Java methods for initialisation
   */
  private static Set<JMethod> getInitMethods(SMachine sMachine, ClassifierSymbolTable symbolTable) {
    var initMethods = new HashSet<JMethod>();
    var method = symbolTable.getInitMethod();
    initMethods.add(method);
    if (symbolTable.getProperties().isCycleBased())
      initMethods.addAll(getCyclesMethods(method, sMachine.getInitialisationCycles(), symbolTable));
    else
      method.getBody().addBlockStatement(translateStatement(sMachine.getInitialisation(), symbolTable));
    return initMethods;
  }

  private static Set<JMethod> getEventMethods(UEvent uEvent, SMachine sMachine, ClassifierSymbolTable symbolTable) {
    var eventMethods = new HashSet<JMethod>();
    var uStateMachine = symbolTable.getUStateMachine(); assert uStateMachine != null;
    var parameters = uEvent.getParameters(uStateMachine).stream().
                                map(uParameter -> new JParameter(symbolTable.getParameterName(uParameter.getName()), symbolTable.getTypeName(uParameter.getType()))).collect(toList());

    var method = new JMethod(symbolTable.getEventMethodName(uEvent), JKeywords.PRIVATE, JKeywords.VOID, parameters);
    eventMethods.add(method);
    if (symbolTable.getProperties().isCycleBased())
      eventMethods.addAll(getCyclesMethods(method, sMachine.getReactionCycles(uEvent), symbolTable));
    else
      method.getBody().addBlockStatement(translateStatement(sMachine.getReaction(uEvent), symbolTable));
    return eventMethods;
  }

  private static Set<JMethod> getCyclesMethods(JMethod method, Pair<SStatement, IdentityHashMap<SPrimitive, SStatement>> cycles, ClassifierSymbolTable symbolTable) {
    var cyclesMethods = new HashSet<JMethod>();
    method.getBody().addBlockStatement(translateStatement(cycles.getFirst(), symbolTable));
    var resumptionsMap = cycles.getSecond();
    for (var suspender : resumptionsMap.keySet()) {
      var resumption = resumptionsMap.get(suspender); assert resumption != null;
      var resumptionMethod = method = new JMethod(symbolTable.getResumptionMethodName(suspender), JKeywords.PRIVATE, JKeywords.VOID, method.getParameters());
      resumptionMethod.getBody().addBlockStatement(translateStatement(resumption, symbolTable));
      cyclesMethods.add(resumptionMethod);
    }
    return cyclesMethods;
  }

  private static JCode getLocalMethodCall(String name, List<UParameter> uParameters, ClassifierSymbolTable symbolTable) {
    StringBuilder callBuilder = new StringBuilder();
    callBuilder.append(JKeywords.THIS);
    callBuilder.append(".");
    callBuilder.append(name);
    callBuilder.append("(");
    String separator = "";
    for (UParameter uParameter : uParameters) {
      callBuilder.append(separator);
      callBuilder.append(symbolTable.getUnboxing(uParameter.getType(), ClassifierSymbolTable.CURRENTEVENTNAME + ".getArgument(" + uParameter.getNumber() + ")"));
      separator = ", ";
    }
    callBuilder.append(");");
    return new JCode(callBuilder.toString());
  }

  private static List<JCase> getCyclesHandlingMethods(IdentityHashMap<SPrimitive, SStatement> resumptionsMap, List<UParameter> uParameters, ClassifierSymbolTable symbolTable) {
    List<JCase> cases = new ArrayList<>();
    for (var suspender : resumptionsMap.keySet()) {
      var resumption = resumptionsMap.get(suspender); assert resumption != null;
      cases.add(new JCase(symbolTable.getAcknowledgementName(suspender),
                          new JSequence(getLocalMethodCall(symbolTable.getResumptionMethodName(suspender), uParameters, symbolTable), new JBreak())));
    }
    return cases;
  }

  private static JMethod getEventBasedRunMethod(SMachine sMachine, ClassifierSymbolTable symbolTable) {
    JMethod runMethod = new JMethod(ClassifierSymbolTable.RUNMETHODNAME, JKeywords.PUBLIC, JKeywords.VOID, new ArrayList<>());
    runMethod.getBody().addBlockStatement(new JCode(JKeywords.THIS + "." + ClassifierSymbolTable.INITMETHODNAME + "();"));

    JSequence main = new JSequence();
    main.addBlockStatement(translateStatement(SBranching.choice(Arrays.asList(sMachine.getTermination())), symbolTable));
    main.addBlockStatement(new JCode(ClassifierSymbolTable.EVENTCLASSNAME + " " + ClassifierSymbolTable.CURRENTEVENTNAME + " = " + symbolTable.getEventQueueAccess() + ".fetch();"));
    List<JCase> handleCases = new ArrayList<>();
    for (UEvent uEvent : sMachine.getStateMachine().getEvents()) {
      var uParameters = uEvent.getParameters(sMachine.getStateMachine());
      handleCases.add(new JCase(symbolTable.getEventName(uEvent),
                                new JSequence(getLocalMethodCall(symbolTable.getEventMethodName(uEvent), uParameters, symbolTable), new JBreak())));
    }
    if (symbolTable.getProperties().isCycleBased()) {
      for (UEvent uEvent : sMachine.getStateMachine().getEvents())
        handleCases.addAll(getCyclesHandlingMethods(sMachine.getReactionCycles(uEvent).getSecond(), uEvent.getParameters(sMachine.getStateMachine()), symbolTable));
      handleCases.addAll(getCyclesHandlingMethods(sMachine.getInitialisationCycles().getSecond(), new ArrayList<>(), symbolTable));
    }
    main.addBlockStatement(new JSwitch(ClassifierSymbolTable.CURRENTEVENTNAME + ".getId()", handleCases, null));
    runMethod.getBody().addBlockStatement(new JWhile(JKeywords.TRUE, main.getStatement()));
    return runMethod;
  }

  private static JMethod getMachineBasedRunMethod(SMachine sMachine, ClassifierSymbolTable symbolTable) {
    JMethod runMethod = new JMethod(ClassifierSymbolTable.RUNMETHODNAME, JKeywords.PUBLIC, JKeywords.VOID, new ArrayList<>());
    runMethod.getBody().addBlockStatement(translateStatement(sMachine.getStatement(), symbolTable));
    // runMethod.getBody().addVariables(symbolTable.getConstantDeclarations());
    runMethod.getBody().addVariables(symbolTable.getVariableDeclarations());
    return runMethod;
  }

  /**
   * Translator for Smile statements.
   * 
   * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
   */
  private static class StatementTranslator implements SStatement.Visitor<JBlockStatement> {
    private ClassifierSymbolTable symbolTable;
    private Set<JGoto> gotos = new HashSet<>();
    private Set<JCycle> targetCycles = new HashSet<>();
    private int loopLabelNumber = 0;
    private Stack<Integer> loopLabelNumbers = new Stack<>();

    public StatementTranslator(ClassifierSymbolTable symbolTable) {
      this.symbolTable = symbolTable;
    }

    private JCode suspend(SStatement sStatement) {
      if (!this.symbolTable.getProperties().isCycleBased() || !(sStatement instanceof SPrimitive) || ((SPrimitive)sStatement).suspended() == null)
        return new JCode("");
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(this.symbolTable.getEventQueueAccess());
      resultBuilder.append(".waitForAcknowledgement(");
      resultBuilder.append(this.symbolTable.getAcknowledgementName((SPrimitive)sStatement));
      resultBuilder.append(");");
      return new JCode(resultBuilder.toString());
    }

    /**
     * Translate a (top) Smile statement.
     *
     * Here the contained gotos are taken into account, by back-patching the
     * registered target cycles.
     * 
     * @param sStatement a Smile statement
     * @return resulting cycle
     */
    public JCycle translate(SStatement sStatement) {
      JCycle jCycle = JCycle.sequential(JCycle.blockStatement(suspend(sStatement)), sStatement.accept(this).asJCycle());

      int targetCycleCounter = 0;
      for (JCycle targetCycle : this.targetCycles) {
        if (targetCycle.backpatch("cycle" + targetCycleCounter, this.gotos))
          targetCycleCounter++;
      }

      return jCycle;
    }

    /**
     * Translate an inner Smile statement.
     * 
     * After turning the translation result into a cycle, the comments of
     * the Smile statement are added.  If the Smile statement can be the
     * target of a goto, the resulting cycle is registered as a target.
     *
     * @param sStatement a Smile statement
     * @return resulting cycle
     */
    private JCycle translateStatement(SStatement sStatement) {
      JCycle jCycle = JCycle.sequential(JCycle.blockStatement(suspend(sStatement)), sStatement.accept(this).asJCycle());
      this.targetCycles.add(jCycle);
      return jCycle;
    }

    private String translateExpression(SGuard sExpression) {
      return SmileTranslator.translateExpression(sExpression, this.symbolTable);
    }

    private void pushLoopLabelNumber() {
      this.loopLabelNumbers.push(this.loopLabelNumber++);
    }
    
    private int getLoopLabelNumber() {
      return this.loopLabelNumbers.peek().intValue();
    }

    private JLabel getLoopLabel() {
      return new JLabel("loop" + getLoopLabelNumber());
    }

    private void popLoopLabelNumber() {
      this.loopLabelNumbers.pop();
    }

    @Override
    public JBlockStatement onSkip() {
      return new JSequence();
    }

    @Override
    public JBlockStatement onAtomic(SStatement statement) {
      return translateStatement(statement);
    }

    @Override
    public JCycle onSequential(SStatement sLeft, SStatement sRight) {
      JCycle jLeft = translateStatement(sLeft);
      JCycle jRight = translateStatement(sRight);
      this.targetCycles.remove(jLeft);
      this.targetCycles.remove(jRight);
      return JCycle.sequential(jLeft, jRight);
    }

    @Override
    public JBlockStatement onChoice(List<SBranch> sBranches, @Nullable SStatement sElseStm) {
      if (sBranches.size() == 1)
        return new JIfs(this.onBranch(sBranches.get(0).getGuard(), sBranches.get(0).getEffect()), sElseStm != null ? translateStatement(sElseStm).getStatement() : null);
 
      List<JBranch> jBranches = new ArrayList<>();
      for (SBranch sBranch : sBranches) {
        JBranch jBranch = this.onBranch(sBranch.getGuard(), sBranch.getEffect());
        if (!jBranch.hasEmptyCycle())
          jBranches.add(jBranch);
      }

      JBranch jElseBranch = null;
      if (sElseStm != null)
        jElseBranch = translateStatement(sElseStm).asBranch();

      return new JIfs(jBranches, jElseBranch);
    }

    @Override
    public JBlockStatement onLoop(List<SBranch> sBranches, @Nullable SStatement sElseStm) {
      this.pushLoopLabelNumber();
      List<JBranch> jBranches = new ArrayList<JBranch>();
      JBranch jElseStm = null;
      for (SBranch sBranch : sBranches)
        jBranches.add(this.onBranch(sBranch.getGuard(), sBranch.getEffect()));
      if (sElseStm != null)
        jElseStm = translateStatement(sElseStm).asBranch();
      JLoop jLoop = new JLoop(this.getLoopLabel(), jBranches, jElseStm);
      this.popLoopLabelNumber();
      return jLoop;
    }

    @Override
    public JBranch onBranch(SGuard sGuard, SStatement sStatement) {
      String jGuard = translateExpression(sGuard);
      JCycle jStatement = translateStatement(sStatement);
      return JBranch.guarded(jGuard, jStatement);
    }

    @Override
    public JBlockStatement onBreak() {
      return new JBreak(this.getLoopLabel());
    }

    @Override
    public JBlockStatement onAssignment(SVariable sVariable, SValue sValue) {
      return new JCode(this.symbolTable.getVariableAccess(sVariable) + " = " + translateValue(sValue, this.symbolTable) + ";");
    }

    @Override
    public JBlockStatement onAssignment(SVariable sVariable, SVariable otherSVariable) {
      return new JCode(this.symbolTable.getVariableAccess(sVariable) + " = " + this.symbolTable.getVariableAccess(otherSVariable) + ";");
    }

    @Override
    public JBlockStatement onExternal(UAction external, UContext context) {
      return UMLTranslator.translateAction(external, context, this.symbolTable);
    }

    @Override
    public JBlockStatement onSuccess() {
      JSequence jSuccess = new JSequence();
      jSuccess.addBlockStatement(new JCode(this.symbolTable.getEventQueueAccess() + ".debug(\"Exiting successfully.\");"));
      jSuccess.addBlockStatement(new JReturn());
      return jSuccess;
    }

    @Override
    public JBlockStatement onFail() {
      JSequence jFail = new JSequence();
      jFail.addBlockStatement(new JCode(this.symbolTable.getEventQueueAccess() + ".debug(\"Exiting with failure.\");"));
      jFail.addBlockStatement(new JCode("System.exit(-1);"));
      return jFail;
    }

    @Override
    public JBlockStatement onAwait(SGuard sExpression) {
      return new JIfElse(translateExpression(SGuard.neg(sExpression)), this.onFail().getStatement(), null);
    }

    @Override
    public JBlockStatement onInitialisation() {
      return new JSequence();
    }

    @Override
    public JBlockStatement onFetch(Set<UState> waitStates) {
      return new JCode(ClassifierSymbolTable.EVENTCLASSNAME + " " + ClassifierSymbolTable.CURRENTEVENTNAME + " = " + symbolTable.getEventQueueAccess() + ".fetch();");
    }

    @Override
    public JBlockStatement onAcknowledge() {
      if (this.symbolTable.getProperties().isCycleBased())
        return new JCode(this.symbolTable.getEventQueueAccess() + ".sendAcknowledgement();");
      return new JCode(this.symbolTable.getEventQueueAccess() + ".acknowledge();");
    }

    @Override
    public JBlockStatement onDefer() {
      return new JCode(this.symbolTable.getEventQueueAccess() + ".defer();");
    }

    @Override
    public JBlockStatement onChosen() {
      @Nullable UStateMachine uStateMachine = this.symbolTable.getUStateMachine();
      if (uStateMachine == null || uStateMachine.getDeferrableEvents().isEmpty())
        return new JSequence();
      return new JCode(this.symbolTable.getEventQueueAccess() + ".chosen();");
    }

    @Override
    public JBlockStatement onComplete(boolean keep, UState uState) {
      if (keep)
        return new JCode(this.symbolTable.getEventQueueAccess() + ".complete(" + this.symbolTable.getEventName(UEvent.completion(uState)) + ");");
      return new JCode(this.symbolTable.getEventQueueAccess() + ".insertCompletion(" + this.symbolTable.getEventName(UEvent.completion(uState)) + ");");
    }

    @Override
    public JBlockStatement onUncomplete(UState uState) {
      return new JCode(this.symbolTable.getEventQueueAccess() + ".uncomplete(" + this.symbolTable.getEventName(UEvent.completion(uState)) + ");");
    }

    @Override
    public JBlockStatement onStartTimer(boolean keep, UEvent event) {
      String timelow = UMLTranslator.translateExpression(event.getTimeLowExpression(), event.getTimeContext(), this.symbolTable);
      String timehigh = UMLTranslator.translateExpression(event.getTimeHighExpression(), event.getTimeContext(), this.symbolTable);
      return new JCode(this.symbolTable.getEventQueueAccess() + ".startTimer(" + this.symbolTable.getEventName(event) + ", " + timelow + ", " + timehigh + ");");
    }

    @Override
    public JBlockStatement onStopTimer(UEvent event) {
      return new JCode(this.symbolTable.getEventQueueAccess() + ".stopTimer(" + this.symbolTable.getEventName(event) + ");");
    }
  }

  static JCycle translateStatement(SStatement sStatement, ClassifierSymbolTable symbolTable) {
    return new StatementTranslator(symbolTable).translate(sStatement);
  }

  /**
   * Translator for Smile expressions.
   * 
   * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
   */
  private static class ExpressionTranslator implements SGuard.Visitor<String>, SLiteral.Visitor<String> {
    private ClassifierSymbolTable symbolTable;

    ExpressionTranslator(ClassifierSymbolTable symbolTable) {
      this.symbolTable = symbolTable;
    }

    /**
     * Translate a Smile expression.
     * 
     * @param sExpression a Smile expression
     * @return Java code string representation of {@code sExpression}
     */
    public String translate(SGuard sExpression) {
      return sExpression.accept(this);
    }

    @Override
    public String onBooleanConstant(boolean booleanConstant) {
      return booleanConstant ? "true" : "false";
    }

    @Override
    public String onEq(boolean positive, SVariable sVariable, Set<SValue> sValues) {
      if (sValues.isEmpty())
        return positive ? "false" : "true";

      Function<SValue, @Nullable String> eqFun = sValue -> this.symbolTable.getVariableAccess(sVariable) + " == " + translateValue(sValue, this.symbolTable);
      String eq = Formatter.separated(sValues, eqFun, " || ");
      return positive ? (sValues.size() > 1 ? Formatter.parenthesised(eq) : eq) : "!(" + eq + ")";
    }

    @Override
    public String onMatch(boolean positive, Set<UEvent> uEvents) {
      if (uEvents.isEmpty())
        return positive ? "false" : "true";

      Function<UEvent, @Nullable String> matchFun = uEvent -> ClassifierSymbolTable.CURRENTEVENTNAME + ".getId() == " + this.symbolTable.getEventName(uEvent);
      String match = Formatter.separated(uEvents, matchFun, " || ");
      if (uEvents.size() > 1)
        match = Formatter.parenthesised(match);
      return positive ? (uEvents.size() > 1 ? Formatter.parenthesised(match) : match) : "!(" + match + ")";
    }

    @Override
    public String onIsCompleted(boolean positive, UState uState) {
      String isCompleted = this.symbolTable.getEventQueueAccess() + ".isCompleted(" + this.symbolTable.getEventName(UEvent.completion(uState)) + ")";
      return positive ? isCompleted : "!(" + isCompleted + ")";
    }

    @Override
    public String onIsTimedOut(boolean positive, UEvent event) {
      String isTimedOut = this.symbolTable.getEventQueueAccess() + ".isTimedOut(" + this.symbolTable.getEventName(event) + ")";
      return positive ? isTimedOut : "!(" + isTimedOut + ")";
    }

    @Override
    public String onExternal(boolean positive, UExpression uExpression, UContext uContext) {
      String external = UMLTranslator.translateExpression(uExpression, uContext, symbolTable);
      return positive ? external : "!(" + external + ")";
    }

    @Override
    public String onLiteral(SLiteral literal) {
      return literal.accept(this);
    }

    @Override
    public String onAnd(SGuard left, SGuard right) {
      StringBuilder resultBuilder = new StringBuilder("(");
      resultBuilder.append(translate(left));
      resultBuilder.append(" && ");
      resultBuilder.append(translate(right));
      return resultBuilder.append(")").toString();
    }

    @Override
    public String onOr(SGuard left, SGuard right) {
      StringBuilder resultBuilder = new StringBuilder("(");
      resultBuilder.append(translate(left));
      resultBuilder.append(" || ");
      resultBuilder.append(translate(right));
      return resultBuilder.append(")").toString();
    }
  }

  static String translateExpression(SGuard sExpression, ClassifierSymbolTable symbolTable) {
    String jExpression = new ExpressionTranslator(symbolTable).translate(sExpression);
    return jExpression;
  }

  private static String translateValue(SValue sValue, ClassifierSymbolTable symbolTable) {
    return sValue.new Cases<String>().booleanConstant(booleanConstant -> booleanConstant ? "true" : "false").
                                      constant(sConstant -> symbolTable.getConstantName(sConstant)).
                                      apply();
  }
}
