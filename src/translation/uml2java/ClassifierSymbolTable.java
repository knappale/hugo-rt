package translation.uml2java;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javacode.JAttribute;
import javacode.JBlock;
import javacode.JClass;
import javacode.JCode;
import javacode.JKeywords;
import javacode.JMethod;
import javacode.JParameter;
import javacode.JVariableDeclaration;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UClassifier;
import uml.UParameter;
import uml.UType;
import uml.smile.SConstant;
import uml.smile.SPrimitive;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UCompoundTransition;
import util.Pair;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * A classifier symbol table stores temporary information about a UML classifier,
 * such as mapping from UML attributes, operations, and receptions
 * to their Java equivalents.
 */
@NonNullByDefault
class ClassifierSymbolTable extends SymbolTable {
  private static final String DEFAULTMETHODVISIBILITY = JKeywords.PUBLIC;
  private static final String DEFAULTINSTANCEVARIABLEVISIBILITY = JKeywords.PRIVATE;
  private static final String DEFAULTCLASSVARIABLEVISIBILITY = JKeywords.PUBLIC;
  static final String EVENTCLASSNAME = "runtime.Event";
  static final String EVENTQUEUECLASSNAME = "runtime.EventQueue";
  static final String EVENTQUEUENAME = "eventQueue";
  static final String CURRENTEVENTNAME = "currentEvent";
  static final String INITMETHODNAME = "init";
  static final String STARTMETHODNAME = "start";
  static final String RUNMETHODNAME = "run";
  
  private ModelSymbolTable modelSymbolTable;
  private UClassifier uClassifier;

  private Map<UAttribute, JAttribute> attributes = new HashMap<>();
  private Map<JAttribute, String> attributeSetterNames = new HashMap<>();
  private @Nullable JMethod initMethod = null;
  private Map<UBehavioural, JMethod> externalMethods = new HashMap<>();
  private Map<UEvent, String> eventMethodNames = new HashMap<>();
  private IdentityHashMap<SPrimitive, String> resumptionMethodNames = new IdentityHashMap<>();
  private IdentityHashMap<SPrimitive, JVariableDeclaration> acknowledgements = new IdentityHashMap<>();
  private Map<Object, JVariableDeclaration> variables = new HashMap<>();
  private Map<Object, JVariableDeclaration> constants = new HashMap<>();

  private @Nullable String eventType = null;
  private Set<String> attributeNames = new HashSet<>();
  private Set<String> methodNames = new HashSet<>();
  private Set<String> variableNames = new HashSet<>();

  private int statesCounter = 1;
  private int transitionsCounter = 1;
  private int eventsCounter = 1;

  /**
   * Create a new classifier symbol table.
   * 
   * @param uClassifier a UML classifier
   * @return class symbol table for {@link uClassifier}
   */
  ClassifierSymbolTable(UClassifier uClassifier, ModelSymbolTable modelSymbolTable) {
    super(uClassifier.getModel());
    this.uClassifier = uClassifier;
    this.modelSymbolTable = modelSymbolTable;
    
    this.attributeNames.add(EVENTQUEUENAME);
    this.methodNames.add(RUNMETHODNAME);
    this.methodNames.add(STARTMETHODNAME);
    this.variableNames.add(EVENTQUEUENAME);
    this.variableNames.add(CURRENTEVENTNAME);
  }

  /**
   * Determine the classifier symbol table of a UML classifier.
   * 
   * @param uClassifier a UML classifier
   * @return classifier symbol table for {@code uClassifier}
   */
  ClassifierSymbolTable getClassifierSymbolTable(UClassifier uClassifier) {
    return this.modelSymbolTable.getClassifierSymbolTable(uClassifier);
  }

  /**
   * Determine the UML classifier of this classifier symbol table.
   * 
   * @return classifier symbol table's underlying UML classifier
   */
  UClassifier getUClassifier() {
    return this.uClassifier;
  }

  /**
   * Determine the UML state machine of this classifier symbol table.
   * 
   * @return classifier symbol table's underlying UML state machine or {@code null} if
   *         no state machine underlies this symbol table
   */
  @Nullable UStateMachine getUStateMachine() {
    try {
      UClass uClass = (UClass)this.uClassifier;
      return uClass.getStateMachine();
    }
    catch (ClassCastException cce) {
      return null;
    }
  }

  @Override
  control.Properties getProperties() {
    return this.modelSymbolTable.getProperties();
  }

  /**
   * Determine the Java class of this classifier symbol table.
   * 
   * @return class symbol table's underlying Java class
   */
  JClass getJClass() {
    return this.modelSymbolTable.getJClass(this.uClassifier);
  }

  @Override
  String getTypeName(UType uType) {
    return this.modelSymbolTable.getTypeName(uType);
  }

  /**
   * @return Java expression for accessing classifier symbol table's underlying Java class' event queue
   */
  String getEventQueueAccess() {
    return JKeywords.THIS + "." + EVENTQUEUENAME;
  }

  /**
   * Determine the name of the Java variable representing a Smile variable.
   * 
   * @param sVariable a Smile variable
   * @return name of a Java variable representing {@link sVariable}
   */
  private String getVariableName(SVariable sVariable) {
    JVariableDeclaration jVariable = this.variables.get(sVariable);
    if (jVariable != null)
      return jVariable.getName();

    String name = uniqueVariableName(sVariable.getName());
    Pair<String, String> typeValue = sVariable.new Cases<Pair<String, String>>().
                      flag(v -> new Pair<>(JKeywords.BOOLEAN, JKeywords.FALSE)).
                      state(v -> new Pair<>(this.getStateType(), "0")).
                      transition(v -> new Pair<>(this.getTransitionType(), "0")).
                      apply();
    jVariable = new JVariableDeclaration(name, typeValue.getFirst(), typeValue.getSecond());
    this.variables.put(sVariable, jVariable);
    return jVariable.getName();
  }

  /**
   * @return Java expression for accessing a Smile variable
   */
  String getVariableAccess(SVariable sVariable) {
    return (this.getProperties().isEventBased() ? JKeywords.THIS + "." : "") + this.getVariableName(sVariable);
  }

  /**
   * Determine the name of the Java constant representing a Smile constant.
   * 
   * @param sConstant a Smile constant
   * @return name of a Java constant representing {@link sConstant}
   */
  String getConstantName(SConstant sConstant) {
    return sConstant.new Cases<String>().
      empty(() -> "0").
      vertex(uVertex -> getStateName(uVertex)).
      transition(uCompoundTransition -> getTransitionName(uCompoundTransition)).
      event(uEvent -> getEventName(uEvent)).
      apply();
  }

  /**
   * Determine the Java name of a UML event.
   *
   * @param uEvent a UML event
   * @return the Java name for {@link uEvent}
   */
  String getEventName(UEvent uEvent) {
    JVariableDeclaration jConstant = this.constants.get(uEvent);
    if (jConstant != null)
      return jConstant.getName();

    String constantName = uniqueVariableName(uEvent.getName());
    jConstant = new JVariableDeclaration(constantName, this.getEventType(), "" + this.eventsCounter++, true);
    this.constants.put(uEvent, jConstant);
    return jConstant.getName();
  }

  /**
   * Determine the Java type for representing events.
   *
   * This is the minimal integer type for tracking any event.
   *
   * @return the Java type for representing events
   */
  private String getEventType() {
    var eventType = this.eventType;
    if (eventType != null)
      return eventType;

    int numberOfEvents = 0;
    UStateMachine uStateMachine = this.getUStateMachine();
    if (uStateMachine != null)
      numberOfEvents = uStateMachine.getEvents().size();
    eventType = JKeywords.getMinimalType(numberOfEvents);
    return eventType;
  }

  /**
   * Get the name for "acknowledging" a Smile primitive
   *
   * @param sPrimitive a UML action
   * @return the name for acknowledging {@code sPrimitive}
   */
  String getAcknowledgementName(SPrimitive sPrimitive) {
    JVariableDeclaration jConstant = this.acknowledgements.get(sPrimitive);
    if (jConstant != null)
      return jConstant.getName();

    String name = sPrimitive.toString();
    String constantName = uniqueVariableName("ack" + name.substring(0, 1).toUpperCase() + name.substring(1, name.length()));
    jConstant = new JVariableDeclaration(constantName, this.getEventType(), "" + this.eventsCounter++, true);
    this.acknowledgements.put(sPrimitive, jConstant);
    this.constants.put(jConstant, jConstant);
    return jConstant.getName();
  }

  /**
   * Determine the Java name of a UML state vertex.
   *
   * @param uVertex a UML state vertex
   * @return the Java name for {@link uState}
   */
  private String getStateName(UVertex uVertex) {
    JVariableDeclaration jConstant = this.constants.get(uVertex);
    if (jConstant != null)
      return jConstant.getName();

    String constantName = uniqueVariableName(uVertex.getIdentifier().toString());
    jConstant = new JVariableDeclaration(constantName, this.getStateType(), "" + this.statesCounter++, true);
    this.constants.put(uVertex, jConstant);
    return jConstant.getName();
  }

  /**
   * Determine the Java type for representing state vertices.
   *
   * This is the minimal integer type for tracking any state vertex.
   *
   * @return the Java type for representing state vertices
   */
  private String getStateType() {
    int numberOfStates = 0;
    UStateMachine uStateMachine = this.getUStateMachine();
    if (uStateMachine != null)
      numberOfStates = uStateMachine.getVertices().size();
    return JKeywords.getMinimalType(numberOfStates);
  }

  /**
   * @param uTransition a UML compound transition
   * @return a name for {@link uTransition}
   */
  private String getTransitionName(UCompoundTransition uTransition) {
    JVariableDeclaration jConstant = this.constants.get(uTransition);
    if (jConstant != null)
      return jConstant.getName();

    String constantName = uniqueVariableName(uTransition.getName());
    jConstant = new JVariableDeclaration(constantName, this.getTransitionType(), "" + this.transitionsCounter++, true);
    this.constants.put(uTransition, jConstant);
    return jConstant.getName();
  }

  /**
   * Determine the Java type for representing compound transitions.
   *
   * This is the minimal integer type for tracking any compound
   * transition.
   *
   * @return the Java type for representing compound transitions
   */
  private String getTransitionType() {
    int numberOfTransitions = 0;
    UStateMachine uStateMachine = this.getUStateMachine();
    if (uStateMachine != null)
      numberOfTransitions = uStateMachine.getCompounds().size();
    return JKeywords.getMinimalType(numberOfTransitions);
  }

  /**
   * Determine the Java access expression to a UML attribute.
   *
   * @param uAttribute a UML attribute
   * @return Java access expression to {@link uAttribute}
   */
  String getAttributeAccess(UAttribute uAttribute) {
    if (!uAttribute.getOwner().equals(this.uClassifier))
      return this.modelSymbolTable.getAttributeAccess(uAttribute);

    JAttribute jAttribute = createAttribute(uAttribute);
    if (uAttribute.isStatic())
      return this.getClassifierSymbolTable(uAttribute.getOwner()).getJClass().getName() + "." + jAttribute.getName();
    return JKeywords.THIS + "." + jAttribute.getName();
  }

  /**
   * Create a Java attribute representing a UML attribute.
   *
   * Also the infrastructure for this attribute, i.e., a getter and a setter,
   * are created.
   *
   * @param uAttribute a UML attribute
   * @return a Java attribute for {@link uAttribute}
   * @pre uAttribute.getOwner().equals(this.uClassifier)
   */
  JAttribute createAttribute(UAttribute uAttribute) {
    assert uAttribute.getOwner().equals(this.uClassifier);

    JAttribute jAttribute = this.attributes.get(uAttribute);
    if (jAttribute != null)
      return jAttribute;

    String name = uniqueAttributeName(uAttribute.getName());
    String type = this.getTypeName(uAttribute.getType());
    String initialValue = "";
    if (uAttribute.isArray()) {
      initialValue = JKeywords.NEW + " " + type;
      initialValue += "{ ";
      for (int i = 0; i < uAttribute.getSize(); i++) {
        initialValue += UMLTranslator.translateExpression(uAttribute.getInitialValue(i), uAttribute.getInitialValuesContext(), this);
        if (i < uAttribute.getSize()-1)
          initialValue += ", ";
      }
      initialValue += " }";
    }
    else
      initialValue = UMLTranslator.translateExpression(uAttribute.getInitialValue(0), uAttribute.getInitialValuesContext(), this);

    // Set modifiers
    String modifiers = "";
    if (uAttribute.isStatic()) {
      modifiers = DEFAULTCLASSVARIABLEVISIBILITY;
      modifiers += " " + JKeywords.STATIC;
      if (uAttribute.isConstant())
        modifiers += " " + JKeywords.FINAL;
    }
    else {
      modifiers = DEFAULTINSTANCEVARIABLEVISIBILITY;
      if (uAttribute.isConstant())
        modifiers += " " + JKeywords.FINAL;
    }
    jAttribute = new JAttribute(name, type, initialValue, modifiers);
    attributes.put(uAttribute, jAttribute);
    this.getJClass().addAttribute(jAttribute);

    // Create setter
    if (!uAttribute.isFinal()) {
      String setterName = uniqueMethodName("set" + name.substring(0, 1).toUpperCase() + name.substring(1, name.length()));
      this.attributeSetterNames.put(jAttribute, setterName);

      List<JParameter> setterParameters = new ArrayList<>();
      JParameter setterParameter = new JParameter(name, type);
      setterParameters.add(setterParameter);

      JMethod setter = new JMethod(setterName, JKeywords.PUBLIC, JKeywords.VOID, setterParameters);
      setter.getBody().addBlockStatement(new JCode(this.getAttributeAccess(uAttribute) + " = " + setterParameter.getName() + ";"));
      this.getJClass().addMethod(setter);
    }

    // Create getter
    String getterName = uniqueMethodName("get" + name.substring(0, 1).toUpperCase() + name.substring(1, name.length()));
    JMethod getter = new JMethod(getterName, JKeywords.PUBLIC, type, new ArrayList<>());
    getter.getBody().addBlockStatement(new JCode("return " + this.getAttributeAccess(uAttribute) + ";"));
    this.getJClass().addMethod(getter);

    return jAttribute;
  }

  /**
   * Determine the name of the Java setter for a UML attribute of the underlying classifier.
   *
   * @param uAttribute a UML attribute of the underlying classifier
   * @return the method name for the setter of {@code uAttribute} or {@code null} if the attribute is final
   */
  public @Nullable String getAttributeSetterName(UAttribute uAttribute) {
    JAttribute jAttribute = createAttribute(uAttribute);
    return this.attributeSetterNames.get(jAttribute);
  }

  /**
   * Determine the init method of this classifier.
   *
   * If there is an operation with name {@code INITMETHODNAME}, with an empty parameter list,
   * and with void return type, this is taken to be the init method; otherwise, another
   * method is created.
   *
   * @return the classifier's init method
   */
  public JMethod getInitMethod() {
    var initMethod = this.initMethod;
    if (initMethod != null)
      return initMethod;
    for (var uOperation : this.uClassifier.getOperations())
      if (uOperation.getName().equals(INITMETHODNAME) && uOperation.getType().equals(UType.function(new ArrayList<>(), UType.simple(this.getModel().getVoid()))))
        initMethod = createMethod(uOperation);
    initMethod = new JMethod(uniqueMethodName(INITMETHODNAME), JKeywords.PUBLIC, JKeywords.VOID, new ArrayList<>());
    return this.initMethod = initMethod;
  }

  /**
   * Determine the name of the Java method for a UML behavioural.
   *
   * @param uBehavioural a UML behavioural
   * @return the method name for {@code uBehavioural}
   */
  String getMethodName(UBehavioural uBehavioural) {
    if (!uBehavioural.getOwner().equals(this.uClassifier))
      return this.modelSymbolTable.getMethodName(uBehavioural);

    JMethod jMethod = createMethod(uBehavioural);
    return jMethod.getName();
  }

  /**
   * Create a Java method representing a UML behavioural.
   * 
   * @param uBehavioural a UML behavioural
   * @return a Java method for {@code uBehavioural}
   * @pre uBehavioural.getOwner().equals(this.uClassifier)
   */
  JMethod createMethod(UBehavioural uBehavioural) {
    assert uBehavioural.getOwner().equals(this.uClassifier);

    JMethod jMethod = this.externalMethods.get(uBehavioural);
    if (jMethod != null)
      return jMethod;

    String name = uniqueMethodName(uBehavioural.getName());

    var isCycleBasedOperation = this.getProperties().isCycleBased() && this.getUStateMachine() != null && !uBehavioural.isStatic() && uBehavioural.cases(o -> true, r -> false);
    List<JParameter> parameters = new ArrayList<>();
    if (isCycleBasedOperation)
      parameters.add(new JParameter("sndEvtQ", EVENTQUEUECLASSNAME));
    for (UParameter uParameter : uBehavioural.getParameters())
      parameters.add(new JParameter(this.getParameterName(uParameter.getName()), this.getTypeName(uParameter.getType())));

    JBlock body = new JBlock();
    var javaMethodCode = uBehavioural.getMethodCode("Java");
    if (javaMethodCode != null) {
      for (var javaMethodCodeLine : javaMethodCode)
        body.addBlockStatement(new JCode(javaMethodCodeLine));
    }
    if (this.getUStateMachine() != null && !uBehavioural.isStatic()) {
      String eventType = uBehavioural.cases(o -> "Call", r -> "Signal");
      StringBuilder arguments = new StringBuilder(isCycleBasedOperation ? parameters.get(0).getName() + ", " : "");
      arguments.append(this.getEventName(UEvent.behavioural(uBehavioural)));
      for (var parameter : parameters.subList(isCycleBasedOperation ? 1 : 0, parameters.size())) {
        arguments.append(", ");
        arguments.append(parameter.getName());
      }
      body.addBlockStatement(new JCode(this.getEventQueueAccess() + ".insert" + eventType + "(" + arguments.toString() + ");"));
    }

    var modifiers = DEFAULTMETHODVISIBILITY;
    if (uBehavioural.isStatic())
      modifiers += " " + JKeywords.STATIC;
    jMethod = new JMethod(name, modifiers, JKeywords.VOID, parameters, body);
    this.externalMethods.put(uBehavioural, jMethod);
    this.getJClass().addMethod(jMethod);
    return jMethod;
  }

  /**
   * Get the name of the internal method for handling a UML event.
   *
   * @param uEvent a UML event
   * @return the name of the internal method handling {@code uAction}
   */
  String getEventMethodName(UEvent uEvent) {
    String eventMethodName = this.eventMethodNames.get(uEvent);
    if (eventMethodName != null)
      return eventMethodName;

    eventMethodName = uniqueMethodName(uEvent.getName());
    this.eventMethodNames.put(uEvent, eventMethodName);
    return eventMethodName;
  }

  /**
   * Get the name for the resumption method after a Smile primitive has been "acknowledged".
   *
   * @param sPrimitive a Smile primitive
   * @return the name for resuming after acknowledging {@code sPrimitive}
   */
  String getResumptionMethodName(SPrimitive sPrimitive) {
    String resumptionName = this.resumptionMethodNames.get(sPrimitive);
    if (resumptionName != null)
      return resumptionName;

    resumptionName = uniqueMethodName("resume_" + sPrimitive.toString());
    this.resumptionMethodNames.put(sPrimitive, resumptionName);
    return resumptionName;
  }

  /**
   * Determine the Java name for a parameter name.
   * 
   * @param parameterName a parameter name
   * @return the Java parameter name
   */
  String getParameterName(String parameterName) {
    if (parameterName.equals(""))
      parameterName = "p";
    return newName(parameterName, JKeywords.getKeywords());
  }

  /**
   * Determine the Java access expression to a UML parameter.
   *
   * @param uParameter a UML parameter
   * @return Java access expression to {@link uParameter}
   */
  String getParameterAccess(UParameter uParameter) {
    if (this.getProperties().isEventBased())
      return this.getParameterName(uParameter.getName());
    return this.getUnboxing(uParameter.getType(), CURRENTEVENTNAME + ".getArgument(" + uParameter.getNumber() + ")");
  }

  /**
   * Determine the collection of collected constant declarations.
   * 
   * @return a collection of Java constant declarations
   */
  Collection<JVariableDeclaration> getConstantDeclarations() {
    return this.constants.values();
  }

  /**
   * Determine the collection of collected variable declarations.
   * 
   * @return a collection of Java variable declarations
   */
  Collection<JVariableDeclaration> getVariableDeclarations() {
    return this.variables.values();
  }

  /**
   * Determine a unique name for an attribute name.
   * 
   * @param attributeName an attribute name
   * @return a unique attribute name
   */
  private String uniqueAttributeName(String attributeName) {
    if (attributeName.equals(""))
      attributeName = "a";
    String uniqueAttributeName = newName(attributeName, this.attributeNames);
    this.attributeNames.add(uniqueAttributeName);
    return uniqueAttributeName;
  }

  /**
   * Determine a unique name for a method name.
   * 
   * @param methodName a method name
   * @return a unique method name
   */
  private String uniqueMethodName(String methodName) {
    if (methodName.equals(""))
      methodName = "m";
    String uniqueMethodName = this.newName(methodName, this.methodNames);
    this.methodNames.add(uniqueMethodName);
    return uniqueMethodName;
  }

  /**
   * Determine a unique name for a variable name.
   * 
   * @param variableName a variable name
   * @return a unique variable name
   */
  private String uniqueVariableName(String variableName) {
    if (variableName.equals(""))
      variableName = "v";
    String uniqueVariableName = this.newName(variableName, this.variableNames);
    this.variableNames.add(uniqueVariableName);
    return uniqueVariableName;
  }
}
