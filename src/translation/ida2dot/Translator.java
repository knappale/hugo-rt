package translation.ida2dot;

import java.util.*;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UContext;
import uml.UExpression;
import uml.ida.IAction;
import uml.ida.IAutomaton;
import uml.ida.IBranch;
import uml.ida.IClock;
import uml.ida.IClockBound;
import uml.ida.ICondition;
import uml.ida.IEvent;
import uml.ida.IExpression;
import uml.ida.IOperator;
import uml.ida.IState;
import uml.ida.ITransition;
import uml.ida.IVariable;
import uml.interaction.UOccurrenceSpecification;
import uml.interaction.UTiming;


/**
 * Translation of an Ida automaton into a Dot graph.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 * @since 2006-01-29
 */
@NonNullByDefault
public class Translator {
  public static String translate(@Nullable IAutomaton automaton) {
    if (automaton == null)
      return "digraph G {\n}\n";

    Map<UOccurrenceSpecification.Data, String> timers = new HashMap<>();
    for (UTiming timing : automaton.getTimings()) {
      UOccurrenceSpecification.Data lowerData = timing.getLowerData();
      if (timers.get(lowerData) == null)
        timers.put(lowerData, lowerData.getName());
    }

    StringBuilder buffer = new StringBuilder();
    String indent = "  ";
    buffer.append("digraph G {\n");
    
    // Add one shape command for every state
    for (IState state : automaton.getStates()) {
      buffer.append(indent);
      buffer.append(normaliseName(state.getName()));
      if (automaton.isAcceptingState(state)) {
        buffer.append(" [shape=ellipse peripheries=2];\n");
      }
      else {
        if (automaton.isRecurrenceState(state)) {
          buffer.append(" [shape=ellipse peripheries=3];\n");
        }
        else
          buffer.append(" [shape=ellipse];\n");
      }
    }
    
    // Add the arrow to the initial state (invisible start state)
    IState initialState = automaton.getInitialState();
    if (initialState != null) {
      buffer.append(indent);
      buffer.append("start [style=invis,label=\"\"];\n");
      buffer.append(indent);
      buffer.append("start -> ");
      buffer.append(normaliseName(initialState.getName()));
      buffer.append(";\n");
    }

    // Add all the transitions
    TransitionTranslator visitor = new TransitionTranslator();
    for (ITransition iTransition : automaton.getTransitions()) {
      for (IBranch iBranch : iTransition.getBranches()) {
        buffer.append(indent);
        buffer.append(normaliseName(iTransition.getFrom().getName()));
        buffer.append(" -> ");
        buffer.append(normaliseName(iBranch.getTo().getName()));
        buffer.append(" [label=\"");

        IEvent trigger = iTransition.getTrigger();
        if (trigger != null) {
          buffer.append(trigger.toString());
        }
        ICondition guard = iTransition.getGuard();
        if (guard != null) {
          buffer.append(" [");
          visitor.reset();
          guard.getCondition().accept(visitor);
          buffer.append(visitor.getLabel());
          buffer.append("]");
        }
        IClockBound clockBound = iTransition.getClockBound();
        if (clockBound != null) {
          buffer.append(" {");
          visitor.reset();
          clockBound.accept(visitor);
          buffer.append(visitor.getLabel());
          buffer.append("}");
        }
        IAction effect = iBranch.getLabel().getEffect();
        if (effect != null) {
          buffer.append(" / ");
          visitor.reset();
          effect.accept(visitor);
          buffer.append(visitor.getLabel());
        }
        buffer.append("\"];\n");
      }
    }
    
    buffer.append("}\n");
    
    return buffer.toString();
  }

  private static String normaliseName(String identifier) {
    char[] characters = identifier.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    return new String(characters);
  }

  private static class TransitionTranslator implements IAction.Visitor<@Nullable Void>, IExpression.Visitor<@Nullable Void>, IClockBound.Visitor<@Nullable Void> {
    private StringBuilder buffer;

    public TransitionTranslator() {
      this.buffer = new StringBuilder();
    }

    public String getLabel() {
      return this.buffer.toString();
    }

    public void reset() {
      this.buffer = new StringBuilder();
    }
    
    public @Nullable Void onSkip() {
      // nothing to do
      return null;
    }

    public @Nullable Void onAssign(IVariable variable, IExpression expression) {
      buffer.append(variable.getName());
      buffer.append(" = ");
      expression.accept(this);
      buffer.append(";");
      return null;
    }

    public @Nullable Void onReset(IVariable variable) {
      buffer.append(variable.getName() + " = 0;");
      return null;
    }

    public @Nullable Void onInc(IVariable variable) {
      buffer.append(variable.getName() + "++;");
      return null;
    }

    public @Nullable Void onDec(IVariable variable) {
      buffer.append(variable.getName() + "--;");
      return null;
    }

    public @Nullable Void onSet(IVariable variable, int offset) {
      buffer.append(variable.getName());
      buffer.append("[");
      buffer.append(offset);
      buffer.append("] = 1;");
      return null;
    }

    public @Nullable Void onReset(IVariable variable, int offset) {
      buffer.append(variable.getName());
      buffer.append("[");
      buffer.append(offset);
      buffer.append("] = 0;");
      return null;
    }

    public @Nullable Void onReset(IClock clock) {
      buffer.append(clock.getName());
      buffer.append(" = 0;");
      return null;
    }

    public @Nullable Void onSeq(IAction leftAction, IAction rightAction) {
      leftAction.accept(this);
      buffer.append(" ");
      rightAction.accept(this);
      return null;
    }

    public @Nullable Void onBooleanConstant(boolean booleanConstant) {
      buffer.append(booleanConstant);
      return null;
    }

    public @Nullable Void onIntegerConstant(int integerConstant) {
      buffer.append(integerConstant);
      return null;
    }

    public @Nullable Void onInfinity() {
      buffer.append("infty");
      return null;
    }

    public @Nullable Void onVariable(IVariable variable) {
      buffer.append(variable.getName());
      return null;
    }

    public @Nullable Void onArray(IVariable variable, int offset) {
      buffer.append(variable.getName());
      buffer.append("[");
      buffer.append(offset);
      buffer.append("]");
      return null;
    }

    public @Nullable Void onArithmetical(IExpression leftExpression, IOperator op, IExpression rightExpression) {
      buffer.append("(");
      leftExpression.accept(this);
      buffer.append(")");
      buffer.append(op.getOpName());
      buffer.append("(");
      rightExpression.accept(this);
      buffer.append(")");
      return null;
    }

    public @Nullable Void onNegation(IExpression expression) {
      buffer.append("!(");
      expression.accept(this);
      buffer.append(")");
      return null;
    }

    public @Nullable Void onRelational(IExpression leftExpression, IOperator op, IExpression rightExpression) {
      buffer.append("(");
      leftExpression.accept(this);
      buffer.append(")");
      buffer.append(op.getOpName());
      buffer.append("(");
      rightExpression.accept(this);
      buffer.append(")");
      return null;
    }

    public @Nullable Void onJunctional(IExpression leftExpression, IOperator op, IExpression rightExpression) {
      buffer.append("(");
      leftExpression.accept(this);
      buffer.append(")");
      buffer.append(op.getOpName());
      buffer.append("(");
      rightExpression.accept(this);
      buffer.append(")");
      return null;
    }

    public @Nullable Void onExternal(UExpression external, UContext context) {
      buffer.append(external.toString());
      return null;
    }

    private void clockBound(IClock clock, String op, IExpression bound) {
      buffer.append(clock.getName());
      buffer.append(op);
      buffer.append(bound);
    }

    public @Nullable Void onLt(IClock clock, IExpression bound) {
      clockBound(clock, " < ", bound);
      return null;
    }

    public @Nullable Void onLeq(IClock clock, IExpression bound) {
      clockBound(clock, " <= ", bound);
      return null;
    }

    public @Nullable Void onGt(IClock clock, IExpression bound) {
      clockBound(clock, " > ", bound);
      return null;
    }

    public @Nullable Void onGeq(IClock clock, IExpression bound) {
      clockBound(clock, " >= ", bound);
      return null;
    }

    public @Nullable Void onAnd(IClockBound leftClockBound, IClockBound rightClockBound) {
      leftClockBound.accept(this);
      buffer.append(" && ");
      rightClockBound.accept(this);
      return null;
    }
  }
}
