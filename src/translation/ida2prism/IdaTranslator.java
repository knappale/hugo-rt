package translation.ida2prism;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import prism.RBranch;
import prism.RClock;
import prism.RCommand;
import prism.RConstant;
import prism.RExpression;
import prism.RLabel;
import prism.RModule;
import prism.ROperator;
import prism.RSystem;
import prism.RUpdate;
import prism.RVariable;
import uml.UBehavioural;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import uml.ida.IAction;
import uml.ida.IAutomaton;
import uml.ida.IBranch;
import uml.ida.IClock;
import uml.ida.IClockBound;
import uml.ida.ICondition;
import uml.ida.IEvent;
import uml.ida.IExpression;
import uml.ida.IObservation;
import uml.ida.IOperator;
import uml.ida.IState;
import uml.ida.ITransition;
import uml.ida.IVariable;
import util.Formatter;

import static util.Objects.requireNonNull;


/**
 * Translate an Ida automaton into an UPPAAL observer automaton.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 * @since 06/03/06
 */
@NonNullByDefault
public class IdaTranslator {
  private SymbolTable symbolTable;
  private UContext uContext;
  private IAutomaton iAutomaton;

  private RSystem rSystem;
  private RModule rModule;
  private @Nullable RVariable stateRVariable;
  private Map<IClock, RClock> clocks = new HashMap<>();
  private Map<IVariable, RVariable> integerVariables = new HashMap<>();
  private Map<IVariable, Map<Integer, RVariable>> bitArrayVariables = new HashMap<>();
  private Map<IState, Integer> states = new HashMap<>();
  private final int MAX = 10;

  /**
   * Generate PRISM system for an Ida automaton.
   *
   * @param iAutomaton an Ida automaton
   * @param uContext a UML context
   * @return a PRISM system representing {@code iAutomaton}
   */
  public static RSystem translate(IAutomaton iAutomaton, UContext uContext) {
    IdaTranslator t = new IdaTranslator(iAutomaton, uContext);
    return t.rSystem;
  }
  
  /**
   * Generate PRISM module for an Ida automaton.
   *
   * @param iAutomaton an Ida automaton
   * @param uContext a UML context
   */
  private IdaTranslator(IAutomaton iAutomaton, UContext uContext) {
    this.symbolTable = new SymbolTable();
    this.uContext = uContext;
    this.iAutomaton = iAutomaton;
    this.rSystem = RSystem.mdp();
    this.rModule = new RModule("Interaction");
    this.rSystem.addModule(this.rModule);

    // Fill in transitions
    for (ITransition iTransition : this.iAutomaton.getTransitions()) {
      List<RExpression> guards = new ArrayList<>();

      // Add start state
      guards.add(this.getStateRExpression(iTransition.getFrom()));

      // Add transition guards
      ICondition iTransitionGuard = iTransition.getGuard();
      if (iTransitionGuard != null) {
        RExpression expressionGuard = translateExpression(iTransitionGuard.getCondition());
        guards.add(expressionGuard);
      }

      // Add clock bounds
      IClockBound iTransitionClockBound = iTransition.getClockBound();
      if (iTransitionClockBound != null) {
        List<RExpression> clockBoundGuards = translateClockBound(iTransitionClockBound);
        guards.addAll(clockBoundGuards);
      }

      List<RBranch> rBranches = new ArrayList<>();
      for (IBranch iBranch : iTransition.getBranches()) {
        List<RUpdate> rUpdates = new ArrayList<>();
        IAction iBranchEffect = iBranch.getLabel().getEffect();
        if (iBranchEffect != null) {
          rUpdates.addAll(translateAction(iBranchEffect));
          guards.addAll(actionGuard(iBranchEffect));
        }
        rUpdates.add(this.getStateRUpdate(iBranch.getTo()));
        @Nullable Double iBranchRate = iBranch.getLabel().getRate();
        if (iBranchRate != null)
          rBranches.add(new RBranch(iBranchRate, rUpdates));
        else
          rBranches.add(new RBranch(rUpdates));
      }

      String rCommandLabel = "";
      IEvent iEvent = iTransition.getTrigger();
      if (iEvent != null)
        rCommandLabel = translateEvent(iEvent);
      this.rModule.addCommand(new RCommand(rCommandLabel, guards.stream().reduce(RExpression.trueConst(), RExpression::and), rBranches));
    }

    // Create finished formula
    RExpression finish = this.iAutomaton.getAcceptingStates().stream().map(iState -> this.getStateRExpression(iState)).reduce(RExpression.falseConst(), RExpression::or);
    this.rSystem.addLabel(new RLabel("finish", finish));

    // Create recurrence formula
    RExpression recurrence = this.iAutomaton.getRecurrenceStates().stream().map(iState -> this.getStateRExpression(iState)).reduce(RExpression.falseConst(), RExpression::or);
    this.rSystem.addLabel(new RLabel("recurrence", recurrence));

    // Add variables to module
    for (RVariable integerRVariable : this.integerVariables.values())
      this.rModule.addVariable(integerRVariable);
    for (Map<Integer, RVariable> booleanVariables : this.bitArrayVariables.values())
      for (RVariable booleanRVariable : booleanVariables.values())
        this.rModule.addVariable(booleanRVariable);
    this.rModule.addVariable(this.getStateRVariable());
    for (RVariable modelRVariable : this.symbolTable.getVariables())
      this.rModule.addVariable(modelRVariable);

    // Add clocks to module
    for (RClock rClock : this.clocks.values())
      this.rModule.addClock(rClock);

    // Add constants to system
    for (RConstant modelRConstant : this.symbolTable.getConstants())
      this.rSystem.addConstant(modelRConstant);
  }

  RVariable getStateRVariable() {
    RVariable stateRVariable = this.stateRVariable;
    if (stateRVariable != null)
      return stateRVariable;

    // Assign a unique number to every state of the Ida automaton
    int statesCounter = 0;
    for (IState iState : this.iAutomaton.getStates())
      this.states.put(iState, statesCounter++);
    int maxStatesNumber = Integer.max(this.iAutomaton.getStates().size()-1, 0);

    // Create initialised control state variable
    IState initialIState = this.iAutomaton.getInitialState();
    if (initialIState != null)
      stateRVariable = RVariable.integer("state", RExpression.intConst(0), RExpression.intConst(maxStatesNumber),
                                                  RExpression.intConst(requireNonNull(this.states.get(initialIState))));
    else
      stateRVariable = RVariable.integer("state", RExpression.intConst(0), RExpression.intConst(maxStatesNumber));
    this.stateRVariable = stateRVariable;
    return stateRVariable;
  }

  RExpression getStateRExpression(IState iState) {
    return RExpression.eq(RExpression.access(this.getStateRVariable()), RExpression.intConst(requireNonNull(this.states.get(iState))));
  }

  RUpdate getStateRUpdate(IState iState) {
    return RUpdate.assign(this.getStateRVariable(), RExpression.intConst(requireNonNull(this.states.get(iState))));
  }

  RVariable getIntegerRVariable(IVariable iVariable) {
    RVariable rVariable = this.integerVariables.get(iVariable);
    if (rVariable != null)
      return rVariable;

    rVariable = RVariable.integer(iVariable.getName(), RExpression.intConst(0), RExpression.intConst(MAX), RExpression.intConst(0));
    this.integerVariables.put(iVariable, rVariable);
    return rVariable;
  }

  RVariable getBooleanRVariable(IVariable iVariable, int offset) {
    Map<Integer, RVariable> variables = this.bitArrayVariables.get(iVariable);
    if (variables == null) {
      variables = new HashMap<>();
      this.bitArrayVariables.put(iVariable, variables);
    }
    RVariable rVariable = variables.get(offset);
    if (rVariable != null)
      return rVariable;

    rVariable = RVariable.bool(iVariable.getName() + offset, RExpression.falseConst());
    variables.put(offset, rVariable);
    return rVariable;
  }

  RClock getRClock(IClock iClock) {
    RClock rClock = this.clocks.get(iClock);
    if (rClock != null)
      return rClock;

    rClock = new RClock(iClock.getName());
    this.clocks.put(iClock, rClock);
    return rClock;
  }

  private class OccurrenceSpecificationTranslator implements IObservation.InContextVisitor<String> {
    OccurrenceSpecificationTranslator() {
    }
    
    String annotation(@Nullable IEvent event) {
      if (event == null)
        return "";
      return event.getObservation().accept(this);
    }

    String guard(IObservation specification) {
      return specification.accept(this);
    }

    public UContext getContext() {
      return uContext;
    }

    public String onSend() {
      return "snd";
    }

    public String onReceive() {
      return "rcv";
    }

    public String onTermination() {
      return "trm";
    }

    public String onSender(UObject uObject) {
      return uObject.getName();
    }

    public String onReceiver(UObject uObject) {
      return uObject.getName();
    }

    public String onBehavioural(UBehavioural uBehavioural) {
      return uBehavioural.getName();
    }

    public String onArguments(List<UExpression> arguments) {
      return Formatter.separated(arguments, "_");
    }

    public String onExpression(IExpression expression) {
      return translateExpression(expression).toString();
    }

    public String onNot(IObservation specification) {
      return "not_" + guard(specification);
    }

    public String onAnd(IObservation leftSpecification, IObservation rightSpecification) {
      return guard(leftSpecification) + "_" + guard(rightSpecification);
    }

    public String onOr(IObservation leftSpecification, IObservation rightSpecification) {
      return guard(leftSpecification) + "_or_" + guard(rightSpecification);
    }
  }

  String translateEvent(IEvent uEvent) {
    return (new OccurrenceSpecificationTranslator()).annotation(uEvent);
  }

  private class ClockBoundTranslator implements IClockBound.Visitor<List<RExpression>> {
    ClockBoundTranslator() {
    }
    
    List<RExpression> guard(IClockBound timing) {
      return timing.accept(this);
    }

    RExpression getTimerAccess(IClock iClock) {
      return RExpression.access(getRClock(iClock));
    }

    public List<RExpression> onLt(IClock iClock, IExpression bound) {
      return new ArrayList<>(Arrays.asList(RExpression.lt(getTimerAccess(iClock), translateExpression(bound))));
    }

    public List<RExpression> onLeq(IClock iClock, IExpression bound) {
      return new ArrayList<>(Arrays.asList(RExpression.leq(getTimerAccess(iClock), translateExpression(bound))));
    }

    public List<RExpression> onGt(IClock iClock, IExpression bound) {
      return new ArrayList<>(Arrays.asList(RExpression.gt(getTimerAccess(iClock), translateExpression(bound))));
    }

    public List<RExpression> onGeq(IClock iClock, IExpression bound) {
      return new ArrayList<>(Arrays.asList(RExpression.geq(getTimerAccess(iClock), translateExpression(bound))));
    }

    public List<RExpression> onAnd(IClockBound leftClockBound, IClockBound rightClockBound) {
      List<RExpression> result = new ArrayList<>();
      result.addAll(guard(leftClockBound));
      result.addAll(guard(rightClockBound));
      return result;
    }
  }

  List<RExpression> translateClockBound(IClockBound uTiming) {
    return (new ClockBoundTranslator()).guard(uTiming);
  }

  private class ExpressionTranslator implements IExpression.Visitor<RExpression> {
    private @Nullable IExpression iExpression;

    ExpressionTranslator() {
    }

    RExpression expression(IExpression value) {
      this.iExpression = value;
      return value.accept(this);
    }

    IllegalStateException exception() {
      return new IllegalStateException("Cannot translate value expression " + Formatter.quoted(iExpression) + " in Ida automaton into PRISM");
    }

    public RExpression onBooleanConstant(boolean booleanConstant) {
      return booleanConstant ? RExpression.trueConst() : RExpression.falseConst();
    }

    public RExpression onIntegerConstant(int integerConstant) {
      return RExpression.intConst(integerConstant);
    }

    public RExpression onInfinity() {
      // Translate into some big integer
      return RExpression.intConst(MAX);
    }

    public RExpression onVariable(IVariable iVariable) {
      return RExpression.access(getIntegerRVariable(iVariable));
    }

    public RExpression onArray(IVariable iVariable, int offset) {
      return RExpression.access(getBooleanRVariable(iVariable, offset));
    }

    public RExpression onExternal(UExpression uExpression, UContext uContext) {
      return UMLTranslator.translateExpression(uExpression, uContext, symbolTable);
    }

    public RExpression onArithmetical(IExpression iLeftExpression, IOperator iOp, IExpression iRightExpression) {
      RExpression leftExpression = expression(iLeftExpression);
      RExpression rightExpression = expression(iRightExpression);
      switch (iOp) {
        case PLUS:
          return RExpression.arithmetical(leftExpression, ROperator.ADD, rightExpression);
        case MINUS:
          return RExpression.arithmetical(leftExpression, ROperator.SUB, rightExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }

    public RExpression onNegation(IExpression iExpression) {
      return RExpression.neg(expression(iExpression));
    }

    public RExpression onRelational(IExpression iLeftExpression, IOperator iOp, IExpression iRightExpression) {
      RExpression leftExpression = expression(iLeftExpression);
      RExpression rightExpression = expression(iRightExpression);
      switch (iOp) {
        case LT:
          return RExpression.lt(leftExpression, rightExpression);
        case LEQ:
          return RExpression.leq(leftExpression, rightExpression);
        case EQ:
          return RExpression.eq(leftExpression, rightExpression);
        case NEQ:
          return RExpression.neq(leftExpression, rightExpression);
        case GEQ:
          return RExpression.geq(leftExpression, rightExpression);
        case GT:
          return RExpression.gt(leftExpression, rightExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }

    public RExpression onJunctional(IExpression iLeftExpression, IOperator iOp, IExpression iRightExpression) {
      RExpression leftExpression = expression(iLeftExpression);
      RExpression rightExpression = expression(iRightExpression);
      switch (iOp) {
        case AND:
          return RExpression.and(leftExpression, rightExpression);
        case OR:
          return RExpression.or(leftExpression, rightExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }
  }

  RExpression translateExpression(IExpression iExpression) {
    return (new ExpressionTranslator()).expression(iExpression);
  }

  private class ActionTranslator implements IAction.Visitor<List<RUpdate>> {
    ActionTranslator() {
    }

    /**
     * Produce a list of PRISM updates from an Ida action.
     *
     * @param iAction an Ida action
     * @return a list of PRISM updates corresponding to the Ida action
     */
    List<RUpdate> updates(IAction iAction) {
      return iAction.accept(this);
    }

    public List<RUpdate> onSkip() {
      return new ArrayList<>();
    }

    public List<RUpdate> onAssign(IVariable iVariable, IExpression iExpression) {
      return new ArrayList<>(Arrays.asList(RUpdate.assign(getIntegerRVariable(iVariable), translateExpression(iExpression))));
    }

    public List<RUpdate> onReset(IVariable iVariable) {
      return new ArrayList<>(Arrays.asList(RUpdate.assign(getIntegerRVariable(iVariable), RExpression.intConst(0))));
    }

    public List<RUpdate> onInc(IVariable iVariable) {
      return new ArrayList<>(Arrays.asList(RUpdate.inc(getIntegerRVariable(iVariable))));
    }

    public List<RUpdate> onDec(IVariable iVariable) {
      return new ArrayList<>(Arrays.asList(RUpdate.inc(getIntegerRVariable(iVariable))));
    }

    public List<RUpdate> onSet(IVariable iVariable, int offset) {
      return new ArrayList<>(Arrays.asList(RUpdate.assign(getBooleanRVariable(iVariable, offset), RExpression.intConst(1))));
    }

    public List<RUpdate> onReset(IVariable iVariable, int offset) {
      return new ArrayList<>(Arrays.asList(RUpdate.assign(getBooleanRVariable(iVariable, offset), RExpression.intConst(0))));
    }

    public List<RUpdate> onReset(IClock iClock) {
      return new ArrayList<>(Arrays.asList(RUpdate.reset(getRClock(iClock))));
    }

    public List<RUpdate> onSeq(IAction iLeftAction, IAction iRightAction) {
      List<RUpdate> updates = new ArrayList<>();
      updates.addAll(updates(iLeftAction));
      updates.addAll(updates(iRightAction));
      return updates;
    }
  }

  List<RUpdate> translateAction(IAction iAction) {
    return (new ActionTranslator()).updates(iAction);
  }

  private class ActionGuardTranslator implements IAction.Visitor<List<RExpression>> {
    ActionGuardTranslator() {
    }

    /**
     * Produce a list of PRISM updates from an Ida action.
     *
     * @param iAction an Ida action
     * @return a list of PRISM updates corresponding to the Ida action
     */
    List<RExpression> updates(IAction iAction) {
      return iAction.accept(this);
    }

    public List<RExpression> onSkip() {
      return new ArrayList<>();
    }

    public List<RExpression> onAssign(IVariable iVariable, IExpression iExpression) {
      return new ArrayList<>();
    }

    public List<RExpression> onReset(IVariable iVariable) {
      return new ArrayList<>();
    }

    public List<RExpression> onInc(IVariable iVariable) {
      return new ArrayList<>(Arrays.asList(RExpression.lt(RExpression.access(getIntegerRVariable(iVariable)), RExpression.intConst(MAX))));
    }

    public List<RExpression> onDec(IVariable iVariable) {
      return new ArrayList<>(Arrays.asList(RExpression.gt(RExpression.access(getIntegerRVariable(iVariable)), RExpression.intConst(0))));
    }

    public List<RExpression> onSet(IVariable iVariable, int offset) {
      return new ArrayList<>();
    }

    public List<RExpression> onReset(IVariable iVariable, int offset) {
      return new ArrayList<>();
    }

    public List<RExpression> onReset(IClock iClock) {
      return new ArrayList<>();
    }

    public List<RExpression> onSeq(IAction iLeftAction, IAction iRightAction) {
      List<RExpression> guards = new ArrayList<>();
      guards.addAll(updates(iLeftAction));
      guards.addAll(updates(iRightAction));
      return guards;
    }
  }

  List<RExpression> actionGuard(IAction iAction) {
    return (new ActionGuardTranslator()).updates(iAction);
  }
}
