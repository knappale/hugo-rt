package translation.ida2prism;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import prism.RConstant;
import prism.RDataType;
import prism.RExpression;
import prism.RVariable;
import uml.UAttribute;
import uml.UExpression;
import uml.UObject;
import uml.UStatual;

import static util.Objects.requireNonNull;


@NonNullByDefault
public class SymbolTable {
  private @Nullable RConstant emptyConstant;
  private Map<Object, RConstant> constants = new HashMap<>();
  private Map<Object, RVariable> variables = new HashMap<>();
  private Set<String> identifiers = new HashSet<>();
  private int identifiersCounter = 0;
  private int objectsCounter = 0;

  /**
   * @return symbol table's properties
   */
  control.Properties getProperties() {
    return control.Properties.getDefault();
  }

  Collection<RConstant> getConstants() {
    return requireNonNull(this.constants.values());
  }

  Collection<RVariable> getVariables() {
    return requireNonNull(this.variables.values());
  }

  RExpression getEmptyConstantAccess() {
    RConstant emptyConstant = this.emptyConstant;
    if (emptyConstant != null)
      return RExpression.access(emptyConstant);

    emptyConstant = new RConstant(RDataType.intType(), uniqueIdentifier("empty"), RExpression.intConst(0));
    this.constants.put(emptyConstant, emptyConstant);
    this.emptyConstant = emptyConstant;
    return RExpression.access(emptyConstant);
  }

  RExpression getFieldAccess(UAttribute attribute) {
    if (!attribute.isConstant())
      return this.getVariableAccess(attribute);

    RConstant rConstant = this.constants.get(attribute);
    if (rConstant != null)
      return RExpression.access(rConstant);

    StringBuffer constantName = new StringBuffer();
    constantName.append("cls");
    constantName.append(attribute.getOwner().getName().substring(0, 1).toUpperCase());
    constantName.append(attribute.getOwner().getName().substring(1));
    constantName.append("_");
    constantName.append(attribute.getName());
    rConstant = new RConstant(RDataType.intType(), uniqueIdentifier(constantName.toString()), UMLTranslator.translateExpression(requireNonNull(attribute.getInitialValue(0)), attribute.getInitialValuesContext(), this));
    this.constants.put(attribute, rConstant);
    return RExpression.access(rConstant);
  }

  RExpression getVariableAccess(UAttribute uAttribute) {
    RVariable rVariable = this.variables.get(uAttribute);
    if (rVariable != null)
      return RExpression.access(rVariable);

    StringBuilder nameBuilder = new StringBuilder();
    nameBuilder.append("cls");
    nameBuilder.append(uAttribute.getOwner().getName().substring(0, 1).toUpperCase());
    nameBuilder.append(uAttribute.getOwner().getName().substring(1));
    nameBuilder.append("_");
    nameBuilder.append(uAttribute.getName());
    if (uAttribute.getInitialValues().size() > 0) {
      UExpression initialUExpression = uAttribute.getInitialValues().get(0);
      rVariable = RVariable.integer(uniqueIdentifier(nameBuilder.toString()), RExpression.intConst(this.getProperties().getIntMin()), RExpression.intConst(this.getProperties().getIntMax()), UMLTranslator.translateExpression(initialUExpression, uAttribute.getInitialValuesContext(), this));
    }
    else
      rVariable = RVariable.integer(uniqueIdentifier(nameBuilder.toString()), RExpression.intConst(this.getProperties().getIntMin()), RExpression.intConst(this.getProperties().getIntMax()));
    this.variables.put(uAttribute, rVariable);
    return RExpression.access(rVariable);
  }

  RExpression getConstantAccess(UObject object) {
    RConstant rConstant = this.constants.get(object);
    if (rConstant != null)
      return RExpression.access(rConstant);

    StringBuilder constantName = new StringBuilder();
    constantName.append("obj");
    constantName.append(object.getName().substring(0, 1).toUpperCase());
    constantName.append(object.getName().substring(1));
    rConstant = new RConstant(RDataType.intType(), uniqueIdentifier(constantName.toString()), RExpression.intConst(++this.objectsCounter));
    constants.put(object, rConstant);
    return RExpression.access(rConstant);
  }

  /**
   * Compute a PRISM integer expression for accessing a statual.
   *
   * @param uStatual a (UML) statual
   */
  RExpression getAccess(UStatual uStatual) {
    return uStatual.cases(uAttribute -> this.getFieldAccess(uAttribute),
                          uParameter -> this.getEmptyConstantAccess(),
                          uObject -> this.getConstantAccess(uObject));
  }

  private String uniqueIdentifier(@Nullable String identifier) {
    if (identifier == null)
      identifier = "null";
    char[] characters = identifier.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    String uniqueIdentifier = new String(characters).substring(0, Math.min(50, characters.length));
    if (uniqueIdentifier.equals("") || this.identifiers.contains(uniqueIdentifier)) {
      String tmpIdentifier;
      do {
        tmpIdentifier = uniqueIdentifier + "_S" + this.identifiersCounter++;
      } while (this.identifiers.contains(tmpIdentifier));
      uniqueIdentifier = tmpIdentifier;
    }
    this.identifiers.add(uniqueIdentifier);
    return uniqueIdentifier;
  }
}
