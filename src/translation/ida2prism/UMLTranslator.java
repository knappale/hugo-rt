package translation.ida2prism;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import prism.RExpression;
import prism.ROperator;
import uml.UAttribute;
import uml.UContext;
import uml.UExpression;
import uml.UOperator;
import uml.UStatual;
import util.Formatter;


@NonNullByDefault
public class UMLTranslator {
  public static RExpression translateExpression(UExpression uExpression, UContext uContext, SymbolTable symbolTable) {
    return new ExpressionTranslator(uContext, symbolTable).translate(uExpression);
  }

  private static class ExpressionTranslator implements UExpression.InContextVisitor<RExpression> {
    private SymbolTable symbolTable;
    private UContext uContext;
    private @Nullable UExpression uExpression;

    ExpressionTranslator(UContext uContext, SymbolTable symbolTable) {
      this.symbolTable = symbolTable;
      this.uContext = uContext;
    }

    RExpression translate(UExpression uExpression) {
      this.uExpression = uExpression;
      return uExpression.accept(this);
    }

    private SymbolTable getSymbolTable() {
      return this.symbolTable;
    }

    @Override
    public UContext getContext() {
      return this.uContext;
    }

    private RuntimeException exception() {
      return new RuntimeException("Cannot translate UML expression " + Formatter.quoted(this.uExpression) + " into PRISM.");
    }

    @Override
    public RExpression onBooleanConstant(boolean booleanConstant) {
      return (booleanConstant ? RExpression.trueConst() : RExpression.falseConst());
    }

    @Override
    public RExpression onIntegerConstant(int integerConstant) {
      return RExpression.intConst(integerConstant);
    }

    @Override
    public RExpression onStringConstant(String stringConstant) {
      throw exception();
    }

    @Override
    public RExpression onNull() {
      return this.getSymbolTable().getEmptyConstantAccess();
    }

    @Override
    public RExpression onThis() {
      return this.getSymbolTable().getEmptyConstantAccess();
    }

    @Override
    public RExpression onStatual(UStatual uStatual) {
      return this.getSymbolTable().getAccess(uStatual);
    }

    @Override
    public RExpression onArray(UAttribute uAttribute, UExpression offsetUExpression) {
      throw exception();
    }

    @Override
    public RExpression onConditional(UExpression conditionUExpression, UExpression trueUExpression, UExpression falseUExpression) {
      RExpression conditionRExpression = translate(conditionUExpression);
      RExpression trueRExpression = translate(trueUExpression);
      RExpression falseRExpression = translate(falseUExpression);
      return RExpression.conditional(conditionRExpression, trueRExpression, falseRExpression);
    }

    @Override
    public RExpression onUnary(UOperator uOperator, UExpression innerUExpression) {
      RExpression innerRExpression = translate(innerUExpression);
      switch (uOperator) {
        case UPLUS:
          return innerRExpression;
        case UMINUS:
          return RExpression.minus(innerRExpression);
        case UNEG:
          return RExpression.neg(innerRExpression);
        //$CASES-OMITTED$
        default :
          throw exception();
      }
    }

    @Override
    public RExpression onArithmetical(UExpression leftUExpression, UOperator uOperator, UExpression rightUExpression) {
      RExpression leftRExpression = translate(leftUExpression);
      RExpression rightRExpression = translate(rightUExpression);
      switch (uOperator) {
        case ADD:
          return RExpression.arithmetical(leftRExpression, ROperator.ADD, rightRExpression);
        case SUB:
          return RExpression.arithmetical(leftRExpression, ROperator.SUB, rightRExpression);
        case MULT:
          return RExpression.arithmetical(leftRExpression, ROperator.MULT, rightRExpression);
        case DIV:
          return RExpression.arithmetical(leftRExpression, ROperator.DIV, rightRExpression);
        case MOD:
          return RExpression.arithmetical(leftRExpression, ROperator.MOD, rightRExpression);
        //$CASES-OMITTED$
        default :
          throw exception();
      }
    }

    @Override
    public RExpression onRelational(UExpression leftUExpression, UOperator uOperator, UExpression rightUExpression) {
      RExpression leftRExpression = translate(leftUExpression);
      RExpression rightRExpression = translate(rightUExpression);
      switch (uOperator) {
        case LT:
          return RExpression.relational(leftRExpression, ROperator.LT, rightRExpression);
        case LEQ:
          return RExpression.relational(leftRExpression, ROperator.LEQ, rightRExpression);
        case EQ:
          return RExpression.relational(leftRExpression, ROperator.EQ, rightRExpression);
        case NEQ:
          return RExpression.relational(leftRExpression, ROperator.NEQ, rightRExpression);
        case GEQ:
          return RExpression.relational(leftRExpression, ROperator.GEQ, rightRExpression);
        case GT:
          return RExpression.relational(leftRExpression, ROperator.GT, rightRExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }

    @Override
    public RExpression onJunctional(UExpression leftUExpression, UOperator uOperator, UExpression rightUExpression) {
      RExpression leftRExpression = translate(leftUExpression);
      RExpression rightRExpression = translate(rightUExpression);
      switch (uOperator) {
        case AND:
          return RExpression.and(leftRExpression, rightRExpression);
        case OR:
          return RExpression.or(leftRExpression, rightRExpression);
        //$CASES-OMITTED$
        default :
          throw exception();
      }
    }
  }
}
