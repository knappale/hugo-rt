package translation.uml2uppaal;

import static util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UBehavioural;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import uml.UStatual;
import uml.ida.IClock;
import uml.ida.IState;
import uml.ida.IVariable;
import uml.interaction.UInteraction;
import uml.interaction.UMessageOccurrenceSpecification;
import uml.interaction.UOccurrenceSpecification;
import uml.interaction.UTiming;
import uppaal.TAnnotation;
import uppaal.TChannel;
import uppaal.TClock;
import uppaal.TClockCondition;
import uppaal.TConstant;
import uppaal.TDataType;
import uppaal.TExpression;
import uppaal.TField;
import uppaal.TGuard;
import uppaal.TProcess;
import uppaal.TState;
import uppaal.TTemplate;
import uppaal.TTransition;
import uppaal.TVariable;
import uppaal.builder.TNet;
import uppaal.query.TFormula;
import util.Pair;


/**
 * Generate observer.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class Interaction {
  private SymbolTable inST;

  public static enum Observation {
    SENDING,
    RECEIVING,
    DELIVERING,
    TERMINATION;
  }

  public static class SymbolTable extends translation.uml2uppaal.SymbolTable.Template {
    private ModelSymbolTable moST;

    public SymbolTable(ModelSymbolTable moST) {
      super(moST.getModel(), moST.registerTemplate("mdlObserver"));
      this.moST = moST;
    }

    public ModelSymbolTable getModelSymbolTable() {
      return this.moST;
    }

    public final String MSG = "msg";
    public final String KIND = "kind";

    /**
     * @return process for the observer
     */
    public TProcess getProcess() {
      return this.moST.registerProcess("observer", this.getTemplate(), p -> { assert false : "not applicable"; return this.moST.empty(); });
    }

    @Override
    public TConstant.Access empty() {
      return this.moST.empty();
    }

    @Override
    public TField.Access access(UStatual uStatual) {
      return this.moST.access(uStatual);
    }

    /**
     * @return variable for {@code iVariable}
     */
    public TVariable getVariable(IVariable iVariable) {
      return this.registerVariable(key(iVariable, iVariable.getName()),
                                   () -> iVariable.isBitArray() ? TDataType.array(TDataType.boolType(), iVariable.getLength()) : TDataType.intType(),
                                   () -> null);
    }

    /**
     * @return state for {@code iState}
     */
    public TState getState(IState iState) {
      return this.registerState(iState.getName());
    }

    /**
     * @return state for deciding after an observation in {@code iState}
     */
    public TState getReceivedState(IState iState) {
      return this.registerState(iState.getName() + "_R", tState -> { tState.setCommitted(true); } );
    }

    /**
     * @return type of communication elements
     */
    TDataType getObservationType() {
      return moST.registerTypeDef("Observation", () -> { var fields = new ArrayList<Pair<String, TDataType>>();
                                                         fields.add(new Pair<>(this.MSG, moST.getMessageType()));
                                                         fields.add(new Pair<>(this.KIND, TDataType.range(Interaction.Observation.values().length-1)));
                                                         return TDataType.struct(fields); });
    }

    /**
     * @return access to empty observation constant
     */
    public TConstant.Access emptyObservation() {
      return moST.registerConstant(key("emptyObservation"), () -> this.getObservationType(), () -> this.getObservationType().getDefaultValue()).access();
    }

    /**
     * @return channel for informing the observer
     */
    public TChannel.Access toObserver() {
      return moST.registerChannel("toObserver", false).access();
    }

    /**
     * @return access to communication variable for informing the observer about an observation
     */
    public TVariable.Access obsComm() {
      return moST.registerVariable(key("obsComm"), () -> this.getObservationType(), () -> this.emptyObservation()).access();
    }

    /**
     * @return variable for storing an observed message
     */
    public TVariable.Access obs() {
      return this.registerVariable(key("obs"), () -> this.getObservationType()).access();
    }

    /**
     * @return access to constants for indicating the observation kind
     */
    public TConstant.Access getObservationKind(Observation kind) {
      switch (kind) {
        case SENDING:
          return this.moST.registerConstant(key("sending"), () -> TExpression.intConst(0)).access();
        case RECEIVING:
          return this.moST.registerConstant(key("receiving"), () -> TExpression.intConst(1)).access();
        case DELIVERING:
          return this.moST.registerConstant(key("delivering"), () -> TExpression.intConst(2)).access();
        case TERMINATION:
          return this.moST.registerConstant(key("termination"), () -> TExpression.intConst(3)).access();
        default:
          assert false : "not applicable";
          return this.moST.empty();
      }
    }

    /**
     * @return clock for {@code iClock}
     */
    public TClock getClock(IClock iClock) {
      return this.registerClock(iClock.getName());
    }

    /**
     * @return observer success state
     */
    public TState getSuccessState() {
      return this.registerState("Success");
    }
  }

  /**
   * Generate observer automaton and a success formula for the observer automaton,
   * i.e., a formula expressing when the observer has been traversed completely
   * and has finished in an accepting state
   *
   * @param uInteraction a (UML) interaction
   * @param modelSymbolTable shared model symbol table
   * @return a pair of an observer automaton and a query
   */
  public static Pair<TTemplate, TFormula> create(UInteraction uInteraction, ModelSymbolTable modelSymbolTable) {
    var iAutomaton = uInteraction.getAutomaton();
    var observerSymbolTable = modelSymbolTable.getObserverSymbolTable();
    var observer = IdaTranslator.translateAutomaton(iAutomaton, uInteraction.getMessageContext(), observerSymbolTable);
    var success = IdaTranslator.translateQuery(iAutomaton, observerSymbolTable);
    return new Pair<>(observer, success);
  }

  private Interaction(ModelSymbolTable modelSymbolTable) {
    this.inST = modelSymbolTable.getObserverSymbolTable();
  }

  @SuppressWarnings("unused")
  private TTemplate translate(UInteraction uInteraction) {
    var observer = this.inST.getTemplate();

    var uMessages = uInteraction.getSortedMessages();
    var uOccurrences = new ArrayList<UMessageOccurrenceSpecification>();
    for (var uMessage : uMessages) {
      var uMessageSendEvent = uMessage.getSendEvent();
      var uMessageReceiveEvent = uMessage.getReceiveEvent();
      if (uMessageSendEvent != null)
        uOccurrences.add(uMessageSendEvent);
      if (uMessageReceiveEvent != null)
        uOccurrences.add(uMessageReceiveEvent);
    }

    if (uOccurrences.isEmpty()) {
      TState state = new TState("Wait");
      observer.addState(state);
      state.setInitial(true);
      TTransition transition = acceptTransition(state, state);
      observer.addTransition(transition);
      return observer;
    }

    var timers = new HashMap<UOccurrenceSpecification.Data, @Nullable TClock>();
    // Clocks of the observer
    for (UTiming timing : uInteraction.getTimings()) {
      UOccurrenceSpecification.Data lowerData = timing.getLowerData();
      if (timers.get(lowerData) == null) {
        TClock timer = new TClock(lowerData.getName());
        observer.addClock(timer);
        timers.put(lowerData, timer);
      }
    }

    // States of the observer
    var waitStates = new HashMap<UMessageOccurrenceSpecification, TState>();
    var committedStates = new HashMap<UMessageOccurrenceSpecification, TState>();
    for (int i = 0; i < uOccurrences.size(); i++) {
      var uOccurrence = requireNonNull(uOccurrences.get(i));
      var uOccurrenceName = makeOccurrenceName(uOccurrence, i);
      var waitState = new TState("Wait" + uOccurrenceName);
      observer.addState(waitState);
      waitStates.put(uOccurrence, waitState);
      var committedState = new TState("Check" + uOccurrenceName + "Sent");
      committedState.setCommitted(true);
      observer.addState(committedState);
      committedStates.put(uOccurrence, committedState);
    }
    requireNonNull(waitStates.get(uOccurrences.get(0))).setInitial(true);
    var successState = this.inST.getSuccessState();

    // Transitions of the observer
    for (int i = 0; i < uOccurrences.size(); i++) {
      var uOccurrence = requireNonNull(uOccurrences.get(i));
      var waitState = requireNonNull(waitStates.get(uOccurrence));
      var committedState = requireNonNull(committedStates.get(uOccurrence));
      var checkedState = (i == uOccurrences.size()-1 ? successState : requireNonNull(waitStates.get(uOccurrences.get(i+1))));

      // Accept an observation on wait state
      TTransition acceptTransition = acceptTransition(waitState, committedState);
      observer.addTransition(acceptTransition);

      // Stuttering on wait state
      TTransition stutterTransition = new TTransition(committedState, waitState);
      observer.addTransition(stutterTransition);

      // Check for occurrence
      TTransition checkTransition = checkTransition(uInteraction, uOccurrence, committedState, checkedState, timers);
      observer.addTransition(checkTransition);
    }

    return observer;
  }

  /**
   * Accept an observation.
   *
   * @param from source state 
   * @param to target state
   * @return transition for accepting a message
   */
  private TTransition acceptTransition(TState from, TState to) {
    TTransition acceptTransition = new TTransition(from, to);
    acceptTransition.setSynchronisation(inST.toObserver().receive());
    acceptTransition.addUpdate(inST.obs().assign(inST.obsComm()));
    acceptTransition.addUpdate(inST.obsComm().assign(inST.emptyObservation()));
    return acceptTransition;
  }

  private class MessageTranslator implements UOccurrenceSpecification.InContextVisitor<TTransition> {
    private UInteraction uInteraction;
    private @Nullable UMessageOccurrenceSpecification uOccurrence = null;
    private TState from;
    private TState to;
    private Map<UOccurrenceSpecification.Data, TClock> timers;
    
    MessageTranslator(UInteraction uInteraction, TState from, TState to, Map<UOccurrenceSpecification.Data, TClock> timers) {
      this.uInteraction = uInteraction;
      this.from = from;
      this.to = to;
      this.timers = timers;
    }
    
    TTransition process(UMessageOccurrenceSpecification occurrence) {
      this.uOccurrence = occurrence;
      return occurrence.accept(this);
    }
    
    @Override
    public UContext getContext() {
      return uInteraction.getMessageContext();
    }

    public TTransition occurrence(UObject uSender, @Nullable UBehavioural uBehavioural, @Nullable List<UExpression> uArguments, @Nullable UObject uReceiver, TConstant.Access kind) {
      var moST = inST.getModelSymbolTable();
      var obs = inST.obs();
      var obsMsg = obs.access(inST.MSG);
      var obsKind = obs.access(inST.KIND);

      var checkTransition = new TTransition(from, to);
      if (uBehavioural != null)
        checkTransition.addGuard(TExpression.eq(obsMsg.access(moST.ID), moST.id(uBehavioural)));
      checkTransition.addGuard(TExpression.eq(obsMsg.access(moST.SENDER), moST.id(uSender)));
      if (uReceiver != null)
        checkTransition.addGuard(TExpression.eq(obsMsg.access(moST.RECEIVER), moST.id(uReceiver)));
      // java.lang.System.out.println(message.hashCode());
      for (UTiming timing : uInteraction.getTimings()) {
        for (UOccurrenceSpecification lower : timing.getLowers()) {
          if (lower.equals(uOccurrence))
            checkTransition.addUpdate(requireNonNull(timers.get(timing.getLowerData())).access().reset());
        }
      }
      if (uArguments != null) {
        for (int j = 0; j < uArguments.size(); j++) {
          UExpression value = requireNonNull(uArguments.get(j));
          checkTransition.addGuard(TExpression.eq(obsMsg.access(moST.PARAMS).access(TExpression.intConst(j)),
                                                  UMLTranslator.translateValue(value, moST.getCollaboration().getConstraintsContext(), moST)));
        }
      }
      for (UTiming timing : uInteraction.getTimings()) {
        for (UOccurrenceSpecification upper : timing.getUppers()) {
          if (upper.equals(uOccurrence))
            checkTransition.addGuard(translateTiming(timing, timers));
        }
      }
      checkTransition.addGuard(TExpression.eq(obsKind, kind));
      return checkTransition;
    }

    public TTransition onSend(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return occurrence(sender, behavioural, arguments, receiver, inST.getObservationKind(Observation.SENDING));
    }
    
    public TTransition onReceive(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return occurrence(sender, behavioural, arguments, receiver, inST.getObservationKind(Observation.RECEIVING));
    }
    
    public TTransition onTermination(UObject object) {
      return occurrence(object, null, null, null, inST.getObservationKind(Observation.TERMINATION));
    }
  }

  /**
   * Check an observation for sending/receiving a message.
   * 
   * @param message message
   * @param from source state
   * @param to target state
   * @param timers aggregate timers
   * @return transition for checking sending of a message
   */
  private TTransition checkTransition(UInteraction uInteraction, UMessageOccurrenceSpecification message, TState from, TState to, Map<UOccurrenceSpecification.Data, TClock> timers) {
    return new MessageTranslator(uInteraction, from, to, timers).process(message);
  }

  private class TimingTranslator implements UTiming.InContextVisitor<TGuard> {
    private ModelSymbolTable modelSymbolTable;
    private UContext uContext;
    private Map<UOccurrenceSpecification.Data, TClock> timers;

    TimingTranslator(ModelSymbolTable modelSymbolTable, UContext context, Map<UOccurrenceSpecification.Data, TClock> timers) {
      this.modelSymbolTable = modelSymbolTable;
      this.uContext = context;
      this.timers = timers;
    }
    
    TGuard guard(UTiming timing) {
      return timing.accept(this);
    }

    public @NonNull UContext getContext() {
      return this.uContext;
    }

    TClock.Access getTimerAccess(UOccurrenceSpecification.Data occurrenceData) {
      TClock timer = timers.get(occurrenceData);
      if (timer == null)
        throw new RuntimeException("No clock for occurrence specification `" + occurrenceData.toString() + "'");
      return timer.access();
    }

    public TGuard onLt(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound) {
      return TClockCondition.lt(getTimerAccess(lowerData), UMLTranslator.translateValue(bound, this.getContext(), this.modelSymbolTable));
    }

    public TGuard onLeq(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound) {
      return TClockCondition.leq(getTimerAccess(lowerData), UMLTranslator.translateValue(bound, this.getContext(), this.modelSymbolTable));
    }

    public TGuard onGt(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound) {
      return TClockCondition.gt(getTimerAccess(lowerData), UMLTranslator.translateValue(bound, this.getContext(), this.modelSymbolTable));
    }

    public TGuard onGeq(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound) {
      return TClockCondition.geq(getTimerAccess(lowerData), UMLTranslator.translateValue(bound, this.getContext(), this.modelSymbolTable));
    }
  }

  /**
   * Compute guard for checking a timing.
   *
   * @param uTiming timing
   * @param timers aggregated timers
   * @return a clock guard checking the timing
   */
  private TGuard translateTiming(UTiming uTiming, Map<UOccurrenceSpecification.Data, TClock> timers) {
    return (new TimingTranslator(this.inST.getModelSymbolTable(), uTiming.getBoundContext(), timers)).guard(uTiming);
  }

  /**
   * Make a readable occurrence name.
   * 
   * @param occurrence occurrence
   * @param n occurrence number
   * @return readable occurrence name
   */
  private String makeOccurrenceName(@Nullable UMessageOccurrenceSpecification occurrence, int n) {
    String occurrenceName = "";
    if (occurrence != null)
      occurrenceName = occurrence.getName();
    if (occurrenceName.equals(""))
      return "O" + n;
    else {
      occurrenceName = occurrenceName.substring(0, 1).toUpperCase() + occurrenceName.substring(1, occurrenceName.length()) + "_" + n;
      return occurrenceName;
    }
  }

  /**
   * Compute notification to observer for an outgoing event
   * (which will be empty, if no observer is present).
   *
   * What is to be observed is taken from the global communication variable.
   *
   * @param smST state machine symbol table
   * @return annotation net for observing an outgoing event
   */
  public static List<TAnnotation> prepareObserveSending(StateMachine.SymbolTable smST) {
    var moST = smST.getModelSymbolTable();
    if (!moST.hasObserver())
      return new ArrayList<>();

    var inST = moST.getObserverSymbolTable();
    var obsComm = inST.obsComm();
    return Arrays.asList(obsComm.access(inST.MSG).assign(moST.comm()),
                         obsComm.access(inST.KIND).assign(inST.getObservationKind(Observation.SENDING)));
  }

  public static TNet observeSending(StateMachine.SymbolTable smST) {
    var moST = smST.getModelSymbolTable();
    if (!moST.hasObserver())
      return TNet.trans();

    var inST = moST.getObserverSymbolTable();
    return TNet.trans().add(inST.toObserver().send());
  }

  /**
   * Compute notification to observer for an incoming event
   * (which will be empty, if no observer is present).
   * 
   * @param smST state machine symbol table
   * @return annotation net for observing an incoming event
   */ 
  public static TNet observeReceiving(StateMachine.SymbolTable smST) {
    var observer = TNet.trans();
    var moST = smST.getModelSymbolTable();
    if (!moST.hasObserver())
      return observer;
      
    var inST = moST.getObserverSymbolTable();
    observer.add(inST.toObserver().send());
    observer.add(inST.obsComm().access(inST.MSG).assign(smST.current()));
    observer.add(inST.obsComm().access(inST.KIND).assign(inST.getObservationKind(Observation.RECEIVING)));
    return observer;
  }
}
