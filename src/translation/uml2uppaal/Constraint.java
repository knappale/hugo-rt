package translation.uml2uppaal;

import java.util.Set;
import java.util.Stack;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAttribute;
import uml.UClass;
import uml.UContext;
import uml.UObject;
import uml.ocl.OExpression;
import uml.ocl.OOperator;
import uml.ocl.OTCTLConstraint;
import uml.ocl.OTCTLFormula;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UConfiguration;
import uppaal.TOperator;
import uppaal.TProcess;
import uppaal.TQuery;
import uppaal.TState;
import uppaal.query.TFormula;
import uppaal.query.TTerm;

import static util.Objects.requireNonNull;


/**
 * Translation of (simple) UML/OCL constraints into UPPAAL
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class Constraint {
  /**
   * Translate a (UML/OCL) constraint into an UPPAAL query.
   * 
   * @param constraint a (UML/OCL) constraint
   * @param moST shared symbol table
   * @return an UPPAAL query
   */
  public static TQuery create(OTCTLConstraint constraint, ModelSymbolTable moST) {
    return translateConstraint(constraint, moST);
  }

  static final class ConstraintTranslator implements OTCTLConstraint.Visitor<TQuery> {
    private ModelSymbolTable moST;

    ConstraintTranslator(ModelSymbolTable moST) {
      this.moST = moST;
    }

    TQuery process(OTCTLConstraint constraint) {
      return constraint.accept(this);
    }

    public TQuery onAG(OTCTLFormula formula) {
      return TQuery.ag(translateFormula(formula, moST));
    }

    public TQuery onAF(OTCTLFormula formula) {
      return TQuery.af(translateFormula(formula, moST));
    }

    public TQuery onEG(OTCTLFormula formula) {
      return TQuery.eg(translateFormula(formula, moST));
    }

    public TQuery onEF(OTCTLFormula formula) {
      return TQuery.ef(translateFormula(formula, moST));
    }
  }

  public static TQuery translateConstraint(OTCTLConstraint constraint, ModelSymbolTable moST) {
    return (new ConstraintTranslator(moST)).process(constraint);
  }

  static final class FormulaTranslator implements OTCTLFormula.InContextVisitor<TFormula> {
    private ModelSymbolTable moST;
    private UContext context;
    private Stack<OTCTLFormula> formulae = new Stack<>();

    FormulaTranslator(UContext context, ModelSymbolTable moST) {
      this.context = context;
      this.moST = moST;
    }

    TFormula process(OTCTLFormula formula) {
      formulae.push(formula);
      TFormula tFormula = formula.accept(this);
      formulae.pop();
      return tFormula;
    }

    String exceptionMsg() {
      return "Cannot translate formula `" + formulae.peek() + "' in context `" + context.description() + "' into UPPAAL";
    }

    public UContext getContext() {
      return this.context;
    }

    public TFormula onExpression(OExpression expression) {
      TTerm uTerm = translateExpression(expression, moST);
      return TFormula.term(uTerm);
    }

    public TFormula onDeadlock() {
      // There is a deadlock
      TFormula deadlockFormula = TFormula.deadlock();

      // But not in the queues
      if (!moST.getProperties().isBlocking()) {
        for (UObject object : moST.getCollaboration().getObjects())
          deadlockFormula = TFormula.and(deadlockFormula, onOverflow(object));
      }

      // ... nor all state machines are in their final states
      TFormula finalFormula = TFormula.trueConst();
      for (UObject object : moST.getCollaboration().getObjects()) {
        UStateMachine stateMachine = object.getC1ass().getStateMachine();
        if (stateMachine == null)
          continue;

        if (stateMachine.getRegions().stream().allMatch(uRegion -> uRegion.getFinal() != null)) {
          TFormula finalStatesFormula = stateMachine.getRegions().stream().
              map(uRegion -> TFormula.term(getStateTerm(object, requireNonNull(uRegion.getFinal()), moST))).
              reduce(TFormula.trueConst(), TFormula::and);
          finalFormula = TFormula.and(finalFormula, finalStatesFormula);
        }
      }
      // (if there is at least one final state)
      if (!finalFormula.equals(TFormula.trueConst()))
        deadlockFormula = TFormula.and(deadlockFormula, TFormula.neg(finalFormula));
      return deadlockFormula;
    }

    public TFormula onFail() {
      TFormula failFormula = TFormula.falseConst();
      for (UObject object : moST.getCollaboration().getObjects()) {
        UStateMachine uStateMachine = object.getC1ass().getStateMachine();
        if (uStateMachine == null)
          continue;

        var smST = moST.getStateMachineSymbolTable(uStateMachine);
        TProcess stateMachineProcess = smST.getProcess(object);
        TState failState = smST.getFailState();
        failFormula = TFormula.or(failFormula, TFormula.term(TTerm.state(stateMachineProcess, failState)));
      }
      return failFormula;
    }

    public TFormula onOverflow(UObject object) {
      TFormula overflowFormula = TFormula.falseConst();
      UStateMachine uStateMachine = object.getC1ass().getStateMachine();
      if (uStateMachine == null)
        return overflowFormula;
      if (moST.getProperties().isBlocking())
        return overflowFormula;

      var smST = moST.getStateMachineSymbolTable(uStateMachine);
      var quST = smST.getQueueSymbolTable();
      var buST = moST.getBufferSymbolTable();
      var stateMachineProcess = smST.getProcess(object);
      var internalOverflowState = quST.internalsOverflow();
      overflowFormula = TFormula.or(overflowFormula, TFormula.term(TTerm.state(stateMachineProcess, internalOverflowState)));
      var deferredOverflowState = quST.deferredsOverflow();
      overflowFormula = TFormula.or(overflowFormula, TFormula.term(TTerm.state(stateMachineProcess, deferredOverflowState)));
      var externalsOverflowState = quST.externalsOverflow();
      overflowFormula = TFormula.or(overflowFormula, TFormula.term(TTerm.state(stateMachineProcess, externalsOverflowState)));
      var queueProcess = buST.getProcess(object);
      if (queueProcess != null) {
        var bufferOverflowState = buST.bufferOverflow();
        overflowFormula = TFormula.or(overflowFormula, TFormula.term(TTerm.state(queueProcess, bufferOverflowState)));
      }
      return overflowFormula;
    }
    
    public TFormula onNeg(OTCTLFormula formula) {
      return TFormula.neg(process(formula));
    }

    public TFormula onJunctional(OTCTLFormula leftFormula, OOperator op, OTCTLFormula rightFormula) {
      TFormula left = process(leftFormula);
      TFormula right = process(rightFormula);
      switch (op) {
        case FAND:
          return TFormula.and(left, right);
        case FOR:
          return TFormula.or(left, right);
        case FIMPL:
          return TFormula.imply(left, right);
        //$CASES-OMITTED$
        default:
          throw new RuntimeException(exceptionMsg());
      }
    }
  }

  public static TFormula translateFormula(OTCTLFormula formula, ModelSymbolTable moST) {
    return (new FormulaTranslator(moST.getCollaboration().getConstraintsContext(), moST)).process(formula);
  }

  static final class ExpressionTranslator implements OExpression.InContextVisitor<TTerm> {
    private ModelSymbolTable moST;
    private UContext uContext;
    private @Nullable OExpression oExpression;

    ExpressionTranslator(UContext context, ModelSymbolTable moST) {
      this.uContext = context;
      this.moST = moST;
    }

    TTerm process(OExpression oExpression) {
      this.oExpression = oExpression;
      return oExpression.accept(this);
    }

    RuntimeException exception() {
      return new RuntimeException("Cannot translate OCL expression `" + this.oExpression + "' in model `" + uContext.description() + "' into UPPAAL");
    }

    public UContext getContext() {
      return this.uContext;
    }

    public TTerm onBooleanConstant(boolean booleanConstant) {
      return TTerm.boolConst(booleanConstant);
    }

    public TTerm onIntegerConstant(int integerConstant) {
      return TTerm.intConst(integerConstant);
    }

    public TTerm onNullConstant() {
      return TTerm.field(moST.empty());
    }

    public TTerm onReference(UAttribute uAttribute) {
      return TTerm.field(moST.getClassSymbolTable((UClass)uAttribute.getOwner()).access(uAttribute));
    }

    public TTerm onReference(UObject uObject, UAttribute uAttribute) {
      UStateMachine stateMachine = uObject.getC1ass().getStateMachine();
      assert stateMachine != null;
      TProcess stateMachineProcess = moST.getStateMachineSymbolTable(stateMachine).getProcess(uObject);
      StateMachine.SymbolTable smST = moST.getStateMachineSymbolTable(stateMachine);
      return TTerm.field(stateMachineProcess, smST.access(uAttribute));
    }

    public TTerm onArrayReference(UAttribute uAttribute, OExpression offset) {
      return TTerm.field(moST.getClassSymbolTable((UClass)uAttribute.getOwner()).access(uAttribute).access(process(offset)));
    }

    public TTerm onArrayReference(UObject uObject, UAttribute uAttribute, OExpression offset) {
      UStateMachine stateMachine = uObject.getC1ass().getStateMachine();
      assert stateMachine != null;
      TProcess stateMachineProcess = moST.getStateMachineSymbolTable(stateMachine).getProcess(uObject);
      StateMachine.SymbolTable smST = moST.getStateMachineSymbolTable(stateMachine);
      return TTerm.field(stateMachineProcess, smST.access(uAttribute).access(process(offset)));
    }

    public TTerm onState(UObject uObject, UVertex uVertex) {
      return getStateTerm(uObject, uVertex, moST);
    }

    public TTerm onUnary(OOperator op, OExpression expression) {
      TTerm result = process(expression);
      switch (op) {
        case UPLUS:
          return result;
        case UMINUS:
          return TTerm.unary(TOperator.UMINUS, result);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }

    public TTerm onArithmetical(OExpression leftExpression, OOperator op, OExpression rightExpression) {
      TTerm left = process(leftExpression);
      TTerm right = process(rightExpression);
      switch (op) {
        case ADD:
          return TTerm.arithmetical(left, TOperator.ADD, right);
        case SUB:
          return TTerm.arithmetical(left, TOperator.SUB, right);
        case MULT:
          return TTerm.arithmetical(left, TOperator.MULT, right);
        case DIV:
          return TTerm.arithmetical(left, TOperator.DIV, right);
        case MOD:
          return TTerm.arithmetical(left, TOperator.MOD, right);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }

    public TTerm onRelational(OExpression leftExpression, OOperator op, OExpression rightExpression) {
      TTerm left = translateExpression(leftExpression, this.moST);
      TTerm right = translateExpression(rightExpression, this.moST);
      switch (op) {
        case LT:
          return TTerm.relational(left, TOperator.LT, right);
        case LEQ:
          return TTerm.relational(left, TOperator.LEQ, right);
        case EQ:
          return TTerm.relational(left, TOperator.EQ, right);
        case NEQ:
          return TTerm.relational(left, TOperator.NEQ, right);
        case GEQ:
          return TTerm.relational(left, TOperator.GEQ, right);
        case GT:
          return TTerm.relational(left, TOperator.GT, right);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }
  }

  public static TTerm translateExpression(OExpression oExpression, ModelSymbolTable moST) {
    return (new ExpressionTranslator(moST.getCollaboration().getConstraintsContext(), moST)).process(oExpression);
  }

  private static TTerm getStateTerm(UObject uObject, UVertex uVertex, ModelSymbolTable moST) {
    UStateMachine stateMachine = uObject.getC1ass().getStateMachine();
    if (stateMachine == null)
      return TTerm.falseConst();
    StateMachine.SymbolTable smST = moST.getStateMachineSymbolTable(stateMachine);
    TProcess stateMachineProcess = smST.getProcess(uObject);

    if (!smST.isSmileBased()) {
      Set<UConfiguration> configurations = stateMachine.getConfigurations();
      // Message.debug("Configurations = " + configurations);
      TTerm stateTerm = TTerm.falseConst();
      for (UConfiguration configuration : configurations) {
        if (configuration.contains(uVertex)) {
          if (smST.getState(configuration) != null)
            stateTerm = TTerm.or(stateTerm, TTerm.state(stateMachineProcess, smST.getState(configuration)));
        }
      }
      // Message.debug("stateFormula = " + stateFormula);
      return stateTerm;
    }
    else {
      TTerm stateTerm = TTerm.falseConst();
      stateTerm = TTerm.or(stateTerm, TTerm.eq(TTerm.field(stateMachineProcess, smST.access(uVertex)), TTerm.field(stateMachineProcess, smST.getConstant(uVertex).access())));
      // Message.debug("stateFormula = " + stateFormula);
      return stateTerm;
    }
  }
}
