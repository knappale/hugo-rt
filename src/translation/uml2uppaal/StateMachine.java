package translation.uml2uppaal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UObject;
import uml.UOperation;
import uml.UStatual;
import uml.run.UObjectState;
import uml.smile.SConstant;
import uml.smile.SMachine;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UPseudoState;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UCompletionTree;
import uml.statemachine.semantics.UCompoundTransition;
import uml.statemachine.semantics.UConfiguration;
import uml.statemachine.semantics.UConflictFreeSet;
import uml.statemachine.semantics.UTimerTree;
import uppaal.TChannel;
import uppaal.TClock;
import uppaal.TClockCondition;
import uppaal.TClockUpperBound;
import uppaal.TConstant;
import uppaal.TDataType;
import uppaal.TExpression;
import uppaal.TField;
import uppaal.TOperator;
import uppaal.TProcess;
import uppaal.TState;
import uppaal.TSynchronisation;
import uppaal.TTemplate;
import uppaal.TVariable;
import uppaal.builder.TNet;
import uppaal.builder.TSubTemplate;
import uppaal.run.TProcessState;
import uppaal.run.TSystemState;
import util.Lists;
import util.Sets;
import util.Strings;

import static util.Objects.requireNonNull;


/**
 * Generate a UPPAAL representation of the UML state machine of an
 * object.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class StateMachine {
  private SymbolTable smST;

  public static class SymbolTable extends ClassSymbolTable {
    private ModelSymbolTable moST;
    private Queue.SymbolTable quST;
    private UStateMachine uStateMachine;
    private List<UVertex> sortedVertices;
    private List<UEvent> sortedInternalEvents;
    private UTimerTree timerTree;
    private UCompletionTree completionTree;
    private boolean isSmile = false;

    private Map<SVariable, TVariable> stateVariablesMap = new HashMap<>();
    private int transitionsConstantsCounter = 1;

    /**
     * Create a state machine symbol table for a state machine with a
     * model symbol table
     * 
     * @param stateMachine state machine symbol table's state machine
     * @param moST system symbol table
     * @return a state machine symbol table
     */
    public SymbolTable(UStateMachine uStateMachine, ModelSymbolTable moST) {
      super(uStateMachine.getC1ass(), moST.registerTemplate("cls" + Strings.capitalise(uStateMachine.getC1ass().getName()) + "StateMachine"), moST);
      this.uStateMachine = uStateMachine;
      this.sortedVertices = Lists.sort(new ArrayList<>(uStateMachine.getVertices()), UVertex::getName);
      this.sortedInternalEvents = Lists.sort(new ArrayList<>(uStateMachine.getInternalEvents()), UEvent::getName);
      this.completionTree = UCompletionTree.create(uStateMachine);
      this.timerTree = UTimerTree.create(uStateMachine);
      this.moST = moST;
      this.quST = new Queue.SymbolTable(this);
      // TODO (AK060311) The following check calls for a correct implementation of acknowledgments in the flattening translation
      boolean notFlatable = this.getModel().getClasses().stream().anyMatch(uClass -> !uClass.getOperations().isEmpty()) ||
                            uStateMachine.getWaitStates().size() > 0 ||
                            uStateMachine.getVertices().stream().filter(v -> v instanceof UPseudoState).map(v -> (@NonNull UPseudoState)v).anyMatch(pseudoState ->
                                pseudoState.getKind() == UPseudoState.Kind.CHOICE ||
                                pseudoState.getKind() == UPseudoState.Kind.SHALLOWHISTORY ||
                                (pseudoState.getKind() == UPseudoState.Kind.INITIAL && pseudoState.getOutgoingCompounds().stream().anyMatch(uCompound ->
                                                                                           !pseudoState.isAncestor(uCompound.getMainSourceVertex()))));
      this.isSmile = this.getProperties().isSmileBased() || notFlatable;
    }

    /**
     * @return this symbol table's underlying state machine
     */
    public UStateMachine getStateMachine() {
      return this.uStateMachine;
    }

    /**
     * @return this symbol table's underlying model symbol table
     */
    public ModelSymbolTable getModelSymbolTable() {
      return this.moST;
    }

    /**
     * @return this symbol table's extending queue symbol table
     */
    public Queue.SymbolTable getQueueSymbolTable() {
      return this.quST;
    }

    /**
     * @return whether this symbol table uses a Smile-based translation
     */
    public boolean isSmileBased() {
      return this.isSmile;
    }

    /**
     * @return this symbol table's underlying Smile machine
     */
    public SMachine getMachine() {
      return requireNonNull(this.uStateMachine.getMachine(this.getProperties()));
    }

    /**
     * @return success state
     */
    public TState getSuccessState() {
      return this.registerState("Success");
    }

    /**
     * @return success state
     */
    public TState getFailState() {
      return this.registerState("Fail");
    }

    /**
     * @return constant for UML event of the underlying state machine
     */
    public TConstant.Access id(UEvent uEvent) {
      assert uEvent.matches(this.getStateMachine()) : "not applicable for " + uEvent.declaration();
      if (uEvent.isCompletion() || uEvent.isTime())
        return this.registerConstant(key(uEvent, uEvent.getName()),
                                     () -> TDataType.intType(),
                                     () -> TExpression.intConst(this.sortedInternalEvents.indexOf(uEvent)+this.moST.getMaxExternalEventsNumber()+1)).access();
      return super.id(uEvent);
    }

    /**
     * @return UML event represented by {@code n}; or {@code null} if no such event can be found
     */
    public @Nullable UEvent getEvent(int n) {
      // Is it an external event?
      if (n <= moST.getMaxExternalEventsNumber()) {
        var uBehavioural = this.getBehavioural(n);
        if (uBehavioural != null)
          return UEvent.behavioural(uBehavioural);
      }
      // Is it an internal event?
      n = (n - moST.getMaxExternalEventsNumber())-1;
      if (0 <= n && n < this.sortedInternalEvents.size())
        return this.sortedInternalEvents.get(n);
      return null;
    }

    /**
     * @return UPPAAL constant for {@code uVertex}
     */
    public TConstant getConstant(UVertex uVertex) {
      return this.registerConstant(key(uVertex, uVertex.getName()),
                                   () -> TDataType.intType(),
                                   () -> TExpression.intConst(this.sortedVertices.indexOf(uVertex)+1));
    }

    /**
     * @return access to UPPAAL constant for {@code uVertex}
     */
    public TConstant.Access id(UVertex uVertex) {
      return this.getConstant(uVertex).access();
    }

    /**
     * @return UML state vertex represented by {@code n}; or {@code null} if no such state can be found
     */
    public @Nullable UVertex getVertex(int n) {
      n = n-1;
      if (0 <= n && n < this.sortedVertices.size())
        return this.sortedVertices.get(n);
      return null;
    }

    /**
     * @return UPPAAL constant for UML compound transition
     */
    public TConstant.Access id(UCompoundTransition uTransition) {
      return this.registerConstant(key(uTransition, uTransition.getName()), () -> TExpression.intConst(transitionsConstantsCounter++)).access();
    }

    @Override
    public TField.Access access(UStatual uStatual) {
      return uStatual.cases(uAttribute -> super.access(uAttribute),
                            uParameter -> this.current().access(this.moST.PARAMS).access(uParameter.getNumber()),
                            uObject -> moST.id(uObject));
    }

    /**
     * @return access to UPPAAL constant for {@code sConstant}
     */
    public TConstant.Access id(SConstant sConstant) {
      return sConstant.new Cases<TConstant.Access>().
        empty(() -> this.moST.empty()).
        vertex(uVertex -> id(uVertex)).
        transition(uTransition -> id(uTransition)).
        event(uEvent -> id(uEvent)).
        apply();
    }

    /**
     * @return a (small) data type capable of all possible {@code sVariable} values
     */
    private TDataType getVariableType(SVariable sVariable) {
      return sVariable.new Cases<TDataType>().
          flag(v -> TDataType.boolType()).
          state(v -> TDataType.range(this.sortedVertices.size()+1)).
          transition(v -> TDataType.range(this.uStateMachine.getCompounds().size()+1)).
          apply();
    }

    /**
     * @return UPPAAL variable for {@code sVariable}
     */
    public TVariable.Access access(SVariable sVariable) {
      return this.registerVariable(key(sVariable, sVariable.getName()),
                                   () -> this.getVariableType(sVariable),
                                   () -> moST.empty(),
                                   tVariable -> this.stateVariablesMap.put(sVariable, tVariable)).access();
    }

    /**
     * @return access to UPPAAL variable representing a UML vertex
     */
    public TVariable.Access access(UVertex uVertex) {
      return this.access(this.getMachine().getStateVariable(uVertex));
    }

    /**
     * @return accumulated variable representing Smile state variables
     */
    public Collection<TVariable> getStateVariables() {
      return this.stateVariablesMap.values();
    }

    /**
     * Retrieve symbol table's underlying state machine's completion tree.
     * 
     * @return underlying completion tree
     */
    public UCompletionTree getCompletionTree() {
      return this.completionTree;
    }

    /**
     * @return access to the representation of the completion of {@code uState}
     */
    public TVariable.Access completed(UState uState) {
      var completedVariable = this.registerVariable(key("completed"), () -> TDataType.array(TDataType.boolType(), this.getCompletionTree().getMaxCompletionsNumber()));
      var completionConstant = this.registerConstant(key(uState, "cmpl" + Strings.capitalise(uState.getName())), () -> TExpression.intConst(this.getCompletionTree().getCompletionNumber(uState)));
      return completedVariable.access(completionConstant.access());
    }

    /**
     * @return underlying state machine's timer tree
     */
    public UTimerTree getTimerTree() {
      return this.timerTree;
    }

    private TConstant getTimerConstant(UEvent uEvent) {
      return this.registerConstant(key(uEvent, "tmr" + Strings.capitalise(uEvent.getName())), () -> TExpression.intConst(this.getTimerTree().getTimerNumber(uEvent)));
    }

    /**
     * @return access to the representation of timed-out {@code uEvent}
     */
    public TVariable.Access timedOut(UEvent uEvent) {
      var timedOutArray = this.registerVariable(key("timedOut"), () -> TDataType.array(TDataType.boolType(), this.getTimerTree().getMaxTimersNumber()));
      return timedOutArray.access(this.getTimerConstant(uEvent).access());
    }

    /**
     * @return access to lower bound of (time) {@code uEvent}
     */
    public TConstant.Access timerLow(UEvent uEvent) {
      return this.registerConstant(key(uEvent, "low" + uEvent.getName()), () -> UMLTranslator.translateValue(uEvent.getTimeLowExpression(), uEvent.getTimeContext(), this)).access();
    }

    /**
     * @return access to upper bound of (time) {@code uEvent}
     */
    public TConstant.Access timerHigh(UEvent uEvent) {
      return this.registerConstant(key(uEvent, "high" + uEvent.getName()), () -> UMLTranslator.translateValue(uEvent.getTimeHighExpression(), uEvent.getTimeContext(), this)).access();
    }

    private TClock timers() {
      return this.registerClock("timers", () -> this.timerTree.getMaxTimersNumber());
    }

    /**
     * @return access to clock for time {@code uEvent}
     */
    public TClock.Access timer(UEvent uEvent) {
      return this.timers().access(this.getTimerConstant(uEvent).access());
    }

    /**
     * @return access to clock for {@code timerNumber}
     */
    public TClock.Access timer(int timerNumber) {
      return this.timers().access(timerNumber);
    }

    /**
     * @return UPPAAL channel for acknowledgements of state machine
     *         symbol table's state machine
     */
    public TChannel.Access getAck() {
      return moST.getAcknowledgementChannels().access(this.self());
    }

    public TVariable.Access waitForAck() {
      return this.registerVariable(key("waitForAck"), () -> TDataType.boolType()).access();
    }

    // UPPAAL channels for communication to state machine processes and
    // with the queue processes
    // ----------------------------------------------------------------

    /**
     * @return UPPAAL channel for state machine symbol table's state
     *         machine (via network)
     */
    public TChannel.Access getToStateMachine() {
      return moST.toStateMachine(this.self());
    }

    // UPPAAL variables for keeping local communication information
    // ------------------------------------------------------------

    /**
     * @return UPPAAL stimulus variable
     */
    public TVariable.Access current() {
      return this.registerVariable(key("current"), () -> moST.getMessageType()).access();
    }

    /**
     * It is enough just to send an acknowledgement without stating
     * which operation call is acknowledged, since the caller waits
     * for an acknowledgement and all operation calls in the caller
     * are done sequentially.
     *
     * @return acknowledgement synchronisation
     */
    public TSynchronisation acknowledge() {
      return moST.getAcknowledgementChannels().access(this.current().access(moST.SENDER)).send();
    }

    private Map<UConfiguration, TState> configurationStates = new HashMap<>();
    private Map<UConfiguration, TState> fetchStates = new HashMap<>();
    private Map<UConfiguration, TState> preFetchStates = new HashMap<>();

    /**
     * @return state for @code uConfiguration}
     */
    public TState getState(UConfiguration uConfiguration) {
      return this.registerState(uConfiguration.getName(), state -> this.configurationStates.put(uConfiguration, state));
    }

    /**
     * @return state for pre-fetching in {@code uConfiguration}
     */
    public TState getPreFetchState(UConfiguration uConfiguration) {
      return this.registerState("prefetch_" + uConfiguration.getName(), state -> this.preFetchStates.put(uConfiguration, state));
    }

    /**
     * @return state for fetching in {@code uConfiguration}
     */
    public TState getFetchState(UConfiguration uConfiguration) {
      return this.registerState("fetch_" + uConfiguration.getName(), state -> this.fetchStates.put(uConfiguration, state));
    }

    /**
     * Determine which UML configuration is represented by an UPPAAL state.
     * 
     * @param state an UPPAAL state
     * @return UML configuration represented by {@code state}; or
     * {@code null} if no such configuration exists
     */
    public @Nullable UConfiguration getConfiguration(TState state) {
      for (var uConfiguration : this.configurationStates.keySet()) {
        if (state.equals(this.configurationStates.get(uConfiguration)))
          return uConfiguration;
      }
      for (var uConfiguration : this.fetchStates.keySet()) {
        if (state.equals(this.fetchStates.get(uConfiguration)))
          return uConfiguration;
      }
      for (var uConfiguration : this.preFetchStates.keySet()) {
        if (state.equals(this.preFetchStates.get(uConfiguration)))
          return uConfiguration;
      }
      return null;
    }
  }

  /**
   * Create a UPPAAL representation of a UML state machine
   *
   * There are two translation schemes: A Smile-based translation,
   * where a state machine is accompanied with, possible several,
   * timer processes.  And a flattening translation which directly
   * incorporates the timers.  However, flattening is currently not
   * implemented for state machines containing choice- or
   * history-pseudo states.  By default, the flattening translation
   * scheme is chosen whenever possible using the Smile-based
   * translation as a fall-back.  The user may decide to use the
   * Smile-based translation scheme uniformly.
   *
   * Every state machine has an event queue.
   *
   * @param uStateMachine a UML state machine
   * @param moST shared model symbol table
   * @return list of UPPAAL templates
   */
  public static List<TTemplate> create(UStateMachine uStateMachine, ModelSymbolTable moST) {
    List<TTemplate> templates = new ArrayList<>();

    // Create state machine process template
    var smST = moST.getStateMachineSymbolTable(uStateMachine);
    var classTemplate = smST.getTemplate();
    if (smST.isSmileBased())
      classTemplate.addSubProcess(SmileTranslator.translateMachine(uStateMachine.getMachine(moST.getProperties()), smST));
    else
      classTemplate.addSubProcess(new StateMachine(uStateMachine, moST).translate(uStateMachine));
    templates.add(classTemplate);

    return templates;
  }
  
  public static void reconstruct(UObjectState objectState, TSystemState systemState, ModelSymbolTable moST) {
    UObject uObject = objectState.getObject();
    UStateMachine uStateMachine = uObject.getC1ass().getStateMachine();
    if (uStateMachine == null)
      return;

    StateMachine.SymbolTable smST = moST.getStateMachineSymbolTable(uStateMachine);
    if (smST.isSmileBased())
      SmileTranslator.reconstruct(objectState, systemState, moST);
    else
      new StateMachine(uStateMachine, moST).reconstruct(objectState, systemState);
  }

  private StateMachine(UStateMachine uStateMachine, ModelSymbolTable moST) {
    this.smST = moST.getStateMachineSymbolTable(uStateMachine);
  }

  public static class AnnotationStateProducer implements TSubTemplate.StateProducer {
    private StateMachine.SymbolTable smST;
    private @Nullable TState entry = null;
    private @Nullable TState exit = null;
    
    AnnotationStateProducer(@Nullable TState entry, @Nullable TState exit, StateMachine.SymbolTable smST) {
      this.entry = entry;
      this.exit = exit;
      this.smST = smST;
    }
    
    @Override
    public TState getEntryState(boolean isCommitted, boolean isUrgent) {
      TState entry = this.entry;
      if (entry != null)
        return entry;

      entry = getNewState();
      entry.setCommitted(isCommitted);
      entry.setUrgent(isUrgent);
      this.entry = entry;
      return entry;
    }
    
    @Override
    public TState getExitState() {
      TState exit = this.exit;
      if (exit != null)
        return exit;

      exit = getNewState();
      exit.setCommitted(true);
      this.exit = exit;
      return exit;
    }
    
    @Override
    public TState getNewState() {
      return new TState(smST.uniqueIdentifier("S"));
    }

    @Override
    public Collection<TVariable> getCommunicationVariables() {
      var moST = smST.getModelSymbolTable();
      return Sets.union(Sets.singleton(moST.comm().getVariable()),
                                       moST.hasObserver() ? Sets.singleton(moST.getObserverSymbolTable().obsComm().getVariable()) : Sets.empty());
    }
  }

  /**
   * Create a UPPAAL representation of the UML state machine of an
   * object by flattening the state machine.
   *
   * @param configurations set of configurations of state machine
   * @param smST translation symbol table
   * @return UPPAAL sub-process representing flattened state machine
   */
  public TSubTemplate translate(UStateMachine uStateMachine) {
    Set<UConfiguration> configurations = uStateMachine.getConfigurations();

    var moST = smST.getModelSymbolTable();
    var quST = smST.getQueueSymbolTable();

    var hasDeferrableEvents = !smST.getStateMachine().getDeferrableEvents().isEmpty();

    // Create states, identify initial state
    UConfiguration initialConfiguration = null;
    for (UConfiguration configuration : configurations) {
      var configurationState = smST.getState(configuration);
      if (configuration.isInitial())
        initialConfiguration = configuration;
      if (configuration.isPseudo())
        configurationState.setCommitted(true);
        
      if (!configuration.isPseudo()) {
        var preFetchState = smST.getPreFetchState(configuration);
        preFetchState.setCommitted(true);
      }

      if (!configuration.isPseudo()) {
        var fetchState = smST.getFetchState(configuration);
        fetchState.setUrgent(true);
      }
    }
    TSubTemplate subProcess = new TSubTemplate(smST.getState(requireNonNull(initialConfiguration)), null);

    // Determine for each configuration which time events are possibly
    // enabled
    Map<UConfiguration, Set<UEvent>> configurationsTimeEvents = new HashMap<>();
    for (UConfiguration configuration : configurations) {
      configurationsTimeEvents.put(configuration, configuration.getTimeOutables());
    }

    // Add invariants
    for (UConfiguration configuration : configurations) {
      @NonNull TState configurationState = smST.getState(configuration);
      for (UEvent timeEvent : requireNonNull(configurationsTimeEvents.get(configuration))) {
        if (timeEvent.getTimeLowExpression().equals(timeEvent.getTimeHighExpression()))
          configurationState.addInvariant(TClockUpperBound.leq(smST.timer(timeEvent), smST.timerLow(timeEvent)));
        else
          configurationState.addInvariant(TClockUpperBound.leq(smST.timer(timeEvent), smST.timerHigh(timeEvent)));
      }
    }

    // Create transitions
    for (UConfiguration sourceConfiguration : configurations) {
      TState sourceConfigurationState = smST.getState(sourceConfiguration);
      TState sourcePreFetchState = smST.getPreFetchState(sourceConfiguration);
      TState sourceFetchState = smST.getFetchState(sourceConfiguration);
      Set<UConflictFreeSet> sourceOutgoingMaximalConflictFrees = sourceConfiguration.getOutgoingMaximalConflictFrees();

      // Create time-out events
      @NonNull Set<UEvent> sourceTimers = requireNonNull(configurationsTimeEvents.get(sourceConfiguration));
      for (UEvent timeEvent : sourceTimers) {
        UConfiguration timedOutConfiguration = sourceConfiguration.getTimedOut(timeEvent);
        if (timedOutConfiguration == null)
          continue;

        TState timedOutFetchState = smST.getPreFetchState(timedOutConfiguration);

        var timedOut = TNet.trans();
        timedOut.add(TClockCondition.geq(smST.timer(timeEvent), smST.timerLow(timeEvent)));
        timedOut.add(TExpression.eq(quST.internals().access(TExpression.arithmetical(quST.internalsCapacity(), TOperator.SUB, TExpression.intConst(1))), moST.empty()));
        for (int i = quST.getProperties().getInternalQueueCapacity()-1; i > 0; i--)
          timedOut.add(quST.internals().access(i).assign(quST.internals().access(i-1)));
        timedOut.add(quST.internals().access(0).assign(smST.id(timeEvent)));
        timedOut.add(smST.timer(timeEvent).reset());
        
        var timedOutAnnotations = TNet.choice();
        timedOutAnnotations.add(timedOut);
        if (!moST.getProperties().isBlocking()) {
          var overflow = TNet.trans(quST.internalsOverflow());
          overflow.add(TClockCondition.geq(smST.timer(timeEvent), smST.timerLow(timeEvent)));
          overflow.add(TExpression.neq(quST.internals().access(TExpression.arithmetical(quST.internalsCapacity(), TOperator.SUB, TExpression.intConst(1))), moST.empty()));
          timedOutAnnotations.add(overflow);
        }
        TSubTemplate timedOutSubProcess = TSubTemplate.annotationNet(timedOutAnnotations, new AnnotationStateProducer(sourceConfigurationState, timedOutFetchState, smST));
        sourceConfigurationState.setCommitted(false);
        subProcess.addSubProcess(timedOutSubProcess);
      }

      TState fetched = null;
      // In pseudo-configurations no event has to be fetched
      if (sourceConfiguration.isPseudo())
        fetched = sourceConfigurationState;
      else {
        // Guard for deferring events
        TExpression deferringGuard = TExpression.falseConst();
        for (UEvent deferrable : sourceConfiguration.getDeferrables())
          deferringGuard = TExpression.or(deferringGuard, deferringGuard(sourceConfiguration, deferrable));

        // Guard for acknowledging operation calls
        TExpression acknowledgingGuard = TExpression.falseConst();
        for (UOperation operation : smST.getStateMachine().getC1ass().getOperations())
          acknowledgingGuard = TExpression.or(acknowledgingGuard, TExpression.eq(smST.current().access("id"), smST.id(UEvent.call(operation))));

        // Guard for accepting an event, i.e., some transition is
        // enabled
        TExpression acceptingGuard = TExpression.falseConst();
        for (UConflictFreeSet sourceOutgoingMaximalConflictFree : sourceOutgoingMaximalConflictFrees) {
          acceptingGuard = TExpression.or(acceptingGuard, executionGuard(sourceConfiguration, sourceOutgoingMaximalConflictFree));
        }
        // Message.debug(acceptingGuard.toString());

        // Reset stimulus
        var reset = TNet.trans();
        reset.add(smST.current().assign(moST.emptyMessage()));
        TSubTemplate resetSubProcess = TSubTemplate.annotationNet(reset, new AnnotationStateProducer(sourcePreFetchState, sourceFetchState, smST));
        subProcess.addSubProcess(resetSubProcess);

        // Fetch and acknowledge an event from the event queue,
        // provided there is no event in the internal queue
        var fetchExternal1 = TNet.trans();
        fetchExternal1.add(TExpression.neg(quST.internalsEnabled()));
        if (hasDeferrableEvents)
          fetchExternal1.add(TExpression.neg(quST.deferredsEnabled()));
        TSubTemplate fetchExternal1SubProcess = TSubTemplate.annotationNet(fetchExternal1, new AnnotationStateProducer(sourceFetchState, sourceConfigurationState, smST));
        subProcess.addSubProcess(fetchExternal1SubProcess);

        var fetchExternal2 = TNet.trans();
        fetchExternal2.add(smST.getToStateMachine().receive());
        fetchExternal2.add(smST.current().assign(moST.comm()));
        fetchExternal2.add(smST.current().assign(moST.emptyMessage()));
        var fetchExternal = TNet.seq();
        fetchExternal.add(fetchExternal1);
        fetchExternal.add(fetchExternal2);
        if (!acknowledgingGuard.equals(TExpression.falseConst())) {
          // Only acknowledge, when event will not be deferred
          acknowledgingGuard = TExpression.and(acknowledgingGuard, TExpression.neg(deferringGuard));
          // If acknowledgement guard is satisfied, send acknowledgment
          var doAcknowledgement = TNet.seq();
          doAcknowledgement.add(acknowledgingGuard);
          doAcknowledgement.add(smST.acknowledge());
          // If acknowledgement guard is not satisfied, do nothing
          var noAcknowledgement = TNet.seq();
          noAcknowledgement.add(TExpression.neg(acknowledgingGuard));
          fetchExternal.add(doAcknowledgement);
          fetchExternal.add(noAcknowledgement);
        }
        TSubTemplate fetchExternal2SubProcess = TSubTemplate.annotationNet(fetchExternal2, new AnnotationStateProducer(sourceConfigurationState, null, smST));
        subProcess.addSubProcess(fetchExternal2SubProcess);

        fetched = fetchExternal2SubProcess.getExit();

        // Fetch an event from the internal event queue
        var fetchInternal = TNet.trans();
        fetchInternal.add(TExpression.neq(quST.internals().access(0), moST.empty()));
        fetchInternal.add(smST.current().access("id").assign(quST.internals().access(0)));
        for (int i = 0; i < quST.getProperties().getInternalQueueCapacity()-1; i++)
          fetchInternal.add(quST.internals().access(i).assign(quST.internals().access(i+1)));
        fetchInternal.add(quST.internals().access(quST.getProperties().getInternalQueueCapacity()-1).assign(moST.empty()));
        TSubTemplate fetchInternalSubProcess = TSubTemplate.annotationNet(fetchInternal, new AnnotationStateProducer(sourceFetchState, fetched, smST));
        subProcess.addSubProcess(fetchInternalSubProcess);

        // Fetch an event from the deferred event queue
        if (hasDeferrableEvents) {
          var fetchDeferred = TNet.trans();
          fetchDeferred.add(TExpression.neg(quST.internalsEnabled()));
          fetchDeferred.add(quST.deferredsEnabled());
          fetchDeferred.add(smST.current().assign(quST.deferreds().access(0)));
          for (int i = 0; i < quST.getProperties().getDeferredQueueCapacity()-1; i++)
            fetchDeferred.add(quST.deferreds().access(i).assign(quST.deferreds().access(i+1)));
          fetchDeferred.add(quST.deferreds().access(quST.getProperties().getDeferredQueueCapacity()-1).assign(moST.emptyMessage()));
          TSubTemplate fetchDeferredSubProcess = TSubTemplate.annotationNet(fetchDeferred, new AnnotationStateProducer(sourceFetchState, fetched, smST));
          subProcess.addSubProcess(fetchDeferredSubProcess);
        }
        
        // Defer all deferrable events
        if (!deferringGuard.equals(TExpression.falseConst())) {
          TNet deferring = TNet.trans(TExpression.eq(quST.deferreds().access(TExpression.arithmetical(quST.deferredsCapacity(), TOperator.SUB, 1)).access(moST.ID), moST.empty()),
                                      quST.deferredsEnqueue());
          if (!moST.getProperties().isBlocking())
            deferring = TNet.choice(deferring,
                                    TNet.trans(quST.deferredsOverflow(),
                                               TExpression.neq(quST.deferreds().access(TExpression.arithmetical(quST.deferredsCapacity(), TOperator.SUB, 1)).access(moST.ID), moST.empty())));
          TSubTemplate deferringSubProcess = TSubTemplate.annotationNet(TNet.seq(TNet.trans(deferringGuard, quST.chosen().assign(TExpression.falseConst())), deferring),
                                                                        new AnnotationStateProducer(fetched, sourceFetchState, smST));
          subProcess.addSubProcess(deferringSubProcess);
        }

        // Ignore events that cannot be reacted to
        var ignoring = TNet.seq();
        ignoring.add(TExpression.and(TExpression.neg(acceptingGuard), TExpression.neg(deferringGuard)));
        TSubTemplate ignoringSubProcess = TSubTemplate.annotationNet(ignoring, new AnnotationStateProducer(fetched, sourcePreFetchState, smST));
        subProcess.addSubProcess(ignoringSubProcess);
      }

      // Handle all outgoing maximal conflict free sets
      for (UConflictFreeSet outgoing : sourceOutgoingMaximalConflictFrees) {
        // Guard firing of outgoing transition
        TExpression executionGuard = executionGuard(sourceConfiguration, outgoing);

        // Combine guard and effect
        var effect = TNet.seq();
        if (!executionGuard.equals(TExpression.trueConst()))
          effect.add(executionGuard);
        effect.add(UMLTranslator.translateAction(outgoing.getExitAction(), outgoing.getExitContext(), smST));
        effect.add(UMLTranslator.translateAction(outgoing.getEffect(), outgoing.getEffectContext(), smST));
        effect.add(UMLTranslator.translateAction(outgoing.getEntryAction(), outgoing.getEntryContext(), smST));
        if (hasDeferrableEvents)
          effect.add(quST.chosen().assign(TExpression.falseConst()));

        UConfiguration targetConfiguration = outgoing.getTarget(sourceConfiguration);

        // Add timer initialisations
        @NonNull Set<UEvent> targetTimers = requireNonNull(configurationsTimeEvents.get(targetConfiguration));
        if (util.Sets.difference(targetTimers, sourceTimers).size() > 0) {
          var timers = TNet.trans();
          for (UEvent timeEvent : util.Sets.difference(targetTimers, sourceTimers)) {
            timers.add(smST.timer(timeEvent).reset());
          }
          effect.add(timers);
        }

        // Add completions thus changing the target configuration
        if (targetConfiguration.getCompletables().size() > 0) {
          var completions = TNet.seq();
          for (UEvent completionEvent : targetConfiguration.getCompletables()) {
            //-AK why do we check anything here?
            // if (outgoing.getEnteredStates().contains(completionEvent.getState())) {
            targetConfiguration = targetConfiguration.getCompletion(completionEvent);

            var completeAnnotations = TNet.trans();
            completeAnnotations.add(TExpression.eq(quST.internals().access(TExpression.arithmetical(quST.internalsCapacity(), TOperator.SUB, TExpression.intConst(1))), moST.empty()));
            for (int i = quST.getProperties().getInternalQueueCapacity()-1; i > 0; i--)
              completeAnnotations.add(quST.internals().access(i).assign(quST.internals().access(i-1)));
            completeAnnotations.add(quST.internals().access(0).assign(smST.id(UEvent.completion(completionEvent.getState()))));

            var annotations = TNet.choice();
            annotations.add(completeAnnotations);
            if (!smST.getProperties().isBlocking()) {
              var overflowAnnotations = TNet.trans(quST.internalsOverflow());
              overflowAnnotations.add(TExpression.neq(quST.internals().access(TExpression.arithmetical(quST.internalsCapacity(), TOperator.SUB, TExpression.intConst(1))), moST.empty()));
              annotations.add(overflowAnnotations);
            }

            completions.add(annotations);
          }
          effect.add(completions);
        }

        TState targetFetchState = smST.getPreFetchState(requireNonNull(targetConfiguration));
        TSubTemplate transitionSubProcess = TSubTemplate.annotationNet(effect, new AnnotationStateProducer(fetched, targetFetchState, smST));
        subProcess.addSubProcess(transitionSubProcess);
      }
    }

    // Add fail and success states (needed for queries)
    subProcess.addSubProcess(new TSubTemplate(smST.getFailState(), null));
    subProcess.addSubProcess(new TSubTemplate(smST.getSuccessState(), null));

    subProcess.getEntry().setInitial(true);
    return subProcess;
  }

  /**
   * Compute UPPAAL guard for executing a maximal conflict free set of
   * transitions.
   *
   * @param transition maximal conflict free set of UML transitions
   * @param symbolTable translation symbol table
   * @return UPPAAL guard
   */
  private TExpression executionGuard(UConfiguration source, UConflictFreeSet transition) {
    TExpression executionGuard = TExpression.trueConst();
    // Transitions outgoing from a pseudo-configuration do not
    // depend on events
    if (!source.isPseudo()) {
      // Completion transitions may show several outgoing states when
      // stemming from a join
      if (transition.getTrigger().isCompletion() && transition.getTrigger().getStates().size() > 1) {
        TExpression matchGuard = TExpression.falseConst();
        for (UState state : transition.getTrigger().getStates()) {
          matchGuard = TExpression.or(matchGuard, TExpression.eq(smST.current().access("id"), smST.id(UEvent.completion(state))));
        }
        executionGuard = TExpression.and(executionGuard, matchGuard);
      }
      else
        executionGuard = TExpression.and(executionGuard, TExpression.eq(smST.current().access("id"), smST.id(requireNonNull(transition.getTrigger()))));
    }
    executionGuard = TExpression.and(executionGuard, UMLTranslator.translateExpression(transition.getGuard(), transition.getGuardContext(), smST));
    return executionGuard;
  }

  /**
   * Compute UPPAAL guard for deferring an event in a configuration.
   *
   * @param configuration configuration with open deferrable
   * @param deferrable deferrable event in configuration
   * @param symbolTable translation symbol table
   * @return UPPAAL guard
   */
  private TExpression deferringGuard(UConfiguration configuration, UEvent deferrable) {
    TExpression deferringGuard = TExpression.eq(smST.current().access("id"), smST.id(deferrable));
    for (UConflictFreeSet outgoing : configuration.getOutgoingMaximalConflictFrees()) {
      if (outgoing.getTrigger().equals(deferrable))
        deferringGuard = TExpression.and(deferringGuard, TExpression.neg(UMLTranslator.translateExpression(outgoing.getGuard(), outgoing.getGuardContext(), smST)));
    }
    return deferringGuard;
  }
  
  public void reconstruct(UObjectState uObjectState, TSystemState systemState) {
    var uObject = uObjectState.getObject();

    TProcess objectProcess = smST.getProcess(uObject);
    TProcessState objectProcessState = systemState.getProcessState(objectProcess);
    if (objectProcessState == null) {
      util.Message.debug().info("No process state for object `" + uObject.getName() + "' found.");
      return;
    }

    UConfiguration configuration = null;
    TState state = objectProcessState.getState();
    if (state != null) {
      configuration = smST.getConfiguration(state);
      if (configuration == null)
        util.Message.debug().info("No configuration for state `" + state.getName() + "' of object `" + uObject.getName() + "' found.");
    }
    if (configuration != null) {
      uObjectState.setConfiguration(configuration);

      // Add timers for open time events
      for (UEvent timeEvent : configuration.getTimeOutables()) {
        var timer = smST.timer(timeEvent);
        int timerValue = objectProcessState.getValue(timer);
        uObjectState.addTimer(timeEvent, timerValue);
      } 
      // Add timers for closed time events
      for (UConflictFreeSet outgoing : configuration.getOutgoingMaximalConflictFrees()) {
        UEvent event = outgoing.getTrigger();
        if (event.isTime()) {
          var timer = smST.timer(event);
          int timerValue = objectProcessState.getValue(timer);
          uObjectState.addTimer(event, timerValue);
        }
      } 
    }
  }
}
