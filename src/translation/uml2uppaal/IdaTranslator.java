package translation.uml2uppaal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UBehavioural;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import uml.UType;
import uml.ida.IAction;
import uml.ida.IAutomaton;
import uml.ida.IBranch;
import uml.ida.IClock;
import uml.ida.IClockBound;
import uml.ida.IEvent;
import uml.ida.IExpression;
import uml.ida.IObservation;
import uml.ida.IOperator;
import uml.ida.IState;
import uml.ida.IVariable;
import uppaal.TClockCondition;
import uppaal.TClockUpperBound;
import uppaal.TDataType;
import uppaal.TExpression;
import uppaal.TGuard;
import uppaal.TOperator;
import uppaal.TTemplate;
import uppaal.TTransition;
import uppaal.TUpdate;
import uppaal.query.TFormula;
import uppaal.query.TTerm;
import util.Formatter;

import static util.Objects.requireNonNull;


/**
 * Translate an Ida automaton into an UPPAAL observer automaton.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 * @since 06/03/06
 */
@NonNullByDefault
public class IdaTranslator {
  private Interaction.SymbolTable inST;

  private IdaTranslator(Interaction.SymbolTable inST) {
    this.inST = inST;
  }

  /**
   * Generate observer automaton and a success formula for the observer automaton,
   * i.e., a formula expressing when the observer has been traversed completely
   * and has finished in an accepting state
   *
   * @param automaton an Ida automaton
   * @param uContext context for events
   * @param inST observer symbol table
   * @return an observer
   */
  public static TTemplate translateAutomaton(IAutomaton automaton, UContext uContext, Interaction.SymbolTable inST) {
    var translator = new IdaTranslator(inST);
    var observer = inST.getTemplate();

    // Set initial state
    inST.getState(requireNonNull(automaton.getInitialState())).setInitial(true);

    // Add received states and transitions from the decision state
    // to the received state for accepting an observation
    var triggereds = new HashSet<IState>();
    for (var iTransition : automaton.getTransitions()) {
      if (iTransition.getTrigger() != null)
        triggereds.add(iTransition.getFrom());
    }
    for (var from : triggereds)
      observer.addTransition(new TTransition(inST.getState(from), inST.getReceivedState(from), new ArrayList<>(), inST.toObserver().receive(), translator.acceptUpdates()));

    // Fill in transitions
    for (var iTransition : automaton.getTransitions()) {
      var guards = new ArrayList<TGuard>();

      var iTransitionGuard = iTransition.getGuard();
      if (iTransitionGuard != null) {
        var expressionGuard = translator.translateExpression(iTransitionGuard.getCondition());
        guards.add(expressionGuard);
      }
      var iTransitionClockBound = iTransition.getClockBound();
      if (iTransitionClockBound != null) {
        var clockBoundGuards = translator.translateClockBound(iTransitionClockBound);
        guards.addAll(clockBoundGuards);
      }

      // Non-triggered transitions start from a decision state
      var startState = inST.getState(iTransition.getFrom());

      var iTransitionTrigger = iTransition.getTrigger();
      if (iTransitionTrigger != null) {
        guards.add(translator.translateEvent(iTransitionTrigger, uContext));

        // Triggered transitions start from the received state
        startState = inST.getReceivedState(iTransition.getFrom());
      }

      for (IBranch iBranch : iTransition.getBranches()) {
        var updates = new ArrayList<TUpdate>();

        var iBranchEffect = iBranch.getLabel().getEffect();
        if (iBranchEffect != null) {
          var effectUpdates = translator.translateAction(iBranchEffect);
          updates.addAll(effectUpdates);
        }

        var decisionTransition = new TTransition(startState, inST.getState(iBranch.getTo()), guards, null, updates);
        observer.addTransition(decisionTransition);
      }
    }

    return observer;
  }

  public static TFormula translateQuery(IAutomaton iAutomaton, Interaction.SymbolTable inST) {
    var success = TFormula.falseConst();
    for (var acceptingState : iAutomaton.getAcceptingStates()) {
      var state = inST.getState(acceptingState);
      success = TFormula.or(success, TFormula.term(TTerm.state(inST.getProcess(), state)));
    }
    return success;
  }

  /**
   * Updates for accepting an observation.
   *
   * @return updates for accepting an observation
   */
  private List<TUpdate> acceptUpdates() {
    return Arrays.asList(inST.obs().assign(inST.obsComm()),
                         inST.obsComm().assign(inST.emptyObservation()));
  }

  private class OccurrenceSpecificationTranslator implements IObservation.InContextVisitor<TExpression> {
    private UContext uContext;

    OccurrenceSpecificationTranslator(UContext uContext) {
      this.uContext = uContext;
    }

    @Override
    public UContext getContext() {
      return this.uContext;
    }

    TExpression annotation(@Nullable IEvent event) {
      if (event == null)
        return uppaal.TExpression.trueConst();

      return event.getObservation().accept(this);
    }

    TExpression guard(IObservation specification) {
      return specification.accept(this);
    }

    public TExpression onSend() {
      return TExpression.eq(inST.obs().access(inST.KIND), inST.getObservationKind(Interaction.Observation.SENDING));
    }

    public TExpression onReceive() {
      return TExpression.eq(inST.obs().access(inST.KIND), inST.getObservationKind(Interaction.Observation.RECEIVING));
    }

    public TExpression onTermination() {
      return TExpression.eq(inST.obs().access(inST.KIND), inST.getObservationKind(Interaction.Observation.TERMINATION));
    }

    public TExpression onSender(uml.UObject object) {
      var moST = inST.getModelSymbolTable();
      return TExpression.eq(inST.obs().access(inST.MSG).access(moST.SENDER), moST.id(object));
    }

    public TExpression onReceiver(uml.UObject object) {
      var moST = inST.getModelSymbolTable();
      return TExpression.eq(inST.obs().access(inST.MSG).access(moST.RECEIVER), moST.id(object));
    }

    public TExpression onBehavioural(UBehavioural uBehavioural) {
      // Determine who can receive this behavioural.  This is necessary,
      // as behavioural numbers may not be globally unique, but depend on
      // the receiver.  Since we currently do not support inheritance it is enough
      // to check that the receiving object is one of the objects of the type of the
      // owner of the behavioural.
      var moST = inST.getModelSymbolTable();
      TExpression receivingGuard = TExpression.falseConst();
      for (UObject receivingObject : moST.getCollaboration().getObjects()) {
        if (UType.simple(uBehavioural.getOwner()).subsumes(receivingObject.getType())) {
          TExpression receiverGuard = TExpression.eq(inST.obs().access(inST.MSG).access(moST.RECEIVER), moST.id(receivingObject));
          receivingGuard = TExpression.or(receivingGuard, receiverGuard);
        }
      }
      TExpression behaviouralGuard = TExpression.eq(inST.obs().access(inST.MSG).access(moST.ID), moST.id(uBehavioural));
      return TExpression.and(behaviouralGuard, receivingGuard);
    }

    public TExpression onArguments(List<UExpression> arguments) {
      var moST = inST.getModelSymbolTable();
      TExpression guard = TExpression.trueConst();
      for (int j = 0; j < arguments.size(); j++) {
        UExpression value = requireNonNull(arguments.get(j));
        guard = TExpression.and(guard, TExpression.eq(requireNonNull(inST.obs().access(inST.MSG).access(moST.PARAMS)).access(TExpression.intConst(j)),
                                                      UMLTranslator.translateValue(value, this.getContext(), moST)));
      }
      return guard;
    }

    public TExpression onExpression(IExpression expression) {
      return translateExpression(expression);
    }

    public TExpression onNot(IObservation specification) {
      TExpression specificationGuard = guard(specification);
      return TExpression.neg(specificationGuard);
    }

    public TExpression onAnd(IObservation leftSpecification, IObservation rightSpecification) {
      TExpression leftSpecificationGuard = guard(leftSpecification);
      TExpression rightSpecificationGuard = guard(rightSpecification);
      return TExpression.and(leftSpecificationGuard, rightSpecificationGuard);
    }

    public TExpression onOr(IObservation leftSpecification, IObservation rightSpecification) {
      TExpression leftSpecificationGuard = guard(leftSpecification);
      TExpression rightSpecificationGuard = guard(rightSpecification);
      return TExpression.or(leftSpecificationGuard, rightSpecificationGuard);
    }
  }

  TExpression translateEvent(IEvent uEvent, UContext uContext) {
    return (new OccurrenceSpecificationTranslator(uContext)).annotation(uEvent);
  }

  private class ClockBoundTranslator implements IClockBound.Visitor<List<TGuard>> {
    ClockBoundTranslator() {
    }
    
    List<TGuard> guard(IClockBound timing) {
      return timing.accept(this);
    }

    public List<TGuard> onLt(IClock iClock, IExpression bound) {
      return new ArrayList<>(Arrays.asList(TClockUpperBound.lt(inST.getClock(iClock).access(), translateExpression(bound))));
    }

    public List<TGuard> onLeq(IClock iClock, IExpression bound) {
      return new ArrayList<>(Arrays.asList(TClockUpperBound.leq(inST.getClock(iClock).access(), translateExpression(bound))));
    }

    public List<TGuard> onGt(IClock iClock, IExpression bound) {
      return new ArrayList<>(Arrays.asList(TClockCondition.gt(inST.getClock(iClock).access(), translateExpression(bound))));
    }

    public List<TGuard> onGeq(IClock iClock, IExpression bound) {
      return new ArrayList<>(Arrays.asList(TClockCondition.geq(inST.getClock(iClock).access(), translateExpression(bound))));
    }

    public List<TGuard> onAnd(IClockBound leftClockBound, IClockBound rightClockBound) {
      List<TGuard> result = new ArrayList<>();
      result.addAll(guard(leftClockBound));
      result.addAll(guard(rightClockBound));
      return result;
    }
  }

  List<TGuard> translateClockBound(IClockBound uTiming) {
    return (new ClockBoundTranslator()).guard(uTiming);
  }

  private class ExpressionTranslator implements IExpression.Visitor<TExpression> {
    private @Nullable IExpression iExpression;

    ExpressionTranslator() {
    }

    TExpression expression(IExpression value) {
      this.iExpression = value;
      return value.accept(this);
    }

    IllegalStateException exception() {
      return new IllegalStateException("Cannot translate value expression " + Formatter.quoted(iExpression) + " in model " + Formatter.quoted(inST.getModel().getName()) + " into UPPAAL");
    }

    public TExpression onBooleanConstant(boolean booleanConstant) {
      return booleanConstant ? TExpression.trueConst() : TExpression.falseConst();
    }

    public TExpression onIntegerConstant(int integerConstant) {
      return TExpression.intConst(integerConstant);
    }

    public TExpression onInfinity() {
      // Translate into the biggest possible integer
      return TExpression.intConst(TDataType.intType().maxValue());
    }

    public TExpression onVariable(IVariable iVariable) {
      return inST.getVariable(iVariable).access();
    }

    public TExpression onArray(IVariable iVariable, int offset) {
      return inST.getVariable(iVariable).access(TExpression.intConst(offset));
    }

    public TExpression onExternal(UExpression uExpression, UContext uContext) {
      return UMLTranslator.translateExpression(uExpression, uContext, inST.getModelSymbolTable());
    }

    public TExpression onArithmetical(IExpression iLeftExpression, IOperator iOp, IExpression iRightExpression) {
      TExpression leftExpression = expression(iLeftExpression);
      TExpression rightExpression = expression(iRightExpression);
      switch (iOp) {
        case PLUS:
          return TExpression.arithmetical(leftExpression, TOperator.ADD, rightExpression);
        case MINUS:
          return TExpression.arithmetical(leftExpression, TOperator.SUB, rightExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }

    public TExpression onNegation(IExpression iExpression) {
      return TExpression.neg(expression(iExpression));
    }

    public TExpression onRelational(IExpression iLeftExpression, IOperator iOp, IExpression iRightExpression) {
      TExpression leftExpression = expression(iLeftExpression);
      TExpression rightExpression = expression(iRightExpression);
      switch (iOp) {
        case LT:
          return TExpression.lt(leftExpression, rightExpression);
        case LEQ:
          return TExpression.leq(leftExpression, rightExpression);
        case EQ:
          return TExpression.eq(leftExpression, rightExpression);
        case NEQ:
          return TExpression.neq(leftExpression, rightExpression);
        case GEQ:
          return TExpression.geq(leftExpression, rightExpression);
        case GT:
          return TExpression.gt(leftExpression, rightExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }

    public TExpression onJunctional(IExpression iLeftExpression, IOperator iOp, IExpression iRightExpression) {
      TExpression leftExpression = expression(iLeftExpression);
      TExpression rightExpression = expression(iRightExpression);
      switch (iOp) {
        case AND:
          return TExpression.and(leftExpression, rightExpression);
        case OR:
          return TExpression.or(leftExpression, rightExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }
  }

  TExpression translateExpression(IExpression iExpression) {
    return (new ExpressionTranslator()).expression(iExpression);
  }

  private class ActionTranslator implements IAction.Visitor<List<TUpdate>> {
    ActionTranslator() {
    }

    /**
     * Produce a list of UPPAAL updates from an Ida action.
     *
     * @param iAction an Ida action
     * @return a list of UPPAAL updates corresponding to the Ida action
     */
    List<TUpdate> updates(IAction iAction) {
      return iAction.accept(this);
    }

    public List<TUpdate> onSkip() {
      return new ArrayList<>();
    }

    public List<TUpdate> onAssign(IVariable variable, IExpression expression) {
      return new ArrayList<>(Arrays.asList(inST.getVariable(variable).access().assign(translateExpression(expression))));
    }

    public List<TUpdate> onReset(IVariable variable) {
      return new ArrayList<>(Arrays.asList(inST.getVariable(variable).access().assign(uppaal.TExpression.intConst(0))));
    }

    public List<TUpdate> onInc(IVariable variable) {
      return new ArrayList<>(Arrays.asList(inST.getVariable(variable).access().inc()));
    }

    public List<TUpdate> onDec(IVariable variable) {
      return new ArrayList<>(Arrays.asList(inST.getVariable(variable).access().dec()));
    }

    public List<TUpdate> onSet(IVariable variable, int offset) {
      return new ArrayList<>(Arrays.asList(inST.getVariable(variable).access(TExpression.intConst(offset)).assign(TExpression.intConst(1))));
    }

    public List<TUpdate> onReset(IVariable variable, int offset) {
      return new ArrayList<>(Arrays.asList(inST.getVariable(variable).access(TExpression.intConst(offset)).assign(TExpression.intConst(0))));
    }

    public List<TUpdate> onReset(IClock clock) {
      return new ArrayList<>(Arrays.asList(inST.getClock(clock).access().reset()));
    }

    public List<TUpdate> onSeq(IAction iLeftAction, IAction iRightAction) {
      List<TUpdate> updates = new ArrayList<>();
      updates.addAll(updates(iLeftAction));
      updates.addAll(updates(iRightAction));
      return updates;
    }
  }

  List<TUpdate> translateAction(IAction iAction) {
    return (new ActionTranslator()).updates(iAction);
  }
}
