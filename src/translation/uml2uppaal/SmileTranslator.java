package translation.uml2uppaal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAction;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import uml.run.UObjectState;
import uml.smile.SBranch;
import uml.smile.SBranching;
import uml.smile.SCommand;
import uml.smile.SGuard;
import uml.smile.SLiteral;
import uml.smile.SMachine;
import uml.smile.SPrimitive;
import uml.smile.SStatement;
import uml.smile.SValue;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UTransition;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UConfiguration;
import uml.statemachine.semantics.UVertexForest;
import uml.statemachine.semantics.UVertexTree;
import uppaal.TExpression;
import uppaal.TGuard;
import uppaal.TOperator;
import uppaal.TProcess;
import uppaal.TState;
import uppaal.TUpdate;
import uppaal.TVariable;
import uppaal.builder.TNet;
import uppaal.builder.TSubTemplate;
import uppaal.run.TProcessState;
import uppaal.run.TSystemState;
import util.Sets;

import static java.util.stream.Collectors.toSet;
import static util.Objects.requireNonNull;
import static util.Objects.toType;


/**
 * Translation of Smile systems (based on a (simple) UML model) to UPPAAL
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class SmileTranslator {
  @NonNullByDefault
  public static TSubTemplate translateMachine(SMachine sMachine, StateMachine.SymbolTable smST) {
    var moST = smST.getModelSymbolTable();
    var sStatement = sMachine.getStatement();
    var machineProcess = translateStatement(sStatement, smST);
    machineProcess.optimise(Sets.union(Sets.singleton(moST.comm().getVariable()),
                                       moST.hasObserver() ? Sets.singleton(moST.getObserverSymbolTable().obsComm().getVariable()) : Sets.empty()));
    // Add some states needed for queries
    // (We add them after optimisation since if they are not reachable
    // the would be optimised away.)
    machineProcess.addSubProcess(new TSubTemplate(smST.getFailState(), null));
    machineProcess.addSubProcess(new TSubTemplate(smST.getSuccessState(), null));
    machineProcess.getEntry().setInitial(true);
    return machineProcess;
  }

  @NonNullByDefault
  static final class AnnotationStateProducer implements TSubTemplate.StateProducer {
    private StateMachine.SymbolTable smST;

    AnnotationStateProducer(StateMachine.SymbolTable smST) {
      this.smST = smST;
    }

    @Override
    public TState getEntryState(boolean isCommitted, boolean isUrgent) {
      TState entry = getNewState();
      entry.setCommitted(isCommitted);
      entry.setUrgent(isUrgent);
      return entry;
    }

    @Override
    public TState getExitState() {
      TState exitState = getNewState();
      exitState.setCommitted(true);
      return exitState;
    }

    @Override
    public TState getNewState() {
      return new TState(smST.uniqueIdentifier("S"));
    }

    @Override
    public Collection<TVariable> getCommunicationVariables() {
      var moST = smST.getModelSymbolTable();
      return Sets.union(Sets.singleton(moST.comm().getVariable()),
                                       moST.hasObserver() ? Sets.singleton(moST.getObserverSymbolTable().obsComm().getVariable()) : Sets.empty());
    }
  }

  @NonNullByDefault
  static final class StatementTranslator implements SStatement.Visitor<TSubTemplate> {
    private StateMachine.SymbolTable smST;
    private SStatement sStatement;
    private Map<SStatement, TSubTemplate> subProcesses = new HashMap<>();
    private Stack<TState> breakStates = new Stack<>();

    StatementTranslator(SStatement sStatement, StateMachine.SymbolTable smST) {
      this.smST = smST;
      this.sStatement = sStatement;
    }

    /**
     * Produce an UPPAAL sub-process from a Smile statement.
     *
     * @return an UPPAAL sub-process corresponding to the Smile statement
     */
    TSubTemplate process() {
      return subProcess(this.sStatement);
    }

    TSubTemplate toSubTemplate(TNet net) {
      return TSubTemplate.annotationNet(net, new AnnotationStateProducer(smST));
    }

    /**
     * Produce an UPPAAL sub-process from a Smile sub-statement.
     *
     * @param sStatement a Smile statement
     * @return an UPPAAL sub-process corresponding to the Smile statement
     */
    private TSubTemplate subProcess(SStatement sStatement) {
      SStatement oldSStatement = this.sStatement;
      this.sStatement = sStatement;
      TSubTemplate subProcess = sStatement.accept(this);
      this.subProcesses.put(sStatement, subProcess);
      this.sStatement = oldSStatement;
      return subProcess;
    }

    @Override
    public TSubTemplate onSkip() {
      TState entry = new TState(this.smST.uniqueIdentifier("S"));
      entry.setCommitted(true);
      return new TSubTemplate(entry, entry);
    }

    private TSubTemplate assignment(SVariable sVariable, TExpression tExpression) {
      TState entry = new TState(this.smST.uniqueIdentifier("S"));
      entry.setCommitted(true);
      TState exit = new TState(this.smST.uniqueIdentifier("S"));
      exit.setCommitted(true);
      TSubTemplate subProcess = new TSubTemplate(entry, exit);
      List<TUpdate> updates = new ArrayList<>();
      updates.add(this.smST.access(sVariable).assign(tExpression));
      subProcess.addTransition(entry, exit, (TGuard)null, null, updates);
      return subProcess;
    }

    @Override
    public TSubTemplate onAssignment(SVariable sVariable, SValue sValue) {
      return assignment(sVariable, translateValue(sValue, this.smST));
    }

    @Override
    public TSubTemplate onAssignment(SVariable sVariable, SVariable otherSVariable) {
      return assignment(sVariable, this.smST.access(otherSVariable));
    }

    @Override
    public TSubTemplate onSequential(SStatement leftStm, SStatement rightStm) {
      Map<TState, TState> mergings = new HashMap<>();
      TSubTemplate left = subProcess(leftStm);
      TSubTemplate right = subProcess(rightStm);
      TSubTemplate subProcess = TSubTemplate.seq(left, right, mergings);
      return subProcess;
    }

    @Override
    public TSubTemplate onBranch(SGuard guardExpr, SStatement branchStm) {
      TSubTemplate branchProcess = subProcess(branchStm);
      TExpression guard = translateExpression(guardExpr, this.smST);

      if (guard.equals(TExpression.trueConst()))
        return branchProcess;

      TState guardEntry = new TState(this.smST.uniqueIdentifier("S"));
      guardEntry.setCommitted(true);
      TState guardExit = new TState(this.smST.uniqueIdentifier("S"));
      guardExit.setCommitted(true);
      TSubTemplate guardProcess = new TSubTemplate(guardEntry, guardExit);
      List<TGuard> guards = new ArrayList<>();
      guards.add(guard);
      guardProcess.addTransition(guardEntry, guardExit, guards, null, new ArrayList<>());
      Map<TState, TState> mergings = new HashMap<>();
      TSubTemplate subProcess = TSubTemplate.branch(guardProcess, branchProcess, mergings);
      return subProcess;
    }

    @Override
    public TSubTemplate onChoice(List<SBranch> branches, @Nullable SStatement elseStm) {
      List<TSubTemplate> branchProcesses = new ArrayList<>();
      for (SBranch branch : branches) {
        TSubTemplate branchProcess = this.onBranch(branch.getGuard(), branch.getEffect());
        branchProcesses.add(branchProcess);
      }

      if (elseStm != null) {
        SGuard elseGuard = SGuard.and(branches.stream().map(branch -> SGuard.neg(branch.getGuard())).collect(Collectors.toSet()));
        TSubTemplate elseProcess = this.onBranch(elseGuard, elseStm);
        branchProcesses.add(elseProcess);
      }
      Map<TState, TState> mergings = new HashMap<>();
      TSubTemplate subProcess = TSubTemplate.choice(branchProcesses, mergings);
      return subProcess;
    }

    @Override
    public TSubTemplate onLoop(List<SBranch> branches, @Nullable SStatement elseStm) {
      pushBreakState();
      List<TSubTemplate> branchProcesses = new ArrayList<>();
      for (SBranch branch : branches) {
        TSubTemplate branchProcess = this.onBranch(branch.getGuard(), branch.getEffect());
        branchProcesses.add(branchProcess);
      }
      if (elseStm != null) {
        SGuard elseGuard = SGuard.and(branches.stream().map(branch -> SGuard.neg(branch.getGuard())).collect(Collectors.toSet()));
        TSubTemplate elseProcess = this.onBranch(elseGuard, elseStm);
        branchProcesses.add(elseProcess);
      }
      Map<TState, TState> mergings = new HashMap<>();
      TSubTemplate subProcess = TSubTemplate.loop(branchProcesses, getBreakState(), mergings);
      popBreakState();
      return subProcess;
    }

    @Override
    public TSubTemplate onBreak() {
      return new TSubTemplate(getBreakState(), null);
    }

    @Override
    public TSubTemplate onAtomic(SStatement stm) {
      return subProcess(stm);
    }

    @Override
    public TSubTemplate onExternal(UAction external, UContext context) {
      return toSubTemplate(UMLTranslator.translateAction(external, context, smST));
    }

    @Override
    public TSubTemplate onInitialisation() {
      return toSubTemplate(TNet.trans());
    }

    @Override
    public TSubTemplate onFetch(Set<UState> waitStates) {
      return TSubTemplate.annotationNet(Queue.fetch(waitStates, smST), new AnnotationStateProducer(smST));
    }

    @Override
    public TSubTemplate onComplete(boolean keep, UState uState) {
      var moST = smST.getModelSymbolTable(); // Test
      var quST = smST.getQueueSymbolTable();
      TNet complete = TNet.seq(TNet.trans(TExpression.eq(quST.internals().access(TExpression.arithmetical(quST.internalsCapacity(), TOperator.SUB, 1)), moST.empty())))
                          .add(quST.internalsPush(UEvent.completion(uState)));
      if (!smST.getProperties().isBlocking())
        complete = TNet.choice(complete, TNet.trans(quST.internalsOverflow(),
                                                    TExpression.neq(quST.internals().access(TExpression.arithmetical(quST.internalsCapacity(), TOperator.SUB, 1)), moST.empty())));
      return toSubTemplate(TNet.seq(keep ? TNet.trans(smST.completed(uState).assign(true)) : TNet.trans()).add(complete));
    }

    @Override
    public TSubTemplate onUncomplete(UState uState) {
      var quST = smST.getQueueSymbolTable();
      var uncomplete = TNet.seq(TNet.trans(smST.completed(uState).assign(false))).
                            add(quST.internalsRemove(uState));
      return toSubTemplate(uncomplete);
    }

    @Override
    public TSubTemplate onDefer() {
      var moST = smST.getModelSymbolTable();
      var quST = smST.getQueueSymbolTable();
      TNet defer = TNet.trans(TExpression.eq(quST.deferreds().access(TExpression.arithmetical(quST.deferredsCapacity(), TOperator.SUB, 1)).access(moST.ID), moST.empty()),
                              quST.deferredsEnqueue());
      if (!smST.getProperties().isBlocking())
        defer = TNet.choice(defer,
                            TNet.trans(quST.deferredsOverflow(),
                                       TExpression.neq(quST.deferreds().access(TExpression.arithmetical(quST.deferredsCapacity(), TOperator.SUB, 1)).access(moST.ID), moST.empty())));
      return toSubTemplate(TNet.seq(TNet.trans(quST.chosen().assign(false)), defer));
    }

    /**
     * Compute an annotation net for having finished processing a behavioural
     * event, i.e., notify any interested parties, like an observer.
     */
    @Override
    public TSubTemplate onChosen() {
      var moST = smST.getModelSymbolTable();
      var quST = smST.getQueueSymbolTable();
      var chosen = TNet.seq();
      if (!smST.getStateMachine().getDeferrableEvents().isEmpty())
        chosen.add(quST.chosen().assign(true));
      if (moST.hasObserver()) {
        var inST = moST.getObserverSymbolTable();
        chosen.add(TNet.trans(inST.toObserver().send(),
                              inST.obsComm().access(inST.MSG).assign(smST.current()),
                              inST.obsComm().access(inST.KIND).assign(inST.getObservationKind(Interaction.Observation.RECEIVING))));
      }
      return TSubTemplate.annotationNet(chosen, new AnnotationStateProducer(smST));
    }

    @Override
    public TSubTemplate onAcknowledge() {
      return TSubTemplate.annotationNet(TNet.trans(smST.acknowledge()), new AnnotationStateProducer(smST));
    }

    @Override
    public TSubTemplate onStartTimer(boolean keep, UEvent uEvent) {
      var startTimer = TNet.trans();
      startTimer.add(smST.timedOut(uEvent).assign(false));
      startTimer.add(smST.timer(uEvent).reset());
      return TSubTemplate.annotationNet(startTimer, new AnnotationStateProducer(smST));
    }

    @Override
    public TSubTemplate onStopTimer(UEvent uEvent) {
      var stopTimer = TNet.trans();
      stopTimer.add(smST.timedOut(uEvent).assign(false));
      stopTimer.add(smST.timer(uEvent).reset());
      return TSubTemplate.annotationNet(stopTimer, new AnnotationStateProducer(smST));
    }

    @Override
    public TSubTemplate onSuccess() {
      return TSubTemplate.annotationNet(TNet.trans(smST.getSuccessState()), new AnnotationStateProducer(smST));
    }

    @Override
    public TSubTemplate onFail() {
      return TSubTemplate.annotationNet(TNet.trans(smST.getFailState()), new AnnotationStateProducer(smST));
    }

    @Override
    public TSubTemplate onAwait(SGuard sExpression) {
      return subProcess(SBranching.ifThenElse(sExpression, SCommand.skip(), SPrimitive.fail()));
    }

    /**
     * Push a new break state for a Smile loop.
     *
     * @return break state connected to (current) Smile loop
     */
    private void pushBreakState() {
      TState breakState = new TState(smST.uniqueIdentifier("S"));
      breakState.setCommitted(true);
      this.breakStates.push(breakState);
    }

    /**
     * Pop a break state for a Smile loop.
     */
    private void popBreakState() {
      this.breakStates.pop();
    }

    /**
     * Retrieve the current top-most break state for a Smile loop.
     */
    private TState getBreakState() {
      return this.breakStates.peek();
    }
  }

  @NonNullByDefault
  public static TSubTemplate translateStatement(SStatement sStatement, StateMachine.SymbolTable smST) {
    return new StatementTranslator(sStatement, smST).process();
  }

  @NonNullByDefault
  private static final class ExpressionTranslator implements SGuard.Visitor<TExpression>, SLiteral.Visitor<TExpression> {
    private StateMachine.SymbolTable smST;
    private Stack<SGuard> sExpressions = new Stack<>();

    ExpressionTranslator(StateMachine.SymbolTable smST) {
      this.smST = smST;
    }

    TExpression expression(SGuard sExpression) {
      sExpressions.push(sExpression);
      TExpression tExpression = sExpression.accept(this);
      sExpressions.pop();
      return tExpression;
    }

    @Override
    public TExpression onBooleanConstant(boolean booleanConstant) {
      return booleanConstant ? TExpression.trueConst() : TExpression.falseConst();
    }

    @Override
    public TExpression onEq(boolean positive, SVariable sVariable, Set<SValue> sValues) {
      TExpression eq = sValues.stream().map(sValue -> TExpression.eq(smST.access(sVariable), translateValue(sValue, smST))).
                                        reduce(TExpression.falseConst(), TExpression::or);
      return positive ? eq : TExpression.neg(eq);
    }

    @Override
    public TExpression onMatch(boolean positive, Set<UEvent> uEvents) {
      TExpression match = uEvents.stream().map(uEvent -> UMLTranslator.match(uEvent, this.smST)).reduce(TExpression.falseConst(), TExpression::or);
      return positive ? match : TExpression.neg(match);
    }
    
    @Override
    public TExpression onIsCompleted(boolean positive, UState uState) {
      var isCompleted = smST.completed(uState);
      return positive ? isCompleted : TExpression.neg(isCompleted);
    }

    @Override
    public TExpression onIsTimedOut(boolean positive, UEvent uEvent) {
      var isTimedOut = smST.timedOut(uEvent);
      return positive ? isTimedOut : TExpression.neg(isTimedOut);
    }

    @Override
    public TExpression onExternal(boolean positive, UExpression uExpression, UContext uContext) {
      TExpression external = UMLTranslator.translateExpression(uExpression, uContext, this.smST);
      return positive ? external : TExpression.neg(external);
    }

    @Override
    public TExpression onLiteral(SLiteral sLiteral) {
      return sLiteral.accept(this);
    }

    @Override
    public TExpression onAnd(SGuard leftSExpression, SGuard rightSExpression) {
      return TExpression.and(expression(leftSExpression), expression(rightSExpression));
    }

    @Override
    public TExpression onOr(SGuard leftSExpression, SGuard rightSExpression) {
      return TExpression.or(expression(leftSExpression), expression(rightSExpression));
    }
  }

  @NonNullByDefault
  public static TExpression translateExpression(SGuard expression, StateMachine.SymbolTable smST) {
    return new ExpressionTranslator(smST).expression(expression);
  }
  
  @NonNullByDefault
  private static TExpression translateValue(SValue sValue, StateMachine.SymbolTable smST) {
    return sValue.new Cases<TExpression>().booleanConstant(booleanConstant -> booleanConstant ? TExpression.trueConst() : TExpression.falseConst()).
                                           constant(sConstant -> smST.id(sConstant)).
                                           apply();
  }

  public static void reconstruct(UObjectState objectState, TSystemState systemState, ModelSymbolTable moST) {
    UObject object = objectState.getObject();
    UStateMachine stateMachine = object.getC1ass().getStateMachine();
    if (stateMachine == null)
      return;

    StateMachine.SymbolTable smST = moST.getStateMachineSymbolTable(stateMachine);
    if (!smST.isSmileBased())
      return;

    TProcess objectProcess = smST.getProcess(object);
    TProcessState objectProcessState = systemState.getProcessState(objectProcess);
    if (objectProcessState == null) {
      util.Message.debug().info("No process state for object `" + object.getName() + "' found.");
      return;
    }

    // Gather all state vertices we are in
    @NonNull Set<@NonNull UVertex> vertices = new HashSet<>();
    for (TVariable stateVariable : smST.getStateVariables()) {
      int stateVariableValue = objectProcessState.getValue(stateVariable.access());
      if (stateVariableValue != 0) {
        UVertex vertex = smST.getVertex(stateVariableValue);
        if (vertex != null)
          vertices.add(vertex);
      }
    }

    // Build state forest
    UVertexForest stateForest = stateMachine.getTargetStateForest(vertices);

    // Mark all completed states
    for (var uState : vertices.stream().flatMap(toType(UState.class)).filter(uState -> smST.getCompletionTree().isCompletionState(uState)).collect(toSet())) {
      int stateVariableValue = objectProcessState.getValue(smST.completed(uState));
      if (stateVariableValue != 0) {
        UVertexTree subStateTree = stateForest.getVertexTree(uState);
        if (subStateTree != null) {
          UVertexTree completedSubStateTree = UVertexTree.completed(subStateTree);
          stateForest = stateForest.getSubstituted(completedSubStateTree);
        }
      }
    }

    // Mark all timed-out states
    for (var uState : vertices.stream().flatMap(toType(UState.class)).filter(UState::hasOutgoingTime).collect(toSet())) {
      for (UTransition time : uState.getOutgoingTimes()) {
        UEvent timeEvent = time.getTrigger();
        int timedOutVariableValue = objectProcessState.getValue(smST.timedOut(timeEvent));
        if (timedOutVariableValue != 0) {
          UVertexTree subStateTree = stateForest.getVertexTree(uState);
          if (subStateTree != null) {
            @NonNull Set<@NonNull UEvent> timedOutEvents = new HashSet<>(subStateTree.getTopTimedOuts());
            timedOutEvents.add(timeEvent);
            UVertexTree timedOutSubStateTree = UVertexTree.timedOut(timedOutEvents, subStateTree);
            stateForest = stateForest.getSubstituted(timedOutSubStateTree);
          }
        }
      }
    }

    // Add timers
    for (UState state : vertices.stream().flatMap(toType(requireNonNull(UState.class))).filter(UState::hasOutgoingTime).collect(toSet())) {
      for (UTransition time : state.getOutgoingTimes()) {
        UEvent timeEvent = time.getTrigger();
        var timer = smST.timer(timeEvent);
        int timerValue = objectProcessState.getValue(timer);
        objectState.addTimer(timeEvent, timerValue);
      }
    }

    objectState.setStateForest(stateForest);
    if (!stateForest.isConfiguration())
      util.Message.debug().info("No complete configuration for object `" + object.getName() + "' found.");
    else {
      UConfiguration configuration = UConfiguration.create(stateForest);
      objectState.setConfiguration(configuration);
    }
  } 
}
