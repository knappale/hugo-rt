package translation.uml2uppaal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UExpression;
import uml.UObject;
import uml.UStatual;
import uml.interaction.UMessage;
import uml.run.UObjectState;
import uml.smile.SConstant;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uppaal.TBlock;
import uppaal.TClockCondition;
import uppaal.TClockUpperBound;
import uppaal.TConditional;
import uppaal.TConstant;
import uppaal.TDataType;
import uppaal.TExpression;
import uppaal.TField;
import uppaal.TFunction;
import uppaal.TInvariant;
import uppaal.TLoop;
import uppaal.TOperator;
import uppaal.TProcess;
import uppaal.TState;
import uppaal.TStatement;
import uppaal.TUpdate;
import uppaal.TVariable;
import uppaal.builder.TNet;
import uppaal.builder.TSubTemplate;
import uppaal.run.TProcessState;
import uppaal.run.TSystemState;
import util.Pair;
import util.Strings;

import static java.util.stream.Collectors.toList;

import static util.Objects.requireNonNull;


/**
 * Generate event queue
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class Queue {
  private SymbolTable quST;

  public static class SymbolTable extends translation.uml2uppaal.SymbolTable.Template {
    private ModelSymbolTable moST;
    private StateMachine.SymbolTable smST;
    private Map<Pair<Set<UEvent>, String>, TState> timerStates = new HashMap<>();

    public SymbolTable(StateMachine.SymbolTable smST) {
      super(smST.getModel(), smST.getTemplate());
      this.moST = smST.getModelSymbolTable();
      this.smST = smST;
    }

    @Override
    public TConstant.Access empty() {
      return moST.empty();
    }

    @Override
    public TConstant.Access self() {
      return smST.self();
    }

    @Override
    public TField.Access access(UStatual uStatual) {
      return smST.access(uStatual);
    }

    public ModelSymbolTable getModelSymbolTable() {
      return this.moST;
    }

    public StateMachine.SymbolTable getStateMachineSymbolTable() {
      return this.smST;
    }

    public TProcess getProcess(UObject uObject) {
      this.self();
      return moST.registerProcess("obj" + Strings.capitalise(uObject.getName()) + "Buffer", this.getTemplate(), parameter -> this.getArgument(parameter, uObject));
    }

    /**
     * @return access to variable for registering that some event has been chosen
     */
    public TVariable.Access chosen() {
      return smST.registerVariable(key("chosen"), () -> TDataType.boolType()).access();
    }

    /**
     * @return access to constant for external event queue's capacity
     */
    public TConstant.Access externalsCapacity() {
      return smST.registerConstant(key("externalsCapacity"), () -> TExpression.intConst(this.getProperties().getExternalQueueCapacity())).access();
    }

    /**
     * @return array variable for external event queue
     */
    public TVariable.Access externals() {
      return smST.registerVariable(key("externals"), () -> TDataType.array(this.moST.getMessageType(), this.externalsCapacity())).access();
    }

    /**
     * @return external event queue overflow state
     */
    public TState externalsOverflow() {
      return smST.registerState("ExternalsOverflow");
    }

    public TFunction.Call externalsEnqueue() {
      return smST.registerFunction("externals_enqueue", () -> getEnqueueFunctionDeclaration(this.externals(), this.externalsCapacity(), this)).call();
    }

    public TFunction.Call externalsPop() {
      return smST.registerFunction("externals_pop", () -> getPopFunctionDeclaration(this.externals(), this.externalsCapacity(), this)).call();
    }

    /**
     * @return guard expression whether the external event queue is fully occupied
     */
    public TExpression externalsFull() {
      return TExpression.neq(this.externals().access(TExpression.arithmetical(this.externalsCapacity(), TOperator.SUB, 1)).access(moST.ID), smST.empty());
    }

    /**
     * @return guard expression whether the external event queue currently offers an event
     */
    public TExpression externalsEnabled() {
      return TExpression.neq(this.externals().access(0).access(moST.ID), this.empty());
    }

    public TConstant.Access deferredsCapacity() {
      return smST.registerConstant(key("deferredsCapacity"), () -> TExpression.intConst(this.getProperties().getDeferredQueueCapacity())).access();
    }

    public TVariable.Access deferreds() {
      return smST.registerVariable(key("deferreds"), () -> TDataType.array(this.moST.getMessageType(), this.deferredsCapacity())).access();
    }

    public TFunction.Call deferredsEnqueue() {
      return smST.registerFunction("deferreds_enqueue", () -> getEnqueueFunctionDeclaration(this.deferreds(), this.deferredsCapacity(), this)).call();
    }

    public TFunction.Call deferredsPop() {
      return smST.registerFunction("deferreds_pop", () -> getPopFunctionDeclaration(this.deferreds(), this.deferredsCapacity(), this)).call();
    }

    /**
     * @return guard expression whether a deferred event is ready
     */
    public TExpression deferredsEnabled() {
      if (smST.getStateMachine().getDeferrableEvents().isEmpty())
        return TExpression.falseConst();
      return TExpression.and(this.chosen(), TExpression.neq(this.deferreds().access(0).access(moST.ID), moST.empty()));
    }

    /**
     * @return deferred event queue overflow state
     */
    public TState deferredsOverflow() {
      return smST.registerState("DeferredOverflow");
    }

    /**
     * @return access to constant for internal event queue's capacity
     */
    public TConstant.Access internalsCapacity() {
      return smST.registerConstant(key("internalsCapacity"), () -> TExpression.intConst(this.getProperties().getInternalQueueCapacity())).access();
    }

    /**
     * @return array variable for internal events
     */
    public TVariable.Access internals() {
      return smST.registerVariable(key("internals"), () -> TDataType.array(TDataType.range(smST.getStateMachine().getInternalEvents().size()),
                                                                           this.internalsCapacity())).access();
    }

    /**
     * @return guard expression whether the internal event queue is fully occupied
     */
    public TExpression internalsFull() {
      return TExpression.neq(this.internals().access(TExpression.arithmetical(this.externalsCapacity(), TOperator.SUB, 1)), smST.empty());
    }

    /**
     * @return internal event queue overflow state
     */
    public TState internalsOverflow() {
      return smST.registerState("InternalOverflow");
    }
    
    public TFunction.Call internalsPush(UEvent uEvent) {
      var internalsPushFunction = smST.registerFunction("internals_push", () -> getInternalsPushFunctionDeclaration(this));
      return internalsPushFunction.call(TExpression.arithmetical(smST.id(uEvent), TOperator.SUB, this.moST.getMaxExternalEventsNumber()));
    }

    public TFunction.Call internalsRemove(UState uState) {
      var internalsRemoveFunction = smST.registerFunction("internals_remove", () -> getInternalsRemoveFunctionDeclaration(this));
      return internalsRemoveFunction.call(TExpression.arithmetical(smST.id(UEvent.completion(uState)), TOperator.SUB, moST.getMaxExternalEventsNumber()));
    }

    public TFunction.Call internalsPop() {
      return smST.registerFunction("internals_pop", () -> getInternalsPopFunctionDeclaration(this)).call();
    }

    /**
     * @return guard expression whether the internal event queue currently offers an event
     */
    public TExpression internalsEnabled() {
      return TExpression.neq(this.internals().access(0), this.empty());
    }

    /**
     * @return state representing the timers in {@code timerSet} being active
     */
    public TState timersState(String prefix, Set<UEvent> timerSet) {
      var key = new Pair<>(timerSet, prefix);
      var timerState = this.timerStates.get(key);
      if (timerState != null)
        return timerState;

      var invariants = new ArrayList<TInvariant>();
      var id = prefix;
      for (var timeEvent : timerSet) {
        id += "_" + smST.getTimerTree().getTimerNumber(timeEvent) + "W";
        invariants.add(TClockUpperBound.leq(smST.timer(timeEvent), smST.timerHigh(timeEvent)));
      }
      timerState = new TState(smST.uniqueIdentifier(id));
      timerState.addInvariants(invariants);
      // if (timerSet.isEmpty())
      //   timerState.setUrgent(true);
      this.timerStates.put(key, timerState);
      return timerState;
    }

    /**
     * @return guard expression for the timers in {@code timerSet} being active
     */
    public TExpression timersActive(Set<UEvent> timerSet) {
      var timerGuards = new ArrayList<TExpression>();
      for (var timeEvent : smST.getTimerTree().getTimeEvents()) {
        var timerGuard = TExpression.neg(smST.timedOut(timeEvent));
        var timeEventState = requireNonNull(timeEvent.getState());
        timerGuard = TExpression.and(timerGuard, TExpression.eq(smST.access(timeEventState), smST.id(SConstant.vertex(timeEventState))));
        timerGuards.add(timerSet.contains(timeEvent) ? timerGuard : TExpression.neg(timerGuard));
      }
      return timerGuards.stream().reduce(TExpression.trueConst(), TExpression::and);
    }

    public List<TUpdate> timersReset(Set<UEvent> timerSet) {
      var timerTree = smST.getTimerTree();
      var emptyTimerNumberSet = timerTree.getTimerNumbersSet();
      for (var timeEvent : timerSet)
        emptyTimerNumberSet.remove(timerTree.getTimerNumber(timeEvent));
      return emptyTimerNumberSet.stream().map(timerNumber -> smST.timer(timerNumber).reset()).collect(toList());
    }
  }

  private Queue(StateMachine.SymbolTable smST) {
    this.quST = smST.getQueueSymbolTable();
  }

  private static TFunction getEnqueueFunctionDeclaration(TVariable.Access queue, TExpression capacity, SymbolTable quST) {
    var moST = quST.getModelSymbolTable();
    var empty = moST.empty();
    var runningVar = new TVariable("i"); var running = runningVar.access();

    var statements = new ArrayList<TStatement>();
    statements.add(TLoop.loop(running.assign(0),
                              TExpression.relational(running, TOperator.LT, capacity),
                              TBlock.block(TConditional.ifthen(TExpression.eq(queue.access(running).access(moST.ID), empty),
                                                               TBlock.block(queue.access(running).assign(moST.comm()),
                                                                            TFunction.ret()))),
                              running.inc()));

    return TFunction.decl(queue.expression() + "_enqueue", new ArrayList<>(), TBlock.block(Arrays.asList(runningVar), statements));
  }

  private static TFunction getPopFunctionDeclaration(TVariable.Access queue, TExpression capacity, SymbolTable quST) {
    var moST = quST.getModelSymbolTable();
    var smST = quST.getStateMachineSymbolTable();
    var empty = moST.empty();
    var emptyMessage = moST.emptyMessage();
    var runningVar = new TVariable("i"); var running = runningVar.access();

    var statements = new ArrayList<TStatement>();
    statements.add(smST.current().assign(queue.access(0)));
    statements.add(TLoop.loop(running.assign(0),
                              TExpression.relational(running, TOperator.LT, TExpression.arithmetical(capacity, TOperator.SUB, 1)),
                              TBlock.block(queue.access(running).assign(queue.access(TExpression.arithmetical(running, TOperator.ADD, 1))),
                                           TConditional.ifthen(TExpression.eq(queue.access(TExpression.arithmetical(running, TOperator.ADD, 1)).access(moST.ID), empty),
                                                               TBlock.block(TFunction.ret()))),
                              running.inc()));
    statements.add(queue.access(TExpression.arithmetical(capacity, TOperator.SUB, 1)).assign(emptyMessage));

    return TFunction.decl(queue.expression() + "_pop", new ArrayList<>(), TBlock.block(Arrays.asList(runningVar), statements));
  }

  private static TFunction getInternalsPushFunctionDeclaration(SymbolTable quST) {
    var internals = quST.internals();
    var internalsCapacity = quST.internalsCapacity();
    var idVar = new TVariable("id"); var id = idVar.access();
    var runningVar = new TVariable("i"); var running = runningVar.access();

    var statements = new ArrayList<TStatement>();
    statements.add(TLoop.loop(running.assign(TExpression.arithmetical(internalsCapacity, TOperator.SUB, 1)),
                              TExpression.relational(running, TOperator.GT, TExpression.intConst(0)),
                              TBlock.block(internals.access(running).assign(internals.access(TExpression.arithmetical(running, TOperator.SUB, 1)))),
                              running.dec()));
    statements.add(internals.access(0).assign(id));

    return TFunction.decl("internals_push", Arrays.asList(idVar), TBlock.block(Arrays.asList(runningVar), statements));
  }

  private static TFunction getInternalsRemoveFunctionDeclaration(SymbolTable quST) {
    var empty = quST.moST.empty();
    var internals = quST.internals();
    var internalsCapacity = quST.internalsCapacity();
    var idVar = new TVariable("id"); var id = idVar.access();
    var toVar = new TVariable("i"); var to = toVar.access();
    var fromVar = new TVariable("j"); var from = fromVar.access();

    var statements = new ArrayList<TStatement>();
    statements.add(to.assign(0));
    statements.add(from.assign(1));
    statements.add(TLoop.loop(TExpression.relational(to, TOperator.LT, internalsCapacity),
                              TBlock.block(TConditional.ifthenelse(TExpression.eq(internals.access(to), id),
                                                                   TBlock.block(TConditional.ifthenelse(TExpression.relational(from, TOperator.LT, internalsCapacity),
                                                                                                        TBlock.block(internals.access(to).assign(internals.access(from)),
                                                                                                                     from.inc()),
                                                                                                        TBlock.block(internals.access(to).assign(empty)))),
                                                                   TBlock.block(to.inc())))));

    return TFunction.decl("internals_remove", Arrays.asList(idVar), TBlock.block(Arrays.asList(toVar, fromVar), statements));
  }

  private static TFunction getInternalsPopFunctionDeclaration(SymbolTable quST) {
    var moST = quST.getModelSymbolTable();
    var smST = quST.getStateMachineSymbolTable();
    var empty = moST.empty();
    var current = smST.current();
    var internals = quST.internals();
    var internalsCapacity = quST.internalsCapacity();
    var runningVar = new TVariable("i"); var running = runningVar.access();

    var statements = new ArrayList<TStatement>();
    statements.add(current.assign(moST.emptyMessage()));
    statements.add(current.access(moST.ID).assign(TExpression.arithmetical(internals.access(0), TOperator.ADD, moST.getMaxExternalEventsNumber())));
    statements.add(TLoop.loop(running.assign(0),
                              TExpression.relational(running, TOperator.LT, TExpression.arithmetical(internalsCapacity, TOperator.SUB, 1)),
                              TBlock.block(internals.access(running).assign(internals.access(TExpression.arithmetical(running, TOperator.ADD, 1))),
                                           TConditional.ifthen(TExpression.eq(internals.access(TExpression.arithmetical(running, TOperator.ADD, 1)), empty),
                                                               TBlock.block(TFunction.ret()))),
                              running.inc()));
    statements.add(internals.access(TExpression.arithmetical(internalsCapacity, TOperator.SUB, 1)).assign(empty));

    return TFunction.decl("internals_pop", new ArrayList<>(), TBlock.block(Arrays.asList(runningVar), statements));
  }

  /**
   * Compute an annotation net for fetching an event from the event queue.
   *
   * TODO (AK221204) Include cycle-based translation.
   *
   * It can not be expected that some event is present when an event
   * is looked for.  However, events are fetched eagerly, as the
   * communication channel is declared to be `urgent'.
   * 
   * The algorithm is the following: First, we look whether there is an
   * internal event to be handled; these have to be prioritised.  Internal
   * events are either completion or time events.  If there is an internal
   * event to be handled it is made the active stimulus.
   * 
   * If there is no internal event, we move to a state where time may pass.
   * Passage of time has either to be restricted if timers may be running; or
   * it is unlimited; which timers are currently active can be inferred from
   * the current state configuration. In any case, we look for external events
   * coming from the buffer.  If there is an external event to be handled,
   * we make it the active stimulus.  Furthermore, if there are wait states,
   * a completion event for such a wait state can be generated and made the
   * active stimulus.
   * 
   * The computation of the different wait states, where time can pass,
   * relies on the {@link UTimerTree TimerTree} for the current state machine.
   * There, we compute which timers can be active simultaneously, i.e. for
   * which time events the state machine may be waiting for at a given time
   * instance.
   * 
   * @param waitStates set of wait states
   * @param quST symbol table for the current state machine
   * @return an annotation net for fetch
   */
  public static TNet fetch(Set<UState> waitStates, StateMachine.SymbolTable smST) {
    var quST = smST.getQueueSymbolTable();
    var timerSets = smST.getTimerTree().getTimerSets();
    // TODO (AK211008) Uncomment for merging fetching and waiting for acknowledgments
    var mayWaitForAcknowledgements = false; // !smST.getStateMachine().getCalls().isEmpty();
    var waitingForAck = mayWaitForAcknowledgements ? smST.waitForAck() : TExpression.falseConst();

    var fetchImmediateState = new TState(smST.uniqueIdentifier("FetchImmediate"));
    fetchImmediateState.setUrgent(true);

    var preFetchWaitingState = new TState(smST.uniqueIdentifier("PreFetchWaiting"), true);
    var fetchedState = new TState(smST.uniqueIdentifier("Fetched"), true);

    var fetch = new TSubTemplate(fetchImmediateState, fetchedState);
    fetch.addSubProcess(fetchImmediate(TExpression.neg(waitingForAck), fetchImmediateState, fetchedState, preFetchWaitingState, smST));

    for (var timerSet : timerSets) {
      var timersState = quST.timersState("Fetch", timerSet);
      fetch.addTransition(preFetchWaitingState, timersState,
                          quST.timersActive(timerSet), null, quST.timersReset(timerSet));
      fetch.addSubProcess(fetchWaiting(TExpression.neg(waitingForAck), timersState, fetchedState, timerSet, waitStates, smST));
      if (mayWaitForAcknowledgements)
        fetch.addSubProcess(waitForAcknowledgement(waitingForAck, timersState, preFetchWaitingState, fetchedState, timerSet, smST));
    }
    
    return TNet.sub(fetch);   
  }
    
  /**
   * Compute annotation net for waiting for an acknowledgement.
   * 
   * While waiting for an acknowledgement arbitrary timers can run down.
   * The according time events have to be put into the internal queue and
   * the time events have to be marked as timed out.
   * 
   * @param smST state machine symbol table
   * @return an annotation net for waiting for an acknowledgement
   */
  public static TNet waitForAcknowledgement(StateMachine.SymbolTable smST) {
    // TODO (AK211008) Uncomment for merging fetching and waiting for acknowledgments
    // return TNet.trans(smST.waitForAck().assign(true)),
    var quST = smST.getQueueSymbolTable();
    var timerSets = smST.getTimerTree().getTimerSets();
    
    var preAckState = new TState(smST.uniqueIdentifier("PreAck"));
    preAckState.setCommitted(true);

    var ackedState = new TState(smST.uniqueIdentifier("Acked"));
    ackedState.setCommitted(true);

    var waitForAck = new TSubTemplate(preAckState, ackedState);

    for (var timerSet : timerSets) {
      var timersState = quST.timersState(smST.uniqueIdentifier("Ack"), timerSet);
      waitForAck.addTransition(preAckState, timersState,
                               quST.timersActive(timerSet), null, quST.timersReset(timerSet));
      waitForAck.addSubProcess(waitForAcknowledgement(TExpression.trueConst(), timersState, preAckState, ackedState, timerSet, smST));
    }
    
    return TNet.sub(waitForAck);   
  }

  private static TSubTemplate fetchWaiting(TExpression notWaitingForAck, TState from, TState to, Set<UEvent> timerSet, Set<UState> waitStates, StateMachine.SymbolTable smST) {
    var moST = smST.getModelSymbolTable();

    var fetch = new TSubTemplate(from, to);
    // TODO (AK060529) Add receiving notification for observer

    // Leaving a timer state via a time out
    for (var timeEvent : timerSet)
      fetch.addTransition(from, to,
                          Arrays.asList(notWaitingForAck, TClockCondition.geq(smST.timer(timeEvent), smST.timerLow(timeEvent))), null,
                          Arrays.asList(smST.current().assign(moST.emptyMessage()),
                                        smST.current().access(moST.ID).assign(smST.id(timeEvent)),
                                        smST.timer(timeEvent).reset(),
                                        smST.timedOut(timeEvent).assign(true)));
 
    // "Fetch" a completion event for a wait state (i.e., create one)
    for (UState waitState : waitStates)
      fetch.addTransition(from, to,
                          TExpression.and(notWaitingForAck,
                                          TExpression.eq(smST.access(waitState), smST.id(waitState)),
                                          smST.getProperties().doDoubleChecks() ? smST.completed(waitState) : TExpression.trueConst()), null,
                          Arrays.asList(smST.current().assign(moST.emptyMessage()), smST.current().access(moST.ID).assign(smST.id(UEvent.completion(waitState)))));
    
    fetch.addTransition(from, to,
                        notWaitingForAck,
                        smST.getToStateMachine().receive(),
                        Arrays.asList(smST.current().assign(moST.comm()), moST.comm().assign(moST.emptyMessage())));

    return fetch;
  }

  private static TSubTemplate fetchImmediate(TExpression notWaitingForAck, TState from, TState done, TState wait, StateMachine.SymbolTable smST) {
    var quST = smST.getQueueSymbolTable();
    var hasDeferrableEvents = !smST.getStateMachine().getDeferrableEvents().isEmpty();

    var fetch = new TSubTemplate(from, done);
    // TODO (AK060529) Add receiving notification for observer

    // Fetch an event from the internal event queue
    fetch.addTransition(from, done,
                        TExpression.and(notWaitingForAck, quST.internalsEnabled()), null,
                        Arrays.asList(quST.internalsPop()));

    // Fetch an event from the deferred event queue
    if (hasDeferrableEvents)
      fetch.addTransition(from, done,
                          TExpression.and(notWaitingForAck, TExpression.neg(quST.internalsEnabled()), quST.deferredsEnabled()), null,
                          Arrays.asList(quST.deferredsPop()));

    // Fetch an event from the external event queue
    fetch.addTransition(from, done,
                        TExpression.and(notWaitingForAck, TExpression.neg(quST.internalsEnabled()), TExpression.neg(quST.deferredsEnabled()), quST.externalsEnabled()), null,
                        Arrays.asList(quST.externalsPop()));

    // Nothing to fetch, go to waiting state
    fetch.addTransition(from, wait,
                        TExpression.and(notWaitingForAck, TExpression.neg(quST.internalsEnabled()), TExpression.neg(quST.deferredsEnabled()), TExpression.neg(quST.externalsEnabled())), null,
                        Arrays.asList());

    return fetch;
  }

  private static TSubTemplate waitForAcknowledgement(TExpression waiting, TState from, TState restart, TState to, Set<UEvent> timerSet, StateMachine.SymbolTable smST) {
    var quST = smST.getQueueSymbolTable();

    var waitForAck = new TSubTemplate(from, to);

    // Enqueue a time event
    for (var timeEvent : timerSet) {
      waitForAck.addTransition(from, restart,
                               Arrays.asList(waiting, TClockCondition.geq(smST.timer(timeEvent), smST.timerLow(timeEvent)), TExpression.neg(quST.internalsFull())), null,
                               Arrays.asList(quST.internalsPush(timeEvent),
                                             smST.timer(timeEvent).reset(),
                                             smST.timedOut(timeEvent).assign(true)));
      if (!smST.getProperties().isBlocking()) {
        waitForAck.addTransition(from, quST.internalsOverflow(),
                                 Arrays.asList(waiting, TClockCondition.geq(smST.timer(timeEvent), smST.timerLow(timeEvent)), quST.internalsFull()), null,
                                 Arrays.asList());
      }
    }

    // Enqueue an external event
    waitForAck.addTransition(from, from,
                             TExpression.and(waiting, TExpression.neg(quST.externalsFull())),
                             smST.getToStateMachine().receive(),
                             Arrays.asList(quST.externalsEnqueue()));

    // Overflow by an external event
    if (!smST.getProperties().isBlocking()) {
      waitForAck.addTransition(from, quST.externalsOverflow(),
                               TExpression.and(waiting, quST.externalsFull()),
                               smST.getToStateMachine().receive(),
                               Arrays.asList());
    }

    // Acknowledgement arrives
    waitForAck.addTransition(from, to,
                             waiting,
                             smST.getAck().receive(),
    // TODO (AK211008) Uncomment for merging fetching and waiting for acknowledgments
                             Arrays.asList()); // smST.waitForAck().assign(TExpression.falseConst())));

    return waitForAck;
  }

  public static void reconstruct(UObjectState uObjectState, TSystemState systemState, ModelSymbolTable moST) {
    var uStateMachine = uObjectState.getObject().getC1ass().getStateMachine();
    if (uStateMachine == null)
      return;

    new Queue(moST.getStateMachineSymbolTable(uStateMachine)).reconstruct(uObjectState, systemState);
  }

  private void reconstruct(UObjectState uObjectState, TSystemState systemState) {
    var moST = quST.getModelSymbolTable();
    var uObject = uObjectState.getObject();

    var objectProcess = this.quST.getStateMachineSymbolTable().getProcess(uObject);
    var objectProcessState = systemState.getProcessState(objectProcess);
    if (objectProcessState == null) {
      util.Message.debug().info("No process state for object `" + uObject.getName() + "' found.");
      return;
    }

    var uQueueState = uObjectState.getQueueState();

    var objectProcessStateState = objectProcessState.getState();
    if (objectProcessStateState != null && !quST.getProperties().isBlocking() && objectProcessStateState.equals(quST.externalsOverflow()))
      uQueueState.setExternalOverflown(true);
    else {
      var externals = quST.externals();
      for (int i = 0; i < quST.externalsCapacity().getIntValue(); i++) {
        var uMessage = reconstruct(uObject, objectProcessState, externals.access(i), moST);
        if (uMessage != null)
          uQueueState.addExternalMessage(uMessage);
      }
    }

    var smST = quST.getStateMachineSymbolTable();

    if (objectProcessStateState != null && !quST.getProperties().isBlocking() && objectProcessStateState.equals(quST.internalsOverflow())) {
      uQueueState.setInternalOverflown(true);
    }
    else {
      var internals = quST.internals();
      for (int i = 0; i < quST.internalsCapacity().getIntValue(); i++) {
        var uEvent = smST.getEvent(objectProcessState.getValue(internals.access(i)));
        if (uEvent != null)
          uQueueState.addInternalEvent(uEvent);
      }    
    }

    if (objectProcessStateState != null && !quST.getProperties().isBlocking() && objectProcessStateState.equals(quST.deferredsOverflow())) {
      uQueueState.setDeferredOverflown(true);
    }
    else {
      var deferreds = quST.deferreds();
      for (int i = 0; i < quST.deferredsCapacity().getIntValue(); i++) {
        var uMessage = reconstruct(uObject, objectProcessState, deferreds.access(i), moST);
        if (uMessage != null)
          uQueueState.addDeferredMessage(uMessage);
      }
    }
  }

  public static @Nullable UMessage reconstruct(UObject uReceiver, TProcessState processState, TVariable.Access message, ModelSymbolTable moST) {
    if (processState.getValue(message.access(moST.ID)) == 0)
      return null;
    var uSender = moST.getObject(processState.getValue(message.access(moST.SENDER)));
    var uBehavioural = moST.getBehavioural(uReceiver, processState.getValue(message.access(moST.ID)));
    // If we can't find a behavioural this should stimulus should be an acknowledgement
    if (uBehavioural == null)
      return null;
    var uArguments = new ArrayList<UExpression>();
    for (int j = 0; j < uBehavioural.getParameters().size(); j++)
      uArguments.add(UMLTranslator.reconstruct(uBehavioural.getParameters().get(j).getType(), processState.getValue(message.access(moST.PARAMS).access(j)), moST));
    return new UMessage("", uSender, uBehavioural, uArguments, uReceiver);
  }
}
