package translation.uml2uppaal;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UObject;
import uml.UOperation;
import uml.UStatual;
import uml.UType;
import uml.statemachine.UEvent;
import uppaal.TConstant;
import uppaal.TDataType;
import uppaal.TExpression;
import uppaal.TField;
import uppaal.TFunction;
import uppaal.TOpaque;
import uppaal.TProcess;
import uppaal.TTemplate;
import uppaal.TVariable;
import util.Lists;
import util.Strings;


/**
 * State machine symbol table for translating (simple) UML models into
 * UPPAAL.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class ClassSymbolTable extends SymbolTable.Template {
  private ModelSymbolTable moST;
  private UClass uClass;
  private List<UOperation> sortedOperations;

  /**
   * Create a class symbol table for a class with a template and
   * a model symbol table.
   * 
   * @param uClass a class
   * @param template a template to be filled
   * @param moST system symbol table
   * @return a class symbol table
   */
  public ClassSymbolTable(UClass uClass, TTemplate template, ModelSymbolTable moST) {
    super(moST.getModel(), template);
    this.moST = moST;
    this.uClass = uClass;
    this.sortedOperations = Lists.sort(uClass.getOperations(), UOperation::getName);
  }

  /**
   * Create a class symbol table for a class with a model symbol table.
   * 
   * @param uClass a class
   * @param moST model symbol table
   * @return a class symbol table
   */
  public ClassSymbolTable(UClass uClass, ModelSymbolTable moST) {
    this(uClass, moST.registerTemplate("cls" + Strings.capitalise(uClass.getName())), moST);
  }

  @Override
  public control.Properties getProperties() {
    return this.moST.getProperties();
  }

  /**
   * @return this class symbol table's underlying model symbol table
   */
  public ModelSymbolTable getModelSymbolTable() {
    return this.moST;
  }

  @Override
  public TConstant.Access empty() {
    return this.moST.empty();
  }

  @Override
  public TConstant.Access self() {
    return this.registerParameter("this", TDataType.intType(), object -> this.moST.id((UObject)object)).access();
  }

  /**
   * @return UPPAAL process for {@code uObject}
   */
  public TProcess getProcess(UObject uObject) {
    assert uObject.getC1ass().equals(this.uClass) : "not applicable";
    return this.moST.registerProcess("obj" + Strings.capitalise(uObject.getName() + "Proc"), this.getTemplate(), parameter -> this.getArgument(parameter, uObject));
  }

  /**
   * @return UPPAAL constant for {@code uBehavioural}
   */
  public TConstant.Access id(UBehavioural uBehavioural) {
    assert uBehavioural.getOwner().equals(this.uClass) : "not applicable";
    return uBehavioural.cases(uOperation -> { // var uCall = UEvent.call(uOperation);
                                              return this.moST.registerConstant(key(uOperation, uOperation.getName()), // key(uCall, uCall.getName()),
                                                                                () -> TDataType.intType(),
                                                                                () -> TExpression.intConst(this.sortedOperations.indexOf(uOperation)+this.getModel().getSignals().size()+1)).access(); },
                              uReception -> this.moST.id(uReception.getSignal()));
  }

  public TFunction.Call call(UBehavioural uBehavioural, List<TExpression> tArguments) {
    var name = "cls" + Strings.capitalise(uBehavioural.getOwner().getName()) + "_" + uBehavioural.getName();
    return this.moST.registerFunction(name, () -> { return TFunction.decl(name,
                                                                          uBehavioural.getParameters().stream().map(p -> new TVariable(this.getType(p.getType()), p.getName())).collect(toList()),
                                                                          new TOpaque(uBehavioural.getMethodCode("UppAal"))); }).call(tArguments);
  }

  /**
   * @return UML behavioural represented by {@code n}, or {@code null} if none can be found
   */
  public @Nullable UBehavioural getBehavioural(int n) {
    var uSignal = this.moST.getSignal(n);
    if (uSignal != null)
      return this.uClass.getReception(uSignal);
    n = n-this.getModel().getSignals().size()-1;
    var operations = this.uClass.getOperations();
    if (0 <= n && n < operations.size()) 
      return this.sortedOperations.get(n);
    return null;
  }

  /**
   * @return access to UPPAAL constant for behavioural {@code uEvent}
   */
  public TConstant.Access id(UEvent uEvent) {
    var uBehavioural = this.uClass.getBehavioural(uEvent);
    assert uBehavioural != null : "not applicable";
    return this.id(uBehavioural);
  }

  /**
   * @return UPPAAL type for {@code uType}
   */
  private TDataType getType(UType uType) {
    var intType = uType.isClass() ? TDataType.range(this.moST.getObjectsNumber()) : TDataType.range(this.getProperties().getIntMin(), this.getProperties().getIntMax());
    var size = TExpression.intConst(uType.isArray() ? uType.getSize() : 1);
    return uType.isArray() ? TDataType.array(intType, size) : intType;
  }

  /**
   * Determine the value of an object's attribute.
   *
   * @param object a UML object
   * @param attribute an attribute of the object's classifier
   * @param an UPPAAL expression
   */
  private TExpression getValue(UObject uObject, UAttribute uAttribute) {
    // TODO (AK130527) Does UPPAAL support literal arrays?
    // Find the slot for the given attribute
    var uSlot = uObject.getSlot(uAttribute);
    // If there is no matching slot, the initial values of the attribute are taken
    var uValues = uSlot == null ? uAttribute.getInitialValues() : uSlot.getValues();
    var uContext = uSlot == null ? uAttribute.getInitialValuesContext() : uSlot.getValuesContext();
    var values = uValues.stream().map(uValue -> UMLTranslator.translateValue(uValue, uContext, this)).collect(toList());
    return uAttribute.isArray() ? TExpression.array(values) : values.get(0);
  }

  /**
   * @return UPPAAL field for {@code uAttribute}
   */
  private TField getField(UAttribute uAttribute) {
    Supplier<TDataType> typeSupplier = () -> this.getType(uAttribute.getType());
    Supplier<TExpression> initialValueSupplier = () -> { var initialValues = uAttribute.getInitialValues().stream().map(uValue -> UMLTranslator.translateValue(uValue, uAttribute.getInitialValuesContext(), this)).collect(toList());
                                                         return uAttribute.isArray() ? TExpression.array(initialValues) : initialValues.get(0); };
    if (uAttribute.isStatic()) {
      if (uAttribute.isConstant())
        return this.moST.registerConstant(key(uAttribute, "cls" + Strings.capitalise(uAttribute.getOwner().getName()) + "_" + uAttribute.getName()),
                                          typeSupplier,
                                          initialValueSupplier);
      return this.moST.registerVariable(key(uAttribute, "cls" + Strings.capitalise(uAttribute.getOwner().getName()) + "_" + uAttribute.getName()),
                                        typeSupplier,
                                        () -> { if (!uAttribute.hasInitialValues()) return null;
                                                return initialValueSupplier.get(); });
    }
    assert uAttribute.getOwner().equals(this.uClass) : "not applicable for " + uAttribute.declaration();
    if (uAttribute.isConstant())
      return this.registerConstant(key(uAttribute, uAttribute.getName()),
                                   typeSupplier, initialValueSupplier);
                                   // () -> TExpression.array(uAttribute.getInitialValues().stream().map(v -> UMLTranslator.translateValue(v, uAttribute.getInitialValuesContext(), this.moST)).collect(toList())));
    return this.registerVariable(key(uAttribute, uAttribute.getName()), () -> this.getType(uAttribute.getType()), () -> {
      return this.registerParameter("attr" + Strings.capitalise(uAttribute.getName()), this.getType(uAttribute.getType()), object -> this.getValue((UObject)object, uAttribute)).access();
    });
  }

  @Override
  public TField.Access access(UStatual uStatual) {
    return uStatual.cases(uAttribute -> this.getField(uAttribute).access(),
                          uParameter -> this.empty(),
                          uObject -> moST.id(uObject));
  }
}
