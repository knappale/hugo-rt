package translation.uml2uppaal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UBehavioural;
import uml.UClass;
import uml.UCollaboration;
import uml.UObject;
import uml.USignal;
import uml.UStatual;
import uml.interaction.UInteraction;
import uml.statemachine.UStateMachine;
import uppaal.TChannel;
import uppaal.TConstant;
import uppaal.TDataType;
import uppaal.TExpression;
import uppaal.TField;
import uppaal.TProcess;
import uppaal.TSystem;
import uppaal.TVariable;
import util.Lists;
import util.Pair;
import util.Strings;


/**
 * System wide symbol table for translating UML into UPPAAL.
 *
 * The system symbol table contains and provides data for the
 * <UL>
 * <LI>network
 * <LI>queues
 * <LI>observer
 * <LI>state machines
 * </UL>
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class ModelSymbolTable extends SymbolTable.System {
  private static Map<Object, ModelSymbolTable> instances = new HashMap<>();

  private UCollaboration collaboration;
  private @Nullable UInteraction interaction = null;

  private List<USignal> sortedSignals;
  private List<UObject> sortedObjects;

  private Map<UClass, ClassSymbolTable> classSymbolTables = new HashMap<>();
  private Map<UStateMachine, StateMachine.SymbolTable> stateMachineSymbolTables = new HashMap<>();
  private Buffer.@Nullable SymbolTable bufferSymbolTable;
  private Interaction.@Nullable SymbolTable observerSymbolTable;

  public final String SENDER = "sender";
  public final String ID = "id";
  public final String PARAMS = "params";
  public final String RECEIVER = "receiver";
 
  /**
   * Create a system symbol table containing common information for
   * translating UML into UPPAAL.
   *
   * @param collaboration a UML collaboration
   * @param interaction a UML interaction
   * @return a model symbol table for underlying collaboration
   */
  private ModelSymbolTable(UCollaboration collaboration, @Nullable UInteraction interaction) {
    super(collaboration.getModel(), new TSystem());
    this.collaboration = collaboration;
    this.interaction = interaction;
    this.sortedSignals = Lists.sort(this.getModel().getSignals(), USignal::getName);
    this.sortedObjects = Lists.sort(new ArrayList<>(collaboration.getObjects()), UObject::getName);
  }

  /**
   * Create a system symbol table containing common information for
   * translating UML into UPPAAL.
   *
   * @param collaboration a UML collaboration
   * @param interaction a UML interaction
   * @return a model symbol table for underlying collaboration
   */
  public static ModelSymbolTable create(UCollaboration collaboration, @Nullable UInteraction interaction) {
    List<Object> key = new ArrayList<>();
    key.add(collaboration);
    if (interaction != null)
      key.add(interaction);
    ModelSymbolTable instance = instances.get(key);
    if (instance == null) {
      instance = new ModelSymbolTable(collaboration, interaction);
      instances.put(key, instance);
    }
    return instance;
  }

  /**
   * Create a system symbol table containing common information for
   * translating UML into UPPAAL based on a collaboration.
   *
   * @param collaboration a UML collaboration
   * @return a model symbol table for underlying collaboration
   */
  public static ModelSymbolTable collaboration(UCollaboration collaboration) {
    return ModelSymbolTable.create(collaboration, null);
  }

  /**
   * Create a system symbol table containing common information for
   * translating UML into UPPAAL based on an interaction.
   *
   * @param interaction a UML interaction
   * @return a model symbol table for underlying interaction
   */
  public static ModelSymbolTable interaction(UInteraction interaction) {
    return ModelSymbolTable.create(interaction.getCollaboration(), interaction);
  }

  /**
   * @return model symbol table's underlying collaboration
   */
  public UCollaboration getCollaboration() {
    return this.collaboration;
  }

  /**
   * @return model symbol table's underlying interaction
   * (this may be {@code null}, if there is no underlying interaction)
   */
  public @Nullable UInteraction getInteraction() {
    return this.interaction;
  }

  @Override
  public control.Properties getProperties() {
    return this.collaboration.getProperties();
  }

  /**
   * @return whether symbol table's underlying model has an observer
   */
  public boolean hasObserver() {
    return this.interaction != null;
  }

  /**
   * Determine the maximum number assigned to any of the external events
   * accepted by any of the underlying classes.
   * 
   * An event is called external if it is handled by the network.
   * 
   * @return maximum number assigned to any of the external events
   *         accepted by any of the underlying classes
   */
  public int getMaxExternalEventsNumber() {
    return 1 +
           this.getModel().getSignals().size() +
           this.getModel().getClasses().stream().map(uClass -> uClass.getOperations().size()).reduce(0, (m1, m2) -> Integer.max(m1, m2));
  }

  /**
   * @return maximum number assigned to any of the events accepted by any of the underlying classes
   */
  public int getMaxEventsNumber() {
    return this.getMaxExternalEventsNumber() + this.getModel().getClasses().stream().map(uClass -> uClass.getEvents().size()).reduce(0, (m1, m2) -> Integer.max(m1, m2));
  }

  /**
   * @return number of objects in the underlying (UML) model
   */
  public int getObjectsNumber() {
    return this.getCollaboration().getObjects().size();
  }

  /**
   * @return system's observer symbol table
   */
  public Interaction.SymbolTable getObserverSymbolTable() {
    var observerSymbolTable = this.observerSymbolTable;
    if (observerSymbolTable != null)
      return observerSymbolTable;
    observerSymbolTable = new Interaction.SymbolTable(this);
    this.observerSymbolTable = observerSymbolTable;
    return observerSymbolTable;
  }

  /**
   * @return symbol table for event buffer
   */
  public Buffer.SymbolTable getBufferSymbolTable() {
    var bufferSymbolTable = this.bufferSymbolTable;
    if (bufferSymbolTable != null)
      return bufferSymbolTable;
    bufferSymbolTable = new Buffer.SymbolTable(this);
    this.bufferSymbolTable = bufferSymbolTable;
    return bufferSymbolTable;
  }

  /**
   * @return symbol table for {@code uClass}
   */
  public ClassSymbolTable getClassSymbolTable(UClass uClass) {
    var uStateMachine = uClass.getStateMachine();
    if (uStateMachine != null)
      return this.getStateMachineSymbolTable(uStateMachine);
    var clST = this.classSymbolTables.get(uClass);
    if (clST != null)
      return clST;
    clST = new ClassSymbolTable(uClass, this);
    this.classSymbolTables.put(uClass, clST);
    return clST;
  }

  /**
   * @return symbol table for {@code uStateMachine}
   */
  public StateMachine.SymbolTable getStateMachineSymbolTable(UStateMachine uStateMachine) {
    var stateMachineSymbolTable = this.stateMachineSymbolTables.get(uStateMachine);
    if (stateMachineSymbolTable != null)
      return stateMachineSymbolTable;
    stateMachineSymbolTable = new StateMachine.SymbolTable(uStateMachine, this);
    this.stateMachineSymbolTables.put(uStateMachine, stateMachineSymbolTable);
    return stateMachineSymbolTable;
  }

  /**
   * @return access to empty constant
   */
  public TConstant.Access empty() {
    return this.registerConstant(key("empty"), () -> TExpression.intConst(0)).access();
  }

  /**
   * @return access to empty message constant
   */
  public TConstant.Access emptyMessage() {
    return this.registerConstant(key("emptyMessage"), () -> this.getMessageType(), () -> this.getMessageType().getDefaultValue()).access();
  }

  /**
   * @return access to constant representing {@code uObject}
   */
  public TConstant.Access id(UObject uObject) {
    return this.registerConstant(key(uObject, "obj" + Strings.capitalise(uObject.getName())),
                                 () -> TDataType.intType(),
                                 () -> TExpression.intConst(this.sortedObjects.indexOf(uObject)+1)).access();
  }

  /**
   * @return UML object represented by integer {@code n}; or {@code null} if no such object can be found
   */
  public @Nullable UObject getObject(int n) {
    n = n-1;
    if (0 <= n && n < this.sortedObjects.size())
      return this.sortedObjects.get(n);
    return null;
  }

  public TProcess getProcess(UObject uObject) {
    return this.getClassSymbolTable(uObject.getC1ass()).getProcess(uObject);
  }

  /**
   * @return access to constant for {@code uSignal}
   */
  public TConstant.Access id(USignal uSignal) {
    // var uSend = UEvent.signal(uSignal);
    return this.registerConstant(key(uSignal, uSignal.getName()), // key(uSend, uSend.getName()),
                                 () -> TDataType.intType(),
                                 () -> TExpression.intConst(this.sortedSignals.indexOf(uSignal)+1)).access();
  }

  /**
   * @return the signal denoted by {@code n}, if any
   */
  public @Nullable USignal getSignal(int n) {
    n = n-1;
    if (0 <= n && n < this.sortedSignals.size())
      return this.sortedSignals.get(n);
    return null;
  }

  /**
   * @return UPPAAL constant for {@code uBehavioural}
   */
  public TConstant.Access id(UBehavioural uBehavioural) {
    return this.getClassSymbolTable(uBehavioural.getOwner()).id(uBehavioural);
  }

  /**
   * Determine which UML behavioural is represented by an integer in a UML object.
   *
   * @param uObject a UML object
   * @param n an integer
   * @return UML behavioural represented by {@code n} in UML object {@code uObject},
   *         or {@code null} if no such behavioural can be found
   */
  public @Nullable UBehavioural getBehavioural(UObject uObject, int n) {
    return this.getClassSymbolTable(uObject.getC1ass()).getBehavioural(n);
  }

  @Override
  public TField.Access access(UStatual uStatual) {
    return uStatual.cases(uAttribute -> this.getClassSymbolTable(uAttribute.getOwner()).access(uAttribute),
                          uParameter -> this.empty(),
                          uObject -> this.id(uObject));
  }

  /**
   * @return type of communication elements
   */
  TDataType getMessageType() {
    return this.registerTypeDef("Message", () -> { var fields = new ArrayList<Pair<String, TDataType>>();
                                                   fields.add(new Pair<>(SENDER, TDataType.range(this.getObjectsNumber())));
                                                   fields.add(new Pair<>(ID, TDataType.range(this.getMaxEventsNumber())));
                                                   var maxParameters = this.getModel().getMaxParameters();
                                                   if (maxParameters > 0)
                                                     fields.add(new Pair<>(PARAMS, TDataType.array(TDataType.intType(), maxParameters)));
                                                   fields.add(new Pair<>(RECEIVER, TDataType.range(this.getObjectsNumber())));
                                                   return TDataType.struct(fields); });
  }

  /**
   * @return access to communication variable for data exchange
   */
  public TVariable.Access comm() {
    return this.registerVariable(key("comm"), () -> this.getMessageType(), () -> this.emptyMessage()).access();
  }

  public TChannel getAcknowledgementChannels() {
    return this.registerChannel("ack", this.getProperties().getMaxDelay() >= 0, TExpression.intConst(this.getObjectsNumber()+1));
  }

  /**
   * @return access to channel for {@code receiver}'s queue buffer
   */
  public TChannel.Access toQueueBuffer(TExpression receiver) {
    return this.registerChannel("toQueueBuffer", this.getProperties().getMaxDelay() >= 0, TExpression.intConst(this.getObjectsNumber()+1)).access(receiver);
  }

  /**
   * @return access to channel for {@code receiver}'s state machine
   */
  public TChannel.Access toStateMachine(TExpression receiver) {
    return this.registerChannel("toStateMachine", this.getProperties().getMaxDelay() >= 0, TExpression.intConst(this.getObjectsNumber()+1)).access(receiver);
  }
}
