package translation.uml2uppaal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UModel;
import uml.UStatual;
import uppaal.TArgument;
import uppaal.TChannel;
import uppaal.TClock;
import uppaal.TConstant;
import uppaal.TDataType;
import uppaal.TExpression;
import uppaal.TField;
import uppaal.TFrame;
import uppaal.TFunction;
import uppaal.TKeywords;
import uppaal.TParameter;
import uppaal.TProcess;
import uppaal.TState;
import uppaal.TSystem;
import uppaal.TTemplate;
import uppaal.TVariable;
import util.Pair;


/**
 * Symbol table for translating (simple) UML models into UPPAAL.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class SymbolTable {
  private UModel model;
  private Map<Object, TVariable> variables = new HashMap<>();
  private Map<Object, TConstant> constants = new HashMap<>();
  private Map<Object, TClock> clocks = new HashMap<>();
  private Map<Object, TFunction> functions = new HashMap<>();
  private Set<String> identifiers = new HashSet<>();

  public SymbolTable(UModel model) {
    this.model = model;
    this.identifiers.addAll(TKeywords.getKeywords());
  }

  /**
   * @return symbol table's underlying model
   */
  public UModel getModel() {
    return this.model;
  }

  /**
   * @return symbol table's underlying model's properties
   */
  public control.Properties getProperties() {
    return this.model.getProperties();
  }

  /**
   * @return symbol table's underlying declaration frame
   */
  public abstract TFrame getFrame();

  /**
   * @return key from {@code name}
   */
  public static Pair<Object, String> key(String name) {
    return new Pair<>(name, name);
  }

  /**
   * @return key from {@code object} and {@code name}
   */
  public static Pair<Object, String> key(Object object, String name) {
    return new Pair<>(object, name);
  }

  /**
   * @return the constant representing {@code key}; if such a
   * constant is not already present, it is created under {@code key.getSecond()}
   * typed with {@code typeSupplier} and initialised by {@code valueSupplier}; moreover,
   * {@code sideEffect} is executed on registering.
   */
  public TConstant registerConstant(Pair<Object, String> key, Supplier<TDataType> typeSupplier, Supplier<TExpression> valueSupplier, Consumer<TConstant> sideEffect) {
    var constant = this.constants.get(key);
    if (constant != null)
      return constant;
    @SuppressWarnings("null")
    var value = valueSupplier.get();
    @SuppressWarnings("null")
    var type = typeSupplier.get();
    constant = new TConstant(type, uniqueIdentifier(key.getSecond()), value);
    this.constants.put(key, constant);
    sideEffect.accept(constant);
    this.getFrame().addConstant(constant);
    return constant;
  }

  public TConstant registerConstant(Pair<Object, String> key, Supplier<TDataType> typeSupplier, Supplier<TExpression> valueSupplier) {
    return this.registerConstant(key, typeSupplier, valueSupplier, tConstant -> { });
  }

  public TConstant registerConstant(Pair<Object, String> key, Supplier<TExpression> valueSupplier) {
    var constant = this.constants.get(key);
    if (constant != null)
      return constant;
    @SuppressWarnings("null")
    var value = valueSupplier.get();
    return this.registerConstant(key, () -> value.getType(), () -> value);
  }

  /**
   * @return the variable representing {@code key}; if such a
   * variable is not already present, it is created under {@code key.getSecond()}
   * typed with {@code typeSupplier} and initialised by {@code valueSupplier}; moreover,
   * {@code sideEffect} is executed on registering.
   */
  public TVariable registerVariable(Pair<Object, String> key, Supplier<TDataType> typeSupplier, Supplier<@Nullable TExpression> initialValueSupplier, Consumer<TVariable> sideEffect) {
    var variable = this.variables.get(key);
    if (variable != null)
      return variable;
    var name = key.getSecond();
    @SuppressWarnings("null")
    var type = typeSupplier.get();
    var initialValue = initialValueSupplier.get();
    if (initialValue == null)
      variable = new TVariable(type, uniqueIdentifier(name));
    else
      variable = new TVariable(type, uniqueIdentifier(name), initialValue);
    this.variables.put(key, variable);
    sideEffect.accept(variable);
    this.getFrame().addVariable(variable);
    return variable;
  }

  public TVariable registerVariable(Pair<Object, String> key, Supplier<TDataType> typeSupplier, Supplier<@Nullable TExpression> initialValueSupplier) {
    return this.registerVariable(key, typeSupplier, initialValueSupplier, v -> { });
  }

  public TVariable registerVariable(Pair<Object, String> key, Supplier<TDataType> typeSupplier) {
    return this.registerVariable(key, typeSupplier, () -> null);
  }

  /**
   * @return UPPAAL clock for {@code name}; if it does not exist it is created
   */
  public TClock registerClock(String name) {
    var clock = this.clocks.get(name);
    if (clock != null)
      return clock;
    clock = new TClock(uniqueIdentifier(name));
    this.clocks.put(name, clock);
    this.getFrame().addClock(clock);
    return clock;
  }

  /**
   * @return UPPAAL clock array for {@code name} and {@code sizeSupplier}; if it does not exist it is created
   */
  public TClock registerClock(String name, Supplier<Integer> sizeSupplier) {
    var clock = this.clocks.get(name);
    if (clock != null)
      return clock;
    @SuppressWarnings("null")
    var size = sizeSupplier.get();
    clock = new TClock(uniqueIdentifier(name), TExpression.intConst(size));
    this.clocks.put(name, clock);
    this.getFrame().addClock(clock);
    return clock;
  }

  /**
   * @return UPPAAL function for {@code name}; if it does not exist it is created
   */
  public TFunction registerFunction(String name, Supplier<TFunction> functionSupplier) {
    var function = this.functions.get(name);
    if (function != null)
      return function;
    function = functionSupplier.get();
    this.functions.put(name, function);
    this.getFrame().addFunction(function);
    return function;
  }

  /**
   * @return access to empty constant
   */
  public abstract TConstant.Access empty();

  /**
   * @return access to constant for {@code this}
   */
  public TConstant.Access self() {
    assert false : "not applicable";
    return this.empty();
  }

  /**
   * @return UPPAAL expression for accessing {@code uStatual}
   */
  public abstract TField.Access access(UStatual uStatual);

  public abstract static class System extends SymbolTable {
    private TSystem system;
    private Map<String, TDataType.Def> typeDefs = new HashMap<>();
    private Map<String, TChannel> channels = new HashMap<>();
    private Map<String, TTemplate> templates = new HashMap<>();
    private Map<String, TProcess> processes = new HashMap<>();

    System(UModel uModel, TSystem system) {
      super(uModel);
      this.system = system;
    }

    @Override
    public TSystem getFrame() {
      return this.getSystem();
    }

    /**
     * @return symbol table's underlying system
     */
    public TSystem getSystem() {
      return this.system;
    }

    /**
     * @return channel with {@code name} for underlying system; if it
     * does not exist it is created
     */
    public TChannel registerChannel(String name) {
      var channel = this.channels.get(name);
      if (channel != null)
        return channel;
      channel = new TChannel(this.uniqueIdentifier(name));
      this.channels.put(name, channel);
      this.system.addChannel(channel);
      return channel;
    }

    /**
     * @return channel array with {@code name} for underlying system; if it
     * does not exist it is created
     */
    public TChannel registerChannel(String name, boolean isUrgent, @NonNull TExpression ... sizes) {
      var channel = this.channels.get(name);
      if (channel != null)
        return channel;
      channel = new TChannel(isUrgent, this.uniqueIdentifier(name), sizes);
      this.channels.put(name, channel);
      this.system.addChannel(channel);
      return channel;
    }

    /**
     * @return template with {@code name} for underlying system; if it
     * does not exist it is created
     */
    public TTemplate registerTemplate(String name) {
      var template = this.templates.get(name);
      if (template != null)
        return template;
      template = new TTemplate(this.uniqueIdentifier(name));
      this.templates.put(name, template);
      this.system.addTemplate(template);
      return template;
    }

    /**
     * @return type definition for {@code type} with {@code name} for underlying system;
     * if it does not exist it is created
     */
    public TDataType registerTypeDef(String name, Supplier<TDataType> typeSupplier) {
      var typeDef = this.typeDefs.get(name);
      if (typeDef != null)
        return typeDef;
      @SuppressWarnings("null")
      var type = typeSupplier.get();
      typeDef = TDataType.typeDef(this.uniqueIdentifier(name), type);
      this.typeDefs.put(name, typeDef);
      this.system.addTypeDef(typeDef);
      return typeDef;
    }

    /**
     * @return process with {@code name} for {@code template}; if it
     * does not exist it is created
     */
    public TProcess registerProcess(String name, TTemplate template, Function<TParameter, TArgument> argumentProvider) {
      var process = this.processes.get(name);
      if (process != null)
        return process;
      process = new TProcess(this.uniqueIdentifier(name), template);
      for (var parameter : template.getParameters())
        process.addArgument(argumentProvider.apply(parameter));
      this.processes.put(name, process);
      this.system.addProcess(process);
      return process;
    }
  }

  public abstract static class Template extends SymbolTable {
    private TTemplate template;
    private Map<String, TState> states = new HashMap<>();
    private Map<String, TConstant> parameters = new HashMap<>();
    private Map<TParameter, Function<Object, TArgument>> argumentsProvider = new HashMap<>();

    Template(UModel uModel, TTemplate template) {
      super(uModel);
      this.template = template;
    }

    @Override
    public TFrame getFrame() {
      return this.getTemplate();
    }

    public TTemplate getTemplate() {
      return this.template;
    }

    public TConstant registerParameter(String name, TDataType type, Function<Object, TArgument> argumentProvider) {
      var parameter = this.parameters.get(name);
      if (parameter != null)
        return parameter;
      parameter = new TConstant(type, this.uniqueIdentifier(name), type.getDefaultValue());
      this.parameters.put(name, parameter);
      this.argumentsProvider.put(parameter, argumentProvider);
      this.getTemplate().addParameter(parameter);
      return parameter;
    }

    public TArgument getArgument(TParameter parameter, Object object) {
      var argumentProvider = this.argumentsProvider.get(parameter);
      if (argumentProvider == null)
        throw new IllegalStateException("Parameter " + parameter.parameter() + " unknown for template " + this.template.getName() + ": " + argumentsProvider);
      return (@NonNull TArgument)argumentProvider.apply(object);
    }

    /**
     * @return state for {@code name}; if it does not exist it is created with {@code sideEffect}
     */
    public TState registerState(String name, Consumer<TState> sideEffect) {
      var state = this.states.get(name);
      if (state != null)
        return state;
      state = new TState(uniqueIdentifier(name));
      sideEffect.accept(state);
      this.states.put(name, state);
      this.getTemplate().addState(state);
      return state;
    }

    /**
     * @return state for {@code name}; if it does not exist it is created
     */
    public TState registerState(String name) {
      return this.registerState(name, state -> { });
    }
  }

  /**
   * @return {@code identifier} made locally unique
   */
  public String uniqueIdentifier(String identifier) {
    char[] characters = identifier.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    String uniqueIdentifier = new String(characters).substring(0, Math.min(50, characters.length));
    if (uniqueIdentifier.equals("") || this.identifiers.contains(uniqueIdentifier)) {
      int counter = 0;
      String tmpIdentifier;
      do {
        tmpIdentifier = uniqueIdentifier + counter++;
      }
      while (this.identifiers.contains(tmpIdentifier));
      uniqueIdentifier = tmpIdentifier;
    }
    this.identifiers.add(uniqueIdentifier);
    return uniqueIdentifier;
  }
}
