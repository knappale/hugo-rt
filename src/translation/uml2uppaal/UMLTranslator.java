package translation.uml2uppaal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAction;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UCollaboration;
import uml.UContext;
import uml.UExpression;
import uml.UModel;
import uml.UObject;
import uml.UOperator;
import uml.UStatual;
import uml.UType;
import uml.interaction.UInteraction;
import uml.ocl.OTCTLConstraint;
import uml.ocl.OTCTLFormula;
import uml.run.UObjectState;
import uml.run.URun;
import uml.run.USystemState;
import uml.smile.SConstant;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.semantics.UTimerTree;
import uppaal.TClockCondition;
import uppaal.TClockUpperBound;
import uppaal.TExpression;
import uppaal.TGuard;
import uppaal.TInvariant;
import uppaal.TOperator;
import uppaal.TProcess;
import uppaal.TQuery;
import uppaal.TState;
import uppaal.TSystem;
import uppaal.TTransition;
import uppaal.TUpdate;
import uppaal.TVariable;
import uppaal.builder.TNet;
import uppaal.builder.TSubTemplate;
import uppaal.query.TFormula;
import uppaal.run.TProcessState;
import uppaal.run.TSystemState;
import util.Formatter;
import util.Identifier;

import static java.util.stream.Collectors.toList;

import static util.Objects.requireNonNull;


/**
 * Translation of (simple) UML model into UPPAAL
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class UMLTranslator {
  /**
   * Translate a (UML) collaboration and, possibly, a (UML)
   * interaction into an UPPAAL system.
   *
   * Each object is represented by a UPPAAL state machine process that
   * communicate over a central network process.  An observer process
   * is used to check an interaction.  Constraints on the
   * collaboration are translated into UPPAAL queries.
   *
   * @param uCollaboration a (UML) collaboration
   * @param uInteraction a (UML) interaction
   * @param uAssertions a list of (OCL) constraints
   * @return translated UPPAAL system
   */
  public static TSystem translate(@NonNull UCollaboration uCollaboration, UInteraction uInteraction, @NonNull List<@NonNull OTCTLConstraint> uAssertions) {
    ModelSymbolTable moST = ModelSymbolTable.create(uCollaboration, uInteraction);
    TSystem tSystem = moST.getSystem();

    // Create state machines process templates
    for (UClass uClass : uCollaboration.getModel().getClasses()) {
      UStateMachine stateMachine = uClass.getStateMachine();
      if (stateMachine != null)
        StateMachine.create(stateMachine, moST);
    }

    // Create object processes
    for (UObject uObject : uCollaboration.getObjects()) {
      var stateMachine = uObject.getC1ass().getStateMachine();
      if (stateMachine == null)
        continue;
      moST.getStateMachineSymbolTable(stateMachine).getProcess(uObject);
    }

    // Create buffers for objects
    if (moST.getProperties().getNetworkCapacity() > 0) {
      Buffer.create(moST);
      for (UObject uObject : uCollaboration.getObjects())
        if (uObject.getC1ass().getStateMachine() != null && !uObject.getC1ass().getBehaviourals().isEmpty())
          moST.getBufferSymbolTable().getProcess(uObject);
    }

    // Create observer process template
    if (uInteraction != null) {
      var observer = Interaction.create(uInteraction, moST);
      var inST = moST.getObserverSymbolTable();
      // tSystem.addTemplate(inST.getTemplate());
      // tSystem.addProcess(inST.getProcess());
      inST.getProcess();
      tSystem.addQuery(TQuery.ef(observer.getSecond()));
      for (OTCTLFormula assertion : uInteraction.getAssertions()) {
        TFormula assertionFormula = Constraint.translateFormula(assertion, moST);
        // TFormula successState = TFormula.state(moST.getObserverProcess(), moST.getObserverSuccessState());
        tSystem.addQuery(TQuery.ag(TFormula.imply(observer.getSecond(), assertionFormula)));
      }
    }

    // Create constraints
    for (OTCTLConstraint constraint : uAssertions) {
      TQuery query = Constraint.create(constraint, moST);
      tSystem.addQuery(query);
    }

    return tSystem;
  }

  public static URun reconstruct(List<TSystemState> trail, @NonNull UCollaboration collaboration, UInteraction interaction) {
    URun umlRun = new URun();
    ModelSymbolTable moST = ModelSymbolTable.create(collaboration, interaction);

    for (TSystemState systemState : trail) {
      if (systemState == null)
        continue;
      USystemState uSystemState = new USystemState();
      for (@NonNull UObject uObject : collaboration.getObjects()) {
        var uObjectState = uSystemState.getObjectState(uObject);
        Queue.reconstruct(uObjectState, systemState, moST);
        Buffer.reconstruct(uObjectState, systemState, moST);
        UMLTranslator.reconstruct(uObjectState, systemState, moST);
        StateMachine.reconstruct(uObjectState, systemState, moST);
      }
      umlRun.addSystemState(uSystemState);
    }

    return umlRun;    
  }

  /**
   * Translate a UML value (expression) into an UPPAAL integer
   * expression.
   *
   * @param value a UML value (expression)
   * @param uContext a UML context
   * @param symbolTable a translation symbol table
   * @return UPPAAL integer expression representing value
   */
  @NonNullByDefault
  public static TExpression translateValue(UExpression value, UContext uContext, SymbolTable symbolTable) {
    return (new ExpressionTranslator(uContext, symbolTable)).process(value);
  }

  /**
   * Translate a UML expression into an UPPAAL integer expression.
   *
   * @param expression a UML expression
   * @param uContext a UML context
   * @param symbolTable a translation symbol table
   * @return UPPAAL integer expression representing expression
   */
  @NonNullByDefault
  public static TExpression translateExpression(UExpression expression, UContext modelContext, SymbolTable symbolTable) {
    return (new ExpressionTranslator(modelContext, symbolTable)).process(expression);
  }

  /**
   * Translate a UML action into an UPPAAL annotation net.
   *
   * @param action a UML action
   * @param uContext a UML context
   * @param smST state machine symbol table
   * @return UPPAAL annotation net representing action
   */
  @NonNullByDefault
  public static TNet translateAction(UAction action, UContext modelContext, StateMachine.SymbolTable smST) {
    return (new ActionTranslator(modelContext, smST)).process(action);
  }

  /**
   * Translation of UML expressions.
   */
  @NonNullByDefault
  private static class ExpressionTranslator implements UExpression.InContextVisitor<TExpression> {
    private UContext context;
    private SymbolTable symbolTable;
    private @Nullable UExpression uExpression;

    ExpressionTranslator(UContext context, SymbolTable symbolTable) {
      this.context = context;
      this.symbolTable = symbolTable;
    }

    TExpression process(UExpression value) {
      this.uExpression = value;
      return value.accept(this);
    }

    RuntimeException exception() {
      return new RuntimeException("Cannot translate expression `" + this.uExpression + "' in model `" + getSymbolTable().getModel().getName() + "' into UPPAAL");
    }

    public UContext getContext() {
      return this.context;
    }

    SymbolTable getSymbolTable() {
      return this.symbolTable;
    }

    public TExpression onBooleanConstant(boolean boolConst) {
      return boolConst ? TExpression.trueConst() : TExpression.falseConst();
    }

    public TExpression onIntegerConstant(int intConst) {
      return TExpression.intConst(intConst);
    }

    public TExpression onStringConstant(String stringConstant) {
      throw exception();
    }

    public TExpression onNull() {
      return getSymbolTable().empty();
    }

    public TExpression onThis() {
      return getSymbolTable().self();
    }

    public TExpression onStatual(UStatual statual) {
      return getSymbolTable().access(statual);
    }

    public TExpression onArray(UAttribute attribute, UExpression offset) {
      return getSymbolTable().access(attribute).access(process(offset));
    }

    public TExpression onConditional(UExpression uCondition, UExpression uLeft, UExpression uRight) {
      TExpression condition = process(uCondition);
      TExpression left = process(uLeft);
      TExpression right = process(uRight);
      return TExpression.conditional(condition, left, right);
    }

    public TExpression onUnary(UOperator unary, UExpression inner) {
      TExpression innerExpression = process(inner);
      switch (unary) {
        case UPLUS:
          return innerExpression;
        case UMINUS:
          return TExpression.minus(innerExpression);
        case UNEG:
          return TExpression.neg(innerExpression);
        //$CASES-OMITTED$
        default :
          throw exception();
      }
    }

    public TExpression onArithmetical(UExpression left, UOperator arithmetical, UExpression right) {
      TExpression leftExpression = process(left);
      TExpression rightExpression = process(right);
      switch (arithmetical) {
        case ADD:
          return TExpression.arithmetical(leftExpression, TOperator.ADD, rightExpression);
        case SUB:
          return TExpression.arithmetical(leftExpression, TOperator.SUB, rightExpression);
        case MULT:
          return TExpression.arithmetical(leftExpression, TOperator.MULT, rightExpression);
        case DIV:
          return TExpression.arithmetical(leftExpression, TOperator.DIV, rightExpression);
        case MOD:
          return TExpression.arithmetical(leftExpression, TOperator.MOD, rightExpression);
        //$CASES-OMITTED$
        default :
          throw exception();
      }
    }

    public TExpression onRelational(UExpression left, UOperator relational, UExpression right) {
      TExpression leftExpression = process(left);
      TExpression rightExpression = process(right);
      switch (relational) {
        case LT:
          return TExpression.relational(leftExpression, TOperator.LT, rightExpression);
        case LEQ:
          return TExpression.relational(leftExpression, TOperator.LEQ, rightExpression);
        case EQ:
          return TExpression.relational(leftExpression, TOperator.EQ, rightExpression);
        case NEQ:
          return TExpression.relational(leftExpression, TOperator.NEQ, rightExpression);
        case GEQ:
          return TExpression.relational(leftExpression, TOperator.GEQ, rightExpression);
        case GT:
          return TExpression.relational(leftExpression, TOperator.GT, rightExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }

    public TExpression onJunctional(UExpression left, UOperator junctional, UExpression right) {
      TExpression leftExpression = process(left);
      TExpression rightExpression = process(right);
      switch (junctional) {
        case AND:
          return TExpression.and(leftExpression, rightExpression);
        case OR:
          return TExpression.or(leftExpression, rightExpression);
        //$CASES-OMITTED$
        default :
          throw exception();
      }
    }
  }

  /**
   * Translation of UML actions.
   */
  @NonNullByDefault
  private static class ActionTranslator implements UAction.InContextVisitor<TNet> {
    private UContext uContext;
    private StateMachine.SymbolTable smST;
    private @Nullable UAction uAction;

    ActionTranslator(UContext context, StateMachine.SymbolTable smST) {
      this.uContext = context;
      this.smST = smST;
    }

    TNet process(UAction uAction) {
      this.uAction = uAction;
      return uAction.accept(this);
    }

    RuntimeException exception() {
      return new RuntimeException("Cannot translate action " + Formatter.quoted(uAction) + " in class " + Formatter.quoted(this.smST.getClass().getName()) + " into UPPAAL");
    }

    public @NonNull UContext getContext() {
      return this.uContext;
    }

    /**
     * Create an empty annotation net atomic sequence for the skip
     * action.
     */
    public TNet onSkip() {
      return TNet.trans();
    }

    /**
     * Create a combined annotation net for a choice action.
     *
     * The branches for the left and the right choice are combined
     * additively.
     */
    public TNet onChoice(UAction left, UAction right) {
      return TNet.choice(process(left), process(right));
    }

    /**
     * Create annotation matrix for a parallel action.
     *
     * The annotation net is computed from the interleaving normal
     * form of the parallel action, using choices of sequential
     * branches.
     */
    public TNet onParallel(UAction left, UAction right) {
      return process(UAction.par(left, right).getInterleavingNormalForm());
    }

    /**
     * Create annotation net for a sequential action.
     *
     * The annotation net for the left part is combined with the
     * annotation net for the right part.
     */
    public TNet onSequential(UAction left, UAction right) {
      return TNet.seq(process(left), process(right));
    }

    /**
     * Create annotation net for conditional action.
     *
     * The two annotation nets for the true and the false branch
     * are added.
     */
    public TNet onConditional(UExpression condition, UAction left, UAction right) {
      return TNet.choice(TNet.seq().add(translateExpression(condition, uContext, smST)).add(process(left)),
                         TNet.seq().add(translateExpression(UExpression.neg(condition), uContext, smST)).add(process(right)));
    }

    /**
     * Create annotation net for assertion.
     *
     * The two annotation nets for the true and the false branch
     * are added.
     */
    public TNet onAssertion(UExpression expression) {
      return TNet.choice(TNet.seq().add(translateExpression(expression, uContext, smST)),
                         TNet.seq().add(translateExpression(UExpression.neg(expression), uContext, smST)).add(TNet.trans(smST.getFailState())));
    }

    /**
     * Create annotation net for an invocation on {@code this}.
     * 
     * This is treated differently than invocations to other state machines since
     * the message will not travel through the network and thus will not take time.
     * (In fact this only would have to be taken into account if the network delay is
     * greater than zero.)
     */
    private TNet selfInvocation(UBehavioural uBehavioural, List<UExpression> uArguments) {
      if (uBehavioural.isStatic())
        return TNet.trans(smST.call(uBehavioural, uArguments.stream().map(a -> translateExpression(a, uContext, smST)).collect(toList())));

      var moST = smST.getModelSymbolTable();
      var quST = smST.getQueueSymbolTable();
      var comm = moST.comm();
      var invocation = TNet.seq().add(TExpression.neg(quST.externalsFull()))
                                 .add(comm.access(moST.ID).assign(smST.id(uBehavioural)))
                                 .add(comm.access(moST.SENDER).assign(smST.self()))
                                 .add(comm.access(moST.RECEIVER).assign(smST.self()))
                                 .add(IntStream.range(0, uArguments.size()).mapToObj(i ->
                                          comm.access(moST.PARAMS).access(i).assign(translateExpression(uArguments.get(i), uContext, smST))).collect(toList()))
                                 .add(Interaction.prepareObserveSending(smST))
                                 .add(quST.externalsEnqueue())
                                 .add(Interaction.observeSending(smST))
                                 .add((TNet)uBehavioural.cases(uOperation -> TNet.trans(smST.getFailState()), // an acknowledgement to a call on {@code this} is impossible...
                                                               uReception -> TNet.trans()));
      if (smST.getProperties().isBlocking())
        return invocation;
      return TNet.choice(invocation,
                         TNet.seq().add(quST.externalsFull())
                                   .add(TNet.trans(quST.externalsOverflow())));
    }

    public TNet onSelfInvocation(UBehavioural uBehavioural, List<UExpression> uArguments) {
      return this.selfInvocation(uBehavioural, uArguments);
    }

    /**
     * Create an annotation net for a call on {@code uStatual}
     * if this represents an object different from {@code this}.
     */
    private TNet otherInvocation(TExpression receiver, UBehavioural uBehavioural, List<UExpression> uArguments) {
      if (uBehavioural.isStatic())
        return TNet.trans(smST.call(uBehavioural, uArguments.stream().map(a -> translateExpression(a, uContext, smST)).collect(toList())));

      var moST = smST.getModelSymbolTable();
      var comm = moST.comm();
      var isBuffered = moST.getProperties().getNetworkCapacity() > 0;
      return TNet.seq().add(TNet.trans().add((isBuffered ? moST.toQueueBuffer(receiver) : moST.toStateMachine(receiver)).send())
                                        .add(comm.access(moST.ID).assign(moST.id(uBehavioural)))
                                        .add(comm.access(moST.SENDER).assign(smST.self()))
                                        .add(comm.access(moST.RECEIVER).assign(receiver))
                                        .add(IntStream.range(0, uArguments.size()).mapToObj(i ->
                                                 comm.access(moST.PARAMS).access(i).assign(translateExpression(uArguments.get(i), uContext, smST))).collect(toList()))
                                        .add(Interaction.prepareObserveSending(smST)))
                       .add(Interaction.observeSending(smST))
                       .add((TNet)uBehavioural.cases(uOperation -> Queue.waitForAcknowledgement(smST),
                                                     uReception -> TNet.trans()));
    }

    /**
     * Create annotation net for an invocation.
     *
     * A call on an object of a type that does not subsume the
     * type of {@code this} can be translated straightforwardly.
     * If it is possible that the call is on {@code this},
     * both possibilities are taken into account. The distinction is
     * necessary since we assume that a message sent to oneself does not
     * go through the network and thus does not take time.
     */
    public TNet invocation(UType uType, TExpression receiver, UBehavioural uBehavioural, List<UExpression> uParameters) {
      var otherInvocation = otherInvocation(receiver, uBehavioural, uParameters);
      // If it cannot be a call on {@code this}
      if (!uType.subsumes(uContext.getThisType()) || uBehavioural.isStatic())
        return otherInvocation;
      // If it can be a call on {@code this}, we consider both cases
      return TNet.choice(TNet.seq().add(TExpression.neq(receiver, smST.self())).add(otherInvocation),
                         TNet.seq().add(TExpression.eq(receiver, smST.self())).add(selfInvocation(uBehavioural, uParameters)));
    }

    public TNet onInvocation(UStatual uStatual, UBehavioural uBehavioural, List<UExpression> uParameters) {
      return invocation(uStatual.getType(), smST.access(uStatual), uBehavioural, uParameters);
    }

    public TNet onArrayInvocation(UAttribute uAttribute, UExpression offset, UBehavioural uBehavioural, List<UExpression> uParameters) {
      return invocation(uAttribute.getType(), smST.access(uAttribute).access(translateExpression(offset, uContext, smST)), uBehavioural, uParameters);
    }

    /**
     * Create annotation net for an assignment to a simple attribute.
     */
    public TNet onAssignment(UAttribute uAttribute, UExpression uExpression) {
      return TNet.trans(((TVariable.Access)smST.access(uAttribute)).assign(translateExpression(uExpression, uContext, smST)));
    }

    /**
     * Create annotation net for an assignment to an array attribute.
     */
    public TNet onArrayAssignment(UAttribute uAttribute, UExpression uOffset, UExpression uExpression) {
      return TNet.trans(((TVariable.Access)smST.access(uAttribute)).access(translateExpression(uOffset, uContext, smST)).assign(translateExpression(uExpression, uContext, smST)));
    }

    /**
     * Create  an annotation net for post increment/decrement of a variable.
     */
    private TNet post(TVariable.Access access, UAction.PostKind kind) {
      switch (kind) {
        case INC:
          return TNet.trans(access.assign(TExpression.arithmetical(access, TOperator.ADD, 1)));
        case DEC:
          return TNet.trans(access.assign(TExpression.arithmetical(access, TOperator.SUB, 1)));
        default:
          throw exception();
      }
    }

    public TNet onPost(UAttribute uAttribute, UAction.PostKind kind) {
      return post((TVariable.Access)smST.access(uAttribute), kind);
    }

    public TNet onArrayPost(UAttribute attribute, UExpression offset, UAction.PostKind kind) {
      return post(((TVariable.Access)smST.access(attribute)).access(translateExpression(offset, uContext, smST)), kind);
    }
  }

  /**
   * Compute an annotation matrix for fetching an event from the event
   * queue.
   *
   * It can not be expected that some event is present when an event
   * is looked for.  However, events are fetched eagerly, as the
   * communication channel is declared to be `urgent'.
   * 
   * The algorithm is the following: First, we look whether there is an
   * internal event to be handled; these have to be prioritised.  Internal
   * events are either completion or time events.  If there is an internal
   * event to be handled it is made the active stimulus.
   * 
   * If there is no internal event, we move to a state where time may pass.
   * Passage of time has either to be restricted if timers may be running; or
   * it is unlimited; which timers are currently active can be inferred from
   * the current state configuration. In any case, we look for external events
   * coming from the queue via the network.  If there is an external event to
   * be handled, we make it the active stimulus.  Furthermore, if there are
   * wait states, a completion event for such a wait state can be generated
   * and made the active stimulus.
   * 
   * The computation of the different wait states, where time can pass,
   * relies on the {@link UTimerTree TimerTree} for the current state machine.
   * There, we compute which timers can be active simultaneously, i.e. for
   * which time events the state machine may be waiting for at a given time
   * instance.
   * 
   * @param waitStates set of wait states
   * @param smST symbol table for the current state machine
   * @return an annotation net for fetch
   */
  @NonNullByDefault
  public static TNet fetch(Set<UState> waitStates, StateMachine.SymbolTable smST) {
    var moST = smST.getModelSymbolTable();
    var quST = smST.getQueueSymbolTable();
    var hasDeferrableEvents = !smST.getStateMachine().getDeferrableEvents().isEmpty();

    TState preFetchState = new TState(smST.uniqueIdentifier("PreFetch"));
    preFetchState.setCommitted(true);
    TState fetchState = new TState(smST.uniqueIdentifier("Fetch"));
    // fetchState.setUrgent(true);
    TState fetchedState = new TState(smST.uniqueIdentifier("Fetched"));
    fetchedState.setCommitted(true);
    
    TSubTemplate fetch = new TSubTemplate(preFetchState, fetchedState);
    // TODO (AK060529) Add receiving notification for observer

    // First, reset the mutex and the stimulus
    var resetUpdates = new ArrayList<TUpdate>();
    resetUpdates.add(smST.current().assign(moST.emptyMessage()));
    fetch.addTransition(preFetchState, fetchState, (TGuard)null, null, resetUpdates);

    // Fetch an event from the internal event queue
    fetch.addTransition(internal(fetchState, fetchedState, smST));

    // Timer states, recorded in a map from timer sets to UPPAAL states
    Map<Set<UEvent>, TState> timerStates = addTimerStates(fetch, smST);

    var timerTree = smST.getTimerTree();
    for (var timerSet : timerTree.getTimerSets()) {
      var timerState = requireNonNull(timerStates.get(timerSet));

      // Create a set of timer numbers containing all those timer numbers
      // which are not active in the current timer set.
      var emptyTimerNumberSet = timerTree.getTimerNumbersSet();
      for (var timeEvent : timerSet) {
        emptyTimerNumberSet.remove(timerTree.getTimerNumber(timeEvent));
      }
      
      // Entering a timer state
      List<TGuard> timerGuards = enteringTimerStateGuards(timerTree, timerSet, smST);
      // if (smST.hasDeferrableEvents())
      //   timerGuards.add(TIntegerGuard.relational(TIntegerAccess.variable(smST.getDeferredEventsLengthVariable()), TOperators.LEQ, TIntegerAccess.variable(smST.getDeferredCounterVariable())));
      timerGuards.add(TExpression.eq(quST.internals().access(0), smST.empty()));
      fetch.addTransition(fetchState, timerState, timerGuards, null, new ArrayList<>());

      // Leaving a timer state via a time out
      for (UEvent timeEvent : timerSet)
        fetch.addTransition(timer(timeEvent, timerState, fetchedState, emptyTimerNumberSet, smST));

      // Leaving a timer state via an external event
      fetch.addTransition(external(timerState, fetchedState, emptyTimerNumberSet, smST));

      // "Fetch" a completion event for a wait state (i.e., create one)
      if (waitStates != null && !waitStates.isEmpty()) {
        for (UState waitState : waitStates)
          fetch.addTransition(waitStateCompletion(waitState, timerState, fetchedState, smST));
      }
    
      // Fetch an event from the deferred event queue
      if (hasDeferrableEvents)
        fetch.addTransition(deferred(timerState, fetchedState, smST));
    }
    
    return TNet.sub(fetch);
  }

  @NonNullByDefault
  private static TTransition internal(TState from, TState to, StateMachine.SymbolTable smST) {
    var quST = smST.getQueueSymbolTable();
    return new TTransition(from, to, TExpression.neq(quST.internals().access(0), smST.empty()), null,
                                     Arrays.asList(quST.internalsPop()));
  }

  @SuppressWarnings("unused")
  @NonNullByDefault
  private static TTransition internalOld(TState from, TState to, StateMachine.SymbolTable smST) {
    var moST = smST.getModelSymbolTable();
    var quST = smST.getQueueSymbolTable();
    var isMutex = smST.getProperties().isMutex();

    var fetchInternalGuards = new ArrayList<TGuard>();
    fetchInternalGuards.add(TExpression.neq(quST.internals().access(0), smST.empty()));
    var fetchInternalUpdates = new ArrayList<TUpdate>();
    fetchInternalUpdates.add(smST.current().assign(moST.emptyMessage()));
    fetchInternalUpdates.add(smST.current().access(moST.ID).assign(TExpression.arithmetical(quST.internals().access(0), TOperator.ADD, moST.getMaxExternalEventsNumber())));
    for (int i = 0; i < smST.getProperties().getInternalQueueCapacity()-1; i++)
      fetchInternalUpdates.add(quST.internals().access(i).assign(quST.internals().access(i+1)));
    fetchInternalUpdates.add(quST.internals().access(smST.getProperties().getInternalQueueCapacity()-1).assign(smST.empty()));
    return new TTransition(from, to, fetchInternalGuards, null, fetchInternalUpdates);
  }

  @NonNullByDefault
  private static TTransition timer(UEvent timeEvent, TState from, TState to, Set<Integer> emptyTimerNumberSet, StateMachine.SymbolTable smST) {
    var moST = smST.getModelSymbolTable();

    List<TGuard> timedOutGuards = new ArrayList<>();
    timedOutGuards.add(TClockCondition.geq(smST.timer(timeEvent), smST.timerLow(timeEvent)));
    List<TUpdate> timedOutUpdates = new ArrayList<>();
    timedOutUpdates.add(smST.current().assign(moST.emptyMessage()));
    timedOutUpdates.add(smST.current().access(moST.ID).assign(smST.id(timeEvent)));
    timedOutUpdates.add(smST.timer(timeEvent).reset());
    for (int timerNumber : emptyTimerNumberSet)
      timedOutUpdates.add(smST.timer(timerNumber).reset());
    timedOutUpdates.add(smST.timedOut(timeEvent).assign(true));
    return new TTransition(from, to, timedOutGuards, null, timedOutUpdates);
  }

  @NonNullByDefault
  private static TTransition external(TState timerState, TState fetchedState, Set<Integer> emptyTimerNumberSet, StateMachine.SymbolTable smST) {
    var moST = smST.getModelSymbolTable();
    var quST = smST.getQueueSymbolTable();
    var hasDeferrableEvents = !smST.getStateMachine().getDeferrableEvents().isEmpty();

    var externalGuards = new ArrayList<TGuard>();
    if (hasDeferrableEvents)
      externalGuards.add(quST.deferredsEnabled());
    var externalSynchronisation = smST.getToStateMachine().receive();
    var externalUpdates = new ArrayList<TUpdate>();
    externalUpdates.add(smST.current().assign(moST.comm()));
    externalUpdates.add(moST.comm().assign(moST.emptyMessage()));
    for (int timerNumber : emptyTimerNumberSet)
      externalUpdates.add(smST.timer(timerNumber).reset());
    return new TTransition(timerState, fetchedState, externalGuards, externalSynchronisation, externalUpdates);
  }

  @NonNullByDefault
  private static TTransition deferred(TState timerState, TState fetchedState, StateMachine.SymbolTable smST) {
    var quST = smST.getQueueSymbolTable();
    return new TTransition(timerState, fetchedState,
                           TExpression.and(TExpression.neg(quST.internalsEnabled()), TExpression.neg(quST.deferredsEnabled())),
                           null,
                           Arrays.asList(quST.deferredsPop()));
  }

  @NonNullByDefault
  private static TTransition waitStateCompletion(UState waitState, TState from, TState to, StateMachine.SymbolTable smST) {
    var moST = smST.getModelSymbolTable();
    var quST = smST.getQueueSymbolTable();
    var hasDeferrableEvents = !smST.getStateMachine().getDeferrableEvents().isEmpty();

    var completionGuards = new ArrayList<TGuard>();
    if (hasDeferrableEvents)
      completionGuards.add(quST.deferredsEnabled());
    completionGuards.add(quST.internalsEnabled());
    completionGuards.add(TExpression.eq(smST.access(waitState), smST.id(waitState)));
    completionGuards.add(TExpression.eq(smST.completed(waitState), TExpression.trueConst()));
    var completionUpdates = new ArrayList<TUpdate>();
    completionUpdates.add(smST.current().assign(moST.emptyMessage()));
    completionUpdates.add(smST.current().access(moST.ID).assign(smST.id(UEvent.completion(waitState))));
    return new TTransition(from, to, completionGuards, null, completionUpdates);
  }

  /**
   * Generate guards for entering a timer state.
   *
   * @param smST a state machine symbol table
   * @param timerTree a timer tree
   * @param timerSet a timer set
   * @return guards for entering a timer state
   */
  @NonNullByDefault
  private static List<TGuard> enteringTimerStateGuards(UTimerTree timerTree, Set<UEvent> timerSet, StateMachine.SymbolTable smST) {
    var timerGuards = new ArrayList<TGuard>();
    for (var timeEvent : timerTree.getTimeEvents()) {
      var timerGuard = TExpression.neg(smST.timedOut(timeEvent));
      var timeEventState = requireNonNull(timeEvent.getState());
      timerGuard = TExpression.and(timerGuard, TExpression.eq(smST.access(timeEventState), smST.id(SConstant.vertex(timeEventState))));
      timerGuards.add(timerSet.contains(timeEvent) ? timerGuard : TExpression.neg(timerGuard));
    }
    return timerGuards;
  }

  /**
   * Add timer states to a sub-process and record the timer states
   * in a map from timer sets to UPPAAL states
   * 
   * @param subProcess a (UPPAAL) sub-process
   * @param smST a state machine symbol table
   * @return
   */
  @NonNullByDefault
  private static Map<Set<UEvent>, TState> addTimerStates(TSubTemplate subProcess, StateMachine.SymbolTable smST) {
    var timerStates = new HashMap<Set<UEvent>, TState>();
    for (var timerSet : smST.getTimerTree().getTimerSets()) {
      var invariants = new ArrayList<TInvariant>();
      String id = "";
      for (var timeEvent : timerSet) {
        int timeEventNumber = smST.getTimerTree().getTimerNumber(timeEvent);
        id += "_" + timeEventNumber + "W";
        invariants.add(TClockUpperBound.leq(smST.timer(timeEvent), smST.timerHigh(timeEvent)));
      }
      TState state = subProcess.addState(smST.uniqueIdentifier(subProcess.getEntry().getName() + "Time" + id), invariants);
      timerStates.put(timerSet, state);
    }
    return timerStates;
  }

  /**
   * Compute an annotation net for sending a completion event to the
   * event queue.
   */
  @NonNullByDefault
  @Deprecated
  public static TNet complete(UState completeState, StateMachine.SymbolTable smST) {
    var quST = smST.getQueueSymbolTable();

    var annotations = TNet.seq();
    if (!smST.getCompletionTree().isCompletionState(completeState))
      return annotations;

    annotations.add(smST.completed(completeState).assign(true));

    // Only add a completion event, if there is an exiting completion transition.
    if (completeState.getOutgoingCompletions().size() <= completeState.getOutgoingWaits().size())
      return annotations;

    var complete = TNet.trans();
    complete.add(TExpression.eq(quST.internals().access(TExpression.arithmetical(quST.internalsCapacity(), TOperator.SUB, TExpression.intConst(1))), smST.empty()));
    for (int i = quST.getProperties().getInternalQueueCapacity()-1; i > 0; i--)
      complete.add(quST.internals().access(i).assign(quST.internals().access(i-1)));
    complete.add(quST.internals().access(0).assign(smST.id(UEvent.completion(completeState))));

    if (!smST.getProperties().isBlocking()) {
      var overflow = TNet.trans(quST.internalsOverflow(),
                                TExpression.neq(quST.internals().access(TExpression.arithmetical(quST.internalsCapacity(), TOperator.SUB, TExpression.intConst(1))), smST.empty()));
      annotations.add(TNet.choice(complete, overflow));
    }
    else
      annotations.add(complete);
    return annotations;
  }

  /**
   * Compute a UPPAAL expression for matching an event against the current event.
   * 
   * @param uEvent a (UML) event
   * @param smST a state machine symbol table
   * @return a (UPPAAL) expression for matching an event against the current
   * event
   */
  @NonNullByDefault
  public static TExpression match(UEvent uEvent, StateMachine.SymbolTable smST) {
    var moST = smST.getModelSymbolTable();
    if (uEvent.isCompletion() && uEvent.getStates().size() > 1) {
      TExpression matchExpression = TExpression.falseConst();
      for (UState state : uEvent.getStates()) {
        matchExpression = TExpression.or(matchExpression, TExpression.eq(smST.current().access(moST.ID), smST.id(UEvent.completion(state))));
      }
      return matchExpression;
    }
    return TExpression.eq(smST.current().access(moST.ID), smST.id(uEvent));
  }
  
  public static @NonNull UExpression reconstruct(UType type, int value, ModelSymbolTable moST) {
    UModel model = moST.getModel();
    
    if (type.isNull())
      return UExpression.nullConst();
    
    if (type.isClass()) {
      if (value == moST.empty().getIntValue())
        return UExpression.nullConst();

      UObject uObject = moST.getObject(value);
      if (uObject != null)
        return UExpression.id(Identifier.id(uObject.getName()));
    }
    
    if (type.isDataType()) {
      if (type.equals(model.getBooleanType()))
        return (value != 0 ? UExpression.trueConst() : UExpression.falseConst());
      if (type.equals(model.getIntegerType()))
        return UExpression.intConst(value);
      if (type.equals(model.getClockType()))
        return UExpression.intConst(value);
    }
    
    util.Message.debug().info("Cannot reconstruct expression from value `" + value + "' in type `" + type + "'");
    return UExpression.intConst(0);
  }
  
  public static void reconstruct(UObjectState uObjectState, TSystemState tSystemState, ModelSymbolTable moST) {
    UObject uObject = uObjectState.getObject();
    UStateMachine uStateMachine = uObject.getC1ass().getStateMachine();
    if (uStateMachine == null)
      return;

    StateMachine.SymbolTable smST = moST.getStateMachineSymbolTable(uStateMachine);
    TProcess tObjectProcess = smST.getProcess(uObject);
    TProcessState tObjectProcessState = tSystemState.getProcessState(tObjectProcess);
    if (tObjectProcessState == null) {
      util.Message.debug().info("No process state for object `" + uObject.getName() + "' found.");
      return;
    }

    // Reconstruct stimulus
    var current = smST.current();
    int stimulusValue = tObjectProcessState.getValue(current.access(moST.ID));
    if (stimulusValue != 0) {
      UEvent uEvent = smST.getEvent(stimulusValue);
      if (uEvent == null)
        util.Message.debug().info("No event for number `" + stimulusValue + "' found.");
      else {
        // Cope also for queues for objects without operations
        var senderVariable = (uObject.getC1ass().getOperations().size() > 0 ? current.access(moST.SENDER) : null);
        UObject uSender = (senderVariable == null ? null : moST.getObject(tObjectProcessState.getValue(senderVariable)));
        List<@NonNull UExpression> uArguments = new ArrayList<>();
        if (uEvent.isBehavioural()) {
          UBehavioural uBehavioural = uObject.getC1ass().getBehavioural(uEvent);
          if (uBehavioural == null)
            util.Message.debug().info("No behavioural for `" + uEvent + "' found.");
          else {
            var parameters = current.access(moST.PARAMS);
            for (int i = 0; i < uBehavioural.getParameters().size(); i++) {
              UExpression uArgument = UMLTranslator.reconstruct(uBehavioural.getParameters().get(i).getType(), tObjectProcessState.getValue(parameters.access(i)), moST);
              uArguments.add(uArgument);
            }
          }
        }
        uObjectState.setStimulus(uEvent, uArguments, uSender);
      }
    }
    
    // Reconstruct attributes
    for (UAttribute uAttribute : uObject.getC1ass().getAttributes()) {
      if (uAttribute.isConstant())
        continue;

      var attributeAccess = (TVariable.Access)smST.access(uAttribute);

      if (uAttribute.isStatic()) {
        if (uAttribute.isArray()) {
          List<UExpression> uAttributeValues = new ArrayList<>();
          for (int i = 0; i < uAttribute.getSize(); i++) {
            UExpression uAttributeValue = UMLTranslator.reconstruct(uAttribute.getType().getUnderlyingType(), tSystemState.getValue(attributeAccess.access(i)), moST);
            uAttributeValues.add(uAttributeValue);
          }
          uObjectState.addAttributeValues(uAttribute, uAttributeValues);
        }
        else {
          UExpression attributeValue = UMLTranslator.reconstruct(uAttribute.getType(), tSystemState.getValue(attributeAccess), moST);
          uObjectState.addAttributeValue(uAttribute, attributeValue);
        }
        continue;
      }

      if (uAttribute.isArray()) {
        List<UExpression> uAttributeValues = new ArrayList<>();
        for (int i = 0; i < uAttribute.getSize(); i++) {
          UExpression uAttributeValue = UMLTranslator.reconstruct(uAttribute.getType().getUnderlyingType(), tObjectProcessState.getValue(attributeAccess.access(i)), moST);
          uAttributeValues.add(uAttributeValue);
        }
        uObjectState.addAttributeValues(uAttribute, uAttributeValues);
        continue;
      }
      
      // Simple, non-constant, non-static attribute
      UExpression uAttributeValue = UMLTranslator.reconstruct(uAttribute.getType(), tObjectProcessState.getValue(attributeAccess), moST);
      uObjectState.addAttributeValue(uAttribute, uAttributeValue);
    }
  }
}
