package translation.uml2uppaal;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UObject;
import uml.UStatual;
import uml.run.UObjectState;
import uppaal.TBlock;
import uppaal.TChannel;
import uppaal.TClock;
import uppaal.TClockUpperBound;
import uppaal.TConditional;
import uppaal.TConstant;
import uppaal.TDataType;
import uppaal.TExpression;
import uppaal.TField;
import uppaal.TFunction;
import uppaal.TLoop;
import uppaal.TOperator;
import uppaal.TProcess;
import uppaal.TState;
import uppaal.TStatement;
import uppaal.TTemplate;
import uppaal.TTransition;
import uppaal.TVariable;
import uppaal.run.TSystemState;
import util.Strings;


@NonNullByDefault
public class Buffer {
  private SymbolTable buST;

  public static class SymbolTable extends translation.uml2uppaal.SymbolTable.Template {
    private ModelSymbolTable moST;

    public SymbolTable(ModelSymbolTable moST) {
      super(moST.getModel(), moST.registerTemplate("clsBuffer"));
      this.moST = moST;
    }

    @Override
    public TConstant.Access empty() {
      return moST.empty();
    }

    @Override
    public TConstant.Access self() {
      return this.registerParameter("this", TDataType.intType(), object -> this.moST.id((UObject)object)).access();
    }

    @Override
    public TField.Access access(UStatual uStatual) {
      return moST.access(uStatual);
    }

    public ModelSymbolTable getModelSymbolTable() {
      return this.moST;
    }

    public TProcess getProcess(UObject uObject) {
      this.self();
      return moST.registerProcess("obj" + Strings.capitalise(uObject.getName()) + "Buffer", this.getTemplate(), parameter -> this.getArgument(parameter, uObject));
    }

    /**
     * @return access to channel to queue
     */
    private TChannel.Access toQueueBuffer() {
      return moST.toQueueBuffer(this.self());
    }

    /**
     * @return access to channel to state machine
     */
    public TChannel.Access toStateMachine() {
      return moST.toStateMachine(this.self());
    }

    public TConstant.@Nullable Access getMaxDelay() {
      var maxDelay = this.getProperties().getMaxDelay();
      if (maxDelay < 0)
        return null;
      return this.registerConstant(key("maxDelay"), () -> TExpression.intConst(maxDelay)).access();
    }

    public TVariable.Access elements() {
       return this.registerVariable(key("elements"), () -> TDataType.array(moST.getMessageType(), this.getProperties().getNetworkCapacity())).access();
    }

    public TVariable.Access tickets() {
      return this.registerVariable(key("tickets"), () -> TDataType.array(TDataType.range(this.getProperties().getNetworkCapacity()), this.getProperties().getNetworkCapacity())).access();
    }

    public TClock.@Nullable Access clocks() {
      if (this.getProperties().getMaxDelay() < 0)
        return null;
      return this.registerClock("clocks", () -> this.getProperties().getNetworkCapacity()).access();
    }

    public TState bufferOverflow() {
      return this.registerState("BufferOverflow");
    }

    public TFunction.Call fromComm(int source, int place, int target) {
      return this.registerFunction("fromComm", () -> getFromCommFunctionDeclaration(this.getProperties().getNetworkCapacity(), this)).call(source, place, target);
    }

    public TFunction.Call fromBuffer(int source, int place) {
      return this.registerFunction("fromBuffer", () -> getFromBufferFunctionDeclaration(this.getProperties().getNetworkCapacity(), this)).call(source, place);
    }
  }

  private Buffer(ModelSymbolTable moST) {
    this.buST = moST.getBufferSymbolTable();
  }

  /**
   * Create an event buffer (template).
   * 
   * @param moST shared model symbol table
   * @return event queue (template) for state machine
   */
  public static @Nullable TTemplate create(ModelSymbolTable moST) {
    return new Buffer(moST).create();
  }
  
  /**
   * @return a networked event queue (template) for a UML state machine
   */
  private TTemplate create() {
    var moST = this.buST.getModelSymbolTable();
    var empty = moST.empty();
    var queue = this.buST.getTemplate();
    var maxDelay = this.buST.getMaxDelay();
    var toQueue = this.buST.toQueueBuffer();
    var toStateMachine = this.buST.toStateMachine();
    var elements = this.buST.elements();
    var tickets = this.buST.tickets();
    var clocks = this.buST.clocks();

    /* States of the buffer */

    // There are capacity+1 buffer states. Each buffer state n indicates
    // that buffer position n-1 is the highest buffer position that is
    // currently occupied.
    int statesNum = this.buST.getProperties().getNetworkCapacity()+1;
    @NonNull TState[] states = new @NonNull TState[statesNum];
    for (int i = 0; i < statesNum; i++) {
      TState state = new TState("Buffer" + i);
      queue.addState(state);
      states[i] = state;
      if (clocks != null && maxDelay != null) {
        for (int j = 0; j < i; j++)
          state.addInvariant(TClockUpperBound.leq(clocks.access(j), maxDelay));
      }
    }
    states[0].setInitial(true);

    // Additionally, there is a transient state for sending out a stimulus
    var bufferDeliverState = new TState("Deliver");
    bufferDeliverState.setCommitted(true);
    queue.addState(bufferDeliverState);

    /* Transitions of the buffer */

    // Transitions for inserting an event into the network
    // can't put anything into the full buffer (source == statesNum)
    for (int source = 0; source < statesNum; source++) {
      // Transitions for filling places

      // The target may either be the state itself, if there are free
      // places below the highest filled place or the next state
      for (int place = 0; place <= source && place < statesNum - 1; place++) {
        // We do not have to consider a transition that would fill the place
        // with the number source-1 as we already know that this is occupied.
        if (place == source - 1)
          continue;

        int target = (place < source ? source : source + 1);
        var transition = new TTransition(states[source], states[target]);
        for (int filled = 0; filled < place && filled < source - 1; filled++)
          transition.addGuard(TExpression.neq(elements.access(filled).access(moST.ID), empty));
        if (target == source)
          transition.addGuard(TExpression.eq(elements.access(place).access(moST.ID), empty));

        // Copy information from global variables (set by instance)
        transition.setSynchronisation(toQueue.receive());
        transition.addUpdate(buST.fromComm(source, place, target));

        queue.addTransition(transition);
      }
    }

    // Transition for buffer overflow (full == statesNum-1)
    if (!this.buST.getProperties().isBlocking()) {
      var bufferOverflowState = this.buST.bufferOverflow();
      var transition = new TTransition(states[statesNum-1], bufferOverflowState);
      transition.setSynchronisation(toQueue.receive());
      queue.addTransition(transition);
    }

    // Transitions for delivering an event from the network going to a transient state
    // can't deliver anything from the "empty" state (source == 0)
    // can't deliver anything to an object that does not accept behavioural features
    for (int source = 1; source < statesNum; source++) {
      for (int place = 0; place < source; place++) {
        var transition = new TTransition(states[source], bufferDeliverState);
        transition.addGuard(TExpression.neq(elements.access(place).access(moST.ID), empty));
        transition.addGuard(TExpression.eq(tickets.access(place), TExpression.intConst(0)));
        transition.setSynchronisation(toStateMachine.send());
        transition.addUpdate(buST.fromBuffer(source, place));
        queue.addTransition(transition);
      }
    }

    // Transitions for delivering an event from the buffer
    // returning from the transient state
    // can't return to the "full" state (target == statesNum)
    for (int target = 0; target < statesNum-1; target++) {
      var transition = new TTransition(bufferDeliverState, states[target]);
      for (int place = 0; place < statesNum-1; place++) {
        if (place >= target)
          transition.addGuard(TExpression.eq(elements.access(place).access(moST.ID), empty));
        if (place == target - 1)
          transition.addGuard(TExpression.neq(elements.access(place).access(moST.ID), empty));
      }
      queue.addTransition(transition);
    }

    return queue;
  }
  
  private static TFunction getFromCommFunctionDeclaration(int statesNum, SymbolTable quST) {
    var moST = quST.getModelSymbolTable();
    var comm = moST.comm();
    var elements = quST.elements();
    var emptyMessage = moST.emptyMessage();
    var clocks = quST.clocks();
    var tickets = quST.tickets();
    var placeVar = new TVariable("place"); var place = placeVar.access();
    var sourceVar = new TVariable("source"); var source = sourceVar.access();
    var targetVar = new TVariable("target"); var target = targetVar.access();
    var runningVar = new TVariable("i"); var running = runningVar.access();

    var statements = new ArrayList<TStatement>();

    // Put contents of communication variable into element
    statements.add(elements.access(place).assign(comm));
    statements.add(comm.assign(emptyMessage));

    // Update tickets
    statements.add(tickets.access(place).assign(0));
    statements.add(TLoop.loop(running.assign(0),
                              TExpression.relational(running, TOperator.LT, source),
                              TBlock.block(TConditional.ifthen(TExpression.and(TExpression.neq(running, place),
                                                                               TExpression.relational(tickets.access(running), TOperator.GEQ, tickets.access(place)),
                                                                               TExpression.eq(elements.access(place).access(moST.SENDER), elements.access(running).access(moST.SENDER))),
                                                               TBlock.block(tickets.access(place).assign(TExpression.arithmetical(tickets.access(running), TOperator.ADD, 1))))),
                              running.inc()));

    // Reset clocks
    if (clocks != null) {
      statements.add(clocks.access(place).reset());
      statements.add(TLoop.loop(running.assign(target),
                                TExpression.relational(running, TOperator.LT, TExpression.intConst(statesNum-1)),
                                TBlock.block(clocks.access(running).reset()),
                                running.inc()));
    }

    return TFunction.decl("fromComm", Arrays.asList(sourceVar, placeVar, targetVar), TBlock.block(Arrays.asList(runningVar), statements));
  }

  private static TFunction getFromBufferFunctionDeclaration(int statesNum, SymbolTable quST) {
    var moST = quST.getModelSymbolTable();
    var comm = moST.comm();
    var emptyMessage = moST.emptyMessage();
    var elements = quST.elements();
    var clocks = quST.clocks();
    var tickets = quST.tickets();
    var placeVar = new TVariable("place"); var place = placeVar.access();
    var sourceVar = new TVariable("source"); var source = sourceVar.access();
    var runningVar = new TVariable("i"); var running = runningVar.access();

    var statements = new ArrayList<TStatement>();

    // Put contents of element into the communication variable
    statements.add(comm.assign(elements.access(place)));

    // Normalise the tickets
    statements.add(tickets.access(place).assign(0));
    statements.add(TLoop.loop(running.assign(0),
                              TExpression.relational(running, TOperator.LT, source),
                              TBlock.block(TConditional.ifthen(TExpression.and(TExpression.neq(running, place),
                                                                               TExpression.eq(elements.access(place).access("sender"), elements.access(running).access("sender"))),
                                                               TBlock.block(tickets.access(running).dec()))),
                              running.inc()));

    // Reset element
    statements.add(elements.access(place).assign(emptyMessage));
    
    // Reset clocks
    if (clocks != null) {
      statements.add(clocks.access(place).reset());
      statements.add(TLoop.loop(running.assign(source),
                                TExpression.relational(running, TOperator.LT, TExpression.intConst(statesNum-1)),
                                TBlock.block(clocks.access(running).reset()),
                                running.inc()));
    }

    return TFunction.decl("fromBuffer", Arrays.asList(sourceVar, placeVar), TBlock.block(Arrays.asList(runningVar), statements));
  }

  public static void reconstruct(UObjectState uObjectState, TSystemState systemState, ModelSymbolTable moST) {
    new Buffer(moST).reconstruct(uObjectState, systemState);
  }

  private void reconstruct(UObjectState uObjectState, TSystemState systemState) {
    var moST = buST.getModelSymbolTable();
    var uObject = uObjectState.getObject();

    var bufferProcess = buST.getProcess(uObject);
    var bufferProcessState = systemState.getProcessState(bufferProcess);
    if (bufferProcessState == null) {
      util.Message.debug().info("No buffer process state for object `" + uObject.getName() + "' found.");
      return;
    }

    var bufferProcessStateState = bufferProcessState.getState();
    if (!this.buST.getProperties().isBlocking()) {
      if (bufferProcessStateState != null && bufferProcessStateState.equals(buST.bufferOverflow())) {
        uObjectState.getSystemState().getNetworkState().setOverflown(uObject);
        return;
      }
    }

    var uQueueState = uObjectState.getQueueState();
    var elements = buST.elements();
    // Cope for network of "size zero"
    int capacity = (moST.getProperties().getNetworkCapacity() == 0 ? 1 : moST.getProperties().getNetworkCapacity());
    for (int i = 0; i < capacity; i++) {
      var uMessage = Queue.reconstruct(uObject, bufferProcessState, elements.access(i), moST);
      if (uMessage != null)
        uQueueState.addExternalMessage(uMessage);
    }
  }
}
