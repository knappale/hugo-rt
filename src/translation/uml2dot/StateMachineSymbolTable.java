package translation.uml2dot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;

/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class StateMachineSymbolTable {
  private ModelSymbolTable modelSymbolTable;
  private UStateMachine uStateMachine;
  private Map<Object, String> names = new HashMap<>();
  private Set<String> identifiers = new HashSet<>();
  private int identifiersCounter = 0;

  /**
   * Create a new state machine symbol table.
   *
   * @param uStateMachine a (UML) state machine
   * @param modelSymbolTable model symbol table for the model surrounding <CODE>uStateMachine</CODE>
   */
  StateMachineSymbolTable(UStateMachine uStateMachine, ModelSymbolTable modelSymbolTable) {
    this.modelSymbolTable = modelSymbolTable;
    this.uStateMachine = uStateMachine;
    this.identifiers.add(modelSymbolTable.getGraphName(uStateMachine));
  }

  /**
   * Get a graph name for underlying UML state machine.
   *
   * @return unique graph name for underlying UML state machine
   */
  String getGraphName() {
    return modelSymbolTable.getGraphName(this.uStateMachine);
  }

  /**
   * Get a graph name for a UML (composite) state.
   *
   * @param uRegion (UML) region
   * @return unique graph name for {@code uRegion}
   */
  String getGraphName(URegion uRegion) {
    String graphName = this.names.get(uRegion);
    if (graphName != null)
      return graphName;

    graphName = uniqueIdentifier(this.getGraphName() + uRegion.getIdentifier());
    this.names.put(uRegion, graphName);
    return graphName;
  }

  /**
   * Get a graph name for a UML (composite) state.
   *
   * @param uState (UML composite) state
   * @return unique graph name for {@code uState}
   */
  String getGraphName(UState uState) {
    String graphName = this.names.get(uState);
    if (graphName != null)
      return graphName;

    graphName = uniqueIdentifier(this.getGraphName() + uState.getIdentifier().toString());
    this.names.put(uState, graphName);
    return graphName;
  }

  /**
   * Get a node name for a UML state.
   *
   * @param uVertex UML state
   * @return unique node name for {@code uVertex}
   */
  String getNodeName(UVertex uVertex) {
    String nodeName = this.names.get(uVertex);
    if (nodeName != null)
      return nodeName;
    nodeName = uniqueIdentifier(this.getGraphName() + uVertex.getIdentifier().toString());
    this.names.put(uVertex, nodeName);
    return nodeName;
  }

  /**
   * Uniquify a given identifier for (dot graph) local use.
   */
  private String uniqueIdentifier(String identifier) {
    char[] characters = identifier.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    String uniqueIdentifier = new String(characters).substring(0, Math.min(50, characters.length));
    if (uniqueIdentifier.equals("") || this.identifiers.contains(uniqueIdentifier)) {
      String tmpIdentifier;
      do {
        tmpIdentifier = uniqueIdentifier + this.identifiersCounter++;
      }
      while (this.identifiers.contains(tmpIdentifier));
      uniqueIdentifier = tmpIdentifier;
    }
    this.identifiers.add(uniqueIdentifier);
    return uniqueIdentifier;
  }
}
