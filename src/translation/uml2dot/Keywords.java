package translation.uml2dot;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;


/**
 * Dot keywords.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class Keywords {
  // Reserved keywords
  public static final @NonNull String DIGRAPH = "digraph";
  public static final @NonNull String SUBGRAPH = "subgraph";
  public static final @NonNull String NODE = "node";
  public static final @NonNull String EDGE = "edge";
  public static final @NonNull String CLUSTER = "cluster";

  private static @NonNull String[] keywords = {
    DIGRAPH, SUBGRAPH, NODE, EDGE, CLUSTER
  };

  public static boolean isKeyword(String word) {
    return Arrays.stream(keywords).anyMatch(keyword -> keyword.equals(word));
  }

  public static @NonNull Set<@NonNull String> getKeywords() {
    return new HashSet<>(Arrays.asList(keywords));
  }
}
