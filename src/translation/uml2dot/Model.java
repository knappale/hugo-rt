package translation.uml2dot;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UClass;
import uml.UModel;
import uml.statemachine.UStateMachine;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class Model {
  public static String translate(UModel uModel) {
    ModelSymbolTable symbolTable = new ModelSymbolTable(uModel);

    StringBuilder resultBuilder = new StringBuilder("");
    resultBuilder.append("digraph ");
    resultBuilder.append(symbolTable.getGraphName());
    resultBuilder.append(" {\n");
    resultBuilder.append("  graph [fontname=Helvetica];\n");
    resultBuilder.append("  node [fontname=Helvetica, shape=box];\n");
    resultBuilder.append("  edge [fontname=Helvetica];\n");
    resultBuilder.append("  compound=true;\n");
    resultBuilder.append("\n");

    Set<UStateMachine> uStateMachines = new HashSet<>();
    for (UClass uClass : uModel.getClasses()) {
      UStateMachine uStateMachine = uClass.getStateMachine();
      if (uStateMachine != null)
        uStateMachines.add(uStateMachine);
    }
    for (Iterator<UStateMachine> uStateMachinesIt = uStateMachines.iterator(); uStateMachinesIt.hasNext(); ) {
      UStateMachine uStateMachine = uStateMachinesIt.next();
      resultBuilder.append(StateMachine.translate(uStateMachine, symbolTable));
      if (uStateMachinesIt.hasNext())
        resultBuilder.append("\n");
    }

    resultBuilder.append("}\n");
    return resultBuilder.toString();
  }
}
