package translation.uml2dot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UModel;

/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class ModelSymbolTable {
  private UModel uModel;
  private Map<Object, String> names = new HashMap<>();
  private Set<String> identifiers = new HashSet<>();
  private int identifiersCounter = 0;

  /**
   * Create a new model symbol table.
   * 
   * @param uModel a UML model
   */
  ModelSymbolTable(UModel uModel) {
    this.uModel = uModel;
    this.identifiers.addAll(Keywords.getKeywords());
  }

  /**
   * Get a graph name for underlying UML model.
   * 
   * @return graph name for underlying UML model
   */
  String getGraphName() {
    String graphName = this.names.get(uModel);
    if (graphName != null)
      return graphName;

    graphName = uniqueIdentifier(uModel.getName());
    this.names.put(uModel, graphName);
    return graphName;    
  }

  /**
   * Get a graph name for a UML state machine.
   *
   * @param uStateMachine a (UML) state machine
   * @return unique graph name for {@code uStateMachine}
   */
  String getGraphName(uml.statemachine.UStateMachine uStateMachine) {
    String graphName = this.names.get(uStateMachine);
    if (graphName != null)
      return graphName;

    graphName = uniqueIdentifier(uStateMachine.getC1ass().getName());
    this.names.put(uStateMachine, graphName);
    return graphName;
  }

  /**
   * Uniquify a given identifier for (dot graph) local use.
   */
  private String uniqueIdentifier(String identifier) {
    char[] characters = identifier.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    String uniqueIdentifier = new String(characters).substring(0, Math.min(50, characters.length));
    if (uniqueIdentifier.equals("") || this.identifiers.contains(uniqueIdentifier)) {
      String tmpIdentifier;
      do {
        tmpIdentifier = uniqueIdentifier + this.identifiersCounter++;
      }
      while (this.identifiers.contains(tmpIdentifier));
      uniqueIdentifier = tmpIdentifier;
    }
    this.identifiers.add(uniqueIdentifier);
    return uniqueIdentifier;
  }
}
