package translation.uml2dot;

import java.util.Iterator;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UExpression;
import uml.statemachine.UEvent;
import uml.statemachine.UFinalState;
import uml.statemachine.UPseudoState;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UTransition;
import uml.statemachine.UVertex;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class StateMachine {
  private static boolean FULLLABELS = false;

  private UStateMachine uStateMachine;
  private StateMachineSymbolTable symbolTable;

  private StateMachine(UStateMachine uStateMachine, ModelSymbolTable modelSymbolTable) {
    this.uStateMachine = uStateMachine;
    this.symbolTable = new StateMachineSymbolTable(uStateMachine, modelSymbolTable);
  }

  public static String translate(UStateMachine uStateMachine, ModelSymbolTable modelSymbolTable) {
    return new StateMachine(uStateMachine, modelSymbolTable).translate();
  }

  private String translate() {
    String indent = "  ";

    StringBuilder resultBuilder = new StringBuilder("");
    resultBuilder.append(indent);
    resultBuilder.append(Keywords.SUBGRAPH);
    resultBuilder.append(" ");
    resultBuilder.append(Keywords.CLUSTER);
    resultBuilder.append(this.symbolTable.getGraphName());
    resultBuilder.append(" {\n");
    resultBuilder.append(indent);
    resultBuilder.append("  label=\"");
    resultBuilder.append(this.uStateMachine.getC1ass().getName());
    resultBuilder.append("\";\n");
    for (URegion uRegion : this.uStateMachine.getRegions()) 
      resultBuilder.append(this.region(uRegion, indent + "  "));
    for (UTransition uTransition : this.uStateMachine.getTransitions())
      resultBuilder.append(transition(uTransition, indent + "  "));
    resultBuilder.append(indent);
    resultBuilder.append("}\n");
    return resultBuilder.toString();
  }
  
  private String region(URegion uRegion, String indent) {
    StringBuilder resultBuilder = new StringBuilder("");
    resultBuilder.append(indent);

    resultBuilder.append(Keywords.SUBGRAPH);
    resultBuilder.append(" ");
    resultBuilder.append(Keywords.CLUSTER);
    resultBuilder.append(this.symbolTable.getGraphName(uRegion));
    resultBuilder.append(" {\n");
    resultBuilder.append(indent);
    resultBuilder.append("  label=\"");
    resultBuilder.append(regionLabel(uRegion));
    resultBuilder.append("\";\n");
    for (UVertex uVertex : uRegion.getVertices())
      resultBuilder.append(this.vertex(uVertex, indent + "  "));
    resultBuilder.append(indent);
    resultBuilder.append("}\n");
    return resultBuilder.toString();
  }

  private String vertex(UVertex uVertex, String indent) {
    return uVertex.new Cases<String>().
        pseudoState(uPseudoState -> this.pseudoState(uPseudoState, indent)).
        finalState(uFinalState -> this.finalState(uFinalState, indent)).
        state(uState -> this.state(uState, indent)).
        apply();
  }

  private String regionLabel(URegion uRegion) {
    StringBuilder resultBuilder = new StringBuilder("");

    resultBuilder.append(uRegion.getName());
    resultBuilder.append("\\l");

    return resultBuilder.toString();
  }

  private String pseudoState(UPseudoState uPseudoState, String indent) {
    StringBuilder resultBuilder = new StringBuilder("");
    resultBuilder.append(indent);

    UPseudoState.Kind kind = uPseudoState.getKind();
    resultBuilder.append(symbolTable.getNodeName(uPseudoState));
    resultBuilder.append(" ");
    switch (kind) {
      case INITIAL:
        resultBuilder.append("[shape=circle, fillcolor=black, style=filled, height=.1, label=\"\"];\n");
        break;
      case FORK:
      case JOIN:
        resultBuilder.append("[shape=box, fillcolor=black, style=filled, height=.1, label=\"\"];\n");
        break;
      case JUNCTION:
        resultBuilder.append("[shape=circle, fillcolor=black, style=filled, height=.2, label=\"\"];\n");
        break;
      case CHOICE:
        resultBuilder.append("[shape=circle, height=.2, label=\"\"];\n");
        break;
      //$CASES-OMITTED$
      default:
        resultBuilder.append("[label=\"\"];\n");
    }
    return resultBuilder.toString();
  }

  private String finalState(UFinalState uState, String indent) {
    StringBuilder resultBuilder = new StringBuilder("");
    resultBuilder.append(indent);

    resultBuilder.append(symbolTable.getNodeName(uState));
    resultBuilder.append(" ");
    resultBuilder.append("[shape=doublecircle, fillcolor=black, style=filled, height=.2, label=\"\"];\n");

    return resultBuilder.toString();
  }

  private String state(UState uState, String indent) {
    StringBuilder resultBuilder = new StringBuilder("");
    resultBuilder.append(indent);

    if (uState.getRegions().size() == 0) {
      resultBuilder.append(symbolTable.getNodeName(uState));
      resultBuilder.append(" [label=\"");
      resultBuilder.append(stateLabel(uState));
      resultBuilder.append("\"]");
      resultBuilder.append(";\n");
      return resultBuilder.toString();
    }

    resultBuilder.append("subgraph cluster");
    resultBuilder.append(symbolTable.getGraphName(uState));
    resultBuilder.append(" {\n");
    resultBuilder.append(indent);
    resultBuilder.append("  label=\"");
    resultBuilder.append(stateLabel(uState));
    resultBuilder.append("\";\n");
    for (URegion uRegion : uState.getRegions())
      resultBuilder.append(region(uRegion, indent + "  "));
    resultBuilder.append(indent);
    resultBuilder.append("}\n");
    return resultBuilder.toString();
  }

  private String stateLabel(UState uState) {
    StringBuilder resultBuilder = new StringBuilder("");

    resultBuilder.append(uState.getName());
    resultBuilder.append("\\l");
    if (FULLLABELS) {
      if (!uState.getDeferrableEvents().isEmpty()) {
        resultBuilder.append("defer / ");
        for (Iterator<UEvent> deferrablesIt = uState.getDeferrableEvents().iterator(); deferrablesIt.hasNext(); ) {
          UEvent deferrable = deferrablesIt.next();
          resultBuilder.append(deferrable.declaration());
          if (deferrablesIt.hasNext())
            resultBuilder.append(", ");
        }
        resultBuilder.append("\\l");
      }
      if (uState.hasEntryAction()) {
        resultBuilder.append("entry / ");
        resultBuilder.append(uState.getEntryAction());
        resultBuilder.append("\\l");
      }
      if (uState.hasExitAction()) {
        resultBuilder.append("exit ");
        resultBuilder.append(uState.getExitAction());
        resultBuilder.append("\\l");
      }
    }

    return resultBuilder.toString();
  }

  private String transition(UTransition uTransition, String indent) {
    StringBuilder resultBuilder = new StringBuilder("");

    resultBuilder.append(indent);
    resultBuilder.append(symbolTable.getNodeName(uTransition.getSource()));
    resultBuilder.append(" -> ");
    resultBuilder.append(symbolTable.getNodeName(uTransition.getTarget()));
    resultBuilder.append(" [");
    UVertex source = uTransition.getSource();
    if (source instanceof UState && ((UState)source).getRegions().size() > 0) {
      resultBuilder.append("ltail=cluster");
      resultBuilder.append(symbolTable.getGraphName((UState)source));
      resultBuilder.append(", ");
    }
    UVertex target = uTransition.getTarget();
    if (target instanceof UState && ((UState)target).getRegions().size() > 0) {
      resultBuilder.append("lhead=cluster");
      resultBuilder.append(symbolTable.getGraphName((UState)target));
      resultBuilder.append(", ");
    }
    resultBuilder.append("label=\"");
    resultBuilder.append(transitionLabel(uTransition));
    resultBuilder.append("\"];\n");

    return resultBuilder.toString();
  }

  private String transitionLabel(UTransition uTransition) {
    StringBuilder resultBuilder = new StringBuilder("");
    if (FULLLABELS) {
      if (!uTransition.getTrigger().isCompletion() || uTransition.getTrigger().isWait()) {
        resultBuilder.append(uTransition.getTrigger().declaration());
        resultBuilder.append("\\l");        
      }
      if (!(uTransition.getGuard().equals(UExpression.trueConst()))) {
        resultBuilder.append("[");
        resultBuilder.append(uTransition.getGuard().toString());
        resultBuilder.append("]\\l");
      }
      if (!(uTransition.getEffect().isSkip())) {
        resultBuilder.append("/");
        resultBuilder.append(uTransition.getEffect().toString());
        resultBuilder.append("\\l");
      }
    }
    return resultBuilder.toString();
  }
}
