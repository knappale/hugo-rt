package translation.uml2systemc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import cpp.CArrayAttribute;
import cpp.CAttribute;
import cpp.CBlock;
import cpp.CBlockStatement;
import cpp.CBranch;
import cpp.CCode;
import cpp.CFor;
import cpp.CIfElse;
import cpp.CIfs;
import cpp.CKeywords;
import cpp.CMethod;
import cpp.CParameter;
import cpp.CSequence;
import cpp.CStatement;
import cpp.systemc.SCStep;
import cpp.systemc.SCRestart;
import cpp.systemc.SCConstructor;
import cpp.systemc.SCModule;
import uml.UAction;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import uml.USlot;
import uml.smile.SBranch;
import uml.smile.SCommand;
import uml.smile.SGuard;
import uml.smile.SLiteral;
import uml.smile.SMachine;
import uml.smile.SStatement;
import uml.smile.SValue;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.semantics.UTimerTree;

import static util.Objects.requireNonNull;


/**
 * Translate Smile machine into SystemC code.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class SmileTranslator {
  static final boolean DEBUG = true;

  /**
   * Translate a Smile machine.
   * 
   * The state machine method is added to the SystemC Module table.
   * 
   * @param uMachine a Smile machine
   * @param symbolTable a class symbol table
   */
  public static void translate(SMachine uMachine, ClassifierSymbolTable symbolTable) {
    UStateMachine uStateMachine = uMachine.getStateMachine();

    SCModule cModule = symbolTable.getCModule();
    cModule.addPreprocessorDefinition("DEFERRED_QUEUE_SIZE", symbolTable.getModelSymbolTable().getProperties().getDeferredQueueCapacity());
    cModule.addPreprocessorDefinition("INTERNAL_QUEUE_SIZE", symbolTable.getModelSymbolTable().getProperties().getInternalQueueCapacity());
    cModule.addPreprocessorDefinition("EXTERNAL_QUEUE_SIZE", symbolTable.getModelSymbolTable().getProperties().getExternalQueueCapacity());
    cModule.addPreprocessorDefinition("MAX_PARAMETER_SIZE",
        symbolTable.getModelSymbolTable().getModel().getClasses().stream().flatMap(uClass -> uClass.getBehaviourals().stream().
                                                                               map(uBehavioural -> uBehavioural.getParameters().size())).reduce(0, (m1, m2) -> Integer.max(m1, m2)));

    // State machine method
    CMethod runMethod = new CMethod("machine" + cModule.getName().toLowerCase(), CKeywords.VOID, new ArrayList<>(), CKeywords.PRIVATE);
    translateStatement(uMachine, uMachine.getStatement(), symbolTable, runMethod.getBody());

    // Internal queues and variables
    cModule.addAttribute(new CAttribute("clk", "sc_in<bool>", "", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("internal", "queue_internal", "queue_internal()", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("completed", "queue_internal", "queue_internal()", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("deferred", "queue_deferred", "queue_deferred()", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("external", "queue_external", "queue_external()", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("outgoing", "queue_external", "queue_external()", CKeywords.PRIVATE));

    // Some useful things
    cModule.addAttribute(new CAttribute("chosen", "bool", "false", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("blocked", "bool", "false", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("blockedlabel", "int", "false", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("isinitialized", "bool", "false", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("selected", "bool", "false", CKeywords.PRIVATE));
   
    cModule.addAttribute(new CAttribute("current", "event", "", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("jumplabel", "int", "1", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("sc_id", "int", "", CKeywords.PRIVATE));
    if (UTimerTree.create(uStateMachine).getMaxTimersNumber() > 0) {
      @NonNull List<String> timerInit= new ArrayList<>();
      for (int i=0; i < UTimerTree.create(uStateMachine).getMaxTimersNumber();i++) 
        timerInit.add("false"); 
      cModule.addAttribute(new CArrayAttribute("istimedout", "sc_signal<bool>", timerInit, CKeywords.PRIVATE, UTimerTree.create(uStateMachine).getMaxTimersNumber()));
      cModule.addAttribute(new CArrayAttribute("timer", "sc_event",CKeywords.PRIVATE, UTimerTree.create(uStateMachine).getMaxTimersNumber()));
      cModule.addAttribute(new CAttribute("timerselect", "int", "-1", CKeywords.PRIVATE));
    }
    
    // Slots to the controller
    cModule.addAttribute(new CAttribute("input", "sc_in<event>", "", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("output", "sc_out<event>", "", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("outbusy", "sc_inout<bool>", "", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("inbusy", "sc_inout<bool>", "", CKeywords.PRIVATE));
    symbolTable.createStateAndEventConstants();
    
    // Setter for var sc_id
    List<CParameter> idParams =  new ArrayList<>();
    idParams.add(new CParameter("__sc_id","int"));
    CMethod setID = new CMethod("setsc_id", CKeywords.VOID, idParams, CKeywords.PRIVATE);
    setID.addBlockStatement(new CCode("sc_id=__sc_id;"));
    cModule.addMethod(setID);
    
    // Method answers the question SMILE: iscompleted()
    List<CParameter> iscompletedParameters = new ArrayList<>();
    iscompletedParameters.add(new CParameter("e", "event"));
    CMethod iscompletedMethod = new CMethod("iscompleted", "inline " + CKeywords.BOOL, iscompletedParameters, CKeywords.PRIVATE);
    iscompletedMethod.addBlockStatement(new CCode("bool b= completed.contains(event_int(e.name,e.type));"));
    if(DEBUG) iscompletedMethod.addBlockStatement(new CCode(syscDebug(cModule.getName() + ":\" << e <<\" iscompleted==\" << ((b)?\"true\":\"false\") <<\" ")));
    iscompletedMethod.addBlockStatement(new CCode("return b;"));
    cModule.addMethod(iscompletedMethod);
    
    // Method answers the question SMILE: match(event e)
    List<CParameter> matchParameters = new ArrayList<>();
    matchParameters.add(new CParameter("e", "event"));
    CMethod matchMethod = new CMethod("match", "inline " + CKeywords.BOOL, matchParameters, CKeywords.PRIVATE);
    matchMethod.addBlockStatement(new CCode("bool b= current.name==e.name && current.type==e.type;"));
    if(DEBUG) matchMethod.addBlockStatement(new CCode(syscDebug(cModule.getName() + ": match(\" << e <<\") returns \" << ((b)?\"true\":\"false\") <<\" ")));
    matchMethod.addBlockStatement(new CCode("return b;"));
    cModule.addMethod(matchMethod);
    
    // Timer methods
    for (int i=0; i < UTimerTree.create(uStateMachine).getMaxTimersNumber();i++) {
      CMethod timer= new CMethod("timedout_"+i, CKeywords.VOID, new ArrayList<>(),CKeywords.PRIVATE);
      timer.addBlockStatement(new CCode("istimedout["+i+"]=true;"));
      if (DEBUG) timer.addBlockStatement(new CCode(syscDebug(cModule.getName()+": Timer "+i+" timed out!")));
      cModule.addMethod(timer);
    }    

    cModule.addMethod(runMethod);
    CBlock bl = cModule.getConstructors().get(0).getInitBlock();

    // Initialisation of attributes in the constructor
    for (CAttribute a : cModule.getAttributes()) {
      if (a instanceof CArrayAttribute) {
        for (int i=0;i<a.getInitialValues().size();i++) {
          bl.addBlockStatement(new CCode(a.getName()+"["+i+"] = "+a.getInitialValues().get(i)+";"));
        }
      }
      else
      {
        if (a.getInitialValue()!=null && !"".equals(a.getInitialValue()))
          bl.addBlockStatement(new CCode(a.getName()+" = "+a.getInitialValue()+";"));
      }
    }
    
    // Constructor
    SCConstructor scConstructor = cModule.getConstructors().get(0);
    scConstructor.addBlockStatement(new CCode("SC_METHOD(machine" + cModule.getName().toLowerCase() + ");\n"));
    scConstructor.addBlockStatement(new CCode("sensitive_pos <<clk;"));
    scConstructor.addBlockStatement(new CCode("dont_initialize();"));
    for (int i = 0; i < UTimerTree.create(uStateMachine).getMaxTimersNumber(); i++) {
      scConstructor.addBlockStatement(new CCode("SC_METHOD(timedout_"+i+");\n"));
      scConstructor.addBlockStatement(new CCode("sensitive << timer["+i+"];"));
      scConstructor.addBlockStatement(new CCode("dont_initialize();"));
    }
  }

  private static CStatement translateStatement(SMachine sMachine, SStatement statement, ClassifierSymbolTable symbolTable, cpp.CBlock block) {
    return new StatementTranslator(sMachine, symbolTable, block).translate(statement);
  }

  /**
   * Translator for Smile statements.
   * 
   * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
   */
  private static class StatementTranslator implements SStatement.Visitor<@Nullable Void> {
    private static final String BLOCKEDLABELINT = "blockedlabel";
    private SMachine sMachine;
    private ClassifierSymbolTable symbolTable;
    private SCStep current;
    private CBlock result;
    private Stack<String> blockLabel=new Stack<>();
    private Stack<String> destLabel = new Stack<>();
    private Stack<String> breakLabel=new Stack<>();
    private String firstLabel;
    private List<String> targetLabels = new ArrayList<>();
    private List<SCStep> cycles=new ArrayList<>();
    private Map<String,SCStep> labelsMap=new HashMap<>();

    public StatementTranslator(SMachine sMachine, ClassifierSymbolTable symbolTable, CBlock block) {
      this.sMachine = sMachine;
      this.symbolTable = symbolTable;
      this.result = block;
      this.current = new SCStep();
      this.firstLabel = symbolTable.getLabel();
      this.current.addLabel(firstLabel);
      this.targetLabels.add(firstLabel);
    }

    /**
     * Translate a Smile statement.
     * 
     * @param sStatement a Smile statement
     */
    public CBlock translate(SStatement sStatement) {
      createHeader();

      sStatement.accept(this);
      cycleResult();
      optimizeAtomicSteps();

      createFooter();
      return result;
    }

    private void createHeader() {
      result.addBlockStatement(new CIfElse("inbusy && (!external.isfull())",
                                   new CBlock(new CCode("event e = input.read();"),
                                              new CIfElse("e.type == event::ACK",
                                                  new CBlock(new CCode("jumplabel = blockedlabel;"),
                                                             new CCode("blocked = false;")),
                                                  new CBlock(new CCode("external.enqueue(e);"),
                                                             new CCode(syscDebug(symbolTable.getCModule().getName() + ": received event \" << e << \"")))),
                                              new CCode("inbusy=false;")),
                                   null));
      result.addBlockStatement(new CCode("if (outbusy) return;"));
    }

    private void createFooter() {
      result.addBlockStatement(new CIfElse("!outbusy && (!outgoing.isempty())",
                                   new CBlock(new CCode("event e = outgoing.dequeue();"),
                                              new CCode("output = e;"),
                                              new CCode("outbusy = true;"),
                                              new CCode(syscDebug(symbolTable.getCModule().getName() + ": sending event \"<<e<<\"")),
                                              new CIfElse("e.type == event::CALL",
                                                  new CBlock(new CCode("blocked = true;"),
                                                             new CCode(syscDebug(symbolTable.getCModule().getName() + ": wait for acknowledge  \"<<e<<\""))),
                                                  null)),
                                   null));
    }

    private void optimizeAtomicSteps() {
      List<SCStep> resultList=new ArrayList<>();
      for (SCStep cycle: cycles) {
        boolean isReachable=false;
        for (String label: cycle.getJumps()) 
          if (targetLabels.contains(label)) 
            isReachable=true;
        if (isReachable)
          resultList.add(cycle);  
      }
      for (SCStep cycle : resultList)
        result.addBlockStatement(cycle);
    }

    public void result(CBlockStatement s) {
      current.addStatement(s.asStatement());
    }
    
    public void cycleResult() {
      cycles.add(current);
      targetLabels.add(requireNonNull(current.getTarget()));
      for (String l: current.getJumps()) {
        this.labelsMap.put(l,current);
      }
      current=new SCStep();
    }
    
    @Override
    public @Nullable Void onSkip() {
      result(new CSequence());
      return null;
    }

    @Override
    public @Nullable Void onAtomic(SStatement statement) {
      statement.accept(this);
      return null;
    }

    @Override
    public @Nullable Void onSequential(SStatement sLeft, SStatement sRight) {
      sLeft.accept(this);
      sRight.accept(this);
      return null;
    }
    
    @Override
    public @Nullable Void onChoice(List<SBranch> sBranches, @Nullable SStatement sElseStm) {
      String label=symbolTable.getChoiceBranchLabel();
      String afterlabel=symbolTable.getLabel();
      current.setTarget(label);
      cycleResult();
      blockLabel.push(label);
      destLabel.push(afterlabel);
      for (SBranch branch : sBranches)
        branch.accept(this);
      if (sElseStm!=null) {
        result(new CCode("// else stm"));
        current.addLabel(blockLabel.peek());
        current.setTarget(destLabel.peek());
        sElseStm.accept(this);
        cycleResult();
      }
      blockLabel.pop();
      destLabel.pop();
      current.addLabel(afterlabel);
      return null;
    }

    /**
     * Translate a Smile <CODE>do</CODE> statement.
     * 
     * The following code will be generated:
     * 
     * <PRE>
     * 
     * bsplabel: while (true) { if (branch1.condition) { ... continue bsplabel; }
     * if (branch2.condition2) { ... continue bsplabel; } else ... continue
     * bsplabel; }
     * 
     * </PRE>
     */
    @Override
    public @Nullable Void onLoop(List<SBranch> sBranches, @Nullable SStatement sElseStm) {
      if (sElseStm == null)
        sElseStm = SCommand.skip();

      String labelStart = symbolTable.getLoopLabel();
      String label = symbolTable.getBranchLabel(labelStart);
      String afterlabel = symbolTable.getLabel();
      current.setTarget(labelStart);
      cycleResult();

      current.addLabel(labelStart);
      current.setTarget(label);
      cycleResult();

      blockLabel.push(label);
      destLabel.push(labelStart);
      breakLabel.push(afterlabel);
      current = new SCStep();
      for (SBranch branch : sBranches)
        branch.accept(this);

      result(new CCode("// else stm"));
      current.addLabel(blockLabel.peek());
      current.setTarget(destLabel.peek());
      sElseStm.accept(this);
      cycleResult();
      blockLabel.pop();
      destLabel.pop();
      breakLabel.pop();
      current.addLabel(afterlabel);
      return null;
    }

    /**
     * Translates a Smile branch in an <CODE>if</CODE> or <CODE>do</CODE>
     * statement.
     * 
     * <PRE>
     * if (branchStm.condition) { ... [continue|break] mylabel; }
     * </PRE>
     */
    @Override
    public @Nullable Void onBranch(SGuard sGuard, SStatement sStatement) {
      current.setGuard(translateExpression(sGuard, this.symbolTable));
      current.addLabel(blockLabel.peek());
      sStatement.accept(this);
      current.setTarget(destLabel.peek());
      cycleResult();
      return null;
    }

    @Override
    public @Nullable Void onBreak() {
      String oldTarget=current.getTarget();
      current.setTarget(breakLabel.peek());
      cycleResult();
      String label=symbolTable.getLabel();
      current.addLabel(label);
      current.setTarget(oldTarget);
      return null;
    }

    @Override
    public @Nullable Void onAssignment(SVariable sVariable, SValue sValue) {
      result(new CCode(symbolTable.getVariableName(sVariable) + " = " + translateValue(sValue, this.symbolTable) + ";"));
      return null;
    }

    @Override
    public @Nullable Void onAssignment(SVariable sVariable, SVariable otherSVariable) {
      result(new CCode(symbolTable.getVariableName(sVariable) + " = " + symbolTable.getVariableName(otherSVariable) + ";"));
      return null;
    }

    @Override
    public @Nullable Void onExternal(UAction external, UContext context) {
      CSequence resultSequence = UMLTranslator.translateAction(external, context, symbolTable);
      CSequence res = new CSequence();
      for (CStatement statement : resultSequence.getStatements()) {
        if (statement instanceof SCRestart) {
          String label=symbolTable.getLabel();
          result(res);
          result(new CCode(BLOCKEDLABELINT+"="+label+";"));
          current.setTarget("-1");
          cycleResult();
          current.addLabel(label);
          targetLabels.add(label);
          res=new CSequence();
        } else {
          res.addBlockStatement(statement);
        }
      }
      result(res);
      return null;
    }

    @Override
    public @Nullable Void onSuccess() {
      if (DEBUG) result(new CCode(syscDebug("Normal termination of statemachine"+symbolTable.getCModule().getName()+" (id=\" << sc_id <<\")")));
      result(new CCode("jumplabel=-1;"));
      result(new CCode("return;"));
      return null;
    }

    @Override
    public @Nullable Void onFail() {
      if (DEBUG) result(new CCode(syscDebug("Abnormal termination of statemachine"+symbolTable.getCModule().getName()+" (id=\" << sc_id <<\")")));
      result(new CCode("jumplabel = -1;"));
      result(new CCode("return;"));
      return null;
    }

    @Override
    public @Nullable Void onAwait(SGuard sExpression) {
      String debug = DEBUG ? syscDebug("Abnormal termination of statemachine"+symbolTable.getCModule().getName()+" (id = \" << sc_id <<\")") : "";
      result(new CCode("if (" + translateExpression(SGuard.neg(sExpression), this.symbolTable) + ") { " + debug + " jumplabel = -1; return; }"));
      return null;
    }

    @Override
    public @Nullable Void onInitialisation() {
      return null;
    }

    @Override
    public @Nullable Void onFetch(Set<UState> waitStates) {
      if (UTimerTree.create(sMachine.getStateMachine()).getMaxTimersNumber() > 0)
        result(new CCode("timerselect=-1;"));

      List<CBranch> cMain = new ArrayList<>();
      CBranch b1;
      int timerNumbers = UTimerTree.create(sMachine.getStateMachine()).getMaxTimersNumber();
      b1 = new CBranch("!internal.isempty()");
      if(DEBUG) b1.addBlockStatement(debugFetch());
      b1.addBlockStatement(new CCode("event_int e=internal.dequeue();"));
      b1.addBlockStatement(new CCode("current=event(e.name,e.type);"));
      if(DEBUG)b1.addBlockStatement(new CCode(syscDebug(this.symbolTable.getCModule().getName() + " fetched internal: \" << current << \"")));
      cMain.add(b1);

      b1 = new CBranch("chosen && (!deferred.isempty())");
      if(DEBUG) b1.addBlockStatement(debugFetch());
      b1.addBlockStatement(new CCode("current=deferred.dequeue();"));
      if(DEBUG)b1.addBlockStatement(new CCode(syscDebug(this.symbolTable.getCModule().getName() + " fetched deferred: \" << current << \"")));
      cMain.add(b1);

      b1 = new CBranch("!external.isempty()");
      if(DEBUG) b1.addBlockStatement(debugFetch());
      b1.addBlockStatement(new CCode("current=external.dequeue();"));
      if(DEBUG)b1.addBlockStatement(new CCode(syscDebug(this.symbolTable.getCModule().getName() + " fetched external: \" << current << \"")));
      cMain.add(b1);
      if (timerNumbers > 0) {
        for (int i=0;i < timerNumbers;i++) {
          b1= new CBranch("istimedout["+i+"]==true");
          if(DEBUG) b1.addBlockStatement(debugFetch());
          b1.addBlockStatement(new CCode("current=event();"));
          b1.addBlockStatement(new CCode("timerselect="+i+";"));
          b1.addBlockStatement(new CCode("selected=true;"));
          if(DEBUG)b1.addBlockStatement(new CCode(syscDebug(this.symbolTable.getCModule().getName() + " fetched timer event: Timer: \" << timerselect << \"")));
          cMain.add(b1);
        }
      }

      b1 = new CBranch();
      b1.addBlockStatement(new CCode("current=event();"));
      cMain.add(b1);
      result(new CIfs(cMain, null));
      return null;
    }

    private CSequence debugFetch() {
      CSequence seq = new CSequence();
      seq.addBlockStatement(new CCode(syscDebug(this.symbolTable.getCModule().getName() + " external: \" << external << \"")));
      seq.addBlockStatement(new CCode(syscDebug(this.symbolTable.getCModule().getName() + " deferred: \" << deferred << \"")));
      seq.addBlockStatement(new CCode(syscDebug(this.symbolTable.getCModule().getName() + " internal: \" << internal << \"")));
      seq.addBlockStatement(new CCode(syscDebug(this.symbolTable.getCModule().getName() + " current: \" << current << \"")));
      return seq;
    }

    @Override
    public @Nullable Void onAcknowledge() {
      CBlock bl = new CBlock();
      bl.addBlockStatement(new CCode("outgoing.enqueue(event(-1,event::ACK,current.sender));"));
      result(bl);
      if (DEBUG) result(new CCode(syscDebug(symbolTable.getClassifier().getName() + ": acknowledging call event")));
      return null;
    }

    @Override
    public @Nullable Void onDefer() {
      result(new CCode("deferred.enqueue(current);"));
      result(new CCode("chosen=false;"));
      return null;
    }

    @Override
    public @Nullable Void onChosen() {
      result(new CCode("chosen=true;"));
      return null;
    }

    @Override
    public @Nullable Void onComplete(boolean keep, UState state) {
      result(new CCode("internal.enqueue(event_int(" + symbolTable.getStateName(state) + ",event::COMPLETION));"));
      if (keep)
        result(new CCode("completed.enqueue(event_int(" + symbolTable.getStateName(state) + ",event::COMPLETION));"));
      return null;
    }

    @Override
    public @Nullable Void onUncomplete(UState state) {
      result(new CCode("internal.removeall(event_int(" + symbolTable.getStateName(state) + ",event::COMPLETION));"));
      result(new CCode("completed.removeall(event_int(" + symbolTable.getStateName(state) + ",event::COMPLETION));"));
      return null;
    }

    @Override
    public @Nullable Void onStartTimer(boolean keep, UEvent event) {
      String timelow = UMLTranslator.translateExpression(event.getTimeLowExpression(), event.getTimeContext(), symbolTable);
      // String timehigh = UMLTranslator.translateExpression(event.getTimeHighExpression(), event.getTimeContext(), symbolTable);
      String timer = "" + UTimerTree.create(this.sMachine.getStateMachine()).getTimerNumber(event);
      result(new CCode("istimedout["+timer+"]=false;"));
      
      result(new CCode("timer["+timer+"].notify(("+(timelow)+")-1,SC_MS);"));
      if (DEBUG) result(new CCode(syscDebug(symbolTable.getCModule().getName()+": Starting timer \"<<" +timelow + " << \"")));
      return null;
    }

    @Override
    public @Nullable Void onStopTimer(UEvent event) {
      String timer = "" + UTimerTree.create(this.sMachine.getStateMachine()).getTimerNumber(event);
      result(new CCode("istimedout["+timer+"]=false;"));
      result(new CCode("timer["+timer+"].cancel();"));
      return null;
    }
  }

  /**
   * Translator for Smile expressions.
   * 
   * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
   */
  private static class ExpressionTranslator implements SGuard.Visitor<String>, SLiteral.Visitor<String> {
    private ClassifierSymbolTable symbolTable;

    ExpressionTranslator(ClassifierSymbolTable symbolTable) {
      this.symbolTable = symbolTable;
    }

    /**
     * Translate a Smile expression.
     * 
     * @param sExpression a Smile expression
     * @return Java code string representation of {@code sExpression}
     */
    public String translate(SGuard sExpression) {
      return sExpression.accept(this);
    }

    @Override
    public String onBooleanConstant(boolean booleanConstant) {
      return booleanConstant ? "true" : "false";
    }

    @Override
    public String onEq(boolean positive, SVariable sVariable, Set<SValue> sValues) {
      if (sValues.isEmpty())
        return positive ? "false" : "true";

      Function<SValue, String> eqFun = sValue -> this.symbolTable.getVariableName(sVariable) + " == " + translateValue(sValue, this.symbolTable);

      String eq = sValues.size() == 1 ? requireNonNull(eqFun.apply(sValues.iterator().next()))
                                      : "(" + sValues.stream().map(sValue -> eqFun.apply(sValue)).reduce("", (s1, s2) -> s1 + " || " + s2) + ")";
      return positive ? eq : "!(" + eq + ")";
    }

    @Override
    public String onMatch(boolean positive, Set<UEvent> uEvents) {
      if (uEvents.isEmpty())
        return positive ? "false" : "true";

      Function<UEvent, String> matchFun = uEvent -> uEvent.new Cases<String>().
          signal(uSignal -> "match(event(" + symbolTable.getEventName(uEvent) + ", event::SIGNAL))").
          call(uOperation -> "match(event(" + symbolTable.getEventName(uEvent) + ", event::CALL))").
          completion(uStates -> "match(event(" + symbolTable.getStateName(uStates.iterator().next()) + ", event::COMPLETION))").
          time((uState, lower, upper) -> "timerselect == " + symbolTable.getEventName(uEvent)).
          otherwise(() -> "").
          apply();

      String match = uEvents.size() == 1 ? requireNonNull(matchFun.apply(uEvents.iterator().next()))
                                         : "(" + uEvents.stream().map(uEvent -> matchFun.apply(uEvent)).reduce("", (s1, s2) -> s1 + " || " + s2) + ")";
      return positive ? match : "!(" + match + ")";
    }

    @Override
    public String onIsCompleted(boolean positive, UState uState) {
      String isCompleted = "this->iscompleted(event(" + symbolTable.getStateName(uState) + ", event::COMPLETION))";
      return positive ? isCompleted : "!(" + isCompleted + ")";
    }

    @Override
    public String onIsTimedOut(boolean positive, UEvent uEvent) {
      String isTimedOut = "istimedout[" + this.symbolTable.getTimerNumber(uEvent) + "]";
      return positive ? isTimedOut : "!(" + isTimedOut + ")";
    }

    @Override
    public String onExternal(boolean positive, UExpression uExpression, UContext uContext) {
      String external = UMLTranslator.translateExpression(uExpression, uContext, symbolTable);
      return positive ? external : "!(" + external + ")";
    }

    @Override
    public String onLiteral(SLiteral literal) {
      return literal.accept(this);
    }

    @Override
    public String onAnd(SGuard left, SGuard right) {
      StringBuilder resultBuilder = new StringBuilder("(");
      resultBuilder.append(translate(left));
      resultBuilder.append(" && ");
      resultBuilder.append(translate(right));
      return resultBuilder.append(")").toString();
    }

    @Override
    public String onOr(SGuard left, SGuard right) {
      StringBuilder resultBuilder = new StringBuilder("(");
      resultBuilder.append(translate(left));
      resultBuilder.append(" || ");
      resultBuilder.append(translate(right));
      return resultBuilder.append(")").toString();
    }
  }

  static String translateExpression(SGuard sExpression, ClassifierSymbolTable symbolTable) {
    return new ExpressionTranslator(symbolTable).translate(sExpression);
  }

  private static String translateValue(SValue sValue, ClassifierSymbolTable symbolTable) {
    return sValue.new Cases<String>().booleanConstant(booleanConstant -> booleanConstant ? "true" : "false").
                                      constant(sConstant -> symbolTable.getConstantName(sConstant)).
                                      apply();
  }

  public static SCModule createController(ModelSymbolTable symbolTable) {
    Map<UObject, Integer> objects = new HashMap<>();
    Map<String, Integer> objmap = new HashMap<> ();
    int objectsCount = 0;
    //objects get unique number
    for (UObject o : symbolTable.getCollaboration().getObjects()) {
      objects.put(o, objectsCount);
      objmap.put(o.getName(),objectsCount++);
    }
    SCModule cModule = new SCModule("MySystemCController");
    
    SCConstructor cConst = new SCConstructor("MySystemCController");
    CMethod main = new CMethod("main", "int", new ArrayList<>(), "");
    cModule.addGlobalMethod(main);
    cModule.addConstructor(cConst);
    cModule.addAttribute(new CAttribute("output[" + objectsCount + "]", "sc_out<event>", "", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("input[" + objectsCount + "]", "sc_in<event>", "", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("inbusy[" + objectsCount + "]", "sc_inout<bool>", "", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("outbusy[" + objectsCount + "]", "sc_inout<bool>", "", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("queue[" + objectsCount + "]", "queue_external", "queue_external()", CKeywords.PRIVATE));
    for (int i=0;i<objectsCount;i++)
    cConst.addBlockStatement(new CCode("queue["+i+"]=queue_external();"));
    cModule.addAttribute(new CAttribute("offset", "int", "0", CKeywords.PRIVATE));
    cModule.addAttribute(new CAttribute("clk", "sc_in<bool>", "", CKeywords.PRIVATE));

    cModule.addMethod(createControllerReceive(objects.size()));
    
    // Create the constructor of the controller
    cConst.addBlockStatement(new CCode("SC_METHOD(receive);"));
    StringBuffer receiveSensitivity=new StringBuffer("");
    for (UObject o : objects.keySet()) {
      receiveSensitivity.append("<< inbusy[" + objects.get(o) + "]");
      receiveSensitivity.append("<< outbusy[" + objects.get(o) + "]");
    }
    cConst.addBlockStatement(new CCode("sensitive_neg << clk;"));
    cConst.addBlockStatement(new CCode("dont_initialize();"));
    
    // Create the main method of the controller
    main.addBlockStatement(new CCode("sc_set_time_resolution(1,SC_MS);"));
    main.addBlockStatement(new CCode("sc_clock clk (\"clk\",sc_time(2,SC_MS),0.5,SC_ZERO_TIME,true);"));
    main.addBlockStatement(new CCode(" MySystemCController controller(\"Controller\");"));
    main.addBlockStatement(new CCode(" controller.clk(clk);"));
    
    // Create bindings for the objects in the main method
    for (UObject o : objects.keySet()) {
      int id = requireNonNull(objects.get(o));
      main.addBlockStatement(new CCode(o.getC1ass().getName() + " o" + id + "(\"" + o.getName() + "\");\n"));
      main.addBlockStatement(new CCode("o"+id+".clk(clk);"));
      main.addBlockStatement(new CCode("o"+id+".setsc_id("+id+");"));
      for (USlot s:o.getSlots()) {
        main.addBlockStatement(new CCode("o" + id + ".set"+s.getAttribute().getName()+"(" + objmap.get(s.getValues().get(0).toString()) + ");\n"));
      }
      String objectInitTemplate = initSystemCBindings(""+id);
      for (String s : objectInitTemplate.split("\n"))
        main.addBlockStatement(new CCode(s));
    }

    main.addBlockStatement(new CCode("sc_start(-1);"));
    return cModule;
  }

  private static CMethod createControllerReceive(int size) {
    //receiving all events and writing into a queue
    
    CMethod result = new CMethod("receive", CKeywords.VOID, new ArrayList<>(), CKeywords.PRIVATE);
    //if (DEBUG)result.getBody().addStatement(syscDebug("Receive method: sc_time=\"<< sc_simulation_time() <<\""));
    CFor receiving=new CFor("int i=0","i<"+size,"i++");
    receiving.addBlockStatement(new CCode("int index=(offset+i)%"+size+";"));
    CBlock rcb = new CBlock();
    rcb.addBlockStatement(new CCode("event e=input[index].read();"));
    rcb.addBlockStatement(new CCode("int recip=e.sender;"));
    CIfElse realReceive = new CIfElse("!queue[e.sender].isfull()",
                          new CBlock(new CCode("e.sender=index;"),
                                     new CCode("  queue[recip].enqueue(e);"),
                                     new CCode("inbusy[index]=false;")),
                          null);
    rcb.addBlockStatement(realReceive);
    receiving.addBlockStatement(new CIfElse("inbusy[index]",
                                    rcb,
                                    new CBlock()));
    result.addBlockStatement(receiving);
    
    //temporary inuse flags
    //
    StringBuffer tmp = new StringBuffer("outbusy[0]");
    for(int i=1;i<size;i++) tmp.append(",outbusy["+i+"]");
    result.addBlockStatement(new CCode("bool inuse["+size+"] = {"+tmp.toString()+"};" ));
    
    //sending events
    CFor sending = new CFor("int recip=0", "recip<" + size, "recip++");
    rcb = new CBlock();
    rcb.addBlockStatement(new CCode("event e=queue[recip].dequeue();"));
    rcb.addBlockStatement(new CCode("inuse[recip]=true;"));
    rcb.addBlockStatement(new CCode("output[recip].write(e);"));
    rcb.addBlockStatement(new CCode("outbusy[recip]=true;"));
    if (DEBUG)rcb.addBlockStatement(new CCode(syscDebug("Controller: \"<<e.sender<<\" sent event=\"<<e<<\" to \"<<recip<<\"")));
    CBlock rcf = new CBlock();
    if (DEBUG) rcf.addBlockStatement(new CCode("if (inuse[recip]) "+syscDebug("Receiver\" << recip << \"is blocked")));
    
    sending.addBlockStatement(new CIfElse("!inuse[recip] && (!queue[recip].isempty())",
                                  rcb,
                                  rcf));
    result.addBlockStatement(sending);
    result.addBlockStatement(new CCode("offset=(offset+1)%"+size+";"));
    return result;
  }

  private static String initSystemCBindings(String objName) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("// Object " + objName + "\n" +
                         "sc_signal<event> s_in" + objName + ", s_out" + objName + ";\n" +
                         "sc_signal<bool> s_inbusy" + objName + ", s_outbusy" + objName + ";\n" +
                         "s_inbusy" + objName + " = s_outbusy" + objName + " = false;\n"); 
    resultBuilder.append("\n");
    resultBuilder.append("o" + objName + ".input(s_in" + objName + ");\n" + 
                         "controller.output[" + objName + "](s_in" + objName + ");\n" + 
                         "o" + objName + ".output(s_out" + objName + ");\n" + 
                         "controller.input[" + objName + "](s_out" + objName + ");\n" +
                         "o" + objName + ".outbusy(s_outbusy" + objName + ");\n" + 
                         "controller.inbusy[" + objName + "](s_outbusy" + objName + ");\n" + 
                         "o" + objName + ".inbusy(s_inbusy" + objName + ");\n" + 
                         "controller.outbusy[" + objName + "](s_inbusy" + objName + ");\n");
    resultBuilder.append("\n");
    return resultBuilder.toString();
  }
  
  static String syscDebug(String text) {
    if (DEBUG)
      return "cout << \"[\" << sc_simulation_time() << \"] "+text+"\\n\";";
    return "";
  }
}
