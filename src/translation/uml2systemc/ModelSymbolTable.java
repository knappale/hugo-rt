package translation.uml2systemc;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNullByDefault;

import cpp.CKeywords;
import cpp.systemc.SCModule;
import uml.UAttribute;
import uml.UClass;
import uml.UClassifier;
import uml.UCollaboration;
import uml.UModel;
import uml.UType;

import static util.Objects.requireNonNull;


@NonNullByDefault
public class ModelSymbolTable extends SymbolTable {
  private UModel model;
  private Map<UClassifier, SCModule> cppClasses = new HashMap<>();
  private Map<String, UClassifier> classNames = new HashMap<>();
  private Map<UClassifier, ClassifierSymbolTable> classSymbolTables = new HashMap<>();
  private int classNamesCounter;
  private control.Properties properties;
  private int objectCount;
  private UCollaboration collaboration;

  ModelSymbolTable(UCollaboration collaboration) {
    this.model = collaboration.getModel();
    this.properties = collaboration.getProperties();
    this.collaboration = collaboration;
  }

  UCollaboration getCollaboration() {
    return this.collaboration;
  }

  public ClassifierSymbolTable getClassifierSymbolTable(UClassifier uClassifier) {
    ClassifierSymbolTable classifierSymbolTable = this.classSymbolTables.get(uClassifier);
    if (classifierSymbolTable != null)
      return classifierSymbolTable;

    classifierSymbolTable = new ClassifierSymbolTable(uClassifier, this);
    this.classSymbolTables.put(uClassifier, classifierSymbolTable);
    return classifierSymbolTable;
  }

  public String getTypeName(UType uType) {
    String cTypeName = "";
    if (uType.getUnderlyingType().isDataType()) {
      if (this.getModel().getIntegerType().subsumes(uType.getUnderlyingType()))
        cTypeName = CKeywords.INT;
      else {
        if (this.getModel().getBooleanType().subsumes(uType.getUnderlyingType()))
          cTypeName = CKeywords.BOOL;
        else {
          if (this.getModel().getStringType().subsumes(uType.getUnderlyingType()))
            cTypeName = CKeywords.STRING;
        }
      }
    } 
    else
      cTypeName = this.getClassifierSymbolTable(requireNonNull((UClass)uType.getUnderlyingType().getClassifier())).getCModule().getName();
    if (uType.isArray())
      cTypeName += "[]";
    return cTypeName;
  }

  public UModel getModel() {
    return this.model;
  }

  public SCModule getCModule(UClassifier uClassifier) {
    SCModule cClass = this.cppClasses.get(uClassifier);
    if (cClass == null) {
      String className = uniqueModuleName(uClassifier);
      cClass = new SCModule(className);
      this.cppClasses.put(uClassifier, cClass);
    }
    return cClass;
  }

  private String uniqueModuleName(UClassifier uClassifier) {
    char[] characters = uClassifier.getName().toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    String uniqueClassName = new String(characters);
    if (uniqueClassName.equals("") || this.classNames.keySet().contains(uniqueClassName)) {
      String tmpIdentifier;
      do {
        tmpIdentifier = uniqueClassName + this.classNamesCounter++;
      } while (this.classNames.keySet().contains(tmpIdentifier));
      uniqueClassName = tmpIdentifier;
    }
    this.classNames.put(uniqueClassName, uClassifier);
    return uniqueClassName;
  }

  public control.Properties getProperties() {
    return this.properties;
  }

  public String getAttributeName(UAttribute uAttribute) {
    return getClassifierSymbolTable(uAttribute.getOwner()).getAttributeName(uAttribute);
  }

  public int getUniqueObjectNumber() {
    return this.objectCount++;
  }

  public UClassifier getClassByName(String name) {
    return requireNonNull(classNames.get(name));
  }
  
  public Collection<ClassifierSymbolTable> getClassSymbolTables() {
   return this.classSymbolTables.values();
  }
}
