package translation.uml2systemc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import cpp.CArrayAttribute;
import cpp.CArrayParameter;
import cpp.CAttribute;
import cpp.CCode;
import cpp.CEnum;
import cpp.CKeywords;
import cpp.CMethod;
import cpp.CParameter;
import cpp.CPointer;
import cpp.CVariableDeclaration;
import cpp.systemc.SCConstructor;
import cpp.systemc.SCModule;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UClassifier;
import uml.UOperation;
import uml.UParameter;
import uml.UReception;
import uml.UType;
import uml.smile.SConstant;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UCompoundTransition;
import uml.statemachine.semantics.UTimerTree;

import static util.Objects.requireNonNull;


@NonNullByDefault
public class ClassifierSymbolTable extends SymbolTable {
  private static final String DEFAULTCLASSVARIABLEVISIBILITY = CKeywords.PUBLIC;

  private UClassifier uClassifier;
  private ModelSymbolTable modelSymbolTable;
  private control.Properties properties;
  private SCConstructor constructor;
  private Map<Object, CAttribute> attributes;
  private Map<UBehavioural, CMethod> methods;
  private Map<Object, CVariableDeclaration> constants;
  private Set<String> variableNames;
  private int variableNamesCounter = 0;
  private Set<String> methodNames;
  private int methodNamesCounter = 0;
  private Set<String> attributeNames;
  private int attributeNamesCounter = 0;
  private int constantsCounter = 0;
  private Map<UBehavioural, Integer> incomingEvents;
  private int labelCount=0;
  private Map<String, Integer> labels;
  private int loopLabelCount=1;
  private int choiceCount=1;
  private Map<UParameter, Integer> parameterIDs;

  public ClassifierSymbolTable(UClassifier uClassifier, ModelSymbolTable symboltable) {
    this.uClassifier = uClassifier;
    symboltable.getCModule(uClassifier);

    this.modelSymbolTable = symboltable;
    this.properties = modelSymbolTable.getProperties();
    attributes = new HashMap<>();
    methods = new HashMap<>();
    constants = new HashMap<>();
    labels = new HashMap<>();
    incomingEvents = new HashMap<>();
    attributeNames = new HashSet<>();
    methodNames = new HashSet<>();
    variableNames = new HashSet<>();
    constructor = new SCConstructor(uClassifier.getName());
    parameterIDs = new HashMap<>();
  }

  ModelSymbolTable getModelSymbolTable() {
    return this.modelSymbolTable;
  }

  public int getTimerNumber(UEvent uEvent) {
    return UTimerTree.create(requireNonNull(((UClass)this.uClassifier).getStateMachine())).getTimerNumber(uEvent);
  }

  public CAttribute createAttribute(UAttribute uAttribute) {
    CAttribute cAttribute = this.attributes.get(uAttribute);
    if (cAttribute != null)
      return cAttribute;
    String name = uniqueAttributeName(uAttribute.getName());
    String type = this.getTypeName(uAttribute.getType());
    String initialValue = null;
    if (uAttribute.isArray()) {
      initialValue = CKeywords.NEW + " " + type;
      initialValue += "{ ";
      for (int i = 0; i < uAttribute.getSize(); i++) {
        initialValue += UMLTranslator.translateExpression(requireNonNull(uAttribute.getInitialValue(i)), uAttribute.getInitialValuesContext(), this);
        if (i < uAttribute.getSize() - 1)
          initialValue += ", ";
      }
      initialValue += " }";
    } else {
      if (uAttribute.hasInitialValues()&& uAttribute.getInitialValue(0)!=null) {
        initialValue = "" + UMLTranslator.translateExpression(requireNonNull(uAttribute.getInitialValue(0)), uAttribute.getInitialValuesContext(), this);
      }
    }

    // Set modifiers
    String modifiers = "";
    if (uAttribute.isStatic()) {
      modifiers = DEFAULTCLASSVARIABLEVISIBILITY;
      modifiers += " " + javacode.JKeywords.STATIC;
      if (uAttribute.isConstant())
        modifiers += " " + CKeywords.STATIC;
    }
    if (uAttribute.getType().isDataType()) {
      if (uAttribute.isFinal() && uAttribute.isStatic())
        cAttribute = new CAttribute(name, type, requireNonNull(initialValue), modifiers);
      else {
        cAttribute = new CAttribute(name, type, modifiers);
        if (initialValue!=null) constructor.addBlockStatement(new CCode(name + " = " + initialValue + ";"));
      }
    } 
    else {
      if (uAttribute.isArray()) {
        cAttribute = new CArrayAttribute(name, CKeywords.INT, modifiers, uAttribute.getSize());
      } else {
        cAttribute = new CAttribute(name, CKeywords.INT, modifiers);
      }
    }
    attributes.put(uAttribute, cAttribute);

    // Adds Attribute to cpp Class
    this.getCModule().addAttribute(cAttribute);

    // Create setter
    if (!uAttribute.isFinal()) {
      if (uAttribute.getType().isDataType()) {
        String setterName = uniqueMethodName("set" + name.substring(0, 1).toUpperCase() + name.substring(1, name.length()));

        List<CParameter> setterParameters = new ArrayList<>();
        if (cAttribute instanceof CPointer) {
          setterParameters.add(new CParameter("__"+name, type + " *"));
        } else {
          setterParameters.add(new CParameter("__"+name, type + " "));
        }
        CParameter setterParameter = setterParameters.get(0);

        CMethod setter = new CMethod(setterName, CKeywords.VOID, setterParameters, CKeywords.PUBLIC);
        if (cAttribute instanceof CPointer)
          setter.addBlockStatement(new CCode(this.getAttributeName(uAttribute) + " = " + setterParameter.getName() + ";"));
        else
          setter.addBlockStatement(new CCode(this.getAttributeName(uAttribute) + " = " + setterParameter.getName() + ";"));
        this.getCModule().addMethod(setter);
      } 
      else {
        if (!uAttribute.isArray()) {
        List <CParameter> parm= new ArrayList<CParameter>();
        parm.add(new CParameter("__id", CKeywords.INT));
        CMethod m = new CMethod(uniqueMethodName("set"+uAttribute.getName()), CKeywords.VOID, parm, CKeywords.PUBLIC);
        m.addBlockStatement(new CCode(getAttributeName(uAttribute)+"=__id;"));
        this.getCModule().addMethod(m);
        } 
        else {
          List <CParameter> parm= new ArrayList<CParameter>();
          parm.add(new CArrayParameter("__id",CKeywords.INT,uAttribute.getSize()));
          CMethod m = new CMethod(uniqueMethodName("set"+uAttribute.getName()), CKeywords.VOID, parm, CKeywords.PUBLIC);
          m.addBlockStatement(new CCode(getAttributeName(uAttribute)+"=__id;"));
          this.getCModule().addMethod(m);
        }

      }

      // Create getter
      if (uAttribute.getType().isDataType()) {
      String getterName = uniqueMethodName("get" + name.substring(0, 1).toUpperCase() + name.substring(1, name.length()));
      CMethod getter;
      if (cAttribute instanceof CPointer) {
        getter = new CMethod(getterName, type, new ArrayList<>(), CKeywords.PUBLIC);
        getter.addBlockStatement(new CCode("return *" + this.getAttributeName(uAttribute) + ";"));
      } else {
        getter = new CMethod(getterName, type, new ArrayList<>(), CKeywords.PUBLIC);
        getter.addBlockStatement(new CCode("return " + this.getAttributeName(uAttribute) + ";"));
      }
      this.getCModule().addMethod(getter);
      } 
      else {
        if (!uAttribute.isArray()) {
           CMethod m = new CMethod(uniqueMethodName("get"+uAttribute.getName()), CKeywords.INT, new ArrayList<>(), CKeywords.PUBLIC);
           m.addBlockStatement(new CCode("return "+getAttributeName(uAttribute)+";"));
           this.getCModule().addMethod(m);
        }
        else {
          CMethod m = new CMethod(uniqueMethodName("get"+uAttribute.getName()), CKeywords.INT + "["+uAttribute.getSize()+"]", new ArrayList<>(), CKeywords.PUBLIC);
          m.addBlockStatement(new CCode("return "+getAttributeName(uAttribute)+";"));
          this.getCModule().addMethod(m);
        }
      }
    } else {

    }
    return cAttribute;
  }


  public String getAttributeName(UAttribute uAttribute) {
    if (!uAttribute.getOwner().equals(this.uClassifier))
      return modelSymbolTable.getAttributeName(uAttribute);

    CAttribute cAttribute = createAttribute(uAttribute);
    if (uAttribute.isStatic())
      return getClassSymbolTable(uAttribute.getOwner()).getCModule().getName() + "->" + cAttribute.getName();
    else
      return cAttribute.getName();
  }

  private ClassifierSymbolTable getClassSymbolTable(UClassifier uClassifier) {
    return this.modelSymbolTable.getClassifierSymbolTable(uClassifier);
  }

  public String getTypeName(UType uType) {
    return this.modelSymbolTable.getTypeName(uType);
  }

  public CMethod createMethod(UBehavioural uBehavioural) {
    if (incomingEvents.containsKey(uBehavioural))
      return requireNonNull(methods.get(uBehavioural));

    incomingEvents.put(uBehavioural, ++methodNamesCounter);
    int parmID=0;
    List<CParameter> parameters = new ArrayList<>();
    for (UParameter parm : uBehavioural.getParameters()) {
      parameterIDs.put(parm,parmID++);
      String name = parm.getName();
      String type = this.getTypeName(parm.getType());
      CParameter p = new CParameter(name,type);
      parameters.add(p);
    }

    String type = CKeywords.VOID;
    String modifiers = CKeywords.PUBLIC;
    if (uBehavioural.isStatic())
      modifiers += " " + CKeywords.STATIC;
    String name = uniqueMethodName("" + uBehavioural.getName());
    CMethod cMethod = new CMethod(name, type, parameters, modifiers);
    methods.put(uBehavioural, cMethod);

    var systemCMethodCode = uBehavioural.getMethodCode("SystemC");
    if (systemCMethodCode != null) {
      for (var systemCMethodCodeLine : systemCMethodCode)
        cMethod.addBlockStatement(new CCode(systemCMethodCodeLine));
    }

    getCModule().addMethod(cMethod);
    methodNames.add(name);
    return cMethod;
  }

  public String getMethodName(UBehavioural uBehavioural) {
    return requireNonNull(this.methods.get(uBehavioural)).getName();
  }

  public String getParameterID(UParameter uParameter) {
    return requireNonNull(this.parameterIDs.get(uParameter)).toString();
  }

  /**
   * Determine a unique name for an attribute name.
   * 
   * @param attributeName an attribute name
   * @return a unique attribute name
   */
  private String uniqueAttributeName(String attributeName) {
    char[] characters = attributeName.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    String uniqueAttributeName = new String(characters);
    if (uniqueAttributeName.equals("") || this.attributeNames.contains(uniqueAttributeName)) {
      String tmpAttributeName;
      do {
        tmpAttributeName = uniqueAttributeName + this.attributeNamesCounter++;
      } while (this.attributeNames.contains(tmpAttributeName));
      uniqueAttributeName = tmpAttributeName;
    }
    this.attributeNames.add(uniqueAttributeName);
    return uniqueAttributeName;
  }

  /**
   * Determine a unique name for a method name.
   * 
   * @param methodName a method name
   * @return a unique method name
   */
  private String uniqueMethodName(String methodName) {
    char[] characters = methodName.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    String uniqueMethodName = new String(characters);
    if (uniqueMethodName.equals("") || this.methodNames.contains(uniqueMethodName)) {
      String tmpMethodName;
      do {
        tmpMethodName = uniqueMethodName + this.methodNamesCounter++;
      } while (this.methodNames.contains(tmpMethodName));
      uniqueMethodName = tmpMethodName;
    }
    this.methodNames.add(uniqueMethodName);
    return uniqueMethodName;
  }

  /**
   * Determine a unique name for a variable name.
   * 
   * @param variableName a variable name
   * @return a unique variable name
   */
  private String uniqueVariableName(String variableName) {
    char[] characters = variableName.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    String uniqueVariableName = new String(characters);
    if (uniqueVariableName.equals("") || this.variableNames.contains(uniqueVariableName)) {
      String tmpVariableName;
      do {
        tmpVariableName = uniqueVariableName + this.variableNamesCounter++;
      } while (this.variableNames.contains(tmpVariableName));
      uniqueVariableName = tmpVariableName;
    }
    this.variableNames.add(uniqueVariableName);
    return uniqueVariableName;
  }

  public SCModule getCModule() {
    return this.modelSymbolTable.getCModule(this.uClassifier);
  }

  public control.Properties getProperties() {
    return this.properties;
  }

  public UClassifier getClassifier() {
    return this.uClassifier;
  }

  public String getVariableName(SVariable sVariable) {
    CAttribute cAttribute = attributes.get(sVariable);
    if (cAttribute == null) {
      String name = uniqueVariableName(sVariable.getName());
      String type = (sVariable.isFlag()) ? CKeywords.BOOL : CKeywords.INT;
      @NonNull List<String> value = new ArrayList<>();
      value.add((sVariable.isFlag()) ? CKeywords.FALSE : "0");
      cAttribute = new CAttribute(name, type, value.get(0), CKeywords.PRIVATE);
      attributes.put(sVariable, cAttribute);
      this.getCModule().addAttribute(cAttribute);
    }
    
    return cAttribute.getName();
  }

  public String getConstantName(SConstant sConstant) {
    return sConstant.new Cases<String>().
      empty(() -> "0").
      vertex(uVertex -> getStateName(uVertex)).
      transition(uCompound -> getTransitionName(uCompound)).
      event(uEvent -> getEventName(uEvent)).
      apply();
  }

  /**
   * TODO (AK150928) Only ask for event names for behavioured classifiers
   * 
   * @param uEvent a UML event
   * @return a name for {@link uEvent}
   */
  public String getEventName(UEvent uEvent) {
    return uEvent.new Cases<String>().otherwise(() -> uEvent.getName()).
        signal(uSignal -> uSignal.getName().toUpperCase()).
        call(uOperation -> uOperation.getName().toUpperCase()).
        time((uState, lower, upper) -> "" + UTimerTree.create(requireNonNull(((UClass)this.getClassifier()).getStateMachine())).getTimerNumber(uEvent)).
        apply();
  }

  /**
   * @param uVertex a UML state
   * @return a name for {@link uState}
   */
  public String getStateName(UVertex uVertex) {
    CVariableDeclaration constant = constants.get(uVertex);
    if (constant != null)
      return constant.getName();
    String constantName = uniqueVariableName(uVertex.getIdentifier().toString().toUpperCase());
    constant = new CVariableDeclaration(constantName, "int", "" + constantsCounter++);
    constants.put(uVertex, constant);
    return constant.getName();
  }

  /**
   * @param uTransition a UML compound transition
   * @return a name for {@link uTransition}
   */
  private String getTransitionName(UCompoundTransition uTransition) {
    CVariableDeclaration constant = constants.get(uTransition);
    if (constant != null)
      return constant.getName();
    String constantName = uniqueVariableName(uTransition.getName());
    constant = new CVariableDeclaration(constantName, "int", "" + constantsCounter++);
    constants.put(uTransition, constant);
    return constant.getName();
  }

  public SCConstructor getConstructor() {
    return constructor;
  }

  public Collection<CVariableDeclaration> getConstantDeclarations() {
    return this.constants.values();
  }

  public void createStateAndEventConstants() {
    Set<String> cons = new HashSet<String>();
    for (CVariableDeclaration vd : constants.values()) {
      cons.add(vd.getName());
    }
    getCModule().addEnumerationType(new CEnum("states", CKeywords.PRIVATE, cons));
    Map<String, Integer> events = new HashMap<>();
    for (UBehavioural b : incomingEvents.keySet()) {
      if (b instanceof UOperation)
        events.put(b.getName().toUpperCase(), requireNonNull(incomingEvents.get(b)));
      else if (b instanceof UReception)
        events.put(b.getName().toUpperCase(), requireNonNull(incomingEvents.get(b)));
    }
    getCModule().addEnumerationType(new CEnum("events", CKeywords.PRIVATE, events));
    getCModule().addEnumerationType(new CEnum("labels", CKeywords.PRIVATE, labels));
  }

  public ClassifierSymbolTable getcppClassByName(String name) {
    return modelSymbolTable.getClassifierSymbolTable(modelSymbolTable.getClassByName(name));
  }

  public ClassifierSymbolTable getUMLClass(UClass owner) {
    return modelSymbolTable.getClassifierSymbolTable(owner);
  }

  public String getSignalID(UBehavioural uBehavioural) {
    Integer uEventId = this.incomingEvents.get(uBehavioural); assert (uEventId != null);
    return uEventId.toString();
  }

  public String getLabel() {
    labelCount++;
    String name="LABEL_"+labelCount;
    labels.put(name,labelCount);
    return name;
  }
  
  public Set<String> getLabels() {
    return labels.keySet();
  }

  public String getJumpLabel(String labelname) {
    String name=labelname.toUpperCase();
    if(!labels.containsKey(name))
      labels.put(name,++labelCount);
    return name;
  }

  public String getLoopLabel() {
    String name="LOOP_"+(loopLabelCount++);
    labels.put(name,++labelCount);
    return name;
  }

  public String getBranchLabel(String labelStart) {
    String name=labelStart+"_BRANCH";
    labels.put(name,++labelCount);
    return name;
  }

  public String getChoiceBranchLabel() {
    String name="CHOICE_"+(choiceCount++)+"_BRANCH";
    labels.put(name,++labelCount);
    return name;
  }
}
