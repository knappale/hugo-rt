package translation.uml2systemc;

import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import cpp.CBlock;
import cpp.CClass;
import cpp.CCode;
import cpp.CIfElse;
import cpp.CSystem;
import cpp.CSequence;
import cpp.systemc.SCModule;
import cpp.systemc.SCRestart;
import uml.UAction;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UCollaboration;
import uml.UContext;
import uml.UExpression;
import uml.UModel;
import uml.UOperation;
import uml.UOperator;
import uml.UReception;
import uml.UStatual;
import uml.statemachine.UStateMachine;
import util.Formatter;


/**
 * Translate UML model into SystemC code.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class UMLTranslator {
  private ModelSymbolTable modelSymbolTable;

  private UMLTranslator(UCollaboration uCollaboration) {
    this.modelSymbolTable = new ModelSymbolTable(uCollaboration);
  }

  public static UMLTranslator translate(UCollaboration uCollaboration) {
    UMLTranslator instance = new UMLTranslator(uCollaboration);

    UModel uModel = instance.modelSymbolTable.getModel();

    // Translate each class of the model
    for (UClass uClass : uModel.getClasses()) {
      ClassifierSymbolTable classSymbolTable = instance.modelSymbolTable.getClassifierSymbolTable(uClass);

      // Translation of attributes (including getters and setters)
      for (UAttribute uAttribute : uClass.getAttributes()) {
        classSymbolTable.createAttribute(uAttribute);
      }

      // Translation of receptions
      for (UReception uReception : uClass.getReceptions()) {
        classSymbolTable.createMethod(uReception);
      }

      // Translation of operations
      for (UOperation uOperation : uClass.getOperations()) {
        classSymbolTable.createMethod(uOperation);
      }
      
      classSymbolTable.getCModule().addConstructor(classSymbolTable.getConstructor());
    }

    // Translation of state machines
    for (UClass uClass : uModel.getClasses()) {
      ClassifierSymbolTable classSymbolTable = instance.modelSymbolTable.getClassifierSymbolTable(uClass);
      UStateMachine uStateMachine = uClass.getStateMachine();
      if (uStateMachine != null)
        SmileTranslator.translate(uStateMachine.getMachine(classSymbolTable.getProperties()), classSymbolTable);
    }

    return instance;
  }

  public CSystem getCPPCode() {
    CSystem cppcode = new CSystem();
    for (UClass uClass : this.modelSymbolTable.getModel().getClasses()) {
      ClassifierSymbolTable classSymbolTable = this.modelSymbolTable.getClassifierSymbolTable(uClass);
      cppcode.addClass(classSymbolTable.getCModule());
    }
    return cppcode;
  }

  public String getMakefile() {
    StringBuilder makefile = new StringBuilder();

    makefile.append("CC=g++\n");
    makefile.append("INCLUDES=.\n");
    makefile.append("LIBRARYDIR=.\n");
    makefile.append("\n");
    makefile.append("all:\n");
    StringBuffer linkline = new StringBuffer();
    for (CClass cclass: this.getCPPCode().getCClasses()) {
      makefile.append("\t$(CC) -c -g -w src/" + (cclass.getName())+".cpp -o o/" + cclass.getName() + ".o -I$(INCLUDES)\n");
      linkline.append("o/" + cclass.getName() + ".o ");
    }
    makefile.append("\t$(CC) -c -g -w src/Controller.cpp -o o/Controller.o  -I$(INCLUDES)\n");
    makefile.append("\t$(CC) -g -o emu -L$(LIBRARYDIR) " + linkline + "o/Controller.o -lsystemc\n");

    return makefile.toString();
  }
  
  public String getController() {
    StringBuilder controller_includes= new StringBuilder();
    controller_includes.append("#include <systemc.h>\n");
    controller_includes.append("#include \"../include/sc_eventqueue.h\"\n");
    for (CClass cclass: this.getCPPCode().getCClasses()) {
      controller_includes.append("#include \"../include/" + cclass.getName() + ".hh\"\n");
    }
    SCModule cont = translation.uml2systemc.SmileTranslator.createController(this.modelSymbolTable);
    return controller_includes.toString()+cont.getHeaderFile()+cont.getDefinitions()+"\n";
  }

  public final String getEventQueue() {
    int maxParamSize = this.modelSymbolTable.getModel().getClasses().stream().flatMap(uClass -> uClass.getBehaviourals().stream().
                                                                                  map(uBehavioural -> uBehavioural.getParameters().size())).reduce(0, (m1, m2) -> Integer.max(m1, m2));
    int internalSize = this.modelSymbolTable.getProperties().getInternalQueueCapacity();
    int externalSize = this.modelSymbolTable.getProperties().getExternalQueueCapacity();
    int deferredSize = this.modelSymbolTable.getProperties().getDeferredQueueCapacity();

    String result =
    "#define MAX_PARAMETER_SIZE "+maxParamSize+"\n" +
    "#define INTERNAL_QUEUE_SIZE "+internalSize+"\n" +
    "#define DEFERRED_QUEUE_SIZE "+deferredSize+"\n" +
    "#define EXTERNAL_QUEUE_SIZE "+externalSize+"\n" +
    "\n" +
    "#include <systemc.h>\n" +
    "#include <iostream.h>\n" +
    "#ifndef EVENTQUEUE_H_\n" +
    "#define EVENTQUEUE_H_\n" +
    "\n" +
    "\n" +
    " struct event {\n" +
    "    enum etype {SIGNAL=1,CALL=2,TIME=3,COMPLETION=4,CHANGE=5,ACK=6,DUMMY=-1};\n" +
    "    etype type;\n" +
    "    int name;\n" +
    "    int sender;\n" +
    "#if MAX_PARAMETER_SIZE>0\n" +
    "    int parm[MAX_PARAMETER_SIZE];\n" +
    "#endif\n" +
    "    event() {\n" +
    "       name=-1;\n" +
    "       type=DUMMY;\n" +
    "       sender=-1;\n" +
    "    }\n" +
    "    event (int id,etype type) {\n" +
    "       this->name=id;\n" +
    "       this->type=type;\n" +
    "       sender=-1;\n" +
    "    }\n" +
    "    event (int id,etype type,int __sender) {\n" +
    "       this->name=id;\n" +
    "       this->type=type;\n" +
    "       sender=__sender;\n" +
    "    }\n" +
    "    \n" +
    "    \n" +
    "    ~event () {}\n" +
    "    inline bool event::operator== (const event&e2) {\n" +
    "      bool b1 = name==e2.name && type==e2.type;\n" +
    "#if MAX_PARAMETER_SIZE >0\n" +
    "      for (int i=0;i<MAX_PARAMETER_SIZE;i++)\n" +
    "        if (parm[i]!=e2.parm[i]) return false;\n" +
    "#endif\n" +
    "      return b1;\n" +
    "              \n" +
    "    }\n" +
    "    friend ostream& operator<< (ostream& s, const event& e) {\n" +
    "       s << \"event(\" << e.name << \",\" <<e.type;\n" +
    "       if (e.type == event::SIGNAL || e.type == event::CALL) {\n" +
    "         s << \",sender=\"<<e.sender;\n" +
    "#if MAX_PARAMETER_SIZE >0\n" +
    "         s << \",parm=(\"<<e.parm[0];\n" +
    "         for (int i=1;i<MAX_PARAMETER_SIZE;i++)\n" +
    "           s<<\",\"<<e.parm[i];\n" +
    "         s << \")\";\n" +
    "#endif\n" +
    "      }\n" +
    "      s <<\")\";\n" +
    "      return s;\n" +
    "    }\n" +
    "   inline event& operator = (const event& e) {\n" +
    "      name = e.name;\n" +
    "      type = e.type;\n" +
    "      sender= e.sender;\n" +
    "#if MAX_PARAMETER_SIZE>0\n" +
    "      for (int i=0;i<MAX_PARAMETER_SIZE;i++)\n" +
    "      parm[i]= e.parm[i];\n" +
    "#endif\n" +
    "      return *this;\n" +
    "    }\n" +
    "    inline  friend void sc_trace(sc_core::sc_trace_file*& tf,const event& e, const std::string NAME)  {\n" +
    "      sc_trace(tf,e.name, NAME + \".name\");\n" +
    "      sc_trace(tf,e.type, NAME + \".type\");\n" +
    "    }\n" +
    "    \n" +
    "}; \n" +
    " struct event_int {\n" +
    "    event::etype type;\n" +
    "    int name;\n" +
    "    int min,max;\n" +
    "    event_int() {\n" +
    "       name=-1;\n" +
    "       type=event::DUMMY;\n" +
    "    }\n" +
    "    event_int (int id,event::etype type) {\n" +
    "       this->name=id;\n" +
    "       this->type=type;\n" +
    "    }\n" +
    "    event_int (int id,int min,int max) {\n" +
    "       this->name=id;\n" +
    "       this->type=event::TIME;\n" +
    "       this->min=min;\n" +
    "       this->max=max;\n" +
    "    }\n" +
    "    \n" +
    "    ~event_int () {}\n" +
    "    inline bool event_int::operator== (const event_int&e2) {\n" +
    "      return name==e2.name && type==e2.type;\n" +
    "    }\n" +
    "    friend ostream& operator<< (ostream& s, const event_int& e) {\n" +
    "      if (e.type==event::TIME) s << \"event (\" << e.name << \",\" <<e.type <<\",min=\"<<e.min<<\",max=\"<<e.max<<\")\";\n" +
    "      else s << \"event_int(\" << e.name << \",\" <<e.type <<\")\";\n" +
    "      return s;\n" +
    "    }\n" +
    "    inline  friend void sc_trace(sc_core::sc_trace_file*& tf,const event_int& e, const std::string NAME)  {\n" +
    "      sc_trace(tf,e.name, NAME + \".name\");\n" +
    "      sc_trace(tf,e.type, NAME + \".type\");\n" +
    "    }\n" +
    "\n" +
    "   inline event_int& operator = (const event_int& e) {\n" +
    "      name = e.name;\n" +
    "      type = e.type;\n" +
    "      max = e.max;\n" +
    "      min = e.min;\n" +
    "      return *this;\n" +
    "    }\n" +
    "}; \n" +
    "\n" +
    "\n" +
    "struct queue_external {\n" +
    "   event elements[EXTERNAL_QUEUE_SIZE];\n" +
    "   int size;\n" +
    "   int  read, write;\n" +
    "   queue_external () {\n" +
    "       read=write=size=0;\n" +
    "       for (int i=0;i<EXTERNAL_QUEUE_SIZE;i++) elements[i]=event();\n" +
    "   }\n" +
    "   \n" +
    "    bool enqueue(event e) {\n" +
    "       if(size <EXTERNAL_QUEUE_SIZE) { \n" +
    "           elements[write]=e;\n" +
    "           write=(write+1)%EXTERNAL_QUEUE_SIZE;\n" +
    "           size++;\n" +
    "           return true;\n" +
    "       } else {\n" +
    "           cout <<\"Buffer Overflow: \"<< *this <<\"!!!\\n\";\n" +
    "           return false;       \n" +
    "       }\n" +
    "   }\n" +
    "   \n" +
    "    event dequeue() {\n" +
    "       if (size>0) {\n" +
    "           event e=elements[read];\n" +
    "           elements[read]=event();\n" +
    "           read=(read+1)%EXTERNAL_QUEUE_SIZE;\n" +
    "           size--;\n" +
    "           return e;\n" +
    "       }\n" +
    "       return event();\n" +
    "   }\n" +
    "   \n" +
    "   bool isempty () {\n" +
    "       return size==0;\n" +
    "   }\n" +
    "   \n" +
    "   bool isfull() {\n" +
    "       return size==EXTERNAL_QUEUE_SIZE;\n" +
    "   }\n" +
    "   \n" +
    "   void print() {\n" +
    "       for (int i=0;i<EXTERNAL_QUEUE_SIZE;i++) cout << elements[i];\n" +
    "   }\n" +
    "   \n" +
    "    friend ostream& operator<< (ostream & s, const queue_external & m) {\n" +
    "       s <<\"[\";\n" +
    "       for (int i=0;i<EXTERNAL_QUEUE_SIZE;i++) {\n" +
    "           s << m.elements[i]<< ((i+1<EXTERNAL_QUEUE_SIZE)?\",\":\"\");\n" +
    "       }\n" +
    "       s << \"|(read=\"<<m.read<<\",write=\"<<m.write<<\",size=\"<<m.size<<\")]\";\n" +
    "       return s;\n" +
    "   }\n" +
    "   bool contains (event t) {\n" +
    "       if (size==0) return false;\n" +
    "       for (int i=0;i<size;i++) {\n" +
    "           int index=(read+i)%EXTERNAL_QUEUE_SIZE;\n" +
    "           if (elements[index]==t) return true;\n" +
    "       }\n" +
    "       return false;\n" +
    "   }\n" +
    "   \n" +
    "   void removeall(event t) {\n" +
    "       if (size==0) return ;\n" +
    "       int found=0;\n" +
    "       int index=read;\n" +
    "       bool fromfirst=true;\n" +
    "       \n" +
    "       for (int i=0;i<size;i++) {\n" +
    "         index=(read+i)%EXTERNAL_QUEUE_SIZE;\n" +
    "         if (fromfirst && t==elements[index]) {\n" +
    "           elements[index]=event();\n" +
    "           index=(index+1)%EXTERNAL_QUEUE_SIZE;\n" +
    "           read=(read+1)%EXTERNAL_QUEUE_SIZE;\n" +
    "           size--;\n" +
    "         } \n" +
    "         else {\n" +
    "           fromfirst=false;\n" +
    "         }\n" +
    "           \n" +
    "       }\n" +
    "       for (int i=0;i<size;i++) {\n" +
    "       index=(read+i)%EXTERNAL_QUEUE_SIZE;\n" +
    "           if (elements[index]==t) {\n" +
    "               elements[index]=event();\n" +
    "               found++;\n" +
    "           }\n" +
    "           else if (found>0) {\n" +
    "               elements[(index-found+EXTERNAL_QUEUE_SIZE)%EXTERNAL_QUEUE_SIZE]=elements[index];\n" +
    "               elements[index]=event();\n" +
    "           }\n" +
    "       }\n" +
    "       write=(write-found+EXTERNAL_QUEUE_SIZE)%EXTERNAL_QUEUE_SIZE;\n" +
    "       size=size-found;\n" +
    "       \n" +
    "   }\n" +
    "};\n" +
    "\n" +
    "struct queue_deferred {\n" +
    "   event elements[DEFERRED_QUEUE_SIZE];\n" +
    "   int max_size;\n" +
    "   int size;\n" +
    "   int  read, write;\n" +
    "\n" +
    "   queue_deferred () {\n" +
    "       read=write=size=0;\n" +
    "       max_size=DEFERRED_QUEUE_SIZE;\n" +
    "   }\n" +
    "   \n" +
    "    bool enqueue(event e) {\n" +
    "       if(size <DEFERRED_QUEUE_SIZE) { \n" +
    "           elements[write]=e;\n" +
    "           write=(write+1)%DEFERRED_QUEUE_SIZE;\n" +
    "           size++;\n" +
    "           return true;\n" +
    "       } else {\n" +
    "           cout <<\"Buffer Overflow: \"<< *this <<\"!!!\\n\";\n" +
    "           return false;       \n" +
    "       }\n" +
    "   }\n" +
    "   \n" +
    "    event dequeue() {\n" +
    "       if (size>0) {\n" +
    "           event e=elements[read];\n" +
    "           elements[read]=event();\n" +
    "           read=(read+1)%DEFERRED_QUEUE_SIZE;\n" +
    "           size--;\n" +
    "           return e;\n" +
    "       }\n" +
    "       return event();\n" +
    "   }\n" +
    "   \n" +
    "   bool isempty () {\n" +
    "       return size==0;\n" +
    "   }\n" +
    "   \n" +
    "   bool isfull() {\n" +
    "       return size==DEFERRED_QUEUE_SIZE;\n" +
    "   }\n" +
    "   \n" +
    "   void print() {\n" +
    "       for (int i=0;i<DEFERRED_QUEUE_SIZE;i++) cout << elements[i];\n" +
    "   }\n" +
    "   \n" +
    "    friend ostream& operator<< (ostream & s, const queue_deferred & m) {\n" +
    "       s <<\"[\";\n" +
    "       for (int i=0;i<DEFERRED_QUEUE_SIZE;i++) {\n" +
    "           s << m.elements[i]<< ((i+1<DEFERRED_QUEUE_SIZE)?\",\":\"\");\n" +
    "       }\n" +
    "       s << \"|(read=\"<<m.read<<\",write=\"<<m.write<<\",size=\"<<m.size<<\")]\";\n" +
    "       return s;\n" +
    "   }\n" +
    "   bool contains (event t) {\n" +
    "       if (size==0) return false;\n" +
    "       for (int i=0;i<size;i++) {\n" +
    "           int index=(read+i)%DEFERRED_QUEUE_SIZE;\n" +
    "           if (elements[index]==t) return true;\n" +
    "       }\n" +
    "       return false;\n" +
    "   }\n" +
    "   \n" +
    "   void removeall(event t) {\n" +
    "       if (size==0) return ;\n" +
    "       int found=0;\n" +
    "       int index=read;\n" +
    "       bool fromfirst=true;\n" +
    "       \n" +
    "       for (int i=0;i<size;i++) {\n" +
    "       //while (elements[index]==t && size>0) {\n" +
    "           index=(read+i)%DEFERRED_QUEUE_SIZE;\n" +
    "           if (fromfirst && t==elements[index]) {\n" +
    "           elements[index]=event();\n" +
    "           index=(index+1)%DEFERRED_QUEUE_SIZE;\n" +
    "           read=(read+1)%DEFERRED_QUEUE_SIZE;\n" +
    "           size--;\n" +
    "           } else {\n" +
    "               fromfirst=false;\n" +
    "           }\n" +
    "           \n" +
    "       }\n" +
    "       for (int i=0;i<size;i++) {\n" +
    "       index=(read+i)%DEFERRED_QUEUE_SIZE;\n" +
    "           if (elements[index]==t) {\n" +
    "               elements[index]=event();\n" +
    "               found++;\n" +
    "           }\n" +
    "           else if (found>0) {\n" +
    "               elements[(index-found+DEFERRED_QUEUE_SIZE)%DEFERRED_QUEUE_SIZE]=elements[index];\n" +
    "               elements[index]=event();\n" +
    "           }\n" +
    "       }\n" +
    "       write=(write-found+DEFERRED_QUEUE_SIZE)%DEFERRED_QUEUE_SIZE;\n" +
    "       size=size-found;\n" +
    "       \n" +
    "   }\n" +
    "};\n" +
    "\n" +
    "struct queue_internal {\n" +
    "   event_int elements[INTERNAL_QUEUE_SIZE];\n" +
    "   int max_size;\n" +
    "   int size;\n" +
    "   int  read, write;\n" +
    "\n" +
    "   queue_internal () {\n" +
    "       read=write=size=0;\n" +
    "       max_size=INTERNAL_QUEUE_SIZE;\n" +
    "   }\n" +
    "   \n" +
    "    bool enqueue(event_int e) {\n" +
    "       if(size <INTERNAL_QUEUE_SIZE) { \n" +
    "           elements[write]=e;\n" +
    "           write=(write+1)%INTERNAL_QUEUE_SIZE;\n" +
    "           size++;\n" +
    "           return true;\n" +
    "       } else {\n" +
    "           cout <<\"Buffer Overflow: \"<< *this <<\"!!!\\n\";\n" +
    "           return false;       \n" +
    "       }\n" +
    "   }\n" +
    "   \n" +
    "    event_int dequeue() {\n" +
    "       if (size>0) {\n" +
    "           event_int e=elements[read];\n" +
    "           elements[read]=event_int();\n" +
    "           read=(read+1)%INTERNAL_QUEUE_SIZE;\n" +
    "           size--;\n" +
    "           return e;\n" +
    "       }\n" +
    "       return event_int();\n" +
    "   }\n" +
    "   \n" +
    "   bool isempty () {\n" +
    "       return size==0;\n" +
    "   }\n" +
    "   \n" +
    "   bool isfull() {\n" +
    "       return size==INTERNAL_QUEUE_SIZE;\n" +
    "   }\n" +
    "   \n" +
    "   void print() {\n" +
    "       for (int i=0;i<INTERNAL_QUEUE_SIZE;i++) cout << elements[i];\n" +
    "   }\n" +
    "   \n" +
    "    friend ostream& operator<< (ostream & s, const queue_internal & m) {\n" +
    "       s <<\"[\";\n" +
    "       for (int i=0;i<INTERNAL_QUEUE_SIZE;i++) {\n" +
    "           s << m.elements[i]<< ((i+1<INTERNAL_QUEUE_SIZE)?\",\":\"\");\n" +
    "       }\n" +
    "       s << \"|(read=\"<<m.read<<\",write=\"<<m.write<<\",size=\"<<m.size<<\")]\";\n" +
    "       return s;\n" +
    "   }\n" +
    "   bool contains (event_int t) {\n" +
    "       if (size==0) return false;\n" +
    "       for (int i=0;i<size;i++) {\n" +
    "           int index=(read+i)%INTERNAL_QUEUE_SIZE;\n" +
    "           if (elements[index]==t) return true;\n" +
    "       }\n" +
    "       return false;\n" +
    "   }\n" +
    "   \n" +
    "   void removeall(event_int t) {\n" +
    "       if (size==0) return ;\n" +
    "       int found=0;\n" +
    "       int index=read;\n" +
    "       bool fromfirst=true;\n" +
    "       \n" +
    "       for (int i=0;i<size;i++) {\n" +
    "       //while (elements[index]==t && size>0) {\n" +
    "           index=(read+i)%INTERNAL_QUEUE_SIZE;\n" +
    "           if (fromfirst && t==elements[index]) {\n" +
    "           elements[index]=event_int();\n" +
    "           index=(index+1)%INTERNAL_QUEUE_SIZE;\n" +
    "           read=(read+1)%INTERNAL_QUEUE_SIZE;\n" +
    "           size--;\n" +
    "           } else {\n" +
    "               fromfirst=false;\n" +
    "           }\n" +
    "           \n" +
    "       }\n" +
    "       for (int i=0;i<size;i++) {\n" +
    "       index=(read+i)%INTERNAL_QUEUE_SIZE;\n" +
    "           if (elements[index]==t) {\n" +
    "               elements[index]=event_int();\n" +
    "               found++;\n" +
    "           }\n" +
    "           else if (found>0) {\n" +
    "               elements[(index-found+INTERNAL_QUEUE_SIZE)%INTERNAL_QUEUE_SIZE]=elements[index];\n" +
    "               elements[index]=event_int();\n" +
    "           }\n" +
    "       }\n" +
    "       write=(write-found+INTERNAL_QUEUE_SIZE)%INTERNAL_QUEUE_SIZE;\n" +
    "       size=size-found;\n" +
    "       \n" +
    "   }\n" +
    "};\n" +
    "\n" +
    "\n" +
    "\n" +
    "#endif /*EVENTQUEUE_H_*/\n";
    return result;
  }

  private static class ActionTranslator implements UAction.InContextVisitor<CSequence> {
    public static final boolean DEBUG = SmileTranslator.DEBUG;
    private UContext context;
    private ClassifierSymbolTable symbolTable;

    /**
     * Create a new UML action translator.
     * 
     * @param symbolTable class symbol table to use
     * @param context UML Context for this action
     */
    public ActionTranslator(ClassifierSymbolTable symbolTable, UContext context) {
      this.symbolTable = symbolTable;
      this.context = context;
    }

    /**
     * Translate a UML action into a Java block statement.
     * 
     * @param action action to be translated
     * @return Java block statement that represents <CODE>action</CODE>
     */
    public CSequence translate(UAction action) {
      return action.accept(this);
    }

    /**
     * Determine the underlying context of this UML action translator.
     * 
     * @returns UML action translator's underlying context
     */
    public UContext getContext() {
      return this.context;
    }

    public CSequence onSkip() {
      return new CSequence();
    }

    public CSequence onChoice(UAction left, UAction right) {
      return translate(left);
    }

    public CSequence onParallel(UAction left, UAction right) {
      return translate(UAction.par(left, right).getInterleavingNormalForm());
    }

    public CSequence onSequential(UAction left, UAction right) {
      CSequence statements = translate(left);
      statements.addBlockStatement(new CCode(" // sequential"));
      statements.addBlockStatement(translate(right));
      return statements;
    }

    public CSequence onConditional(UExpression condition, UAction left, UAction right) {
      CSequence statements = new CSequence();
      String cCondition = UMLTranslator.translateExpression(condition, this.context, this.symbolTable);
      CSequence cLeft = UMLTranslator.translateAction(left, this.context, this.symbolTable);
      CSequence cRight = null;
      if (right != null && !right.isSkip())
        cRight = UMLTranslator.translateAction(right, this.context, this.symbolTable);
      statements.addBlockStatement(new CIfElse(cCondition, cLeft.asStatement(), cRight != null ? cRight.asStatement() : null));
      return statements;
    }

    public CSequence onAssertion(UExpression expression) {
      CSequence statements = new CSequence();
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("assert(");
      resultBuilder.append(new ExpressionTranslator(this.symbolTable, this.context).translate(expression));
      resultBuilder.append(");");
      statements.addBlockStatement(new CCode(resultBuilder.toString()));
      return statements;
    }

    public CSequence onSelfInvocation(UBehavioural behavioural, List<UExpression> parameters) {
      return createBehaviouralCall("sc_id", behavioural, parameters);
    }

    public CSequence onInvocation(UStatual statual, UBehavioural behavioural, List<UExpression> parameters) {
      return createBehaviouralCall(statual.getName(), behavioural, parameters);
    }

    private CSequence createBehaviouralCall(String sender, UBehavioural behavioural, List<UExpression> parameters) {
      CSequence statements = new CSequence();
      ClassifierSymbolTable foreign = symbolTable.getUMLClass(behavioural.getOwner());
      CBlock block = new CBlock();
      if (behavioural instanceof UReception) {
        block.addBlockStatement(new CCode("event sc_tmp_ev = (event(" + foreign.getSignalID(behavioural) + ", event::SIGNAL));"));
      }
      else { 
        block.addBlockStatement(new CCode("event sc_tmp_ev = (event(" + foreign.getSignalID(behavioural) + ", event::CALL));"));
      }
      block.addBlockStatement(new CCode("sc_tmp_ev.sender = " + sender + ";"));

      for (int i=0; i < parameters.size(); i++) {
        UExpression exp = parameters.get(i);
        block.addBlockStatement(new CCode("sc_tmp_ev.parm[" + i + "] = " + translateExpression(exp) + ";"));
      }
      //block.addStatement(new CCode("cout << \""+symbolTable.getcppClass().getName()+": enqueuing in  queue\" << outgoing << \"\\n\";"));
      block.addBlockStatement(new CCode("outgoing.enqueue(sc_tmp_ev);"));
      statements.addBlockStatement(block);
      if (behavioural instanceof UOperation){
        statements.addBlockStatement(new SCRestart());
        if (DEBUG)
          statements.addBlockStatement(new CCode(syscDebug(this.symbolTable.getClassifier().getName() + "acknowledged!")));
      }
      return statements;
    }

    public CSequence onArrayInvocation(UAttribute attribute, UExpression offset, UBehavioural behavioural, List<UExpression> parameters) {
      CSequence statements = new CSequence();
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(this.translateStatual(attribute) + "[" + translateExpression(offset) + "].");
      resultBuilder.append(this.translateBehaviouralCall(behavioural, parameters));
      statements.addBlockStatement(new CCode(resultBuilder.toString()));
      return statements;
    }

    public CSequence onAssignment(UAttribute attribute, UExpression expression) {
      CSequence statements = new CSequence();
      statements.addBlockStatement(new CCode(translateStatual(attribute) + " = " + translateExpression(expression) + ";"));
      return statements;
    }

    public CSequence onArrayAssignment(UAttribute attribute, UExpression offset, UExpression expression) {
      CSequence statements = new CSequence();
      statements.addBlockStatement(new CCode(translateStatual(attribute) + "[" + translateExpression(offset) + "] = " + translateExpression(expression) + ";"));
      return statements;
    }

    public CSequence onPost(UAttribute attribute, UAction.PostKind kind) {
      CSequence statements = new CSequence();
      statements.addBlockStatement(new CCode(translateStatual(attribute) + ((kind == UAction.PostKind.DEC) ? "--;" : "++;")));
      return statements;
    }

    public CSequence onArrayPost(UAttribute attribute, UExpression offset, UAction.PostKind kind) {
      CSequence statements = new CSequence();
      statements.addBlockStatement(new CCode(translateStatual(attribute) + "[" + translateExpression(offset) + "]" + ((kind == UAction.PostKind.DEC) ? "--;" : "++;")));
      return statements;
    }

    /**
     * Translate a UML expression.
     * 
     * @param expression UML expression
     * @return Java code for <CODE>expression</CODE>
     */
    private String translateExpression(UExpression uExpression) {
      return new ExpressionTranslator(this.symbolTable, this.context).translate(uExpression);
    }

    /**
     * Translate a UML statual.
     * 
     * @param statual UML statual
     * @return Java code for <CODE>statual</CODE>
     */
    private String translateStatual(UStatual uStatual) {
      ExpressionTranslator expressionTranslator = new ExpressionTranslator(symbolTable, this.context);
      expressionTranslator.onStatual(uStatual);
      return expressionTranslator.getResult();
    }

    /**
     * Translate a UML behavioural call.
     * 
     * @param uBehavioural a UML behavioural
     * @param uArguments a list of UML argument expression
     * @return SystemC code for call to <CODE>uBehavioural</CODE> with <CODE>uArguments</CODE>
     */
    private String translateBehaviouralCall(UBehavioural uBehavioural, List<UExpression> uArguments) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(this.symbolTable.getMethodName(uBehavioural));
      resultBuilder.append(Formatter.tuple(uArguments, uArgument -> this.translateExpression(uArgument)));
      resultBuilder.append(";");
      return resultBuilder.toString();
    }
  }

  /**
   * Translate a UML action into a Java block statement.
   * 
   * @param action a UML action
   * @param context a UML context
   * @param symbolTable a class symbol table
   * @return Java block statement for {@code action}
   */
  public static CSequence translateAction(UAction action, UContext context, ClassifierSymbolTable symbolTable) {
    return new ActionTranslator(symbolTable, context).translate(action);
  }

  /**
   * Translator for UML expressions.
   */
  private static class ExpressionTranslator implements UExpression.InContextVisitor<@Nullable Void> {
    private ClassifierSymbolTable symboltable;
    private UContext context;
    private StringBuilder resultBuilder;

    /**
     * Constructor
     * 
     * @param symboltable ModelSymbolTable of UML Class
     * @param context UML Context
     */
    public ExpressionTranslator(ClassifierSymbolTable symboltable, UContext context) {
      resultBuilder = new StringBuilder();
      this.symboltable = symboltable;
      this.context = context;
    }

    /**
     * @return result of translation
     */
    public String getResult() {
      return resultBuilder.toString();
    }

    /**
     * Translate a UML expression into a Java expression string.
     *
     * @param uExpression UML expression
     * @return Java expression string for <CODE>uExpression</CODE>
     */
    private String translate(UExpression uExpression) {
      uExpression.accept(this);
      return resultBuilder.toString();
    }

    /**
     * Returns the UML context.
     * 
     * @return UML context
     */
    public UContext getContext() {
      return context;
    }

    public @Nullable Void onBooleanConstant(boolean booleanConstant) {
      if (booleanConstant)
        resultBuilder.append("true");
      else
        resultBuilder.append("false");
      return null;
    }

    public @Nullable Void onIntegerConstant(int integerConstant) {
      resultBuilder.append(integerConstant);
      return null;
    }

    public @Nullable Void onStringConstant(String stringConstant) {
      resultBuilder.append("\"" + stringConstant + "\"");
      return null;
    }

    public @Nullable Void onNull() {
      resultBuilder.append("NULL");
      return null;
    }

    public @Nullable Void onThis() {
      resultBuilder.append("");
      return null;
    }

    /**
     * Translate UML statual.
     * 
     * If the statual is a class attribute, this method gets the real name from
     * the translator's model system table. Otherwise it is assumed that the
     * statual is a parameter, which was delivered by a reception or operation.
     * In this case, the method gets the value of the parameter of the current
     * event.
     * 
     * @param statual UML attribute or parameter of an operation or reception
     */
    public @Nullable Void onStatual(UStatual uStatual) {
      uStatual.cases(uAttribute -> resultBuilder.append(symboltable.getAttributeName(uAttribute)),
                     uParameter -> { resultBuilder.append("current.parm[");
                                     resultBuilder.append(symboltable.getParameterID(uParameter));
                                     resultBuilder.append("]");
                                     return resultBuilder; },
                     uObject -> resultBuilder);
      return null;
    }

    /**
     * Translate an access to an array attribute
     */
    public @Nullable Void onArray(UAttribute attribute, UExpression offset) {
      onStatual(attribute);
      resultBuilder.append("[");
      translate(offset);
      resultBuilder.append("]");
      return null;
    }

    /**
     * Translate conditional expression.
     */
    public @Nullable Void onConditional(UExpression conditionExpression, UExpression trueExpression, UExpression falseExpression) {
      resultBuilder.append("((");
      translate(conditionExpression);
      resultBuilder.append(") ? ");
      translate(trueExpression);
      resultBuilder.append(" : ");
      translate(falseExpression);
      resultBuilder.append(")");
      return null;
    }

    /**
     * Translates unary expression
     */
    public @Nullable Void onUnary(UOperator unary, UExpression expression) {
      resultBuilder.append("(");
      resultBuilder.append(unary.getName());
      translate(expression);
      resultBuilder.append(")");
      return null;
    }

    /**
     * Translates an arithmetical operation between two expressions
     */
    public @Nullable Void onArithmetical(UExpression leftExpression, UOperator arithmetical, UExpression rightExpression) {
      resultBuilder.append("(");
      translate(leftExpression);
      resultBuilder.append(arithmetical.getName());
      translate(rightExpression);
      resultBuilder.append(")");
      return null;
    }

    /**
     * Translates a relational operation between two expressions
     */
    public @Nullable Void onRelational(UExpression leftExpression, UOperator relational, UExpression rightExpression) {
      resultBuilder.append("(");
      translate(leftExpression);
      resultBuilder.append(relational.getName());
      translate(rightExpression);
      resultBuilder.append(")");
      return null;
    }

    /**
     * Translates a junctional operation between two expressions
     */
    public @Nullable Void onJunctional(UExpression leftExpression, UOperator junctional, UExpression rightExpression) {
      resultBuilder.append("(");
      translate(leftExpression);
      resultBuilder.append(junctional.getName());
      translate(rightExpression);
      resultBuilder.append(")");
      return null;
    }
  }

  /**
   * Translate a UML expression into a Java expression string.
   * 
   * @param uExpression a UML expression
   * @param context a UML context
   * @param symbolTable a class symbol table
   * @return Java expression string for <CODE>expression</CODE>
   */
  public static String translateExpression(UExpression uExpression, UContext context, ClassifierSymbolTable symbolTable) {
    String jExpression = new ExpressionTranslator(symbolTable, context).translate(uExpression);
    return jExpression;
  }

  static String syscDebug(String text) {
    return SmileTranslator.syscDebug(text);
  }
}
