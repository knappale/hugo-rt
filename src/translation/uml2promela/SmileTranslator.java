package translation.uml2promela;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import promela.PConstant;
import promela.PExpression;
import promela.POperator;
import promela.PProcess;
import promela.PStatement;
import promela.PVariable;
import uml.UAction;
import uml.UAttribute;
import uml.UContext;
import uml.UExpression;
import uml.smile.SBranch;
import uml.smile.SConstant;
import uml.smile.SGuard;
import uml.smile.SLiteral;
import uml.smile.SMachine;
import uml.smile.SStatement;
import uml.smile.SValue;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UState;


/**
 * Translator of Smile state machines into Promela
 */
@NonNullByDefault
public class SmileTranslator {
  private SmileTranslator() {
  }

  /**
   * Translate Smile machine into Promela
   * 
   * @param sMachine Smile machine
   * @param symbolTable symbol table for translation of Smile
   * @return translated Promela process
   */
  static PProcess translateMachine(SMachine sMachine, StateMachineSymbolTable symbolTable) {
    // Create statement
    PStatement pStatement = PStatement.skip();
    if (!symbolTable.getProperties().isMutex()) {
      // We cannot declare exclusive read access to our own event queue, as other
      // processes may check for "full" and "nfull"
      pStatement = PStatement.sequence(pStatement, PStatement.exclusiveRead(PExpression.channel(symbolTable.getEventQueueChannel())));
    }
    var sStatement = sMachine.getStatement();
    pStatement = PStatement.sequence(pStatement, translateStatement(sStatement, symbolTable));
    pStatement = PStatement.sequence(pStatement, symbolTable.getEndStatement());

    // Create process
    PProcess pProcess = symbolTable.getProcess();
    pProcess.addChanDecls(symbolTable.getChannels());
    pProcess.addVarDecls(symbolTable.getVariables());
    pProcess.setStatement(pStatement);
    pProcess.addParam(symbolTable.getEventQueueChannel());
    pProcess.addParam(symbolTable.getThisVariable());
    for (PVariable variableParameter : symbolTable.getVariableParameters())
      pProcess.addParam(variableParameter);

    return pProcess;
  }

  /**
   * Translation of Smile expression into Promela.
   */
  private static final class ExpressionTranslator implements SGuard.Visitor<PExpression>, SLiteral.Visitor<PExpression> {
    private StateMachineSymbolTable symbolTable;

    ExpressionTranslator(StateMachineSymbolTable symbolTable) {
      this.symbolTable = symbolTable;
    }

    PExpression expression(SGuard sExpression) {
      return sExpression.accept(this);
    }

    @Override
    public PExpression onBooleanConstant(boolean booleanConstant) {
      return booleanConstant ? PExpression.trueConst() : PExpression.falseConst();
    }

    @Override
    public PExpression onEq(boolean positive, SVariable sVariable, Set<SValue> sValues) {
      PExpression eq = sValues.stream().map(sValue -> PExpression.eq(PExpression.variable(symbolTable.getVariable(sVariable)), translateValue(sValue, symbolTable))).
                                        reduce(PExpression.falseConst(), PExpression::or);
      return positive ? eq : PExpression.not(eq);
    }

    @Override
    public PExpression onMatch(boolean positive, Set<UEvent> uEvents) {
      PExpression match = PExpression.falseConst();
      for (UEvent uEvent : uEvents) {
        PExpression eventsMatch = PExpression.falseConst();
        // If UML completion event for multiple UML states
        if (uEvent.isCompletion() && uEvent.getStates().size() > 1) {
          for (UState uState : uEvent.getStates())
            eventsMatch = PExpression.or(eventsMatch, PExpression.eq(PExpression.variable(symbolTable.getStimulusVariable()), PExpression.constant(symbolTable.getConstant(UEvent.completion(uState)))));
        }
        else
          eventsMatch = PExpression.eq(PExpression.variable(symbolTable.getStimulusVariable()), PExpression.constant(symbolTable.getConstant(uEvent)));
        match = PExpression.or(match, eventsMatch);
      }
      return positive ? match : PExpression.not(match);
    }

    @Override
    public PExpression onIsCompleted(boolean positive, UState uState) {
      PExpression pCompleted = PExpression.bracket(PExpression.variable(symbolTable.getCompletedVariable()), PExpression.constant(symbolTable.getCompletionConstant(uState)));
      return PExpression.eq(pCompleted, positive ? PExpression.trueConst() : PExpression.falseConst());
    }

    @Override
    public PExpression onIsTimedOut(boolean positive, UEvent uEvent) {
      PExpression pTimedOut = PExpression.bracket(PExpression.variable(symbolTable.getTimedOutVariable()), PExpression.constant(symbolTable.getTimerConstant(uEvent)));
      return PExpression.eq(pTimedOut, positive ? PExpression.trueConst() : PExpression.falseConst());
    }

    @Override
    public PExpression onExternal(boolean positive, UExpression uExpression, UContext uContext) {
      PExpression external = UMLTranslator.translateExpression(uExpression, uContext, this.symbolTable);
      return positive ? external : PExpression.not(external);
    }

    @Override
    public PExpression onLiteral(SLiteral literal) {
      return literal.accept(this);
    }

    @Override
    public PExpression onAnd(SGuard sLeftExpression, SGuard sRightExpression) {
      return PExpression.and(expression(sLeftExpression), expression(sRightExpression));
    }

    @Override
    public PExpression onOr(SGuard sLeftExpression, SGuard sRightExpression) {
      return PExpression.or(expression(sLeftExpression), expression(sRightExpression));
    }
  }

  /**
   * Translate Smile expression into Promela which origins from a guard of a
   * Smile branch statement
   * 
   * @param sGuard Smile guard
   * @param symbolTable symbol table for translation of Smile
   * @return translated Promela expression
   */
  private static PExpression translateGuard(SGuard sGuard, StateMachineSymbolTable symbolTable) {
    return new ExpressionTranslator(symbolTable).expression(sGuard);
  }

  /**
   * Translate Smile value into Promela.
   * 
   * @param sValue Smile value
   * @param symbolTable symbol table for translation of Smile
   * @return translated Promela expression
   */
  private static PExpression translateValue(SValue sValue, StateMachineSymbolTable symbolTable) {
    return sValue.new Cases<PExpression>().booleanConstant(booleanConstant -> booleanConstant ? PExpression.trueConst() : PExpression.falseConst()).
                                           constant(sConstant -> PExpression.constant(symbolTable.getConstant(sConstant))).
                                           apply();
  }

  /**
   * Translation of Smile statements into Promela.
   */
  private static final class StatementTranslator implements SStatement.Visitor<PStatement> {
    private StateMachineSymbolTable symbolTable;
    private Stack<SStatement> sStatements = new Stack<>();

    private StatementTranslator(StateMachineSymbolTable symbolTable) {
      this.symbolTable = symbolTable;
    }

    private PStatement statement(SStatement sStatement) {
      this.sStatements.push(sStatement);
      PStatement pStatement = sStatement.accept(this);
      this.sStatements.pop();
      return pStatement;
    }

    @Override
    public PStatement onSkip() {
      return PStatement.skip();
    }

    @Override
    public PStatement onSuccess() {
      return PStatement.gotoStm(this.symbolTable.getEndStatement());
    }

    @Override
    public PStatement onFail() {
      return PStatement.assertion(PExpression.falseConst());
    }

    @Override
    public PStatement onAwait(SGuard sExpression) {
      return PStatement.expression(SmileTranslator.translateGuard(sExpression, this.symbolTable));
    }

    @Override
    public PStatement onBreak() {
      return PStatement.breakStm();
    }

    @Override
    public PStatement onAtomic(SStatement sStm) {
      return PStatement.atomic(this.statement(sStm));
    }

    @Override
    public PStatement onSequential(SStatement sLeftStm, SStatement sRightStm) {
      return PStatement.sequence(this.statement(sLeftStm), this.statement(sRightStm));
    }

    @Override
    public PStatement onChoice(List<SBranch> sBranches, @Nullable SStatement sElseStm) {
      PStatement pStatement = PStatement.selection();
      for (SBranch sBranch : sBranches)
        pStatement.addBranch(this.onBranch(sBranch.getGuard(), sBranch.getEffect()));
      if (sElseStm != null)
        pStatement.setElseBranch(this.statement(sElseStm));
      return pStatement;
    }

    @Override
    public PStatement onLoop(List<SBranch> sBranches, @Nullable SStatement sElseStm) {
      PStatement pStatement = PStatement.repetition();
      for (SBranch sBranch : sBranches)
        pStatement.addBranch(this.onBranch(sBranch.getGuard(), sBranch.getEffect()));
      if (sElseStm != null)
        pStatement.setElseBranch(this.statement(sElseStm));
      return pStatement;
    }

    @Override
    public PStatement onBranch(SGuard sGuard, SStatement sBranchStm) {
      PStatement pStatement = null;
      pStatement = PStatement.expression(SmileTranslator.translateGuard(sGuard, this.symbolTable));
      pStatement = PStatement.sequence(pStatement, this.statement(sBranchStm));
      return pStatement;
    }

    @Override
    public PStatement onAssignment(SVariable sVariable, SValue sValue) {
      return PStatement.assignment(PExpression.variable(this.symbolTable.getVariable(sVariable)), translateValue(sValue, this.symbolTable));
    }

    @Override
    public PStatement onAssignment(SVariable sVariable, SVariable otherSVariable) {
      return PStatement.assignment(PExpression.variable(this.symbolTable.getVariable(sVariable)), PExpression.variable(symbolTable.getVariable(otherSVariable)));
    }

    @Override
    public PStatement onExternal(UAction uExternal, UContext uContext) {
      return UMLTranslator.translateAction(uExternal, uContext, this.symbolTable);
    }

    /**
     * Compute a statement for fetching an event from the event queue.
     * 
     * The algorithm is the following: First, we look whether there is an
     * internal event to be handled; these have to be prioritised. Internal
     * events are either completion or time events. If there is an internal
     * event to be handled it is made the active stimulus.
     * 
     * If there is no internal event, we check for deferred events, if the state
     * machines shows any. A deferred event will only be made the active
     * stimulus if any other event has been consumed between deferring the event
     * and this lookup.
     * 
     * If there is no internal event and no deferred event, we look for external
     * events coming from the external queue. If there is an external event to
     * be handled, we make it the active stimulus. Simultaneously, if there are
     * wait states, a completion event for such a wait state can be generated
     * and made the active stimulus.
     */
    @Override
    public PStatement onFetch(Set<UState> waitStates) {
      PExpression currentEvent = PExpression.variable(this.symbolTable.getStimulusVariable());
      PStatement resetStatement = PStatement.assignment(currentEvent, PExpression.constant(this.symbolTable.getEmptyConstant()));

      PExpression senderVariable = PExpression.variable(this.symbolTable.getSenderVariable());
      PStatement resetSender = PStatement.assignment(senderVariable, PExpression.constant(this.symbolTable.getEmptyConstant()));
      resetStatement = PStatement.sequence(resetStatement, resetSender);

      // Polling needed because of else-branch for external events (see Spin
      // manual on else)
      PExpression internalGuard = pollInternalQueue();
      PStatement internalStep = receiveInternalQueue();
      PStatement internalBranch = PStatement.branch(internalGuard, internalStep);

      PStatement deferredBranch = null;
      if (this.symbolTable.hasDeferrableEvents()) {
        PExpression deferred_queue = PExpression.channel(this.symbolTable.getDeferredQueueChannel());
        PExpression deferred_counter = PExpression.variable(this.symbolTable.getDeferredCounterVariable());

        PExpression deferredGuard = PExpression.binary(PExpression.len(deferred_queue), POperator.GT, deferred_counter);
        PStatement deferredStep = receiveNonInternalQueue(deferred_queue);
        deferredBranch = PStatement.branch(deferredGuard, deferredStep);
      }

      List<PStatement> waitBranches = new ArrayList<>();
      if (waitStates != null && !waitStates.isEmpty()) {
        for (UState waitState : waitStates) {
          PExpression waitStateVariable = PExpression.variable(this.symbolTable.getVertexVariable(waitState));
          PExpression waitStateConstant = PExpression.constant(this.symbolTable.getConstant(SConstant.vertex(waitState)));
          PExpression waitStateCompleted = this.symbolTable.getProperties().doDoubleChecks()
                                             ? PExpression.bracket(PExpression.variable(symbolTable.getCompletedVariable()), PExpression.constant(symbolTable.getCompletionConstant(waitState)))
                                             : PExpression.trueConst();

          PExpression waitGuard = PExpression.and(PExpression.eq(waitStateVariable, waitStateConstant), waitStateCompleted);
          PStatement waitStep = PStatement.assignment(currentEvent, PExpression.constant(this.symbolTable.getConstant(UEvent.completion(waitState))));
          PStatement waitBranch = PStatement.branch(waitGuard, waitStep);

          waitBranches.add(waitBranch);
        }
      }

      PExpression event_queue = PExpression.channel(this.symbolTable.getEventQueueChannel());
      List<PStatement> externalBranches = new ArrayList<>();
      if (symbolTable.getProperties().isMutex()) {
        PExpression delayedExternalGuard = PExpression.not(pollNonInternalQueue(event_queue));
        PStatement delayedExternalStep = PStatement.sequence(symbolTable.exitMutex(), PStatement.sequence(receiveNonInternalQueue(event_queue), symbolTable.enterMutex()));
        // if (this.symbolTable.hasObserver()) {
        //   delayedExternalStep = PStatement.sequence(delayedExternalStep, this.sendReceiveNotification());
        // }
        PStatement delayedExternalBranch = PStatement.branch(delayedExternalGuard, delayedExternalStep);
        externalBranches.add(delayedExternalBranch);

        PExpression immediateExternalGuard = pollNonInternalQueue(event_queue);
        PStatement immediateExternalStep = receiveNonInternalQueue(event_queue);
        // if (this.symbolTable.hasObserver()) {
        //   immediateExternalStep = PStatement.sequence(immediateExternalStep, this.sendReceiveNotification());
        // }
        PStatement immediateExternalBranch = PStatement.branch(immediateExternalGuard, immediateExternalStep);
        externalBranches.add(immediateExternalBranch);
      }
      else {
        PStatement externalStep = receiveNonInternalQueue(event_queue);
        externalBranches.add(externalStep);
      }

      // Waits and fetching from the external queue are treated on the same level
      List<PStatement> waitAndExternalBranches = new ArrayList<>();
      waitAndExternalBranches.addAll(waitBranches);
      waitAndExternalBranches.addAll(externalBranches);
      PStatement waitExternal = PStatement.selection(waitAndExternalBranches, null);

      // Deferred events take priority
      PStatement deferredWaitExternal = waitExternal;
      if (deferredBranch != null) {
        deferredWaitExternal = PStatement.selection(deferredBranch, waitExternal);
      }

      PStatement fetch = PStatement.selection(internalBranch, deferredWaitExternal);

      PStatement pStatement = fetch;
      if (this.symbolTable.getProperties().isMutex())
        pStatement = PStatement.sequence(this.symbolTable.yieldMutex(), pStatement);
      pStatement = PStatement.sequence(resetStatement, pStatement);

      return pStatement;
      // return PStatement.call(this.symbolTable.addInline("fetch", new ArrayList<>(), fetch), new ArrayList<>());
    }

    @Override
    public PStatement onInitialisation() {
      PStatement pStatement = null;
      for (UAttribute uAttribute : this.symbolTable.getOrderedAttributes()) {
        if (!uAttribute.isFinal() && !uAttribute.isStatic()) {
          PExpression instanceVar = PExpression.variable(this.symbolTable.getVariable(uAttribute));

          if (uAttribute.getType().isSimple()) {
            PExpression initialiser = PExpression.variable(this.symbolTable.getInitialisationVariable(uAttribute, 0));
            PStatement assignStm = PStatement.assignment(instanceVar, initialiser);
            pStatement = (pStatement == null) ? assignStm : PStatement.sequence(pStatement, assignStm);
          }
          else { // Is array
            // Simultaneous initialisation of components
            for (int i = 0; i < uAttribute.getSize(); i++) {
              PExpression initialiser = PExpression.variable(this.symbolTable.getInitialisationVariable(uAttribute, i));
              PStatement assignStm = PStatement.assignment(PExpression.bracket(instanceVar, PExpression.constant(PConstant.numeric(i))), initialiser);
              pStatement = (pStatement == null) ? assignStm : PStatement.sequence(pStatement, assignStm);
            }
          }
        }
      }
      if (pStatement == null) { // Nothing initialised
        pStatement = PStatement.skip();
      }
      if (symbolTable.getProperties().isMutex())
        pStatement = PStatement.sequence(symbolTable.enterMutex(), pStatement);
      return pStatement;
    }

    @Override
    public PStatement onComplete(boolean keep, UState uState) {
      PStatement pStatement = PStatement.skip();
      if (symbolTable.getCompletionTree().isCompletionState(uState)) {
        if (keep)
          pStatement = PStatement.assignment(PExpression.bracket(PExpression.variable(symbolTable.getCompletedVariable()), PExpression.constant(symbolTable.getCompletionConstant(uState))), PExpression.trueConst());

        // Only add a completion event, if there is an exiting completion
        // transition.
        if (uState.getOutgoingCompletions().size() > uState.getOutgoingWaits().size()) {
          UEvent uCompletionEvent = UEvent.completion(uState);
          pStatement = PStatement.sequence(pStatement, this.addInternalEvent(uCompletionEvent));
        }
      }
      return pStatement;
    }

    @Override
    public PStatement onUncomplete(UState uState) {
      PStatement pStatement = PStatement.skip();
      if (symbolTable.getCompletionTree().isCompletionState(uState)) {
        pStatement = PStatement.assignment(PExpression.bracket(PExpression.variable(symbolTable.getCompletedVariable()), PExpression.constant(symbolTable.getCompletionConstant(uState))), PExpression.falseConst());

        // Only remove a completion event, if there is an exiting completion
        // transition and some completion transition targets a join pseudo-state.
        if ((uState.getOutgoingCompletions().size() > uState.getOutgoingWaits().size()) && uState.hasOutgoingJoinCompletion()) {
          UEvent uCompletionEvent = UEvent.completion(uState);
          pStatement = PStatement.sequence(pStatement, this.removeInternalEvent(uCompletionEvent));
        }
      }
      return pStatement;
    }

    @Override
    public PStatement onChosen() {
      PStatement pResetDeferredCounter = null;
      if (this.symbolTable.hasDeferrableEvents()) {
        PExpression deferred_counter = PExpression.variable(this.symbolTable.getDeferredCounterVariable());
        PExpression exp = PExpression.constant(PConstant.numeric(0));
        pResetDeferredCounter = PStatement.assignment(deferred_counter, exp);
      }

      PStatement pSendReceiveNotification = null;
      // Chosen is only generated in the context of a behavioural event
      if (this.symbolTable.isObserved())
        pSendReceiveNotification = this.sendReceiveNotification();
      /*
      if (this.symbolTable.isObserved() && this.symbolTable.getNetworkEventsNumber() > 0) {
        pSendReceiveNotification = PStatement.selection(PStatement.branch(this.isBehaviouralGuard(), this.sendReceiveNotification()), PStatement.skip());
      }
      */

      PStatement pChosen = pResetDeferredCounter;
      if (pSendReceiveNotification != null) {
        if (pChosen == null)
          pChosen = pSendReceiveNotification;
        else
          pChosen = PStatement.sequence(pChosen, pSendReceiveNotification);
      }
      if (pChosen == null)
        pChosen = PStatement.skip();

      return pChosen;
    }

    @Override
    public PStatement onDefer() {
      PStatement pStatement = null;

      // Check whether deferred queue is full
      var deferred_queue = PExpression.channel(this.symbolTable.getDeferredQueueChannel());
      if (this.symbolTable.getProperties().isMutex())
        pStatement = this.symbolTable.checkFull(deferred_queue);

      var current_event = PExpression.variable(this.symbolTable.getStimulusVariable());
      var pStep = PStatement.send(deferred_queue);
      pStep.addSendStmArg(current_event);
      pStep.addSendStmArg(PExpression.variable(this.symbolTable.getSenderVariable()));
      for (int i = 0; i < this.symbolTable.getMaxParameters(); i++)
        pStep.addSendStmArg(PExpression.bracket(PExpression.variable(this.symbolTable.getParametersVariable()), PExpression.constant(PConstant.numeric(i))));
      if (this.symbolTable.hasOperations())
        pStep.addSendStmArg(PExpression.channel(this.symbolTable.getAcknowledgeOutChannel()));
      pStatement = (pStatement == null) ? pStep : PStatement.sequence(pStatement, pStep);

      pStep = PStatement.expression(PExpression.increment(PExpression.variable(this.symbolTable.getDeferredCounterVariable())));

      pStatement = (pStatement == null) ? pStep : PStatement.sequence(pStatement, pStep);

      return pStatement;
    }

    @Override
    public PStatement onAcknowledge() {
      PStatement pAcknowledge = PStatement.send(PExpression.channel(this.symbolTable.getAcknowledgeOutChannel()));
      pAcknowledge.addSendStmArg(PExpression.constant(this.symbolTable.getAckOutSendArgConstant()));
      PStatement pStatement = pAcknowledge;
      return pStatement;
    }

    @Override
    public PStatement onStartTimer(boolean keep, UEvent uEvent) {
      PStatement pStatement = null;
      PStatement pStep = null;

      PExpression event_queue = PExpression.channel(symbolTable.getEventQueueChannel());

      // Check whether event queue is full
      if (this.symbolTable.getProperties().isMutex()) {
        pStep = this.symbolTable.checkFull(event_queue);
        pStatement = pStep;
      }

      PExpression ack_in = PExpression.channel(symbolTable.getAcknowledgeInChannel());
      PExpression sendStmArg = PExpression.constant(this.symbolTable.getConstant(uEvent));
      pStep = PStatement.send(event_queue);
      pStep.addSendStmArg(sendStmArg);
      // Sender
      pStep.addSendStmArg(symbolTable.getThisExpression());
      // Parameters
      for (int i = 0; i < symbolTable.getMaxParameters(); i++)
        pStep.addSendStmArg(PExpression.constant(symbolTable.getEmptyConstant()));
      // Possibly acknowledgment channel
      if (this.symbolTable.hasOperations())
        pStep.addSendStmArg(ack_in);
      pStep = PStatement.sequence(pStep, PStatement.assignment(PExpression.bracket(PExpression.variable(symbolTable.getTimedOutVariable()), PExpression.constant(symbolTable.getTimerConstant(uEvent))), PExpression.trueConst()));
      pStatement = (pStatement == null) ? pStep : PStatement.sequence(pStatement, pStep);

      return pStatement;
    }

    @Override
    public PStatement onStopTimer(UEvent uEvent) {
      PExpression eventQueue = PExpression.channel(symbolTable.getEventQueueChannel());

      PExpression removeGuard = randomPollNonInternalQueue(eventQueue);
      /*
      PExpression removeGuard = PExpression.randomPollNoSideEffect(eventQueue);
      removeGuard.addPollNoSideEffectArg(PExpression.constant(this.symbolTable.getConstant(uEvent)));
      // Possibly the sender
      if (this.symbolTable.hasObserver())
        removeGuard.addPollNoSideEffectArg(PExpression.variable(PVariable.uscore()));
      // Parameters
      for (int i = 0; i < symbolTable.getMaxParameters(); i++)
        removeGuard.addPollNoSideEffectArg(PExpression.variable(PVariable.uscore()));
      // Possibly acknowledgment channel
      if (this.symbolTable.hasOperations())
        removeGuard.addPollNoSideEffectArg(PExpression.channel(symbolTable.getAcknowledgeOutChannel()));
      */

      PStatement removeStatement = randomReceiveNonInternalQueue(eventQueue);
      /*
      PStatement removeStatement = PStatement.randomReceive(eventQueue);
      removeStatement.addRecvStmArg(PExpression.constant(this.symbolTable.getConstant(uEvent)));
      // Possibly the sender
      if (this.symbolTable.hasObserver())
        removeStatement.addRecvStmArg(PExpression.variable(PVariable.uscore()));
      // Parameters
      for (int i = 0; i < symbolTable.getMaxParameters(); i++)
        removeStatement.addRecvStmArg(PExpression.variable(PVariable.uscore()));
      // Possibly acknowledgment channel
      if (this.symbolTable.hasOperations())
        removeStatement.addRecvStmArg(PExpression.channel(symbolTable.getAcknowledgeOutChannel()));
      */

      PStatement removeBranch = PStatement.sequence(PStatement.expression(removeGuard), removeStatement);
      PStatement pStatement = PStatement.selection();
      pStatement.addBranch(removeBranch);
      pStatement.setElseBranch(PStatement.skip());

      pStatement = PStatement.sequence(pStatement, PStatement.assignment(PExpression.bracket(PExpression.variable(symbolTable.getTimedOutVariable()), PExpression.constant(symbolTable.getTimerConstant(uEvent))), PExpression.falseConst()));
      return pStatement;
    }

    private PStatement addInternalEvent(UEvent uEvent) {
      PStatement pStatement = null;
      PStatement pStep = null;

      PExpression internal_queue = PExpression.channel(this.symbolTable.getInternalQueueChannel());

      // Check whether internal queue is full
      if (this.symbolTable.getProperties().isMutex()) {
        pStep = this.symbolTable.checkFull(internal_queue);
        pStatement = pStep;
      }

      // Add event to internal queue
      PExpression sendStmArg = PExpression.constant(this.symbolTable.getConstant(uEvent));
      pStep = PStatement.send(internal_queue);
      pStep.addSendStmArg(sendStmArg);
      pStatement = (pStatement == null) ? pStep : PStatement.sequence(pStatement, pStep);

      return pStatement;
    }

    private PStatement removeInternalEvent(UEvent uEvent) {
      var internalQueue = PExpression.channel(this.symbolTable.getInternalQueueChannel());
      var eventConstant = PExpression.constant(this.symbolTable.getConstant(uEvent));
      return PStatement.repetition()
                       .addBranch(PStatement.sequence(PStatement.expression(PExpression.randomPollNoSideEffect(internalQueue)
                                                                                       .addPollNoSideEffectArg(eventConstant)),
                                                      PStatement.randomReceive(internalQueue)
                                                                .addRecvStmArg(eventConstant)))
                       .setElseBranch(PStatement.breakStm());
    }

    private PStatement sendReceiveNotification() {
      if (!this.symbolTable.isObserved())
        return PStatement.skip();

      PExpression direction = PExpression.constant(this.symbolTable.getReceivingConstant());
      PExpression source = PExpression.variable(this.symbolTable.getSenderVariable());
      PExpression target = this.symbolTable.getThisExpression();
      PExpression event = PExpression.variable(this.symbolTable.getStimulusVariable());
      List<PExpression> arguments = new ArrayList<>();
      for (int i = 0; i < this.symbolTable.getMaxParameters(); i++)
        arguments.add(PExpression.bracket(PExpression.variable(this.symbolTable.getParametersVariable()),
                                          PExpression.constant(PConstant.numeric(i))));
      return UMLTranslator.getNotification(direction, source, target, event, arguments, this.symbolTable);
    }

    private PExpression pollInternalQueue() {
      PExpression currentEvent = PExpression.variable(this.symbolTable.getStimulusVariable());
      PExpression internalQueue = PExpression.channel(this.symbolTable.getInternalQueueChannel());

      PExpression poll = PExpression.pollNoSideEffect(internalQueue);
      poll.addPollNoSideEffectArg(currentEvent);
      return poll;
    }

    private PStatement receiveInternalQueue() {
      PExpression currentEvent = PExpression.variable(this.symbolTable.getStimulusVariable());
      PExpression internalQueue = PExpression.channel(this.symbolTable.getInternalQueueChannel());

      PStatement receive = PStatement.receive(internalQueue);
      receive.addRecvStmArg(currentEvent);
      return receive;
    }

    private PExpression pollNonInternalQueue(PExpression eventQueue) {
      return pollNonInternalQueue(eventQueue, false);
    }

    private PExpression randomPollNonInternalQueue(PExpression eventQueue) {
      return pollNonInternalQueue(eventQueue, true);
    }

    private PExpression pollNonInternalQueue(PExpression eventQueue, boolean random) {
      PExpression currentEvent = PExpression.variable(this.symbolTable.getStimulusVariable());

      PExpression poll = (random ? PExpression.randomPollNoSideEffect(eventQueue) : PExpression.pollNoSideEffect(eventQueue));
      poll.addPollNoSideEffectArg(currentEvent);
      PExpression senderVariable = PExpression.variable(this.symbolTable.getSenderVariable());
      poll.addPollNoSideEffectArg(senderVariable);
      for (int i = 0; i < this.symbolTable.getMaxParameters(); i++)
        poll.addPollNoSideEffectArg(PExpression.bracket(PExpression.variable(this.symbolTable.getParametersVariable()), PExpression.constant(PConstant.numeric(i))));
      if (this.symbolTable.hasOperations()) {
        PExpression ackOut = PExpression.channel(this.symbolTable.getAcknowledgeOutChannel());
        poll.addPollNoSideEffectArg(ackOut);
      }
      return poll;
    }

    private PStatement receiveNonInternalQueue(PExpression eventQueue) {
      return receiveNonInternalQueue(eventQueue, false);
    }

    private PStatement randomReceiveNonInternalQueue(PExpression eventQueue) {
      return receiveNonInternalQueue(eventQueue, true);
    }

    private PStatement receiveNonInternalQueue(PExpression eventQueue, boolean random) {
      PExpression currentEvent = PExpression.variable(this.symbolTable.getStimulusVariable());

      PStatement receive = (random ? PStatement.randomReceive(eventQueue) : PStatement.receive(eventQueue));
      receive.addRecvStmArg(currentEvent);
      PExpression senderVariable = PExpression.variable(this.symbolTable.getSenderVariable());
      receive.addRecvStmArg(senderVariable);
      for (int i = 0; i < this.symbolTable.getMaxParameters(); i++)
        receive.addRecvStmArg(PExpression.bracket(PExpression.variable(this.symbolTable.getParametersVariable()), PExpression.constant(PConstant.numeric(i))));
      if (this.symbolTable.hasOperations()) {
        PExpression ackOut = PExpression.channel(this.symbolTable.getAcknowledgeOutChannel());
        receive.addRecvStmArg(ackOut);
      }
      return receive;
    }
  }

  /**
   * Translate Smile statement into Promela
   * 
   * @param sStatement Smile statement
   * @param symbolTable symbol table for translation of Smile
   * @return translated Promela statement
   */
  private static PStatement translateStatement(SStatement sStatement, StateMachineSymbolTable symbolTable) {
    return (new StatementTranslator(symbolTable).statement(sStatement));
  }
}
