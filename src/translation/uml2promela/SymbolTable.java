package translation.uml2promela;

import org.eclipse.jdt.annotation.NonNullByDefault;

import promela.PConstant;
import promela.PExpression;
import uml.UModel;
import uml.UStatual;


@NonNullByDefault
public abstract class SymbolTable {
  private UModel model;

  public SymbolTable(UModel model) {
    this.model = model;
  }

  /**
   * @return symbol table's underlying model
   */
  public UModel getModel() {
    return this.model;
  }

  /**
   * @return symbol table's underlying model properties
   */
  public abstract control.Properties getProperties();

  /**
   * @return whether symbol table's underlying model contains an observer
   */
  public boolean isObserved() {
    return false;
  }

  /**
   * Determine which constant is used for <CODE>null</CODE>.
   *
   * @return Promela empty constant
   */
  abstract PConstant getEmptyConstant();

  /**
   * Determine a Promela expression for <CODE>this</CODE>.
   *
   * @return Promela expression for <CODE>this</CODE>
   */
  PExpression getThisExpression() {
    return PExpression.constant(this.getEmptyConstant());
  }

  /**
   * Determine a Promela expression for accessing a UML statual.
   *
   * @param uStatual a UML statual
   * @return Promela expression for accessing UML statual
   */
  abstract PExpression access(UStatual uStatual);

  /**
   * Determine a Promela expression for accessing a UML statual with an offset.
   *
   * @param uStatual a UML statual
   * @param pOffset a UPPAAL integer expression
   * @return Promela expression for accessing UML statual with offset
   */
  abstract PExpression access(UStatual uStatual, PExpression pOffset);
}
