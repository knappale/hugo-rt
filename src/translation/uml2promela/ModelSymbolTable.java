package translation.uml2promela;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import promela.PChannel;
import promela.PConstant;
import promela.PDataType;
import promela.PExpression;
import promela.PInline;
import promela.PKeywords;
import promela.PMacro;
import promela.PMessageType;
import promela.PProcess;
import promela.PStatement;
import promela.PVariable;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UClassifier;
import uml.UCollaboration;
import uml.UDataType;
import uml.UObject;
import uml.UOperation;
import uml.USignal;
import uml.UStatual;
import uml.UType;
import uml.interaction.UInteraction;
import uml.statemachine.UEvent;
import uml.statemachine.UStateMachine;
import util.Lists;

import static util.Objects.requireNonNull;


/**
 * Symbol table for translation of UML
 *
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public final class ModelSymbolTable extends SymbolTable {
  private static Map<Object, ModelSymbolTable> instances = new HashMap<>();

  // Static symbols and values
  private static final String QUEUE_SIZE_NAME = "queue_size";
  private static final String COMPLETION_QUEUE_SIZE_NAME = "completion_queue_size";
  private static final String DEFERRED_QUEUE_SIZE_NAME = "deferred_queue_size";
  private static final String EMPTY_CONSTANT_NAME = "empty";
  private static final int EMPTY_CONSTANT_VALUE = 0;
  private static final String NUMBER_OBJECTS_NAME = "number_objects";
  private static final String OBJ_LABEL = "obj_";
  private static final String MAX_PARAMETERS_NAME = "max_parameters";
  private static final String GLOBAL_EVENT_QUEUES_CHANNEL_NAME = "event_queues";
  private static final String OBSERVER_CHANNEL_NAME = "observer";
  private static final String OBSERVER_PROCESS_NAME = "Observer";
  private static final String TESTER_PROCESS_NAME = "Tester";
  private static final int ACTION_SEND_DIRECTION = 0;
  private static final String ACTION_SEND_NAME = "SEND";
  private static final int ACTION_RECEIVE_DIRECTION = 1;
  private static final String ACTION_RECEIVE_NAME = "RECEIVE";
  private static final int ACTION_TERMINATION = 2;
  private static final String ACTION_TERMINATION_NAME = "TERMINATION";
  private static final String INITIALISER_LABEL = "initialiser_";
  private static final int ACK_OUT_SEND_ARG_VALUE = 1;
  private static final String SUCCESS_NAME = "success";
  private static final int SUCCESS_VALUE = 0;
  private static final String PROCESSIDS = "processIds";
  private static final int PVALUE = 0;
  private static final int VVALUE = 1;
  private static final String SEMAPHORE = "semaphore";
  private static final String DEADLOCK_PROCESS_NAME = "Deadlock";
  private static final String DEADLOCK_VARIABLE = "deadlock";

  // UML model
  private UCollaboration uCollaboration;
  private @Nullable UInteraction uInteraction;
  private uml.testcase.@Nullable UTestCase uTestCase;

  // Contexts for UML classes
  private Map<UClass, StateMachineSymbolTable> stateMachineSymbolTables = new HashMap<>();

  // Generic translation results
  private List<PMacro> macros = new ArrayList<>();
  private List<PInline> inlines = new ArrayList<>();
  private List<PProcess> processes = new ArrayList<>();
  private PStatement initialisation = PStatement.skip();
  private Map<Object, PConstant> constants = new HashMap<>();
  private Map<PDataType, PDataType> dataTypes = new HashMap<>();
  private Map<PChannel, PChannel> channels = new HashMap<>();
  private Map<Object, PVariable> variables = new HashMap<>();
  private Map<UAttribute, PExpression> expressions = new HashMap<>();
  private Map<UClass, String> processNames = new HashMap<>();

  // Static translation results
  private @Nullable PConstant queueSizeConstant;
  private @Nullable PConstant completionQueueSizeConstant;
  private @Nullable PConstant deferredQueueSizeConstant;
  private @Nullable PConstant emptyConstant;
  private @Nullable PConstant numberObjectsConstant;
  private @Nullable PConstant maxParametersConstant;
  private @Nullable PChannel globalEventQueuesChannel;
  private @Nullable PChannel observerChannel;
  private @Nullable String observerProcessName;
  private @Nullable String testerProcessName;
  private @Nullable PConstant terminationConstant;
  private @Nullable PConstant sendingConstant;
  private @Nullable PConstant receivingConstant;
  private @Nullable PMacro successMacro;
  private @Nullable PVariable processIds;
  private @Nullable PChannel mutexChannel;
  private @Nullable PVariable deadlockVariable;

  private Map<UStateMachine, Integer> internalEventsConstantsCounters = new HashMap<>();
  private int otherEventsConstantsCounter = 0;

  private int objectConstantsCounter = 0;
  private Set<String> identifiers = new HashSet<>();

  /**
   * Create a system symbol table containing common information for
   * translating UML into Promela.
   *
   * @param collaboration a UML collaboration
   * @param interaction a UML interaction
   * @return a model symbol table for underlying collaboration
   * @pre collaboration != null
   */
  private ModelSymbolTable(UCollaboration uCollaboration, @Nullable UInteraction uInteraction, uml.testcase.@Nullable UTestCase uTestCase) {
    super(uCollaboration.getModel());
    this.uCollaboration = uCollaboration;
    this.uInteraction = uInteraction;
    this.uTestCase = uTestCase;
    this.identifiers.addAll(PKeywords.getKeywords());
  }
  
  /**
   * Create a system symbol table containing common information for
   * translating UML into Promela
   *
   * @param collaboration a UML collaboration
   * @param interaction a UML interaction
   * @return a model symbol table for underlying collaboration
   * @pre collaboration != null
   */
  public static ModelSymbolTable create(UCollaboration collaboration, @Nullable UInteraction interaction, uml.testcase.@Nullable UTestCase testCase) {
    List<Object> key = new ArrayList<>();
    key.add(collaboration);
    if (interaction != null)
      key.add(interaction);
    if (testCase != null)
      key.add(testCase);
    ModelSymbolTable instance = instances.get(key);
    if (instance == null) {
      instance = new ModelSymbolTable(collaboration, interaction, testCase);
      instances.put(key, instance);
    }
    return instance;
  }
  
  /**
   * @return whether model symbol table's underlying model has any operations
   */
  public boolean hasOperations() {
    return this.getModel().getClasses().stream().anyMatch(uClass -> !uClass.getOperations().isEmpty());
  }

  /**
   * @return model symbol table's underlying collaboration
   */
  public UCollaboration getCollaboration() {
    return this.uCollaboration;
  }

  /**
   * @return whether model symbol table's underlying collaboration has an observer
   * @see translation.uml2promela.SymbolTable#isObserved()
   */
  public boolean isObserved() {
    return this.uInteraction != null ||
           this.uTestCase != null;
  }

  @Override
  public control.@NonNull Properties getProperties() {
    return this.uCollaboration.getProperties();
  }

  /**
   * Determine the number of objects in this collaboration (including
   * <CODE>null</CODE>).
   *
   * @return number of objects in this collaboration
   */
  int getObjectsNumber() {
    return this.uCollaboration.getObjects().size()+1;
  }

  /**
   * Retrieve Promela constant for UML object
   *
   * @param uObject UML object
   * @return Promela constant for UML object
   */
  public PConstant getConstant(UObject uObject) {
    PConstant pConstant = this.constants.get(uObject);
    if (pConstant != null)
      return pConstant;

    String pObjectName = this.uniqueIdentifier(OBJ_LABEL + uObject.getName());
    PConstant pObjectConstant = PConstant.numeric(++this.objectConstantsCounter);
    PMacro pMacro = PMacro.constant(pObjectName, pObjectConstant);
    this.addMacro(pMacro);
    pConstant = PConstant.macro(pMacro);
    this.constants.put(uObject, pConstant);
    return pConstant;
  }

  /**
   * Determine which UML object is represented by an integer.
   *
   * @param n an integer
   * @return UML object represented by integer {@code n}; or {@code null} if no such object can be found
   */
  public @Nullable UObject getObject(int n) {
    for (Object element : this.constants.keySet()) {
      if (element instanceof UObject uObject) {
        var pConstant = this.constants.get(uObject);
        if (pConstant != null && n == pConstant.getValue())
          return uObject;
      }
    }
    return null;
  }

  /**
   * Retrieve Promela constant for UML behavioural (Promela constant for UML event)
   *
   * @param uBehavioural UML behavioural
   * @return Promela constant for UML behavioural (Promela constant for UML event)
   */
  public PConstant getConstant(UBehavioural uBehavioural) {
    return getConstant(uBehavioural.getOwner().getStateMachine(), UEvent.behavioural(uBehavioural));
  }

  static final class EventKey {
    @Nullable UStateMachine uStateMachine;
    UEvent uEvent;

    EventKey(@Nullable UStateMachine stateMachine, UEvent event) {
      this.uStateMachine = stateMachine;
      this.uEvent = event;
    }

    public UEvent getEvent() {
      return this.uEvent;
    }

    public boolean matches(@Nullable UStateMachine otherUStateMachine) {
      return (this.uStateMachine != null && this.uStateMachine.equals(otherUStateMachine));
    }

    public int hashCode() {
      return Objects.hash(this.uStateMachine, this.uEvent);
    }

    public boolean equals(java.lang.@Nullable Object object) {
      if (object == null)
        return false;
      if (object instanceof EventKey other)
        return (Objects.equals(this.uStateMachine, other.uStateMachine) &&
                Objects.equals(this.uEvent, other.uEvent));
      return false;
    }
  }

  /**
   * Retrieve Promela constant for UML event
   *
   * @param uStateMachine a UML state machine, or {@code null} for global events
   * @param uEvent a UML event
   * @return Promela constant for UML event
   */
  PConstant getConstant(@Nullable UStateMachine uStateMachine, UEvent uEvent) {
    EventKey key = new EventKey(uStateMachine, uEvent);
    PConstant pConstant = this.constants.get(key);
    if (pConstant != null)
      return pConstant;

    PMacro macro = PMacro.constant(this.uniqueIdentifier(uEvent.getName()), PConstant.numeric(this.getConstantNumber(uStateMachine, uEvent)));
    this.addMacro(macro);
    pConstant = PConstant.macro(macro);
    this.constants.put(key, pConstant);
    return pConstant;
  }

  /**
   * Retrieve a number for an event of a state machine.
   *
   * @param uStateMachine a UML state machine, or {@code null} for global events
   * @param uEvent a UML event
   * @return
   */
  private int getConstantNumber(@Nullable UStateMachine uStateMachine, UEvent uEvent) {
    if (uStateMachine == null)
      return ++otherEventsConstantsCounter;

    UBehavioural uBehavioural = uStateMachine.getC1ass().getBehavioural(uEvent);
    if (uBehavioural != null) {
      return uBehavioural.cases(uOperation -> Lists.sort(uStateMachine.getC1ass().getOperations(), UOperation::getName).indexOf(uOperation)+this.getModel().getSignals().size()+1,
                                uReception -> Lists.sort(this.getModel().getSignals(), USignal::getName).indexOf(uReception.getSignal())+1);
    }

    Integer internalEventsConstantsCounter = internalEventsConstantsCounters.get(uStateMachine);
    if (internalEventsConstantsCounter == null) {
      int constantNumber = this.getModel().getSignals().size() + uStateMachine.getC1ass().getOperations().size() + 1;
      internalEventsConstantsCounters.put(uStateMachine, constantNumber+1);
      return constantNumber;
    }

    int constantNumber = internalEventsConstantsCounter.intValue();
    internalEventsConstantsCounters.put(uStateMachine, constantNumber+1);
    return constantNumber;
  }

  /**
   * Determine the maximum number used for any event accepted by a state machine.
   *
   * @param uStateMachine a UML state machine
   * @return maximum number used for any event accepted by {@code uStateMachine}
   */
  int getEventsMax(UStateMachine uStateMachine) {
    return this.getModel().getSignals().size() + uStateMachine.getC1ass().getOperations().size() + uStateMachine.getCompletionStates().size() + uStateMachine.getTimeds().size() + 1;
  }

  /**
   * Determine which UML event is represented by an integer for a given UML state machine.
   *
   * @param uStateMachine a UML state machine or {@code null} for global events
   * @param n an integer
   * @return UML event represented by integer {@code n}; or {@code null} if no such event can be found
   */
  public @Nullable UEvent getEvent(@Nullable UStateMachine uStateMachine, int n) {
    for (Object element : this.constants.keySet()) {
      if (element instanceof EventKey) {
        EventKey key = (EventKey)element;
        if (key.matches(uStateMachine)) {
          PConstant pConstant = this.constants.get(key);
          if (pConstant != null && n == pConstant.getValue()) {
            return key.getEvent();
          }
        }
      }
    }
    return null;
  }

  /**
   * Retrieve Promela variable for non-final static UML attribute
   *
   * @param uAttribute UML attribute
   * @return Promela variable for non-final static UML attribute
   */
  PVariable getVariable(UAttribute uAttribute) {
    PVariable pVariable = this.variables.get(uAttribute);
    if (pVariable != null)
      return pVariable;

    // Determine Promela data type
    PDataType pDataType = null;
    UClassifier uClassifier = uAttribute.getClassifier();
    if (uClassifier instanceof UDataType) {
      if (uClassifier.equals(uClassifier.getModel().getBoolean())) {
        pDataType = PDataType.boolType();
      }
      else
        if (uClassifier.equals(uClassifier.getModel().getInteger())) {
          pDataType = PDataType.intType();
        }
    }
    else
      if (uClassifier instanceof UClass) {
        pDataType = PDataType.minimal(this.uCollaboration.getObjects().size()+1);
      }
    if (pDataType == null)
      throw new RuntimeException("Cannot translate UML attribute " + uAttribute.toString() + " into Promela");

    // Get variable name
    String variableName = this.uniqueIdentifier(uAttribute.getOwner().getName() + "_" + uAttribute.getName());

    // Create variable
    UType uType = uAttribute.getType();
    if (uType.isSimple()) {
      pVariable = PVariable.simple(variableName, pDataType);
    }
    else
      if (uType.isArray()) {
        PConstant arraySize = PConstant.numeric(uType.getSize());
        pVariable = PVariable.array(variableName, pDataType, arraySize);
      }
    if (pVariable == null)
      throw new RuntimeException("Cannot translate UML attribute " + uAttribute.toString() + " into Promela");

    // Initialise variable
    if (uAttribute.hasInitialValues()) {
      PExpression initialExpression = UMLTranslator.translateValue(requireNonNull(uAttribute.getInitialValue(0)), uAttribute.getInitialValuesContext(), this);
      pVariable.initialise(initialExpression);
    }

    this.variables.put(uAttribute, pVariable);

    return pVariable;
  }
    
  /**
   * Retrieve Promela expression for UML expression of static final UML attribute
   *
   * @param uAttribute UML constant static attribute
   * @return Promela expression for UML expression of static final UML attribute
   */
  PExpression getExpression(UAttribute uAttribute) {
    PExpression pExpression = this.expressions.get(uAttribute);
    if (pExpression != null)
      return pExpression;

    String expressionName = this.uniqueIdentifier(uAttribute.getOwner().getName() + "_" + uAttribute.getName());
    PExpression exp = UMLTranslator.translateValue(requireNonNull(uAttribute.getInitialValue(0)), uAttribute.getInitialValuesContext(), this);
    PMacro macro = PMacro.expression(expressionName, exp);
    this.addMacro(macro);
    pExpression = PExpression.macro(macro);
    this.expressions.put(uAttribute, pExpression);
    return pExpression;
  }

  /**
   * Retrieve name of Promela process for context's UML class
   *
   * @param uClass UML class
   * @return name of Promela process for context's UML class
   */
  String getProcessName(UClass uClass) {
    var processName = this.processNames.get(uClass);
    if (processName != null)
      return processName;

    processName = this.uniqueIdentifier(uClass.getName());
    this.processNames.put(uClass, processName);
    return processName;
  }

  /**
   * Retrieve context for translation of Smile
   *
   * @param uClass UML class
   * @return context for translation of Smile
   */
  StateMachineSymbolTable getStateMachineSymbolTable(UClass uClass) {
    var stateMachineSymbolTable = stateMachineSymbolTables.get(uClass);
    if (stateMachineSymbolTable != null)
      return stateMachineSymbolTable;

    stateMachineSymbolTable = new StateMachineSymbolTable(uClass, this);
    this.stateMachineSymbolTables.put(uClass, stateMachineSymbolTable);
    return stateMachineSymbolTable;
  }

  /**
   * @return Promela constant for queue size
   */
  PConstant getQueueSizeConstant() {
    var queueSizeConstant = this.queueSizeConstant;
    if (queueSizeConstant != null)
      return queueSizeConstant;

    var queueSizeMacro = PMacro.constant(QUEUE_SIZE_NAME,
                                         PConstant.numeric(this.getProperties().getExternalQueueCapacity()));
    this.addMacro(queueSizeMacro);
    return this.queueSizeConstant = PConstant.macro(queueSizeMacro);
  }

  /**
   * @return Promela constant for queue size
   */
  PConstant getInternalQueueSizeConstant() {
    var completionQueueSizeConstant = this.completionQueueSizeConstant;
    if (completionQueueSizeConstant != null)
      return completionQueueSizeConstant;
  
    var completionQueueSizeMacro = PMacro.constant(COMPLETION_QUEUE_SIZE_NAME,
                                                   PConstant.numeric(this.getProperties().getInternalQueueCapacity()));
    this.addMacro(completionQueueSizeMacro);
    return this.completionQueueSizeConstant = PConstant.macro(completionQueueSizeMacro);
  }

  /**
   * @return Promela constant for queue size
   */
  PConstant getDeferredQueueSizeConstant() {
    var deferredQueueSizeConstant = this.deferredQueueSizeConstant;
    if (deferredQueueSizeConstant != null)
      return deferredQueueSizeConstant;

    var deferredQueueSizeMacro = PMacro.constant(DEFERRED_QUEUE_SIZE_NAME,
                                                 PConstant.numeric(this.getProperties().getDeferredQueueCapacity()));
    this.addMacro(deferredQueueSizeMacro);
    return this.deferredQueueSizeConstant = PConstant.macro(deferredQueueSizeMacro);
  }

  /**
   * Determine a Promela expression for accessing a UML statual.
   *
   * @param uStatual a UML statual
   * @return Promela expression for accessing UML statual
   */
  PExpression access(UStatual uStatual) {
    return uStatual.cases(uAttribute -> uAttribute.isConstant() ? this.getExpression(uAttribute) : PExpression.variable(this.getVariable(uAttribute)),
                          uParameter -> PExpression.constant(this.getEmptyConstant()),
                          uObject -> PExpression.constant(this.getConstant(uObject)));
  }

  /**
   * Determine a Promela expression for accessing a UML statual with an offset.
   *
   * @param uStatual a UML statual
   * @param pOffset a UPPAAL integer expression
   * @return Promela expression for accessing UML statual with offset
   */
  PExpression access(UStatual uStatual, PExpression pOffset) {
    return uStatual.cases(uAttribute -> PExpression.bracket(PExpression.variable(this.getVariable(uAttribute)), pOffset),
                          uParameter -> PExpression.constant(this.getEmptyConstant()),
                          uObject -> PExpression.constant(this.getEmptyConstant()));
  }

  /**
   * Determine Promela expression for accessing the external event queue of a target expression denoting a UML object.
   * 
   * @param target a Promela expression denoting a UML object
   * @return Promela expression for accessing the external event queue of <CODE>target</CODE>
   */
  PExpression accessExternalEventQueue(PExpression target) {
    // return PExpression.bracket(PExpression.channel(this.getGlobalEventQueuesChannel()), PExpression.binary(target, POperator.MINUS, PExpression.constant(PConstant.numeric(1))));
    return PExpression.bracket(PExpression.channel(this.getGlobalEventQueuesChannel()), target);
  }

  /**
   * @return Promela constant which is is used for {@code null} or, more generally, an "empty" Promela variable
   */
  PConstant getEmptyConstant() {
    if (this.emptyConstant != null)
      return this.emptyConstant;

    PMacro emptyMacro = PMacro.constant(EMPTY_CONSTANT_NAME, PConstant.numeric(EMPTY_CONSTANT_VALUE));
    this.addMacro(emptyMacro);
    return this.emptyConstant = PConstant.macro(emptyMacro);
  }

  /**
   * @return Promela constant for number of objects owned by UML model
   */
  PConstant getNumberObjectsConstant() {
    if (this.numberObjectsConstant != null)
      return this.numberObjectsConstant;

    PMacro numberObjectsMacro = PMacro.constant(NUMBER_OBJECTS_NAME, PConstant.numeric(this.uCollaboration.getObjects().size()+1));
    this.addMacro(numberObjectsMacro);
    return this.numberObjectsConstant = PConstant.macro(numberObjectsMacro);
  }

  /**
   * @return Promela constant for maximal number of parameters appearing in UML model
   */
  PConstant getMaxParametersConstant() {
    if (this.maxParametersConstant != null)
      return this.maxParametersConstant;

    int maxParameters = this.getMaxParameters();
    // TODO (AK160206) work-around for models without parameters
    if (maxParameters <= 0)
      maxParameters = 1;
    PMacro macro = PMacro.constant(MAX_PARAMETERS_NAME, PConstant.numeric(maxParameters));
    this.addMacro(macro);
    this.maxParametersConstant = PConstant.macro(macro);
    return this.maxParametersConstant;
  }

  /**
   * @return Promela constant sent by acknowledge channel
   */
  PConstant getAckOutSendArgConstant() {
    return PConstant.numeric(ACK_OUT_SEND_ARG_VALUE);
  }

  /**
   * Determine the maximum number assigned to any of the external events
   * accepted by any of the underlying classes.
   * 
   * An event is called external if it is handled by the global event queues.
   * 
   * @return maximum number assigned to any of the external events
   *         accepted by any of the underlying classes
   */
  public int getExternalEventsNumber() {
    return this.getModel().getSignals().size() +
           this.getModel().getClasses().stream().map(uClass -> uClass.getOperations().size()).reduce(0, (m1, m2) -> Integer.max(m1, m2));
  }

  /**
   * @return Promela channel for global event queues
   */
  PMessageType getExternalEventsMessageType() {
    var messageType = PMessageType.create();
    messageType.addPartDataType(PDataType.minimal(this.getExternalEventsNumber()+1)); // event
    messageType.addPartDataType(PDataType.minimal(this.getObjectsNumber())); // sender
    for (int i = 0; i < this.getMaxParameters(); i++)
      messageType.addPartDataType(PDataType.intType()); // parameters
    if (this.hasOperations())
      messageType.addPartChannel(); // Acknowledge channel
    return messageType;
  }

  /**
   * @return Promela channel for global event queues
   */
  PChannel getGlobalEventQueuesChannel() {
    if (this.globalEventQueuesChannel != null)
      return this.globalEventQueuesChannel;

    var globalEventQueuesChannel = PChannel.array(GLOBAL_EVENT_QUEUES_CHANNEL_NAME, this.getNumberObjectsConstant())
                                           .initialise(this.getQueueSizeConstant(), this.getExternalEventsMessageType());
    this.channels.put(globalEventQueuesChannel, globalEventQueuesChannel);
    return this.globalEventQueuesChannel = globalEventQueuesChannel;
  }

  /**
   * @return Promela channel for observer
   */
  PChannel getObserverChannel() {
    if (this.observerChannel != null)
      return this.observerChannel;

    var messageType = PMessageType.create();
    messageType.addPartDataType(PDataType.byteType()); // direction
    messageType.addPartDataType(PDataType.intType()); // event
    messageType.addPartDataType(PDataType.minimal(this.getObjectsNumber()+1)); // sender
    for (int i = 0; i < this.getMaxParameters(); i++)
      messageType.addPartDataType(PDataType.intType()); // parameters
    messageType.addPartDataType(PDataType.minimal(this.getObjectsNumber()+1)); // receiver

    var observerChannel = PChannel.simple(OBSERVER_CHANNEL_NAME)
                                  .initialise(PConstant.numeric(0), messageType);
    this.channels.put(observerChannel, observerChannel);
    return this.observerChannel = observerChannel;
  }

  /**
   * Retrieve name of Promela process for the tester.
   *
   * @return name of Promela process for the tester
   */
  String getTesterProcessName() {
    if (this.testerProcessName != null)
      return this.testerProcessName;

    return this.testerProcessName = this.uniqueIdentifier(TESTER_PROCESS_NAME);
  }

  /**
   * Retrieve name of Promela process for the observer.
   *
   * @return name of Promela process for the observer
   */
  String getObserverProcessName() {
    if (this.observerProcessName != null)
      return this.observerProcessName;

    return this.observerProcessName = this.uniqueIdentifier(OBSERVER_PROCESS_NAME);
  }

  /**
   * @return Promela constant which is is used for indicating a termination action
   */
  PConstant getTerminationConstant() {
    if (this.terminationConstant != null)
      return this.terminationConstant;

    var terminationMacro = PMacro.constant(ACTION_TERMINATION_NAME, PConstant.numeric(ACTION_TERMINATION));
    this.addMacro(terminationMacro);
    return this.terminationConstant = PConstant.macro(terminationMacro);
  }

  /**
   * @return Promela constant which is is used for indicating a sending action
   */
  PConstant getSendingConstant() {
    if (this.sendingConstant != null)
      return this.sendingConstant;

    var sendingMacro = PMacro.constant(ACTION_SEND_NAME, PConstant.numeric(ACTION_SEND_DIRECTION));
    this.addMacro(sendingMacro);
    return this.sendingConstant = PConstant.macro(sendingMacro);
  }

  /**
   * @return Promela constant which is is used for indicating a receiving action
   */
  PConstant getReceivingConstant() {
    if (this.receivingConstant != null)
      return this.receivingConstant;

    PMacro receivingMacro = PMacro.constant(ACTION_RECEIVE_NAME, PConstant.numeric(ACTION_RECEIVE_DIRECTION));
    this.addMacro(receivingMacro);
    return this.receivingConstant = PConstant.macro(receivingMacro);
  }

  /**
   * Register a Promela variable.
   *
   * @param pVariable a Promela variable
   */
  public void addVariable(PVariable pVariable) {
    this.variables.put(pVariable, pVariable);
  }

  /**
   * @return Promela statement for successful process termination
   */
  PMacro getSuccessMacro() {
    if (this.successMacro != null)
      return this.successMacro;

    var successMacro = PMacro.expression(SUCCESS_NAME, PExpression.constant(PConstant.numeric(SUCCESS_VALUE)));
    this.addMacro(successMacro);
    return this.successMacro = successMacro;
  }

  /**
   * @return maximal number of UML parameters appearing in UML model
   */
  int getMaxParameters() {
    return this.getModel().getMaxParameters();
  }

  /**
   * @return process identifiers array
   */
  PVariable getProcessIdentifiers() {
    if (this.processIds != null)
      return this.processIds;

    var processIds = PVariable.array(PROCESSIDS, PDataType.pidType(), getNumberObjectsConstant());
    this.variables.put(processIds, processIds);
    return this.processIds = processIds;
  }

  /**
   * Retrieve Promela expression for accessing the process identifier of a UML object.
   * 
   * @param uObject a UML object
   * @return Promela expression for accessing the process identifier of <CODE>uObject</CODE>
   */
  PExpression getProcessIdentifier(UObject uObject) {
    // PExpression pObject = PExpression.constant(this.getConstant(uObject));
    // PExpression pConst1 = PExpression.constant(PConstant.numeric(1));
    // PExpression pOffset = PExpression.binary(pObject, POperator.MINUS, pConst1);
    return PExpression.bracket(PExpression.variable(this.getProcessIdentifiers()), PExpression.constant(this.getConstant(uObject)));
  }

  /**
   * Retrieve Promela expression for remotely accessing a statual local to a state machine.
   * 
   * @param uObject a UML object
   * @param uStatual a UML statual of {@code uObject}
   * @return Promela expression for remotely accessing {@code uStatual} in {@code uObject}
   */
  PExpression accessRemote(UObject uObject, UStatual uStatual) {
    if (uStatual.isStatic())
      return this.access(uStatual);

    StateMachineSymbolTable stateMachineSymbolTable = getStateMachineSymbolTable(uObject.getC1ass());
    String processName = stateMachineSymbolTable.getProcessName();
    PExpression pid = getProcessIdentifier(uObject);
    PExpression localAccess = stateMachineSymbolTable.access(uStatual);
    return PExpression.remoteRef(processName, pid, localAccess);
  }
  
  /**
   * Retrieve Promela expression for remotely accessing an array statual
   * local to a state machine.
   * 
   * @param uObject a UML object
   * @param uStatual a UML statual (of {@code uObject})
   * @param pOffset a Promela expression for the offset (of {@code uStatual})
   * @return Promela expression for remotely accessing {@code uStatual} in {@code uObject}
   */
  PExpression accessRemote(UObject uObject, UStatual uStatual, PExpression pOffset) {
    if (uStatual.isStatic())
      return this.access(uStatual, pOffset);

    StateMachineSymbolTable stateMachineSymbolTable = getStateMachineSymbolTable(uObject.getC1ass());
    String processName = stateMachineSymbolTable.getProcessName();
    PExpression pid = getProcessIdentifier(uObject);
    PExpression localAccess = stateMachineSymbolTable.access(uStatual, pOffset);
    return PExpression.remoteRef(processName, pid, localAccess);
  }
  
  /**
   * @return label for initialisation variable of non-static UML attribute
   */
  String getInitialiserLabel() {
    return INITIALISER_LABEL;
  }

  /**
   * @return mutex channel
   */
  PChannel getMutexChannel() {
    if (this.mutexChannel != null)
      return this.mutexChannel;

    var mutexChannel = PChannel.simple(this.uniqueIdentifier(SEMAPHORE));
    PMessageType messageType = PMessageType.create();
    messageType.addPartDataType(PDataType.bitType());
    mutexChannel.initialise(PConstant.numeric(0), messageType);
    this.channels.put(mutexChannel, mutexChannel);
    return this.mutexChannel = mutexChannel;
  }

  /**
   * The mutex loop repeatedly first offers synchronisation on the
   * mutex channel and then requests synchronisation on the mutex channel.
   *
   * @return Promela statement for the mutex loop
   */
  PStatement getMutexLoop() {
    PStatement put = PStatement.send(PExpression.channel(getMutexChannel()));
    put.addSendStmArg(PExpression.constant(PConstant.numeric(PVALUE)));
    PStatement labelledPut = PStatement.label("end_mutex", put);
    PStatement poll = PStatement.receive(PExpression.channel(getMutexChannel())); 
    poll.addRecvStmArg(PExpression.constant(PConstant.numeric(VVALUE)));
    // PStatement labelledPoll = PStatement.label("mutex_poll", poll);
    PStatement gotoPut = PStatement.gotoStm("end_mutex");
    return PStatement.sequence(PStatement.sequence(labelledPut, poll), gotoPut);
  }

  /**
   * For acquiring the mutex, synchronisation on the mutex channel
   * has to be requested.
   *
   * @return Promela statement for acquiring the mutex
   */
  PStatement enterMutex() {
    var poll = PStatement.receive(PExpression.channel(getMutexChannel())); 
    poll.addRecvStmArg(PExpression.constant(PConstant.numeric(PVALUE)));
    return poll;
  }

  /**
   * For releasing the mutex, synchronisation on the mutex channel
   * has to be offered.
   *
   * @return Promela statement for releasing the mutex
   */
  PStatement exitMutex() {
    var put = PStatement.send(PExpression.channel(getMutexChannel()));
    put.addSendStmArg(PExpression.constant(PConstant.numeric(VVALUE)));
    return put;
  }

  /**
   * For yielding the mutex, the mutex is released and then required again.
   *
   * @return Promela statement for yielding the mutex
   */
  PStatement yieldMutex() {
    return PStatement.sequence(this.exitMutex(), this.enterMutex());
  }

  /**
   * @return deadlock variable
   */
  PVariable getDeadlockVariable() {
    if (this.deadlockVariable != null)
      return this.deadlockVariable;

    var deadlockVariable = PVariable.simple(DEADLOCK_VARIABLE, PDataType.boolType());
    this.variables.put(deadlockVariable, deadlockVariable);

    this.processes.add(this.getDeadlockCheck());
    var runDeadlockCheck = PStatement.expression(PExpression.run(DEADLOCK_PROCESS_NAME));
    this.initialisation = (this.initialisation == null ? runDeadlockCheck : PStatement.sequence(this.initialisation, runDeadlockCheck));
    return this.deadlockVariable = deadlockVariable;
  }

  private PProcess getDeadlockCheck() {
    PStatement deadlockCheck = PStatement.selection();

    PStatement isTimedOut = PStatement.expression(PExpression.variable(PVariable.timeout()));

    @NonNull PExpression someQueueNotFullCheck = PExpression.falseConst();
    // For every non-null object
    for (int i = 1; i < getObjectsNumber(); i++) {
      PExpression eventQueue = PExpression.bracket(PExpression.channel(this.getGlobalEventQueuesChannel()), PExpression.numeric(i-1));
      someQueueNotFullCheck = PExpression.or(someQueueNotFullCheck, PExpression.nfull(eventQueue));
    }
    PStatement deadlockAssign = PStatement.assignment(PExpression.variable(this.getDeadlockVariable()), PExpression.trueConst());

    PStatement isDeadlocked = PStatement.selection();
    isDeadlocked.addBranch(PStatement.sequence(PStatement.expression(someQueueNotFullCheck), deadlockAssign));

    PStatement timedOutBranch = PStatement.sequence(isTimedOut, isDeadlocked);
    deadlockCheck.addBranch(timedOutBranch);

    return PProcess.proctype(DEADLOCK_PROCESS_NAME, deadlockCheck);
  }

  /**
   * Create and add Promela inline for a statement.
   *
   * @param name inline's name
   * @param pParameters inline's parameters
   * @param pStatement inline's statement
   */
  public PInline addInline(String name, List<PVariable> pParameters, PStatement pStatement) {
    var pInline = new PInline(name, pParameters, pStatement);
    this.inlines.add(pInline);
    return pInline;
  }

  /**
   * @return collected macros
   */
  List<PInline> getInlines() {
    return this.inlines;
  }

  /**
   * Add Promela macro.
   *
   * @param macro Promela macro
   * 
   * TODO (AK050902) this should work as a factory, i.e., getMacro(Expression)
   */
  public void addMacro(PMacro macro) {
    if (!this.macros.contains(macro))
      this.macros.add(macro);
  }

  /**
   * @return collected macros
   */
  List<PMacro> getMacros() {
    return this.macros;
  }

  /**
   * @return names that are used globally
   */
  Collection<String> getGlobalNames() {
    var globalNames = new HashSet<String>();
    for (var macro : this.macros)
      globalNames.add(macro.getName());
    globalNames.addAll(this.identifiers);
    return globalNames;
  }

  /**
   * @return collected processes
   */
  List<PProcess> getProcesses() {
    return this.processes;
  }

  /**
   * @return collected initialisation
   */
  PStatement getInitialisation() {
    return this.initialisation;
  }

  /**
   * @return collected data types
   */
  Collection<PDataType> getDataTypes() {
    return this.dataTypes.values();
  }

  /**
   * @return collected channels
   */
  Collection<PChannel> getChannels() {
    return this.channels.values();
  }

  /**
   * @return collected variables
   */
  public Collection<PVariable> getVariables() {
    return this.variables.values();
  }

  /**
   * Generate unique identifier.
   * 
   * This has to take into account that in Promela macros break the block structure,
   * i.e. macro names are not shadowed in proctypes.
   * 
   * TODO (AK130428) Introduce a symbol table that is written out and read.
   */
  String uniqueIdentifier(String identifier) {
    var characters = identifier.toCharArray();
    for (var i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    var allIdentifiers = new HashSet<String>();
    allIdentifiers.addAll(this.identifiers);
    for (var stateMachineSymbolTable : this.stateMachineSymbolTables.values())
      allIdentifiers.addAll(stateMachineSymbolTable.getLocalNames());
    var uniqueIdentifier = new String(characters).substring(0, Math.min(50, characters.length));
    if (uniqueIdentifier.equals("") || allIdentifiers.contains(uniqueIdentifier)) {
      var tmpIdentifier = "";
      var counter = 0;
      do {
        tmpIdentifier = uniqueIdentifier + "_G" + counter++;
      } while (allIdentifiers.contains(tmpIdentifier));
      uniqueIdentifier = tmpIdentifier;
    }
    this.identifiers.add(uniqueIdentifier);
    return uniqueIdentifier;
  }
}
