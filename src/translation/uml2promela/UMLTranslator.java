package translation.uml2promela;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import promela.PChannel;
import promela.PConstant;
import promela.PDataType;
import promela.PExpression;
import promela.PFormula;
import promela.PInline;
import promela.PMacro;
import promela.POperator;
import promela.PProcess;
import promela.PSpecification;
import promela.PStatement;
import promela.PSystem;
import promela.PVariable;
import promela.run.PProcessState;
import promela.run.PSystemState;
import uml.UAction;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UCollaboration;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import uml.UOperation;
import uml.UOperator;
import uml.USlot;
import uml.UStatual;
import uml.UType;
import uml.interaction.UInteraction;
import uml.interaction.UMessage;
import uml.ocl.OLTLConstraint;
import uml.run.UObjectState;
import uml.run.URun;
import uml.smile.SMachine;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UTransition;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UConfiguration;
import uml.statemachine.semantics.UVertexForest;
import uml.statemachine.semantics.UVertexTree;
import uml.testcase.UTestCase;
import util.Formatter;
import util.Identifier;

import static java.util.stream.Collectors.toSet;
import static util.Objects.requireNonNull;


/**
 * Translator of UML model into Promela.
 */
@NonNullByDefault
public final class UMLTranslator {
  private UMLTranslator() {
  }
  
  /**
   * Translate UML model into Promela.
   *
   * @param uCollaboration a (UML) collaboration
   * @param uInteraction a (UML) interaction
   * @param uAssertions a list of (OCL) constraints
   * @return translated Promela system
   */
  public static PSystem translate(UCollaboration uCollaboration, @Nullable UInteraction uInteraction, @Nullable UTestCase uTestCase, @Nullable List<OLTLConstraint> uAssertions) {
    PSystem system = null;
    
    // Generic
    Map<UClass, PProcess> processes = new HashMap<>();
    PProcess observerProcess = null;
    PProcess testerProcess = null;
    PProcess initProcess = null;
    PSpecification specification = null;
    
    // Set up symbol table for translation of UML
    ModelSymbolTable modelSymbolTable = ModelSymbolTable.create(uCollaboration, uInteraction, uTestCase);
    
    // Create Promela proctype processes
    for (UClass uClass : uCollaboration.getModel().getClasses()) {
      UStateMachine uStateMachine = uClass.getStateMachine();
      if (uStateMachine == null)
        continue;
      
      // Translate UML state machine into Smile
      SMachine sMachine = uStateMachine.getMachine(uCollaboration.getProperties());
      
      // Translate Smile machine into Promela
      StateMachineSymbolTable stateMachineSymbolTable = modelSymbolTable.getStateMachineSymbolTable(uClass);
      PProcess process = SmileTranslator.translateMachine(sMachine, stateMachineSymbolTable);
      
      processes.put(uClass, process);
    }
    
    // Create observer
    {
      if (uInteraction != null) {
        uml.ida.IAutomaton iAutomaton = uInteraction.getAutomaton();
        observerProcess = IdaTranslator.translate(modelSymbolTable, iAutomaton);
      }
    }

    // Create tester
    {
      if (uTestCase != null) {
        testerProcess = TestTranslator.translate(modelSymbolTable, uTestCase);
      }
    }

    // Create constraints
    {
      if (uAssertions != null &&
          !uAssertions.isEmpty()) {
        specification = PSpecification.create();
        
        for (OLTLConstraint constraint : uAssertions) {
          PFormula formula = Constraint.create(constraint, modelSymbolTable);
          if (formula != null)
            specification.addFormula(formula);
        }
      }
    }
    
    // Create init process
    // NB: Init process creation must be the last action, as
    // it relies on all other parts of the Promela system having
    // been instantiated already.
    {
      PStatement statement = modelSymbolTable.getInitialisation();

      for (UObject uObject : uCollaboration.getObjects()) {
        UClass uClass = null;
        StateMachineSymbolTable context = null;
        Map<UAttribute, USlot> slotsMap = new HashMap<>();

        uClass = uObject.getC1ass();
        if (uClass.getStateMachine() == null)
          continue;

        context = modelSymbolTable.getStateMachineSymbolTable(uClass);
        for (UAttribute uAttribute : context.getOrderedAttributes()) {
          for (USlot uSlot : uObject.getSlots()) {
            if (uAttribute.equals(uSlot.getAttribute())) {
              slotsMap.put(uAttribute, uSlot); // Reverse mapping from UML attributes to UML slots
            }
          }
        }

        PExpression run = PExpression.run(modelSymbolTable.getProcessName(uClass)); // Creation of UML object and its UML state machine
        run.addRunArg(modelSymbolTable.accessExternalEventQueue(PExpression.constant(modelSymbolTable.getConstant(uObject)))); // UML object's global EQ
        run.addRunArg(PExpression.constant(modelSymbolTable.getConstant(uObject))); // UML object's ID ("this")
        for (UAttribute uAttribute : context.getOrderedAttributes()) {
          if (uAttribute.isFinal() || uAttribute.isStatic())
            continue;
          
          for (PExpression initialiser : getInitialisers(uObject, uAttribute, modelSymbolTable))
            run.addRunArg(initialiser);
        }

        statement = PStatement.sequence(statement, PStatement.assignment(modelSymbolTable.getProcessIdentifier(uObject), run));
      }

      if (observerProcess != null)
        statement = PStatement.sequence(statement, PStatement.expression(PExpression.run(modelSymbolTable.getObserverProcessName())));

      if (testerProcess != null)
        statement = PStatement.sequence(statement, PStatement.expression(PExpression.run(modelSymbolTable.getTesterProcessName())));

      statement = PStatement.atomic(statement);
      if (modelSymbolTable.getProperties().isMutex())
        statement = PStatement.sequence(statement, modelSymbolTable.getMutexLoop());
      initProcess = PProcess.init(statement);
    }

    // Create Promela system
    system = PSystem.create();

    // Add collected inline definitions
    for (PInline inline : modelSymbolTable.getInlines() ) {
      system.addInlineDef(inline);
    }

    // Add collected macro definitions
    for (PMacro macro : modelSymbolTable.getMacros() ) {
      system.addMacroDef(macro);
    }

    // Add collected data types
    for (PDataType data : modelSymbolTable.getDataTypes()) {
      system.addDataType(data);
    }

    // Add collected channels
    for (PChannel channel : modelSymbolTable.getChannels()) {
      system.addChanDecl(channel);
    }

    // Add collected variables
    for (PVariable variable : modelSymbolTable.getVariables()) {
      system.addVarDecl(variable);
    }

    // Add proctype processes
    for (PProcess process : processes.values()) {
      system.addProcess(process);
    }
    for (PProcess process : modelSymbolTable.getProcesses()) {
      system.addProcess(process);
    }

    // Add observer process
    if (observerProcess != null)
      system.addProcess(observerProcess);

    // Add tester process
    if (testerProcess != null)
      system.addProcess(testerProcess);

    // Add init process
    system.addProcess(initProcess);

    // Add specification
    if (specification != null)
      system.setSpecification(specification);

    return system;
  }

  /**
   * Determine initialising expressions for an object's attribute.
   *
   * @param object a UML object
   * @param attribute an attribute of the object's classifier
   * @return a list of initialising expressions
   */
  private static List<PExpression> getInitialisers(UObject object, UAttribute attribute, ModelSymbolTable symbolTable) {
    List<PExpression> initialisers = new ArrayList<>();
    
    // Find the slot for the given attribute
    USlot slot = null;
    for (USlot objectSlot : object.getSlots()) {
      if (attribute.equals(objectSlot.getAttribute())) {
        slot = objectSlot;
        break;
      }
    }
    // If there is no matching slot, the initial values of the
    // attribute are taken as the constant's values
    if (slot == null) {
      // There may be several initial values for the attribute
      if (attribute.isArray()) {
        int size = attribute.getSize();
        for (int i = 0; i < size; i++)
          initialisers.add(UMLTranslator.translateValue(requireNonNull(attribute.getInitialValue(i)), attribute.getInitialValuesContext(), symbolTable));
      }
      else
        initialisers.add(UMLTranslator.translateValue(requireNonNull(attribute.getInitialValue(0)), attribute.getInitialValuesContext(), symbolTable));
    }
    // If there is a matching slot, the values of the slot are
    // taken; but maybe they have to be supplemented by initial
    // values for the attribute
    else {
      // There may be several values of the slot
      if (attribute.isArray()) {
        int size = attribute.getSize();
        int i = 0;
        while (i < slot.getValues().size() && i < size) {
          UExpression value = slot.getValues().get(i);
          initialisers.add(UMLTranslator.translateValue(requireNonNull(value), slot.getValuesContext(), symbolTable));
          i++;
        }
        while (i < size) {
          UExpression value = attribute.getInitialValue(i);
          initialisers.add(UMLTranslator.translateValue(requireNonNull(value), attribute.getInitialValuesContext(), symbolTable));
          i++;
        }
      }
      else {
        initialisers.add(UMLTranslator.translateValue(requireNonNull(slot.getValues().get(0)), slot.getValuesContext(), symbolTable));
      } 
    }
    
    return initialisers;
  }
  
  /**
   * Determine notification statement for observer.
   *
   * @param direction a Promela expression, representing the notification's direction
   * @param source a Promela expression, representing the notification's sender
   * @param target a Promela expression, representing the notification's receiver
   * @param event a Promela expression, representing the notification's event
   * @param arguments a list of Promela expression, representing the notification's arguments
   * @param symbolTable state machine symbol table
   * @return Promela statement for notification
   */
  static PStatement getNotification(PExpression direction, PExpression source, PExpression target, PExpression event, List<PExpression> arguments, StateMachineSymbolTable symbolTable) {
    if (!symbolTable.isObserved())
      return PStatement.skip();

    PStatement statement = PStatement.send(PExpression.channel(symbolTable.getObserverChannel()));
    statement.addSendStmArg(direction);
    statement.addSendStmArg(source);
    statement.addSendStmArg(target);
    statement.addSendStmArg(event);
    for (int i = 0; i < symbolTable.getMaxParameters(); i++) {
      if (i < arguments.size())
        statement.addSendStmArg(arguments.get(i));
      else
        statement.addSendStmArg(PExpression.constant(PConstant.numeric(0)));
    }
    return statement;
  }
  
  /**
   * Translate UML action into Promela
   *
   * @param uAction UML action
   * @param uContext UML context
   * @param context Context for translation of Smile
   * @return translated Promela statement
   */
  static PStatement translateAction(UAction uAction, UContext uContext, StateMachineSymbolTable context) {
    return new ActionTranslator(uContext, context).statement(uAction);
  }
  
  /**
   * Translate UML expression into Promela
   *
   * @param uExpression UML expression
   * @param uContext UML context
   * @param context Context for translation of Smile
   * @return translated Promela expression
   */
  static PExpression translateExpression(UExpression uExpression, UContext uContext, StateMachineSymbolTable context) {
    return new ExpressionTranslator(uContext, context).expression(uExpression);
  }
  
  /**
   * Translate UML guard expression into Promela
   *
   * (Guard origins from Smile branch statement)
   *
   * @param uExpression UML expression
   * @param uContext UML context
   * @param symbolTable symbol table for translation of Smile
   * @return translated Promela expression
   */
  static PExpression translateGuardExpression(UExpression uExpression, UContext uContext, SymbolTable symbolTable) {
    return new ExpressionTranslator(uContext, symbolTable).expression(uExpression);
  }
  
  /**
   * Translate UML constant expression into Promela
   *
   * @param uExpression UML expression
   * @param uContext UML context
   * @param context Context for translation of Smile
   * @return translated Promela expression
   */
  static PExpression translateValue(UExpression uExpression, UContext uContext, ModelSymbolTable symbolTable) {
    return new ExpressionTranslator(uContext, symbolTable).expression(uExpression);
  }
  
  /**
   * Translator of UML actions
   */
  private static final class ActionTranslator implements UAction.InContextVisitor<PStatement> {
    private UContext uContext;
    private StateMachineSymbolTable symbolTable;
    private @Nullable UAction uAction;
    
    private ActionTranslator(UContext uContext, StateMachineSymbolTable symbolTable) {
      this.uContext = uContext;
      this.symbolTable = symbolTable;
    }
    
    private PStatement statement(UAction uAction) {
      this.uAction = uAction;
      return uAction.accept(this);
    }
    
    private RuntimeException exception() {
      return new RuntimeException("Cannot translate UML action " + Formatter.quoted(uAction) + " into Promela");
    }
    
    public UContext getContext() {
      return this.uContext;
    }
    
    public PStatement onSkip() {
      return PStatement.skip();
    }
    
    public PStatement onChoice(UAction left, UAction right) {
      PStatement pStatement = PStatement.selection();
      pStatement.addBranch(PStatement.sequence(PStatement.expression(PExpression.trueConst()), this.statement(left)));
      pStatement.addBranch(PStatement.sequence(PStatement.expression(PExpression.trueConst()), this.statement(right)));
      return pStatement;
    }
    
    public PStatement onParallel(UAction left, UAction right) {
      return this.statement(UAction.par(left, right).getInterleavingNormalForm());
    }
    
    public PStatement onSequential(UAction left, UAction right) {
      return PStatement.sequence(this.statement(left), this.statement(right));
    }
    
    public PStatement onConditional(UExpression condition, UAction left, UAction right) {
      PStatement pStatement = PStatement.selection();
      pStatement.addBranch(PStatement.sequence(PStatement.expression(UMLTranslator.translateGuardExpression(condition, this.uContext, this.symbolTable)),
                                               this.statement(left)));
      pStatement.setElseBranch(this.statement(right));
      return pStatement;
    }
    
    public PStatement onAssertion(UExpression uExpression) {
      return PStatement.assertion(UMLTranslator.translateGuardExpression(uExpression, this.uContext, this.symbolTable));
    }
    
    public PStatement onSelfInvocation(UBehavioural uBehavioural, List<UExpression> uArguments) {
      if (!uBehavioural.isStatic())
        return getInvocation(PExpression.variable(symbolTable.getThisVariable()), uBehavioural, uArguments);
      return PStatement.skip();
    }

    public PStatement onInvocation(UStatual uStatual, UBehavioural uBehavioural, List<UExpression> uArguments) {
      return getInvocation(this.symbolTable.access(uStatual), uBehavioural, uArguments);
    }

    public PStatement onArrayInvocation(UAttribute uAttribute, UExpression uOffset, UBehavioural uBehavioural, List<UExpression> uArguments) {
      return getInvocation(PExpression.bracket(PExpression.variable(this.symbolTable.getVariable(uAttribute)),
                                               UMLTranslator.translateExpression(uOffset, this.uContext, this.symbolTable)), uBehavioural, uArguments);
    }

    public PStatement onAssignment(UAttribute uAttribute, UExpression uExpression) {
      PExpression varExp = PExpression.variable(this.symbolTable.getVariable(uAttribute));
      PExpression exp = UMLTranslator.translateExpression(uExpression, this.uContext, this.symbolTable);
      PStatement statement = PStatement.assignment(varExp, exp);
      return statement;
    }
    
    public PStatement onArrayAssignment(UAttribute uAttribute, UExpression uOffset, UExpression uExpression) {
      PExpression varExp = PExpression.variable(this.symbolTable.getVariable(uAttribute));
      PExpression offsetExp = UMLTranslator.translateExpression(uOffset, this.uContext, this.symbolTable);
      PExpression arrayExp = PExpression.bracket(varExp, offsetExp);
      PExpression exp = UMLTranslator.translateExpression(uExpression, this.uContext, this.symbolTable);
      PStatement statement = PStatement.assignment(arrayExp, exp);
      return statement;
    }
    
    public PStatement onPost(UAttribute uAttribute, UAction.PostKind kind) {
      PStatement statement = null;
      
      PExpression varExp = PExpression.variable(this.symbolTable.getVariable(uAttribute));
      
      switch (kind) {
        case INC:
          statement = PStatement.expression(PExpression.increment(varExp));
          break;
        case DEC:
          statement = PStatement.expression(PExpression.decrement(varExp));
          break;
      }
      
      if (statement == null)
        throw exception();
      return statement;
    }
    
    public PStatement onArrayPost(UAttribute uAttribute, UExpression uOffset, UAction.PostKind kind) {
      PStatement statement = null;
      
      PExpression varExp = PExpression.variable(this.symbolTable.getVariable(uAttribute));
      PExpression offsetExp = UMLTranslator.translateExpression(uOffset, this.uContext, this.symbolTable);
      PExpression arrayExp = PExpression.bracket(varExp, offsetExp);
      
      switch (kind) {
      case INC :
        statement = PStatement.expression(PExpression.increment(arrayExp));
        break;
      case DEC :
        statement = PStatement.expression(PExpression.decrement(arrayExp));
        break;
      }
      
      if (statement == null)
        throw exception();
      return statement;
    }

    /**
     * Get Promela statement for an invocation of a behavioural with parameters on a target.
     *
     * @param target
     * @param uBehavioural
     * @param uArguments
     * @return
     */
    private PStatement getInvocation(PExpression target, UBehavioural uBehavioural, List<UExpression> uArguments) {
      PStatement statement = null;
      
      PExpression source = PExpression.variable(symbolTable.getThisVariable());
      PExpression event = PExpression.constant(symbolTable.getConstant(uBehavioural));
      List<PExpression> arguments = getArguments(uArguments);
      PExpression queueExpression = symbolTable.accessExternalEventQueue(target);
      PExpression ack_in = null;
      if (this.symbolTable.hasOperations())
        ack_in = PExpression.channel(symbolTable.getAcknowledgeInChannel());
      
      // Check whether queue is full
      if (this.symbolTable.getProperties().isMutex())
        statement = PStatement.sequence(statement, this.symbolTable.checkFull(queueExpression));
        
      // Create observer notification
      statement = PStatement.sequence(statement, getNotification(PExpression.constant(symbolTable.getSendingConstant()), source, target, event, arguments, symbolTable));
      
      // Create send statement
      statement = PStatement.sequence(statement, getSend(source, queueExpression, event, arguments, ack_in));

      // Create acknowledge statement
      if (uBehavioural instanceof UOperation) {
        PStatement step = getAck(requireNonNull(ack_in));
        if (this.symbolTable.getProperties().isMutex())
          step = this.symbolTable.yieldMutex(step);
        statement = PStatement.sequence(statement, step);
      }
 
      return statement;
    }
    
    /**
     * Get Promela expressions for arguments of an invocation.
     *
     * @param uParameters list of parameters for the invocation
     * @return list of Promela expressions for arguments
     */
    private List<PExpression> getArguments(List<UExpression> uParameters) {
      @NonNull List<PExpression> arguments = new ArrayList<>();
      // Actual parameters
      for (int i = 0; i < uParameters.size(); i++) {
        UExpression actualParameter = requireNonNull(uParameters.get(i));
        arguments.add(UMLTranslator.translateExpression(actualParameter, this.uContext, this.symbolTable));
      }
      // Empty parameters
      for (int i = uParameters.size(); i < this.symbolTable.getMaxParameters(); i++) {
        arguments.add(PExpression.constant(this.symbolTable.getEmptyConstant()));
      }
      return arguments;
    }

    /**
     * @param source source object expression
     * @param queueExpression receiving queue expression
     * @param event event expression
     * @param arguments list of argument expressions
     * @param ack_in possibly a channel expression for receiving an acknowledgement
     * @return Promela sending expression
     */
    private PStatement getSend(PExpression source, PExpression queueExpression, PExpression event, List<PExpression> arguments, @Nullable PExpression ack_in) {
      PStatement sendStm = PStatement.send(queueExpression);
      sendStm.addSendStmArg(event);
      sendStm.addSendStmArg(source);
      sendStm.addSendStmArgs(arguments);
      if (ack_in != null)
        sendStm.addSendStmArg(ack_in);
      return sendStm;
    }

    /**
     * @param ack_in channel expression for receiving the acknowledgement
     * @return Promela acknowledgement expression
     */
    private PStatement getAck(PExpression ack_in) {
      PStatement step = PStatement.receive(ack_in);
      step.addRecvStmArg(PExpression.variable(PVariable.uscore()));
      return step;
    }
  }
  
  /**
   * Translator of UML expressions
   */
  private static final class ExpressionTranslator implements UExpression.InContextVisitor<PExpression> {
    private UContext uContext;
    private SymbolTable symbolTable;
    private @Nullable UExpression uExpression;
    
    private ExpressionTranslator(UContext uContext, SymbolTable symbolTable) {
      this.uContext = uContext;
      this.symbolTable = symbolTable;
    }
    
    private PExpression expression(UExpression uExpression) {
      this.uExpression = uExpression;
      return uExpression.accept(this);
    }
    
    private RuntimeException exception() {
      return new RuntimeException("Cannot translate UML expression " + requireNonNull(this.uExpression).toString() + " into Promela");
    }
    
    public UContext getContext() {
      return this.uContext;
    }
    
    public PExpression onBooleanConstant(boolean boolConst) {
      return (boolConst ? PExpression.trueConst() : PExpression.falseConst());
    }
    
    public PExpression onIntegerConstant(int intConst) {
      return PExpression.constant(PConstant.numeric(intConst));
    }

    public PExpression onStringConstant(String stringConst) {
      throw exception();
    }
    
    public PExpression onNull() {
      return PExpression.constant(symbolTable.getEmptyConstant());
    }
    
    public PExpression onThis() {
      return symbolTable.getThisExpression();
    }
    
    public PExpression onStatual(UStatual uStatual) {
      return this.symbolTable.access(uStatual);
    }
    
    public PExpression onArray(UAttribute uAttribute, UExpression uOffset) {
      PExpression offsetExpression = expression(uOffset);
      return this.symbolTable.access(uAttribute, offsetExpression);
    }
    
    public PExpression onConditional(UExpression condition, UExpression left, UExpression right) {
      PExpression expCond = UMLTranslator.translateGuardExpression(condition, this.uContext, this.symbolTable);
      PExpression expLeft = this.expression(left);
      PExpression expRight = this.expression(right);
      return PExpression.conditional(expCond, expLeft, expRight);
    }
    
    public PExpression onUnary(UOperator unary, UExpression inner) {
      PExpression innerExpression = this.expression(inner);
      switch (unary) {
        case UPLUS:
          return innerExpression;
        case UMINUS:
          return PExpression.unaryMinus(innerExpression);
        case UNEG:
          return PExpression.not(innerExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }
    
    public PExpression onArithmetical(UExpression left, UOperator arithmetical, UExpression right) {
      PExpression leftExp = this.expression(left);
      PExpression rightExp = this.expression(right);
      switch (arithmetical) {
        case ADD:
          return PExpression.binary(leftExp, POperator.PLUS, rightExp);
        case SUB:
          return PExpression.binary(leftExp, POperator.MINUS, rightExp);
        case MULT:
          return PExpression.binary(leftExp, POperator.MULT, rightExp);
        case DIV:
          return PExpression.binary(leftExp, POperator.DIV, rightExp);
        case MOD:
          return PExpression.binary(leftExp, POperator.MOD, rightExp);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }
    
    public PExpression onRelational(UExpression left, UOperator relational, UExpression right) {
      switch (relational) {
        case LT:
          return PExpression.binary(this.expression(left), POperator.LT, this.expression(right));
        case LEQ:
          return PExpression.binary(this.expression(left), POperator.LE, this.expression(right));
        case EQ:
          return PExpression.binary(this.expression(left), POperator.EQ, this.expression(right));
        case NEQ:
          return PExpression.binary(this.expression(left), POperator.NEQ, this.expression(right));
        case GEQ:
          return PExpression.binary(this.expression(left), POperator.GE, this.expression(right));
        case GT:
          return PExpression.binary(this.expression(left), POperator.GT, this.expression(right));
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }
    
    public PExpression onJunctional(UExpression left, UOperator junctional, UExpression right) {
      PExpression leftExp = this.expression(left);
      PExpression rightExp = this.expression(right);
      PExpression trueExp = PExpression.trueConst();
      PExpression falseExp = PExpression.falseConst();
      
      switch (junctional) {
        case AND: {
          PExpression expCond = PExpression.binary(leftExp, POperator.EQ, falseExp);
          return PExpression.conditional(expCond, falseExp, rightExp);
        }
      
        case OR: {
          PExpression expCond = PExpression.binary(leftExp, POperator.EQ, trueExp);
          return PExpression.conditional(expCond, trueExp, rightExp);
        }

        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }
  }
  
  public static URun reconstruct(List<PSystemState> pTrail, UCollaboration uCollaboration, @Nullable UInteraction uInteraction, @Nullable UTestCase uTestCase) {
    var uRun = new URun();
    var symbolTable = translation.uml2promela.ModelSymbolTable.create(uCollaboration, uInteraction, uTestCase);
    for (var pSystemState : pTrail) {
      var uSystemState = new uml.run.USystemState();
      var isReconstructable = false;
      for (var uObject : uCollaboration.getObjects()) {
        var uObjectState = uSystemState.getObjectState(uObject);
        isReconstructable |= reconstructQueue(uObjectState, pSystemState, symbolTable);
        isReconstructable |= reconstructObject(uObjectState, pSystemState, symbolTable);
        isReconstructable |= reconstructStateMachine(uObjectState, pSystemState, symbolTable);
      }
      if (isReconstructable)
        uRun.addSystemState(uSystemState);
    }
    return uRun;    
  }
  
  private static boolean reconstructObject(UObjectState uObjectState, PSystemState pSystemState, ModelSymbolTable symbolTable) {
    UObject uObject = uObjectState.getObject();
    if (uObject.getC1ass().getStateMachine() == null)
      return false;
    
    StateMachineSymbolTable stateMachineSymbolTable = symbolTable.getStateMachineSymbolTable(uObject.getC1ass());
    PProcess pProcess = stateMachineSymbolTable.getProcess();
    int pProcessNumber = pSystemState.getValue(symbolTable.getProcessIdentifiers(), symbolTable.getConstant(uObject).getValue());
    if (pProcessNumber == 0) {
      util.Message.debug().info("No process number for object `" + uObject.getName() + "' found.");
      return false;
    }
    PProcessState pObjectProcessState = pSystemState.getProcessState(pProcess, pProcessNumber);
    if (pObjectProcessState == null) {
      util.Message.debug().info("No process state for object `" + uObject.getName() + "' (process number " + pProcessNumber + ") found.");
      return false;
    }
    
    // Reconstruct stimulus
    PVariable stimulusVariable = stateMachineSymbolTable.getStimulusVariable();
    int stimulusValue = pObjectProcessState.getValue(stimulusVariable);
    if (stimulusValue != 0) {
      // Cope also for queues for objects without operations
      UEvent uStimulus = stateMachineSymbolTable.getEvent(stimulusValue);
      if (uStimulus == null)
        util.Message.debug().info("No event for number `" + stimulusValue + "' found.");
      else {
        UObject uSender = null;
        List<UExpression> uArguments = new ArrayList<>();
        if (uStimulus.isBehavioural()) {
          UBehavioural uBehavioural = uObject.getC1ass().getBehavioural(uStimulus);
          if (uBehavioural == null)
            util.Message.debug().info("No behavioural for `" + uStimulus + "' in object `" + uObject.getName() + "' (process number " + pProcessNumber + ") found.");
          else {
            PVariable parameters = stateMachineSymbolTable.getParametersVariable();
            for (int i = 0; i < uBehavioural.getParameters().size(); i++) {
              UExpression uArgument = UMLTranslator.reconstructExpression(uBehavioural.getParameters().get(i).getType(), pObjectProcessState.getValue(parameters, i), symbolTable);
              uArguments.add(uArgument);
            }
          }
        }
        uObjectState.setStimulus(uStimulus, uArguments, uSender);
      }
    }
    
    // Reconstruct attributes
    for (UAttribute uAttribute : uObject.getC1ass().getAttributes()) {
      if (uAttribute.isConstant())
        continue;
      
      PVariable attributeVariable = stateMachineSymbolTable.getVariable(uAttribute);
      
      if (uAttribute.isStatic()) {
        if (uAttribute.isArray()) {
          List<UExpression> attributeValues = new ArrayList<>();
          for (int i = 0; i < uAttribute.getSize(); i++) {
            UExpression attributeValue = UMLTranslator.reconstructExpression(uAttribute.getType().getUnderlyingType(), pSystemState.getValue(attributeVariable, i), symbolTable);
            attributeValues.add(attributeValue);
          }
          uObjectState.addAttributeValues(uAttribute, attributeValues);
        }
        else {
          UExpression uAttributeValue = UMLTranslator.reconstructExpression(uAttribute.getType(), pSystemState.getValue(attributeVariable), symbolTable);
          uObjectState.addAttributeValue(uAttribute, uAttributeValue);
        }
        continue;
      }
      
      if (uAttribute.isArray()) {
        List<UExpression> uAttributeValues = new ArrayList<>();
        for (int i = 0; i < uAttribute.getSize(); i++) {
          UExpression uAttributeValue = UMLTranslator.reconstructExpression(uAttribute.getType().getUnderlyingType(), pObjectProcessState.getValue(attributeVariable, i), symbolTable);
          uAttributeValues.add(uAttributeValue);
        }
        uObjectState.addAttributeValues(uAttribute, uAttributeValues);
        continue;
      }
      
      // Simple, non-constant, non-static attribute
      UExpression uAttributeValue = UMLTranslator.reconstructExpression(uAttribute.getType(), pObjectProcessState.getValue(attributeVariable), symbolTable);
      uObjectState.addAttributeValue(uAttribute, uAttributeValue);
    }

    return true;
  }
  
  private static boolean reconstructStateMachine(UObjectState uObjectState, PSystemState pSystemState, ModelSymbolTable symbolTable) {
    UObject uObject = uObjectState.getObject();
    if (uObject.getC1ass().getStateMachine() == null)
      return false;
    
    UStateMachine stateMachine = requireNonNull(uObject.getC1ass().getStateMachine());
    StateMachineSymbolTable stateMachineSymbolTable = symbolTable.getStateMachineSymbolTable(uObject.getC1ass());
    PProcess process = stateMachineSymbolTable.getProcess();
    int processNumber = pSystemState.getValue(symbolTable.getProcessIdentifiers(), symbolTable.getConstant(uObject).getValue());
    if (processNumber == 0) {
      util.Message.debug().info("No process number for object `" + uObject.getName() + "' found.");
      return false;
    }
    PProcessState objectProcessState = pSystemState.getProcessState(process, processNumber);
    if (objectProcessState == null) {
      util.Message.debug().info("No process state for object `" + uObject.getName() + "' (process number " + processNumber + ") found.");
      return false;
    }
    
    // Gather all states we are in
    Set<UVertex> vertices = new HashSet<>();
    for (PVariable stateVariable : stateMachineSymbolTable.getStateVariables()) {
      int stateVariableValue = objectProcessState.getValue(stateVariable);
      if (stateVariableValue != 0) {
        UVertex uVertex = stateMachineSymbolTable.getVertex(stateVariableValue);
        if (uVertex != null)
          vertices.add(uVertex);
      }
    }
    
    // Build state forest
    UVertexForest stateForest = stateMachine.getTargetStateForest(vertices);
    util.Message.debug().info("Vertices = `" + vertices + "', stateForest = `" + stateForest + "'");
    
    // Mark all completed states
    for (UState state : vertices.stream().filter(v -> v instanceof UState).map(v -> (@NonNull UState)v).
                                          filter(s -> stateMachineSymbolTable.getCompletionTree().isCompletionState(s)).collect(toSet())) {
      PVariable completedVariable = stateMachineSymbolTable.getCompletedVariable();
      int completionNumber = stateMachineSymbolTable.getCompletionTree().getCompletionNumber(state);
      int stateVariableValue = objectProcessState.getValue(completedVariable, completionNumber);
      if (stateVariableValue != 0) {
        UVertexTree subStateTree = stateForest.getVertexTree(state);
        util.Message.debug().info("State = `" + state + "', subStateTree = `" + subStateTree + "'");
        if (subStateTree != null) {
          UVertexTree completedSubStateTree = UVertexTree.completed(subStateTree);
          stateForest = stateForest.getSubstituted(completedSubStateTree);
        }
      }
    }
    
    // Mark all timed-out states
    for (UState state : vertices.stream().filter(v -> v instanceof UState).map(v -> (@NonNull UState)v).
                                          filter(UState::hasOutgoingTime).collect(toSet())) {
      for (UTransition time : state.getOutgoingTimes()) {
        UEvent timeEvent = time.getTrigger();
        PVariable timedOutVariable = stateMachineSymbolTable.getTimedOutVariable();
        int timeEventNumber = stateMachineSymbolTable.getTimerTree().getTimerNumber(timeEvent);
        int timedOutVariableValue = objectProcessState.getValue(timedOutVariable, timeEventNumber);
        if (timedOutVariableValue != 0) {
          UVertexTree subStateTree = stateForest.getVertexTree(state);
          if (subStateTree != null) {
            Set<UEvent> timedOutEvents = new HashSet<>(subStateTree.getTopTimedOuts());
            timedOutEvents.add(timeEvent);
            UVertexTree timedOutSubStateTree = UVertexTree.timedOut(timedOutEvents, subStateTree);
            stateForest = stateForest.getSubstituted(timedOutSubStateTree);
          }
        }
      }
    }

    uObjectState.setStateForest(stateForest);
    if (!stateForest.isConfiguration())
      util.Message.debug().info("No complete configuration for object `" + uObject.getName() + "' found.");
    else {
      UConfiguration configuration = UConfiguration.create(stateForest);
      uObjectState.setConfiguration(configuration);
    }

    return true;
  } 
  
  private static boolean reconstructQueue(UObjectState uObjectState, PSystemState pSystemState, ModelSymbolTable symbolTable) {
    UObject uObject = uObjectState.getObject();
    if (uObject.getC1ass().getStateMachine() == null)
      return false;

    StateMachineSymbolTable stateMachineSymbolTable = symbolTable.getStateMachineSymbolTable(uObject.getC1ass());
    PProcess objectProcess = stateMachineSymbolTable.getProcess();
    int objectNumber = symbolTable.getConstant(uObject).getValue();
    
    var uQueueState = uObjectState.getQueueState();

    PChannel externalEvents = symbolTable.getGlobalEventQueuesChannel();
    int externalSize = symbolTable.getQueueSizeConstant().getValue();
    for (int i = 0; i < externalSize; i++) {
      List<Integer> externalEventList = pSystemState.getValue(externalEvents, objectNumber, i);
      if (externalEventList == null || externalEventList.isEmpty())
        continue;

      UObject sender = null; // TODO (JW060311) How to reconstruct the sender?
      var eventNumber = externalEventList.get(0).intValue();
      var event = stateMachineSymbolTable.getEvent(eventNumber);
      if (event == null) {
        util.Message.debug().info("No external event with number " + eventNumber + " found.");
        continue;
      }
      
      UBehavioural behavioural = uObject.getC1ass().getBehavioural(event);
      if (behavioural == null) {
        util.Message.debug().info("No behavioural for external event `" + event + "' found.");
        continue;
      }

      List<UExpression> arguments = new ArrayList<>();
      for (int j = 0; j < behavioural.getParameters().size(); j++) {
        UExpression argument = UMLTranslator.reconstructExpression(behavioural.getParameters().get(j).getType(), externalEventList.get(j+1).intValue(), symbolTable);
        arguments.add(argument);
      }
      uQueueState.addExternalMessage(new UMessage(""+i, sender, behavioural, arguments, uObject));
      util.Message.debug().info("" + uQueueState);
    }
    
    int processNumber = pSystemState.getValue(symbolTable.getProcessIdentifiers(), objectNumber);
    if (processNumber == 0) {
      util.Message.debug().info("No process number for object `" + uObject.getName() + "' found.");
      return false;
    }
    PProcessState objectProcessState = pSystemState.getProcessState(objectProcess, processNumber);
    if (objectProcessState == null) {
      util.Message.debug().info("No process state for object `" + uObject.getName() + "' (process number = " + processNumber + ") found.");
      return !uQueueState.isEmpty();
    }
    
    PChannel internalEvents = stateMachineSymbolTable.getInternalQueueChannel();
    int internalSize = symbolTable.getInternalQueueSizeConstant().getValue();
    for (int i = 0; i < internalSize; i++) {
      List<Integer> internalEventList = objectProcessState.getValue(internalEvents, i);
      if (internalEventList != null && !internalEventList.isEmpty()) {
        UEvent event = stateMachineSymbolTable.getEvent(internalEventList.get(0).intValue());
        if (event == null)
          util.Message.debug().info("No internal event for number `" + i + "' found.");
        if (event != null)
          uQueueState.addInternalEvent(event);
      }
    }    
    
    if (stateMachineSymbolTable.hasDeferrableEvents()) {
      PChannel deferredEvents = stateMachineSymbolTable.getDeferredQueueChannel();
      int deferredSize = symbolTable.getDeferredQueueSizeConstant().getValue();
      for (int i = 0; i < deferredSize; i++) {
        UObject sender = null;
        List<Integer> deferredEventList = objectProcessState.getValue(deferredEvents, i); 
        if (deferredEventList != null && !deferredEventList.isEmpty()) {
          UEvent deferredEvent = stateMachineSymbolTable.getEvent(deferredEventList.get(0).intValue());
          if (deferredEvent == null)
            util.Message.debug().info("No deferred event for number `" + i + "' found.");
          if (deferredEvent != null) {
            UBehavioural behavioural = uObject.getC1ass().getBehavioural(deferredEvent);
            if (behavioural == null)
              util.Message.debug().info("No behavioural for deferred event `" + deferredEvent + "' found.");
            if (behavioural != null) {
              List<UExpression> arguments = new ArrayList<>();
              for (int j = 0; j < behavioural.getParameters().size(); j++) {
                UExpression argument = UMLTranslator.reconstructExpression(behavioural.getParameters().get(j).getType(), deferredEventList.get(j+1).intValue(), symbolTable);
                arguments.add(argument);
              }
              uQueueState.addDeferredMessage(new UMessage(""+i, sender, behavioural, arguments, uObject));
            }
          }
        }
      }
    }
    
    return true;
  }
  
  public static UExpression reconstructExpression(UType type, int value, ModelSymbolTable symbolTable) {
    var model = symbolTable.getModel();
    
    if (type.isNull())
      return UExpression.nullConst();
    
    if (type.isClass()) {
      if (value == symbolTable.getEmptyConstant().getValue())
        return UExpression.nullConst();
      var uObject = symbolTable.getObject(value);
      if (uObject != null)
        return UExpression.id(Identifier.id(uObject.getName()));
    }
    
    if (type.isDataType()) {
      if (type.equals(model.getBooleanType()))
        return (value != 0 ? UExpression.trueConst() : UExpression.falseConst());
      if (type.equals(model.getIntegerType()))
        return UExpression.intConst(value);
      if (type.equals(model.getClockType()))
        return UExpression.intConst(value);
    }
    
    util.Message.debug().info("Cannot reconstruct expression from value `" + value + "' in type `" + type + "'");
    return UExpression.intConst(0);
  }
}
