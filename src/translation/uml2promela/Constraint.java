package translation.uml2promela;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.eclipse.jdt.annotation.NonNullByDefault;

import promela.PConstant;
import promela.PExpression;
import promela.PFormula;
import promela.PMacro;
import promela.POperator;
import uml.UAttribute;
import uml.UContext;
import uml.UObject;
import uml.ocl.OExpression;
import uml.ocl.OLTLConstraint;
import uml.ocl.OLTLFormula;
import uml.ocl.OOperator;
import uml.smile.SConstant;
import uml.statemachine.UVertex;


/**
 * Translation of (simple) UML/OCL constraints into Promela
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class Constraint {
  /**
   * Translate a (UML/OCL) constraint into a Promela-LTL formula.
   *
   * @param constraint a (UML/OCL) constraint
   * @param symbolTable shared symbol table
   * @return a Promela-LTL formula
   */
  public static PFormula create(OLTLConstraint constraint, ModelSymbolTable symbolTable) {
    return translateConstraint(constraint, symbolTable);
  }

  static final class ConstraintTranslator implements OLTLConstraint.Visitor<PFormula> {
    private ModelSymbolTable symbolTable;

    ConstraintTranslator(ModelSymbolTable symbolTable) {
      this.symbolTable = symbolTable;
    }

    PFormula process(OLTLConstraint constraint) {
      return constraint.accept(this);
    }

    public PFormula onFormula(OLTLFormula formula) {
      return translateFormula(formula, symbolTable);
    }
  }

  public static PFormula translateConstraint(OLTLConstraint constraint, ModelSymbolTable symbolTable) {
    return (new ConstraintTranslator(symbolTable)).process(constraint);
  }

  static final class FormulaTranslator implements OLTLFormula.InContextVisitor<PFormula> {
    private ModelSymbolTable symbolTable;
    private UContext context;
    private Stack<OLTLFormula> oFormulae = new Stack<>();
    private Map<PExpression, PMacro> pTerms = new HashMap<>();

    FormulaTranslator(UContext context, ModelSymbolTable symbolTable) {
      this.context = context;
      this.symbolTable = symbolTable;
    }

    PFormula process(OLTLFormula formula) {
      oFormulae.push(formula);
      PFormula pFormula = formula.accept(this);
      oFormulae.pop();
      return pFormula;
    }

    PMacro getMacro(PExpression pExpression) {
      PMacro macro = this.pTerms.get(pExpression);
      if (macro != null)
        return macro;

      macro = PMacro.expression(symbolTable.uniqueIdentifier("term"), pExpression);
      this.pTerms.put(pExpression, macro);
      return macro;
    }

    RuntimeException exception() {
      return new RuntimeException("Cannot translate formula `" + oFormulae.peek() + "' in context `" + context.description() + "' into Promela-LTL");
    }

    @Override
    public UContext getContext() {
      return this.context;
    }

    @Override
    public PFormula onDeadlock() {
      return PFormula.term(this.getMacro(PExpression.variable(symbolTable.getDeadlockVariable())));
    }

    @Override
    public PFormula onExpression(OExpression oExpression) {
      return PFormula.term(this.getMacro(translateExpression(oExpression, symbolTable)));
    }

    @Override
    public PFormula onNeg(OLTLFormula oFormula) {
      return PFormula.neg(process(oFormula));
    }

    @Override
    public PFormula onJunctional(OLTLFormula leftOFormula, OOperator oOp, OLTLFormula rightOFormula) {
      PFormula leftPFormula = process(leftOFormula);
      PFormula rightPFormula = process(rightOFormula);
      switch (oOp) {
        case FAND:
          return PFormula.and(leftPFormula, rightPFormula);
        case FOR:
          return PFormula.or(leftPFormula, rightPFormula);
        case FIMPL:
          return PFormula.imply(leftPFormula, rightPFormula);
        //$CASES-OMITTED$
        default:
          throw exception();
      }      
    }

    @Override
    public PFormula onSometime(OLTLFormula oFormula) {
      return PFormula.sometime(process(oFormula));
    }

    @Override
    public PFormula onAlways(OLTLFormula oFormula) {
      return PFormula.always(process(oFormula));
    }

    @Override
    public PFormula onUntil(OLTLFormula leftOFormula, OLTLFormula rightOFormula) {
      return PFormula.until(process(leftOFormula), process(rightOFormula));
    }
  }

  public static PFormula translateFormula(OLTLFormula oFormula, ModelSymbolTable symbolTable) {
    return (new FormulaTranslator(symbolTable.getCollaboration().getConstraintsContext(), symbolTable)).process(oFormula);
  }

  static final class ExpressionTranslator implements OExpression.InContextVisitor<PExpression> {
    private ModelSymbolTable symbolTable;
    private UContext context;
    private Stack<OExpression> oExpressions = new Stack<>();

    ExpressionTranslator(UContext context, ModelSymbolTable symbolTable) {
      this.context = context;
      this.symbolTable = symbolTable;
    }

    PExpression process(OExpression oExpression) {
      this.oExpressions.push(oExpression);
      PExpression pExpression = oExpression.accept(this);
      this.oExpressions.pop();
      return pExpression;
    }

    RuntimeException exception() {
      throw new RuntimeException("Cannot translate OCL expression `" + this.oExpressions.peek() + "' in model `" + context.description() + "' into Promela");
    }

    @Override
    public UContext getContext() {
      return this.context;
    }

    @Override
    public PExpression onBooleanConstant(boolean booleanConstant) {
      return (booleanConstant ? PExpression.trueConst() : PExpression.falseConst());
    }

    @Override
    public PExpression onIntegerConstant(int integerConstant) {
      return PExpression.constant(PConstant.numeric(integerConstant));
    }
    
    @Override
    public PExpression onNullConstant() {
      return PExpression.constant(symbolTable.getEmptyConstant());
    }
    
    @Override
    public PExpression onReference(UAttribute uAttribute) {
      return this.symbolTable.access(uAttribute);
    }
    
    @Override
    public PExpression onReference(UObject object, UAttribute attribute) {
      return this.symbolTable.accessRemote(object, attribute);
    }
    
    @Override
    public PExpression onArrayReference(UAttribute uAttribute, OExpression offset) {
      return this.symbolTable.access(uAttribute, process(offset));
    }
    
    @Override
    public PExpression onArrayReference(UObject uObject, UAttribute uAttribute, OExpression offset) {
      return this.symbolTable.accessRemote(uObject, uAttribute, process(offset));
    }

    @Override
    public PExpression onState(UObject uObject, UVertex uVertex) {
      StateMachineSymbolTable stateMachineSymbolTable = this.symbolTable.getStateMachineSymbolTable(uObject.getC1ass());
      PExpression pVertexVariable = PExpression.variable(stateMachineSymbolTable.getVertexVariable(uVertex));
      PExpression pVertexRef = PExpression.remoteRef(stateMachineSymbolTable.getProcessName(), stateMachineSymbolTable.getProcessIdentifier(uObject), pVertexVariable);
      PExpression pVertexConstant = PExpression.constant(stateMachineSymbolTable.getConstant(SConstant.vertex(uVertex)));
      return PExpression.eq(pVertexRef, pVertexConstant);
    }

    @Override
    public PExpression onUnary(OOperator oOp, OExpression oExpression) {
      PExpression pExpression = process(oExpression);
      switch (oOp) {
        case UPLUS:
          return pExpression;
        case UMINUS:
          return PExpression.unaryMinus(pExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }
    
    @Override
    public PExpression onArithmetical(OExpression leftOExpression, OOperator oOp, OExpression rightOExpression) {
      PExpression pLeftExpression = this.process(leftOExpression);
      PExpression pRightExpression = this.process(rightOExpression);
      switch (oOp) {
        case ADD :
          return PExpression.binary(pLeftExpression, POperator.PLUS, pRightExpression);
        case SUB :
          return PExpression.binary(pLeftExpression, POperator.MINUS, pRightExpression);
        case MULT :
          return PExpression.binary(pLeftExpression, POperator.MULT, pRightExpression);
        case DIV :
          return PExpression.binary(pLeftExpression, POperator.DIV, pRightExpression);
        case MOD :
          return PExpression.binary(pLeftExpression, POperator.MOD, pRightExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }      
    }
    
    @Override
    public PExpression onRelational(OExpression leftOExpression, OOperator oOp, OExpression rightOExpression) {
      PExpression pLeftExpression = translateExpression(leftOExpression, this.symbolTable);
      PExpression pRightExpression = translateExpression(rightOExpression, this.symbolTable);
      switch (oOp) {
        case LT:
          return PExpression.binary(pLeftExpression, POperator.LT, pRightExpression);
        case LEQ:
          return PExpression.binary(pLeftExpression, POperator.LE, pRightExpression);
        case EQ:
          return PExpression.binary(pLeftExpression, POperator.EQ, pRightExpression);
        case NEQ:
          return PExpression.binary(pLeftExpression, POperator.NEQ, pRightExpression);
        case GEQ:
          return PExpression.binary(pLeftExpression, POperator.GE, pRightExpression);
        case GT:
          return PExpression.binary(pLeftExpression, POperator.GT, pRightExpression);
        //$CASES-OMITTED$
        default:
          throw exception();
      }
    }
  }

  public static PExpression translateExpression(OExpression oExpression, ModelSymbolTable symbolTable) {
    return (new ExpressionTranslator(symbolTable.getCollaboration().getConstraintsContext(), symbolTable)).process(oExpression);
  }
}
