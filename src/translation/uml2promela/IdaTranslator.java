package translation.uml2promela;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import promela.PConstant;
import promela.PDataType;
import promela.PExpression;
import promela.POperator;
import promela.PProcess;
import promela.PStatement;
import promela.PVariable;
import uml.UBehavioural;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import uml.UType;
import uml.ida.IAction;
import uml.ida.IAutomaton;
import uml.ida.IBranch;
import uml.ida.IClock;
import uml.ida.ICondition;
import uml.ida.IEvent;
import uml.ida.IExpression;
import uml.ida.IObservation;
import uml.ida.IOperator;
import uml.ida.IState;
import uml.ida.ITransition;
import uml.ida.IVariable;

import static util.Objects.requireNonNull;


/**
 * Translates an Ida automaton to a Promela process.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de>Jochen Wuttke</A>
 * @since Jan 1, 2006
 */
public class IdaTranslator {
  private static final @NonNull String DIRECTION_VARIABLE_NAME = "direction";
  private static final @NonNull String SENDER_VARIABLE_NAME = "sender";
  private static final @NonNull String BEHAVIOURAL_VARIABLE_NAME = "behavioural";
  private static final @NonNull String ARGUMENTS_VARIABLE_NAME = "arguments";
  private static final @NonNull String RECEIVER_VARIABLE_NAME = "receiver";

  private @NonNull ModelSymbolTable symbolTable;
  private Map<@NonNull IVariable, @NonNull PVariable> observerVariables = new HashMap<>();
  private @NonNull PVariable directionVariable;
  private @NonNull PVariable senderVariable;
  private @NonNull PVariable behaviouralVariable;
  private @NonNull PVariable argumentsVariable;
  private @NonNull PVariable receiverVariable;

  @NonNullByDefault
  public static PProcess translate(ModelSymbolTable symbolTable, IAutomaton automaton) {
    return new IdaTranslator(symbolTable).create(automaton);
  }

  @NonNullByDefault
  private IdaTranslator(ModelSymbolTable symbolTable) {
    this.symbolTable = symbolTable;
    this.directionVariable = PVariable.simple(DIRECTION_VARIABLE_NAME, PDataType.bitType());
    this.senderVariable = PVariable.simple(SENDER_VARIABLE_NAME, PDataType.minimal(this.symbolTable.getObjectsNumber()));
    this.behaviouralVariable = PVariable.simple(BEHAVIOURAL_VARIABLE_NAME, PDataType.intType());
    this.argumentsVariable = PVariable.array(ARGUMENTS_VARIABLE_NAME, PDataType.intType(), symbolTable.getMaxParametersConstant());
    this.receiverVariable = PVariable.simple(RECEIVER_VARIABLE_NAME, PDataType.minimal(this.symbolTable.getObjectsNumber()));
  }
  
  @NonNullByDefault
  public PProcess create(IAutomaton automaton) {
    PStatement observerStatement = PStatement.skip();
    
    for (IVariable automatonVariable : automaton.getVariables()) {
      PVariable observerVariable = null;
      if (automatonVariable.isBitArray())
        observerVariable = PVariable.array(automatonVariable.getName(), PDataType.bitType(), PConstant.numeric(automatonVariable.getLength()));
      else // automatonVariable.isInteger()
        observerVariable = PVariable.simple(automatonVariable.getName(), PDataType.intType());
      this.observerVariables.put(automatonVariable, observerVariable);
    }

    Map<IState, PStatement> selectionStatements = new HashMap<>();

    // Iterate over all states and create the select statements.
    for (IState state : automaton.getStates())
      selectionStatements.put(state, createStateSelectionStatement(automaton, state));

    // Build up observer statement, starting with the initial state
    if (automaton.getInitialState() != null)
      observerStatement = PStatement.sequence(observerStatement, selectionStatements.get(automaton.getInitialState()));
    for (IState state : selectionStatements.keySet()) {
      if (automaton.isInitialState(state))
        continue;
      observerStatement = PStatement.sequence(observerStatement, selectionStatements.get(state));
    }
    if (automaton.getAcceptingStates() != null && !automaton.getAcceptingStates().isEmpty()) {
      // Looping for acceptance cycle, no trivial loop to avoid "unconditional self loop" error
      PStatement acceptStatement = PStatement.selection();
      PExpression acceptGuard = PExpression.eq(PExpression.constant(PConstant.numeric(0)), PExpression.constant(PConstant.numeric(0)));
      acceptStatement.addBranch(PStatement.sequence(PStatement.expression(acceptGuard), PStatement.gotoStm("acceptAll")));

      acceptStatement = PStatement.label("acceptAll", acceptStatement);
      observerStatement = PStatement.sequence(observerStatement, acceptStatement);
    }

    String observerName = symbolTable.getObserverProcessName();
    List<PVariable> observerProcessVariables = new ArrayList<>(this.observerVariables.values());
    observerProcessVariables.add(this.directionVariable);
    observerProcessVariables.add(this.senderVariable);
    observerProcessVariables.add(this.behaviouralVariable);
    observerProcessVariables.add(this.argumentsVariable);
    observerProcessVariables.add(this.receiverVariable);
    PProcess observerProcess = PProcess.proctype(observerName, new ArrayList<>(), observerProcessVariables, observerStatement);
    return observerProcess;
  }
  
  /**
   * Creates the label, select statement and required message macros for all
   * transitions leaving a given state.
   * 
   * @param state
   * @return a Promela expression
   */
  private @NonNull PStatement createStateSelectionStatement(IAutomaton automaton, IState state) {
    if (automaton == null || state == null)
      return PStatement.skip();

    // Create a selection
    PStatement selectStatement = PStatement.selection();

    List<ITransition> triggeredTransitions = new ArrayList<>();
    List<ITransition> triggerlessTransitions = new ArrayList<>();
    for (ITransition transition : automaton.getOutgoings(state)) {
      if (transition.getTrigger() != null)
        triggeredTransitions.add(transition);
      else
        triggerlessTransitions.add(transition);
    }
    
    // Add one option for every leaving triggerless transition
    for (ITransition transition : triggerlessTransitions) {
      // Create the guard expression
      PExpression guardExpression = PExpression.trueConst();
      ICondition transitionGuard = transition.getGuard();
      if (transitionGuard != null)
        guardExpression = translateExpression(transitionGuard.getCondition());
      PStatement branchSelectionStatement = PStatement.expression(guardExpression);

      // "Label" all transitions
      for (IBranch iBranch : transition.getBranches()) {
        PStatement inBranchStatement = PStatement.gotoStm(processStateLabel(automaton, iBranch.getTo()));
        IAction effect = iBranch.getLabel().getEffect();
        if (effect != null) {
          // Add the action if necessary
          PStatement actionStatement = translateAction(effect);
          inBranchStatement = PStatement.atomic(PStatement.sequence(actionStatement, inBranchStatement));
        }
        // Add the new choice to the current select expression
        selectStatement.addBranch(PStatement.sequence(branchSelectionStatement, inBranchStatement));
      }
    }

    if (!triggeredTransitions.isEmpty()) {
      PStatement triggerStatement = PStatement.receive(PExpression.channel(symbolTable.getObserverChannel()));
      triggerStatement.addRecvStmArg(PExpression.variable(this.directionVariable));
      triggerStatement.addRecvStmArg(PExpression.variable(this.senderVariable));
      triggerStatement.addRecvStmArg(PExpression.variable(this.receiverVariable));      
      triggerStatement.addRecvStmArg(PExpression.variable(this.behaviouralVariable));
      for (int i = 0; i < this.symbolTable.getMaxParameters(); i++)
        triggerStatement.addRecvStmArg(PExpression.bracket(PExpression.variable(this.argumentsVariable), PExpression.constant(PConstant.numeric(i))));
      PStatement subSelectStatement = PStatement.selection();

      // Add one option for every triggered transition
      for (ITransition transition : triggeredTransitions) {
        // Create the trigger and guard expressions
        PExpression triggerExpression = translateEvent(transition.getTrigger());
        PExpression guardExpression = PExpression.trueConst();
        ICondition transitionGuard = transition.getGuard();
        if (transitionGuard != null)
          guardExpression = translateExpression(transitionGuard.getCondition());
        PExpression branchSelectionExpression = PExpression.and(triggerExpression, guardExpression);
        PStatement branchSelectionStatement = PStatement.expression(branchSelectionExpression);

        // "Label" all transitions
        for (IBranch iBranch : transition.getBranches()) {
          PStatement inBranchStatement = PStatement.gotoStm(processStateLabel(automaton, iBranch.getTo()));
          IAction effect = iBranch.getLabel().getEffect();
          if (effect != null) {
            // Add the action if necessary
            PStatement actionStatement = translateAction(effect);
            inBranchStatement = PStatement.atomic(PStatement.sequence(actionStatement, inBranchStatement));
          }
          subSelectStatement.addBranch(PStatement.sequence(branchSelectionStatement, inBranchStatement));
        }
      }

      selectStatement.addBranch(PStatement.sequence(triggerStatement, subSelectStatement));
    }

    if (automaton.isAcceptingState(state)) {
      PExpression acceptGuard = PExpression.trueConst();
      PStatement acceptGoto = PStatement.gotoStm("acceptAll");
      selectStatement.addBranch(PStatement.sequence(PStatement.expression(acceptGuard), acceptGoto));
    }
    else {
      // Looping for rejection cycle, no trivial loop to avoid "unconditional self loop" error
      if (automaton.getOutgoings(state) == null || automaton.getOutgoings(state).isEmpty()) {
        PExpression rejectGuard = PExpression.eq(PExpression.constant(PConstant.numeric(0)), PExpression.constant(promela.PConstant.numeric(0)));
        PStatement rejectGoto = PStatement.gotoStm(processStateLabel(automaton, state));
        selectStatement.addBranch(PStatement.sequence(PStatement.expression(rejectGuard), rejectGoto));
      }
    }

    return PStatement.label(processStateLabel(automaton, state), selectStatement);
  }

  /**
   * Create a unique label for a state.
   *
   * @param automaton the automaton the {@code state} occurs in 
   * @param state the state for which the label is created
   * @return a string containing the label text
   */
  private @NonNull String processStateLabel(IAutomaton automaton, IState state) {
    String label = state.getName();
    char[] characters = label.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    label = new String(characters);

    return (automaton.isRecurrenceState(state) ? "accept_" : "") + "nc_" + label;
  }

  @NonNullByDefault
  private class EventTranslator implements IObservation.InContextVisitor<PExpression> {
    EventTranslator() {
    }
    
    PExpression expression(@Nullable IEvent iEvent) {
      if (iEvent == null)
        return PExpression.trueConst();
      return iEvent.getObservation().accept(this);
    }

    public UContext getContext() {
      return symbolTable.getCollaboration().getConstraintsContext();
    }

    public PExpression onSend() {
      return PExpression.eq(PExpression.variable(directionVariable), PExpression.constant(symbolTable.getSendingConstant()));
    }

    public PExpression onReceive() {
      return PExpression.eq(PExpression.variable(directionVariable), PExpression.constant(symbolTable.getReceivingConstant()));
    }

    public PExpression onTermination() {
      return PExpression.eq(PExpression.variable(directionVariable), PExpression.constant(symbolTable.getTerminationConstant()));
    }

    public PExpression onSender(UObject object) {
      return PExpression.eq(PExpression.variable(senderVariable), PExpression.constant(symbolTable.getConstant(object)));
    }

    public PExpression onReceiver(UObject object) {
      return PExpression.eq(PExpression.variable(receiverVariable), PExpression.constant(symbolTable.getConstant(object)));
    }

    public PExpression onBehavioural(UBehavioural behavioural) {
      // Determine who can receive this behavioural.  This is necessary,
      // as behavioural numbers may not be globally unique, but depend on
      // the receiver.  Since we currently do not support inheritance it is enough
      // to check that the receiving object is one of the objects of the type of the
      // owner of the behavioural.
      PExpression receivingExpression = PExpression.falseConst();
      for (UObject receivingObject : symbolTable.getCollaboration().getObjects()) {
        if (UType.simple(behavioural.getOwner()).subsumes(receivingObject.getType())) {
          PExpression receiverExpression = PExpression.eq(PExpression.variable(receiverVariable), PExpression.constant(symbolTable.getConstant(receivingObject)));
          receivingExpression = PExpression.or(receivingExpression, receiverExpression);
        }
      }
      PExpression behaviouralExpression = PExpression.eq(PExpression.variable(behaviouralVariable), PExpression.constant(symbolTable.getConstant(behavioural)));
      return PExpression.and(behaviouralExpression, receivingExpression);
    }

    public PExpression onArguments(List<UExpression> arguments) {
      PExpression argumentsExpression = PExpression.trueConst();
      for (int i = 0; i < arguments.size(); i++) {
        PExpression argumentExpression = PExpression.eq(PExpression.bracket(PExpression.variable(argumentsVariable), PExpression.constant(PConstant.numeric(i))), UMLTranslator.translateValue(requireNonNull(arguments.get(i)), this.getContext(), symbolTable));
        argumentsExpression = PExpression.and(argumentsExpression, argumentExpression);
      }
      return argumentsExpression;
    }

    public PExpression onExpression(IExpression expression) {
      return translateExpression(expression);
    }

    public PExpression onNot(IObservation specification) {
      PExpression innerExpression = specification.accept(this);
      return PExpression.not(innerExpression);
    }

    public PExpression onAnd(IObservation leftSpecification, IObservation rightSpecification) {
      PExpression leftExpression = leftSpecification.accept(this);
      PExpression rightExpression = rightSpecification.accept(this);
      return PExpression.and(leftExpression, rightExpression);
    }

    public PExpression onOr(IObservation leftSpecification, IObservation rightSpecification) {
      PExpression leftExpression = leftSpecification.accept(this);
      PExpression rightExpression = rightSpecification.accept(this);
      return PExpression.or(leftExpression, rightExpression);
    }
  }
  
  private @NonNull PExpression translateEvent(IEvent iEvent) {
    if (iEvent != null) {
      return new EventTranslator().expression(iEvent);
    }
    return PExpression.trueConst();
  }

  @NonNullByDefault
  private class ExpressionTranslator implements IExpression.Visitor<PExpression> {
    ExpressionTranslator() {
    }
    
    PExpression expression(IExpression iExpression) {
      return iExpression.accept(this);
    }
    
    public PExpression onBooleanConstant(boolean booleanConstant) {
      return (booleanConstant ? PExpression.trueConst() : PExpression.falseConst());
    }
  
    public PExpression onIntegerConstant(int integerConstant) {
      return PExpression.constant(PConstant.numeric(integerConstant));
    }

    public PExpression onInfinity() {
      // Translate the biggest possible integer
      return PExpression.constant(PConstant.numeric(PDataType.intType().maxValue()));
    }
  
    public PExpression onVariable(IVariable variable) {
      return PExpression.variable(requireNonNull(observerVariables.get(variable)));
    }

    public PExpression onArray(IVariable variable, int offset) {
      return PExpression.bracket(PExpression.variable(requireNonNull(observerVariables.get(variable))), PExpression.numeric(offset));
    }

    public PExpression onArithmetical(IExpression iLeftExp, IOperator iOp, IExpression iRightExp) {
      PExpression pLeftExp = this.expression(iLeftExp);
      PExpression pRightExp = this.expression(iRightExp);

      POperator pOp = null;
      switch (iOp) {
        case PLUS:
          pOp = POperator.PLUS;
          break;
        case MINUS:
          pOp = POperator.MINUS;
          break;
        //$CASES-OMITTED$
        default:
      }
      return PExpression.binary(pLeftExp, requireNonNull(pOp), pRightExp);
    }
  
    public PExpression onNegation(IExpression iExpression) {
      return PExpression.not(this.expression(iExpression));
    }

    public PExpression onRelational(IExpression iLeftExp, IOperator iOp, IExpression iRightExp) {
      PExpression pLeftExp = this.expression(iLeftExp);
      PExpression pRightExp = this.expression(iRightExp);

      POperator pOp = null;
      switch (iOp) {
        case LT:
          pOp = POperator.LT;
          break;
        case LEQ:
          pOp = POperator.LE;
          break;
        case EQ:
          pOp = POperator.EQ;
          break;
        case NEQ:
          pOp = POperator.NEQ;
          break;
        case GEQ:
          pOp = POperator.GE;
          break;
        case GT:
          pOp = POperator.GT;
          break;
        //$CASES-OMITTED$
        default:
      }
      return PExpression.binary(pLeftExp, requireNonNull(pOp), pRightExp);
    }
  
    public PExpression onJunctional(IExpression iLeftExp, IOperator iOp, IExpression iRightExp) {
      PExpression pLeftExp = this.expression(iLeftExp);
      PExpression pRightExp = this.expression(iRightExp);

      POperator pOp = null;
      switch (iOp) {
        case AND:
          pOp = POperator.CAND;
          break;
        case OR:
          pOp = POperator.COR;
          break;
        //$CASES-OMITTED$
        default:
      }
      return PExpression.binary(pLeftExp, requireNonNull(pOp), pRightExp);
    }

    public PExpression onExternal(UExpression uExpression, UContext uContext) {
      return UMLTranslator.translateValue(uExpression, uContext, symbolTable);
    }
  }

  /**
   * Translate an Ida expression into a boolean Promela expression.
   * 
   * @param iExpression an Ida expression
   * @return a Promela expression equivalent to <code>iExpression</code>, or <code>true</code> if 
   * <code>expression</code> is <code>null</code>.
   */
  private @NonNull PExpression translateExpression(IExpression iExpression) {
    if (iExpression != null)
      return new ExpressionTranslator().expression(iExpression);
    else
      return PExpression.trueConst();
  }

  @NonNullByDefault
  private class ActionTranslator implements IAction.Visitor<PStatement> {
    ActionTranslator() {
    }
    
    PStatement statement(IAction iAction) {
      return iAction.accept(this);
    }
    
    private PExpression getObserverVariable(IVariable iVariable) {
      return PExpression.variable(requireNonNull(observerVariables.get(iVariable)));
    }

    public PStatement onSkip() {
      return PStatement.skip();
    }

    public PStatement onAssign(IVariable iVariable, IExpression iExpression) {
      return PStatement.assignment(getObserverVariable(iVariable), translateExpression(iExpression));
    }

    public PStatement onReset(IVariable iVariable) {
      return PStatement.assignment(getObserverVariable(iVariable), PExpression.constant(PConstant.numeric(0)));
    }

    public PStatement onInc(IVariable iVariable) {
      return PStatement.expression(PExpression.increment(getObserverVariable(iVariable)));
    }

    public PStatement onDec(IVariable iVariable) {
      return PStatement.expression(PExpression.decrement(getObserverVariable(iVariable)));
    }

    public PStatement onSet(IVariable iVariable, int offset) {
      return PStatement.assignment(PExpression.bracket(getObserverVariable(iVariable), PExpression.numeric(offset)), PExpression.numeric(1));
    }

    public PStatement onReset(IVariable iVariable, int offset) {
      return PStatement.assignment(PExpression.bracket(getObserverVariable(iVariable), PExpression.numeric(offset)), PExpression.numeric(0));
    }

    public PStatement onReset(IClock iClock) {
      // Silently ignore clocks
      return PStatement.skip();
    }

    public PStatement onSeq(IAction leftAction, IAction rightAction) {
      return PStatement.sequence(this.statement(leftAction), this.statement(rightAction));
    }
  }

  /**
   * Translate an Ida action into a Promela statement.
   * 
   * @param iAction an Ida action
   * @return a Promela action equivalent to {@code iAction}
   */
  @NonNullByDefault
  private PStatement translateAction(IAction iAction) {
    return (new ActionTranslator().statement(iAction));
  }
}
