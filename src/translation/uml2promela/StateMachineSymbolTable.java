package translation.uml2promela;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import promela.PChannel;
import promela.PConstant;
import promela.PDataType;
import promela.PExpression;
import promela.PInline;
import promela.PKeywords;
import promela.PMacro;
import promela.PMessageType;
import promela.PProcess;
import promela.PStatement;
import promela.PVariable;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UClassifier;
import uml.UObject;
import uml.UStatual;
import uml.UType;
import uml.smile.SConstant;
import uml.smile.SMachine;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UCompletionTree;
import uml.statemachine.semantics.UCompoundTransition;
import uml.statemachine.semantics.UTimerTree;

import static util.Objects.requireNonNull;


/**
 * Context for translation of Smile
 *
 * @author Magdi Ismail (<A HREF="mailto:ismail@cip.informatik.uni-muenchen.de">ismail@cip.informatik.uni-muenchen.de</A>)
 * @author Alexander Knapp (<A HREF="mailto:knapp@pst.informatik.uni-muenchen.de">knapp@pst.informatik.uni-muenchen.de</A>)
 */
@NonNullByDefault
public class StateMachineSymbolTable extends SymbolTable {
  private static final String THIS_VARIABLE_NAME = "this";
  private static final String EVENT_QUEUE_NAME = "event_queue";
  private static final String INTERNAL_QUEUE_NAME = "internal_queue";
  private static final String DEFERRED_QUEUE_NAME = "deferred_queue";
  private static final String DEFERRED_COUNTER_VARIABLE_NAME = "deferred_counter";
  private static final String ACKNOWLEDGE_IN_CHANNEL_NAME = "ack_in";
  private static final String ACKNOWLEDGE_OUT_CHANNEL_NAME = "ack_out";
  private static final String CURRENT_EVENT_VARIABLE_NAME = "current_event";
  private static final String SENDER_VARIABLE_NAME = "sender";
  private static final String PARAMETERS_VARIABLE_NAME = "parameters";
  private static final String END_LABEL_NAME = "end_machine";
  private static final int EMPTY_CONSTANT_VALUE = 0;

  private ModelSymbolTable symbolTable;
  private UClass uClass;
  private UStateMachine uStateMachine;
  private UCompletionTree uCompletionTree;
  private UTimerTree uTimerTree;
  
  // Generic translation results
  private Map<Object, PConstant> constants = new HashMap<>();
  private Map<Object, PVariable> variables = new HashMap<>();
  private Map<PChannel, PChannel> channels = new HashMap<>();
  private Map<UAttribute, PExpression> expressions = new HashMap<>();
  private Map<ParameterKey, PVariable> parameters = new HashMap<>();
  private List<PVariable> variableParameters = new ArrayList<>();

  private @Nullable PVariable thisVariable = null;
  private @Nullable PChannel eventQueueChannel = null;
  private @Nullable PChannel internalQueueChannel = null;
  private @Nullable PChannel deferredQueueChannel = null;
  private @Nullable PVariable deferredCounterVariable = null;
  private @Nullable PChannel acknowledgeInChannel = null;
  private @Nullable PChannel acknowledgeOutChannel = null;
  private @Nullable PVariable currentEventVariable = null;
  private @Nullable PVariable senderVariable = null;
  private @Nullable PVariable parametersVariable = null;
  private @Nullable PVariable completedVariable = null;
  private Map<UState, PConstant> completionConstants = new HashMap<>();
  private @Nullable PVariable timedOutVariable = null;
  private Map<UEvent, PConstant> timerConstants = new HashMap<>();
  private @Nullable PStatement endStatement = null;

  private int stateConstantsCounter = EMPTY_CONSTANT_VALUE+1;
  private int transitionConstantsCounter = EMPTY_CONSTANT_VALUE+1;
  private int identifiersCounter = 0;
  private Set<String> identifiers = new HashSet<>();

  /**
   * Create a state machine symbol table for a (UML) class.
   *
   * @return a state machine symbol table
   */
  StateMachineSymbolTable(UClass uClass, ModelSymbolTable symbolTable) {
    super(uClass.getModel());
    this.symbolTable = symbolTable;
    this.uClass = uClass;
    this.uStateMachine = requireNonNull(uClass.getStateMachine());
    this.uCompletionTree = UCompletionTree.create(this.uStateMachine);
    this.uTimerTree = UTimerTree.create(this.uStateMachine);
    this.identifiers.addAll(PKeywords.getKeywords());
  }

  @Override
  public control.Properties getProperties() {
    return this.symbolTable.getProperties();
  }

  /**
   * @return whether state machine symbol table's underlying model has an observer
   * @see translation.uml2promela.SymbolTable#isObserved()
   */
  public boolean isObserved() {
    return this.symbolTable.isObserved();
  }

  /**
   * @return whether state machine symbol table's underlying underlying model shows any operations
   */
  public boolean hasOperations() {
    return this.symbolTable.hasOperations();
  }

  /**
   * @return maximum number used for any event accepted by the underlying state machine
   */
  int getEventsMax() {
    return this.symbolTable.getEventsMax(this.uStateMachine);
  }

  /**
   * Determine a Promela data type for a UML classifier.
   *
   * @param classifier a UML classifier
   * @return Promela data type
   */
  PDataType getType(UClassifier classifier) {
    UType type = UType.simple(classifier);
    if (type.isClass())
      return PDataType.minimal(this.symbolTable.getObjectsNumber());
    if (type.isDataType()) {
      if (type.equals(this.getModel().getIntegerType()))
        return PDataType.intType();
      if (type.equals(this.getModel().getBooleanType()))
        return PDataType.boolType();
    }
    return PDataType.intType();
  }

  /**
   * @return Promela constant sent by acknowledge channel
   */
  PConstant getAckOutSendArgConstant() {
    return this.symbolTable.getAckOutSendArgConstant();
  }

  /**
   * @return Promela channel for global event queues
   */
  PChannel getGlobalEventQueuesChannel() {
    return this.symbolTable.getGlobalEventQueuesChannel();
  }

  /**
   * @return Promela channel for observer
   */
  PChannel getObserverChannel() {
    return this.symbolTable.getObserverChannel();
  }

  /**
   * @return Promela constant which is is used for indicating a termination action
   */
  PConstant getTerminationConstant() {
    return this.symbolTable.getTerminationConstant();
  }

  /**
   * @return Promela constant which is is used for indicating a sending action
   */
  PConstant getSendingConstant() {
    return this.symbolTable.getSendingConstant();
  }

  /**
   * @return Promela constant which is is used for indicating a receiving action
   */
  PConstant getReceivingConstant() {
    return this.symbolTable.getReceivingConstant();
  }

  /**
   * @return Promela constant which is is used for <CODE>null</CODE>
   * or, more generally, an "empty" Promela variable
   */
  PConstant getEmptyConstant() {
    return this.symbolTable.getEmptyConstant();
  }

  /**
   * Retrieve Promela constant for UML behavioural (Promela constant for UML event)
   *
   * @param uBehavioural UML behavioural
   * @return Promela constant for UML behavioural (Promela constant for UML event)
   */
  PConstant getConstant(UBehavioural uBehavioural) {
    return this.symbolTable.getConstant(uBehavioural);
  }

  /**
   * @return Promela channel for event queue
   */
  PChannel getEventQueueChannel() {
    if (this.eventQueueChannel != null)
      return this.eventQueueChannel;

    this.eventQueueChannel = PChannel.simple(EVENT_QUEUE_NAME);
    //-AK don't add to channels as it will be a parameter
    return this.eventQueueChannel;
  }

  /**
   * @return Promela channel for internal event queue
   */
  PChannel getInternalQueueChannel() {
    if (this.internalQueueChannel != null)
      return this.internalQueueChannel;

    var bufferSize = this.symbolTable.getInternalQueueSizeConstant();
    var messageType = PMessageType.create();
    messageType.addPartDataType(PDataType.minimal(this.symbolTable.getEventsMax(this.uStateMachine)));

    var internalQueueChannel = PChannel.simple(INTERNAL_QUEUE_NAME);
    internalQueueChannel.initialise(bufferSize, messageType);
    this.addChannel(internalQueueChannel);
    return this.internalQueueChannel = internalQueueChannel;
  }

  /**
   * @return Promela channel for deferred event queue
   */
  PChannel getDeferredQueueChannel() {
    if (this.deferredQueueChannel != null)
      return this.deferredQueueChannel;

    var deferredQueueChannel = PChannel.simple(DEFERRED_QUEUE_NAME);
    deferredQueueChannel.initialise(this.symbolTable.getDeferredQueueSizeConstant(), this.symbolTable.getExternalEventsMessageType());
    this.addChannel(deferredQueueChannel);
    return this.deferredQueueChannel = deferredQueueChannel;
  }

  /**
   * @return Promela variable for counting of deferred events
   */
  PVariable getDeferredCounterVariable() {
    if (this.deferredCounterVariable != null)
      return this.deferredCounterVariable;
    
    var deferredCounterVariable = PVariable.simple(DEFERRED_COUNTER_VARIABLE_NAME, PDataType.minimal(this.getProperties().getDeferredQueueCapacity()));
    deferredCounterVariable.initialise(PExpression.constant(PConstant.numeric(0)));
    this.addVariable(deferredCounterVariable);
    return this.deferredCounterVariable = deferredCounterVariable;
  }

  /**
   * @return Promela channel for receiving an acknowledgement of call events
   */
  PChannel getAcknowledgeInChannel() {
    if (this.acknowledgeInChannel != null)
      return this.acknowledgeInChannel;

    var messageType = PMessageType.create();
    messageType.addPartDataType(PDataType.bitType());
    var acknowledgeInChannel = PChannel.simple(ACKNOWLEDGE_IN_CHANNEL_NAME);
    acknowledgeInChannel.initialise(PConstant.numeric(0), messageType);
    this.addChannel(acknowledgeInChannel);
    return this.acknowledgeInChannel = acknowledgeInChannel;
  }

  /**
   * @return Promela channel for sending an acknowledgement of call events
   */
  PChannel getAcknowledgeOutChannel() {
    if (this.acknowledgeOutChannel != null)
      return this.acknowledgeOutChannel;

    var messageType = PMessageType.create();
    messageType.addPartDataType(PDataType.bitType());
    var acknowledgeOutChannel = PChannel.simple(ACKNOWLEDGE_OUT_CHANNEL_NAME);
    this.addChannel(acknowledgeOutChannel);
    return this.acknowledgeOutChannel = acknowledgeOutChannel;
  }

  /**
   * @return Promela variable for parametrisation of UML constant expression 'this'
   */
  PVariable getThisVariable() {
    if (this.thisVariable != null)
      return this.thisVariable;

    var thisVariable = PVariable.simple(THIS_VARIABLE_NAME, this.getType(this.uClass));
    //-AK don't add to variables as it will be a parameter
    return this.thisVariable = thisVariable;
  }

  /**
   * @return Promela expression for <CODE>this</CODE>
   */
  PExpression getThisExpression() {
    return PExpression.variable(getThisVariable());
  }

  /**
   * Determine a Promela expression for accessing a UML statual.
   *
   * @param uStatual a UML statual
   * @return Promela expression for accessing UML statual
   */
  @NonNull PExpression access(UStatual uStatual) {
    return uStatual.cases(uAttribute -> uAttribute.isConstant() ? this.getExpression(uAttribute) : PExpression.variable(this.getVariable(uAttribute)),
                          uParameter -> PExpression.bracket(PExpression.variable(this.getParametersVariable()),
                                                            PExpression.constant(PConstant.numeric(uParameter.getNumber()))),
                          uObject -> PExpression.constant(this.getEmptyConstant()));
  }

  /**
   * Determine a Promela expression for accessing a UML statual with an offset.
   *
   * @param uStatual a UML statual
   * @param pOffset a UPPAAL integer expression
   * @return Promela expression for accessing UML statual with offset
   */
  @NonNull PExpression access(UStatual uStatual, PExpression pOffset) {
    return uStatual.cases(uAttribute -> PExpression.bracket(PExpression.variable(this.getVariable(uAttribute)), pOffset),
                          uParameter -> PExpression.constant(this.getEmptyConstant()),
                          uObject -> PExpression.constant(this.getEmptyConstant()));
  }

  /**
   * Determine Promela expression for accessing the external event queue of a target expression denoting a UML object.
   * 
   * @param target a Promela expression denoting a UML object
   * @return Promela expression for accessing the external event queue of <CODE>target</CODE>
   */
  PExpression accessExternalEventQueue(PExpression target) {
    return this.symbolTable.accessExternalEventQueue(target);
  }

  /**
   * @return Promela variable for current event
   */
  PVariable getStimulusVariable() {
    if (this.currentEventVariable != null)
      return this.currentEventVariable;

    this.currentEventVariable = PVariable.simple(CURRENT_EVENT_VARIABLE_NAME, PDataType.minimal(this.getEventsMax()));
    this.addVariable(this.currentEventVariable);
    return requireNonNull(this.currentEventVariable);
  }

  /**
   * @return Promela variable for sender
   */
  PVariable getSenderVariable() {
    if (this.senderVariable != null)
      return this.senderVariable;

    this.senderVariable = PVariable.simple(SENDER_VARIABLE_NAME, PDataType.minimal(this.symbolTable.getObjectsNumber()));
    this.addVariable(this.senderVariable);
    return requireNonNull(this.senderVariable);
  }

  /**
   * @return Promela variable for event parameters
   */
  PVariable getParametersVariable() {
    if (this.parametersVariable != null)
      return this.parametersVariable;

    this.parametersVariable = PVariable.array(PARAMETERS_VARIABLE_NAME, PDataType.intType(), symbolTable.getMaxParametersConstant());
    this.addVariable(this.parametersVariable);
    return requireNonNull(this.parametersVariable);
  }

  /**
   * @return Promela statement for successful process termination
   */
  PStatement getEndStatement() {
    if (this.endStatement != null)
      return this.endStatement;

    this.endStatement = PStatement.label(END_LABEL_NAME, PStatement.expression(PExpression.macro(symbolTable.getSuccessMacro())));

    if (this.isObserved()) {
      PStatement notificationStatement = PStatement.send(PExpression.channel(this.getObserverChannel()));
      notificationStatement.addSendStmArg(PExpression.constant(this.getTerminationConstant()));
      notificationStatement.addSendStmArg(PExpression.variable(this.getThisVariable()));
      notificationStatement.addSendStmArg(PExpression.constant(this.getEmptyConstant()));
      notificationStatement.addSendStmArg(PExpression.constant(this.getEmptyConstant()));
      for (int i = 0; i < this.getMaxParameters(); i++) {
        notificationStatement.addSendStmArg(PExpression.constant(PConstant.numeric(0)));
      }
      // this.endStatement = PStatement.label(END_LABEL_NAME + "0", PStatement.sequence(notificationStatement, this.endStatement));
      this.endStatement = PStatement.sequence(notificationStatement, this.endStatement);
    }

    if (this.getProperties().isMutex()) {
      // this.endStatement = PStatement.label(END_LABEL_NAME + "1", PStatement.sequence(this.exitMutex(), this.endStatement));
      this.endStatement = PStatement.sequence(this.exitMutex(), this.endStatement);
    }
    return requireNonNull(this.endStatement);
  }
  
  /**
   * @return Promela statement for entering mutex
   */
  PStatement enterMutex() {
    return symbolTable.enterMutex();
  }

  /**
   * @return Promela statement for exiting mutex
   */
  PStatement exitMutex() {
    return symbolTable.exitMutex();
  }

  /**
   * @return Promela statement for yielding mutex
   */
  PStatement yieldMutex() {
    return PStatement.sequence(exitMutex(), enterMutex());    
  }

  /**
   * @param pStatement a Promela statement
   * @return Promela statement for yielding mutex for a given Promela statement
   */
  PStatement yieldMutex(PStatement pStatement) {
    return PStatement.sequence(exitMutex(), PStatement.sequence(pStatement, enterMutex()));
  }

  /**
   * Retrieve Promela statement for waiting until a queue is not full.
   *
   * @param queueExpression Promela expression determining a queue
   * @return
   */
  PStatement checkFull(PExpression queueExpression) {
    if (!this.getProperties().isMutex())
      return PStatement.skip();

    PStatement checkFull = PStatement.repetition();
    PExpression full = PExpression.full(queueExpression);
    checkFull.addBranch(PStatement.sequence(PStatement.expression(full), this.yieldMutex()));
    PExpression nfull = PExpression.nfull(queueExpression);
    checkFull.addBranch(PStatement.sequence(PStatement.expression(nfull), PStatement.breakStm()));
    return checkFull;
  }

  /**
   * Retrieve Promela constant for UML event.
   *
   * @param uEvent UML event
   * @return Promela constant for UML event
   */
  PConstant getConstant(UEvent uEvent) {
    return this.symbolTable.getConstant(this.uClass.getStateMachine(), uEvent);
  }

  /**
   * Determine which UML event is represented by an integer.
   *
   * @param n an integer
   * @return UML event represented by integer {@link n}; or <CODE>null</CODE> if no such event can be found
   */
  @Nullable UEvent getEvent(int n) {
    return this.symbolTable.getEvent(this.uClass.getStateMachine(), n);
  }

  /**
   * Retrieve Promela constant for Smile constant.
   *
   * @param sConstant Smile constant
   * @return Promela constant for Smile constant
   */
  PConstant getConstant(SConstant sConstant) {
    return sConstant.new Cases<PConstant>().
      empty(() -> this.getEmptyConstant()).
      vertex(uVertex -> getConstant(uVertex)).
      transition(uTransition -> getConstant(uTransition)).
      event(uEvent -> getConstant(uEvent)).
      apply();
  }

  /**
   * Retrieve Promela constant representing a UML state.
   * 
   * The values of the constants representing UML states are all distinct
   * and thus globally unique.
   * 
   * @return Promela constant for UML state
   */
  private PConstant getConstant(UVertex uVertex) {
    PConstant pConstant = this.constants.get(uVertex);
    if (pConstant != null)
      return pConstant;

    String constantName = this.symbolTable.uniqueIdentifier(uVertex.getName());
    PConstant con = PConstant.numeric(this.stateConstantsCounter++);
    PMacro macro = PMacro.constant(constantName, con);
    this.symbolTable.addMacro(macro);
    pConstant = PConstant.macro(macro);
    this.constants.put(uVertex, pConstant);

    return pConstant;
  }
  
  /**
   * Determine which UML vertex is represented by an integer value.
   * 
   * @param n an integer value
   * @return UML vertex represented by {@code n}; or {@code null} if no such vertex exists
   */
  @Nullable UVertex getVertex(int n) {
    for (Object element : this.constants.keySet()) {
      if (element instanceof UState) {
        PConstant pConstant = this.constants.get(element);
        if (pConstant != null && n == pConstant.getValue())
          return (UState)element;
      }
    }
    return null;
  }

  /**
   * Retrieve Promela constant representing a UML compound transition.
   * 
   * @return Promela constant for UML compound transition
   */
  private PConstant getConstant(UCompoundTransition uTransition) {
    PConstant pConstant = this.constants.get(uTransition);
    if (pConstant != null)
      return pConstant;

    String constantName = this.symbolTable.uniqueIdentifier(uTransition.getName());
    PConstant con = PConstant.numeric(this.transitionConstantsCounter++);
    PMacro macro = PMacro.constant(constantName, con);
    this.symbolTable.addMacro(macro);
    pConstant = PConstant.macro(macro);
    this.constants.put(uTransition, pConstant);

    return pConstant;
  }
  
  /**
   * Get maximal number of UML parameters appearing in UML model
   *
   * @return maximal number of UML parameters appearing in UML model
   */
  int getMaxParameters() {
    return symbolTable.getMaxParameters();
  }

  /**
   * Retrieve Promela variable for non-final UML attribute
   *
   * @param uAttribute UML attribute
   * @return Promela variable for non-final UML attribute
   */
  PVariable getVariable(UAttribute uAttribute) {
    if (uAttribute.isStatic())
      return this.symbolTable.getVariable(uAttribute);

    var pVariable = this.variables.get(uAttribute);
    if (pVariable != null)
      return pVariable;

    var pDataType = this.getType(uAttribute.getClassifier());
    var variableName = this.uniqueIdentifier(uAttribute.getName());

    // Create variable
    var uType = uAttribute.getType();
    if (uType.isSimple()) {
      pVariable = PVariable.simple(variableName, pDataType);
    }
    else
      if (uType.isArray()) {
        PConstant arraySize = PConstant.numeric(uType.getSize());
        pVariable = PVariable.array(variableName, pDataType, arraySize);
      }
    assert pVariable != null;
    this.variables.put(uAttribute, pVariable);
    return pVariable;
  }

  static final class ParameterKey {
    UAttribute uAttribute;
    int offset = 0;

    ParameterKey(UAttribute uAttribute, int offset) {
      this.uAttribute = uAttribute;
      this.offset = offset;
    }

    public int hashCode() {
      return Objects.hash(this.uAttribute, this.offset);
    }

    public boolean equals(java.lang.@Nullable Object object) {
      if (object == null)
        return false;
      if (object instanceof ParameterKey other) {
        return Objects.equals(this.uAttribute, other.uAttribute) &&
               this.offset == other.offset;
      }
      return false;
    }
  }

  /**
   * Retrieve Promela variable for parametrisation of non-static and
   * non-final UML attribute's initial value
   *
   * @param uAttribute UML attribute
   * @param offset offset of array attributes
   * @return Promela variable for parameterisation
   * @pre !uAttribute.isStatic() && !uAttribute.isFinal()
   */
  PVariable getInitialisationVariable(UAttribute uAttribute, int offset) {
    ParameterKey parameterKey = new ParameterKey(uAttribute, offset);
    PVariable pVariable = parameters.get(parameterKey);
    if (pVariable != null)
      return pVariable;

    PDataType pDataType = this.getType(uAttribute.getClassifier());
    String variableName = this.uniqueIdentifier(this.symbolTable.getInitialiserLabel()+uAttribute.getName()+offset);
    pVariable = PVariable.simple(variableName, pDataType);
    this.variableParameters.add(pVariable);
    this.parameters.put(parameterKey, pVariable);
    return pVariable;
  }

  /**
   * Retrieve Promela variable for Smile variable
   *
   * @param sVariable Smile variable
   * @return Promela variable
   */
  PVariable getVariable(SVariable sVariable) {
    var pVariable = this.variables.get(sVariable);
    if (pVariable != null)
      return pVariable;

    int bound = 1;
    if (sVariable.isFlag())
      bound = 1;
    else {
      var machine = this.uStateMachine.getMachine(this.getProperties());
      var machineConstants = machine.getConstants();
      bound = 0;
      if (sVariable.isState()) {
        for (var machineConstant : machineConstants) {
          if (machineConstant.getKind() == SConstant.Kind.VERTEX)
            bound++;
        }
      }
      else {
        if (sVariable.isTransition()) {
          for (var machineConstant : machineConstants) {
            if (machineConstant.getKind() == SConstant.Kind.TRANSITION)
              bound++;
          }
        }
      }
    }
    var pDataType = PDataType.minimal(bound);
    pVariable = PVariable.simple(this.uniqueIdentifier(sVariable.getName()), pDataType);
    this.variables.put(sVariable, pVariable);

    return pVariable;
  }

  /**
   * Retrieve symbol table's Promela variables for Smile state variables.
   * 
   * @return the list of Promela variables for Smile state variables used in this symbol table
   */
  List<PVariable> getStateVariables() {
    List<PVariable> stateVariables = new ArrayList<>();
    for (Object element : this.variables.keySet()) {
      if (element instanceof SVariable) {
        SVariable variable = (SVariable)element;
        if (variable.isState())
          stateVariables.add(requireNonNull(variables.get(variable)));
      }
    }
    return stateVariables;
  }

  /**
   * Retrieve Promela expression for UML expression of final UML attribute
   *
   * @param uAttribute UML attribute
   * @return Promela expression for UML expression of final UML attribute
   */
  PExpression getExpression(UAttribute uAttribute) {
    PExpression pExpression = null;

    if (uAttribute.isStatic()) {
      pExpression = symbolTable.getExpression(uAttribute);
    }
    else {
      pExpression = this.expressions.get(uAttribute);

      if (pExpression == null) {
        PExpression exp = null;
        String expressionName = null;
        PMacro macro = null;

        exp = UMLTranslator.translateValue(requireNonNull(uAttribute.getInitialValue(0)), uAttribute.getInitialValuesContext(), this.symbolTable);
        expressionName = this.symbolTable.uniqueIdentifier(uAttribute.getName());
        macro = PMacro.expression(expressionName, exp);
        this.symbolTable.addMacro(macro);

        pExpression = PExpression.macro(macro);

        this.expressions.put(uAttribute, pExpression);
      }
    }

    return pExpression;
  }

  /**
   * Retrieve (Promela) variable representing a (UML) state.
   * 
   * @param uVertex a (UML) vertex
   * @return (Promela) variable representing state
   */
  public PVariable getVertexVariable(UVertex uVertex) {
    return this.getVariable(this.uStateMachine.getMachine(this.getProperties()).getStateVariable(uVertex));
  }

  /**
   * Retrieve (Promela array) variable representing completions.
   * 
   * @return (Promela) variable representing completions
   */
  public PVariable getCompletedVariable() {
    if (this.completedVariable != null)
      return this.completedVariable;

    this.completedVariable = PVariable.array(this.uniqueIdentifier("completed"), PDataType.boolType(), PConstant.numeric(this.uCompletionTree.getMaxCompletionsNumber()));
    this.variables.put(this.completedVariable, this.completedVariable);
    return requireNonNull(this.completedVariable);
  }

  /**
   * Retrieve (Promela) constant representing a completion state.
   * 
   * @param uCompletionState a (UML) state
   * @return (Promela) constant representing <CODE>uCompletionState</CODE>
   */
  public PConstant getCompletionConstant(UState uCompletionState) {
    PConstant completionConstant = this.completionConstants.get(uCompletionState);
    if (completionConstant != null)
      return completionConstant;

    completionConstant = PConstant.numeric(this.uCompletionTree.getCompletionNumber(uCompletionState));
    var completionConstantName = new StringBuilder();
    completionConstantName.append("cmpl");
    completionConstantName.append(uCompletionState.getName().substring(0, 1).toUpperCase());
    completionConstantName.append(uCompletionState.getName().substring(1));
    PMacro completionMacro = PMacro.constant(this.symbolTable.uniqueIdentifier(completionConstantName.toString()), completionConstant);
    completionConstant = PConstant.macro(completionMacro);
    this.completionConstants.put(uCompletionState, completionConstant);
    this.symbolTable.addMacro(completionMacro);
    return completionConstant;
  }

  /**
   * Retrieve (Promela array) variable for timed-out time events.
   * 
   * @return (Promela array) variable
   */
  PVariable getTimedOutVariable() {
    if (this.timedOutVariable != null)
      return this.timedOutVariable;

    this.timedOutVariable = PVariable.array(this.uniqueIdentifier("timedOut"), PDataType.boolType(), PConstant.numeric(this.uTimerTree.getMaxTimersNumber()));
    this.variables.put(this.timedOutVariable, this.timedOutVariable);
    return requireNonNull(this.timedOutVariable);
  }

  /**
   * Retrieve (Promela) constant representing a completion state.
   * 
   * @param uCompletionState a (UML) state
   * @return (Promela) constant representing <CODE>uCompletionState</CODE>
   */
  public PConstant getTimerConstant(UEvent uTimeEvent) {
    PConstant timerConstant = this.timerConstants.get(uTimeEvent);
    if (timerConstant != null)
      return timerConstant;

    timerConstant = PConstant.numeric(this.uTimerTree.getTimerNumber(uTimeEvent));
    var timerConstantName = new StringBuilder();
    timerConstantName.append("tmr");
    timerConstantName.append(uTimeEvent.getName().substring(0, 1).toUpperCase());
    timerConstantName.append(uTimeEvent.getName().substring(1));
    PMacro timerMacro = PMacro.constant(this.symbolTable.uniqueIdentifier(timerConstantName.toString()), timerConstant);
    timerConstant = PConstant.macro(timerMacro);
    this.timerConstants.put(uTimeEvent, timerConstant);
    this.symbolTable.addMacro(timerMacro);
    return timerConstant;
  }

  /**
   * Retrieve Promela expression for accessing the process identifier of a UML object.
   * 
   * @param uObject a UML object
   * @return Promela expression for accessing the process identifier of <CODE>uObject</CODE>
   */
  PExpression getProcessIdentifier(UObject uObject) {
    return this.symbolTable.getProcessIdentifier(uObject);
  }

  /**
   * Retrieve Promela process for symbol table's UML class
   *
   * @return Promela process for symbol table's UML class
   */
  PProcess getProcess() {
    return PProcess.proctype(this.getProcessName());
  }

  /**
   * Retrieve name of Promela process for symbol table's UML class
   *
   * @return name of Promela process for symbol table's UML class
   */
  String getProcessName() {
    return this.symbolTable.getProcessName(this.uClass);
  }

  /**
   * @return symbol table's class' list of attributes (determines order of Promela process' parameter list)
   */
  List<UAttribute> getOrderedAttributes() {
    return this.uClass.getAttributes();
  }

  /**
   * @return whether symbol table's class' state machine shows deferrable events
   */
  boolean hasDeferrableEvents() {
    return this.uStateMachine.getDeferrableEvents().size() > 0;
  }

  /**
   * @return symbol table's UML class
   */
  SMachine getMachine() {
    return this.uStateMachine.getMachine(this.getProperties());
  }

  /**
   * @return symbol table's completion tree
   */
  UCompletionTree getCompletionTree() {
    return this.uCompletionTree;
  }

  /**
   * @return symbol table's timer tree
   */
  UTimerTree getTimerTree() {
    return this.uTimerTree;
  }

  /**
   * @return ordered Promela variables declared in Promela process' parameter list
   */
  List<PVariable> getVariableParameters() {
    return this.variableParameters;
  }

  /**
   * Add Promela channel
   *
   * @param channel Promela channel
   */
  void addChannel(PChannel channel) {
    this.channels.put(channel, channel);
  }

  /**
   * Create and add Promela inline for a statement.
   *
   * @param name inline's name
   * @param pParameters inline's parameters
   * @param pStatement inline's statement
   */
  public PInline addInline(String name, List<PVariable> pParameters, PStatement pStatement) {
    return this.symbolTable.addInline(this.getProcessName() + "_" + name, pParameters, pStatement);
  }

  /**
   * Add Promela variable.
   *
   * @param variable Promela variable
   */
  void addVariable(PVariable variable) {
    this.variables.put(variable, variable);
  }

  /**
   * @return collected Promela channels
   */
  Collection<PChannel> getChannels() {
    return this.channels.values();
  }

  /**
   * @return collected Promela variables
   */
  Collection<PVariable> getVariables() {
    return this.variables.values();
  }

  /**
   * @return names that are used locally
   */
  Collection<String> getLocalNames() {
    return this.identifiers;
  }

  /**
   * Generate unique identifier.
   * 
   * This has to take into account that in Promela macros break the block structure,
   * i.e. macro names are not shadowed in proctypes.
   */
  String uniqueIdentifier(String identifier) {
    var characters = identifier.toCharArray();
    for (var i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    var allIdentifiers = new HashSet<String>();
    allIdentifiers.addAll(this.identifiers);
    allIdentifiers.addAll(this.symbolTable.getGlobalNames());
    var uniqueIdentifier = new String(characters).substring(0, Math.min(50, characters.length));
    if (uniqueIdentifier.equals("") || allIdentifiers.contains(uniqueIdentifier)) {
      var tmpIdentifier = "";
      do {
        tmpIdentifier = uniqueIdentifier + "_L" + this.identifiersCounter++;
      } while (allIdentifiers.contains(tmpIdentifier));
      uniqueIdentifier = tmpIdentifier;
    }
    this.identifiers.add(uniqueIdentifier);
    return uniqueIdentifier;
  }
}
