package translation.uml2promela;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import promela.PChannel;
import promela.PConstant;
import promela.PDataType;
import promela.PExpression;
import promela.PMessageType;
import promela.POperator;
import promela.PProcess;
import promela.PStatement;
import promela.PVariable;
import uml.UBehavioural;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import uml.UOperation;
import uml.testcase.UObservable;
import uml.testcase.UTestCase;


/**
 * Translates a test case to a Promela process.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de>Alexander Knapp</A>
 * @since 2015-01-10
 */
@NonNullByDefault
public class TestTranslator {
  private static final String DIRECTION_VARIABLE_NAME = "direction";
  private static final String SENDER_VARIABLE_NAME = "sender";
  private static final String BEHAVIOURAL_VARIABLE_NAME = "behavioural";
  private static final String ARGUMENTS_VARIABLE_NAME = "arguments";
  private static final String RECEIVER_VARIABLE_NAME = "receiver";
  private static final String ACKNOWLEDGE_IN_CHANNEL_NAME = "ack_in";

  private static final String ACCEPT_LABEL = "acceptAll";
  private static final String PROGRESS_LABEL = "progressEnd";
  private static final String FAILED_LABEL = "failEnd";

  private ModelSymbolTable symbolTable;
  private PVariable directionVariable;
  private PVariable senderVariable;
  private PVariable behaviouralVariable;
  private PVariable argumentsVariable;
  private PVariable receiverVariable;
  private @Nullable PChannel acknowledgeInChannel;

  private PStatement pFailedStm;
  private PStatement pAcceptStm;

  public static PProcess translate(ModelSymbolTable symbolTable, UTestCase uTestCase) {
    return new TestTranslator(symbolTable).create(uTestCase);
  }

  private TestTranslator(ModelSymbolTable symbolTable) {
    this.symbolTable = symbolTable;
    this.directionVariable = PVariable.simple(DIRECTION_VARIABLE_NAME, PDataType.bitType());
    this.senderVariable = PVariable.simple(SENDER_VARIABLE_NAME, PDataType.minimal(this.symbolTable.getObjectsNumber()));
    this.behaviouralVariable = PVariable.simple(BEHAVIOURAL_VARIABLE_NAME, PDataType.intType());
    this.argumentsVariable = PVariable.array(ARGUMENTS_VARIABLE_NAME, PDataType.intType(), symbolTable.getMaxParametersConstant());
    this.receiverVariable = PVariable.simple(RECEIVER_VARIABLE_NAME, PDataType.minimal(this.symbolTable.getObjectsNumber()));

    // Acceptance state, no trivial acceptance loop to avoid "unconditional self loop" error
    PExpression pAcceptGuard = PExpression.eq(PExpression.constant(PConstant.numeric(0)), PExpression.constant(PConstant.numeric(0)));
    pAcceptStm = PStatement.selection().addBranch(PStatement.sequence(PStatement.expression(pAcceptGuard), PStatement.label(PROGRESS_LABEL, PStatement.gotoStm(ACCEPT_LABEL))));
    pAcceptStm = PStatement.label(ACCEPT_LABEL, pAcceptStm);

    // Failing state
    pFailedStm = PStatement.assertion(PExpression.falseConst());
    pFailedStm = PStatement.label(FAILED_LABEL, pFailedStm);
  }
  
  public PProcess create(UTestCase uTestCase) {
    // Testing sequence
    List<UObservable> sequence = uTestCase.getTestSequence();
    Set<UObservable> observables = uTestCase.getObservables();
    PStatement testingSequenceStm = PStatement.skip();
    for (int i = 0; i < sequence.size(); i++) {
      @NonNull UObservable current = sequence.get(i);
      String currentLabel = "nc_" + i;
      String nextLabel = (i+1 < sequence.size() ? "nc_" + (i+1) : ACCEPT_LABEL);
      testingSequenceStm = PStatement.sequence(testingSequenceStm,
                                               PStatement.label(currentLabel, translateOccurrenceSpecification(current, currentLabel, observables, nextLabel)));
    }

    String testerName = symbolTable.getTesterProcessName();
    List<PVariable> testerProcessVariables = new ArrayList<>();
    testerProcessVariables.add(this.directionVariable);
    testerProcessVariables.add(this.senderVariable);
    testerProcessVariables.add(this.behaviouralVariable);
    testerProcessVariables.add(this.argumentsVariable);
    testerProcessVariables.add(this.receiverVariable);
    List<PChannel> testerChannels = new ArrayList<>();
    if (this.acknowledgeInChannel != null)
      testerChannels.add(this.acknowledgeInChannel);
    PStatement testerStm = PStatement.sequence(testingSequenceStm,
                           PStatement.sequence(pAcceptStm,
                                               pFailedStm));
    PProcess testerProcess = PProcess.proctype(testerName, testerChannels, testerProcessVariables, testerStm);
    return testerProcess;
  }
  
  /**
   * @return Promela channel for receiving an acknowledgement of call events
   */
  PChannel getAcknowledgeInChannel() {
    PChannel acknowledgeInChannel = this.acknowledgeInChannel;
    if (acknowledgeInChannel != null)
      return acknowledgeInChannel;

    PConstant bufferSize = PConstant.numeric(0);
    PMessageType messageType = PMessageType.create().addPartDataType(PDataType.bitType());
    acknowledgeInChannel = PChannel.simple(ACKNOWLEDGE_IN_CHANNEL_NAME).initialise(bufferSize, messageType);
    this.acknowledgeInChannel = acknowledgeInChannel;
    return acknowledgeInChannel;
  }

  /** 
   * @return Promela expression for checking for sending
   */
  public PExpression getSend() {
    return PExpression.eq(PExpression.variable(directionVariable), PExpression.constant(symbolTable.getSendingConstant()));
  }

  /** 
   * @return Promela expression for checking for receiving
   */
  public PExpression getReceive() {
    return PExpression.eq(PExpression.variable(directionVariable), PExpression.constant(symbolTable.getReceivingConstant()));
  }

  /** 
   * @return Promela expression for checking for sender
   */
  public PExpression getSender(@Nullable UObject uObject) {
    if (uObject == null)
      return PExpression.eq(PExpression.variable(senderVariable), PExpression.constant(symbolTable.getEmptyConstant()));
    else
      return PExpression.eq(PExpression.variable(senderVariable), PExpression.constant(symbolTable.getConstant(uObject)));
  }

  /** 
   * @return Promela expression for checking for receiver
   */
  public PExpression getReceiver(UObject uObject) {
    return PExpression.eq(PExpression.variable(receiverVariable), PExpression.constant(symbolTable.getConstant(uObject)));
  }

  /** 
   * @return Promela expression for checking for behavioural
   */
  public PExpression getBehavioural(UBehavioural uBehavioural) {
    return PExpression.eq(PExpression.variable(behaviouralVariable), PExpression.constant(symbolTable.getConstant(uBehavioural)));
  }

  /** 
   * @return Promela expression for checking for arguments
   */
  public PExpression getArguments(List<UExpression> uArguments, UContext uContext) {
    PExpression result = PExpression.trueConst();
    for (int i = 0; i < uArguments.size(); i++) {
      PExpression argumentExpression = PExpression.eq(PExpression.bracket(PExpression.variable(argumentsVariable), PExpression.constant(PConstant.numeric(i))), UMLTranslator.translateValue(uArguments.get(i), uContext, symbolTable));
      result = PExpression.and(result, argumentExpression);
    }
    return result;
  }

  private class OccurrenceSpecificationTranslator implements UObservable.InContextVisitor<PStatement> {
    private Set<UObservable> observables;
    private String currentLabel;
    private String nextLabel;

    OccurrenceSpecificationTranslator(String currentLabel, Set<UObservable> observables, String nextLabel) {
      this.currentLabel = currentLabel;
      this.observables = observables;
      this.nextLabel = nextLabel;
    }
    
    PStatement statement(UObservable uOccurrence) {
      return uOccurrence.accept(this);
    }

    @Override
    public UContext getContext() {
      return symbolTable.getCollaboration().getConstraintsContext();
    }

    @Override
    public PStatement onTesterOcc(UBehavioural uBehavioural, List<UExpression> uArguments, UObject uReceiver) {
      PStatement selectStatement = PStatement.selection();
      selectStatement.addBranch(PStatement.sequence(this.triggerStatement(), this.checkNoneStatement()));
      selectStatement.addBranch(this.sendStatement(uBehavioural, uArguments, uReceiver));
      return selectStatement;
    }

    @Override
    public PStatement onTesterDisp(UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return PStatement.selection().
               addBranch(selection(PExpression.and(getReceive(),
                                   PExpression.and(PExpression.eq(PExpression.variable(senderVariable), PExpression.constant(symbolTable.getEmptyConstant())),
                                   PExpression.and(getBehavioural(behavioural),
                                   PExpression.and(getArguments(arguments, this.getContext()),
                                                   getReceiver(receiver))))))).
               addBranch(PExpression.variable(PVariable.timeout()), PStatement.gotoStm(pFailedStm));
    }

    @Override
    public PStatement onOcc(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return PStatement.selection().
               addBranch(selection(PExpression.and(getSend(),
                                   PExpression.and(getSender(sender),
                                   PExpression.and(getBehavioural(behavioural),
                                   PExpression.and(getArguments(arguments, this.getContext()),
                                                   getReceiver(receiver))))))).
               addBranch(PExpression.variable(PVariable.timeout()), PStatement.gotoStm(pFailedStm));
    }

    @Override
    public PStatement onDisp(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return PStatement.selection().
               addBranch(selection(PExpression.and(getReceive(),
                                   PExpression.and(getSender(sender),
                                   PExpression.and(getBehavioural(behavioural),
                                   PExpression.and(getArguments(arguments, this.getContext()),
                                                   getReceiver(receiver))))))).
               addBranch(PExpression.variable(PVariable.timeout()), PStatement.gotoStm(pFailedStm));
    }

    private PStatement selection(PExpression pGuard) {
      PStatement selectStatement = PStatement.selection();
      selectStatement.addBranch(pGuard, PStatement.gotoStm(nextLabel));
      selectStatement.setElseBranch(this.checkNoneStatement());

      return PStatement.sequence(triggerStatement(), selectStatement);
    }

    private PStatement checkNoneStatement() {
      PStatement subSelectStatement = PStatement.selection();
      for (UObservable observable : this.observables)
        subSelectStatement.addBranch(translateOutOfPlace(observable));
      subSelectStatement.setElseBranch(PStatement.gotoStm(currentLabel));
      return subSelectStatement;
    }

    private PStatement sendStatement(UBehavioural uBehavioural, List<UExpression> uArguments, UObject uReceiver) {
      PStatement statement = PStatement.send(PExpression.bracket(PExpression.channel(symbolTable.getGlobalEventQueuesChannel()),
                                                                 PExpression.binary(symbolTable.access(uReceiver), POperator.MINUS,
                                                                                    PExpression.constant(PConstant.numeric(1)))));
      statement.addSendStmArg(PExpression.constant(symbolTable.getConstant(uBehavioural)));
      statement.addSendStmArg(PExpression.constant(symbolTable.getEmptyConstant())); // unknown sender
      for (int i = 0; i < uArguments.size(); i++)
        statement.addSendStmArg(UMLTranslator.translateValue(uArguments.get(i), this.getContext(), symbolTable));
      for (int i = uArguments.size(); i < symbolTable.getMaxParameters(); i++)
        statement.addSendStmArg(PExpression.constant(symbolTable.getEmptyConstant()));
      if (symbolTable.hasOperations())
        statement.addSendStmArg(PExpression.channel(getAcknowledgeInChannel()));
      
      // Create acknowledge statement
      if (uBehavioural instanceof UOperation) {
        PStatement step = PStatement.receive(PExpression.channel(getAcknowledgeInChannel()));
        step.addRecvStmArg(PExpression.variable(PVariable.uscore()));
        if (symbolTable.getProperties().isMutex())
          step = PStatement.sequence(symbolTable.exitMutex(), PStatement.sequence(step, symbolTable.enterMutex()));
        
        statement = PStatement.sequence(statement, step);
      }

      return statement;
    }

    private PStatement triggerStatement() {
      PStatement triggerStatement = PStatement.receive(PExpression.channel(symbolTable.getObserverChannel()));
      triggerStatement.addRecvStmArg(PExpression.variable(directionVariable));
      triggerStatement.addRecvStmArg(PExpression.variable(senderVariable));
      triggerStatement.addRecvStmArg(PExpression.variable(receiverVariable));      
      triggerStatement.addRecvStmArg(PExpression.variable(behaviouralVariable));
      for (int i = 0; i < symbolTable.getMaxParameters(); i++)
        triggerStatement.addRecvStmArg(PExpression.bracket(PExpression.variable(argumentsVariable), PExpression.constant(PConstant.numeric(i))));
      return triggerStatement;
    }
  }

  private PStatement translateOccurrenceSpecification(UObservable uOccurrence, String currentLabel, Set<UObservable> observables, String nextLabel) {
    return new OccurrenceSpecificationTranslator(currentLabel, observables, nextLabel).statement(uOccurrence);
  }

  private class OutOfPlaceTranslator implements UObservable.InContextVisitor<PStatement> {
    OutOfPlaceTranslator() {
    }
    
    PStatement statement(UObservable uObservable) {
      return uObservable.accept(this);
    }

    @Override
    public UContext getContext() {
      return symbolTable.getCollaboration().getConstraintsContext();
    }

    @Override
    public PStatement onTesterOcc(UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return selection(PExpression.and(getSend(),
                       PExpression.and(PExpression.eq(PExpression.variable(senderVariable), PExpression.constant(symbolTable.getEmptyConstant())),
                       PExpression.and(getBehavioural(behavioural),
                       PExpression.and(getArguments(arguments, this.getContext()),
                                       getReceiver(receiver))))));
    }

    @Override
    public PStatement onTesterDisp(UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return selection(PExpression.and(getReceive(),
                       PExpression.and(PExpression.eq(PExpression.variable(senderVariable), PExpression.constant(symbolTable.getEmptyConstant())),
                       PExpression.and(getBehavioural(behavioural),
                       PExpression.and(getArguments(arguments, this.getContext()),
                                       getReceiver(receiver))))));
    }

    @Override
    public PStatement onOcc(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return selection(PExpression.and(getSend(),
                       PExpression.and(getSender(sender),
                       PExpression.and(getBehavioural(behavioural),
                       PExpression.and(getArguments(arguments, this.getContext()),
                                       getReceiver(receiver))))));
    }

    @Override
    public PStatement onDisp(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return selection(PExpression.and(getReceive(),
                       PExpression.and(getSender(sender),
                       PExpression.and(getBehavioural(behavioural),
                       PExpression.and(getArguments(arguments, this.getContext()),
                                              getReceiver(receiver))))));
    }

    private PStatement selection(PExpression pGuard) {
      return PStatement.sequence(PStatement.expression(pGuard), PStatement.gotoStm(pFailedStm));
    }
  }

  private PStatement translateOutOfPlace(UObservable observable) {
    return new OutOfPlaceTranslator().statement(observable);
  }
}
