package translation.uppaal2dot;

import java.util.*;

import uppaal.TState;
import uppaal.TSystem;
import uppaal.TTemplate;
import uppaal.TTransition;


/**
 * Format an UPPAAL system as a dot file
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
public class DotFormatter implements TSystem.Visitor {
  private StringBuffer dot = new StringBuffer();
  private String currentTemplateName = "";
  private boolean inTransition = false;
  private HashMap<String, String> shortStateNames = new HashMap<String, String>();
  private int shortStateNamesCounter = 0;

  public DotFormatter() {
  }

  public String declaration() {
    return dot.toString();
  }

  public void onSystem(List<TTemplate> templates) {
    dot.append("digraph " + "HUGO" + " {\n");
    for (TTemplate template : templates)
      template.accept(this);
    dot.append("size=\"11.5,8\";\n}\n");
  }

  public void onTemplate(String templateName, List<TState> states, List<TTransition> transitions) {
    this.currentTemplateName = templateName;
    dot.append("subgraph cluster" + this.currentTemplateName + " {\n");
    for (TState state : states)
      state.accept(this);
    for (TTransition transition : transitions)
      transition.accept(this);
    dot.append("}\n");
  }

  public void onState(String stateName) {
    if (this.inTransition) {
      dot.append(this.getShortStateName(stateName));
    }
  }

  public void onTransition(TState source, TState target) {
    this.inTransition = true;
    source.accept(this);
    dot.append(" -> ");
    target.accept(this);
    dot.append(";\n");
    this.inTransition = false;
  }

  private String getShortStateName(String stateName) {
    String fullStateName = this.currentTemplateName + "_" + stateName;
    String shortStateName = this.shortStateNames.get(fullStateName);
    if (shortStateName == null) {
      shortStateName = "S" + shortStateNamesCounter++;
      this.shortStateNames.put(fullStateName, shortStateName);
    }
    return shortStateName;
  }
}
