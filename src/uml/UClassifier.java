package uml;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Contains data of a classifier.
 *
 * A classifier always induces a simple type, and thus provides an
 * initial value.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class UClassifier extends UElement {
  private UModel model;

  /**
   * Create a classifier.
   *
   * @param name a string
   * @param model a model
   */
  public UClassifier(String name, UModel model) {
    super(name);
    this.model = model;
  }

  /**
   * @return classifier's model
   */
  public UModel getModel() {
    return this.model;
  }

  /**
   * @return classifier's attributes
   */
  public List<UAttribute> getAttributes() {
    return new ArrayList<>();
  }

  /**
   * @return classifier's operations
   */
  public List<UOperation> getOperations() {
    return new ArrayList<>();
  }

  /**
   * @return initial value for this classifier
   */
  public abstract UExpression getInitialValue();
}
