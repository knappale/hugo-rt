package uml;

import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * Contains data of a reception.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UReception extends UBehavioural {
  private USignal signal;

  /**
   * Create a new reception
   *
   * @param name reception's name
   * @param signal reception's signal
   * @param owner reception's owning class
   */
  public UReception(String name, USignal signal, UClass owner) {
    super(name, owner);
    this.signal = signal;
    this.signal.addReception(this);
  }

  @Override
  @NonNullByDefault({})
  public <T> T cases(@NonNull Function<@NonNull UOperation, T> operationFun,
                     @NonNull Function<@NonNull UReception, T> receptionFun) {
    return receptionFun.apply(this);
  }

  /**
   * @return reception's signal
   */
  public USignal getSignal() {
    return this.signal;
  }

  @Override
  public void addParameter(UParameter parameter) {
    super.addParameter(parameter);
  }

  /**
   * @param prefix prefix string
   * @return operation's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder builder = new StringBuilder();

    builder.append(prefix);
    builder.append("reception ");
    builder.append(this.getName());
    builder.append("(");
    builder.append(Formatter.separated(this.getParameters(), p -> p.declaration(), ", "));
    builder.append(")");
    if (this.getMethods().isEmpty())
      builder.append(";");
    else {
      builder.append(" {\n");
      builder.append(Formatter.separated(this.getMethods(), m -> m.declaration(prefix + "  "), "\n"));
      builder.append("\n");
      builder.append(prefix);
      builder.append("}");
    }
    return builder.toString();
  }

  /**
   * @return reception's string representation
   */
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Reception [name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append(", signal = ");
    resultBuilder.append(this.getSignal().getName());
    resultBuilder.append(", parameters = (");
    String sep = "";
    for (UParameter parameter : this.getParameters()) {
      resultBuilder.append(sep);
      resultBuilder.append(parameter.getName());
      resultBuilder.append(" : ");
      resultBuilder.append(parameter.getClassifier().getName());
      sep = ", ";
    }
    resultBuilder.append(")");
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
