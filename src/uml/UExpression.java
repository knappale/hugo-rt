package uml;

import org.eclipse.jdt.annotation.Nullable;

import util.Identifier;

import org.eclipse.jdt.annotation.NonNullByDefault;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static util.Formatter.parenthesised;
import static util.Formatter.context;
import static util.Formatter.quoted;
import static util.Formatter.type;
import static util.Objects.requireNonNull;


/**
 * UML expression
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UExpression {
  static enum Kind {
    BOOLEAN,
    INTEGER,
    STRING,
    NULL,
    THIS,
    ID,
    ARRAY,
    CONDITIONAL,
    UNARY,
    BINARY;
  }

  public boolean hasPrecedence(UExpression other) {
    if ((this.kind == Kind.UNARY || this.kind == Kind.BINARY) &&
        (other.kind == Kind.UNARY || other.kind == Kind.BINARY)) {
      return UOperator.precedes(this.operator, other.operator);
    }
    else
      return true;
  }

  private static final UExpression TRUEEXPRESSION;
  private static final UExpression FALSEEXPRESSION;
  private static final UExpression NULLEXPRESSION;
  static {
    TRUEEXPRESSION = new UExpression(Kind.BOOLEAN);
    TRUEEXPRESSION.operator = UOperator.TRUE;
    FALSEEXPRESSION = new UExpression(Kind.BOOLEAN);
    FALSEEXPRESSION.operator = UOperator.FALSE;
    NULLEXPRESSION = new UExpression(Kind.NULL);
  }
  
  private Kind kind;
  private @Nullable Identifier identifier = null;
  private @Nullable UExpression condExpression = null;
  private @Nullable UExpression leftExpression = null;
  private @Nullable UExpression rightExpression = null;
  private @Nullable UOperator operator = null;
  private int intVal = 0;
  private String strVal = "";

  public interface InContextVisitor<T> {
    public UContext getContext();

    public T onBooleanConstant(boolean booleanConstant);
    public T onIntegerConstant(int integerConstant);
    public T onStringConstant(String stringConstant);
    public T onNull();
    public T onThis();
    public T onStatual(UStatual statual);
    public T onArray(UAttribute attribute, UExpression offset);
    public T onConditional(UExpression conditionExpression, UExpression trueExpression, UExpression falseExpression);
    public T onUnary(UOperator operator, UExpression expression);
    public T onArithmetical(UExpression leftExpression, UOperator arithmetical, UExpression rightExpression);
    public T onRelational(UExpression leftExpression, UOperator relational, UExpression rightExpression);
    public T onJunctional(UExpression leftExpression, UOperator junctional, UExpression rightExpression);
  }

  private UExpression(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create an expression which represents a boolean constant.
   * 
   * @param booleanConstant a boolean
   * @return an expression
   */
  public static UExpression boolConst(boolean booleanConstant) {
    return (booleanConstant ? TRUEEXPRESSION : FALSEEXPRESSION);
  }

  /**
   * Create an expression which represents the boolean constant <CODE>true</CODE>.
   * 
   * @return an expression
   */
  public static UExpression trueConst() {
    return TRUEEXPRESSION;
  }

  /**
   * Create an expression which represents the boolean constant <CODE>false</CODE>.
   * 
   * @return an expression
   */
  public static UExpression falseConst() {
    return FALSEEXPRESSION;
  }

  /**
   * Create an expression which represents <CODE>null</CODE>.
   * 
   * @return an expression
   */
  public static UExpression nullConst() {
    return NULLEXPRESSION;
  }

  /**
   * Create an expression which represents an integer constant.
   * 
   * @param integerConstant integer
   * @return an expression
   */
  public static UExpression intConst(int integerConstant) {
    UExpression e = new UExpression(Kind.INTEGER);
    e.intVal = integerConstant;
    return e;
  }

  /**
   * Create an expression which represents a string constant.
   * 
   * @param stringConstant string
   * @return an expression
   */
  public static UExpression stringConst(String stringConstant) {
    UExpression e = new UExpression(Kind.STRING);
    e.strVal = stringConstant;
    return e;
  }

  /**
   * Create an expression which represents an empty string constant.
   * 
   * @return an expression
   */
  public static UExpression emptyStringConst() {
    UExpression e = new UExpression(Kind.STRING);
    e.strVal = "";
    return e;
  }

  /**
   * Create an expression which represents <CODE>this</CODE>.
   * 
   * @return an expression
   */
  public static UExpression thisConst() {
    UExpression e = new UExpression(Kind.THIS);
    return e;
  }

  /**
   * Create an expression which represents an identifier.
   * 
   * @param identifier identifier
   * @return an expression
   */
  public static UExpression id(Identifier identifier) {
    UExpression e = new UExpression(Kind.ID);
    e.identifier = identifier;
    return e;
  }

  /**
   * Create an expression which represents an array retrieval.
   * 
   * @param identifier array identifier
   * @param expression offset expression
   * @return an expression
   */
  public static UExpression array(Identifier identifier, UExpression expression) {
    UExpression e = new UExpression(Kind.ARRAY);
    e.identifier = identifier;
    e.leftExpression = expression;
    return e;
  }

  /**
   * Create an expression which represents a conditional expression.
   * 
   * @param condExpression boolean condition
   * @param leftExpression true-branch
   * @param rightExpression false-branch
   * @return an expression
   */
  public static UExpression conditional(UExpression condExpression, UExpression leftExpression, UExpression rightExpression) {
    if (leftExpression.equals(rightExpression))
      return leftExpression;
    if (condExpression.equals(UExpression.trueConst()))
      return leftExpression;
    if (condExpression.equals(UExpression.falseConst()))
      return rightExpression;

    UExpression e = new UExpression(Kind.CONDITIONAL);
    e.condExpression = condExpression;
    e.leftExpression = leftExpression;
    e.rightExpression = rightExpression;
    return e;
  }

  /**
   * Create an expression which represents a unary operation.
   * 
   * @param expression expression
   * @param unary unary operator
   * @return an expression
   */
  private static UExpression unary(UOperator unary, UExpression expression) {
    UExpression e = new UExpression(Kind.UNARY);
    e.operator = unary;
    e.leftExpression = expression;
    return e;
  }

  /**
   * Create an expression which represents a negation.
   * 
   * @param expression expression
   * @return an expression
   */
  public static UExpression neg(UExpression expression) {
    switch (expression.kind) {
      case BOOLEAN: {
        return ((expression.operator == UOperator.TRUE) ? UExpression.falseConst() : UExpression.trueConst());
      }

      case UNARY: {
        if (expression.operator == UOperator.UNEG)
          return requireNonNull(expression.leftExpression);
        break;
      }

      case CONDITIONAL: {
        return conditional(requireNonNull(expression.condExpression), UExpression.neg(requireNonNull(expression.leftExpression)), UExpression.neg(requireNonNull(expression.rightExpression)));
      }

      case BINARY: {
        UOperator negOp = requireNonNull(expression.operator);
        UExpression negLeft = requireNonNull(expression.leftExpression);
        UExpression negRight = requireNonNull(expression.rightExpression);
        switch (negOp) {
          case LT:
            negOp = UOperator.GEQ;
            break;
          case LEQ:
            negOp = UOperator.GT;
            break;
          case GEQ:
            negOp = UOperator.LT;
            break;
          case GT:
            negOp = UOperator.LEQ;
            break;
          case EQ:
            negOp = UOperator.NEQ;
            break;
          case NEQ:
            negOp = UOperator.EQ;
            break;

          case AND:
            negOp = UOperator.OR; negLeft = UExpression.neg(negLeft); negRight = UExpression.neg(negRight);
            break;
          case OR:
            negOp = UOperator.AND; negLeft = UExpression.neg(negLeft); negRight = UExpression.neg(negRight);
            break;

          //$CASES-OMITTED$
          default:
        }
        if (negOp != expression.operator)
          return UExpression.binary(negLeft, negOp, negRight);
        break;
      }

      //$CASES-OMITTED$
      default:
    }

    return UExpression.unary(UOperator.UNEG, expression);
  }

  /**
   * Create an expression which represents a unary plus.
   * 
   * @param expression expression
   * @return an expression
   */
  public static UExpression plus(UExpression expression) {
    return expression;
  }

  /**
   * Create an expression which represents a unary minus.
   * 
   * @param expression expression
   * @return an expression
   */

  public static UExpression minus(UExpression expression) {
    if (expression.kind == Kind.UNARY && expression.operator == UOperator.UMINUS)
      return requireNonNull(expression.leftExpression);
    return UExpression.unary(UOperator.UMINUS, expression);
  }

  /**
   * Create an expression which represents a binary expression.
   * 
   * @param leftExpression left expression
   * @param binary binary operator
   * @param rightExpression right expression
   * @return an expression
   */
  private static UExpression binary(UExpression leftExpression, UOperator binary, UExpression rightExpression) {
    UExpression e = new UExpression(Kind.BINARY);
    e.leftExpression = leftExpression;
    e.operator = binary;
    e.rightExpression = rightExpression;
    return e;
  }

  /**
   * Create an expression which represents an arithmetical expression.
   * 
   * @param leftExpression left expression
   * @param rightExpression right expression
   * @param arithmetical binary arithmetical operator
   * @return an expression
   */
  public static UExpression arithmetical(UExpression leftExpression, UOperator arithmetical, UExpression rightExpression) {
    return UExpression.binary(leftExpression, arithmetical, rightExpression);
  }

  /**
   * Create an expression which represents a relational expression.
   * 
   * @param leftExpression left expression
   * @param rightExpression right expression
   * @param relational relational operator
   * @return an expression
   */
  public static UExpression relational(UExpression leftExpression, UOperator relational, UExpression rightExpression) {
    return UExpression.binary(leftExpression, relational, rightExpression);
  }

  /**
   * Create an expression which represents a conjunctional expression.
   * 
   * @param leftExpression left expression
   * @param rightExpression right expression
   * @return an expression
   */
  public static UExpression and(UExpression leftExpression, UExpression rightExpression) {
    if (leftExpression.equals(rightExpression))
      return leftExpression;

    if (leftExpression.equals(trueConst()))
      return rightExpression;
    if (rightExpression.equals(trueConst()))
      return leftExpression;
    if (leftExpression.equals(falseConst()) || rightExpression.equals(falseConst()))
      return UExpression.falseConst();

    return UExpression.binary(leftExpression, UOperator.AND, rightExpression);
  }

  /**
   * Create an expression which represents a disjunctional expression.
   * 
   * @param leftExpression left expression
   * @param rightExpression right expression
   * @return an expression
   */
  public static UExpression or(UExpression leftExpression, UExpression rightExpression) {
    if (leftExpression.equals(rightExpression))
      return leftExpression;

    if (leftExpression.equals(falseConst()))
      return rightExpression;
    if (rightExpression.equals(falseConst()))
      return leftExpression;
    if (leftExpression.equals(trueConst()) || rightExpression.equals(trueConst()))
      return UExpression.trueConst();

    return UExpression.binary(leftExpression, UOperator.OR, rightExpression);
  }

  /**
   * Determine expression's type.
   * 
   * @param context expression context
   * @return expression's type
   */
  public UType getType(UContext context) {
    switch (this.kind) {
      case BOOLEAN:
        return context.getBooleanType();

      case INTEGER:
        return context.getIntegerType();

      case STRING:
        return context.getStringType();

      case THIS:
        return context.getThisType();

      case NULL:
        return context.getNullType();

      case ID: {
        Identifier identifier = requireNonNull(this.identifier);

        UStatual statual = context.getStatual(identifier);
        if (statual == null)
          return UType.error("No matching element found for " + context(identifier, context.description()));
        UType identifierType = statual.getType();
        if (identifierType.isArray())
          return UType.error("non-array access to array identifier " + type(identifier, identifierType));
        return identifierType;
      }

      case ARRAY: {
        Identifier identifier = requireNonNull(this.identifier);
        UExpression leftExpression = requireNonNull(this.leftExpression);

        UAttribute attribute = context.getAttribute(this.identifier);
        if (attribute == null)
          return UType.error("No matching element found for " + context(identifier, context.description()));
        UType identifierType = attribute.getType();
        if (identifierType.isError())
          return identifierType;
        UType offsetType = leftExpression.getType(context);
        if (offsetType.isError())
          return offsetType;
        if (!identifierType.isArray())
          return UType.error("array access to non-array identifier " + type(identifier, identifierType));
        UType underlyingType = identifierType.getUnderlyingType();
        if (!context.getIntegerType().subsumes(leftExpression.getType(context)))
          return UType.error("non-integer array offset " + type(leftExpression, offsetType));
        return underlyingType;
      }

      case UNARY: {
        UOperator operator = requireNonNull(this.operator);
        UExpression leftExpression = requireNonNull(this.leftExpression);

        UType leftType = leftExpression.getType(context);
        if (leftType.isError())
          return leftType;
        switch (operator) {
          case UPLUS:
          case UMINUS:
            if (!context.getIntegerType().subsumes(leftType))
              return UType.error(quoted(operator.getName()) + " applied to non-integer expression " + type(leftExpression, leftType));
            return leftType;
          case UNEG:
            if (!context.getBooleanType().subsumes(leftType))
              return UType.error(quoted(operator.getName()) + " applied to non-boolean expression " + type(leftExpression, leftType));
            return leftType;
          //$CASES-OMITTED$
          default :
            return UType.error("[illegal unary operator]");
        }
      }

      case CONDITIONAL: {
        UExpression condExpression = requireNonNull(this.condExpression);
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UExpression rightExpression = requireNonNull(this.rightExpression);

        UType condType = condExpression.getType(context);
        if (condType.isError())
          return condType;
        UType leftType = leftExpression.getType(context);
        if (leftType.isError())
          return leftType;
        UType rightType = rightExpression.getType(context);
        if (rightType.isError())
          return rightType;
        if (!context.getBooleanType().subsumes(condType))
          return UType.error("non-boolean condition " + type(condExpression, condType));
        if (leftType.subsumes(rightType))
          return leftType;
        if (rightType.subsumes(leftType))
          return rightType;
        return UType.error("type incompatibility in conditional expression " + type(leftExpression, leftType) + " - " + type(rightExpression, rightType));
      }

      case BINARY: {
        UOperator operator = requireNonNull(this.operator);
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UExpression rightExpression = requireNonNull(this.rightExpression);

        UType leftType = leftExpression.getType(context);
        if (leftType.isError())
          return leftType;
        UType rightType = rightExpression.getType(context);
        if (rightType.isError())
          return rightType;
        switch (operator) {
          case ADD:
            if (!context.getIntegerType().subsumes(leftType) && !context.getStringType().subsumes(leftType))
              return UType.error("non-integer and non-string type in additive expression " + type(leftExpression, leftType));
            if (context.getIntegerType().subsumes(leftType)) {
              if (!context.getIntegerType().subsumes(rightType))
                return UType.error("non-integer type in arithmetical expression " + type(rightExpression, rightType));
            }
            else {
              if (!context.getIntegerType().subsumes(rightType) && !context.getStringType().subsumes(leftType))
                return UType.error("non-integer and non-string type in additive expression " + type(rightExpression, rightType));
            }
            return leftType;

          case SUB:
          case MULT:
          case DIV:
          case MOD:
            if (!context.getIntegerType().subsumes(leftType))
              return UType.error("non-integer type in arithmetical expression " + type(leftExpression, leftType));
            if (!context.getIntegerType().subsumes(rightType))
              return UType.error("non-integer type in arithmetical expression " + type(rightExpression, rightType));
            return leftType;

          case LT:
          case LEQ:
          case GEQ:
          case GT:
            if (!context.getIntegerType().subsumes(leftType))
              return UType.error("non-integer type in comparison " + type(leftExpression, leftType));
            if (!context.getIntegerType().subsumes(rightType))
              return UType.error("non-integer type in comparison " + type(rightExpression, rightType));
            return context.getBooleanType();

          case EQ:
          case NEQ:
            if (!(leftType.subsumes(rightType) || rightType.subsumes(leftType)))
              return UType.error("type incompatibility in equation " + type(leftExpression, leftType) + " - " + type(rightExpression, rightType));
            return context.getBooleanType();

          case AND:
          case OR:
            if (!context.getBooleanType().subsumes(leftType))
              return UType.error("non-boolean type in junctional expression " + type(leftExpression, leftType));
            if (!context.getBooleanType().subsumes(rightType))
              return UType.error("non-integer type in junctional expression `" + type(rightExpression, rightType));
            return leftType;

          //$CASES-OMITTED$
          default:
            return UType.error("[illegal binary operator]");
        }
      }

      default:
       return UType.error("[unknown expression kind]");
    }
  }

  /**
   * Partially evaluate this expression.
   * 
   * @param context expression context
   * @return expression representing partial evaluation of this expression
   * @pre !getType(context).isError()
   * TODO (AK141229) Further extend partial evaluation of UML expressions to constants
   */
  public UExpression getEvaluated(UContext context) {
    switch (this.kind) {
      case ID: {
        Identifier identifier = requireNonNull(this.identifier);

        UType type = this.getType(context);
        if (type.equals(context.getBooleanType()) ||
            type.equals(context.getIntegerType())) {
          UAttribute uAttribute = context.getAttribute(identifier);
          if (uAttribute != null && uAttribute.isFinal() &&
              uAttribute.hasInitialValues() && uAttribute.getInitialValues().size() == 1) {
            UExpression someInitialValue = requireNonNull(uAttribute.getInitialValue(0));
            return someInitialValue.getEvaluated(context);
          }
        }
        return this;
      }

      case ARRAY: {
        Identifier identifier = requireNonNull(this.identifier);
        UExpression leftExpression = requireNonNull(this.leftExpression);

        UExpression evaluatedLeftExpression = leftExpression.getEvaluated(context);
        if (evaluatedLeftExpression.kind == Kind.INTEGER) {
          int intVal = evaluatedLeftExpression.intVal;
          UType type = this.getType(context);
          if (type.equals(context.getBooleanType()) ||
              type.equals(context.getIntegerType())) {
            UAttribute uAttribute = context.getAttribute(identifier);
            if (uAttribute != null && uAttribute.isFinal() &&
                uAttribute.hasInitialValues() && uAttribute.getInitialValues().size() > intVal) {
              UExpression someInitialValue = requireNonNull(uAttribute.getInitialValue(intVal));
              return someInitialValue.getEvaluated(context);
            }
          }
        }

        return UExpression.array(identifier, evaluatedLeftExpression);
      }

      case CONDITIONAL: {
        UExpression condExpression = requireNonNull(this.condExpression);
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UExpression rightExpression = requireNonNull(this.rightExpression);

        UExpression evaluatedCondExpression = condExpression.getEvaluated(context);
        UExpression evaluatedLeftExpression = leftExpression.getEvaluated(context);
        UExpression evaluatedRightExpression = rightExpression.getEvaluated(context);
        if (evaluatedCondExpression.equals(trueConst()))
          return evaluatedLeftExpression;
        if (evaluatedCondExpression.equals(falseConst()))
          return evaluatedRightExpression;
        if (evaluatedLeftExpression.equals(evaluatedRightExpression))
          return evaluatedLeftExpression;
        return UExpression.conditional(evaluatedCondExpression, evaluatedLeftExpression, evaluatedRightExpression);
      }

      case UNARY: {
        UOperator operator = requireNonNull(this.operator);
        UExpression leftExpression = requireNonNull(this.leftExpression);

        UExpression evaluatedLeftExpression = leftExpression.getEvaluated(context);

        if (evaluatedLeftExpression.kind == Kind.BOOLEAN) {
          switch (operator) {
            case UNEG: {
              return UExpression.boolConst(!(evaluatedLeftExpression.operator == UOperator.TRUE));
            }

            //$CASES-OMITTED$
            default:
              break;
          }
        }

        if (evaluatedLeftExpression.kind == Kind.INTEGER) {
          switch (operator) {
            case UPLUS:
              return UExpression.intConst(evaluatedLeftExpression.intVal);
            case UMINUS:
              return UExpression.intConst(-evaluatedLeftExpression.intVal);

            //$CASES-OMITTED$
            default:
              break;
          }
        }

        return UExpression.unary(operator, evaluatedLeftExpression);
      }

      case BINARY: {
        UOperator operator = requireNonNull(this.operator);
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UExpression rightExpression = requireNonNull(this.rightExpression);

        UExpression evaluatedLeftExpression = leftExpression.getEvaluated(context);
        UExpression evaluatedRightExpression = rightExpression.getEvaluated(context);

        if (evaluatedLeftExpression.kind == Kind.INTEGER && evaluatedRightExpression.kind == Kind.INTEGER) {
          switch (operator) {
            case ADD:
              return UExpression.intConst(evaluatedLeftExpression.intVal + evaluatedRightExpression.intVal);
            case SUB:
              return UExpression.intConst(evaluatedLeftExpression.intVal - evaluatedRightExpression.intVal);
            case MULT:
              return UExpression.intConst(evaluatedLeftExpression.intVal * evaluatedRightExpression.intVal);
            case DIV:
              return UExpression.intConst(evaluatedLeftExpression.intVal / evaluatedRightExpression.intVal);
            case MOD:
              return UExpression.intConst(evaluatedLeftExpression.intVal % evaluatedRightExpression.intVal);

            case LT:
              return UExpression.boolConst(evaluatedLeftExpression.intVal < evaluatedRightExpression.intVal);
            case LEQ:
              return UExpression.boolConst(evaluatedLeftExpression.intVal <= evaluatedRightExpression.intVal);
            case GEQ:
              return UExpression.boolConst(evaluatedLeftExpression.intVal >= evaluatedRightExpression.intVal);
            case GT:
              return UExpression.boolConst(evaluatedLeftExpression.intVal > evaluatedRightExpression.intVal);
            case EQ:
              return UExpression.boolConst(evaluatedLeftExpression.intVal == evaluatedRightExpression.intVal);
            case NEQ:
              return UExpression.boolConst(evaluatedLeftExpression.intVal != evaluatedRightExpression.intVal);

            //$CASES-OMITTED$
            default:
              break;
          }
        }

        if (evaluatedLeftExpression.kind == Kind.BOOLEAN && evaluatedRightExpression.kind == Kind.BOOLEAN) {
          switch (operator) {
            case EQ:
              return UExpression.boolConst((evaluatedLeftExpression.operator == UOperator.TRUE) == (evaluatedRightExpression.operator == UOperator.TRUE));
            case NEQ:
              return UExpression.boolConst((evaluatedLeftExpression.operator == UOperator.TRUE) != (evaluatedRightExpression.operator == UOperator.TRUE));

            case AND:
              return UExpression.boolConst((evaluatedLeftExpression.operator == UOperator.TRUE) && (evaluatedRightExpression.operator == UOperator.TRUE));
            case OR:
              return UExpression.boolConst((evaluatedLeftExpression.operator == UOperator.TRUE) || (evaluatedRightExpression.operator == UOperator.TRUE));

            //$CASES-OMITTED$
            default:
              break;
          }
        }

        return UExpression.binary(evaluatedLeftExpression, operator, evaluatedRightExpression);
      }

      //$CASES-OMITTED$
      default:
        return this;
    }
  }
  
  /**
   * Determine integer value of this expression.
   * 
   * @param context expression context
   * @return integer value of this expression
   * @pre context.isConstant() && !getType(context).isError() && getEvaluated(context).kind == INTEGER
   */
  public int getIntegerValue(UContext context) {
    UExpression evaluated = this.getEvaluated(context);
    if (evaluated.kind == Kind.INTEGER)
      return evaluated.intVal;
    throw new IllegalStateException("Cannot access `" + this + "' in context `" + context.description() + "'");
  }
  
  /**
   * Determine boolean value of this expression.
   * 
   * @param context expression context
   * @return boolean value of this expression
   * @pre context.isConstant() && !getType(context).isError() && getEvaluated(context).kind == BOOLEAN
   */
  public boolean getBooleanValue(UContext context) {
    UExpression evaluated = this.getEvaluated(context);
    if (evaluated.kind == Kind.BOOLEAN)
      return (evaluated.operator == UOperator.TRUE);
    throw new IllegalStateException("Cannot access `" + this + "' in context `" + context.description() + "'");
  }

  /**
   * Determine all top-level literals connected by a given operator.
   *
   * @param modeOperator connection operator, i.e., AND or OR
   * @param context a UML context
   * @return top-level literals connected by {@code modeOperator}
   */
  public Set<UExpression> getTopLevelLiterals(UOperator modeOperator, UContext context) {
    Set<UExpression> atoms = new HashSet<>();
    if (!context.getBooleanType().subsumes(this.getType(context)))
      return atoms;

    switch (this.kind) {
      case ID: {
        atoms.add(this);
        break;
      }

      case ARRAY: {
        atoms.add(this);
        break;
      }

      case UNARY: {
        var operator = requireNonNull(this.operator);
        if (operator == UOperator.UNEG)
          atoms.add(this);
        break;
      }

      case CONDITIONAL: {
        var evaluatedCondExpression = requireNonNull(this.condExpression).getEvaluated(context);
        if (evaluatedCondExpression.equals(trueConst())) {
          atoms.addAll(requireNonNull(this.leftExpression).getTopLevelLiterals(modeOperator, context));
          break;
        }
        if (evaluatedCondExpression.equals(falseConst())) {
          atoms.addAll(requireNonNull(this.rightExpression).getTopLevelLiterals(modeOperator, context));
          break;
        }
        break;
      }

      case BINARY: {
        var operator = requireNonNull(this.operator);
        if (operator.isComparison()) {
          atoms.add(this);
          break;
        }
        if (modeOperator == operator) {
          atoms.addAll(requireNonNull(this.leftExpression).getTopLevelLiterals(modeOperator, context));
          atoms.addAll(requireNonNull(this.rightExpression).getTopLevelLiterals(modeOperator, context));
          break;
        }
        break;
      }

      //$CASES-OMITTED$
      default:
    }

    return atoms;
  }

  public boolean isValue(UContext context) {
    switch (this.getEvaluated(context).kind) {
      case BOOLEAN:
      case INTEGER:
      case NULL:
      case STRING:
      case THIS:
        return true;
      default:
        return false;
    }
  }

  /**
   * Determine whether this expression is disjoint from another one, i.e.,
   * whether they cannot evaluate to the same.
   *
   * @param other another UML expression
   * @param context a UML context
   * @return whether {@code this} and {@code other} are disjoint in {@code context}
   */
  public boolean disjoint(UExpression other, UContext context) {
    var thisEvaluated = this.getEvaluated(context);
    var otherEvaluated = other.getEvaluated(context);
    return thisEvaluated.isValue(context) && otherEvaluated.isValue(context) && !thisEvaluated.equals(otherEvaluated);
  }

  public boolean isContradictory(UExpression other, UContext context) {
    if ((this.kind == Kind.ID || this.kind == Kind.ARRAY) && other.kind == Kind.UNARY) {
      var otherOperator = requireNonNull(other.operator);
      if (otherOperator != UOperator.UNEG)
        return false;
      var otherSubExpression = requireNonNull(other.leftExpression);
      if (otherSubExpression.kind != this.kind)
        return false;
      if (this.equals(otherSubExpression))
        return true;
    }
    if (this.kind == Kind.UNARY && (other.kind == Kind.ID || other.kind == Kind.ARRAY))
      return other.isContradictory(this, context);

    if (this.kind != Kind.BINARY || other.kind != Kind.BINARY)
      return false;
    var thisOperator = requireNonNull(this.operator);
    var otherOperator = requireNonNull(other.operator);
    if (!(thisOperator.isComparison() && otherOperator.isComparison()))
      return false;
    var thisLeftExpression = requireNonNull(this.leftExpression).getEvaluated(context);
    var thisRightExpression = requireNonNull(this.rightExpression).getEvaluated(context);
    var otherLeftExpression = requireNonNull(other.leftExpression).getEvaluated(context);
    var otherRightExpression = requireNonNull(other.rightExpression).getEvaluated(context);
    if (thisOperator == UOperator.EQ && otherOperator == UOperator.EQ &&
        ((thisLeftExpression.equals(otherLeftExpression) && thisRightExpression.disjoint(otherRightExpression, context)) ||
         (thisLeftExpression.equals(otherRightExpression) && thisRightExpression.disjoint(otherLeftExpression, context))))
      return true;
    if (thisOperator.isComplement(otherOperator) &&
        ((thisLeftExpression.equals(otherLeftExpression) && thisRightExpression.equals(otherRightExpression)) ||
         (thisLeftExpression.equals(otherRightExpression) && thisRightExpression.equals(otherLeftExpression))))
      return true;
    return false;
  }

  public boolean isContradictory(UContext context) {
    UExpression evaluated = this.getEvaluated(context);
    if (evaluated.equals(falseConst()))
      return true;
    Set<UExpression> equations = evaluated.getTopLevelLiterals(UOperator.AND, context);
    for (var equation1 : equations)
      for (var equation2 : equations)
        if (equation1.isContradictory(equation2, context))
          return true;
    return false;
  }

  public boolean isTautology(UContext context) {
    UExpression evaluated = this.getEvaluated(context);
    if (evaluated.equals(trueConst()))
      return true;
    Set<UExpression> equations = evaluated.getTopLevelLiterals(UOperator.OR, context);
    for (var equation1 : equations)
      for (var equation2 : equations)
        if (!UExpression.neg(equation1).isContradictory(UExpression.neg(equation2), context))
          return true;
    return false;
  }

  /**
   * Determine the set of elements this expression depends on.
   * 
   * @param context expression context
   * @return expression's dependency set
   * @pre !getType(context).isError()
   */
  public Set<UStatual> getDependencies(UContext context) {
    return getDependencies(context, new HashSet<>());
  }

  /**
   * Determine the set of elements this expression depends on, avoiding
   * cyclic dependencies.
   * 
   * @param context expression context
   * @param dependencies set of elements already visited
   * @return expression's dependency set
   * @pre !getType(context).isError()
   */
  private Set<UStatual> getDependencies(UContext context, Set<UStatual> dependencies) {
    switch (this.kind) {
      case ID: {
        Identifier identifier = requireNonNull(this.identifier);

        UStatual statual = context.getStatual(identifier);
        if (statual != null && !dependencies.contains(statual)) {
          dependencies.add(statual);
          if (statual.isConstant())
            dependencies.addAll(statual.getDependencies(context));
        }
        return dependencies;
      }

      case ARRAY: {
        Identifier identifier = requireNonNull(this.identifier);
        UExpression leftExpression = requireNonNull(this.leftExpression);

        UStatual statual = context.getStatual(identifier);
        if (statual != null && !dependencies.contains(statual)) {
          dependencies.add(statual);
          if (statual.isConstant())
            dependencies.addAll(statual.getDependencies(context));
        }
        dependencies.addAll(leftExpression.getDependencies(context, dependencies));
        return dependencies;
      }

      case UNARY: {
        UExpression leftExpression = requireNonNull(this.leftExpression);

        dependencies.addAll(leftExpression.getDependencies(context, dependencies));
        return dependencies;
      }

      case CONDITIONAL: {
        UExpression condExpression = requireNonNull(this.condExpression);
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UExpression rightExpression = requireNonNull(this.rightExpression);

        dependencies.addAll(condExpression.getDependencies(context, dependencies));
        dependencies.addAll(leftExpression.getDependencies(context, dependencies));
        dependencies.addAll(rightExpression.getDependencies(context, dependencies));
        return dependencies;
      }

      case BINARY: {
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UExpression rightExpression = requireNonNull(this.rightExpression);

        dependencies.addAll(leftExpression.getDependencies(context, dependencies));
        dependencies.addAll(rightExpression.getDependencies(context, dependencies));
        return dependencies;
      }

      //$CASES-OMITTED$
      default:
        return dependencies;
    }
  }

  /**
   * Determine whether expression is a constant in given context.
   * 
   * @param context expression context
   * @return whether expression represents a constant
   * @pre !getType(context).isError()
   */
  public boolean isConstant(UContext context) {
    switch (this.kind) {
      case THIS:
        return false;

      case NULL:
        return true;

      case BOOLEAN:
        return true;

      case INTEGER:
        return true;

      case STRING:
        return true;

      case ID: {
        Identifier identifier = requireNonNull(this.identifier);
        UStatual statual = requireNonNull(context.getStatual(identifier));

        return (statual.isConstant());
      }

      case ARRAY: {
        Identifier identifier = requireNonNull(this.identifier);
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UAttribute attribute = requireNonNull(context.getAttribute(identifier));

        return (attribute.isConstant() && leftExpression.isConstant(context));
      }

      case CONDITIONAL: {
        UExpression condExpression = requireNonNull(this.condExpression);
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UExpression rightExpression = requireNonNull(this.rightExpression);

        return (condExpression.isConstant(context) && leftExpression.isConstant(context) && rightExpression.isConstant(context));
      }

      case UNARY: {
        UExpression leftExpression = requireNonNull(this.leftExpression);

        return (leftExpression.isConstant(context));
      }

      case BINARY: {
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UExpression rightExpression = requireNonNull(this.rightExpression);

        return (leftExpression.isConstant(context) && rightExpression.isConstant(context));
      }

      default:
        return false;
    }
  }

  /**
   * Compute disjunctive normal form of the current expression.
   * 
   * @return expression's disjunctive normal form
   */
  public UExpression getDisjunctiveNormalForm() {
    if (this.kind != Kind.BINARY || (this.operator != UOperator.AND && this.operator != UOperator.OR))
      return this;

    UExpression leftExpression = requireNonNull(this.leftExpression);
    UExpression rightExpression = requireNonNull(this.rightExpression);

    UExpression leftNormalForm = leftExpression.getDisjunctiveNormalForm();
    UExpression rightNormalForm = rightExpression.getDisjunctiveNormalForm();

    if (this.operator == UOperator.OR)
      return UExpression.or(leftNormalForm, rightNormalForm);

    if (!(leftNormalForm.kind == Kind.BINARY && leftNormalForm.operator == UOperator.OR) &&
        !(rightNormalForm.kind == Kind.BINARY && rightNormalForm.operator == UOperator.OR))
      return UExpression.and(leftNormalForm, rightNormalForm);

    if ((leftNormalForm.kind == Kind.BINARY && leftNormalForm.operator == UOperator.OR)) {
      UExpression leftNormalFormLeftExpression = requireNonNull(leftNormalForm.leftExpression);
      UExpression leftNormalFormRightExpression = requireNonNull(leftNormalForm.rightExpression);
      return UExpression.or(UExpression.and(leftNormalFormLeftExpression, rightNormalForm).getDisjunctiveNormalForm(),
                            UExpression.and(leftNormalFormRightExpression, rightNormalForm).getDisjunctiveNormalForm());
    }

    UExpression rightNormalFormLeftExpression = requireNonNull(rightNormalForm.leftExpression);
    UExpression rightNormalFormRightExpression = requireNonNull(rightNormalForm.rightExpression);
    return UExpression.or(UExpression.and(leftNormalForm, rightNormalFormLeftExpression).getDisjunctiveNormalForm(),
                          UExpression.and(leftNormalForm, rightNormalFormRightExpression).getDisjunctiveNormalForm());
  }

  /**
   * Accept an expression-in-context visitor
   */
  public <T> T accept(InContextVisitor<T> visitor) {
    switch (this.kind) {
      case BOOLEAN:
        return visitor.onBooleanConstant(this.operator == UOperator.TRUE);

      case INTEGER:
        return visitor.onIntegerConstant(this.intVal);

      case STRING:
        return visitor.onStringConstant(this.strVal);

      case THIS:
        return visitor.onThis();

      case NULL:
        return visitor.onNull();

      case ID: {
        Identifier identifier = requireNonNull(this.identifier);
        UStatual statual = requireNonNull(visitor.getContext().getStatual(identifier));

        return visitor.onStatual(statual);
      }

      case ARRAY: {
        Identifier identifier = requireNonNull(this.identifier);
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UAttribute attribute = requireNonNull(visitor.getContext().getAttribute(identifier));

        return visitor.onArray(attribute, leftExpression);
      }

      case CONDITIONAL: {
        UExpression condExpression = requireNonNull(this.condExpression);
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UExpression rightExpression = requireNonNull(this.rightExpression);

        return visitor.onConditional(condExpression, leftExpression, rightExpression);
      }

      case UNARY: {
        UOperator operator = requireNonNull(this.operator);
        UExpression leftExpression = requireNonNull(this.leftExpression);

        return visitor.onUnary(operator, leftExpression);
      }

      case BINARY: {
        UOperator operator = requireNonNull(this.operator);
        UExpression leftExpression = requireNonNull(this.leftExpression);
        UExpression rightExpression = requireNonNull(this.rightExpression);

        switch (operator) {
          case ADD:
          case SUB:
          case MULT:
          case DIV:
          case MOD:
            return visitor.onArithmetical(leftExpression, operator, rightExpression);

          case LT:
          case LEQ:
          case EQ:
          case NEQ:
          case GEQ:
          case GT:
            return visitor.onRelational(leftExpression, operator, rightExpression);

          case AND:
          case OR:
            return visitor.onJunctional(leftExpression, operator, rightExpression);

          //$CASES-OMITTED$
          default:
            break;
        }
      }
    }

    throw new IllegalStateException("Unknown UML expression kind.");
  }

  /**
   * @return expression's hash code
   */
  public int hashCode() {
    return Objects.hash(this.identifier,
                        this.condExpression,
                        this.leftExpression,
                        this.rightExpression);
  }

  /**
   * @return whether object is equal to this expression
   */
  public boolean equals(java.lang.@Nullable Object object) {
    if (object == null)
      return false;

    try {
      UExpression other = (UExpression)object;
      return (this.kind == other.kind &&
              Objects.equals(this.identifier, other.identifier) &&
              Objects.equals(this.condExpression, other.condExpression) &&
              Objects.equals(this.leftExpression, other.leftExpression) &&
              Objects.equals(this.rightExpression, other.rightExpression) &&
              this.operator == other.operator &&
              this.intVal == other.intVal &&
              Objects.equals(this.strVal, other.strVal));
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case THIS: {
        resultBuilder.append("this");
        return resultBuilder.toString();
      }

      case NULL: {
        resultBuilder.append("null");
        return resultBuilder.toString();
      }

      case BOOLEAN: {
        resultBuilder.append((this.operator == UOperator.TRUE));
        return resultBuilder.toString();
      }

      case INTEGER: {
        resultBuilder.append(this.intVal);
        return resultBuilder.toString();
      }

      case STRING: {
        resultBuilder.append("\"");
        resultBuilder.append(this.strVal);
        resultBuilder.append("\"");
        return resultBuilder.toString();
      }

      case ID: {
        resultBuilder.append(this.identifier);
        return resultBuilder.toString();
      }

      case ARRAY: {
        resultBuilder.append(this.identifier);
        resultBuilder.append("[");
        resultBuilder.append(this.leftExpression);
        resultBuilder.append("]");
        return resultBuilder.toString();
      }

      case CONDITIONAL: {
        resultBuilder.append("(");
        resultBuilder.append(this.condExpression);
        resultBuilder.append(" ? ");
        resultBuilder.append(this.leftExpression);
        resultBuilder.append(" : ");
        resultBuilder.append(this.rightExpression);
        resultBuilder.append(")");
        return resultBuilder.toString();
      }

      case UNARY: {
        resultBuilder.append(requireNonNull(this.operator).getName());
        resultBuilder.append(parenthesised(requireNonNull(this.leftExpression), e -> e.hasPrecedence(this)));
        return resultBuilder.toString();
      }

      case BINARY: {
        resultBuilder.append(parenthesised(requireNonNull(this.leftExpression), e -> e.hasPrecedence(this)));
        resultBuilder.append(requireNonNull(this.operator).getName());
        resultBuilder.append(parenthesised(requireNonNull(this.rightExpression), e -> e.hasPrecedence(this)));
        return resultBuilder.toString();
      }

      default:
        return resultBuilder.toString();
    }
  } 
}
