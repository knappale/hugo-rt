package uml;

import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Contains data of an operation parameter.
 *
 * @author <A HREF="mailto:majnu@gmx.net">Timm Schäfer</A>
 * @author <A HREF="mailto:baeumlea@cip.ifi.lmu.de">Simon Bäumler</A>
*/
@NonNullByDefault
public class UParameter extends UStatual {
  private UBehavioural owner;
  private UClassifier classifier;

  /**
   * Create a parameter
   *
   * @param name parameter's name
   * @param classifier parameter's type classifier
   * @param owner parameter's owning behavioural (feature)
   */
  public UParameter(String name, UClassifier classifier, UBehavioural owner) {
    super(name);
    this.classifier = classifier;
    this.owner = owner;
  }

  @Override
  @NonNullByDefault({})
  public <T> T cases(@NonNull Function<@NonNull UAttribute, T> attributeFun,
                     @NonNull Function<@NonNull UParameter, T> parameterFun,
                     @NonNull Function<@NonNull UObject, T> objectFun) {
    return parameterFun.apply(this);
  }

  /**
   * @return parameter's owner
   */
  public UBehavioural getOwner() {
    return this.owner;
  }

  /**
   * @return parameter's classifier
   */
  public UClassifier getClassifier() {
    return this.classifier;
  }

  @Override
  public UType getType() {
    return UType.simple(this.classifier);
  }

  @Override
  public boolean isConstant() {
    return true;
  }

  @Override
  public boolean isStatic() {
    return false;
  }

  /**
   * @return this paramter's offset in the parameter list of its owner
   */
  public int getNumber() {
    return this.getOwner().getParameters().indexOf(this);
  }

  /**
   * @param prefix prefix string
   * @return parameter's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(prefix);
    resultBuilder.append(this.getName());
    resultBuilder.append(" : ");
    resultBuilder.append(this.classifier.getName());
    return resultBuilder.toString();
  }

  /**
   * @return parameter's representation in UTE format
   */
  public String declaration() {
    return this.declaration("");
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Parameter [name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append(", classifier = ");
    resultBuilder.append(this.classifier.getName());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
