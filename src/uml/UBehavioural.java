package uml;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toList;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Contains data of a UML behavioural (feature).
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class UBehavioural extends UTypedElement {
  private UClass owner;
  private List<UParameter> parameters = new ArrayList<>();
  private List<UMethod> methods = new ArrayList<>();

  @NonNullByDefault({})
  public class Cases<T> {
    private Function<@NonNull UOperation, T> operationFun = null;
    private Function<@NonNull UReception, T> receptionFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases<T> operation(Function<@NonNull UOperation, T> operationFun) {
      this.operationFun = operationFun;
      return this;
    }
    
    public Cases<T> reception(Function<@NonNull UReception, T> receptionFun) {
      this.receptionFun = receptionFun;
      return this;
    }
    
    public Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply() {
      if (UBehavioural.this instanceof UOperation && this.operationFun != null)
        return this.operationFun.apply((UOperation)UBehavioural.this);
      if (UBehavioural.this instanceof UReception && this.receptionFun != null)
        return this.receptionFun.apply((UReception)UBehavioural.this);

      assert (this.otherwiseFun != null) : "No default for case distinction on behavioural type";
      return this.otherwiseFun.get();
    }
  }

  /**
   * Make a case distinction on the sub-type.
   *
   * @param operationFun function called when this behavioural is an attribute
   * @param receptionFun function called when this behavioural is a parameter
   * @return result of applying one of the sub-type functions
   */
  @NonNullByDefault({})
  public abstract <T> T cases(@NonNull Function<@NonNull UOperation, T> operationFun,
                              @NonNull Function<@NonNull UReception, T> receptionFun);

  /**
   * Create a behavioural (feature)
   *
   * @param name behavioural (feature)'s name
   * @param owner behavioural (feature)'s owner
   */
  public UBehavioural(String name, UClass owner) {
    super(name);
    this.owner = owner;
  }

  /**
   * @return behavioural (feature)'s owning class
   */
  public UClass getOwner() {
    return this.owner;
  }

  /**
   * Add a parameter to behavioural (feature)
   *
   * @param parameter additional parameter
   */
  public void addParameter(UParameter parameter) {
    this.parameters.add(parameter);
  }

  /**
   * @return behavioural (feature)'s parameters
   */
  public List<UParameter> getParameters() {
    return this.parameters;
  }

  @Override
  public UType getType() {
    return UType.function(this.parameters.stream().map(UParameter::getType).collect(toList()), UType.simple(this.getOwner().getModel().getVoid()));
  }

  /**
   * Add a method to this behavioural (feature).
   *
   * @param method a method
   */
  public void addMethod(UMethod method) {
    this.methods.add(method);
  }

  /**
   * @return behavioural (feature)'s methods
   */
  public List<UMethod> getMethods() {
    return this.methods;
  }

  /**
   * @return whether this behavioural (feature) is static
   */
  public boolean isStatic() {
    return false;
  }

  /**
   * Determine this behavioural (feature)'s method code for a language.
   *
   * @param language a programming language
   * @return this behavioural (feature)'s method code for {@code language} or {@code null} if there is no such body
   */
  public @Nullable List<String> getMethodCode(String language) {
    language = language.toLowerCase();
    for (var method : this.methods) {
      for (var methodLanguage : method.getLanguages())
        if (methodLanguage.toLowerCase().equals(language))
          return method.getCode();
    }
    return null;
  }
    
  /**
   * @param prefix prefix string
   * @return behavioural's representation in UTE format with prefix
   */
  abstract public String declaration(String prefix);

  /**
   * @return behavioural's representation in UTE format
   */
  public String declaration() {
    return this.declaration("");
  }
}
