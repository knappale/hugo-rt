package uml;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * UML elements that have a type.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class UTypedElement extends UElement {
  /**
   * Creates a new typed UML element.
   *
   * @param name the name of the UML element
   */
  public UTypedElement(String name) {
    super(name);
  }

  /**
   * @return typed element's type
   */
  public abstract UType getType();
}
