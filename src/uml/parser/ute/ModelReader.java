package uml.parser.ute;

import java.io.*;

import org.eclipse.jdt.annotation.NonNull;

import uml.UModel;
import uml.UModelException;


/**
 * Reads data from a UTE-file.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class ModelReader extends uml.parser.ModelReader {
  private @NonNull UModel rawModel;

  public ModelReader(BufferedInputStream modelInputStream) throws UModelException {
    DEBUG.info("Parsing with UTE reader");

    uml.UModel rawModel = null;
    try {
      rawModel = uml.parser.ute.UMLParser.model(modelInputStream);
    }
    catch (Exception e) {
      String message = e.getMessage();
      if (message == null || message.equals("")) {
        StringWriter stackTraceWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stackTraceWriter, true));
        message = stackTraceWriter.toString();
      }
      throw new UModelException("UTE parsing failed: " + message);
    }
    if (rawModel == null)
      throw new UModelException("No model found in UTE input");
    this.rawModel = rawModel;
  }

  public String getModelName() {
    return rawModel.getName();
  }

  public @NonNull UModel getRawModel() {
    return rawModel;
  }
}
