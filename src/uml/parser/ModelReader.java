package uml.parser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;

import uml.UAction;
import uml.UAttribute;
import uml.UClass;
import uml.UCollaboration;
import uml.UContext;
import uml.UExpression;
import uml.UModel;
import uml.UModelException;
import uml.UObject;
import uml.USlot;
import uml.UStatual;
import uml.UType;
import uml.interaction.UInteraction;
import uml.interaction.UMessage;
import uml.interaction.UMessageOccurrenceSpecification;
import uml.interaction.UOccurrenceSpecification;
import uml.interaction.UTiming;
import uml.ocl.OConstraint;
import uml.ocl.OTCTLFormula;
import uml.statemachine.UEvent;
import uml.statemachine.UPseudoState;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UTransition;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UCompoundTransition;
import uml.testcase.UTestCase;

import static java.util.stream.Collectors.toSet;
import static util.Formatter.quoted;


/**
 * Abstract factory for reading data from an XMI-file, using Novosoft's nsuml,
 * or from an UTE-file.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public abstract class ModelReader {
  public static java.util.logging.Logger DEBUG = util.Message.debug();

  /**
   * Create a UML model reader.
   * 
   * The creator tries to determine whether the input stream contains an XMI
   * file or whether it is a UTE file. If an XMI file is assumed, the creator
   * also tries to determine which XMI type is read, either XMI 1.0, or XMI 1.1,
   * or XMI 1.2, or XMI 2.1.  The first is supported for UML 1.3, the second for UML 1.4.
   * 
   * The method for guessing the file type and, possibly, the XMI type is rather
   * crude: The instantiation of a suitable XMI reader is triggered by the first
   * string which is "1.0" or "1.1" or "1.2" or "2.1" after the string "xmi.version" in the first
   * <CODE>lookaheadNum</CODE> characters of the input stream.
   * 
   * @see uml.parser.ute.ModelReader
   * 
   * @param modelInputStream model input stream
   * @return a new XMI reader
   */
  public static ModelReader create(BufferedInputStream modelInputStream) throws UModelException {
    String xmiVersion = "";

    final int lookaheadNum = 1 << 18; // -AK seems to be the minimum possible, when working with marks
    try {
      // Set a mark that will be valid up to reading lookaheadNum
      // times such that we can reset to start of stream
      modelInputStream.mark(lookaheadNum);
      BufferedReader modelReader = new BufferedReader(new InputStreamReader(modelInputStream));
      char[] lookaheadChars = new char[lookaheadNum];
      for (int i = 0; i < lookaheadNum; i++) {
        int c = modelReader.read();
        // End of stream reached
        if (c == -1)
          break;
        lookaheadChars[i] = (char)c;
      }
      String lookahead = new String(lookaheadChars);
      // String xmiVersionPrefix = "(?s).*XMI\\p{Space}*xmi.version\\p{Space}*=\\p{Space}*[\"\']";
      String xmiVersionPrefix = "(?s).*xmi(\\.|:)version\\p{Space}*=\\p{Space}*[\"\']";
      String xmiVersionSuffix = "[\"\'].*";
      if (java.util.regex.Pattern.matches(xmiVersionPrefix + "1\\.0" + xmiVersionSuffix, lookahead)) {
        xmiVersion = "1.0";
      }
      else {
        if (java.util.regex.Pattern.matches(xmiVersionPrefix + "1\\.1" + xmiVersionSuffix, lookahead)) {
          xmiVersion = "1.1";
        }
        else {
          if (java.util.regex.Pattern.matches(xmiVersionPrefix + "1\\.2" + xmiVersionSuffix, lookahead)) {
            xmiVersion = "1.2";
          }
          else {
            if (java.util.regex.Pattern.matches(xmiVersionPrefix + "2\\.1" + xmiVersionSuffix, lookahead)) {
              xmiVersion = "2.1";
            }
            else {
              if (java.util.regex.Pattern.matches(xmiVersionPrefix + "201.*" + xmiVersionSuffix, lookahead)) {
                xmiVersion = "2.1";
              }
            }
          }
        }
      }
      modelInputStream.reset();

      if ("".equals(xmiVersion)) {
        return new uml.parser.ute.ModelReader(modelInputStream);
      }

      if ("2.1".equals(xmiVersion)) {
        return new uml.parser.xmi.uml23xmi21.ModelReader(modelInputStream);
      }

      if ("1.0".equals(xmiVersion) ||
          "1.1".equals(xmiVersion) ||
          "1.2".equals(xmiVersion)) {
        try {
          modelInputStream.mark(1 << 31);
          return new uml.parser.xmi.ModelReader(modelInputStream, xmiVersion);
        }
        catch (UModelException me) {
          util.Message.debug().info("DOM-based XMI parsing failed with model exception: " + me);
        }
      }
    }
    catch (IOException ioe) {
      util.Message.debug().info("Exception: " + ioe);
    }

    throw new UModelException("Unable to determine input file type");
  }
  
  /**
   * Determine the read-in model
   * 
   * @return model
   * @throws UModelException when an error occurs in statically checking the model
   */
  public final @NonNull UModel getModel() throws UModelException {
    UModel model = getRawModel();
    check(model);
    return model;
  }
  
  /**
   * Determine the name of the model
   * 
   * @return model name
   */
  public abstract String getModelName();
  
  /**
   * Determine the read-in, unchecked model
   */
  public abstract @NonNull UModel getRawModel() throws UModelException;
  
  /**
   * Check a model.
   */
  public void check(UModel model) throws UModelException {
    for (UClass hclass1 : model.getClasses()) {
      for (UClass hclass2 : model.getClasses()) {
        if (hclass1 == hclass2)
          continue;
        if (hclass1.getName().equals(hclass2.getName()))
          throw new UModelException("Two classes with the same name " + quoted(hclass1.getName()));
      }
    }

    for (UClass uClass : model.getClasses()) {
      UStateMachine stateMachine = uClass.getStateMachine();
      if (stateMachine == null)
        continue;
      String stateMachineContext = "In state machine for class " + quoted(uClass.getName());

      for (UVertex uVertex : stateMachine.getVertices()) {
        for (UVertex otherUVertex : stateMachine.getVertices()) {
          if (!uVertex.equals(otherUVertex) && uVertex.getIdentifier().toString().equals(otherUVertex.getIdentifier().toString()))
            throw new UModelException(stateMachineContext + ": two different vertices with same full name " + quoted(uVertex.getIdentifier()));
        }
      }

      for (UState state : stateMachine.getVertices().stream().filter(v -> v instanceof UState).map(v -> (@NonNull UState)v).collect(toSet())) {
        String stateContext = stateMachineContext + ", state " + quoted(state.getIdentifier());
        if (state.hasEntryAction()) {
          UAction entry = state.getEntryAction();
          String entryContext = stateContext + ", entry " + quoted(entry);

          UContext context = state.getEntryContext();
          UType entryType = entry.getType(context);
          if (entryType.isError())
            throw new UModelException(entryContext + ": " + entryType.getErrorMessage());
          if (!context.getVoidType().subsumes(entryType))
            throw new UModelException(entryContext + ": expected type " + quoted(context.getVoidType().getName()) + ", found " + quoted(entryType.getName()));
        }
        if (state.hasExitAction()) {
          UAction exit = state.getExitAction();
          String exitContext = stateContext + ", exit " + quoted(exit);

          UContext context = state.getExitContext();
          UType exitType = exit.getType(context);
          if (exitType.isError())
            throw new UModelException(exitContext + ": " + exitType.getErrorMessage());
          if (!context.getVoidType().subsumes(exitType))
            throw new UModelException(exitContext + ": expected type " + quoted(context.getVoidType().getName()) + ", found " + quoted(exitType.getName()));
        }
      }

      // Check that triggers are owned by the context class
      for (var uTransition : stateMachine.getTransitions()) {
        UEvent uEvent = uTransition.getTrigger();
        if (!uEvent.new Cases<Boolean>().
               signal(uSignal -> uClass.getReception(uSignal) != null).
               call(uOperation -> uOperation.getOwner().equals(uClass)).
               otherwise(() -> true).
               apply())
          throw new UModelException(stateMachineContext + ": transition " + quoted(uTransition.getName()) + " from " + quoted(uTransition.getSource().getIdentifier()) + " to " + quoted(uTransition.getTarget().getIdentifier()) + " has unsatisfiable trigger " + quoted(uEvent.getName()));
      }
      
      for (UTransition transition : stateMachine.getTransitions()) {
        UEvent event = transition.getTrigger();
        if (!event.isCompletion())
          continue;
        
        if (!transition.getSource().isCompletable())
          throw new UModelException(stateMachineContext + ": state " + quoted(transition.getSource().getIdentifier()) + " has a completion transition, but is not completable");
      }
      
      for (UTransition transition : stateMachine.getTransitions()) {
        UEvent event = transition.getTrigger();
        if (!event.isTime())
          continue;
        
        UContext context = event.getTimeContext();

        UExpression timeLowExpression = event.getTimeLowExpression();
        String timeLowExpressionContext = stateMachineContext + ", lower time bound " + quoted(timeLowExpression);
        
        UType timeLowExpressionType = timeLowExpression.getType(context);
        if (timeLowExpressionType.isError())
          throw new UModelException(timeLowExpressionContext + ": " + timeLowExpressionType.getErrorMessage());
        if (!context.getIntegerType().subsumes(timeLowExpressionType))
          throw new UModelException(timeLowExpressionContext + ": expected type " + quoted(context.getIntegerType().getName()) + ", found " + quoted(timeLowExpressionType.getName()));
        
        UExpression timeHighExpression = event.getTimeHighExpression();
        String timeHighExpressionContext = stateMachineContext + ", upper time bound " + quoted(timeHighExpression);

        UType timeHighExpressionType = timeHighExpression.getType(context);
        if (timeHighExpressionType.isError())
          throw new UModelException(timeHighExpressionContext + ": " + timeHighExpressionType.getErrorMessage());
        if (!context.getIntegerType().subsumes(timeHighExpressionType))
          throw new UModelException(timeHighExpressionContext + ": expected type " + quoted(context.getIntegerType().getName()) + ", found " + quoted(timeHighExpressionType.getName()));
      }
      
      for (UTransition transition : stateMachine.getTransitions()) {
        UContext context = transition.getGuardContext();

        UExpression guard = transition.getGuard();
        String guardContext = stateMachineContext + ", guard " + quoted(guard);
        UType guardType = guard.getType(context);
        if (guardType.isError())
          throw new UModelException(guardContext + ": " + guardType.getErrorMessage());
        if (!context.getBooleanType().subsumes(guardType))
          throw new UModelException(guardContext + ": expected type " + quoted(context.getBooleanType().getName()) + ", found " + quoted(guardType.getName()));
      }
      
      for (UTransition transition : stateMachine.getTransitions()) {
        UContext context = transition.getEffectContext();

        UAction effect = transition.getEffect();
        String effectContext = stateMachineContext + ", effect " + quoted(effect);
        UType effectType = effect.getType(context);
        if (effectType.isError())
          throw new UModelException(effectContext + ": " + effectType.getErrorMessage());
        if (!context.getVoidType().subsumes(effectType))
          throw new UModelException(effectContext + ": expected type " + quoted(context.getVoidType().getName()) + ", found " + quoted(effectType.getName()));
      }

      // Check for default entries
      for (URegion region : stateMachine.getRegions())
        if (region.getInitial() == null)
          throw new UModelException(stateMachineContext + ": Top-level region " + quoted(region.getIdentifier()) + " has no initial state");
      for (UState state : stateMachine.getVertices().stream().filter(v -> v instanceof UState).map(v -> (@NonNull UState)v).collect(toSet())) {
        if (!state.getIncomings().isEmpty() && state.getRegions().stream().anyMatch(region -> region.getInitial() == null))
          throw new UModelException(stateMachineContext + ": Composite state " + quoted(state.getIdentifier()) + " has incoming transitions, but no default entry");
        for (UCompoundTransition compoundTransition : state.getOutgoingCompounds()) {
          @NonNull Set<@NonNull URegion> improperlyEnteredRegions = compoundTransition.getImproperlyEnteredRegions();
          if (!improperlyEnteredRegions.isEmpty()) {
            URegion improperlyEnteredRegion = improperlyEnteredRegions.iterator().next();
            throw new UModelException(stateMachineContext + ": Outgoing compound transition " + quoted(compoundTransition.getFullName()) + " from state " + quoted(state.getIdentifier()) + " also enters region " + quoted(improperlyEnteredRegion.getIdentifier()) + " which has no initial state");
          }
        }
      }

      // Check incoming and outgoing transitions of initial and history pseudo-states
      for (UVertex vertex : stateMachine.getVertices()) {
        if (vertex.getKind() == UPseudoState.Kind.INITIAL) {
          if (vertex.getIncomings().size() != 0)
            throw new UModelException(stateMachineContext + ": Initial pseudo-state of region " + quoted(vertex.getContainer().getIdentifier()) + " has an incoming transition.");
        }
        if (vertex.getKind() == UPseudoState.Kind.SHALLOWHISTORY ||
            vertex.getKind() == UPseudoState.Kind.DEEPHISTORY) {
          if (vertex.getOutgoings().size() > 1)
            throw new UModelException(stateMachineContext + ": History pseudo-state of region " + quoted(vertex.getContainer().getIdentifier()) + " has more than one outgoing transition.");
        }
      }

      // Check whether states a fork targets to or a join sources from are contained in concurrent regions
      for (UVertex vertex : stateMachine.getVertices()) {
        if (vertex.getKind() == UPseudoState.Kind.FORK) {
          for (UCompoundTransition compoundTransition : vertex.getOutgoingCompounds()) {
            @NonNull Set<@NonNull UVertex> targets = compoundTransition.getTargets();
            if (targets.stream().anyMatch(target -> target.getContainer().getLeastOrthogonal() == null))
              throw new UModelException(stateMachineContext + ": The outgoing transition " + quoted(compoundTransition.getFullName()) + " from fork pseudo-state " + quoted(vertex.getIdentifier()) + " do not target vertices in orthogonal regions.");
          }
        }
        if (vertex.getKind() == UPseudoState.Kind.JOIN) {
          if (vertex.getIncomings().stream().anyMatch(transition -> transition.getSource().getContainer().getLeastOrthogonal() == null))
            throw new UModelException(stateMachineContext + ": An incoming transition of join pseudo-state " + quoted(vertex.getIdentifier()) + " does not originate from an orthogonal region.");
        }
      }

      // Check for non-pseudo triggers on transitions leaving a pseudo-state
      for (UVertex vertex : stateMachine.getVertices()) {
        if (vertex.getKind() != null) {
          for (UTransition transition : vertex.getOutgoings()) {
            if (!transition.getTrigger().isPseudo())
              throw new UModelException(stateMachineContext + ": Outgoing transition of pseudo state " + quoted(vertex.getName()) + " shows trigger " + quoted(transition.getTrigger().declaration()));
          }
        }
      }
    }
    
    for (UClass uClass : model.getClasses()) {
      // TODO (AK060311) Check on polymorphic operations and receptions
      
      for (UAttribute attribute : uClass.getAttributes()) {
        String attributeContext = "In attribute " + quoted(attribute.getName()) + " of class " + quoted(attribute.getOwner().getName());
        UType attributeType = attribute.getType();
        
        // Check uniqueness of name
        for (UAttribute otherAttribute : uClass.getAttributes()) {
          if ((!attribute.equals(otherAttribute)) && (attribute.getName().equals(otherAttribute.getName())))
            throw new UModelException("Two attributes with same name " + quoted(attribute.getName()) + " in class " + quoted(attribute.getOwner().getName()));
        }
        
        // Check upper bound
        {
          UContext context = attribute.getUpperContext();

          UExpression upper = attribute.getUpper();
          String upperContext = attributeContext + ", upper bound " + quoted(upper);
          UType upperType = upper.getType(context);
          if (upperType.isError())
            throw new UModelException(upperContext + ": " + upperType.getErrorMessage());
          if (!context.getIntegerType().subsumes(upperType))
            throw new UModelException(upperContext + ": expected type " + quoted(context.getIntegerType().getName()) + ", found: " + quoted(upperType.getName()));
          if (!upper.isConstant(context))
            throw new UModelException(upperContext + ": not a constant");
          if (attribute.getSize() <= 0)
            throw new UModelException(upperContext + ": not positive");
        }
        
        if (attribute.hasInitialValues()) {
          // Check number of initial values
          if (attribute.getSize() < attribute.getInitialValues().size())
            throw new UModelException(attributeContext + ": wrong number of initial values, expected less or equal " + attribute.getSize() + ", found " + attribute.getInitialValues().size());
          
          // Check initial values
          UContext context = attribute.getInitialValuesContext();
          UType slotType = attributeType;
          if (attributeType.isArray())
            slotType = attributeType.getUnderlyingType();
          for (UExpression initialValue : attribute.getInitialValues()) {
            String initialValueContext = attributeContext + ", initial value " + quoted(initialValue);
            UType initialValueType = initialValue.getType(context);
            if (initialValueType.isError())
              throw new UModelException(initialValueContext + ": " + initialValueType.getErrorMessage());
            if (!slotType.subsumes(initialValueType))
              throw new UModelException(initialValueContext + ": expected type " + quoted(attributeType.getName()) + ", found: " + quoted(initialValueType.getName()));
            
            // There must be no cyclic dependencies for initial values
            @NonNull Set<@NonNull UStatual> dependencies = initialValue.getDependencies(context);
            if (dependencies.contains(attribute))
              throw new UModelException(initialValueContext + ": cyclic dependency");
            
            // Static attributes may only depend on other static attributes (and constants)
            if (attribute.isStatic()) {
              for (UStatual statual : dependencies) {
                if (!statual.isStatic())
                  throw new UModelException(initialValueContext + ": dependency on instance feature " + quoted(statual.getName()));
              }
            }
          }
        }
      }
    }
    
    for (UCollaboration collaboration : model.getCollaborations()) {
      for (UObject object : collaboration.getObjects()) {
        for (USlot slot : object.getSlots()) {
          String slotContext = "In slot " + quoted(slot.getName()) + " of object " + quoted(object.getName()) + " of class " + quoted(object.getC1ass().getName());
          // Check number of values
          if (slot.getValues().size() > slot.getAttribute().getUpper().getIntegerValue(slot.getAttribute().getUpperContext()))
            throw new UModelException(slotContext + ": too many initialisation values");
          
          // Check values
          UContext context = slot.getValuesContext();
          UType slotType = slot.getAttribute().getType();
          if (slotType.isArray())
            slotType = slot.getAttribute().getType().getUnderlyingType();
          for (UExpression value : slot.getValues()) {
            String valueContext = slotContext + ", value " + quoted(value);
            UType valueType = value.getType(context);
            if (valueType.isError())
              throw new UModelException(valueContext + ": " + valueType.getErrorMessage());
            if (!slotType.subsumes(valueType))
              throw new UModelException(valueContext + ": expected type " + quoted(slotType.getName()) + ", found: " + quoted(valueType.getName()));
          }
        }
      }
      
      for (UInteraction interaction : collaboration.getInteractions()) {
        for (UMessage message : interaction.getMessages()) {
          UContext context = interaction.getMessageContext();
          String messageContext = "In message " + quoted(message) + " of interaction " + quoted(interaction.getName());
          UType messageType = message.getType(context);
          if (messageType.isError())
            throw new UModelException(messageContext + ": " + messageType.getErrorMessage());
          // Ignore actual type of message
        }
        
        for (UOccurrenceSpecification occurrence : interaction.getOccurrences()) {
          if (occurrence instanceof UMessageOccurrenceSpecification) {
            UMessageOccurrenceSpecification messageOccurrence = (UMessageOccurrenceSpecification)occurrence;
            String messageOccurrenceContext = "In message occurrence " + quoted(messageOccurrence) + " of interaction " + quoted(interaction.getName());
            UObject activeObject = messageOccurrence.getActiveObject(); 
            if (activeObject != null) {
              UObject lifelineObject = messageOccurrence.getLifeline().getObject(); 
              if (!lifelineObject.equals(activeObject)) {
                throw new UModelException(messageOccurrenceContext + ": expected active object: " + quoted( activeObject.getName()) + ", found: " + quoted(lifelineObject.getName()));
              }
            }
          }
        }

        // TODO (AK060414) Check that all message names in an ignore-CombinedFragment denote some behavioural

        for (OTCTLFormula assertion : interaction.getAssertions()) {
          String assertionContext = "In assertion " + quoted(assertion);
          UContext context = interaction.getAssertionContext();
          UType assertionType = assertion.getType(context);
          if (assertionType.isError())
            throw new UModelException(assertionContext + ": " + assertionType.getErrorMessage());
          if (!context.getBooleanType().subsumes(assertionType))
            throw new UModelException(assertionContext + ": expected type " + quoted(context.getBooleanType().getName()) + ", found: " + quoted(assertionType.getName()));
        }
        
        for (UTiming timing : interaction.getTimings()) {
          String timingContext = "In timing " + quoted(timing);
          UContext context = timing.getBoundContext();
          UType boundType = timing.getBound().getType(context);
          if (boundType.isError())
            throw new UModelException(timingContext + ": " + boundType.getErrorMessage());
          if (!context.getIntegerType().subsumes(boundType))
            throw new UModelException(timingContext + ": expected type " + quoted(context.getIntegerType().getName()) + ", found: " + quoted(boundType.getName()));
        }
      }
      
      for (OConstraint constraint : collaboration.getConstraints()) {
        String constraintContext = "In constraint " + quoted(constraint);
        UContext context = collaboration.getConstraintsContext();
        UType constraintType = constraint.getType(context);
        if (constraintType.isError())
          throw new UModelException(constraintContext + ": " + constraintType.getErrorMessage());
        if (!context.getBooleanType().subsumes(constraintType))
          throw new UModelException(constraintContext + ": expected type " + quoted(context.getBooleanType().getName()) + ", found " + quoted(constraintType.getName()));
      }

      for (UTestCase testCase : collaboration.getTestCases()) {
        String testCaseContext = "In test case " + quoted(testCase.getName());
        UContext context = collaboration.getTestCaseContext();
        UType testCaseType = testCase.getType(context);
        if (testCaseType.isError())
          throw new UModelException(testCaseContext + ": " + testCaseType.getErrorMessage());
      }
    }
  }
}
