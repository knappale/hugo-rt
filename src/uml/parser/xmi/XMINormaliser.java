package uml.parser.xmi;

import java.util.*;

import uml.UModelException;
import util.Message;


/**
 * Normalise an XMI model.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class XMINormaliser {
  private uml.parser.xmi.metamodel.Model xmiModel;

  private XMINormaliser(uml.parser.xmi.metamodel.Model xmiModel) {
    this.xmiModel = xmiModel;
  }

  /**
   * Normalise an XMI model representation.
   *
   * @param xmiModel an XMI model representation
   * @return a normalised XMI model representation
   * @throws UModelException
   */
  public static uml.parser.xmi.metamodel.Model normalise(uml.parser.xmi.metamodel.Model xmiModel) throws UModelException {
    XMINormaliser normaliser = new XMINormaliser(xmiModel);
    normaliser.normaliseClasses();
    normaliser.normaliseStateMachines();
    normaliser.normaliseCollaborations();
    normaliser.normaliseInteractions();
    normaliser.normaliseLinks();
    normaliser.normaliseConstraints();
    return xmiModel;
  }

  /**
   * Normalise classes.
   * <UL>
   * <LI>An operation with name "receive" becomes a Reception and a corresponding
   * signal is created, if it shows a parameter with parameter name and parameter type
   * equals (lower-cased).
   * <LI>Classes representing a signal are removed.
   * <LI>Anonymous association ends are given the name of their type with the
   * first letter lower-cased.
   * <LI>Every association end without a multiplicity gets
   * multiplicity "0..1".
   * </UL>
   */
  private void normaliseClasses() throws UModelException {
    Set<uml.parser.xmi.metamodel.Class> removeXMIClasses = new HashSet<uml.parser.xmi.metamodel.Class>();
    int xmiClassNameCounter = 0;

    for (uml.parser.xmi.metamodel.Class xmiClass : this.xmiModel.getClasses()) {
      if (xmiClass.getName().trim().equals("")) {
        xmiClass.setName("C" + xmiClassNameCounter++);
        Message.info("Set class name from empty to `" + xmiClass.getName() + "'");
      }

      // Normalise operations
      Set<uml.parser.xmi.metamodel.Operation> removeXMIOperations = new HashSet<uml.parser.xmi.metamodel.Operation>();

      for (uml.parser.xmi.metamodel.Operation xmiOperation : xmiClass.getOperations()) {
        if (xmiOperation.getName().trim().toLowerCase().equals("receive")) {
          // Seek for signal parameter, which must be the first
          // parameter that is not of kind RETURN or OUT
          uml.parser.xmi.metamodel.Parameter xmiSignalParameter = null;
          Iterator<uml.parser.xmi.metamodel.Parameter> xmiParametersIt = xmiOperation.getParameters().iterator();
          while (xmiParametersIt.hasNext()) {
            uml.parser.xmi.metamodel.Parameter xmiParameter = xmiParametersIt.next();            
            if (xmiParameter.getKind() != uml.parser.xmi.metamodel.ParameterDirectionKind.RETURN && xmiParameter.getKind() != uml.parser.xmi.metamodel.ParameterDirectionKind.OUT) {
              xmiSignalParameter = xmiParameter;
              break;
            }
          }
          // Has a parameter been found which can be interpreted as a signal,
          // i.e., its name equals its type name (lower-cased)?
          if (xmiSignalParameter == null || xmiSignalParameter.getName() == null || xmiSignalParameter.getName().equals("") || xmiSignalParameter.getType() == null) {
            continue;
            // throw new ModelException("There is no non-return and non-out parameter in a receive operation in class `" + xmiClass.getName() + "' that can be interpreted as a signal");
          }
          if (!xmiSignalParameter.getName().trim().toLowerCase().equals(xmiSignalParameter.getType().getName().trim().toLowerCase()))
            continue;

          List<uml.parser.xmi.metamodel.Parameter> xmiReceptionParameters = new ArrayList<uml.parser.xmi.metamodel.Parameter>();
          while (xmiParametersIt.hasNext()) {
            uml.parser.xmi.metamodel.Parameter parameter = xmiParametersIt.next();
            if (parameter.getKind() != uml.parser.xmi.metamodel.ParameterDirectionKind.RETURN && parameter.getKind() != uml.parser.xmi.metamodel.ParameterDirectionKind.OUT) {
              xmiReceptionParameters.add(parameter);
            }
          }
          // Does a signal with the name of xmiSignalParameter already exist?
          uml.parser.xmi.metamodel.Signal xmiReceptionSignal = null;
          for (uml.parser.xmi.metamodel.Signal xmiSignal : this.xmiModel.getSignals()) {
            if (xmiSignal.getName().equals(xmiSignalParameter.getType().getName())) {
              xmiReceptionSignal = xmiSignal;
              break;
            }
          }
          if (xmiReceptionSignal == null) {
            // Create a new signal with parameters as attributes
            xmiReceptionSignal = this.xmiModel.createSignal(xmiSignalParameter.getName());
            for (uml.parser.xmi.metamodel.Parameter xmiReceptionParameter : xmiReceptionParameters)
              xmiReceptionSignal.createAttribute(xmiReceptionParameter.getName(), xmiReceptionParameter.getType());
            if (xmiSignalParameter.getType() instanceof uml.parser.xmi.metamodel.Class)
              removeXMIClasses.add((uml.parser.xmi.metamodel.Class)xmiSignalParameter.getType());
            Message.info("Added signal `" + xmiReceptionSignal.getName() + "'");
          }
          else {
            String oldXMISignalName = xmiReceptionSignal.getName();
            xmiReceptionSignal.setName(xmiSignalParameter.getName().trim());
            if (!oldXMISignalName.equals(xmiReceptionSignal.getName()))
              Message.info("Changed name of signal `" + oldXMISignalName + "' to `" + xmiReceptionSignal.getName() + "'");
          }
          removeXMIOperations.add(xmiOperation);
          uml.parser.xmi.metamodel.Reception xmiReception = xmiClass.createReception(xmiReceptionSignal.getName(), xmiReceptionSignal);
          for (uml.parser.xmi.metamodel.Parameter xmiReceptionParameter : xmiReceptionParameters)
            xmiReception.addParameter(xmiReceptionParameter);
          Message.info("Added reception `" + xmiReception.getName() + "' to class `" + xmiClass.getName() + "'");
        }
      }

      for (uml.parser.xmi.metamodel.Operation xmiOperation : removeXMIOperations) {
        xmiClass.removeOperation(xmiOperation);
        Message.info("Removed operation `" + xmiOperation.getName() + "' from classifier `" + xmiClass.getName() + "'");
      }
    }

    for (uml.parser.xmi.metamodel.Class xmiClass : removeXMIClasses) {
      this.xmiModel.removeClass(xmiClass);
      Message.info("Removed class `" + xmiClass.getName() + "'");
    }

    for (uml.parser.xmi.metamodel.Class xmiClass : this.xmiModel.getClasses()) {
      // Check whether all reception parameters are attributes of corresponding signal
      for (uml.parser.xmi.metamodel.Reception xmiReception : xmiClass.getReceptions()) {
        uml.parser.xmi.metamodel.Signal xmiReceptionSignal = xmiReception.getSignal();
        for (uml.parser.xmi.metamodel.Parameter xmiReceptionParameter : xmiReception.getParameters()) {
          boolean parameterFound = false;
          for (uml.parser.xmi.metamodel.Attribute xmiSignalAttribute : xmiReceptionSignal.getAttributes()) {
            if (xmiSignalAttribute.getName().equals(xmiReceptionParameter.getName()) && xmiSignalAttribute.getType().equals(xmiReceptionParameter.getType())) {
              parameterFound = true;
              break;
            }
          }
          if (!parameterFound)
            throw new UModelException("Reception `" + xmiReception.getName() + "' in class `" + xmiClass.getName() + "' uses parameter `" + xmiReceptionParameter.getName() + "' which is not part of signal `" + xmiReceptionSignal.getName() + "'");
        }
      }

      // Normalise opposite association end names and multiplicities
      for (uml.parser.xmi.metamodel.AssociationEnd oppositeXMIAssociationEnd : xmiClass.getOppositeAssociationEnds()) {
        if (oppositeXMIAssociationEnd.getType() instanceof uml.parser.xmi.metamodel.DataType)
          throw new UModelException("Opposite association end `" + oppositeXMIAssociationEnd.getName() + "' of class `" + xmiClass.getName() + "' has data type `" + oppositeXMIAssociationEnd.getType().getName() + "'");
        if (oppositeXMIAssociationEnd.getName() == null || oppositeXMIAssociationEnd.getName().equals("")) {
          String oppositeClassName = oppositeXMIAssociationEnd.getType().getName();
          String oppositeXMIAssociationEndName = oppositeClassName.substring(0, 1).toLowerCase() + oppositeClassName.substring(1);
          
          // Look for existing names
          List<String> names = new ArrayList<String>();
          for (uml.parser.xmi.metamodel.AssociationEnd otherOppositeXMIAssociationEnd : xmiClass.getOppositeAssociationEnds())
            names.add(otherOppositeXMIAssociationEnd.getName());
          for (uml.parser.xmi.metamodel.Attribute otherXMIAttribute : xmiClass.getAttributes())
            names.add(otherXMIAttribute.getName());
          int nameCounter = 0;
          String tmpName = oppositeXMIAssociationEndName;
          while (names.contains(tmpName))
            tmpName = oppositeXMIAssociationEndName + nameCounter++;
          oppositeXMIAssociationEndName = tmpName;
          oppositeXMIAssociationEnd.setName(oppositeXMIAssociationEndName);
          Message.info("Set association end name at class `" + oppositeXMIAssociationEnd.getType().getName() + "' from empty to `" + oppositeXMIAssociationEnd.getName() + "'");
        }
        if (oppositeXMIAssociationEnd.getUpperMultiplicity() <= 0) {
          oppositeXMIAssociationEnd.setUpperMultiplicity(1);
          Message.info("Set association end upper multiplicity of `" + oppositeXMIAssociationEnd.getName() + "' at class `" + oppositeXMIAssociationEnd.getType().getName() + "' from empty to `" + oppositeXMIAssociationEnd.getUpperMultiplicity() + "'");
        }
      }

      // Normalise attributes
      for (uml.parser.xmi.metamodel.Attribute attribute : xmiClass.getAttributes()) {
        if (attribute.getUpperMultiplicity() <= 0) {
          attribute.setUpperMultiplicity(1);
          Message.info("Set attribute upper multiplicity of `" + attribute.getName() + "' in class `" + xmiClass.getName() + "' from empty to `" + attribute.getUpperMultiplicity() + "'");
        }
      }

      // Normalise behaviours
      if (xmiClass.getBehaviours().size() > 1)
        throw new UModelException(xmiClass.getName() + " has more than one state machine");
    }
  }

  /**
   * Normalise a state machine.
   * <UL>
   * <LI>If a composite state carries "(concurrent)" in its name then set the isConcurrent
   * (NSUML-speak: isConcurent) attribute and remove "(concurrent)" from the name.
   * <LI>If a (simple) state carries "(choice)" in its name then replace it by a choice
   * pseudo-state and remove "(concurrent)" from the name.
   * <LI>If a state carries no name (getName() == null || getName().equals("")) give it a unique name.
   * <LI>All final states of a composite state are unified into a single one.
   * <LI>Empty state actions (entry, exit, doActivity) are replaced by no such action (i.e. null).
   * <LI>Empty guards (<CODE>getGuard().getExpression() == null ||
   *                         getGuard().getExpression().getBody().equals("")</CODE>)
   * are replaced by no guard at all (i.e. null).
   * <LI>All call events with the lower-cased name of a signal are turned into a signal event.
   * <LI>All call events with a name starting with "after" are turned into time events.
   * <LI>All deferrable events are linked to an operation or a signal.
   * <LI>Check whether initial and history states have at most one outgoing transition.
   * </UL>
   *
   * <P>-AK what does it mean that an event may show parameters (not arguments)?
   */
  private void normaliseStateMachines() throws UModelException {
    for (uml.parser.xmi.metamodel.Class xmiClass : this.xmiModel.getClasses()) {
      for (uml.parser.xmi.metamodel.StateMachine xmiStateMachine : xmiClass.getBehaviours()) {
        if (xmiStateMachine.getTop() == null)
          throw new uml.UModelException("State machine of class `" + xmiClass.getName() + "' has no top state");
        xmiStateMachine.getTop().setName("top");
        Message.info("Set name of top state of state machine for class `" + xmiClass.getName() + "' to `top'");
        normaliseConcurrentStates(xmiClass, xmiStateMachine.getTop());
        normaliseChoiceStates(xmiClass, xmiStateMachine.getTop());
        normaliseStateNames(xmiClass, xmiStateMachine.getTop(), new Hashtable<Object, Integer>());
        normaliseFinalStates(xmiClass, xmiStateMachine.getTop());
        normaliseDeferrableEvents(xmiClass, xmiStateMachine.getTop());
        normaliseStateActions(xmiClass, xmiStateMachine.getTop());
        normaliseTriggers(xmiClass, xmiStateMachine.getTransitions());
        normaliseGuards(xmiStateMachine.getTransitions());
        normaliseEffects(xmiStateMachine.getTransitions());
      }
    }
  }

  private void normaliseConcurrentStates(uml.parser.xmi.metamodel.Class xmiClass, uml.parser.xmi.metamodel.State xmiState) {
    if (xmiState.isComposite()) {
      String xmiStateName = xmiState.getName();
      int concurrentIndex = (xmiStateName == null ? -1 : xmiStateName.indexOf("(concurrent)"));
      if (concurrentIndex != -1) {
        xmiState.setConcurrent(true);
        xmiState.setName((xmiState.getName().substring(0, concurrentIndex) + xmiState.getName().substring(concurrentIndex + "(concurrent)".length())).trim());
        Message.info("Set state `" + xmiStateName + "' of state machine for class `" + xmiClass.getName() + "' to concurrent");
      }
      for (uml.parser.xmi.metamodel.State subXMIState : xmiState.getSubStates())
        normaliseConcurrentStates(xmiClass, subXMIState);
    }
  }

  private void normaliseChoiceStates(uml.parser.xmi.metamodel.Class xmiClass, uml.parser.xmi.metamodel.State xmiState) {
    String xmiStateName = xmiState.getName();
    int choiceIndex = (xmiStateName == null ? -1 : xmiStateName.indexOf("(choice)"));
    if (choiceIndex != -1) {
      xmiState.setKind(uml.parser.xmi.metamodel.StateKind.CHOICE);
      xmiState.setName((xmiState.getName().substring(0, choiceIndex) + xmiState.getName().substring(choiceIndex + "(choice)".length())).trim());
      Message.info("Replaced state `" + xmiStateName + "' of state machine for class `" + xmiClass.getName() + "' by a choice pseudo-state");
    }

    if (xmiState.isComposite()) {
      for (uml.parser.xmi.metamodel.State subXMIState : xmiState.getSubStates())
        normaliseChoiceStates(xmiClass, subXMIState);
    }
  }

  private void normaliseFinalStates(uml.parser.xmi.metamodel.Class xmiClass, uml.parser.xmi.metamodel.State xmiState) {
    if (xmiState.isComposite()) {
      Set<uml.parser.xmi.metamodel.State> finalXMIStates = new HashSet<uml.parser.xmi.metamodel.State>();
      for (uml.parser.xmi.metamodel.State subXMIState : xmiState.getSubStates()) {
        if (subXMIState.getKind() == uml.parser.xmi.metamodel.StateKind.FINAL)
          finalXMIStates.add(subXMIState);
      }
      if (finalXMIStates.size() > 1) {
        Iterator<uml.parser.xmi.metamodel.State> finalXMIStatesIt = finalXMIStates.iterator();
        List<uml.parser.xmi.metamodel.State> oldFinalXMIStates = new ArrayList<uml.parser.xmi.metamodel.State>();
        uml.parser.xmi.metamodel.State finalXMIState = finalXMIStatesIt.next();
        while (finalXMIStatesIt.hasNext()) {
          uml.parser.xmi.metamodel.State oldFinalXMIState = finalXMIStatesIt.next();
          oldFinalXMIStates.add(oldFinalXMIState);
          while (!oldFinalXMIState.getIncomings().isEmpty()) {
            uml.parser.xmi.metamodel.Transition incomingXMITransition = oldFinalXMIState.getIncomings().get(0);
            incomingXMITransition.setTarget(finalXMIState);
          }
        }
        for (uml.parser.xmi.metamodel.State oldFinalXMIState : oldFinalXMIStates)
          oldFinalXMIState.getContainer().removeSubState(oldFinalXMIState);
        Message.info("Unified final states in `" + xmiState.getName() + "' of state machine for class `" + xmiClass.getName() + "' to final state `" + finalXMIState.getName() + "'");
      }
      for (uml.parser.xmi.metamodel.State subXMIState : xmiState.getSubStates())
        normaliseFinalStates(xmiClass, subXMIState);
    }
  }

  private void normaliseStateNames(uml.parser.xmi.metamodel.Class xmiClass, uml.parser.xmi.metamodel.State xmiState, Hashtable<Object, Integer> stateNameCounters) {
    if (xmiState.getName() == null || xmiState.getName().equals("")) {
      Object stateKey = null;
      String statePrefix = (xmiState.getContainer() != null ? xmiState.getContainer().getName() : "") + "_";

      stateKey = xmiState.getKind();
      statePrefix += xmiState.getKind().toString();
      String stateClassName = "";
      if (xmiState.isComposite() && (xmiState.getContainer() != null && xmiState.getContainer().isConcurrent())) {
        stateClassName = "region";
      }
      else
        if (xmiState.isComposite() && xmiState.isConcurrent()) {
          stateClassName = "concurrent";
        }
      statePrefix += stateClassName;
      Integer num = stateNameCounters.get(stateKey);
      if (num == null) {
        num = 0;
        stateNameCounters.put(stateKey, num);
      }
      stateNameCounters.put(stateKey, num.intValue() + 1);
      xmiState.setName(statePrefix + num);
      if (xmiState.getContainer() == null)
        Message.info("Set top state name of state machine for class `" + xmiClass.getName() + "' from empty to `" + xmiState.getName() + "'");
      else
        Message.info("Set state name in `" + xmiState.getContainer().getName() + "' of state machine for class `" + xmiClass.getName() + "' from empty to `" + xmiState.getName() + "'");
    }

    if (xmiState.isComposite()) {
      for (uml.parser.xmi.metamodel.State subXMIState : xmiState.getSubStates()) {
        normaliseStateNames(xmiClass, subXMIState, stateNameCounters);
      }
    }
  }

  private void normaliseDeferrableEvents(uml.parser.xmi.metamodel.Class xmiClass, uml.parser.xmi.metamodel.State xmiState) throws UModelException {
    if (xmiState.isComposite()) {
      for (uml.parser.xmi.metamodel.State subXMIState : xmiState.getSubStates())
        normaliseDeferrableEvents(xmiClass, subXMIState);
    }

    if (xmiState.isPseudo())
      return;

    // Work-around for CASE-tools that do not support deferred events
    for (uml.parser.xmi.metamodel.TaggedValue xmiTaggedValue : xmiState.getTaggedValues()) {
      if (xmiTaggedValue.getTag() != null && xmiTaggedValue.getTag().toLowerCase().trim().equals("defer")) {
        // Choose some deferrable kind of event (a call event would work, too)
        uml.parser.xmi.metamodel.Event deferrableEvent = new uml.parser.xmi.metamodel.Event(xmiTaggedValue.getValue(), uml.parser.xmi.metamodel.EventKind.SIGNAL);
        // Make sure that matching operation or signal will be chosen later on
        deferrableEvent.setSignal(null);
        xmiState.addDeferrableEvent(deferrableEvent);
        Message.info("Interpreted tagged value (`" + xmiTaggedValue.getTag() + "', `" + xmiTaggedValue.getValue() + "') as a definition of a deferrable event for state `" + xmiState.getName() + "' in state machine for class `" + xmiClass.getName() + "'");
      }
    }

    for (uml.parser.xmi.metamodel.Event deferrableXMIEvent : xmiState.getDeferrableEvents()) {
      if (deferrableXMIEvent.getKind() != uml.parser.xmi.metamodel.EventKind.CALL && deferrableXMIEvent.getKind() != uml.parser.xmi.metamodel.EventKind.SIGNAL)
        throw new UModelException("Illegal deferrence of non-call/signal event `" + deferrableXMIEvent.getName() + "' in state `" + xmiState.getName() + "' in state machine for class `" + xmiClass.getName() + "'");

      if (deferrableXMIEvent.getKind() == uml.parser.xmi.metamodel.EventKind.CALL) {
        if (deferrableXMIEvent.getOperation() != null)
          continue;
      }

      if (deferrableXMIEvent.getKind() == uml.parser.xmi.metamodel.EventKind.SIGNAL) {
        if (deferrableXMIEvent.getSignal() != null)
          continue;
      }

      // Deferrable event is not attached to information from model
      String deferrableXMIEventName = deferrableXMIEvent.getName();
      boolean foundMatch = false;

      // Normalise call and signal events
      // Is it an operation name?
      for (uml.parser.xmi.metamodel.Operation xmiOperation : xmiClass.getOperations()) {
        if (deferrableXMIEventName.equals(xmiOperation.getName())) {
          deferrableXMIEvent.setKind(uml.parser.xmi.metamodel.EventKind.CALL);
          deferrableXMIEvent.setOperation(xmiOperation);
          foundMatch = true;
          Message.info("Replaced deferrable event `" + deferrableXMIEventName + "' of state `" + xmiState.getName() + "' in state machine for class `" + xmiClass.getName() + "' by a call event for operation `" + xmiOperation.getName() + "'");
          break;
        }
      }
      if (foundMatch)
        continue;

      // Is it a signal name?
      for (uml.parser.xmi.metamodel.Reception xmiReception : xmiClass.getReceptions()) {
        if (deferrableXMIEventName.equals(xmiReception.getName()) || deferrableXMIEventName.equals(xmiReception.getSignal().getName())) {
          if (foundMatch)
            throw new UModelException("Several receptions for signal event `" + deferrableXMIEventName + "' in class `" + xmiClass.getName() + "'");
          deferrableXMIEvent.setKind(uml.parser.xmi.metamodel.EventKind.SIGNAL);
          deferrableXMIEvent.setSignal(xmiReception.getSignal());
          foundMatch = true;
          Message.info("Replaced deferrable event `" + deferrableXMIEventName + "' of state `" + xmiState.getName() + "' in state machine for class `" + xmiClass.getName() + "' by a signal event for signal `" + xmiReception.getSignal().getName() + "'");
        }
      }
      if (foundMatch)
        continue;

      // No match found
      throw new UModelException("No matching operation or reception found for deferrable event `" + deferrableXMIEventName + "' in state `" + xmiState.getName() + "' for class `" + xmiClass.getName() + "'");
    }
  }

  private void normaliseTriggers(uml.parser.xmi.metamodel.Class xmiClass, Collection<uml.parser.xmi.metamodel.Transition> xmiTransitions) throws UModelException {
    for (uml.parser.xmi.metamodel.Transition xmiTransition : xmiTransitions) {
      uml.parser.xmi.metamodel.Event trigger = xmiTransition.getTrigger();
      if (trigger != null) {
        if (trigger.getKind() == uml.parser.xmi.metamodel.EventKind.CALL) {
          if (trigger.getOperation() != null)
            continue;
        }

        if (trigger.getKind() == uml.parser.xmi.metamodel.EventKind.SIGNAL) {
          if (trigger.getSignal() != null)
            continue;
        }

        if (trigger.getKind() == uml.parser.xmi.metamodel.EventKind.TIME) {
          if (trigger.getWhen() != null && !trigger.getWhen().trim().equals(""))
            continue;
        }

        // Trigger is not attached to information from model
        String triggerName = (trigger.getName() == null ? "" : trigger.getName().trim());
        boolean foundMatch = false;

        // Normalise completion events
        if (triggerName.equals("")) {
          xmiTransition.setTrigger(null);
          foundMatch = true;
          Message.info("Replaced information-less trigger on transition from `" + xmiTransition.getSource().getName() + "' to `" + xmiTransition.getTarget().getName() + "' in state machine for class `" + xmiClass.getName() + "' by completion event");
        }
        if (foundMatch)
          continue;

        // Normalise time events
        if (triggerName.startsWith("after")) {
          String whenBody = triggerName.substring("after".length()).trim();
          if (whenBody.startsWith("("))
            whenBody = whenBody.substring(1, whenBody.length() - 1);
          trigger.setKind(uml.parser.xmi.metamodel.EventKind.TIME);
          trigger.setName(triggerName);
          trigger.setWhen(whenBody);
          foundMatch = true;
          Message.info("Replaced trigger `" + triggerName + "' in state machine for class `" + xmiClass.getName() + "' by time event `after " + trigger.getWhen() + "'");
        }
        if (foundMatch)
          continue;

        // Normalise call and signal events
        // Is it an operation name?
        for (uml.parser.xmi.metamodel.Operation xmiOperation : xmiClass.getOperations()) {
          if (triggerName.equals(xmiOperation.getName())) {
            trigger.setKind(uml.parser.xmi.metamodel.EventKind.CALL);
            trigger.setOperation(xmiOperation);
            foundMatch = true;
            Message.info("Replaced trigger `" + triggerName + "' in state machine for class `" + xmiClass.getName() + "' by a call event for operation `" + trigger.getOperation().getName() + "'");
            break;
          }
        }
        if (foundMatch)
          continue;

        // Is it a signal name?
        for (uml.parser.xmi.metamodel.Reception xmiReception : xmiClass.getReceptions()) {
          if (triggerName.equals(xmiReception.getName()) || triggerName.equals(xmiReception.getSignal().getName())) {
            if (foundMatch)
              throw new UModelException("Several receptions for signal event `" + triggerName + "' in class `" + xmiClass.getName() + "'");
            trigger.setKind(uml.parser.xmi.metamodel.EventKind.SIGNAL);
            trigger.setSignal(xmiReception.getSignal());
            foundMatch = true;
            Message.info("Replaced trigger `" + triggerName + "' in state machine for class `" + xmiClass.getName() + "' by a signal event for signal `" + trigger.getSignal().getName() + "'");
          }
        }
        if (foundMatch)
          continue;

        // No match found
        throw new UModelException("No matching operation or reception found for trigger `" + triggerName + "' on transition from `" + xmiTransition.getSource().getName() + "' to `" + xmiTransition.getTarget().getName() + "' in state machine of class `" + xmiClass.getName() + "'");
      }
    }
  }

  private void normaliseGuards(Collection<uml.parser.xmi.metamodel.Transition> xmiTransitions) {
    // Replace empty guards by no guard
    for (uml.parser.xmi.metamodel.Transition xmiTransition : xmiTransitions) {
      String guard = xmiTransition.getGuard();
      if (guard == null || guard.trim().equals("")) {
        xmiTransition.setGuard(null);
      }
    }
  }

  private void normaliseEffects(Collection<uml.parser.xmi.metamodel.Transition> xmiTransitions) {
    for (uml.parser.xmi.metamodel.Transition xmiTransition : xmiTransitions) {
      uml.parser.xmi.metamodel.Action effect = xmiTransition.getEffect();
      if (effect == null || effect.getScript() == null || effect.getScript().trim().equals(""))
        xmiTransition.setEffect(null);
    }
  }

  private void normaliseStateActions(uml.parser.xmi.metamodel.Class xmiClass, uml.parser.xmi.metamodel.State xmiState) throws UModelException {
    uml.parser.xmi.metamodel.Action entry = xmiState.getEntry();
    if (entry == null || entry.getScript() == null || entry.getScript().trim().equals(""))
      xmiState.setEntry(null);

    uml.parser.xmi.metamodel.Action exit = xmiState.getExit();
    if (exit == null || exit.getScript() == null || exit.getScript().trim().equals(""))
      xmiState.setExit(null);

    if (xmiState.isComposite()) {
      for (uml.parser.xmi.metamodel.State subXMIState : xmiState.getSubStates())
        normaliseStateActions(xmiClass, subXMIState);
    }
  }

  private void normaliseCollaborations() throws UModelException {
    int nameCounter = 0;
    for (uml.parser.xmi.metamodel.Collaboration xmiCollaboration : this.xmiModel.getCollaborations()) {
      // Check classifier roles for name and base class
      for (uml.parser.xmi.metamodel.ClassifierRole xmiClassifierRole : xmiCollaboration.getClassifierRoles()) {
        if (xmiClassifierRole.getName() == null || xmiClassifierRole.getName().equals("")) {
          if (xmiClassifierRole.getBase() != null)
            xmiClassifierRole.setName(xmiClassifierRole.getBase().getName());
          else
            throw new UModelException("Cannot determine base classifier for anonymous classifier role in collaboration `" + xmiCollaboration.getName() + "'");
          if (xmiClassifierRole.getName() == null || xmiClassifierRole.getName().equals(""))
            xmiClassifierRole.setName("role" + nameCounter++);
          Message.info("Changed name of classifier role in collaboration `" + xmiCollaboration.getName() + "' from empty to `" + xmiClassifierRole.getName() + "'");
        }
        // TODO (AK050627) perhaps remove trailing number from name when searching for matching classifier
        if (xmiClassifierRole.getBase() == null) {
          boolean noBase = true;
          // Try to figure out base classifier
          for (uml.parser.xmi.metamodel.Class xmiClass : this.xmiModel.getClasses()) {
            if (xmiClass.getName().toLowerCase().equals(xmiClassifierRole.getName())) {
              xmiClassifierRole.setBase(xmiClass);
              Message.info("Set base of classifier role `" + xmiClassifierRole.getName() + "' to class `" + xmiClass.getName() + "'");
              noBase = false;
              break;
            }
          }
          if (noBase) {
            throw new UModelException("Cannot determine base classifier for classifier role `" + xmiClassifierRole.getName() + "'");
          }
        }
        // Now base is set
        if (!(xmiClassifierRole.getBase() instanceof uml.parser.xmi.metamodel.Class))
          throw new UModelException("Classifier role `" + xmiClassifierRole.getName() + "' in collaboration `" + xmiCollaboration.getName() + "' has non-class base classifier + `" + xmiClassifierRole.getBase().getName() + "'");
      }

      // Check classifier roles for association end roles
      for (uml.parser.xmi.metamodel.ClassifierRole xmiClassifierRole : xmiCollaboration.getClassifierRoles()) {
        for (uml.parser.xmi.metamodel.AssociationEndRole xmiAssociationEndRole : xmiClassifierRole.getAssociationEndRoles()) {
          if (xmiAssociationEndRole.getName() == null || xmiAssociationEndRole.getName().trim().equals("")) {
            if (xmiAssociationEndRole.getBase() == null)
              throw new UModelException("Cannot determine base association end for anonymous association end role in classifier role `" + xmiClassifierRole.getName() + "' in collaboration `" + xmiCollaboration.getName() + "'");
            xmiAssociationEndRole.setName(xmiAssociationEndRole.getBase().getName());
            if (xmiAssociationEndRole.getName() == null || xmiAssociationEndRole.getName().equals(""))
              xmiClassifierRole.setName("endrole" + nameCounter++);
          }
          if (xmiAssociationEndRole.getBase() == null) {                   
            boolean noBase = true;
            // Try to figure out base association end
            for (uml.parser.xmi.metamodel.AssociationEnd xmiAssociationEnd : xmiClassifierRole.getBase().getAssociationEnds()) {
              if (xmiAssociationEnd.getName().equals(xmiAssociationEndRole.getName())) {
                xmiAssociationEndRole.setBase(xmiAssociationEnd);
                Message.info("Set base of association end role `" + xmiAssociationEndRole.getName() + "' to association end `" + xmiAssociationEnd.getName() + "'");
                noBase = false;
                break;
              }
            }
            if (noBase) {
              throw new UModelException("Cannot determine base association end for association end role `" + xmiAssociationEndRole.getName() + "'");
            }
          }
        }
      }
    }
  }

  private void normaliseInteractions() throws UModelException {
    for (uml.parser.xmi.metamodel.Collaboration xmiCollaboration : this.xmiModel.getCollaborations()) {
      // Check interaction names
      int nameCounter = 0;
      for (uml.parser.xmi.metamodel.Interaction xmiInteraction : xmiCollaboration.getInteractions()) {
        if (xmiInteraction.getName() == null || xmiInteraction.getName().equals("")) {
          xmiInteraction.setName("interaction" + nameCounter++);
          Message.info("Changed name of interaction in collaboration `" + xmiCollaboration.getName() + "' from empty to `" + xmiInteraction.getName() + "'");
        }
      }

      // Check message names, senders, and receivers
      for (uml.parser.xmi.metamodel.Interaction xmiInteraction : xmiCollaboration.getInteractions()) {
        nameCounter = 0;
        for (uml.parser.xmi.metamodel.Message xmiMessage : xmiInteraction.getMessages()) {
          if (xmiMessage.getName() == null || xmiMessage.getName().equals("")) {
            xmiInteraction.setName("msg" + nameCounter++);
            Message.info("Changed name of message in interaction `" + xmiInteraction.getName() + "' in collaboration `" + xmiCollaboration.getName() + "' from empty to `" + xmiMessage.getName() + "'");
          }
          if (xmiMessage.getSender() == null) {
            throw new UModelException("Message `" + xmiMessage.getName() + "' in interaction `" + xmiInteraction.getName() + "' in collaboration `" + xmiCollaboration.getName() + "' has no sender");
          }
          if (xmiMessage.getReceiver() == null) {
            throw new UModelException("Message `" + xmiMessage.getName() + "' in interaction `" + xmiInteraction.getName() + "' in collaboration `" + xmiCollaboration.getName() + "' has no receiver");
          }
          if (!xmiCollaboration.getClassifierRoles().contains(xmiMessage.getSender())) {
            throw new UModelException("Sender classifier role `" + xmiMessage.getSender().getName() + "` of message `" + xmiMessage.getName() + "' in interaction `" + xmiInteraction.getName() + "' in collaboration `" + xmiCollaboration.getName() + "' is not contained in collaboration");
          }
          if (!xmiCollaboration.getClassifierRoles().contains(xmiMessage.getReceiver())) {
            throw new UModelException("Receiver classifier role `" + xmiMessage.getReceiver().getName() + "` of message `" + xmiMessage.getName() + "' in interaction `" + xmiInteraction.getName() + "' in collaboration `" + xmiCollaboration.getName() + "' is not contained in collaboration");
          }
        }
      }

      // Check message predecessors
      for (uml.parser.xmi.metamodel.Interaction xmiInteraction : xmiCollaboration.getInteractions()) {
        for (uml.parser.xmi.metamodel.Message xmiMessage : xmiInteraction.getMessages()) {
          if (xmiMessage.getPredecessors() != null) {
            for (uml.parser.xmi.metamodel.Message predecessorXMIMessage : xmiMessage.getPredecessors()) {
              if (!xmiInteraction.getMessages().contains(predecessorXMIMessage))
                throw new UModelException("Predecessor message `" + predecessorXMIMessage.getName() + "` of message `" + xmiMessage.getName() + "' in interaction `" + xmiInteraction.getName() + "' in collaboration `" + xmiCollaboration.getName() + "' is not contained in interaction");
            }
          }
        }
      }
    }
  }

  /**
   * Find the association for a link and the association ends for its link ends.
   * 
   * Finding the association will only work if there are stimuli corresponding
   * to a link, if the sender and receiver instances of this stimulus are
   * different and have a single classifier, respectively, as type. This is only
   * necessary for old ArgoUML versions (around 0.9 to 0.12), which did not
   * properly store collaborations and interactions.
   *
   * A missing association end for a link end is determined by name matching.
   */
  private void normaliseLinks() throws UModelException {
    for (uml.parser.xmi.metamodel.Link xmiLink : this.xmiModel.getLinks()) {
      if (xmiLink.getAssociation() == null) {
        if (xmiLink.getStimuli().isEmpty()) {
          throw new UModelException("Cannot determine association for link `" + xmiLink.getName() + "', as it does not show stimuli");
        }
        uml.parser.xmi.metamodel.Stimulus xmiStimulus = xmiLink.getStimuli().iterator().next();
        uml.parser.xmi.metamodel.Object xmiSender = xmiStimulus.getSender();
        uml.parser.xmi.metamodel.Object xmiReceiver = xmiStimulus.getReceiver();
        if (xmiSender.equals(xmiReceiver))
          throw new UModelException("Cannot determine association for link `" + xmiLink.getName() + "', as sender and receiver of stimulus `" + xmiStimulus.getName() + "' on link `" + xmiLink.getName() + "' are the same");

        uml.parser.xmi.metamodel.Class senderXMIClass = xmiStimulus.getSender().getClassifier();
        uml.parser.xmi.metamodel.Class receiverXMIClass = xmiStimulus.getReceiver().getClassifier();
        for (uml.parser.xmi.metamodel.AssociationEnd senderXMIAssociationEnd : senderXMIClass.getAssociationEnds()) {
          uml.parser.xmi.metamodel.AssociationEnd receiverXMIAssociationEnd = senderXMIAssociationEnd.getOppositeAssociationEnd();
          if (receiverXMIAssociationEnd.getType().equals(receiverXMIClass)) {
            uml.parser.xmi.metamodel.Association xmiAssociation = senderXMIAssociationEnd.getAssociation();
            xmiLink.setAssociation(xmiAssociation);
            for (uml.parser.xmi.metamodel.LinkEnd xmiLinkEnd : xmiLink.getLinkEnds()) {
              if (xmiLinkEnd.getInstance().equals(xmiSender))
                xmiLinkEnd.setAssociationEnd(senderXMIAssociationEnd);
              if (xmiLinkEnd.getInstance().equals(xmiReceiver))
                xmiLinkEnd.setAssociationEnd(receiverXMIAssociationEnd);
            }
            Message.info("Associated link with stimulus `" + xmiStimulus.getName() + "' with association from `" + senderXMIClass.getName() + "' to `" + receiverXMIClass.getName() + "'");
            break;
          }
        }
      }

      if (xmiLink.getAssociation() != null) {
        for (uml.parser.xmi.metamodel.AssociationEnd xmiAssociationEnd : xmiLink.getAssociation().getAssociationEnds()) {
          if (xmiAssociationEnd.getName() == null)
            continue;
          for (uml.parser.xmi.metamodel.LinkEnd xmiLinkEnd : xmiLink.getLinkEnds()) {
            if (xmiLinkEnd.getAssociationEnd() != null)
              continue;
            if (xmiLinkEnd.getName() == null)
              continue;
            if (xmiAssociationEnd.getName().trim().equals(xmiLinkEnd.getName().trim())) {
              xmiLinkEnd.setAssociationEnd(xmiAssociationEnd);
              Message.info("Associated link end `" + xmiLinkEnd.getName() + "' of link `" + xmiLink.getName() + "' with equally named association end of association `" + xmiLink.getAssociation().getName() + "'");
            }
          }
        } 
      }  
    }
  }

  /**
   * Find constraints in the model attached to objects and add these to the
   * model; find constraints in the model attached to some part of a collaboration and
   * and add these to the collaboration.
   */
  private void normaliseConstraints() throws UModelException {
    Set<uml.parser.xmi.metamodel.Element> constraintSources = new HashSet<uml.parser.xmi.metamodel.Element>();
    constraintSources.addAll(this.xmiModel.getObjects());
    constraintSources.add(this.xmiModel);
    normaliseConstraints(constraintSources, this.xmiModel);
    for (uml.parser.xmi.metamodel.Collaboration xmiCollaboration : this.xmiModel.getCollaborations()) {
      constraintSources = new HashSet<uml.parser.xmi.metamodel.Element>();
      for (uml.parser.xmi.metamodel.ClassifierRole xmiClassifierRole : xmiCollaboration.getClassifierRoles())
        constraintSources.add(xmiClassifierRole);
      constraintSources.add(xmiCollaboration);
      normaliseConstraints(constraintSources, xmiCollaboration);
    }
  }

  private void normaliseConstraints(Set<uml.parser.xmi.metamodel.Element> constraintSources, uml.parser.xmi.metamodel.Element constrained) throws UModelException {
    int constraintsCounter = 0;
    for (uml.parser.xmi.metamodel.Element constraintSource : constraintSources) {
      for (uml.parser.xmi.metamodel.TaggedValue xmiTaggedValue : constraintSource.getTaggedValues()) {
        // Every tagged value that ends with "constraint" is interpreted as constraint
        if (xmiTaggedValue.getTag() != null && xmiTaggedValue.getTag().toLowerCase().trim().endsWith("constraint")) {
          String xmiConstraintName = xmiTaggedValue.getTag().trim().substring(0, xmiTaggedValue.getTag().trim().length()-"constraint".length());
          String xmiConstraintBody = xmiTaggedValue.getValue();
          uml.parser.xmi.metamodel.Constraint xmiConstraint = new uml.parser.xmi.metamodel.Constraint(xmiConstraintName, xmiConstraintBody);
          constrained.addConstraint(xmiConstraint);           
          Message.info("Added constraint `" + xmiTaggedValue.getValue() + "' as `" + xmiConstraint.getName() + "' to model element `" + constrained.getName() + "'");
        }
      }
    }
    List<uml.parser.xmi.metamodel.Constraint> xmiConstraints = new ArrayList<uml.parser.xmi.metamodel.Constraint>(constrained.getConstraints());
    for (uml.parser.xmi.metamodel.Constraint xmiConstraint : xmiConstraints) {
      if (xmiConstraint.getName() == null || xmiConstraint.getName().equals("")) {
        xmiConstraint.setName("constraint" + constraintsCounter++);
        Message.info("Changed name of constraint `" + xmiConstraint.getBody() + "' of model element `" + constrained.getName() + "' from empty to `" + xmiConstraint.getName() + "'");
      }
      if (xmiConstraint.getBody() == null || xmiConstraint.getBody().trim().equals("")) {
        constrained.removeConstraint(xmiConstraint);
        Message.info("Ignored constraint `" + xmiConstraint.getName() + "' of model element `" + constrained.getName() + "' with empty body");
      }
      if (xmiConstraint.getBody() != null && xmiConstraint.getBody().trim().equals(xmiConstraint.getName().trim())) {
        constrained.removeConstraint(xmiConstraint);
        Message.warning("Ignored constraint `" + xmiConstraint.getName() + "' of model element `" + constrained.getName() + "' as name and body are equal");
      }
    }
  }
}
