package uml.parser.xmi;

import java.util.*;
import org.w3c.dom.*;


/**
 * XMI 1.0 parser.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class XMI10Parser extends XMIParser {
  private static final String XMIID = "xmi.id";
  private static final String XMIIDREF = "xmi.idref";

  private uml.parser.xmi.metamodel.Model xmiModel = null;
  private List<uml.parser.xmi.metamodel.State> xmiStates = null;
  private List<uml.parser.xmi.metamodel.Event> xmiEvents = null;
  private List<uml.parser.xmi.metamodel.Action> xmiActions = null;

  public XMI10Parser() {
    this.reset(null);
  }

  private void reset(Document document) {
    super.reset(document, XMIID, XMIIDREF);
    this.xmiModel = null;
    this.xmiStates = null;
    this.xmiEvents = null;
    this.xmiActions = null;
  }

  public uml.parser.xmi.metamodel.Model parse(Document document) throws uml.UModelException {
    this.reset(document);

    this.xmiModel = this.getModel();
    if (xmiModel == null)
      throw new uml.UModelException("No model in XMI 1.0 document found");

    util.Message.info("Parsing model `" + xmiModel.getName() + "' from XMI 1.0 document");
    if (xmiModel != null) {
      this.addClasses(xmiModel);
      this.addDataTypes(xmiModel);
      this.addSignals(xmiModel);

      this.addAssociations(xmiModel);
      this.addAssociationEnds();

      this.addAttributes();
      this.addOperations();
      this.addReceptions();
      this.addParameters();

      this.addStateMachines();
      this.addStates();
      this.addTransitions();

      this.addCollaborations(xmiModel);
      this.addClassifierRoles();
      this.addAssociationRoles();
      this.addAssociationEndRoles();

      this.addInteractions();
      this.addMessages();

      this.addObjects(xmiModel);
      this.addLinks(xmiModel);
      this.addLinkEnds();
      this.addStimuli();

      this.addConstraints();
      this.addTaggedValues();
      
      return xmiModel;
    }

    throw new uml.UModelException("No model found in XMI 1.0");
  }

  private String getElementString(Node xmiNode, String name) {
    Node subXMINode = getChild(xmiNode, name);
    if (subXMINode != null) {
      NamedNodeMap attributes = subXMINode.getAttributes();
      Node valueNode = attributes.getNamedItem("xmi.value");
      return valueNode.getNodeValue();
    }
    return "";
  }

  private String getName(Node xmiNode) {
    Node subXMINode = getChild(xmiNode, "Foundation.Core.ModelElement.name");
    if (subXMINode != null)
      return subXMINode.getTextContent();
    return "";
  }

  private uml.parser.xmi.metamodel.Model getModel() {
    if (this.xmiModel != null)
      return this.xmiModel;

    List<uml.parser.xmi.metamodel.Model> xmiModels = new ArrayList<uml.parser.xmi.metamodel.Model>();
    for (Node modelNode : this.getXMINodes("Model_Management.Model")) {
      uml.parser.xmi.metamodel.Model xmiModel = new uml.parser.xmi.metamodel.Model(this.getName(modelNode));
      xmiModels.add(xmiModel);
      this.addElementNode(xmiModel, modelNode);
    }
    if (xmiModels.isEmpty())
      return null;
    return xmiModels.get(0);
  }

  private List<uml.parser.xmi.metamodel.Class> addClasses(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Class> xmiClasses = new ArrayList<uml.parser.xmi.metamodel.Class>();
    Node modelNode = this.getNode(xmiModel);
    if (modelNode == null)
      return xmiClasses;

    for (Node classNode : this.getXMINodes("Foundation.Core.Class")) {
      if (this.isContained(classNode, modelNode)) {
        uml.parser.xmi.metamodel.Class xmiClass = xmiModel.createClass(this.getName(classNode));
        xmiClasses.add(xmiClass);
        this.addElementNode(xmiClass, classNode);
        util.Message.debug().info(xmiClass.toString());
      }
    }
    return xmiClasses;
  }

  private List<uml.parser.xmi.metamodel.DataType> addDataTypes(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.DataType> xmiDataTypes = new ArrayList<uml.parser.xmi.metamodel.DataType>();
    Node modelNode = this.getNode(xmiModel);
    if (modelNode == null)
      return xmiDataTypes;

    for (Node dataTypeNode : this.getXMINodes("Foundation.Core.DataType")) {
      if (this.isContained(dataTypeNode, modelNode)) {
        uml.parser.xmi.metamodel.DataType xmiDataType = xmiModel.createDataType(this.getName(dataTypeNode));
        xmiDataTypes.add(xmiDataType);
        this.addElementNode(xmiDataType, dataTypeNode);
        util.Message.debug().info(xmiDataType.toString());
      }
    }
    return xmiDataTypes;
  }

  private List<uml.parser.xmi.metamodel.Signal> addSignals(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Signal> xmiSignals = new ArrayList<uml.parser.xmi.metamodel.Signal>();
    Node modelNode = this.getNode(xmiModel);
    if (modelNode == null)
      return xmiSignals;

    for (Node signalNode : this.getXMINodes("Behavioral_Elements.Common_Behavior.Signal")) {
      if (this.isContained(signalNode, modelNode)) {
        uml.parser.xmi.metamodel.Signal xmiSignal = xmiModel.createSignal(this.getName(signalNode));
        xmiSignals.add(xmiSignal);
        this.addElementNode(xmiSignal, signalNode);
        util.Message.debug().info(xmiSignal.toString());
      }
    }
    return xmiSignals;
  }

  private List<uml.parser.xmi.metamodel.Association> addAssociations(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Association> xmiAssociations = new ArrayList<uml.parser.xmi.metamodel.Association>();
    Node modelNode = this.getNode(xmiModel);
    if (modelNode == null)
      return xmiAssociations;

    for (Node associationNode : this.getXMINodes("Foundation.Core.Association")) {
      if (this.isContained(associationNode, modelNode)) {
        uml.parser.xmi.metamodel.Association xmiAssociation = xmiModel.createAssociation(this.getName(associationNode));
        xmiAssociations.add(xmiAssociation);
        this.addElementNode(xmiAssociation, associationNode);
        util.Message.debug().info(xmiAssociation.toString());
      }
    }
    return xmiAssociations;
  }

  private List<uml.parser.xmi.metamodel.AssociationEnd> addAssociationEnds() {
    List<uml.parser.xmi.metamodel.AssociationEnd> xmiAssociationEnds = new ArrayList<uml.parser.xmi.metamodel.AssociationEnd>();
    for (Node associationEndNode : this.getXMINodes("Foundation.Core.AssociationEnd")) {
      // Get owning association
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(associationEndNode, "Foundation.Core.AssociationEnd.association");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Association))
        continue;

      uml.parser.xmi.metamodel.Association xmiAssociation = (uml.parser.xmi.metamodel.Association)xmiElement;

      // Get type
      xmiElement = this.getElementXMIRef(associationEndNode, "Foundation.Core.AssociationEnd.type");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;
      
      uml.parser.xmi.metamodel.Classifier xmiType = (uml.parser.xmi.metamodel.Classifier)xmiElement;
      uml.parser.xmi.metamodel.AssociationEnd xmiAssociationEnd = xmiAssociation.createAssociationEnd(this.getName(associationEndNode), xmiType);
      xmiAssociationEnds.add(xmiAssociationEnd);
      this.addElementNode(xmiAssociationEnd, associationEndNode);

      // Get association end's multiplicity
      Node multiplicityNode = getChild(associationEndNode, "Foundation.Core.AssociationEnd.multiplicity");
      if (multiplicityNode != null) {
        Node upperMultiplicityNode = getDescendant(multiplicityNode, "Foundation.Data_Types.MultiplicityRange.upper");
        if (upperMultiplicityNode != null) {
          xmiAssociationEnd.setUpperMultiplicity(getElementInt(upperMultiplicityNode, xmiAssociationEnd.getUpperMultiplicity()));
        }
      }

      // Get association end's navigability
      String navigability = getElementString(associationEndNode, "Foundation.Core.AssociationEnd.isNavigable");
      if ("false".equals(navigability))
        xmiAssociationEnd.setNavigable(false);

      util.Message.debug().info(xmiAssociationEnd.toString());
    }
    return xmiAssociationEnds;    
  }

  private List<uml.parser.xmi.metamodel.Attribute> addAttributes() {
    List<uml.parser.xmi.metamodel.Attribute> xmiAttributes = new ArrayList<uml.parser.xmi.metamodel.Attribute>();
    for (Node attributeNode : this.getXMINodes("Foundation.Core.Attribute")) {
      // Get owning class
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(attributeNode, "Foundation.Core.Feature.owner");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;

      uml.parser.xmi.metamodel.Classifier xmiClassifier = (uml.parser.xmi.metamodel.Class)xmiElement;

      // Get type
      xmiElement = this.getElementXMIRef(attributeNode, "Foundation.Core.StructuralFeature.type");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;
      
      uml.parser.xmi.metamodel.Classifier xmiType = (uml.parser.xmi.metamodel.Classifier)xmiElement;
      uml.parser.xmi.metamodel.Attribute xmiAttribute = xmiClassifier.createAttribute(this.getName(attributeNode), xmiType);
      xmiAttributes.add(xmiAttribute);
      this.addElementNode(xmiAttribute, attributeNode);

      // Get attribute's multiplicity
      Node multiplicityNode = getChild(attributeNode, "Foundation.Core.StructuralFeature.multiplicity");
      if (multiplicityNode != null) {
        Node upperMultiplicityNode = getDescendant(multiplicityNode, "Foundation.Data_Types.MultiplicityRange.upper");
        if (upperMultiplicityNode != null) {
          xmiAttribute.setUpperMultiplicity(getElementInt(upperMultiplicityNode, xmiAttribute.getUpperMultiplicity()));
        }
      }
      
      // Get whether attribute is constant
      String changeability = getElementString(attributeNode, "Foundation.Core.StructuralFeature.changeability");
      if ("frozen".equals(changeability))
        xmiAttribute.setFinal(true);

      // Get whether attribute is static
      String ownerScope = getElementString(attributeNode, "Foundation.Core.Feature.ownerScope");
      if ("classifier".equals(ownerScope))
        xmiAttribute.setStatic(true);

      // Get initial value
      Node initialValueNode = getChild(attributeNode, "Foundation.Core.Attribute.initialValue");
      if (initialValueNode != null) {
        Node expressionDataNode = getDescendant(initialValueNode, "Foundation.Data_Types.Expression.body");
        if (expressionDataNode != null)
          xmiAttribute.addInitialValue(expressionDataNode.getTextContent());
      }

      util.Message.debug().info(xmiAttribute.toString());
    }
    return xmiAttributes;    
  }

  private List<uml.parser.xmi.metamodel.Operation> addOperations() {
    List<uml.parser.xmi.metamodel.Operation> xmiOperations = new ArrayList<uml.parser.xmi.metamodel.Operation>();
    for (Node operationNode : this.getXMINodes("Foundation.Core.Operation")) {
      // Get owning class
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(operationNode, "Foundation.Core.Feature.owner");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;

      uml.parser.xmi.metamodel.Classifier xmiClassifier = (uml.parser.xmi.metamodel.Class)xmiElement;
      uml.parser.xmi.metamodel.Operation xmiOperation = xmiClassifier.createOperation(this.getName(operationNode));
      xmiOperations.add(xmiOperation);
      this.addElementNode(xmiOperation, operationNode);

      util.Message.debug().info(xmiOperation.toString());
    }
    return xmiOperations;    
  }

  private List<uml.parser.xmi.metamodel.Reception> addReceptions() {
    List<uml.parser.xmi.metamodel.Reception> xmiReceptions = new ArrayList<uml.parser.xmi.metamodel.Reception>();
    for (Node receptionNode : this.getXMINodes("Behavioral_Elements.Common_Behavior.Reception")) {
      // Get owning class
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(receptionNode, "Foundation.Core.Feature.owner");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;

      uml.parser.xmi.metamodel.Classifier xmiClassifier = (uml.parser.xmi.metamodel.Class)xmiElement;

      // Get type
      xmiElement = this.getElementXMIRef(receptionNode, "Behavioral_Elements.Common_Behavior.Reception.signal");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Signal))
        continue;
      
      uml.parser.xmi.metamodel.Signal xmiSignal = (uml.parser.xmi.metamodel.Signal)xmiElement;
      uml.parser.xmi.metamodel.Reception xmiReception = xmiClassifier.createReception(this.getName(receptionNode), xmiSignal);
      xmiReceptions.add(xmiReception);
      this.addElementNode(xmiReception, receptionNode);

      util.Message.debug().info(xmiReception.toString());
    }
    return xmiReceptions;    
  }

  private List<uml.parser.xmi.metamodel.Parameter> addParameters() {
    List<uml.parser.xmi.metamodel.Parameter> xmiParameters = new ArrayList<uml.parser.xmi.metamodel.Parameter>();
    for (Node parameterNode : this.getXMINodes("Foundation.Core.Parameter")) {
      // Get owning behavioural (feature)
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(parameterNode, "Foundation.Core.Parameter.behavioralFeature");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Behavioural))
        continue;

      uml.parser.xmi.metamodel.Behavioural xmiBehavioural = (uml.parser.xmi.metamodel.Behavioural)xmiElement;

      // Get type
      xmiElement = this.getElementXMIRef(parameterNode, "Foundation.Core.Parameter.type");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;

      uml.parser.xmi.metamodel.Classifier xmiClassifier = (uml.parser.xmi.metamodel.Classifier)xmiElement;
      
      // Get parameter direction kind
      uml.parser.xmi.metamodel.ParameterDirectionKind kind = uml.parser.xmi.metamodel.ParameterDirectionKind.IN;
      String kindString = getElementString(parameterNode, "Foundation.Core.Parameter.kind");
      if (kindString != null) {
        kindString = kindString.toLowerCase().trim();
        if ("in".equals(kindString))
          kind = uml.parser.xmi.metamodel.ParameterDirectionKind.IN;
        if ("inout".equals(kindString))
          kind = uml.parser.xmi.metamodel.ParameterDirectionKind.INOUT;
        if ("out".equals(kindString))
          kind = uml.parser.xmi.metamodel.ParameterDirectionKind.OUT;
        if ("return".equals(kindString))
          kind = uml.parser.xmi.metamodel.ParameterDirectionKind.RETURN;
      }

      uml.parser.xmi.metamodel.Parameter xmiParameter = xmiBehavioural.createParameter(this.getName(parameterNode), kind, xmiClassifier);
      xmiParameters.add(xmiParameter);
      this.addElementNode(xmiParameter, parameterNode);
      
      util.Message.debug().info(xmiParameter.toString());
    }
    return xmiParameters;    
  }

  private List<uml.parser.xmi.metamodel.StateMachine> addStateMachines() {
    List<uml.parser.xmi.metamodel.StateMachine> xmiStateMachines = new ArrayList<uml.parser.xmi.metamodel.StateMachine>();
    for (Node stateMachineNode : this.getXMINodes("Behavioral_Elements.State_Machines.StateMachine")) {
      // Get owning class
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(stateMachineNode, "Behavioral_Elements.State_Machines.StateMachine.context");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Class))
        continue;

      uml.parser.xmi.metamodel.Class xmiClass = (uml.parser.xmi.metamodel.Class)xmiElement;
      uml.parser.xmi.metamodel.StateMachine xmiStateMachine = xmiClass.createStateMachine(this.getName(stateMachineNode));
      xmiStateMachines.add(xmiStateMachine);
      this.addElementNode(xmiStateMachine, stateMachineNode);
      
      util.Message.debug().info(xmiStateMachine.toString());
    }
    return xmiStateMachines;    
  }

  private List<uml.parser.xmi.metamodel.Event> getEvents() {
    if (this.xmiEvents != null)
      return this.xmiEvents;

    this.xmiEvents = new ArrayList<uml.parser.xmi.metamodel.Event>();

    for (Node signalEventNode : this.getXMINodes("Behavioral_Elements.State_Machines.SignalEvent")) {
      uml.parser.xmi.metamodel.Event xmiSignalEvent = new uml.parser.xmi.metamodel.Event(this.getName(signalEventNode), uml.parser.xmi.metamodel.EventKind.SIGNAL);
      this.xmiEvents.add(xmiSignalEvent);
      this.addElementNode(xmiSignalEvent, signalEventNode);
    }

    for (Node callEventNode : this.getXMINodes("Behavioral_Elements.State_Machines.CallEvent")) {
      uml.parser.xmi.metamodel.Event xmiCallEvent = new uml.parser.xmi.metamodel.Event(this.getName(callEventNode), uml.parser.xmi.metamodel.EventKind.CALL);
      this.xmiEvents.add(xmiCallEvent);
      this.addElementNode(xmiCallEvent, callEventNode);
    }

    for (Node timeEventNode : this.getXMINodes("Behavioral_Elements.State_Machines.TimeEvent")) {
      uml.parser.xmi.metamodel.Event xmiTimeEvent = new uml.parser.xmi.metamodel.Event(this.getName(timeEventNode), uml.parser.xmi.metamodel.EventKind.TIME);
      this.xmiEvents.add(xmiTimeEvent);
      this.addElementNode(xmiTimeEvent, timeEventNode);

      // Get when expression
      Node whenNode = getChild(timeEventNode, "Behavioral_Elements.State_Machines.TimeEvent.when");
      if (whenNode != null) {
        Node expressionDataNode = getDescendant(whenNode, "Foundation.Data_Types.Expression.body");
        if (expressionDataNode != null) {
          xmiTimeEvent.setWhen(expressionDataNode.getTextContent());
        }
      }
    }

    return this.xmiEvents;
  }

  private List<uml.parser.xmi.metamodel.Event> getElementEvents(Node node, String name) {
    List<uml.parser.xmi.metamodel.Event> xmiEvents = new ArrayList<uml.parser.xmi.metamodel.Event>();

    List<Node> eventNodes = getChildren(node, name);
    if (eventNodes == null || eventNodes.isEmpty())
      return xmiEvents;

    for (uml.parser.xmi.metamodel.Event xmiEvent : this.getEvents()) {
      for (Node eventNode : eventNodes) {
        if (getNode(xmiEvent) != null && this.isContained(getNode(xmiEvent), eventNode))
          xmiEvents.add(xmiEvent);
      }
    }

    for (uml.parser.xmi.metamodel.Element xmiElement : this.getElementXMIRefs(node, name)) {
      if (xmiElement != null && (xmiElement instanceof uml.parser.xmi.metamodel.Event))
        xmiEvents.add((uml.parser.xmi.metamodel.Event)xmiElement);
    }

    return xmiEvents;
  }

  private uml.parser.xmi.metamodel.Event getElementEvent(Node node, String name) {
    List<uml.parser.xmi.metamodel.Event> xmiEvents = this.getElementEvents(node, name);
    if (xmiEvents == null || xmiEvents.isEmpty())
      return null;
    return xmiEvents.get(0);
  }

  private List<uml.parser.xmi.metamodel.Action> getActions() {
    if (this.xmiActions != null)
      return this.xmiActions;

    this.xmiActions = new ArrayList<uml.parser.xmi.metamodel.Action>();
    List<Node> actionNodes = new ArrayList<Node>();
    actionNodes.addAll(this.getXMINodes("Behavioral_Elements.Common_Behavior.Action"));
    actionNodes.addAll(this.getXMINodes("Behavioral_Elements.Common_Behavior.CallAction"));
    actionNodes.addAll(this.getXMINodes("Behavioral_Elements.Common_Behavior.SendAction"));
    actionNodes.addAll(this.getXMINodes("Behavioral_Elements.Common_Behavior.UninterpretedAction"));    
    for (Node actionNode : actionNodes) {
      Node expressionDataNode = getDescendant(actionNode, "Foundation.Data_Types.Expression.body");
      if (expressionDataNode != null) {
        String script = expressionDataNode.getTextContent();
        if (script == null)
          script = "";
        uml.parser.xmi.metamodel.Action xmiAction = new uml.parser.xmi.metamodel.Action(script);
        this.xmiActions.add(xmiAction);
        this.addElementNode(xmiAction, actionNode);
      }
    }

    return this.xmiActions;
  }

  private uml.parser.xmi.metamodel.Action getAction(Node node, String name) {
    Node actionNode = getChild(node, name);
    if (actionNode == null)
      return null;

    for (uml.parser.xmi.metamodel.Action xmiAction : this.getActions()) {
      if (getNode(xmiAction) != null && this.isContained(getNode(xmiAction), node))
        return xmiAction;
    }

    uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIRef(node, name);
    if (xmiElement != null && (xmiElement instanceof uml.parser.xmi.metamodel.Action))
      return (uml.parser.xmi.metamodel.Action)xmiElement;

    return null;
  }

  private List<uml.parser.xmi.metamodel.State> getStates() {
    if (this.xmiStates != null)
      return this.xmiStates;

    this.xmiStates = new ArrayList<uml.parser.xmi.metamodel.State>();

    List<Node> simpleStateNodes = new ArrayList<Node>();
    simpleStateNodes.addAll(this.getXMINodes("Behavioral_Elements.State_Machines.State"));
    simpleStateNodes.addAll(this.getXMINodes("Behavioral_Elements.State_Machines.SimpleState"));
    for (Node simpleStateNode : simpleStateNodes) {
      uml.parser.xmi.metamodel.State xmiState = new uml.parser.xmi.metamodel.State(this.getName(simpleStateNode), uml.parser.xmi.metamodel.StateKind.SIMPLE);
      this.xmiStates.add(xmiState);
      this.addElementNode(xmiState, simpleStateNode);
    }

    for (Node stateNode : this.getXMINodes("Behavioral_Elements.State_Machines.FinalState")) {
      uml.parser.xmi.metamodel.State xmiState = new uml.parser.xmi.metamodel.State(this.getName(stateNode), uml.parser.xmi.metamodel.StateKind.FINAL);
      this.xmiStates.add(xmiState);
      this.addElementNode(xmiState, stateNode);
    }

    for (Node stateNode : this.getXMINodes("Behavioral_Elements.State_Machines.CompositeState")) {
      uml.parser.xmi.metamodel.State xmiState = new uml.parser.xmi.metamodel.State(this.getName(stateNode), uml.parser.xmi.metamodel.StateKind.COMPOSITE);
      String isConcurrentString = getElementString(stateNode, "Behavioral_Elements.State_Machines.CompositeState.isConcurent");
      if (isConcurrentString == null)
        isConcurrentString = getElementString(stateNode, "Behavioral_Elements.State_Machines.CompositeState.isConcurrent");
      if (isConcurrentString != null) {
        isConcurrentString = isConcurrentString.toLowerCase().trim();
        if ("true".equals(isConcurrentString))
          xmiState.setConcurrent(true);
      }
      this.xmiStates.add(xmiState);
      this.addElementNode(xmiState, stateNode);
    }

    for (Node stateNode : this.getXMINodes("Behavioral_Elements.State_Machines.Pseudostate")) {
      // Get pseudostate kind
      uml.parser.xmi.metamodel.StateKind kind = uml.parser.xmi.metamodel.StateKind.SIMPLE;
      String kindString = getElementString(stateNode, "Behavioral_Elements.State_Machines.Pseudostate.kind");
      if (kindString != null) {
        kindString = kindString.toLowerCase().trim();
        if ("choice".equals(kindString) || "branch".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.CHOICE;
        else if ("deephistory".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.DEEPHISTORY;
        else if ("fork".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.FORK;
        else if ("initial".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.INITIAL;
        else if ("join".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.JOIN;
        else if ("junction".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.JUNCTION;
        else if ("shallowhistory".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.SHALLOWHISTORY;
        else if ("synch".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.SYNCH;
        else if ("final".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.FINAL;
      }
      uml.parser.xmi.metamodel.State xmiState = new uml.parser.xmi.metamodel.State(this.getName(stateNode), kind);
      this.xmiStates.add(xmiState);
      this.addElementNode(xmiState, stateNode);
    }

    // Add deferrable events
    for (uml.parser.xmi.metamodel.State xmiState : this.xmiStates) {
      Node stateNode = this.getNode(xmiState);
      List<uml.parser.xmi.metamodel.Event> xmiEvents = this.getElementEvents(stateNode, "Behavioral_Elements.State_Machines.State.deferrableEvent");
      for (uml.parser.xmi.metamodel.Event xmiEvent : xmiEvents) {
        if (xmiEvent != null)
          xmiState.addDeferrableEvent(xmiEvent);
      }
    }

    // Get entry actions
    for (uml.parser.xmi.metamodel.State xmiState : this.xmiStates) {
      Node stateNode = this.getNode(xmiState);
      uml.parser.xmi.metamodel.Action xmiAction = this.getAction(stateNode, "Behavioral_Elements.State_Machines.State.entry");
      if (xmiAction != null)
        xmiState.setEntry(xmiAction);
    }

    // Get exit actions
    for (uml.parser.xmi.metamodel.State xmiState : this.xmiStates) {
      Node stateNode = this.getNode(xmiState);
      uml.parser.xmi.metamodel.Action xmiAction = getAction(stateNode, "Behavioral_Elements.State_Machines.State.exit");
      if (xmiAction != null)
        xmiState.setExit(xmiAction);
    }

    return this.xmiStates;
  }

  private List<uml.parser.xmi.metamodel.State> addStates() {
    List<uml.parser.xmi.metamodel.State> xmiStates = this.getStates();
    for (uml.parser.xmi.metamodel.State xmiState : xmiStates) {
      Node stateNode = this.getNode(xmiState);
      if (stateNode == null)
        continue;
      uml.parser.xmi.metamodel.Element xmiElement = null;

      // Get owning state machine (for top states)
      xmiElement = this.getElementXMIOwner(stateNode, "Behavioral_Elements.State_Machines.State.stateMachine");
      if (xmiElement != null && (xmiElement instanceof uml.parser.xmi.metamodel.StateMachine)) {
        uml.parser.xmi.metamodel.StateMachine xmiStateMachine = (uml.parser.xmi.metamodel.StateMachine)xmiElement;
        xmiState.setStateMachine(xmiStateMachine);
        continue;
      }

      // Get container state (for non-top states)
      xmiElement = this.getElementXMIOwner(stateNode, "Behavioral_Elements.State_Machines.StateVertex.container");
      if (xmiElement != null && (xmiElement instanceof uml.parser.xmi.metamodel.State)) {
        uml.parser.xmi.metamodel.State xmiContainer = (uml.parser.xmi.metamodel.State)xmiElement;
        xmiState.setContainer(xmiContainer);
      }

      util.Message.debug().info(xmiState.toString());
    }
    return xmiStates;
  }

  private List<uml.parser.xmi.metamodel.Transition> addTransitions() {
    List<uml.parser.xmi.metamodel.Transition> xmiTransitions = new ArrayList<uml.parser.xmi.metamodel.Transition>();
    for (Node transitionNode : this.getXMINodes("Behavioral_Elements.State_Machines.Transition")) {
      uml.parser.xmi.metamodel.Element xmiElement = null;

      // Get owning state machine
      xmiElement = this.getElementXMIOwner(transitionNode, "Behavioral_Elements.State_Machines.Transition.stateMachine");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.StateMachine))
        continue;

      uml.parser.xmi.metamodel.StateMachine xmiStateMachine = (uml.parser.xmi.metamodel.StateMachine)xmiElement;

      // Get source state
      xmiElement = this.getElementXMIRef(transitionNode, "Behavioral_Elements.State_Machines.Transition.source");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.State))
        continue;

      uml.parser.xmi.metamodel.State xmiSourceState = (uml.parser.xmi.metamodel.State)xmiElement;

      // Get target state
      xmiElement = this.getElementXMIRef(transitionNode, "Behavioral_Elements.State_Machines.Transition.target");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.State))
        continue;

      uml.parser.xmi.metamodel.State xmiTargetState = (uml.parser.xmi.metamodel.State)xmiElement;
      
      uml.parser.xmi.metamodel.Transition xmiTransition = xmiStateMachine.createTransition(this.getName(transitionNode), xmiSourceState, xmiTargetState);
      xmiTransitions.add(xmiTransition);
      this.addElementNode(xmiTransition, transitionNode);
      
      // Get trigger
      uml.parser.xmi.metamodel.Event xmiEvent = this.getElementEvent(transitionNode, "Behavioral_Elements.State_Machines.Transition.trigger");
      if (xmiEvent != null) {
        xmiTransition.setTrigger(xmiEvent);
      }

      // Get guard
      Node guardNode = getChild(transitionNode, "Behavioral_Elements.State_Machines.Transition.guard");
      if (guardNode != null) {
        Node expressionDataNode = getDescendant(guardNode, "Foundation.Data_Types.Expression.body");
        if (expressionDataNode != null)
          xmiTransition.setGuard(expressionDataNode.getTextContent());
      }

      // Get effect
      uml.parser.xmi.metamodel.Action xmiAction = getAction(transitionNode, "Behavioral_Elements.State_Machines.Transition.effect");
      if (xmiAction != null)
        xmiTransition.setEffect(xmiAction);

      util.Message.debug().info(xmiTransition.toString());
    }
    return xmiTransitions;    
  }

  private List<uml.parser.xmi.metamodel.Collaboration> addCollaborations(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Collaboration> xmiCollaborations = new ArrayList<uml.parser.xmi.metamodel.Collaboration>();
    Node modelNode = this.getNode(xmiModel);
    if (modelNode == null)
      return xmiCollaborations;

    for (Node collaborationNode : this.getXMINodes("Behavioral_Elements.Collaborations.Collaboration")) {
      if (this.isContained(collaborationNode, modelNode)) {
        uml.parser.xmi.metamodel.Collaboration xmiCollaboration = xmiModel.createCollaboration(this.getName(collaborationNode));
        xmiCollaborations.add(xmiCollaboration);
        this.addElementNode(xmiCollaboration, collaborationNode);
        util.Message.debug().info(xmiCollaboration.toString());
      }
    }
    return xmiCollaborations;    
  }

  private List<uml.parser.xmi.metamodel.ClassifierRole> addClassifierRoles() {
    List<uml.parser.xmi.metamodel.ClassifierRole> xmiClassifierRoles = new ArrayList<uml.parser.xmi.metamodel.ClassifierRole>();
    for (Node classifierRoleNode : this.getXMINodes("Behavioral_Elements.Collaborations.ClassifierRole")) {
      // Get owning collaboration
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(classifierRoleNode, null);
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Collaboration))
        continue;

      uml.parser.xmi.metamodel.Collaboration xmiCollaboration = (uml.parser.xmi.metamodel.Collaboration)xmiElement;

      // Get base
      xmiElement = this.getElementXMIRef(classifierRoleNode, "Behavioral_Elements.Collaborations.ClassifierRole.base");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;
      
      uml.parser.xmi.metamodel.Classifier xmiBase = (uml.parser.xmi.metamodel.Classifier)xmiElement;
      uml.parser.xmi.metamodel.ClassifierRole xmiClassifierRole = xmiCollaboration.createClassifierRole(this.getName(classifierRoleNode), xmiBase);
      xmiClassifierRoles.add(xmiClassifierRole);
      this.addElementNode(xmiClassifierRole, classifierRoleNode);

      util.Message.debug().info(xmiClassifierRole.toString());
    }
    return xmiClassifierRoles;
  }

  private List<uml.parser.xmi.metamodel.AssociationRole> addAssociationRoles() {
    List<uml.parser.xmi.metamodel.AssociationRole> xmiAssociationRoles = new ArrayList<uml.parser.xmi.metamodel.AssociationRole>();
    for (Node associationRoleNode : this.getXMINodes("Behavioral_Elements.Collaborations.AssociationRole")) {
      // Get owning collaboration
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(associationRoleNode, null);
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Collaboration))
        continue;

      uml.parser.xmi.metamodel.Collaboration xmiCollaboration = (uml.parser.xmi.metamodel.Collaboration)xmiElement;

      // Get base (we also accept <code>null</code>)
      xmiElement = this.getElementXMIRef(associationRoleNode, "Behavioral_Elements.Collaborations.AssociationRole.base");
      if (xmiElement != null && !(xmiElement instanceof uml.parser.xmi.metamodel.Association))
        continue;

      uml.parser.xmi.metamodel.Association xmiBase = (uml.parser.xmi.metamodel.Association)xmiElement;
      uml.parser.xmi.metamodel.AssociationRole xmiAssociationRole = xmiCollaboration.createAssociationRole(this.getName(associationRoleNode), xmiBase);
      xmiAssociationRoles.add(xmiAssociationRole);
      this.addElementNode(xmiAssociationRole, associationRoleNode);

      util.Message.debug().info(xmiAssociationRole.toString());
    }
    return xmiAssociationRoles;    
  }

  private List<uml.parser.xmi.metamodel.AssociationEndRole> addAssociationEndRoles() {
    List<uml.parser.xmi.metamodel.AssociationEndRole> xmiAssociationEndRoles = new ArrayList<uml.parser.xmi.metamodel.AssociationEndRole>();
    for (Node associationEndRoleNode : this.getXMINodes("Behavioral_Elements.Collaborations.AssociationEndRole")) {
      // Get owning collaboration
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(associationEndRoleNode, "Foundation.Core.AssociationEnd.association");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.AssociationRole))
        continue;

      uml.parser.xmi.metamodel.AssociationRole xmiAssociationRole = (uml.parser.xmi.metamodel.AssociationRole)xmiElement;

      // Get type
      xmiElement = this.getElementXMIRef(associationEndRoleNode, "Foundation.Core.AssociationEnd.type");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.ClassifierRole))
        continue;
      
      uml.parser.xmi.metamodel.ClassifierRole xmiType = (uml.parser.xmi.metamodel.ClassifierRole)xmiElement;

      // Get base (we also accept <code>null</code>)
      xmiElement = this.getElementXMIRef(associationEndRoleNode, "Behavioral_Elements.Collaborations.AssociationEndRole.base");
      if (xmiElement != null && !(xmiElement instanceof uml.parser.xmi.metamodel.AssociationEnd))
        continue;

      uml.parser.xmi.metamodel.AssociationEnd xmiBase = (uml.parser.xmi.metamodel.AssociationEnd)xmiElement;
      uml.parser.xmi.metamodel.AssociationEndRole xmiAssociationEndRole = xmiAssociationRole.createAssociationEndRole(this.getName(associationEndRoleNode), xmiType, xmiBase);
      xmiAssociationEndRoles.add(xmiAssociationEndRole);
      this.addElementNode(xmiAssociationEndRole, associationEndRoleNode);

      util.Message.debug().info(xmiAssociationEndRole.toString());
    }
    return xmiAssociationEndRoles;    
  }

  private List<uml.parser.xmi.metamodel.Interaction> addInteractions() {
    List<uml.parser.xmi.metamodel.Interaction> xmiInteractions = new ArrayList<uml.parser.xmi.metamodel.Interaction>();
    for (Node interactionNode : this.getXMINodes("Behavioral_Elements.Collaborations.Interaction")) {
      // Get owning collaboration
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(interactionNode, "Behavioral_Elements.Collaborations.Interaction.context");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Collaboration))
        continue;

      uml.parser.xmi.metamodel.Collaboration xmiCollaboration = (uml.parser.xmi.metamodel.Collaboration)xmiElement;
      uml.parser.xmi.metamodel.Interaction xmiInteraction = xmiCollaboration.createInteraction(this.getName(interactionNode));
      xmiInteractions.add(xmiInteraction);
      this.addElementNode(xmiInteraction, interactionNode);

      util.Message.debug().info(xmiInteraction.toString());
    }
    return xmiInteractions;
  }

  private List<uml.parser.xmi.metamodel.Message> addMessages() {
    List<uml.parser.xmi.metamodel.Message> xmiMessages = new ArrayList<uml.parser.xmi.metamodel.Message>();
    for (Node messageNode : this.getXMINodes("Behavioral_Elements.Collaborations.Message")) {
      // Get owning interaction
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(messageNode, "Behavioral_Elements.Collaborations.Message.interaction");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Interaction))
        continue;
      uml.parser.xmi.metamodel.Interaction xmiInteraction = (uml.parser.xmi.metamodel.Interaction)xmiElement;

      // Get sender
      xmiElement = this.getElementXMIRef(messageNode, "Behavioral_Elements.Collaborations.Message.sender");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.ClassifierRole))
        continue;
      uml.parser.xmi.metamodel.ClassifierRole xmiSender = (uml.parser.xmi.metamodel.ClassifierRole)xmiElement;

      // Get receiver
      xmiElement = this.getElementXMIRef(messageNode, "Behavioral_Elements.Collaborations.Message.receiver");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.ClassifierRole))
        continue;
      uml.parser.xmi.metamodel.ClassifierRole xmiReceiver = (uml.parser.xmi.metamodel.ClassifierRole)xmiElement;

      // Get action
      uml.parser.xmi.metamodel.Action xmiAction = this.getAction(messageNode, "Behavioral_Elements.Collaborations.Message.action");
      if (xmiAction == null)
        continue;
      if (xmiAction.getScript() == null || xmiAction.getScript().trim().equals("")) {
        String script = this.getName(messageNode);
        if (script == null)
          script = "";
        xmiAction = new uml.parser.xmi.metamodel.Action(script);
      }

      uml.parser.xmi.metamodel.Message xmiMessage = xmiInteraction.createMessage(this.getName(messageNode), xmiSender, xmiAction, xmiReceiver);
      xmiMessages.add(xmiMessage);
      this.addElementNode(xmiMessage, messageNode);
    }

    for (uml.parser.xmi.metamodel.Message xmiMessage : xmiMessages) {
      Node messageNode = this.getNode(xmiMessage);
      if (messageNode == null)
        continue;

      for (uml.parser.xmi.metamodel.Element xmiElement : this.getElementXMIRefs(messageNode, "Behavioral_Elements.Collaborations.Message.predecessor")) {
        if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Message))
          continue;
        uml.parser.xmi.metamodel.Message predecessorXMIMessage = (uml.parser.xmi.metamodel.Message)xmiElement;
        xmiMessage.addPredecessor(predecessorXMIMessage);
      }

      util.Message.debug().info(xmiMessage.toString());
    }

    return xmiMessages;    
  }

  private List<uml.parser.xmi.metamodel.Object> addObjects(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Object> xmiObjects = new ArrayList<uml.parser.xmi.metamodel.Object>();
    for (Node objectNode : this.getXMINodes("Behavioral_Elements.Common_Behavior.Object")) {
      // Get classifier
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIRef(objectNode, "Behavioral_Elements.Common_Behavior.Instance.classifier");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Class))
        continue;

      uml.parser.xmi.metamodel.Class xmiClass = (uml.parser.xmi.metamodel.Class)xmiElement;
      uml.parser.xmi.metamodel.Object xmiObject = xmiModel.createObject(this.getName(objectNode), xmiClass);
      xmiObjects.add(xmiObject);
      this.addElementNode(xmiObject, objectNode);

      util.Message.debug().info(xmiObject.toString());
    }
    return xmiObjects;
  }

  private List<uml.parser.xmi.metamodel.Link> addLinks(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Link> xmiLinks = new ArrayList<uml.parser.xmi.metamodel.Link>();
    for (Node objectNode : this.getXMINodes("Behavioral_Elements.Common_Behavior.Link")) {
      // Get association
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIRef(objectNode, "Behavioral_Elements.Common_Behavior.Link.association");
      if (xmiElement != null && !(xmiElement instanceof uml.parser.xmi.metamodel.Association))
        continue;

      uml.parser.xmi.metamodel.Association xmiAssociation = (uml.parser.xmi.metamodel.Association)xmiElement;
      uml.parser.xmi.metamodel.Link xmiLink = xmiModel.createLink(this.getName(objectNode), xmiAssociation);
      xmiLinks.add(xmiLink);
      this.addElementNode(xmiLink, objectNode);

      util.Message.debug().info(xmiLink.toString());
    }
    return xmiLinks;
  }

  private List<uml.parser.xmi.metamodel.LinkEnd> addLinkEnds() {
    List<uml.parser.xmi.metamodel.LinkEnd> xmiLinkEnds = new ArrayList<uml.parser.xmi.metamodel.LinkEnd>();
    for (Node linkEndNode : this.getXMINodes("Behavioral_Elements.Common_Behavior.LinkEnd")) {
      // Get owning link
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(linkEndNode, "Behavioral_Elements.Common_Behavior.LinkEnd.link");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Link))
        continue;

      uml.parser.xmi.metamodel.Link xmiLink = (uml.parser.xmi.metamodel.Link)xmiElement;

      // Get instance
      xmiElement = this.getElementXMIRef(linkEndNode, "Behavioral_Elements.Common_Behavior.LinkEnd.instance");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Object))
        continue;
      
      uml.parser.xmi.metamodel.Object xmiObject = (uml.parser.xmi.metamodel.Object)xmiElement;
      uml.parser.xmi.metamodel.LinkEnd xmiLinkEnd = xmiLink.createLinkEnd(this.getName(linkEndNode), xmiObject);
      xmiLinkEnds.add(xmiLinkEnd);
      this.addElementNode(xmiLinkEnd, linkEndNode);

      util.Message.debug().info(xmiLinkEnd.toString());
    }
    return xmiLinkEnds;    
  }

  private List<uml.parser.xmi.metamodel.Stimulus> addStimuli() {
    List<uml.parser.xmi.metamodel.Stimulus> xmiStimuli = new ArrayList<uml.parser.xmi.metamodel.Stimulus>();
    for (Node stimulusNode : this.getXMINodes("Behavioral_Elements.Common_Behavior.Stimulus")) {
      uml.parser.xmi.metamodel.Element xmiElement = null;

      // Get owning link
      xmiElement = this.getElementXMIRef(stimulusNode, "Behavioral_Elements.Common_Behavior.Stimulus.communicationLink");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Link))
        continue;

      uml.parser.xmi.metamodel.Link xmiLink = (uml.parser.xmi.metamodel.Link)xmiElement;

      // Get sending instance
      xmiElement = this.getElementXMIRef(stimulusNode, "Behavioral_Elements.Common_Behavior.Stimulus.sender");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Object))
        continue;

      uml.parser.xmi.metamodel.Object xmiSender = (uml.parser.xmi.metamodel.Object)xmiElement;

      // Get receiving instance
      xmiElement = this.getElementXMIRef(stimulusNode, "Behavioral_Elements.Common_Behavior.Stimulus.receiver");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Object))
        continue;

      uml.parser.xmi.metamodel.Object xmiReceiver = (uml.parser.xmi.metamodel.Object)xmiElement;
      
      uml.parser.xmi.metamodel.Stimulus xmiStimulus = xmiLink.createStimulus(this.getName(stimulusNode), xmiSender, xmiReceiver);
      xmiStimuli.add(xmiStimulus);
      this.addElementNode(xmiStimulus, stimulusNode);

      util.Message.debug().info(xmiStimulus.toString());
    }
    return xmiStimuli;    
  }

  private List<uml.parser.xmi.metamodel.Constraint> addConstraints() {
    List<uml.parser.xmi.metamodel.Constraint> xmiConstraints = new ArrayList<uml.parser.xmi.metamodel.Constraint>();
    for (Node constraintNode : this.getXMINodes("Foundation.Core.Constraint")) {
      // Get model element
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(constraintNode, "Foundation.Core.Constraint.constrainedElement");
      if (xmiElement == null)
        continue;

      // Get body
      Node constraintBodyNode = this.getChild(constraintNode, "Foundation.Core.Constraint.body");
      if (constraintBodyNode == null)
        continue;
      Node expressionDataNode = getDescendant(constraintBodyNode, "Foundation.Data_Types.Expression.body");
      if (expressionDataNode == null)
        continue;
      String constraintBody = expressionDataNode.getTextContent();

      uml.parser.xmi.metamodel.Constraint xmiConstraint = new uml.parser.xmi.metamodel.Constraint(this.getName(constraintNode), constraintBody);
      xmiConstraints.add(xmiConstraint);
      xmiElement.addConstraint(xmiConstraint);
      util.Message.debug().info(xmiConstraint.toString());
    }
    return xmiConstraints;
  }

  private List<uml.parser.xmi.metamodel.TaggedValue> addTaggedValues() {
    List<uml.parser.xmi.metamodel.TaggedValue> xmiTaggedValues = new ArrayList<uml.parser.xmi.metamodel.TaggedValue>();
    for (Node taggedValueNode : this.getXMINodes("Foundation.Extension_Mechanisms.TaggedValue")) {
      // Get model element
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(taggedValueNode, "Foundation.Extension_Mechanisms.TaggedValue.modelElement");
      if (xmiElement == null)
        continue;

      // Get tag
      Node taggedValueTagNode = this.getChild(taggedValueNode, "Foundation.Extension_Mechanisms.TaggedValue.tag");
      if (taggedValueTagNode == null)
        continue;
      String taggedValueTag = this.getElementString(taggedValueTagNode);
      if (taggedValueTag == null)
        continue;

      // Get value
      Node taggedValueValueNode = this.getChild(taggedValueNode, "Foundation.Extension_Mechanisms.TaggedValue.value");
      if (taggedValueValueNode == null)
        continue;
      String taggedValueValue = this.getElementString(taggedValueValueNode);
      if (taggedValueValue == null)
        continue;

      uml.parser.xmi.metamodel.TaggedValue xmiTaggedValue = new uml.parser.xmi.metamodel.TaggedValue(taggedValueTag, taggedValueValue);
      xmiTaggedValues.add(xmiTaggedValue);
      xmiElement.addTaggedValue(xmiTaggedValue);
      util.Message.debug().info(xmiTaggedValue.toString());
    }
    return xmiTaggedValues;
  }
}
