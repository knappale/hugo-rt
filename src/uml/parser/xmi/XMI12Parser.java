package uml.parser.xmi;

import java.util.*;
import org.w3c.dom.*;


/**
 * XMI 1.2 parser.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class XMI12Parser extends XMIParser {
  private static final String XMIID = "xmi.id";
  private static final String XMIIDREF = "xmi.idref";

  private uml.parser.xmi.metamodel.Model xmiModel = null;
  private List<uml.parser.xmi.metamodel.State> xmiStates = null;
  private List<uml.parser.xmi.metamodel.Event> xmiEvents = null;
  private List<uml.parser.xmi.metamodel.Action> xmiActions = null;

  private static java.util.logging.Logger DEBUG = util.Message.debug();

  public XMI12Parser() {
    this.reset(null);
  }

  private void reset(Document document) {
    super.reset(document, XMIID, XMIIDREF);
    this.xmiModel = null;
    this.xmiStates = null;
    this.xmiEvents = null;
    this.xmiActions = null;
  }

  public uml.parser.xmi.metamodel.Model parse(Document document) throws uml.UModelException {
    this.reset(document);

    uml.parser.xmi.metamodel.Model xmiModel = this.getModel();
    if (xmiModel == null)
      throw new uml.UModelException("No model in XMI 1.2 document found");

    util.Message.info("Parsing model `" + xmiModel.getName() + "' from XMI 1.2 document");
    this.addClasses(xmiModel);
    this.addDataTypes(xmiModel);
    this.addSignals(xmiModel);

    this.addAssociations(xmiModel);
    this.addAssociationEnds();

    this.addAttributes();
    this.addOperations();
    this.addReceptions();
    this.addParameters();

    this.addStateMachines();
    this.addStates();
    this.addTransitions();

    this.addCollaborations(xmiModel);
    this.addClassifierRoles();

    this.addAssociationRoles();
    this.addAssociationEndRoles();

    this.addInteractions();
    this.addMessages();

    this.addObjects(xmiModel);
    this.addLinks(xmiModel);
    this.addLinkEnds();
    this.addStimuli();
      
    this.addConstraints();
    this.addTaggedValues();

    return xmiModel;
  }

  private String getName(Node xmiNode) {
    NamedNodeMap attributes = xmiNode.getAttributes();
    if (attributes != null) {
      Node nameNode = attributes.getNamedItem("name");
      if (nameNode != null) {
        String nameNodeValue = nameNode.getNodeValue();
        if (nameNodeValue != null)
          return nameNodeValue;
      }
    }
    return "";
  }

  private List<uml.parser.xmi.metamodel.Event> getElementEvents(Node node, String name) {
    List<uml.parser.xmi.metamodel.Event> xmiEvents = new ArrayList<uml.parser.xmi.metamodel.Event>();

    List<Node> eventNodes = getChildren(node, name);
    if (eventNodes == null || eventNodes.isEmpty())
      return xmiEvents;

    for (uml.parser.xmi.metamodel.Event xmiEvent : this.getEvents()) {
      for (Node eventNode : eventNodes) {
        if (getNode(xmiEvent) != null && this.isContained(getNode(xmiEvent), eventNode))
          xmiEvents.add(xmiEvent);
      }
    }

    for (uml.parser.xmi.metamodel.Element xmiElement : this.getElementXMIRefs(node, name)) {
      if (xmiElement != null && (xmiElement instanceof uml.parser.xmi.metamodel.Event))
        xmiEvents.add((uml.parser.xmi.metamodel.Event)xmiElement);
    }

    return xmiEvents;
  }

  private uml.parser.xmi.metamodel.Model getModel() {
    if (this.xmiModel != null)
      return this.xmiModel;

    List<uml.parser.xmi.metamodel.Model> xmiModels = new ArrayList<uml.parser.xmi.metamodel.Model>();
    for (Node modelNode : this.getXMINodes("UML:Model")) {
      uml.parser.xmi.metamodel.Model xmiModel = new uml.parser.xmi.metamodel.Model(this.getName(modelNode));
      xmiModels.add(xmiModel);
      this.addElementNode(xmiModel, modelNode);
    }
    if (xmiModels.isEmpty())
      return null;
    return xmiModels.get(0);
  }

  private List<uml.parser.xmi.metamodel.Class> addClasses(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Class> xmiClasses = new ArrayList<uml.parser.xmi.metamodel.Class>();
    Node modelNode = this.getNode(xmiModel);
    if (modelNode == null)
      return xmiClasses;

    for (Node classNode : this.getXMINodes("UML:Class")) {
      if (this.isContained(classNode, modelNode)) {
        uml.parser.xmi.metamodel.Class xmiClass = xmiModel.createClass(this.getName(classNode));
        xmiClasses.add(xmiClass);
        this.addElementNode(xmiClass, classNode);
        DEBUG.info(xmiClass.toString());
      }
    }
    return xmiClasses;
  }

  private List<uml.parser.xmi.metamodel.DataType> addDataTypes(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.DataType> xmiDataTypes = new ArrayList<uml.parser.xmi.metamodel.DataType>();
    Node modelNode = this.getNode(xmiModel);
    if (modelNode == null)
      return xmiDataTypes;

    for (Node dataTypeNode : this.getXMINodes("UML:DataType")) {
      if (this.isContained(dataTypeNode, modelNode)) {
        uml.parser.xmi.metamodel.DataType xmiDataType = xmiModel.createDataType(this.getName(dataTypeNode));
        xmiDataTypes.add(xmiDataType);
        this.addElementNode(xmiDataType, dataTypeNode);
        DEBUG.info(xmiDataType.toString());
      }
    }
    return xmiDataTypes;
  }

  private List<uml.parser.xmi.metamodel.Signal> addSignals(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Signal> xmiSignals = new ArrayList<uml.parser.xmi.metamodel.Signal>();
    Node modelNode = this.getNode(xmiModel);
    if (modelNode == null)
      return xmiSignals;

    for (Node signalNode : this.getXMINodes("UML:Signal")) {
      if (this.isContained(signalNode, modelNode)) {
        uml.parser.xmi.metamodel.Signal xmiSignal = xmiModel.createSignal(this.getName(signalNode));
        xmiSignals.add(xmiSignal);
        this.addElementNode(xmiSignal, signalNode);
        DEBUG.info(xmiSignal.toString());
      }
    }
    return xmiSignals;
  }

  private List<uml.parser.xmi.metamodel.Association> addAssociations(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Association> xmiAssociations = new ArrayList<uml.parser.xmi.metamodel.Association>();
    Node modelNode = this.getNode(xmiModel);
    if (modelNode == null)
      return xmiAssociations;

    for (Node associationNode : this.getXMINodes("UML:Association")) {
      if (this.isContained(associationNode, modelNode)) {
        uml.parser.xmi.metamodel.Association xmiAssociation = xmiModel.createAssociation(this.getName(associationNode));
        xmiAssociations.add(xmiAssociation);
        this.addElementNode(xmiAssociation, associationNode);
        DEBUG.info(xmiAssociation.toString());
      }
    }
    return xmiAssociations;
  }

  private List<uml.parser.xmi.metamodel.AssociationEnd> addAssociationEnds() {
    List<uml.parser.xmi.metamodel.AssociationEnd> xmiAssociationEnds = new ArrayList<uml.parser.xmi.metamodel.AssociationEnd>();
    for (Node associationEndNode : this.getXMINodes("UML:AssociationEnd")) {
      // Get owning association
      uml.parser.xmi.metamodel.Element xmiElement = this.getAttributeXMIOwner(associationEndNode, "association");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Association))
        continue;

      uml.parser.xmi.metamodel.Association xmiAssociation = (uml.parser.xmi.metamodel.Association)xmiElement;

      // Get type
      xmiElement = this.getXMIRef(associationEndNode, "UML:AssociationEnd.participant", "participant");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;

      uml.parser.xmi.metamodel.Classifier xmiType = (uml.parser.xmi.metamodel.Classifier)xmiElement;
      uml.parser.xmi.metamodel.AssociationEnd xmiAssociationEnd = xmiAssociation.createAssociationEnd(this.getName(associationEndNode), xmiType);
      xmiAssociationEnds.add(xmiAssociationEnd);
      this.addElementNode(xmiAssociationEnd, associationEndNode);

      // Get association end's multiplicity
      Node multiplicityNode = getDescendant(associationEndNode, "UML:MultiplicityRange");
      if (multiplicityNode != null) {
        xmiAssociationEnd.setUpperMultiplicity(getAttributeInt(multiplicityNode, "upper", xmiAssociationEnd.getUpperMultiplicity()));
      }

      // Get association end's navigability
      String navigability = getAttributeString(associationEndNode, "isNavigable");
      if ("false".equals(navigability))
        xmiAssociationEnd.setNavigable(false);

      DEBUG.info(xmiAssociationEnd.toString());
    }
    return xmiAssociationEnds;    
  }

  private List<uml.parser.xmi.metamodel.Attribute> addAttributes() {
    List<uml.parser.xmi.metamodel.Attribute> xmiAttributes = new ArrayList<uml.parser.xmi.metamodel.Attribute>();
    for (Node attributeNode : this.getXMINodes("UML:Attribute")) {
      // Get owning class
      uml.parser.xmi.metamodel.Element xmiElement = this.getAttributeXMIOwner(attributeNode, "owner");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;
      uml.parser.xmi.metamodel.Classifier xmiClassifier = (uml.parser.xmi.metamodel.Class)xmiElement;

      // Get type
      xmiElement = this.getXMIRef(attributeNode, "UML:StructuralFeature.type", "type");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;
      uml.parser.xmi.metamodel.Classifier xmiType = (uml.parser.xmi.metamodel.Classifier)xmiElement;

      uml.parser.xmi.metamodel.Attribute xmiAttribute = xmiClassifier.createAttribute(this.getName(attributeNode), xmiType);
      xmiAttributes.add(xmiAttribute);
      this.addElementNode(xmiAttribute, attributeNode);

      // Get attribute's multiplicity
      Node multiplicityNode = getDescendant(attributeNode, "UML:MultiplicityRange");
      if (multiplicityNode != null) {
        xmiAttribute.setUpperMultiplicity(getAttributeInt(multiplicityNode, "upper", xmiAttribute.getUpperMultiplicity()));
      }

      // Get whether attribute is constant
      String changeability = getAttributeString(attributeNode, "changeability");
      if ("frozen".equals(changeability))
        xmiAttribute.setFinal(true);

      // Get whether attribute is static
      String ownerScope = getAttributeString(attributeNode, "ownerScope");
      if ("classifier".equals(ownerScope))
        xmiAttribute.setStatic(true);

      // Get initial value
      Node initialValueNode = getChild(attributeNode, "UML:Attribute.initialValue");
      if (initialValueNode != null) {
        Node expressionNode = getDescendant(initialValueNode, "UML:Expression");
        if (expressionNode != null) {
          xmiAttribute.addInitialValue(getAttributeString(expressionNode, "body"));
        }
      }

      DEBUG.info(xmiAttribute.toString());
    }
    return xmiAttributes;    
  }

  private List<uml.parser.xmi.metamodel.Operation> addOperations() {
    List<uml.parser.xmi.metamodel.Operation> xmiOperations = new ArrayList<uml.parser.xmi.metamodel.Operation>();
    for (Node operationNode : this.getXMINodes("UML:Operation")) {
      // Get owning class
      uml.parser.xmi.metamodel.Element xmiElement = this.getAttributeXMIOwner(operationNode, "owner");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;

      uml.parser.xmi.metamodel.Classifier xmiClassifier = (uml.parser.xmi.metamodel.Class)xmiElement;
      uml.parser.xmi.metamodel.Operation xmiOperation = xmiClassifier.createOperation(this.getName(operationNode));
      xmiOperations.add(xmiOperation);
      this.addElementNode(xmiOperation, operationNode);

      DEBUG.info(xmiOperation.toString());
    }
    return xmiOperations;    
  }

  private List<uml.parser.xmi.metamodel.Reception> addReceptions() {
    List<uml.parser.xmi.metamodel.Reception> xmiReceptions = new ArrayList<uml.parser.xmi.metamodel.Reception>();
    for (Node receptionNode : this.getXMINodes("UML:Reception")) {
      // Get owning class
      uml.parser.xmi.metamodel.Element xmiElement = this.getAttributeXMIOwner(receptionNode, "owner");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;

      uml.parser.xmi.metamodel.Classifier xmiClassifier = (uml.parser.xmi.metamodel.Class)xmiElement;

      // Get type
      xmiElement = this.getXMIRef(receptionNode, "UML:Reception.signal", "signal");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Signal))
        continue;
      
      uml.parser.xmi.metamodel.Signal xmiSignal = (uml.parser.xmi.metamodel.Signal)xmiElement;
      uml.parser.xmi.metamodel.Reception xmiReception = xmiClassifier.createReception(this.getName(receptionNode), xmiSignal);
      xmiReceptions.add(xmiReception);
      this.addElementNode(xmiReception, receptionNode);

      DEBUG.info(xmiReception.toString());
    }
    return xmiReceptions;    
  }

  private List<uml.parser.xmi.metamodel.Parameter> addParameters() {
    List<uml.parser.xmi.metamodel.Parameter> xmiParameters = new ArrayList<uml.parser.xmi.metamodel.Parameter>();
    for (Node parameterNode : this.getXMINodes("UML:Parameter")) {
      // Get owning behavioural (feature)
      uml.parser.xmi.metamodel.Element xmiElement = this.getAttributeXMIOwner(parameterNode, "behavioralFeature");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Behavioural))
        continue;

      uml.parser.xmi.metamodel.Behavioural xmiBehavioural = (uml.parser.xmi.metamodel.Behavioural)xmiElement;

      // Get type
      xmiElement = this.getXMIRef(parameterNode, "UML:Parameter.type", "type");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier))
        continue;

      uml.parser.xmi.metamodel.Classifier xmiClassifier = (uml.parser.xmi.metamodel.Classifier)xmiElement;
      
      // Get parameter direction kind
      uml.parser.xmi.metamodel.ParameterDirectionKind kind = uml.parser.xmi.metamodel.ParameterDirectionKind.IN;
      String kindString = getAttributeString(parameterNode, "kind");
      if (kindString != null) {
        kindString = kindString.toLowerCase().trim();
        if ("in".equals(kindString))
          kind = uml.parser.xmi.metamodel.ParameterDirectionKind.IN;
        if ("inout".equals(kindString))
          kind = uml.parser.xmi.metamodel.ParameterDirectionKind.INOUT;
        if ("out".equals(kindString))
          kind = uml.parser.xmi.metamodel.ParameterDirectionKind.OUT;
        if ("return".equals(kindString))
          kind = uml.parser.xmi.metamodel.ParameterDirectionKind.RETURN;
      }

      uml.parser.xmi.metamodel.Parameter xmiParameter = xmiBehavioural.createParameter(this.getName(parameterNode), kind, xmiClassifier);
      xmiParameters.add(xmiParameter);
      this.addElementNode(xmiParameter, parameterNode);
      
      DEBUG.info(xmiParameter.toString());
    }
    return xmiParameters;    
  }

  private List<uml.parser.xmi.metamodel.StateMachine> addStateMachines() {
    List<uml.parser.xmi.metamodel.StateMachine> xmiStateMachines = new ArrayList<uml.parser.xmi.metamodel.StateMachine>();
    for (Node stateMachineNode : this.getXMINodes("UML:StateMachine")) {
      // Get owning class
      uml.parser.xmi.metamodel.Element xmiElement = this.getXMIOwner(stateMachineNode, "UML:StateMachine.context", "context");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Class))
        continue;

      uml.parser.xmi.metamodel.Class xmiClass = (uml.parser.xmi.metamodel.Class)xmiElement;
      uml.parser.xmi.metamodel.StateMachine xmiStateMachine = xmiClass.createStateMachine(this.getName(stateMachineNode));
      xmiStateMachines.add(xmiStateMachine);
      this.addElementNode(xmiStateMachine, stateMachineNode);

      DEBUG.info(xmiStateMachine.toString());
    }
    return xmiStateMachines;    
  }

  private List<uml.parser.xmi.metamodel.Event> getEvents() {
    if (this.xmiEvents != null)
      return this.xmiEvents;

    this.xmiEvents = new ArrayList<uml.parser.xmi.metamodel.Event>();

    for (Node signalEventNode : this.getXMINodes("UML:SignalEvent")) {
      uml.parser.xmi.metamodel.Event xmiSignalEvent = new uml.parser.xmi.metamodel.Event(this.getName(signalEventNode), uml.parser.xmi.metamodel.EventKind.SIGNAL);
      this.xmiEvents.add(xmiSignalEvent);
      this.addElementNode(xmiSignalEvent, signalEventNode);

      uml.parser.xmi.metamodel.Element xmiElement = this.getAttributeXMIRef(signalEventNode, "signal");
      if (xmiElement != null && (xmiElement instanceof uml.parser.xmi.metamodel.Signal))
        xmiSignalEvent.setSignal((uml.parser.xmi.metamodel.Signal)xmiElement);
    }

    for (Node callEventNode : this.getXMINodes("UML:CallEvent")) {
      uml.parser.xmi.metamodel.Event xmiCallEvent = new uml.parser.xmi.metamodel.Event(this.getName(callEventNode), uml.parser.xmi.metamodel.EventKind.CALL);
      this.xmiEvents.add(xmiCallEvent);
      this.addElementNode(xmiCallEvent, callEventNode);

      uml.parser.xmi.metamodel.Element xmiElement = this.getAttributeXMIRef(callEventNode, "operation");
      if (xmiElement != null && (xmiElement instanceof uml.parser.xmi.metamodel.Operation))
        xmiCallEvent.setOperation((uml.parser.xmi.metamodel.Operation)xmiElement);
    }

    for (Node timeEventNode : this.getXMINodes("UML:TimeEvent")) {
      uml.parser.xmi.metamodel.Event xmiTimeEvent = new uml.parser.xmi.metamodel.Event(this.getName(timeEventNode), uml.parser.xmi.metamodel.EventKind.TIME);
      this.xmiEvents.add(xmiTimeEvent);
      this.addElementNode(xmiTimeEvent, timeEventNode);

      // Get when expression
      Node whenNode = getChild(timeEventNode, "UML:TimeEvent.when");
      if (whenNode != null) {
        Node expressionNode = getDescendant(whenNode, "UML:Expression");
        if (expressionNode != null) {
          xmiTimeEvent.setWhen(this.getAttributeString(expressionNode, "body"));
        }
      }
    }

    return this.xmiEvents;
  }

  private uml.parser.xmi.metamodel.Event getEvent(Node node, String elementName, String attributeName) {
    // First get all events
    this.getEvents();

    uml.parser.xmi.metamodel.Element xmiElement = this.getXMIElement(node, elementName, attributeName);
    if (xmiElement instanceof uml.parser.xmi.metamodel.Event)
      return (uml.parser.xmi.metamodel.Event)xmiElement;

    return null;
  }

  private List<uml.parser.xmi.metamodel.Action> getActions() {
    if (this.xmiActions != null)
      return this.xmiActions;

    this.xmiActions = new ArrayList<uml.parser.xmi.metamodel.Action>();
    List<Node> actionNodes = new ArrayList<Node>();
    actionNodes.addAll(this.getXMINodes("UML:Action"));
    actionNodes.addAll(this.getXMINodes("UML:CallAction"));
    actionNodes.addAll(this.getXMINodes("UML:SendAction"));
    actionNodes.addAll(this.getXMINodes("UML:UninterpretedAction"));
    for (Node actionNode : actionNodes) {
      String script = null;

      Node scriptNode = getChild(actionNode, "UML:Action.script");
      if (scriptNode != null) {
        Node expressionNode = getDescendant(actionNode, "UML:ActionExpression");
        if (expressionNode != null) {
          script = this.getAttributeString(expressionNode, "body");
          if (script == null)
            script = "";
        }
      }
      else {
        uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIRef(actionNode, "UML:CallAction.operation");
        if (xmiElement != null && (xmiElement instanceof uml.parser.xmi.metamodel.Operation)) {
          script = xmiElement.getName() + "()";
          util.Message.warning("Parsing action script for call action `" + this.getName(actionNode) + "' as `" + script + "'");
        }
      }

      if (script != null) {
        uml.parser.xmi.metamodel.Action xmiAction = new uml.parser.xmi.metamodel.Action(script);
        this.xmiActions.add(xmiAction);
        this.addElementNode(xmiAction, actionNode);
      }
      else
        util.Message.warning("Ignoring `" + this.getName(actionNode) + "' since it has no action script");
    }

    return this.xmiActions;
  }

  private uml.parser.xmi.metamodel.Action getAction(Node node, String elementName) {
    // First get all actions
    this.getActions();

    uml.parser.xmi.metamodel.Element xmiElement = this.getXMIElement(node, elementName, null);
    if (xmiElement instanceof uml.parser.xmi.metamodel.Action)
      return (uml.parser.xmi.metamodel.Action)xmiElement;

    return null;
  }

  private List<uml.parser.xmi.metamodel.State> getStates() {
    if (this.xmiStates != null)
      return this.xmiStates;

    this.xmiStates = new ArrayList<uml.parser.xmi.metamodel.State>();

    List<Node> simpleStateNodes = new ArrayList<Node>();
    simpleStateNodes.addAll(this.getXMINodes("UML:State"));
    simpleStateNodes.addAll(this.getXMINodes("UML:SimpleState"));
    for (Node simpleStateNode : simpleStateNodes) {
      uml.parser.xmi.metamodel.State xmiState = new uml.parser.xmi.metamodel.State(this.getName(simpleStateNode), uml.parser.xmi.metamodel.StateKind.SIMPLE);
      this.xmiStates.add(xmiState);
      this.addElementNode(xmiState, simpleStateNode);
    }

    for (Node stateNode : this.getXMINodes("UML:FinalState")) {
      uml.parser.xmi.metamodel.State xmiState = new uml.parser.xmi.metamodel.State(this.getName(stateNode), uml.parser.xmi.metamodel.StateKind.FINAL);
      this.xmiStates.add(xmiState);
      this.addElementNode(xmiState, stateNode);
    }

    for (Node stateNode : this.getXMINodes("UML:CompositeState")) {
      uml.parser.xmi.metamodel.State xmiState = new uml.parser.xmi.metamodel.State(this.getName(stateNode), uml.parser.xmi.metamodel.StateKind.COMPOSITE);
      String isConcurrentString = getAttributeString(stateNode, "isConcurrent");
      if (isConcurrentString != null) {
        isConcurrentString = isConcurrentString.toLowerCase().trim();
        if ("true".equals(isConcurrentString))
          xmiState.setConcurrent(true);
      }
      this.xmiStates.add(xmiState);
      this.addElementNode(xmiState, stateNode);
    }

    for (Node stateNode : this.getXMINodes("UML:Pseudostate")) {
      // Get pseudostate kind
      uml.parser.xmi.metamodel.StateKind kind = uml.parser.xmi.metamodel.StateKind.SIMPLE;
      String kindString = getAttributeString(stateNode, "kind");
      if (kindString != null) {
        kindString = kindString.toLowerCase().trim();
        if ("choice".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.CHOICE;
        else if ("deephistory".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.DEEPHISTORY;
        else if ("fork".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.FORK;
        else if ("initial".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.INITIAL;
        else if ("join".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.JOIN;
        else if ("junction".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.JUNCTION;
        else if ("shallowhistory".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.SHALLOWHISTORY;
        else if ("synch".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.SYNCH;
        else if ("final".equals(kindString))
          kind = uml.parser.xmi.metamodel.StateKind.FINAL;
      }
      uml.parser.xmi.metamodel.State xmiState = new uml.parser.xmi.metamodel.State(this.getName(stateNode), kind);
      this.xmiStates.add(xmiState);
      this.addElementNode(xmiState, stateNode);
    }

    // Add deferrable events
    for (uml.parser.xmi.metamodel.State xmiState : this.xmiStates) {
      Node stateNode = this.getNode(xmiState);
      for (uml.parser.xmi.metamodel.Event xmiEvent : this.getElementEvents(stateNode, "UML:State.deferrableEvent")) {
        if (xmiEvent != null)
          xmiState.addDeferrableEvent(xmiEvent);
      }
    }

    // Get entry actions
    for (uml.parser.xmi.metamodel.State xmiState : this.xmiStates) {
      Node stateNode = this.getNode(xmiState);
      uml.parser.xmi.metamodel.Action xmiAction = this.getAction(stateNode, "UML:State.entry");
      if (xmiAction != null)
        xmiState.setEntry(xmiAction);
    }

    // Get exit actions
    for (uml.parser.xmi.metamodel.State xmiState : this.xmiStates) {
      Node stateNode = this.getNode(xmiState);
      uml.parser.xmi.metamodel.Action xmiAction = getAction(stateNode, "UML:State.exit");
      if (xmiAction != null)
        xmiState.setExit(xmiAction);
    }

    return this.xmiStates;
  }

  private List<uml.parser.xmi.metamodel.State> addStates() {
    List<uml.parser.xmi.metamodel.State> xmiStates = this.getStates();
    for (uml.parser.xmi.metamodel.State xmiState : xmiStates) {
      Node stateNode = this.getNode(xmiState);
      if (stateNode == null)
        continue;
      uml.parser.xmi.metamodel.Element xmiElement = null;

      // Get owning state machine (for top states)
      xmiElement = this.getAttributeXMIOwner(stateNode, "stateMachine");
      if (xmiElement != null && (xmiElement instanceof uml.parser.xmi.metamodel.StateMachine)) {
        uml.parser.xmi.metamodel.StateMachine xmiStateMachine = (uml.parser.xmi.metamodel.StateMachine)xmiElement;
        xmiState.setStateMachine(xmiStateMachine);
        continue;
      }

      // Get container state (for non-top states)
      xmiElement = this.getAttributeXMIOwner(stateNode, "container");
      if (xmiElement != null && (xmiElement instanceof uml.parser.xmi.metamodel.State)) {
        uml.parser.xmi.metamodel.State xmiContainer = (uml.parser.xmi.metamodel.State)xmiElement;
        xmiState.setContainer(xmiContainer);
      }

      DEBUG.info(xmiState.toString());
    }
    return xmiStates;
  }

  private List<uml.parser.xmi.metamodel.Transition> addTransitions() {
    List<uml.parser.xmi.metamodel.Transition> xmiTransitions = new ArrayList<uml.parser.xmi.metamodel.Transition>();
    for (Node transitionNode : this.getXMINodes("UML:Transition")) {
      uml.parser.xmi.metamodel.Element xmiElement = null;

      // Get owning state machine
      xmiElement = this.getAttributeXMIOwner(transitionNode, "stateMachine");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.StateMachine))
        continue;

      uml.parser.xmi.metamodel.StateMachine xmiStateMachine = (uml.parser.xmi.metamodel.StateMachine)xmiElement;

      // Get source state
      xmiElement = this.getXMIRef(transitionNode, "UML:Transition.source", "source");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.State))
        continue;

      uml.parser.xmi.metamodel.State xmiSourceState = (uml.parser.xmi.metamodel.State)xmiElement;

      // Get target state
      xmiElement = this.getXMIRef(transitionNode, "UML:Transition.target", "target");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.State))
        continue;

      uml.parser.xmi.metamodel.State xmiTargetState = (uml.parser.xmi.metamodel.State)xmiElement;
      
      uml.parser.xmi.metamodel.Transition xmiTransition = xmiStateMachine.createTransition(this.getName(transitionNode), xmiSourceState, xmiTargetState);
      xmiTransitions.add(xmiTransition);
      this.addElementNode(xmiTransition, transitionNode);

      // Get trigger
      uml.parser.xmi.metamodel.Event xmiEvent = this.getEvent(transitionNode, "UML:Transition.trigger", "trigger");
      if (xmiEvent != null) {
        xmiTransition.setTrigger(xmiEvent);
      }

      // Get guard
      Node guardNode = getChild(transitionNode, "UML:Transition.guard");
      if (guardNode != null) {
        Node expressionNode = getDescendant(guardNode, "UML:BooleanExpression");
        if (expressionNode != null)
          xmiTransition.setGuard(this.getAttributeString(expressionNode, "body"));
      }

      // Get effect
      uml.parser.xmi.metamodel.Action xmiAction = getAction(transitionNode, "UML:Transition.effect");
      if (xmiAction != null)
        xmiTransition.setEffect(xmiAction);

      DEBUG.info(xmiTransition.toString());
    }
    return xmiTransitions;
  }
  
  private List<uml.parser.xmi.metamodel.Collaboration> addCollaborations(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Collaboration> xmiCollaborations = new ArrayList<uml.parser.xmi.metamodel.Collaboration>();
    Node modelNode = this.getNode(xmiModel);
    if (modelNode == null)
      return xmiCollaborations;

    for (Node collaborationNode : this.getXMINodes("UML:Collaboration")) {
      if (this.isContained(collaborationNode, modelNode)) {
        uml.parser.xmi.metamodel.Collaboration xmiCollaboration = xmiModel.createCollaboration(this.getName(collaborationNode));
        xmiCollaborations.add(xmiCollaboration);
        this.addElementNode(xmiCollaboration, collaborationNode);
        DEBUG.info(xmiCollaboration.toString());
      }
    }
    return xmiCollaborations;
  }

  private List<uml.parser.xmi.metamodel.ClassifierRole> addClassifierRoles() {
    List<uml.parser.xmi.metamodel.ClassifierRole> xmiClassifierRoles = new ArrayList<uml.parser.xmi.metamodel.ClassifierRole>();
    for (Node classifierRoleNode : this.getXMINodes("UML:ClassifierRole", "UML:Object")) {
      // Get owning collaboration
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(classifierRoleNode, null);
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Collaboration))
        continue;

      uml.parser.xmi.metamodel.Collaboration xmiCollaboration = (uml.parser.xmi.metamodel.Collaboration)xmiElement;

      // Get base
      xmiElement = this.getElementXMIRef(classifierRoleNode, "UML:ClassifierRole.base", "UML:Instance.classifier");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Classifier)) {
        util.Message.warning("Ignoring classifier role `" + this.getName(classifierRoleNode) + "' because it has no associated classifier");
        continue;
      }

      uml.parser.xmi.metamodel.Classifier xmiBase = (uml.parser.xmi.metamodel.Classifier)xmiElement;
      uml.parser.xmi.metamodel.ClassifierRole xmiClassifierRole = xmiCollaboration.createClassifierRole(this.getName(classifierRoleNode), xmiBase);
      xmiClassifierRoles.add(xmiClassifierRole);
      this.addElementNode(xmiClassifierRole, classifierRoleNode);

      DEBUG.info(xmiClassifierRole.toString());
    }
    return xmiClassifierRoles;
  }

  private List<uml.parser.xmi.metamodel.AssociationRole> addAssociationRoles() {
    List<uml.parser.xmi.metamodel.AssociationRole> xmiAssociationRoles = new ArrayList<uml.parser.xmi.metamodel.AssociationRole>();
    for (Node associationRoleNode : this.getXMINodes("UML:AssociationRole", "UML:Link")) {
      // Get owning collaboration
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(associationRoleNode, null);
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Collaboration))
        continue;

      uml.parser.xmi.metamodel.Collaboration xmiCollaboration = (uml.parser.xmi.metamodel.Collaboration)xmiElement;

      // Get base
      xmiElement = this.getElementXMIRef(associationRoleNode, "UML:AssociationRole.base");
      if (xmiElement != null && !(xmiElement instanceof uml.parser.xmi.metamodel.Association))
        continue;

      uml.parser.xmi.metamodel.Association xmiBase = (uml.parser.xmi.metamodel.Association)xmiElement;
      uml.parser.xmi.metamodel.AssociationRole xmiAssociationRole = xmiCollaboration.createAssociationRole(this.getName(associationRoleNode), xmiBase);
      xmiAssociationRoles.add(xmiAssociationRole);
      this.addElementNode(xmiAssociationRole, associationRoleNode);

      DEBUG.info(xmiAssociationRole.toString());
    }
    return xmiAssociationRoles;
  }

  private List<uml.parser.xmi.metamodel.AssociationEndRole> addAssociationEndRoles() {
    List<uml.parser.xmi.metamodel.AssociationEndRole> xmiAssociationEndRoles = new ArrayList<uml.parser.xmi.metamodel.AssociationEndRole>();
    for (Node associationEndRoleNode : this.getXMINodes("UML:AssociationEndRole")) {
      // Get owning association role    	
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(associationEndRoleNode, null);
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.AssociationRole))
        continue;

      uml.parser.xmi.metamodel.AssociationRole xmiAssociationRole = (uml.parser.xmi.metamodel.AssociationRole)xmiElement;

      // Get type
      xmiElement = this.getElementXMIRef(associationEndRoleNode, "UML:AssociationEnd.participant");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.ClassifierRole))
        continue;

      uml.parser.xmi.metamodel.ClassifierRole xmiType = (uml.parser.xmi.metamodel.ClassifierRole)xmiElement;

      // Get base
      xmiElement = this.getElementXMIRef(associationEndRoleNode, "UML:AssociationEndRole.base");
      if (xmiElement != null && !(xmiElement instanceof uml.parser.xmi.metamodel.AssociationEnd))
        continue;

      uml.parser.xmi.metamodel.AssociationEnd xmiBase = (uml.parser.xmi.metamodel.AssociationEnd)xmiElement;

      uml.parser.xmi.metamodel.AssociationEndRole xmiAssociationEndRole = xmiAssociationRole.createAssociationEndRole(this.getName(associationEndRoleNode), xmiType, xmiBase);
      xmiAssociationEndRoles.add(xmiAssociationEndRole);
      this.addElementNode(xmiAssociationEndRole, associationEndRoleNode);

      DEBUG.info(xmiAssociationEndRole.toString());
    }
    return xmiAssociationEndRoles;
  }

  private List<uml.parser.xmi.metamodel.Interaction> addInteractions() {
    List<uml.parser.xmi.metamodel.Interaction> xmiInteractions = new ArrayList<uml.parser.xmi.metamodel.Interaction>();
    for (Node interactionNode : this.getXMINodes("UML:Interaction")) {
      // Get owning collaboration
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(interactionNode, null);
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Collaboration)) {
        util.Message.warning("Ignoring interaction `" + this.getName(interactionNode) + "' because it has no owning collaboration");
        continue;
      }

      uml.parser.xmi.metamodel.Collaboration xmiCollaboration = (uml.parser.xmi.metamodel.Collaboration)xmiElement;
      uml.parser.xmi.metamodel.Interaction xmiInteraction = xmiCollaboration.createInteraction(this.getName(interactionNode));
      xmiInteractions.add(xmiInteraction);
      this.addElementNode(xmiInteraction, interactionNode);

      DEBUG.info(xmiInteraction.toString());
    }
    return xmiInteractions;
  }

  private List<uml.parser.xmi.metamodel.Message> addMessages() {
    List<uml.parser.xmi.metamodel.Message> xmiMessages = new ArrayList<uml.parser.xmi.metamodel.Message>();
    for (Node messageNode : this.getXMINodes("UML:Message")) {
      // Get owning interaction
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(messageNode, null);
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Interaction)) {
        util.Message.warning("Ignoring message `" + this.getName(messageNode) + "' because it has no owning interaction");
        continue;
      }
      uml.parser.xmi.metamodel.Interaction xmiInteraction = (uml.parser.xmi.metamodel.Interaction)xmiElement;

      // Get sender
      xmiElement = this.getElementXMIRef(messageNode, "UML:Message.sender");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.ClassifierRole)) {
        util.Message.warning("Ignoring message `" + this.getName(messageNode) + "' since it shows no sender");
        continue;
      }
      uml.parser.xmi.metamodel.ClassifierRole xmiSender = (uml.parser.xmi.metamodel.ClassifierRole)xmiElement;

      // Get receiver
      xmiElement = this.getElementXMIRef(messageNode, "UML:Message.receiver");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.ClassifierRole)) {
        util.Message.warning("Ignoring message `" + this.getName(messageNode) + "' since it shows no receiver");
        continue;
      }
      uml.parser.xmi.metamodel.ClassifierRole xmiReceiver = (uml.parser.xmi.metamodel.ClassifierRole)xmiElement;

      // Get action
      uml.parser.xmi.metamodel.Action xmiAction = this.getAction(messageNode, "UML:Message.action");
      if (xmiAction == null) {
        util.Message.warning("Ignoring message `" + this.getName(messageNode) + "' since it shows no action");
        continue;
      }
      if (xmiAction.getScript() == null || xmiAction.getScript().trim().equals("")) {
        String script = this.getName(messageNode);
        if (script == null)
          script = "";
        xmiAction = new uml.parser.xmi.metamodel.Action(script);
      }

      uml.parser.xmi.metamodel.Message xmiMessage = xmiInteraction.createMessage(this.getName(messageNode), xmiSender, xmiAction, xmiReceiver);
      xmiMessages.add(xmiMessage);
      this.addElementNode(xmiMessage, messageNode);
    }

    for (uml.parser.xmi.metamodel.Message xmiMessage : xmiMessages) {
      Node messageNode = this.getNode(xmiMessage);
      if (messageNode == null)
        continue;
      List<uml.parser.xmi.metamodel.Element> predecessors = new ArrayList<uml.parser.xmi.metamodel.Element>();
      predecessors.addAll(this.getElementXMIRefs(messageNode, "UML:Message.predecessor"));
      // if no predecessor is found use an activator
      // this is necessary to provide compatibility with ArgoUML.
      if (predecessors.size() == 0)
        predecessors.add(this.getElementXMIRef(messageNode, "UML:Message.activator"));
      for (uml.parser.xmi.metamodel.Element xmiElement : predecessors) {
        if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Message))
          continue;
        uml.parser.xmi.metamodel.Message predecessorXMIMessage = (uml.parser.xmi.metamodel.Message)xmiElement;
        xmiMessage.addPredecessor(predecessorXMIMessage);
      }

      DEBUG.info(xmiMessage.toString());
    }

    return xmiMessages;
  }

  private List<uml.parser.xmi.metamodel.Object> addObjects(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Object> xmiObjects = new ArrayList<uml.parser.xmi.metamodel.Object>();
    for (Node objectNode : this.getXMINodes("UML:Object")) {
      // Get classifier
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIRef(objectNode, "UML:Instance.classifier");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Class))
        continue;

      uml.parser.xmi.metamodel.Class xmiClass = (uml.parser.xmi.metamodel.Class)xmiElement;
      uml.parser.xmi.metamodel.Object xmiObject = xmiModel.createObject(this.getName(objectNode), xmiClass);
      xmiObjects.add(xmiObject);
      this.addElementNode(xmiObject, objectNode);

      DEBUG.info(xmiObject.toString());
    }
    return xmiObjects;
  }

  private List<uml.parser.xmi.metamodel.Link> addLinks(uml.parser.xmi.metamodel.Model xmiModel) {
    List<uml.parser.xmi.metamodel.Link> xmiLinks = new ArrayList<uml.parser.xmi.metamodel.Link>();
    for (Node objectNode : this.getXMINodes("UML:Link")) {
      // Get association
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIRef(objectNode, "UML:Link.association");
      if (xmiElement != null && !(xmiElement instanceof uml.parser.xmi.metamodel.Association))
        continue;

      uml.parser.xmi.metamodel.Association xmiAssociation = (uml.parser.xmi.metamodel.Association)xmiElement;
      uml.parser.xmi.metamodel.Link xmiLink = xmiModel.createLink(this.getName(objectNode), xmiAssociation);
      xmiLinks.add(xmiLink);
      this.addElementNode(xmiLink, objectNode);

      DEBUG.info(xmiLink.toString());
    }
    return xmiLinks;
  }

  private List<uml.parser.xmi.metamodel.LinkEnd> addLinkEnds() {
    List<uml.parser.xmi.metamodel.LinkEnd> xmiLinkEnds = new ArrayList<uml.parser.xmi.metamodel.LinkEnd>();
    for (Node linkEndNode : this.getXMINodes("UML:LinkEnd")) {
      // Get owning link
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(linkEndNode, "UML:Link");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Link))
        continue;

      uml.parser.xmi.metamodel.Link xmiLink = (uml.parser.xmi.metamodel.Link)xmiElement;

      // Get instance
      xmiElement = this.getElementXMIRef(linkEndNode, "UML:LinkEnd.instance");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Object))
        continue;
      
      uml.parser.xmi.metamodel.Object xmiObject = (uml.parser.xmi.metamodel.Object)xmiElement;
      uml.parser.xmi.metamodel.LinkEnd xmiLinkEnd = xmiLink.createLinkEnd(this.getName(linkEndNode), xmiObject);
      xmiLinkEnds.add(xmiLinkEnd);
      this.addElementNode(xmiLinkEnd, linkEndNode);

      DEBUG.info(xmiLinkEnd.toString());
    }
    return xmiLinkEnds;    
  }

  private List<uml.parser.xmi.metamodel.Stimulus> addStimuli() {
    List<uml.parser.xmi.metamodel.Stimulus> xmiStimuli = new ArrayList<uml.parser.xmi.metamodel.Stimulus>();
    for (Node stimulusNode : this.getXMINodes("UML:Stimulus")) {
      uml.parser.xmi.metamodel.Element xmiElement = null;

      // Get owning link
      xmiElement = this.getElementXMIRef(stimulusNode, "UML:Stimulus.communicationLink");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Link))
        continue;

      uml.parser.xmi.metamodel.Link xmiLink = (uml.parser.xmi.metamodel.Link)xmiElement;

      // Get sending instance
      xmiElement = this.getElementXMIRef(stimulusNode, "UML:Stimulus.sender");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Object))
        continue;

      uml.parser.xmi.metamodel.Object xmiSender = (uml.parser.xmi.metamodel.Object)xmiElement;

      // Get receiving instance
      xmiElement = this.getElementXMIRef(stimulusNode, "UML:Stimulus.receiver");
      if (xmiElement == null || !(xmiElement instanceof uml.parser.xmi.metamodel.Object))
        continue;

      uml.parser.xmi.metamodel.Object xmiReceiver = (uml.parser.xmi.metamodel.Object)xmiElement;
      
      uml.parser.xmi.metamodel.Stimulus xmiStimulus = xmiLink.createStimulus(this.getName(stimulusNode), xmiSender, xmiReceiver);
      xmiStimuli.add(xmiStimulus);
      this.addElementNode(xmiStimulus, stimulusNode);

      DEBUG.info(xmiStimulus.toString());
    }
    return xmiStimuli;    
  }

  private List<uml.parser.xmi.metamodel.Constraint> addConstraints() {
    List<uml.parser.xmi.metamodel.Constraint> xmiConstraints = new ArrayList<uml.parser.xmi.metamodel.Constraint>();
    for (Node constraintNode : this.getXMINodes("UML:Constraint")) {
      // Get model element
      uml.parser.xmi.metamodel.Element xmiElement = this.getElementXMIOwner(constraintNode, null);
      if (xmiElement == null)
        continue;

      // Get body
      Node constraintBodyNode = this.getChild(constraintNode, "UML:Constraint.body");
      if (constraintBodyNode == null)
        continue;
      Node expressionDataNode = getDescendant(constraintBodyNode, "UML:Expression.body");
      if (expressionDataNode == null)
        continue;
      String constraintBody = expressionDataNode.getTextContent();

      uml.parser.xmi.metamodel.Constraint xmiConstraint = new uml.parser.xmi.metamodel.Constraint(this.getName(constraintNode), constraintBody);
      xmiConstraints.add(xmiConstraint);
      xmiElement.addConstraint(xmiConstraint);
      DEBUG.info(xmiConstraint.toString());
    }
    return xmiConstraints;
  }

  private List<uml.parser.xmi.metamodel.TaggedValue> addTaggedValues() {
    List<uml.parser.xmi.metamodel.TaggedValue> xmiTaggedValues = new ArrayList<uml.parser.xmi.metamodel.TaggedValue>();
    for (Node taggedValueNode : this.getXMINodes("UML:TaggedValue")) {
      // Get model element
      uml.parser.xmi.metamodel.Element xmiElement = this.getXMIOwner(taggedValueNode, "UML:ModelElement.taggedValue", "modelElement");
      if (xmiElement == null)
        continue;

      // Get tag
      String taggedValueTag = this.getTagDefinitionName(taggedValueNode);      

      if (taggedValueTag == "")
        continue;

      // Get value
      Node taggedValueValueNode = this.getChild(taggedValueNode, "UML:TaggedValue.dataValue");
      if (taggedValueValueNode == null)
        continue;
      String taggedValueValue = this.getElementString(taggedValueValueNode);
      if (taggedValueValue == null)
        continue;

      uml.parser.xmi.metamodel.TaggedValue xmiTaggedValue = new uml.parser.xmi.metamodel.TaggedValue(taggedValueTag, taggedValueValue);
      xmiTaggedValues.add(xmiTaggedValue);
      xmiElement.addTaggedValue(xmiTaggedValue);
      DEBUG.info(xmiTaggedValue.toString());
    }
    return xmiTaggedValues;
  }

  private String getTagDefinitionName(Node taggedValueNode) {
    Node tagDefinitionNode = this.getDescendant(taggedValueNode, "UML:TagDefinition");
    String name = this.getAttributeString(tagDefinitionNode, "name");
    if (!name.equals(""))
      return name;

    String idref = this.getAttributeString(tagDefinitionNode, XMIIDREF);
    if (idref.equals(""))
      return "";

    tagDefinitionNode = this.getNodeById(idref);

    if (tagDefinitionNode == null)
      return "";

    return this.getAttributeString(tagDefinitionNode, "name");
  }
}
