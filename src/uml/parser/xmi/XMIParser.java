package uml.parser.xmi;

import java.util.*;
import org.w3c.dom.*;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public abstract class XMIParser {
  private Document document;
  private String xmiIdString = "";
  private String xmiIdRefString = "";
  private HashMap<uml.parser.xmi.metamodel.Element, Node> elementNodes = new HashMap<>();
  private HashMap<Node, uml.parser.xmi.metamodel.Element> nodeElements = new HashMap<>();
  private HashMap<String, Node> xmiIdNodes = new HashMap<>();

  void reset(Document document, String xmiId, String xmiIdRef) {
    this.document = document;
    this.xmiIdString = xmiId;
    this.xmiIdRefString = xmiIdRef;
    this.elementNodes = new HashMap<>();
    this.nodeElements = new HashMap<>();
    this.xmiIdNodes = new HashMap<>();
    if (this.document != null)
      this.initialiseXMIElements(this.document);
  }

  Document getDocument() {
    return this.document;
  }

  void addElementNode(uml.parser.xmi.metamodel.Element xmiElement, Node node) {
    this.elementNodes.put(xmiElement, node);
    this.nodeElements.put(node, xmiElement);
  }

  Node getNode(uml.parser.xmi.metamodel.Element xmiElement) {
    if (xmiElement == null)
      return null;
    return elementNodes.get(xmiElement);
  }

  uml.parser.xmi.metamodel.Element getElement(Node node) {
    if (node == null)
      return null;
    return nodeElements.get(node);
  }

  Node getNodeById(String id) {
    return this.xmiIdNodes.get(id);
  }

  Node getChild(Node node, String name) {
    NodeList childNodes = node.getChildNodes();
    for (int i = 0; i < childNodes.getLength(); i++) {
      Node childNode = childNodes.item(i);
      if (childNode.getNodeName().equals(name))
        return childNode;
    }
    return null;
  }

  List<Node> getChildren(Node node, String name) {
    List<Node> children = new ArrayList<>();
    NodeList childNodes = node.getChildNodes();
    for (int i = 0; i < childNodes.getLength(); i++) {
      Node childNode = childNodes.item(i);
      if (childNode.getNodeName().equals(name))
        children.add(childNode);
    }
    return children;
  }

  Node getDescendant(Node node, String name) {
    if (node.getNodeName().equals(name))
      return node;
    NodeList childNodes = node.getChildNodes();
    for (int i = 0; i < childNodes.getLength(); i++) {
      Node childNode = childNodes.item(i);
      Node descendantNode = getDescendant(childNode, name);
      if (descendantNode != null)
        return descendantNode;
    }
    return null;
  }

  boolean isContained(Node childNode, Node ancestorNode) {
    while (!childNode.isSameNode(ancestorNode) && childNode.getParentNode() != null)
      childNode = childNode.getParentNode();
    return childNode.isSameNode(ancestorNode);
  }

  int getElementInt(Node xmiNode, int def) {
    String content = xmiNode.getTextContent();
    if (content != null) {
      content = content.trim();
      if (!content.equals("")) {
        try {
          return Integer.parseInt(content);
        }
        catch (NumberFormatException nfe) {
        }
      }
    }
    return def;
  }

  int getAttributeInt(Node xmiNode, String attributeName, int def) {
    if (xmiNode == null)
      return def;

    NamedNodeMap attributes = xmiNode.getAttributes();
    if (attributes != null) {
      Node nameNode = attributes.getNamedItem(attributeName);
      if (nameNode != null) {
        String content = nameNode.getNodeValue();
        if (content != null) {
          content = content.trim();
          if (!content.equals("")) {
            try {
              return Integer.parseInt(content);
            }
            catch (NumberFormatException nfe) {
            }
          }
        }
      }
    }

    return def;
  }  

  String getElementString(Node xmiNode) {
    String content = xmiNode.getTextContent();
    if (content == null)
      content = "";
    return content;
  }

  String getAttributeString(Node xmiNode, String attributeName) {
    NamedNodeMap attributes = xmiNode.getAttributes();
    if (attributes != null) {
      Node nameNode = attributes.getNamedItem(attributeName);
      if (nameNode != null)
        return nameNode.getNodeValue();
    }
    return "";
  }

  void initialiseXMIElements(Node node) {
    NamedNodeMap attributes = node.getAttributes();
    if (attributes != null) {
      for (int i = 0; i < attributes.getLength(); i++) {
        Node attribute = attributes.item(i);
        if (attribute.getNodeName().equals(this.xmiIdString))
          this.xmiIdNodes.put(attribute.getNodeValue(), node);
      }
    }

    NodeList childNodes = node.getChildNodes();
    if (childNodes != null) {
      for (int i = 0; i < childNodes.getLength(); i++) {
        Node childNode = childNodes.item(i);
        initialiseXMIElements(childNode);
      }
    }    
  }

  List<Node> getXMINodes(String... elementNames) {
    List<Node> xmiNodes = new ArrayList<>();
    for (String elementName : elementNames) {
      NodeList allNodes = this.document.getElementsByTagName(elementName);
      for (int i = 0; i < allNodes.getLength(); i++) {
        Node node = allNodes.item(i);
        if (this.xmiIdNodes.containsValue(node))
          xmiNodes.add(node);
      }
    }
    return xmiNodes;
  }

  List<uml.parser.xmi.metamodel.Element> getElementXMIRefs(Node xmiNode, String elementName) {
    List<uml.parser.xmi.metamodel.Element> xmiElements = new ArrayList<>();
    if (xmiNode == null)
      return xmiElements;

    List<Node> subXMINodes = new ArrayList<>();
    if (elementName != null && elementName.equals(xmiNode.getNodeName()))
      subXMINodes.add(xmiNode);
    else
      subXMINodes.addAll(this.getChildren(xmiNode, elementName));
    for (Node subXMINode : subXMINodes) {
      Node subSubXMINode = subXMINode.getFirstChild();
      if (subSubXMINode != null) {
        Node refXMINode = subSubXMINode.getNextSibling();
        if (refXMINode != null) {
          NamedNodeMap attributes = refXMINode.getAttributes();
          Node idRefNode = attributes.getNamedItem(this.xmiIdRefString);
          if (idRefNode != null) {
            String id = idRefNode.getNodeValue();
            uml.parser.xmi.metamodel.Element xmiElement = this.getElement(this.getNodeById(id));
            if (xmiElement != null)
              xmiElements.add(xmiElement);
          }
        }
      }
    }
    return xmiElements;
  }

  uml.parser.xmi.metamodel.Element getElementXMIRef(Node xmiNode, String... elementNames) {
    for (String elementName : elementNames) {
      List<uml.parser.xmi.metamodel.Element> xmiElements = this.getElementXMIRefs(xmiNode, elementName);
      if (xmiElements != null && !xmiElements.isEmpty())
        return xmiElements.get(0);
    }
    return null;
  }

  uml.parser.xmi.metamodel.Element getAttributeXMIRef(Node xmiNode, String attributeName) {
    if (xmiNode == null)
      return null;

    NamedNodeMap attributes = xmiNode.getAttributes();
    if (attributes != null) {
      Node nameNode = attributes.getNamedItem(attributeName);
      if (nameNode != null) {
        String nameNodeValue = nameNode.getNodeValue();
        if (nameNodeValue != null)
          return this.getElement(this.getNodeById(nameNodeValue));
      }
    }

    return null;
  }  

  uml.parser.xmi.metamodel.Element getXMIRef(Node xmiNode, String elementName, String attributeName) {
    uml.parser.xmi.metamodel.Element xmiElement = null;
    if (elementName != null)
      xmiElement = this.getElementXMIRef(xmiNode, elementName);
    if (xmiElement == null && attributeName != null)
      xmiElement = this.getAttributeXMIRef(xmiNode, attributeName);
    return xmiElement;
  }

  /**
   * Find all XMI elements of {@code xmiNode} either referenced through a
   * contained element {@code elementName}, or referenced through an
   * attribute {@code attributeName}, or contained in element {@code elementName}
   * of {@code xmiNode}.
   * 
   * @param xmiNode node representing an XMI element
   * @param elementName element name, where reference may be contained
   * @param attributeName attribute name, where reference may be contained
   * @return a list of XMI elements referenced or contained in {@code xmiNode}
   * @post result != null
   */
  List<uml.parser.xmi.metamodel.Element> getXMIElements(Node xmiNode, String elementName, String attributeName) {
    List<uml.parser.xmi.metamodel.Element> xmiElements = new ArrayList<>();
    if (xmiNode == null)
      return xmiElements;

    if (elementName != null) {
      xmiElements.addAll(this.getElementXMIRefs(xmiNode, elementName));
      if (!xmiElements.isEmpty())
        return xmiElements;
    }

    if (attributeName != null) {
      xmiElements.add(this.getAttributeXMIRef(xmiNode, attributeName));
      if (!xmiElements.isEmpty())
        return xmiElements;
    }

    if (elementName != null) {
      List<Node> elementNodes = new ArrayList<>();
      if (elementName.equals(xmiNode.getNodeName()))
        elementNodes.add(xmiNode);
      else
        elementNodes.addAll(this.getChildren(xmiNode, elementName));
      for (Node elementNode : elementNodes) {
        NodeList subElementNodes = elementNode.getChildNodes();
        for (int i = 0; i < subElementNodes.getLength(); i++) {
          Node subElementNode = subElementNodes.item(i);
          uml.parser.xmi.metamodel.Element xmiElement = this.getElement(subElementNode);
          if (xmiElement != null)
            xmiElements.add(xmiElement);
        }
      }
    }
    return xmiElements;
  }

  /**
   * Find an XMI element of {@code xmiNode} either referenced through a
   * contained element {@code elementName}, or referenced through an
   * attribute {@code attributeName}, or contained in
   * {@code xmiNode}.
   * 
   * @param xmiNode node representing an XMI element
   * @param elementName element name, where reference may be contained
   * @param attributeName attribute name, where reference may be contained
   * @return an XMI element referenced or contained in {@code xmiNode}, or
   *   {@code null} if no such XMI element has been found
   */
  uml.parser.xmi.metamodel.Element getXMIElement(Node xmiNode, String elementName, String attributeName) {
    List<uml.parser.xmi.metamodel.Element> xmiElements = this.getXMIElements(xmiNode, elementName, attributeName);
    if (xmiElements != null && !xmiElements.isEmpty())
      return xmiElements.get(0);
    return null;
  }

  /**
   * Find owner of an XMI element, represented by {@code xmiNode}, by
   * looking at the containers of {@code xmiNode}.
   *
   * @param xmiNode node representing an XMI element
   * @return container owner of {@code xmiNode}, or {@code null} if no such element
   *   has been found
   */
  uml.parser.xmi.metamodel.Element getContainerXMIOwner(Node xmiNode) {
    return this.getXMIOwner(xmiNode, null, null);
  }

  /**
   * Find owner of an XMI element, represented by {@code xmiNode}, by
   * first looking at the contained element {@code elementName}, and,
   * if this fails, looking for a container owner.
   *
   * @param xmiNode node representing an XMI element
   * @param elementName element name, where owner reference may be contained
   * @return element owner of {@code xmiNode}, or {@code null} if no such element
   *   has been found
   */
  uml.parser.xmi.metamodel.Element getElementXMIOwner(Node xmiNode, String elementName) {
    return this.getXMIOwner(xmiNode, elementName, null);
  }

  /**
   * Find owner of an XMI element, represented by {@code xmiNode}, by
   * first looking at the attribute {@code attributeName}, and,
   * if this fails looking for a container owner.
   *
   * @param xmiNode node, representing an XMI element
   * @param attributeName attribute name, where owner reference may be contained
   * @return attribute owner of {@code xmiNode}, or {@code null} if no such
   *   element has been found
   */
  uml.parser.xmi.metamodel.Element getAttributeXMIOwner(Node xmiNode, String attributeName) {
    return this.getXMIOwner(xmiNode, null, attributeName);
  }

  /**
   * Find owner of an XMI element, represented by {@code xmiNode}, by
   * first looking at the contained element {@code elementName} (if not {@code null}),
   * second to the attribute {@code attributeName} (if not {@code null}), and,
   * if all this fails looking for a container owner.
   *
   * @param xmiNode node, representing an XMI element
   * @param elementName element name, where owner reference may be contained
   * @param attributeName attribute name, where owner reference may be contained
   * @return owner of {@code xmiNode}, or {@code null} if no such
   *   element has been found
   */
  uml.parser.xmi.metamodel.Element getXMIOwner(Node xmiNode, String elementName, String attributeName) {
    uml.parser.xmi.metamodel.Element xmiElement = null;

    if (elementName != null)
      xmiElement = this.getElementXMIRef(xmiNode, elementName);

    if (xmiElement == null && attributeName != null)
      xmiElement = this.getAttributeXMIRef(xmiNode, attributeName);

    while (xmiElement == null) {
      if (xmiNode.getParentNode() == null)
        break;
      xmiNode = xmiNode.getParentNode();
      xmiElement = this.getElement(xmiNode);
    }

    return xmiElement;
  }
}
