package uml.parser.xmi;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAction;
import uml.UAttribute;
import uml.UClass;
import uml.UClassifier;
import uml.UCollaboration;
import uml.UExpression;
import uml.UModel;
import uml.UModelException;
import uml.UObject;
import uml.UOperation;
import uml.UParameter;
import uml.UReception;
import uml.USignal;
import uml.USlot;
import uml.UType;
import uml.interaction.UInteraction;
import uml.interaction.UMessage;
import uml.parser.ute.UMLParser;
import uml.statemachine.UEvent;
import uml.statemachine.UPseudoState;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import util.Message;
import util.Pair;
import util.Identifier;
import util.properties.PropertyException;
import static util.Formatter.quoted;
import static util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Translates data from an XMI-file into internal representation.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Translator {
  private uml.parser.xmi.metamodel.@NonNull Model xmiModel;
  private @NonNull UModel uModel;
  private Map<uml.parser.xmi.metamodel.Classifier, UClassifier> classifiers = new HashMap<>();
  private Map<uml.parser.xmi.metamodel.Element, UAttribute> attributes = new HashMap<>();
  private Map<uml.parser.xmi.metamodel.Signal, USignal> signals = new HashMap<>();
  private Map<uml.parser.xmi.metamodel.Operation, UOperation> operations = new HashMap<>();
  private Map<uml.parser.xmi.metamodel.Element, UObject> objects = new HashMap<>();

  private Translator(uml.parser.xmi.metamodel.@NonNull Model xmiModel) {
    this.xmiModel = xmiModel;
    this.uModel = new UModel(canonise(xmiModel.getName()));
  }

  public static @NonNull UModel translate(uml.parser.xmi.metamodel.@NonNull Model xmiModel) throws UModelException {
    Translator translator = new Translator(xmiModel);
    translator.initialiseModel();
    return translator.uModel;
  }

  private List<@NonNull String> getProperties(Collection<? extends uml.parser.xmi.metamodel.@NonNull Element> propertySources) {
    List<@NonNull String> properties = new ArrayList<>();
    for (uml.parser.xmi.metamodel.Element propertySource : propertySources) {
      for (uml.parser.xmi.metamodel.TaggedValue taggedValue : propertySource.getTaggedValues()) {
        if (taggedValue.getTag() != null && taggedValue.getTag().trim().toLowerCase().endsWith("property")) {
          String propertyValue = taggedValue.getValue();
          if (propertyValue != null)
            properties.add(propertyValue);
        }
      }
    }
    return properties;
  }

  private void initialiseModel() throws UModelException {
    // Fill-in classes ...
    for (uml.parser.xmi.metamodel.Class xmiClass : this.xmiModel.getClasses()) {
      UClass hclass = new UClass(canonise(xmiClass.getName()), this.uModel);
      String xmiClassName = xmiClass.getName().trim().toLowerCase();

      if (xmiClassName.equals("int") || xmiClassName.equals("integer")) {
        this.classifiers.put(xmiClass, this.uModel.getInteger());
        util.Message.warning("Treating class " + quoted(xmiClass.getName()) + " as data type `int'");
        continue;
      }
      else
        if (xmiClassName.equals("bool") || xmiClassName.equals("boolean")) {
          this.classifiers.put(xmiClass, this.uModel.getBoolean());
          util.Message.warning("Treating class " + quoted(xmiClass.getName()) + " as data type `boolean'");
          continue;
        }
        else
          if (xmiClassName.equals("string")) {
            this.classifiers.put(xmiClass, this.uModel.getString());
            util.Message.warning("Treating class " + quoted(xmiClass.getName()) + " as data type `string'");
            continue;
          }
          else
            if (xmiClassName.equals("void")) {
              this.classifiers.put(xmiClass, this.uModel.getVoid());
              util.Message.warning("Treating class " + quoted(xmiClass.getName()) + " as data type `void'");
              continue;
            }
            else
              if (xmiClassName.equals("clock")) {
                this.classifiers.put(xmiClass, this.uModel.getClock());
                util.Message.warning("Treating class " + quoted(xmiClass.getName()) + " as data type `clock'");
                continue;
              }

      if (xmiClass.getBehaviours().isEmpty())
        util.Message.warning("Class " + quoted(xmiClass.getName()) + " shows no behaviour");
      if (xmiClass.getBehaviours().size() > 1)
        util.Message.warning("Class " + quoted(xmiClass.getName()) + " shows more than one behaviour");

      this.classifiers.put(xmiClass, hclass);
      this.uModel.addClass(hclass);
    }

    // ... data types ...
    for (uml.parser.xmi.metamodel.DataType xmiDataType : this.xmiModel.getDataTypes()) {
      String mDataTypeName = xmiDataType.getName().trim().toLowerCase();
      if (mDataTypeName.equals("int") || mDataTypeName.equals("integer"))
        this.classifiers.put(xmiDataType, this.uModel.getInteger());
      else
        if (mDataTypeName.equals("bool") || mDataTypeName.equals("boolean"))
          this.classifiers.put(xmiDataType, this.uModel.getBoolean());
        else
          if (mDataTypeName.equals("string"))
            this.classifiers.put(xmiDataType, this.uModel.getString());
          else
            if (mDataTypeName.equals("void"))
              this.classifiers.put(xmiDataType, this.uModel.getVoid());
            else
              if (mDataTypeName.equals("clock"))
                this.classifiers.put(xmiDataType, this.uModel.getClock());
              else {
                util.Message.warning("Ignoring data type " + quoted(xmiDataType.getName()));
              }
    }

    // ... signals ...
    for (uml.parser.xmi.metamodel.Signal xmiSignal : this.xmiModel.getSignals()) {
      USignal signal = new USignal(canonise(xmiSignal.getName()), this.uModel);
      this.signals.put(xmiSignal, signal);
      this.uModel.addSignal(signal);
    }

    // Initialise classes ...
    var classEntries = new HashSet<@NonNull Pair<uml.parser.xmi.metamodel.@NonNull Class, @NonNull UClass>>();
    for (uml.parser.xmi.metamodel.Class xmiClass : this.xmiModel.getClasses()) {
      UClassifier classifier = this.getClassifier(xmiClass);

      // If classifier does not denote a datatype (int, boolean, &c.)
      if (classifier instanceof UClass) {
        UClass uClass = (UClass)classifier;
        setAttributes(xmiClass, uClass);
        setOperations(xmiClass, uClass);
        setReceptions(xmiClass, uClass);
        setStateMachine(xmiClass, uClass);
      }
    }
    for (var classEntry : classEntries)
      setStateMachine(classEntry.getFirst(), classEntry.getSecond());

    // ... and collaborations
    for (uml.parser.xmi.metamodel.Collaboration xmiCollaboration : this.xmiModel.getCollaborations()) {
      UCollaboration uCollaboration = new UCollaboration(canonise(xmiCollaboration.getName()), this.uModel);
      this.uModel.addCollaboration(uCollaboration);

      // Set up collaboration objects
      for (uml.parser.xmi.metamodel.ClassifierRole xmiClassifierRole : xmiCollaboration.getClassifierRoles()) {
        UClass uClass = (UClass)getClassifier(xmiClassifierRole.getBase());
        if (uClass == null) {
          util.Message.warning("No UML base class for XMI classifier role " + quoted(xmiClassifierRole.getName()) + " found; ignoring this classifier role");
          continue;
        }
        UObject uObject = new UObject(canonise(xmiClassifierRole.getName()), uClass);
        this.objects.put(xmiClassifierRole, uObject);
        uCollaboration.addObject(uObject);
      }
      for (uml.parser.xmi.metamodel.Object xmiObject : xmiCollaboration.getObjects()) {
        UClass uClass = (UClass)getClassifier(xmiObject.getClassifier());
        if (uClass == null) {
          util.Message.warning("No UML object for XMI object " + quoted(xmiObject.getName()) + " found; ignoring this object");
          continue;
        }
        UObject uObject = new UObject(canonise(xmiObject.getName()), uClass);
        this.objects.put(xmiObject, uObject);
        uCollaboration.addObject(uObject);
      }

      // Initialise objects
      for (uml.parser.xmi.metamodel.ClassifierRole xmiClassifierRole : xmiCollaboration.getClassifierRoles()) {
        UObject uObject = getObject(xmiClassifierRole);
        if (uObject == null)
          continue;
        setSlots(xmiClassifierRole, uObject);
      }
      for (uml.parser.xmi.metamodel.Object xmiObject : xmiCollaboration.getObjects()) {
        UObject uObject = getObject(xmiObject);
        if (uObject == null)
          continue;
        setSlots(xmiObject, uObject);
      }

      // Add interactions
      for (uml.parser.xmi.metamodel.Interaction xmiInteraction : xmiCollaboration.getInteractions()) {
        UInteraction uInteraction = uCollaboration.addInteraction(canonise(xmiInteraction.getName()));
        uInteraction.addMessages(this.getMessages(xmiInteraction, uInteraction));
        uInteraction.makeBasicFragment();
      }

      // Add constraints
      for (uml.parser.xmi.metamodel.Constraint xmiConstraint : xmiCollaboration.getConstraints()) {
        uCollaboration.addConstraint(requireNonNull(UMLParser.constraint(xmiConstraint.getName(), xmiConstraint.getBody())));
      }

      // Add properties
      for (String property : this.getProperties(xmiCollaboration.getClassifierRoles())) {
        try {
          if (!uCollaboration.getProperties().setPropertyByName(property))
            Message.warning("Unknown property " + quoted(property) + " in collaboration " + quoted(uCollaboration.getName()));
        }
        catch (PropertyException pe) {
          Message.warning("Cannot set property " + quoted(property) + " in collaboration " + quoted(uCollaboration.getName()));
        }
      }
      try {
        uCollaboration.getProperties().commit();
      }
      catch (PropertyException pe) {
        uCollaboration.getProperties().abort();
        Message.warning("Properties of collaboration " + quoted(uCollaboration.getName()) + " have not been changed. Reason: " + pe.getMessage());
      }
    }

    // Add an anonymous collaboration to the model that contains all
    // freewheeling objects and constraints
    UCollaboration anonymousCollaboration = null;
    if (!this.xmiModel.getObjects().isEmpty() || !this.xmiModel.getConstraints().isEmpty()) {
      anonymousCollaboration = new UCollaboration(canonise("unnamed"), uModel);
      util.Message.info("Creating collaboration " + quoted(anonymousCollaboration.getName()));
      uModel.addCollaboration(anonymousCollaboration);

      // Set up collaboration objects
      for (uml.parser.xmi.metamodel.Object xmiObject : this.xmiModel.getObjects()) {
        UClass uClass = (UClass)getClassifier(xmiObject.getClassifier());
        if (uClass == null) {
          util.Message.warning("No UML class for XMI object " + quoted(xmiObject.getName()) + "; ignoring this object");
          continue;
        }
        UObject object = new UObject(canonise(xmiObject.getName()), uClass);
        objects.put(xmiObject, object);
        util.Message.info("Adding object " + quoted(object.getName()) + " to collaboration " + quoted(anonymousCollaboration.getName()));
        anonymousCollaboration.addObject(object);
      }

      // Initialise objects
      for (uml.parser.xmi.metamodel.Object xmiObject : this.xmiModel.getObjects()) {
        UObject uObject = getObject(xmiObject);
        if (uObject == null)
          continue;
        setSlots(xmiObject, uObject);
      }

      // Add constraints
      for (uml.parser.xmi.metamodel.Constraint xmiConstraint : this.xmiModel.getConstraints()) {
        util.Message.info("Adding constraint " + quoted(xmiConstraint.getName()) + " to collaboration " + quoted(anonymousCollaboration.getName()));
        anonymousCollaboration.addConstraint(requireNonNull(UMLParser.constraint(xmiConstraint.getName(), xmiConstraint.getBody())));
      }

      // Add properties
      for (String property : this.getProperties(this.xmiModel.getObjects())) {
        try {
          if (!anonymousCollaboration.getProperties().setPropertyByName(property))
            Message.warning("Unknown property " + quoted(property) + " in collaboration " + quoted(anonymousCollaboration.getName()));
        }
        catch (PropertyException pe) {
          Message.warning("Cannot set property " + quoted(property) + " in collaboration " + quoted(anonymousCollaboration.getName()) + ". Reason: " + pe.getMessage());
        }
      }
      try {
        anonymousCollaboration.getProperties().commit();
      }
      catch (PropertyException pe) {
        anonymousCollaboration.getProperties().abort();
        Message.warning("Properties of collaboration " + quoted(anonymousCollaboration.getName()) + " have not been changed. Reason: " + pe.getMessage());
      }
    }

    for (String property : this.getProperties(this.xmiModel.getClasses())) {
      try {
        if (!uModel.getProperties().setPropertyByName(property))
          Message.warning("Unknown property " + quoted(property) + " in model " + quoted(uModel.getName()));
      }
      catch (PropertyException pe) {
        uModel.getProperties().abort();
        Message.warning("Cannot set property " + quoted(property) + " in model " + quoted(uModel.getName()) + ". Reason: " + pe.getMessage());
      }
    }
    try {
      uModel.getProperties().commit();
    }
    catch (PropertyException pe) {
      uModel.getProperties().abort();
      Message.warning("Properties of model " + quoted(uModel.getName()) + " have not been changed. Reason: " + pe.getMessage());
    }
  }

  private @Nullable UClassifier getClassifier(uml.parser.xmi.metamodel.Classifier xmiClassifier) {
    UClassifier uClassifier = this.classifiers.get(xmiClassifier);
    return uClassifier;
  }

  private @Nullable UAttribute getAttribute(uml.parser.xmi.metamodel.Element xmiElement) {
    UAttribute uAttribute = this.attributes.get(xmiElement);
    return uAttribute;
  }

  private @Nullable USignal getSignal(uml.parser.xmi.metamodel.Signal xmiSignal) {
    USignal uSignal = this.signals.get(xmiSignal);
    return uSignal;
  }

  private @Nullable UOperation getOperation(uml.parser.xmi.metamodel.Operation xmiOperation) {
    UOperation uOperation = this.operations.get(xmiOperation);
    return uOperation;
  }

  private UObject getObject(uml.parser.xmi.metamodel.Element xmiElement) {
    UObject uObject = this.objects.get(xmiElement);
    return uObject;
  }

  private void setAttributes(uml.parser.xmi.metamodel.@NonNull Class xmiClass, @NonNull UClass uClass) throws UModelException {
    Set<String> knownNames = new HashSet<String>();

    for (uml.parser.xmi.metamodel.AssociationEnd oppositeXMIAssociationEnd : xmiClass.getOppositeAssociationEnds()) {
      if (!oppositeXMIAssociationEnd.isNavigable())
        continue;

      String oppositeXMIAssociationEndName = canonise(oppositeXMIAssociationEnd.getName());
      if (knownNames.contains(oppositeXMIAssociationEndName))
        throw new UModelException("Double declaration of opposite association end name " + quoted(oppositeXMIAssociationEndName) + " in class " + quoted(xmiClass.getName()));

      UClassifier uTypeClassifier = getClassifier(oppositeXMIAssociationEnd.getType());
      if (uTypeClassifier == null) {
        util.Message.warning("Ignoring opposite association end " + quoted(oppositeXMIAssociationEnd.getName()) + " in class " + quoted(uClass.getName()) + " of unknown type " + quoted(oppositeXMIAssociationEnd.getType().getName()));
        continue;
      }
      UAttribute uAttribute = new UAttribute(oppositeXMIAssociationEndName, uTypeClassifier, uClass);
      // TODO (AK060507) a non-navigable attribute is NOT added here.
      this.attributes.put(oppositeXMIAssociationEnd, uAttribute);
      uClass.addAttribute(uAttribute);
      knownNames.add(uAttribute.getName());

      if (oppositeXMIAssociationEnd.getUpperMultiplicity() > 1)
        uAttribute.setUpper(UExpression.intConst(oppositeXMIAssociationEnd.getUpperMultiplicity()));
      if (oppositeXMIAssociationEnd.getUpperMultiplicity() < 0)
        util.Message.warning("Ignoring multiplicity of opposite association end " + quoted(uAttribute.getName()) + " in class " + quoted(uClass.getName()));
    }

    for (uml.parser.xmi.metamodel.Attribute xmiAttribute : xmiClass.getAttributes()) {
      String xmiAttributeName = canonise(xmiAttribute.getName());
      if (knownNames.contains(xmiAttributeName))
        throw new UModelException("Double declaration of attribute name " + quoted(xmiAttributeName) + " in class " + quoted(xmiClass.getName()));

      UClassifier uTypeClassifier = getClassifier(xmiAttribute.getType());
      if (uTypeClassifier == null) {
        util.Message.warning("Ignoring attribute " + quoted(xmiAttribute.getName()) + " in class " + quoted(uClass.getName()) + " of unknown type " + quoted(xmiAttribute.getType().getName()));
        continue;
      }
      UAttribute uAttribute = new UAttribute(xmiAttributeName, uTypeClassifier, uClass);
      this.attributes.put(xmiAttribute, uAttribute);
      uClass.addAttribute(uAttribute);
      knownNames.add(uAttribute.getName());

      if (xmiAttribute.getUpperMultiplicity() > 1)
        uAttribute.setUpper(UExpression.intConst(xmiAttribute.getUpperMultiplicity()));
      if (xmiAttribute.getUpperMultiplicity() < 0)
        util.Message.warning("Ignoring multiplicity of attribute " + quoted(uAttribute.getName()) + " in class " + quoted(uClass.getName()));

      if (!xmiAttribute.getInitialValues().isEmpty()) {
        if (UType.simple(this.uModel.getClock()).equals(uAttribute.getType()))
          util.Message.warning("Ignoring clock initialisation of attribute " + quoted(uAttribute.getName()) + " in class " + quoted(uClass.getName()));
        for (String initialValue : xmiAttribute.getInitialValues()) {
          if (initialValue != null && !initialValue.trim().equals("")) {
            UExpression uExpression = requireNonNull(UMLParser.expression(initialValue));
            uAttribute.addInitialValue(uExpression);
          }
        }
      }

      if (xmiAttribute.isFinal()) {
        uAttribute.setFinal(true);
        if (uAttribute.getInitialValues().isEmpty()) {
          util.Message.warning("Final attribute " + quoted(uAttribute.getName()) + " in class " + quoted(uClass.getName()) + " shows no initial value");
        }
      }

      if (xmiAttribute.isStatic()) {
        uAttribute.setStatic(true);
      }
    }
  }

  private void setOperations(uml.parser.xmi.metamodel.@NonNull Class xmiClass, @NonNull UClass uClass) {
    for (uml.parser.xmi.metamodel.Operation xmiOperation : xmiClass.getOperations()) {
      UOperation uOperation = new UOperation(canonise(xmiOperation.getName().substring(0, 1).toLowerCase() + xmiOperation.getName().substring(1)), uClass, false);
      for (uml.parser.xmi.metamodel.Parameter xmiParameter : xmiOperation.getParameters()) {
        if (xmiParameter.getKind() == uml.parser.xmi.metamodel.ParameterDirectionKind.RETURN) {
          util.Message.warning("Ignoring return parameter " + quoted(xmiParameter.getName()) + " of operation " + quoted(xmiOperation.getName()) + " in class " + quoted(xmiClass.getName()));
          continue;
        }
        if (xmiParameter.getKind() == uml.parser.xmi.metamodel.ParameterDirectionKind.OUT) {
          util.Message.warning("Ignoring out parameter " + quoted(xmiParameter.getName()) + " of operation " + quoted(xmiOperation.getName()) + " in class " + quoted(xmiClass.getName()));
          continue;
        }
        UClassifier uTypeClassifier = getClassifier(xmiParameter.getType());
        if (uTypeClassifier == null) {
          util.Message.warning("Ignoring parameter " + quoted(xmiParameter.getName()) + " of operation " + quoted(xmiOperation.getName()) + " in class " + quoted(xmiClass.getName()) + " with unknown type " + quoted(xmiParameter.getType().getName()));
          continue;          
        }
        uOperation.addParameter(new UParameter(canonise(xmiParameter.getName()), uTypeClassifier, uOperation));
      }
      operations.put(xmiOperation, uOperation);
      uClass.addOperation(uOperation);
    }
  }

  private void setReceptions(uml.parser.xmi.metamodel.@NonNull Class xmiClass, @NonNull UClass uClass) throws UModelException {
    Set<USignal> knownUSignals = new HashSet<>();

    for (uml.parser.xmi.metamodel.Reception xmiReception : xmiClass.getReceptions()) {
      USignal uSignal = getSignal(xmiReception.getSignal());
      if (uSignal == null) {
        util.Message.warning("No signal for reception " + quoted(xmiReception.getName()) + " in class " + quoted(xmiClass.getName()) + "; ignoring this reception");
        continue;
      }

      if (knownUSignals.contains(uSignal))
        throw new UModelException("Double declaration of reception for signal " + quoted(uSignal.getName()) + " in class " + quoted(xmiClass.getName()));

      UReception uReception = new UReception(canonise(xmiReception.getName()), uSignal, uClass);
      for (uml.parser.xmi.metamodel.Parameter xmiParameter : xmiReception.getParameters()) {
        if (xmiParameter.getKind() == uml.parser.xmi.metamodel.ParameterDirectionKind.RETURN) {
          util.Message.warning("Ignoring return parameter " + quoted(xmiParameter.getName()) + " of reception " + quoted(xmiReception.getName()) + " in class " + quoted(xmiClass.getName()));
          continue;
        }
        if (xmiParameter.getKind() == uml.parser.xmi.metamodel.ParameterDirectionKind.OUT) {
          util.Message.warning("Ignoring out parameter " + quoted(xmiParameter.getName()) + " of reception " + quoted(xmiReception.getName()) + " in class " + quoted(xmiClass.getName()));
          continue;
        }
        UClassifier uTypeClassifier = getClassifier(xmiParameter.getType());
        if (uTypeClassifier == null) {
          util.Message.warning("Ignoring parameter " + quoted(xmiParameter.getName()) + " of reception " + quoted(xmiReception.getName()) + " in class " + quoted(xmiClass.getName()) + " with unknown type " + quoted(xmiParameter.getType().getName()));
          continue;          
        }
        uReception.addParameter(new UParameter(canonise(xmiParameter.getName()), uTypeClassifier, uReception));
      }
      uClass.addReception(uReception);
    }
  }

  class StateMachineInfo {
    private UStateMachine uStateMachine;
    private Map<uml.parser.xmi.metamodel.State, UVertex> verticesTable;
    private Map<uml.parser.xmi.metamodel.State, URegion> regionsTable;

    StateMachineInfo(UStateMachine uStateMachine) {
      this.uStateMachine = uStateMachine;
      this.verticesTable = new HashMap<>();
      this.regionsTable = new HashMap<>();
    }

    UStateMachine getUStateMachine() {
      return this.uStateMachine;
    }

    @Nullable UVertex getState(uml.parser.xmi.metamodel.State mState) {
      return this.verticesTable.get(mState);
    }

    @Nullable URegion getRegion(uml.parser.xmi.metamodel.State mState) {
      return this.regionsTable.get(mState);
    }

    void putState(uml.parser.xmi.metamodel.State mState, UVertex uVertex) {
      this.verticesTable.put(mState, uVertex);
    }

    void putRegion(uml.parser.xmi.metamodel.State mState, URegion uRegion) {
      this.regionsTable.put(mState, uRegion);
    }
  }

  private void setStateMachine(uml.parser.xmi.metamodel.@NonNull Class xmiClass, @NonNull UClass uClass) throws UModelException {
    if (xmiClass.getBehaviours() == null || xmiClass.getBehaviours().isEmpty())
      return;

    uml.parser.xmi.metamodel.StateMachine xmiStateMachine = xmiClass.getBehaviours().iterator().next();
    uml.parser.xmi.metamodel.State xmiTop = xmiStateMachine.getTop();
    if (xmiTop == null)
      return;

    UStateMachine uStateMachine = uClass.setStateMachine();
    StateMachineInfo info = new StateMachineInfo(uStateMachine);
    for (uml.parser.xmi.metamodel.State subXMIState : xmiTop.getSubStates())
      getState(subXMIState, info);
    for (uml.parser.xmi.metamodel.Transition xmiTransition : xmiStateMachine.getTransitions())
      addTransition(xmiTransition, info);
  }

  private @NonNull URegion getRegion(uml.parser.xmi.metamodel.State xmiState, StateMachineInfo info) throws UModelException {
    URegion uRegion = info.getRegion(xmiState);
    if (uRegion != null)
      return uRegion;

    if (xmiState.isTop()) {
      UStateMachine uStateMachine = info.getUStateMachine();
      uRegion = uStateMachine.addRegion(canonise("top"));
      info.putRegion(xmiState, uRegion);
    }
    else {
      uml.parser.xmi.metamodel.State xmiContainer = xmiState.getContainer();
      if (xmiContainer.isConcurrent()) {
        UState uContainer = (UState)getState(xmiContainer, info);
        uRegion = uContainer.addRegion(canonise(xmiState.getName()));
        info.putRegion(xmiState, uRegion);
        for (uml.parser.xmi.metamodel.State subXMIState : xmiState.getSubStates())
          getState(subXMIState, info);
      }
      else {
        UState uState = (UState)getState(xmiState, info);
        uRegion = uState.addRegion(canonise(uState.getName()));
        info.putRegion(xmiState, uRegion);
      }
    }

    return uRegion;
  }

  private @NonNull UVertex getState(uml.parser.xmi.metamodel.State xmiState, StateMachineInfo info) throws UModelException {
    UVertex uVertex = info.getState(xmiState);
    if (uVertex != null)
      return uVertex;

    uml.parser.xmi.metamodel.State xmiContainer = xmiState.getContainer();
    URegion uContainer = getRegion(xmiContainer, info);
    String name = canonise(xmiState.getName());
    switch (xmiState.getKind()) {
      case INITIAL:
        uVertex = uContainer.addPseudoState(name, UPseudoState.Kind.INITIAL);
        break;
      case SHALLOWHISTORY:
        uVertex = uContainer.addPseudoState(name, UPseudoState.Kind.SHALLOWHISTORY);
        break;
      case DEEPHISTORY:
        uVertex = uContainer.addPseudoState(name, UPseudoState.Kind.DEEPHISTORY);
        break;
      case JOIN:
        if (xmiState.getOutgoings().size() > 1) {
          uVertex = uContainer.addPseudoState(name, UPseudoState.Kind.FORK);
          Message.warning("Treating join pseudo-state " + quoted(xmiState.getName()) + " in state machine for class " + quoted(info.getUStateMachine().getC1ass().getName()) + " as fork pseudo-state, since it has more than one outgoing transitions");
        }
        else
          uVertex = uContainer.addPseudoState(name, UPseudoState.Kind.JOIN);
        break;
      case FORK:
        if (xmiState.getIncomings().size() > 1) {
          uVertex = uContainer.addPseudoState(name, UPseudoState.Kind.JOIN);
          Message.warning("Treating fork pseudo-state " + quoted(xmiState.getName()) + " in state machine for class " + quoted(info.getUStateMachine().getC1ass().getName()) + " as join pseudo-state, since it has more than one incoming transitions");
        }
        else
          uVertex = uContainer.addPseudoState(name, UPseudoState.Kind.FORK);
        break;
      case JUNCTION:
        uVertex = uContainer.addPseudoState(name, UPseudoState.Kind.JUNCTION);
        break;
      case CHOICE:
        uVertex = uContainer.addPseudoState(name, UPseudoState.Kind.CHOICE);
        break;
      case SYNCH:
        uVertex = uContainer.addPseudoState(name, UPseudoState.Kind.SYNCH);
        break;
      case FINAL:
        uVertex = uContainer.addFinalState(name);
        break;
      default:
        uVertex = uContainer.addState(name);
    }
    info.putState(xmiState, uVertex);

    if (!(uVertex instanceof UState))
      return uVertex;
    UState uState = (UState)uVertex;

    // Add entry and exit actions
    if (xmiState.getEntry() != null) {
      uState.setEntryAction(parseAction(xmiState.getEntry().getScript()));
      // util.Message.debug("" + state.getEntryAction());
    }
    if (xmiState.getExit() != null) {
      uState.setExitAction(parseAction(xmiState.getExit().getScript()));
      // util.Message.debug("" + state.getExitAction());
    }

    // Add deferrable events
    for (uml.parser.xmi.metamodel.Event deferrableXMIEvent : xmiState.getDeferrableEvents()) {
      UEvent deferrableUEvent = null;
      if (deferrableXMIEvent.getKind() == uml.parser.xmi.metamodel.EventKind.SIGNAL)
        deferrableUEvent = UEvent.signal(requireNonNull(getSignal(deferrableXMIEvent.getSignal())));
      if (deferrableXMIEvent.getKind() == uml.parser.xmi.metamodel.EventKind.CALL)
        deferrableUEvent = UEvent.call(requireNonNull(getOperation(deferrableXMIEvent.getOperation())));
      if (deferrableUEvent != null)
        uState.addDeferrableEvent(deferrableUEvent);
    }

    if (xmiState.isComposite()) {
      if (xmiState.isConcurrent()) {
        // Initialize regions
        for (uml.parser.xmi.metamodel.State subXMIState : xmiState.getSubStates())
          getRegion(subXMIState, info);
      }
      else {
        // Initialize subvertices
        for (uml.parser.xmi.metamodel.State subXMIState : xmiState.getSubStates())
          getState(subXMIState, info);
      }
    }

    return uVertex;
  }

  private void addTransition(uml.parser.xmi.metamodel.Transition xmiTransition, StateMachineInfo info) throws UModelException {
    UClass uClass = info.getUStateMachine().getC1ass();

    uml.parser.xmi.metamodel.State sourceXMIState = xmiTransition.getSource();
    uml.parser.xmi.metamodel.State targetXMIState = xmiTransition.getTarget();
    if (sourceXMIState == null || targetXMIState == null) {
      util.Message.warning("Ignoring transition " + quoted(xmiTransition.getName()) + " in state machine for class " + quoted(uClass.getName()) + " because it does not have a source or target state.");
      return;
    }
    UVertex sourceUVertex = getState(sourceXMIState, info);
    UVertex targetUVertex = getState(targetXMIState, info);

    UEvent uTrigger = null;
    uml.parser.xmi.metamodel.Event xmiEvent = xmiTransition.getTrigger();
    if (xmiEvent != null) {
      if (xmiEvent.getKind() == uml.parser.xmi.metamodel.EventKind.SIGNAL)
        uTrigger = UEvent.signal(requireNonNull(getSignal(xmiEvent.getSignal())));
      if (xmiEvent.getKind() == uml.parser.xmi.metamodel.EventKind.CALL)
        uTrigger = UEvent.call(requireNonNull(getOperation(xmiEvent.getOperation())));
      if (xmiEvent.getKind() == uml.parser.xmi.metamodel.EventKind.TIME && sourceUVertex instanceof UState) {
        String timeEventWhenBody = xmiEvent.getWhen().trim();
        if (timeEventWhenBody.startsWith("[")) {
          String timeEventWhenBody1 = timeEventWhenBody.substring(1, timeEventWhenBody.indexOf(","));
          String timeEventWhenBody2 = timeEventWhenBody.substring(timeEventWhenBody.indexOf(",") + 1, timeEventWhenBody.length() - 1);
          uTrigger = UEvent.time((UState)sourceUVertex, requireNonNull(UMLParser.expression(timeEventWhenBody1)), requireNonNull(UMLParser.expression(timeEventWhenBody2)));
        }
        else
          uTrigger = UEvent.time((UState)sourceUVertex, requireNonNull(UMLParser.expression(timeEventWhenBody)));
      }
      if (uTrigger == null) {
        if (sourceUVertex instanceof UState) {
          util.Message.warning("Treating event " + quoted(xmiEvent.getName()) + " as completion event");
          uTrigger = UEvent.completion((UState)sourceUVertex);
        }
        else {
          util.Message.warning("Ignoring event " + quoted(xmiEvent.getName()));
          uTrigger = UEvent.pseudo();
        }
      }
    }
    else {
      // Completion event
      if (sourceUVertex instanceof UState)
        uTrigger = UEvent.completion((UState)sourceUVertex);
      else
        uTrigger = UEvent.pseudo();
    }

    @NonNull UExpression uGuard = UExpression.trueConst();
    String xmiGuard = xmiTransition.getGuard();
    if (xmiGuard != null) {
      if (xmiGuard.trim().equals("else")) {
        uml.parser.xmi.metamodel.State mState = xmiTransition.getSource();
        for (uml.parser.xmi.metamodel.Transition outgoingMTransition : mState.getOutgoings()) {
          if (outgoingMTransition.equals(xmiTransition))
            continue;

          String otherMGuard = outgoingMTransition.getGuard();
          if (otherMGuard != null) {
            UExpression otherUGuard = requireNonNull(UMLParser.expression(otherMGuard));
            uGuard = UExpression.and(uGuard, UExpression.neg(otherUGuard));
          }
          else {
            uGuard = UExpression.falseConst();
            break;
          }
        }
        util.Message.info("Replacing guard `else' on state " + quoted(xmiTransition.getSource().getName()) + " in state machine for class " + quoted(uClass.getName()) + " by " + quoted(uGuard.toString()));
      }
      else
        uGuard = requireNonNull(UMLParser.expression(xmiGuard));
    }

    @NonNull UAction uEffect;
    uml.parser.xmi.metamodel.Action xmiAction = xmiTransition.getEffect();
    if (xmiAction != null)
      uEffect = parseAction(xmiAction.getScript());
    else
      uEffect = UAction.skip();

    sourceUVertex.getContainer().addTransition(sourceUVertex, targetUVertex, uTrigger, uGuard, uEffect);
  }

  private @NonNull UAction parseAction(String xmiAction) throws UModelException {
    if (xmiAction == null)
      return UAction.skip();

    UAction uAction = UMLParser.action(xmiAction);
    assert (uAction != null);
    return uAction;
  }

  private void setSlots(uml.parser.xmi.metamodel.@NonNull Object xmiObject, @NonNull UObject uObject) {
    uml.parser.xmi.metamodel.Class mClass = xmiObject.getClassifier();
    for (uml.parser.xmi.metamodel.AssociationEnd oppositeMAssociationEnd : mClass.getOppositeAssociationEnds()) {
      UAttribute endAttribute = getAttribute(oppositeMAssociationEnd);
      if (endAttribute == null)
        continue;
      for (uml.parser.xmi.metamodel.LinkEnd oppositeMLinkEnd : oppositeMAssociationEnd.getLinkEnds()) {
        for (uml.parser.xmi.metamodel.LinkEnd mLinkEnd : oppositeMLinkEnd.getLink().getLinkEnds()) {
          if (!mLinkEnd.getInstance().equals(xmiObject)) {
            USlot endSlot = new USlot(endAttribute, UExpression.id(Identifier.id(getObject(mLinkEnd.getInstance()).getName())));
            uObject.addSlot(endSlot);
          }
        }
      }
    }
  }

  private void setSlots(uml.parser.xmi.metamodel.@NonNull ClassifierRole xmiClassifierRole, @NonNull UObject uObject) {	
    for (uml.parser.xmi.metamodel.AssociationEndRole xmiAssociationEndRole : xmiClassifierRole.getAssociationEndRoles()) {    	
      uml.parser.xmi.metamodel.AssociationEndRole oppositeMAssociationEndRole = xmiAssociationEndRole.getOppositeAssociationEndRole();      
      UAttribute endAttribute = getAttribute(oppositeMAssociationEndRole.getBase());
      // TODO (AK060507) endAttribute is null, if the association end role is not navigable
      if (endAttribute == null)
        continue;
      USlot endSlot = new USlot(endAttribute, UExpression.id(Identifier.id(getObject(oppositeMAssociationEndRole.getType()).getName())));
      if (!containsSlot(uObject, endSlot))
        uObject.addSlot(endSlot);      
    }
  }

  private boolean containsSlot(@NonNull UObject object, @NonNull USlot newSlot) {	
    for (USlot slot : object.getSlots()){
      if (slot.getAttribute().equals(newSlot.getAttribute()) && slot.getValues().equals(newSlot.getValues())){
        return true; 
      }
    }
    return false;
  }

  private @NonNull Set<@NonNull UMessage> getMessages(uml.parser.xmi.metamodel.@NonNull Interaction xmiInteraction, @NonNull UInteraction uInteraction) throws UModelException {
    Map<uml.parser.xmi.metamodel.Message, @NonNull UMessage> messages = new HashMap<>();

    for (uml.parser.xmi.metamodel.Message xmiMessage : xmiInteraction.getMessages()) {
      UObject uSender = getObject(xmiMessage.getSender());
      UObject uReceiver = getObject(xmiMessage.getReceiver());
      UMessage message = UMLParser.message(xmiMessage.getName(), uSender, uReceiver, xmiMessage.getAction().getScript(), uInteraction);
      if (message != null)
        messages.put(xmiMessage, message);
    }

    for (uml.parser.xmi.metamodel.Message xmiMessage : xmiInteraction.getMessages()) {
      UMessage uMessage = messages.get(xmiMessage);
      if (uMessage == null)
        continue;

      for (uml.parser.xmi.metamodel.Message xmiPredecessor : xmiMessage.getPredecessors()) {
        UMessage predecessor = messages.get(xmiPredecessor);
        if (predecessor != null)
          uMessage.addPredecessor(predecessor);
      }
    }

    return new HashSet<>(messages.values());
  }

  private @NonNull String canonise(String string) {
    if (string == null || string.equals(""))
      string = "empty";
    char[] characters = string.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    return new String(characters);
  }
}
