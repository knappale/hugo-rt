package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * Contains data of a state machine driven class.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Class extends Classifier {
  private Set<StateMachine> stateMachines = new HashSet<StateMachine>();

  /**
   * Creates a new <code>Class</code> instance.
   *
   * @param name class's name
   * @param model class's model
   */
  Class(String name, Model model) {
    super(name, model);
  }


  /**
   * Create a new state machine instance, belonging to this class.
   *
   * @param name state machine's name
   * @return a new state machine instance
   */
  public StateMachine createStateMachine(String name) {
    StateMachine stateMachine = new StateMachine(name, this);
    this.addBehaviour(stateMachine);
    return stateMachine;
  }

  /**
   * Set the state machine of this class
   *
   * @param stateMachine a <code>StateMachine</code>
   */
  public void addBehaviour(StateMachine stateMachine) {
    if (stateMachine != null && !this.stateMachines.contains(stateMachine)) {
      this.stateMachines.add(stateMachine);
      stateMachine.setContext(this);
    }
  }

  /**
   * @return class's state machine
   */
  public Set<StateMachine> getBehaviours() {
    return this.stateMachines;
  }

  /**
   * @return class's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Class [name = ");
    result.append(getName());
    result.append(", #attributes = ");
    result.append(getAttributes().size());
    result.append(", #operations = ");
    result.append(getOperations().size());
    result.append(", #receptions = ");
    result.append(getReceptions().size());
    result.append(", #behaviours = ");
    result.append(getBehaviours().size());
    result.append("]");
    return result.toString();
  }
}
