package uml.parser.xmi.metamodel;


/**
 * Contains data of a data type.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class DataType extends Classifier {
  /**
   * Create a new <code>DateType</code> instance.
   *
   * @param name data type's name
   * @param model data type's model
   */
  DataType(String name, Model model) {
    super(name, model);
  }

  /**
   * @return data type's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("DataType [name = ");
    result.append(getName());
    result.append("]");
    return result.toString();
  }
}
