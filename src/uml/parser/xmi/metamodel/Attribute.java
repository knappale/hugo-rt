package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * Contains data of a UML attribute.
 * 
 * An attribute may either be a class attribute (static) or an instance
 * attribute. Moreover it may be a constant (final) or variable.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Attribute extends Structural {
  private Classifier type = null;
  private int upperMultiplicity = 1;
  private boolean isStatic = false;
  private boolean isFinal = false;
  private List<String> initialValues = new ArrayList<String>();

  /**
   * Creates a new attribute.
   * 
   * @param name attribute's name
   * @param type attribute's classifier type
   * @param owner attribute's owning class
   */
  Attribute(String name, Classifier type, Classifier owner) {
    super(name, owner);
    this.type = type;
  }

  /**
   * @return attribute's type classifier
   */
  public Classifier getType() {
    return this.type;
  }

  /**
   * Add an initial value to this attribute.
   * 
   * @param initialValue initial value
   * @pre initialValue != null
   */
  public void addInitialValue(String initialValue) {
    this.initialValues.add(initialValue);
  }

  /**
   * @return whether attribute has proper initial values
   */
  public boolean hasInitialValues() {
    return this.initialValues.size() > 0;
  }

  /**
   * @return attribute's initial values
   */
  public List<String> getInitialValues() {
    return this.initialValues;
  }

  /**
   * Set attribute's upper multiplicity
   * 
   * @param upper upper multiplicity expression
   */
  public void setUpperMultiplicity(int upperMultiplicity) {
    this.upperMultiplicity = upperMultiplicity;
  }

  /**
   * @return attribute's upper multiplicity
   */
  public int getUpperMultiplicity() {
    return this.upperMultiplicity;
  }

  /**
   * Set attribute's final status.
   * 
   * @param isFinal whether the attribute is final
   */
  public void setFinal(boolean isFinal) {
    this.isFinal = isFinal;
  }

  /**
   * @return whether this attribute is final
   */
  public boolean isFinal() {
    return this.isFinal;
  }

  /**
   * Set attribute's static status.
   * 
   * @param isStatic whether the attribute is static
   */
  public void setStatic(boolean isStatic) {
    this.isStatic = isStatic;
  }

  /**
   * @return whether this attribute is static
   */
  public boolean isStatic() {
    return this.isStatic;
  }

  /**
   * @return whether attribute is a constant (final)
   */
  public boolean isConstant() {
    return this.isFinal();
  }

  /**
   * @return attribute's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Attribute [name = ");
    result.append(getName());
    result.append(", owner = ");
    result.append(getOwner() == null ? "null" : getOwner().getName());
    result.append(", type = ");
    result.append(getType() == null ? "null" : getType().getName());
    result.append(", upperMultiplicity = ");
    result.append(getUpperMultiplicity());
    result.append(", final = ");
    result.append(isFinal());
    result.append(", static = ");
    result.append(isStatic());
    if (hasInitialValues()) {
      result.append(", initialValues = ");
      result.append(getInitialValues().toString());
    }
    result.append("]");
    return result.toString();
  }
}
