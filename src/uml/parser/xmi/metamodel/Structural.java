package uml.parser.xmi.metamodel;


/**
 * Contains data of a UML structural (feature).
 *
 * @author Alexander Knapp (<A HREF="mailto:knapp@informatik.uni-muenchen.de">knapp@informatik.uni-muenchen.de</A>)
 */
public abstract class Structural extends Element {
  private Classifier owner;

  /**
   * Create a structural (feature)
   *
   * @param name structural (feature)'s name
   */
  public Structural(String name, Classifier owner) {
    super(name);
    this.setOwner(owner);
  }

  /**
   * Set structural (feature)'s owner.
   *
   * @param owner structural (feature)'s new owning classifier.
   */
  void setOwner(Classifier owner) {
    this.owner = owner;
  }

  /**
   * @return structural (feature)'s owning classifier
   */
  public Classifier getOwner() {
    return this.owner;
  }
}
