package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * Contains data of a UML behavioural (feature).
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public abstract class Behavioural extends Element {
  private Classifier owner = null;
  private List<Parameter> parameters = new ArrayList<Parameter>();

  /**
   * Create a behavioural (feature)
   *
   * @param name behavioural (feature)'s name
   * @param owner behavioural (feature)'s owning classifier
   */
  Behavioural(String name, Classifier owner) {
    super(name);
    this.setOwner(owner);
  }

  /**
   * Set behavioural (feature)'s owner.
   *
   * @param owner behavioural (feature)'s new owning classifier
   */
  void setOwner(Classifier owner) {
    this.owner = owner;
  }

  /**
   * @return behavioural (feature)'s owning class
   */
  public Classifier getOwner() {
    return this.owner;
  }

  /**
   * Create a parameter for this behavioral (feature).
   *
   * @param name parameter's name
   * @param kind parameter's kind
   * @param type parameter's classifier type
   * @return a new parameter of this behavioral (feature)
   */
  public Parameter createParameter(String name, ParameterDirectionKind kind, Classifier type) {
    Parameter parameter = new Parameter(name, kind, type, this);
    this.addParameter(parameter);
    return parameter;
  }

  /**
   * Add a parameter to behavioural (feature).
   *
   * @param parameter additional parameter
   */
  public void addParameter(Parameter parameter) {
    if (parameter != null && !this.parameters.contains(parameter)) {
      if (parameter.getBehavioural() != null)
        parameter.getBehavioural().removeParameter(parameter);
      this.parameters.add(parameter);
      parameter.setBehavioural(this);
    }
  }

  /**
   * Remove a parameter from behavioural (feature).
   *
   * @param parameter parameter to be removed
   */
  public void removeParameter(Parameter parameter) {
    if (parameter != null && this.parameters.contains(parameter)) {
      this.parameters.remove(parameter);
      parameter.setBehavioural(null);
    }
  }

  /**
   * @return behavioural (feature)'s parameters
   */
  public List<Parameter> getParameters() {
    return this.parameters;
  }
}
