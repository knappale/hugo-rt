package uml.parser.xmi.metamodel;


public enum StateKind {
  INITIAL,
  SHALLOWHISTORY,
  DEEPHISTORY,
  JOIN,
  FORK,
  JUNCTION,
  CHOICE,
  SYNCH,
  SIMPLE,
  COMPOSITE,
  FINAL;

  public boolean isPseudo() {
    switch (this) {
      case INITIAL:
      case SHALLOWHISTORY:
      case DEEPHISTORY:
      case JOIN:
      case FORK:
      case JUNCTION:
      case CHOICE:
      case SYNCH:
        return true;
      //$CASES-OMITTED$
      default:
        return false;
    }
  }

  public String toString() {
    switch (this) {
      case INITIAL: return "initial";
      case SHALLOWHISTORY: return "shallowhistory";
      case DEEPHISTORY: return "deephistory";
      case JOIN: return "join";
      case FORK: return "fork";
      case JUNCTION: return "junction";
      case CHOICE: return "choice";
      case SYNCH: return "synch";
      case SIMPLE: return "simple";
      case COMPOSITE: return "composite";
      case FINAL: return "final";
      default : return "[unknown]";
    }
  }
}
