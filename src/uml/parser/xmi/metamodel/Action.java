package uml.parser.xmi.metamodel;


public class Action extends Element {
  private String script = "";

  /**
   * Create an action.
   *
   * @param script action's script
   */
  public Action(String script) {
    super("");
    this.script = script;
  }

  /**
   * @return action's script
   */
  public String getScript() {
    return this.script;
  }
}
