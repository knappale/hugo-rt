package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * Contains data of a classifier.
 *
 * A classifier always induces a simple type, and thus provides an
 * initial value.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public abstract class Classifier extends Element {
  private Model model = null;
  private List<Attribute> attributes = new ArrayList<Attribute>();
  private List<Operation> operations = new ArrayList<Operation>();
  private List<Reception> receptions = new ArrayList<Reception>();

  /**
   * Create a classifier.
   *
   * @param name classifier's name
   * @param model classifier's model
   */
  Classifier(String name, Model model) {
    super(name);
    this.setModel(model);
  }

  /**
   * Set classifier's model.
   *
   * @param model classifier's new model
   */
  void setModel(Model model) {
    this.model = model;
  }

  /**
   * @return classifier's model
   */
  public Model getModel() {
    return this.model;
  }

  /**
   * Create a new attribute instance, belonging to this classifier.
   * 
   * @param name new attribute's name
   * @param type new attribute's classifier type
   * @return a new attribute instance
   */
  public Attribute createAttribute(String name, Classifier type) {
    Attribute attribute = new Attribute(name, type, this);
    this.addAttribute(attribute);
    return attribute;
  }

  /**
   * Add an attribute to this classifier.
   *
   * @param attribute an <code>Attribute</code>
   */
  public void addAttribute(Attribute attribute) {
    if (attribute != null && !this.attributes.contains(attribute)) {
      this.attributes.add(attribute);
      attribute.setOwner(this);
    }
  }

  /**
   * @return classifier's attributes
   */
  public List<Attribute> getAttributes() {
    return this.attributes;
  }

  /**
   * Create a new operation instance, belonging to this classifier.
   * 
   * @param name new operation's name
   * @return a new operation instance
   */
  public Operation createOperation(String name) {
    Operation operation = new Operation(name, this);
    this.addOperation(operation);
    return operation;
  }

  /**
   * Add an operation to this classifier.
   *
   * @param operation an <code>Operation</code>
   */
  public void addOperation(Operation operation) {
    if (operation != null && !this.operations.contains(operation)) {
      this.operations.add(operation);
      operation.setOwner(this);
    }
  }

  /**
   * Remove an operation from this classifier.
   *
   * @param operation <code>Operation</code> to be removed
   */
  public void removeOperation(Operation operation) {
    if (operation != null && this.operations.contains(operation)) {
      this.operations.remove(operation);
      operation.setOwner(null);
    }
  }

  /**
   * @return whether this classifier shows operations
   */
  public boolean hasOperations() {
    return (this.operations.size() > 0);
  }

  /**
   * @return classifier's operations
   */
  public List<Operation> getOperations() {
    return this.operations;
  }

  /**
   * Create a new reception instance, belonging to this classifier.
   * 
   * @param name new reception's name
   * @param signal new reception's signal
   * @return a new reception instance
   */
  public Reception createReception(String name, Signal signal) {
    Reception reception = new Reception(name, signal, this);
    this.addReception(reception);
    return reception;
  }

  /**
   * Add a reception to this classifier.
   *
   * @param reception a <code>Reception</code>
   */
  public void addReception(Reception reception) {
    if (reception != null && !this.receptions.contains(reception)) {
      this.receptions.add(reception);
      reception.setOwner(this);
    }
  }

  /**
   * @return whether this classifier shows receptions
   */
  public boolean hasReceptions() {
    return (this.receptions.size() > 0);
  }

  /**
   * @return classifier's receptions
   */
  public List<Reception> getReceptions() {
    return this.receptions;
  }

  /**
   * Determine (some) reception of this classifier for a given signal.
   * 
   * @param signal a UML signal
   * @return a reception for <CODE>signal</CODE> of this class; or <CODE>null</CODE>
   * if no such reception exists
   */
  public Reception getReception(Signal signal) {
    for (Reception reception : this.getReceptions()) {
      if (reception.getSignal().equals(signal))
        return reception;
    }
    return null;
  }

  /**
   * @return whether this classifier shows behavioural (feature)s
   */
  public boolean hasBehaviourals() {
    return getBehaviourals().size() > 0;
  }

  /**
   * @return classifier's behavioural (feature)s
   */
  public List<Behavioural> getBehaviourals() {
    List<Behavioural> behaviourals = new ArrayList<Behavioural>();
    behaviourals.addAll(getOperations());
    behaviourals.addAll(getReceptions());
    return behaviourals;
  }

  /**
   * @return classifier's association ends
   */
  public Set<AssociationEnd> getAssociationEnds() {
    Set<AssociationEnd> associationEnds = new HashSet<AssociationEnd>();
    if (this.getModel() == null)
      return associationEnds;

    for (Association association : this.getModel().getAssociations()) {
      for (AssociationEnd associationEnd : association.getAssociationEnds()) {
        if (associationEnd.getType().equals(this))
          associationEnds.add(associationEnd);
      }
    }
    return associationEnds;
  }

  /**
   * @return classifier's opposite association ends
   */
  public Set<AssociationEnd> getOppositeAssociationEnds() {
    Set<AssociationEnd> oppositeAssociationEnds = new HashSet<AssociationEnd>();
    if (this.getModel() == null)
      return oppositeAssociationEnds;

    for (Association association : this.getModel().getAssociations()) {
      for (AssociationEnd associationEnd : association.getAssociationEnds()) {
        if (associationEnd.getType().equals(this))
          oppositeAssociationEnds.add(association.getOppositeAssociationEnd(associationEnd));
      }
    }
    return oppositeAssociationEnds;
  }
}
