package uml.parser.xmi.metamodel;


public enum ParameterDirectionKind {
  IN, OUT, INOUT, RETURN;

  public String toString() {
    switch (this) {
    case IN: return "IN";
    case OUT: return "OUT";
    case INOUT: return "INOUT";
    case RETURN: return "RETURN";
    default: return "[unknown]";
    }
  }
}
