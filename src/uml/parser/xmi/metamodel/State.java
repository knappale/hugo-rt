package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * Contains data of a (UML state machine) state.
 *
 * @author <A HREF="mailto:majnu@gmx.net">Timm Schäfer</A>
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class State extends Element {
  private StateMachine stateMachine;
  private StateKind kind;
  private State container;
  private boolean concurrent;
  /** @inv subStates != null */
  private List<State> subStates = new ArrayList<State>();
  private Action entryAction = null;
  private Action exitAction = null;
  /** @inv deferrableEvents != null */
  private List<Event> deferrableEvents = new ArrayList<Event>();
  /** @inv incomings != null */
  private List<Transition> incomings = new ArrayList<Transition>();
  /** @inv outgoings != null */
  private List<Transition> outgoings = new ArrayList<Transition>();

  /**
   * Create a state.
   *
   * @param name state's name
   * @param kind state's kind
   */
  public State(String name, StateKind kind) {
    super(name);
    this.kind = kind;
  }

  /**
   * Set state's owning state machine
   *
   * @param stateMachine a state machine
   */
  public void setStateMachine(StateMachine stateMachine) {
    if (stateMachine != null && !stateMachine.equals(this.stateMachine)) {
      this.stateMachine = stateMachine;
      this.stateMachine.setTop(this);
    }
  }

  /**
   * @return state's owning state machine
   */
  public StateMachine getStateMachine() {
    if (this.stateMachine == null) {
      if (this.getContainer() != null)
        return this.getContainer().getStateMachine();
    }
    return this.stateMachine;
  }

  /**
   * Set state's kind.
   * 
   * @param kind a state kind
   */
  public void setKind(StateKind kind) {
    this.kind = kind;
  }

  /**
   * @return state's kind
   */
  public StateKind getKind() {
    return this.kind;
  }

  /**
   * 
   */
  public boolean isPseudo() {
    return this.kind.isPseudo();
  }

  /**
   * Set state's container state
   */
  public void setContainer(State container) {
    if (container != null && !container.equals(this.container)) {
      this.container = container;
      this.container.addSubState(this);
    }
  }

  /**
   * @return state's container state
   */
  public State getContainer() {
    return this.container;
  }

  /**
   * @return whether this state is a top state (i.e. has no container)
   */
  public boolean isTop() {
    return (getContainer() == null);
  }

  /**
   * @return state's topmost container state
   */
  public State getTop() {
    return (getContainer() != null ? getContainer().getTop() : this);
  }

  /**
   * @return whether this state is a composite state
   */
  public boolean isComposite() {
    return (getKind() == StateKind.COMPOSITE);
  }

  /**
   * Set concurrency of this state.
   *
   * @param concurrent whether this state is concurrent
   */
  public void setConcurrent(boolean concurrent) {
    this.concurrent = concurrent;
  }

  /**
   * @return whether state is concurrent
   */
  public boolean isConcurrent() {
    return this.concurrent;
  }

  /**
   * Add a sub-state to this state.
   *
   * @param subState sub-state to be added
   */
  public void addSubState(State subState) {
    if (subState != null && !(this.subStates.contains(subState))) {
      if (subState.getContainer() != null && !this.equals(subState.getContainer()))
        subState.getContainer().removeSubState(subState);
      this.subStates.add(subState);
      subState.setContainer(this);
    }
  }

  /**
   * Remove a sub-state from this state.
   *
   * @param subState sub-state to be removed
   */
  public void removeSubState(State subState) {
    if (subState != null && this.subStates.contains(subState)) {
      this.subStates.remove(subState);
      subState.setContainer(null);
    }
  }

  /**
   * @return whether state has (direct) sub-states
   */
  public boolean hasSubStates() {
    return (this.subStates.size() > 0);
  }

  /**
   * @return state's (direct) sub-states
   */
  public List<State> getSubStates() {
    return this.subStates;
  }

  /**
   * Set state's entry action.
   *
   * @param entryAction state's new entry action
   */
  public void setEntry(Action entryAction) {
    this.entryAction = entryAction;
  }

  /**
   * @return state's entry action
   */
  public Action getEntry() {
    return this.entryAction;
  }

  /**
   * Set state's exit action.
   *
   * @param entryAction state's new exit action
   */
  public void setExit(Action exitAction) {
    this.exitAction = exitAction;
  }

  /**
   * @return state's exit action
   */
  public Action getExit() {
    return this.exitAction;
  }

  /**
   * Add an incoming transition to this state
   *
   * @param transition transition to be added to incoming transitions
   */
  public void addIncoming(Transition transition) {
    if (transition != null)
      this.incomings.add(transition);
  }

  /**
   * Remove an incoming transition from this state.
   *
   * @param transition incoming transition to be removed
   */
  public void removeIncoming(Transition transition) {
    if (transition != null && this.incomings.contains(transition)) {
      this.incomings.remove(transition);
      transition.setTarget(null);
    }
  }

  /**
   * @return whether state has incoming transitions
   */
  public boolean hasIncomings() {
    return (this.incomings.size() > 0);
  }

  /**
   * @return state's incoming transitions
   */
  public List<Transition> getIncomings() {
    return this.incomings;
  }

  /**
   * Add an outgoing transition to this state.
   *
   * @param transition transition to be added to outgoing transitions
   */
  public void addOutgoing(Transition transition) {
    if (transition != null)
      this.outgoings.add(transition);
  }

  /**
   * Remove an outgoing transition from this state.
   *
   * @param transition outgoing transition to be removed
   */
  public void removeOutgoing(Transition transition) {
    if (transition != null && this.outgoings.contains(transition)) {
      this.outgoings.remove(transition);
      transition.setSource(null);
    }
  }

  /**
   * @return whether state has outgoing transitions
   */
  public boolean hasOutgoings() {
    return (this.outgoings.size() > 0);
  }

  /**
   * @return state's outgoing transitions
   */
  public List<Transition> getOutgoings() {
    return this.outgoings;
  }

  /**
   * Add a deferrable event to this state
   *
   * @param event new deferrable event
   */
  public void addDeferrableEvent(Event event) {
    if (event != null && !this.deferrableEvents.contains(event))
      this.deferrableEvents.add(event);
  }

  /**
   * @return whether state has deferrable events
   */
  public boolean hasDeferrableEvents() {
    return (this.deferrableEvents.size() > 0);
  }

  /**
   * @return state's deferrable events
   */
  public List<Event> getDeferrableEvents() {
    return deferrableEvents;
  }

  /**
   * @return state's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();

    result.append("State [name = ");
    result.append(this.getName());
    result.append(", kind = ");
    result.append(this.getKind());
    if (isConcurrent())
      result.append(" (concurrent)");
    result.append(", owner = ");
    result.append(this.getStateMachine() == null ? "null" : this.getStateMachine().getName());
    result.append(", container = ");
    result.append(this.getContainer() == null ? "null" : this.getContainer().getName());
    result.append(", #incomings = ");
    result.append(this.getIncomings().size());
    result.append(", #outgoings = ");
    result.append(this.getOutgoings().size());
    result.append(", #subStates = ");
    result.append(this.getSubStates().size());
    result.append("]");
    return result.toString();
  }
}
