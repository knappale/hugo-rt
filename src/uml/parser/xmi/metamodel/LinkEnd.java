package uml.parser.xmi.metamodel;


public class LinkEnd extends Element {
  private Object instance;
  private Link link;
  private AssociationEnd associationEnd;

  public LinkEnd(String name, Object instance, Link link) {
    super(name);
    this.instance = instance;
    this.link = link;
  }

  /**
   * @return link end's object
   */
  public Object getInstance() {
    return this.instance;
  }

  /**
   * Set link end's link.
   * 
   * @param link a link
   */
  public void setLink(Link link) {
    if (this.getLink() == null || !this.getLink().equals(link)) {
      this.link = link;
      link.addLinkEnd(this);
    }
  }

  /**
   * @return link end's link
   */
  public Link getLink() {
    return this.link;
  }

  /**
   * @return opposite link end of this link end w.r.t. underlying link
   */
  public LinkEnd getOppositeLinkEnd() {
    return this.link.getOppositeLinkEnd(this);
  }

  /**
   * Set link end's association end.
   *
   * @param associationEnd link end's new association end
   */
  public void setAssociationEnd(AssociationEnd associationEnd) {
    this.associationEnd = associationEnd;
  }

  /**
   * @return link end's association end
   */
  public AssociationEnd getAssociationEnd() {
    return this.associationEnd;
  }

  /**
   * @return link end's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("LinkEnd [name = ");
    result.append(this.getName());
    result.append(", instance = ");
    result.append(this.getInstance() == null ? "null" : getInstance().getName());
    result.append(", link = ");
    result.append(this.getLink() == null ? "null" : getLink().getName());
    result.append(", associationEnd = ");
    result.append(this.getAssociationEnd() == null ? "null" : getAssociationEnd().getName());
    result.append("]");
    return result.toString();
  }
}
