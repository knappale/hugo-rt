package uml.parser.xmi.metamodel;


/**
 * Contains data of a UML signal.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Signal extends Classifier {
  /**
   * Creates a signal.
   *
   * @param name signal's name
   * @param model signal's model
   * @pre model != null
   */
  public Signal(String name, Model model) {
    super(name, model);
  }

  /**
   * @see uml.parser.xmi.metamodel.Classifier#addToModel(uml.parser.xmi.metamodel.Model)
   */
  void addToModel(Model model) {
    if (model != null)
      model.addSignal(this);
  }

  /**
   * @see uml.parser.xmi.metamodel.Classifier#removeFromModel()
   */
  void removeFromModel() {
    if (this.getModel() != null)
      this.getModel().removeSignal(this);
  }


  /**
   * @return signal's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Signal [name = ");
    result.append(getName());
    result.append("]");
    return result.toString();
  }
}
