package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * UML association role.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class AssociationRole extends Element {
  private Association base;
  private List<AssociationEndRole> associationEndRoles = new ArrayList<AssociationEndRole>();
  private Collaboration collaboration;

  AssociationRole(String name, Association base, Collaboration collaboration) {
    super(name);
    this.base = base;
    this.collaboration = collaboration;
  }

  /**
   * Set association role's base association.
   *
   * @param base association role's new base association
   */
  public void setBase(Association base) {
    this.base = base;
  }

  /**
   * @return association role's base association
   */
  public Association getBase() {
    return this.base;
  }

  /**
   * Set association role's collaboration.
   *
   * @param collaboration a collaboration
   */
  void setCollaboration(Collaboration collaboration) {
    this.collaboration = collaboration;
  }

  /**
   * @return classifier role's collaboration
   */
  public Collaboration getCollaboration() {
    return this.collaboration;
  }

  /**
   * Create a new association end role instance, belonging to this association role.
   * 
   * @param name new association end role's name
   * @param type new association end role's classifier role type
   * @return a new association end role instance
   */
  public AssociationEndRole createAssociationEndRole(String name, ClassifierRole type, AssociationEnd base) {
    AssociationEndRole associationEndRole = new AssociationEndRole(name, type, base, this);
    this.addAssociationEndRole(associationEndRole);
    return associationEndRole;
  }

  /**
   * Add an association end role to this association role.
   *
   * @param associationEndRole association end role to be added
   */
  public void addAssociationEndRole(AssociationEndRole associationEndRole) {
    if (associationEndRole != null && !this.getAssociationEndRoles().contains(associationEndRole)) {
      if (associationEndRole.getAssociationRole() != null)
        associationEndRole.getAssociationRole().removeAssociationEndRole(associationEndRole);
      this.associationEndRoles.add(associationEndRole);
      associationEndRole.setAssociationRole(this);
    }
  }

  /**
   * Remove an association end role from this association role.
   *
   * @param associationEndRole association end role to be removed
   */
  public void removeAssociationEndRole(AssociationEndRole associationEndRole) {
    if (associationEndRole != null && this.getAssociationEndRoles().contains(associationEndRole)) {
      this.associationEndRoles.remove(associationEndRole);
      associationEndRole.setAssociationRole(null);
    }
  }

  /**
   * @return association role's association end roles
   */
  public List<AssociationEndRole> getAssociationEndRoles() {
    return this.associationEndRoles;
  }

  /**
   * Determine opposite association end of an association end w.r.t. to this association.
   *
   * @param oppositeAssociationEnd an association end of this association
   * @return opposite association end
   */
  public AssociationEndRole getOppositeAssociationEndRole(AssociationEndRole oppositeAssociationEndRole) {
    for (AssociationEndRole associationEndRole : this.getAssociationEndRoles())
      if (!associationEndRole.equals(oppositeAssociationEndRole))
        return associationEndRole;
    return null;
  }

  /**
   * @return association role's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("AssociationRole [name = ");
    result.append(this.getName());
    result.append(", base = ");
    result.append(this.getBase() == null ? "null" : this.getBase().getName());
    result.append(", collaboration = ");
    result.append(this.getCollaboration() == null ? "null" : this.getCollaboration().getName());
    result.append(", #association end roles = ");
    result.append(this.getAssociationEndRoles().size());
    result.append("]");
    return result.toString();
  }
}
