package uml.parser.xmi.metamodel;


/**
 * UML association end role.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class AssociationEndRole extends Element {
  private AssociationRole associationRole;
  private AssociationEnd base;
  private ClassifierRole type;

  AssociationEndRole(String name, ClassifierRole type, AssociationEnd base, AssociationRole associationRole) {
    super(name);
    this.type = type;
    this.base = base;
    this.associationRole = associationRole;
  }

  /**
   * Set association end role's base association end.
   *
   * @param base association end role's new base association end
   */
  public void setBase(AssociationEnd base) {
    this.base = base;
  }

  /**
   * @return association end role's base association end
   */
  public AssociationEnd getBase() {
    return this.base;
  }

  /**
   * @return association end's type classifier role
   */
  public ClassifierRole getType() {
    return this.type;
  }

  /**
   * Set association end role's association role.
   * 
   * @param associationRole an association role
   */
  public void setAssociationRole(AssociationRole associationRole) {
    if (this.getAssociationRole() == null || !this.getAssociationRole().equals(associationRole)) {
      this.associationRole = associationRole;
      associationRole.addAssociationEndRole(this);
    }
  }

  /**
   * @return association end's association
   */
  public AssociationRole getAssociationRole() {
    return this.associationRole;
  }

  /**
   * @return opposite association end role of this association end role w.r.t. underlying association role
   */
  public AssociationEndRole getOppositeAssociationEndRole() {
    return this.associationRole.getOppositeAssociationEndRole(this);
  }

  /**
   * @return association end role's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("AssociationEndRole [name = ");
    result.append(this.getName());
    result.append(", base = ");
    result.append(this.getBase() == null ? "null" : this.getBase().getName());
    result.append(", association role = ");
    result.append(this.getAssociationRole() == null ? "null" : this.getAssociationRole().getName());
    result.append("]");
    return result.toString();
  }
}
