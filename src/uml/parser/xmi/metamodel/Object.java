package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * Contains data of a (UML) object.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Object extends Element {
  private Model model;
  /** @inv hclass != null */
  private Class hclass;
  private List<Slot> slots = new ArrayList<Slot>();

  /**
   * Create a named object of a given class.
   *
   * @param name a name
   * @param hclass a class
   * @return a new object
   */
  public Object(String name, Class hclass) {
    super(name);
    this.hclass = hclass;
  }

  /**
   * Set object's model.
   *
   * @param model object's new model
   */
  void setModel(Model model) {
    this.model = model;
  }

  /**
   * @return object's model
   */
  public Model getModel() {
    return this.model;
  }

  /**
   * Determine object's class
   *
   * @return object's class
   */
  public Class getClassifier() {
    return hclass;
  }

  /**
   * Add a slot to the object.
   *
   * @param slot a slot
   */
  public void addSlot(Slot slot) {
    slots.add(slot);
  }

  /**
   * Determine object's slots.
   *
   * @return object's slots
   */
  public List<uml.parser.xmi.metamodel.Slot> getSlots() {
    return this.slots;
  }

  /**
   * @return whether this object is a constant, i.e. <CODE>false</CODE>
   */
  public boolean isConstant() {
    return false;
  }

  /**
   * @return object's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Object [name = ");
    result.append(this.getName());
    result.append(", class = ");
    result.append(this.getClassifier().getName());
    result.append(", slots = ");
    result.append(this.getSlots());
    result.append("]");
    return result.toString();
  }
}
