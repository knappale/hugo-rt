package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * UML association end.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class AssociationEnd extends Element {
  private Association association;
  private Classifier type;
  private boolean isNavigable = true;
  private int upperMultiplicity = 1;

  AssociationEnd(String name, Classifier type, Association association) {
    super(name);
    this.association = association;
    this.type = type;
  }

  /**
   * Set association end's association.
   * 
   * @param association association end's new association
   */
  void setAssociation(Association association) {
    this.association = association;
  }

  /**
   * @return association end's association
   */
  public Association getAssociation() {
    return this.association;
  }

  /**
   * @return association end's (type) classifier
   */
  public Classifier getType() {
    return this.type;
  }

  /**
   * Set association end's navigability.
   *
   * @param isNavigable association end's new navigability
   */
  public void setNavigable(boolean isNavigable) {
    this.isNavigable = isNavigable;
  }

  /**
   * @return association end's navigability
   */
  public boolean isNavigable() {
    return this.isNavigable;
  }

  /**
   * Set association end's upper multiplicity.
   *
   * @param upperMultiplicity association end's new upper multiplicity
   */
  public void setUpperMultiplicity(int upperMultiplicity) {
    this.upperMultiplicity = upperMultiplicity;
  }

  /**
   * @return association end's upper multiplicity
   */
  public int getUpperMultiplicity() {
    return this.upperMultiplicity;
  }

  /**
   * @return association end's link ends
   */
  public Set<LinkEnd> getLinkEnds() {
    Set<LinkEnd> linkEnds = new HashSet<LinkEnd>();
    Model model = this.getAssociation().getModel();
    if (model == null)
      return linkEnds;

    for (Link link : model.getLinks()) {
      for (LinkEnd linkEnd : link.getLinkEnds())
        if (this.equals(linkEnd.getAssociationEnd()))
          linkEnds.add(linkEnd);
    }

    return linkEnds;
  }

  /**
   * @return opposite association end of this association end w.r.t. underlying association
   */
  public AssociationEnd getOppositeAssociationEnd() {
    if (this.association != null)
      return this.association.getOppositeAssociationEnd(this);
    else
      return null;
  }

  /**
   * @return association end's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("AssociationEnd [name = ");
    result.append(getName());
    result.append(", type = ");
    result.append(getType() == null ? "null" : getType().getName());
    result.append(", upperMultiplicity = ");
    result.append(getUpperMultiplicity());
    result.append("]");
    return result.toString();
  }
}
