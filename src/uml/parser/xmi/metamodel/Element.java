package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * A UML element.
 *
 * An element is a UML entity that can be referred to by a name.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public abstract class Element {
  /** @inv name != null */
  private String name = "";
  /** @inv constraints != null */
  private List<Constraint> constraints = new ArrayList<Constraint>();
  /** @inv taggedValues != null */
  private List<TaggedValue> taggedValues = new ArrayList<TaggedValue>();

  /**
   * Creates a new UML element
   *
   * @param name the name of the UML element
   */
  public Element(String name) {
    if (name != null)
      this.name = name;
  }

  /**
   * Set element's name.
   *
   * @param name element's new name
   */
  public void setName(String name) {
    if (name != null)
      this.name = name;
  }

  /**
   * @return element's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Add a constraint to this element.
   *
   * @param constraint a constraint
   */
  public void addConstraint(Constraint constraint) {
    if (constraint != null)
      this.constraints.add(constraint);
  }

  /**
   * Remove a constraint from this element.
   *
   * @param constraint constraint to be removed
   */
  public void removeConstraint(Constraint constraint) {
    if (constraint != null)
      this.constraints.remove(constraint);
  }

  /**
   * @return element's constraints
   */
  public List<Constraint> getConstraints() {
    return this.constraints;
  }

  /**
   * Add a tagged value to this element.
   *
   * @param taggedValue a tagged value
   */
  public void addTaggedValue(TaggedValue taggedValue) {
    if (taggedValue != null)
      this.taggedValues.add(taggedValue);
  }

  /**
   * @return element's tagged values
   */
  public List<TaggedValue> getTaggedValues() {
    return this.taggedValues;
  }
}
