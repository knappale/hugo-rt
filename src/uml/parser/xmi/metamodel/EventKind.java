package uml.parser.xmi.metamodel;


public enum EventKind {
  COMPLETION,
  CALL,
  SIGNAL,
  TIME;

  public String toString() {
    switch (this) {
      case COMPLETION: return "completion";
      case CALL: return "call";
      case SIGNAL: return "signal";
      case TIME: return "time";
      default : return "[unknown]";
    }
  }
}
