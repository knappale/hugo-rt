package uml.parser.xmi.metamodel;


/**
 * Contains data of a UML operation
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Operation extends Behavioural {
  /**
   * Create an operation.
   *
   * @param name operation's name
   * @param owner operation's owning classifier
   */
  Operation(String name, Classifier owner) {
    super(name, owner);
  }

  /**
   * @return operation's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Operation [name = ");
    result.append(getName());
    result.append(", owner = ");
    result.append(getOwner() == null ? "null" : getOwner().getName());
    result.append(", #parameters = ");
    result.append(getParameters().size());
    result.append("]");
    return result.toString();
  }
}
