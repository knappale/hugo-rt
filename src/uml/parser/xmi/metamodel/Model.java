package uml.parser.xmi.metamodel;

import java.util.*;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * UML model.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Model extends Element {
  private @NonNull List<@NonNull Class> classes = new ArrayList<>();
  private @NonNull Set<@NonNull Association> associations = new HashSet<>();
  private @NonNull Set<@NonNull DataType> datatypes = new HashSet<>();
  private @NonNull Set<@NonNull Signal> signals = new HashSet<>();
  private @NonNull Set<@NonNull Collaboration> collaborations = new HashSet<>();
  private @NonNull Set<@NonNull Object> objects = new HashSet<>();
  private @NonNull Set<@NonNull Link> links = new HashSet<>();
  
  /**
   * Create a new named model.
   *
   * @param name model's name
   * @return a model
   */
  public Model(String name) {
    super(name);
  }

  /**
   * Create a new class instance, belonging to this model.
   * 
   * @param name new class's name
   * @return a new class instance
   */
  public Class createClass(String name) {
    Class hclass = new Class(name, this);
    this.addClass(hclass);
    return hclass;
  }

  /**
   * Add a class to this model.
   *
   * @param hclass a class
   */
  public void addClass(Class hclass) {
    if (hclass != null && !this.classes.contains(hclass)) {
      if (hclass.getModel() != null)
        hclass.getModel().removeClass(hclass);
      this.classes.add(hclass);
      hclass.setModel(this);
    }
  }

  /**
   * Remove a class from this model.
   *
   * @param hclass a class
   */
  public void removeClass(Class hclass) {
    if (hclass != null && this.classes.contains(hclass)) {
      this.classes.remove(hclass);
      hclass.setModel(null);
    }
  }
  
  /**
   * Determine model's classes.
   *
   * @return model's classes
   */
  public @NonNull List<@NonNull Class> getClasses() {
    return this.classes;
  }

  /**
   * Create a new association instance, belonging to this model.
   * 
   * @param name new association's name
   * @return a new association instance
   */
  public Association createAssociation(String name) {
    Association association = new Association(name, this);
    this.addAssociation(association);
    return association;
  }

  /**
   * Add an association to the model.
   *
   * @param association an association
   */
  public void addAssociation(Association association) {
    if (association != null && !this.associations.contains(association)) {
      if (association.getModel() != null)
        association.getModel().removeAssociation(association);
      this.associations.add(association);
      association.setModel(this);
    }
  }

  /**
   * Remove an association from the model.
   *
   * @param association an association
   */
  public void removeAssociation(Association association) {
    if (association != null && this.associations.contains(association)) {
      this.associations.remove(association);
      association.setModel(null);
    }
  }

  /**
   * Determine model's associations.
   *
   * @return model's associations
   */
  @NonNullByDefault
  public Set<Association> getAssociations() {
    return this.associations;
  }

  /**
   * Create a new data type instance, belonging to this model.
   * 
   * @param name new data type's name
   * @return a new data type instance
   */
  public DataType createDataType(String name) {
    DataType dataType = new DataType(name, this);
    this.addDataType(dataType);
    return dataType;
  }

  /**
   * Add a data type to the model.
   *
   * @param datatype a data type
   */
  public void addDataType(DataType dataType) {
    if (dataType != null && !this.datatypes.contains(dataType)) {
      if (dataType.getModel() != null)
        dataType.getModel().removeDataType(dataType);
      this.datatypes.add(dataType);
      dataType.setModel(this);
    }
  }

  /**
   * Remove a data type from the model.
   *
   * @param datatype a data type
   */
  public void removeDataType(DataType dataType) {
    if (dataType != null && this.datatypes.contains(dataType)) {
      this.datatypes.remove(dataType);
      dataType.setModel(null);
    }
  }

  /**
   * Determine model's data types.
   *
   * @return model's data types
   */
  public @NonNull Set<@NonNull DataType> getDataTypes() {
    return this.datatypes;
  }

  /**
   * Create a new signal instance, belonging to this model.
   * 
   * @param name new signal's name
   * @return a new signal instance
   */
  public Signal createSignal(String name) {
    Signal signal = new Signal(name, this);
    this.addSignal(signal);
    return signal;
  }

  /**
   * Add a signal to the model.
   *
   * @param signal a signal
   */
  public void addSignal(Signal signal) {
    if (signal != null && !this.signals.contains(signal)) {
      if (signal.getModel() != null)
        signal.getModel().removeSignal(signal);
      this.signals.add(signal);
      signal.setModel(this);
    }
  }

  /**
   * Remove a signal from the model.
   *
   * @param signal a signal
   */
  public void removeSignal(Signal signal) {
    if (signal != null && this.signals.contains(signal)) {
      this.signals.remove(signal);
      signal.setModel(null);
    }
  }

  /**
   * Determine model's signals.
   *
   * @return model's signals
   */
  @NonNullByDefault
  public Set<Signal> getSignals() {
    return this.signals;
  }

  /**
   * Create a new collaboration instance, belonging to this model.
   * 
   * @param name new collaboration's name
   * @return a new collaboration instance
   */
  public Collaboration createCollaboration(String name) {
    Collaboration collaboration = new Collaboration(name, this);
    this.addCollaboration(collaboration);
    return collaboration;
  }

  /**
   * Add a collaboration to the model.
   *
   * @param collaboration a collaboration
   */
  public void addCollaboration(Collaboration collaboration) {
    if (collaboration != null && !this.collaborations.contains(collaboration)) {
      if (collaboration.getModel() != null)
        collaboration.getModel().removeCollaboration(collaboration);
      this.collaborations.add(collaboration);
      collaboration.setModel(this);
    }
  }

  /**
   * Remove a collaboration from the model.
   *
   * @param collaboration a collaboration
   */
  public void removeCollaboration(Collaboration collaboration) {
    if (collaboration != null && this.collaborations.contains(collaboration)) {
      this.collaborations.remove(collaboration);
      collaboration.setModel(null);
    }
  }

  /**
   * Determine model's collaborations.
   *
   * @return model's collaborations
   */
  public @NonNull Set<@NonNull Collaboration> getCollaborations() {
    return this.collaborations;
  }

  /**
   * Create a new object, belonging to this model.
   * 
   * @param name new object's name
   * @param hclass new object's class
   * @return a new object instance
   */
  public Object createObject(String name, Class hclass) {
    Object object = new Object(name, hclass);
    this.addObject(object);
    return object;
  }

  /**
   * Add an object to this model.
   *
   * @param object an object
   */
  public void addObject(Object object) {
    if (object != null && !this.objects.contains(object)) {
      if (object.getModel() != null)
        object.getModel().removeObject(object);
      this.objects.add(object);
      object.setModel(this);
    }
  }

  /**
   * Remove an object from the model.
   *
   * @param object an object
   */
  public void removeObject(Object object) {
    if (object != null && this.objects.contains(object)) {
      this.objects.remove(object);
      object.setModel(null);
    }
  }

  /**
   * Determine model's objects.
   *
   * @return model's objects
   */
  public @NonNull Set<@NonNull Object> getObjects() {
    return this.objects;
  }

  /**
   * Create a new link, belonging to this model.
   * 
   * @param name new link's name
   * @param association new link's association
   * @return a new object instance
   */
  public Link createLink(String name, Association association) {
    Link link = new Link(name, association);
    this.addLink(link);
    return link;
  }

  /**
   * Add a link to this model.
   *
   * @param link a link
   */
  public void addLink(Link link) {
    if (link != null && !this.links.contains(link)) {
      if (link.getModel() != null)
        link.getModel().removeLink(link);
      this.links.add(link);
      link.setModel(this);
    }
  }

  /**
   * Remove a link from the model.
   *
   * @param link a link
   */
  public void removeLink(Link link) {
    if (link != null && this.links.contains(link)) {
      this.links.remove(link);
      link.setModel(null);
    }
  }

  /**
   * Determine model's links.
   *
   * @return model's objects
   */
  public @NonNull Set<@NonNull Link> getLinks() {
    return this.links;
  }
}
