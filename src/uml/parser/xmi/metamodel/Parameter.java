package uml.parser.xmi.metamodel;


/**
 * Contains data of an operation parameter.
 *
 * @author <A HREF="mailto:majnu@gmx.net">Timm Sch�fer</A>
 * @author <A HREF="mailto:baeumlea@cip.ifi.lmu.de">Simon B�umler</A>
*/
public class Parameter extends Element {
  private Behavioural behavioural = null;
  private ParameterDirectionKind kind;
  private Classifier type;

  /**
   * Create a parameter.
   *
   * @param name parameter's name
   * @param type parameter's type classifier
   * @param owner parameter's owning behavioural (feature)
   */
  public Parameter(String name, ParameterDirectionKind kind, Classifier type, Behavioural owner) {
    super(name);
    this.kind = kind;
    this.type = type;
    this.behavioural = owner;
  }

  /**
   * Set parameter's owning behavioural.
   *
   * @param behavioural parameter's new owning behavioural
   */
  void setBehavioural(Behavioural behavioural) {
    this.behavioural = behavioural;
  }

  /**
   * @return parameter's owning behavioural
   */
  public Behavioural getBehavioural() {
    return this.behavioural;
  }

  /**
   * @return parameter's direction kind
   */
  public ParameterDirectionKind getKind() {
    return this.kind;
  }

  /**
   * @return parameter's classifier
   */
  public Classifier getType() {
    return this.type;
  }

  /**
   * @return parameter's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Parameter [name = ");
    result.append(this.getName());
    result.append(", kind = ");
    result.append(this.getKind());
    result.append(", classifier = ");
    result.append(this.getType().getName());
    result.append("]");
    return result.toString();
  }
}
