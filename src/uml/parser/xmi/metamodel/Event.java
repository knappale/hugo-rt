package uml.parser.xmi.metamodel;


/**
 * Contains data of an event.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Event extends Element {
  private EventKind kind = EventKind.COMPLETION;
  private State state = null;
  private Operation operation = null;
  private Signal signal = null;
  private String when = null;

  /**
   * Create a new event.
   *
   * @param name event's name
   * @param kind event's kind
   */
  public Event(String name, EventKind kind) {
    super(name);
    this.kind = kind;
  }

  /**
   * Set event's kind.
   *
   * @param kind event's new kind
   */
  public void setKind(EventKind kind) {
    this.kind = kind;
  }

  /**
   * @return event's kind
   */
  public EventKind getKind() {
    return this.kind;
  }

  /**
   * Set call event's operation.
   *
   * @param operation call event's new operation
   * @pre this.getKind() == EventKind.CALL
   */
  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  /**
   * @return call event's operation
   * @pre this.getKind() == EventKind.CALL
   */
  public Operation getOperation() {
    return this.operation;
  }

  /**
   * Set signal event's signal.
   *
   * @param signal signal event's new signal
   * @pre this.getKind() == EventKind.SIGNAL
   */
  public void setSignal(Signal signal) {
    this.signal = signal;
  }

  /**
   * @return signal event's signal
   * @pre this.getKind() == EventKind.SIGNAL
   */
  public Signal getSignal() {
    return this.signal;
  }

  /**
   * Set completion or time event's state.
   *
   * @param state completion or time event's new state
   * @pre this.getKind() == EventKind.COMPLETION || this.getKind() == EventKind.TIME
   */
  public void setState(State state) {
    this.state = state;
  }

  /**
   * @return completion or time event's state
   * @pre this.getKind() == EventKind.COMPLETION || this.getKind() == EventKind.TIME
   */
  public State getState() {
    return this.state;
  }

  /**
   * Set time event's when expression.
   *
   * @param when time event's new when expression
   * @pre this.getKind() == EventKind.TIME
   */
  public void setWhen(String when) {
    this.when = when;
  }

  /**
   * @return time event's when expression
   * @pre this.getKind() == EventKind.TIME
   */
  public String getWhen() {
    return this.when;
  }

  /**
   * @return event's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Event [kind = ");
    result.append(this.getKind());
    result.append(", name = ");
    result.append(this.getName());
    if (this.getKind() == EventKind.SIGNAL) {
      result.append(", signal = ");
      result.append(this.getSignal() == null ? "null" : this.getSignal().getName());
    }
    if (this.getKind() == EventKind.CALL) {
      result.append(", operation = ");
      result.append(this.getOperation() == null ? "null" : this.getOperation().getName());
    }
    if (this.getKind() == EventKind.TIME) {
      result.append(", state = ");
      result.append(this.getState() == null ? "null" : this.getState().getName());
      result.append(", when = ");
      result.append(this.getWhen());
    }
    if (this.getKind() == EventKind.COMPLETION) {
      result.append(", completion = ");
      result.append(this.getState() == null ? "null" : this.getState().getName());
    }
    result.append("]");
    return result.toString();
  }
}
