package uml.parser.xmi.metamodel;


public class Stimulus extends Element {
  // @inv link != null
  private Link link;
  // @inv sender != null
  private Object sender;
  // @inv receiver != null
  private Object receiver;

  public Stimulus(String name, Object sender, Object receiver, Link link) {
    super(name);
    this.sender = sender;
    this.receiver = receiver;
    this.link = link;
  }

  /**
   * @return stimulus' link
   */
  public Link getLink() {
    return this.link;
  }

  /**
   * @return stimulus' sender object
   */
  public Object getSender() {
    return this.sender;
  }

  /**
   * @return stimulus' receiver object
   */
  public Object getReceiver() {
    return this.receiver;
  }

  /**
   * @return stimulus' string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Stimulus [name = ");
    result.append(this.getName());
    result.append(", link = ");
    result.append(this.getLink().getName());
    result.append(", sender = ");
    result.append(getSender().getName());
    result.append(", receiver = ");
    result.append(getReceiver().getName());
    result.append("]");
    return result.toString();
  }
}
