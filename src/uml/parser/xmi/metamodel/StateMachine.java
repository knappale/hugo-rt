package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * UML state machine
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class StateMachine extends Element {
  private Class context = null;
  private State top = null;
  /** @inv transitions != null */
  private Set<Transition> transitions = new HashSet<Transition>();

  /**
   * Create a state machine.
   *
   * @param context state machine's context class
   */
  StateMachine(String name, Class context) {
    super(name);
    this.context = context;
  }

  /**
   * Set state machine's context class.
   *
   * @param context state machine's new context class
   */
  void setContext(Class context) {
    this.context = context;
  }

  /**
   * @return state machine's context class
   */
  public Class getContext() {
    return this.context;
  }

  /**
   * Set state machine's top state.
   *
   * @param top state machine's new top state
   */
  public void setTop(State top) {
    if (top != null && !top.equals(this.top)) {
      this.top = top;
      this.top.setStateMachine(this);
    }
  }

  /**
   * @return state machine's top state
   */
  public State getTop() {
    return this.top;
  }

  /**
   * Create a transition for this state machine.
   *
   * @param name transition's name
   * @param source transition's source state
   * @param target transition's target state
   * @return a new transition for this state machine
   */
  public Transition createTransition(String name, State source, State target) {
    Transition transition = new Transition(name, source, target, this);
    this.addTransition(transition);
    return transition;
  }

  /**
   * Add a transition to this state machine.
   *
   * @param transition transition to be added
   */
  public void addTransition(Transition transition) {
    if (transition != null && !(this.transitions.contains(transition))) {
      this.transitions.add(transition);
      transition.setStateMachine(this);
    }
  }

  /**
   * Add transitions to this state machine.
   *
   * @param transitions set of transitions to be added
   */
  public void addTransitions(Set<Transition> transitions) {
    if (transitions != null) {
      for (Transition transition : transitions)
        this.addTransition(transition);
    }
  }

  /**
   * @return state machine's transitions
   */
  public Set<Transition> getTransitions() {
    return this.transitions;
  }

  /**
   * @return state machine's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();

    result.append("StateMachine [context = ");
    result.append(getContext() == null ? "null" : getContext().getName());
    result.append(", top = ");
    result.append(getTop() == null ? "null" : getTop().getName());
    result.append(", #transitions = ");
    result.append(getTransitions().size());
    result.append("]");
    return result.toString();
  }
}
