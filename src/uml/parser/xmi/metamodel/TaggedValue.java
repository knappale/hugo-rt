package uml.parser.xmi.metamodel;

/**
 * UML tagged value.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class TaggedValue {
  /** @inv tag != null */
  private String tag = "";
  /** @inv value != null */
  private String value = "";

  /**
   * Create a tagged value.
   *
   * @param tag tagged value's tag
   * @param value tagged value's value
   */
  public TaggedValue(String tag, String value) {
    if (tag != null)
      this.tag = tag;
    if (value != null)
      this.value = value;
  }

  /**
   * @return tagged value's tag
   */
  public String getTag() {
    return this.tag;
  }

  /**
   * @return tagged value's value
   */
  public String getValue() {
    return this.value;
  }

  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("TaggedValue [tag = ");
    result.append(this.getTag());
    result.append(", value = ");
    result.append(this.getValue());
    result.append("]");
    return result.toString();
  }
}
