package uml.parser.xmi.metamodel;


/**
 * Contains data of a transition.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Transition extends Element {
  private StateMachine stateMachine;
  private State source;
  private State target;
  private Event trigger;
  private String guard;
  private Action effect;

  /**
   * @pre source != null && target != null
   */
  public Transition(String name, State source, State target, StateMachine stateMachine) {
    super(name);
    this.source = source;
    this.target = target;
    this.stateMachine = stateMachine;
    if (this.source != null)
      this.source.addOutgoing(this);
    if (this.target != null)
      this.target.addIncoming(this);
  }

  /**
   * Set state machine to which this transition belongs.
   *
   * @param stateMachine transition's new owning state machine
   */
  void setStateMachine(StateMachine stateMachine) {
    this.stateMachine = stateMachine;
  }

  /**
   * @return transition's state machine
   */
  public StateMachine getStateMachine() {
    return this.stateMachine;
  }

  /**
   * Set transition's source.
   *
   * @param source transition's new target
   */
  public void setSource(State source) {
    if (source == null) {
      if (this.source != null)
        this.source.removeOutgoing(this);
      this.source = null;
    }
    else {
      if (!source.equals(this.source)) {
        if (this.source != null)
          this.source.removeOutgoing(this);
        this.source = source;
        this.source.addOutgoing(this);
      }
    }
  }

  /**
   * @return transition's source state
   */
  public State getSource() {
    return this.source;
  }

  /**
   * Set transition's target.
   *
   * @param target transition's new target
   */
  public void setTarget(State target) {
    if (target == null) {
      if (this.target != null)
        this.target.removeIncoming(this);
      this.target = null;
    }
    else {
      if (!target.equals(this.target)) {
        if (this.target != null)
          this.target.removeIncoming(this);
        this.target = target;
        this.target.addIncoming(this);
      }
    }
  }

  /**
   * @return transition's target state
   */
  public State getTarget() {
    return this.target;
  }

  /**
   * Set transition's triggering event.
   *
   * @param trigger transition's triggering event
   */
  public void setTrigger(Event trigger) {
    this.trigger = trigger;
  }

  /**
   * @return transition's triggering event
   */
  public Event getTrigger() {
    return this.trigger;
  }

  /**
   * Set transition's guard.
   *
   * @param guard transition's guard
   */
  public void setGuard(String guard) {
    this.guard = guard;
  }

  /**
   * @return transition's guard
   */
  public String getGuard() {
    return this.guard;
  }

  /**
   * Set transition's effect.
   *
   * @param effect transition's effect
   */
  public void setEffect(Action effect) {
    this.effect = effect;
  }

  /**
   * @return transition's effect action
   */
  public Action getEffect() {
    return this.effect;
  }

  /**
   * @return transition's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();

    result.append("Transition [name = ");
    result.append(this.getName());
    result.append(", owner = ");
    result.append(this.getStateMachine() == null ? "null" : this.getStateMachine().getName());
    result.append(", source = ");
    result.append(this.getSource().getName());
    result.append(", target = ");
    result.append(this.getTarget().getName());
    result.append(", trigger = ");
    result.append(this.getTrigger() == null ? "null" : this.getTrigger().getName());
    result.append(", guard = ");
    result.append(this.getGuard() == null ? "null" : this.getGuard().toString());
    result.append(", effect = ");
    result.append(this.getEffect() == null ? "null" : this.getEffect().getScript());
    result.append("]");
    return result.toString();
  }
}
