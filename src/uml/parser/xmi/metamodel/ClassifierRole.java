package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * UML classifier role.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class ClassifierRole extends Element {
  private Classifier base;
  private Collaboration collaboration;

  ClassifierRole(String name, Classifier base, Collaboration collaboration) {
    super(name);
    this.base = base;
    this.collaboration = collaboration;
  }

  /**
   * Set classifier role's base classifier.
   *
   * @param base classifier role's new base classifier
   */
  public void setBase(Classifier base) {
    this.base = base;
  }

  /**
   * @return classifier role's base classifier
   */
  public Classifier getBase() {
    return this.base;
  }

  /**
   * Set classifier role's collaboration.
   *
   * @param collaboration a collaboration
   */
  void setCollaboration(Collaboration collaboration) {
    this.collaboration = collaboration;
  }

  /**
   * @return classifier role's collaboration
   */
  public Collaboration getCollaboration() {
    return this.collaboration;
  }

  /**
   * @return classifier role's association end roles
   */
  public Set<AssociationEndRole> getAssociationEndRoles() {
    Set<AssociationEndRole> associationEndRoles = new HashSet<AssociationEndRole>();
    if (this.getCollaboration() == null)
      return associationEndRoles;

    for (AssociationRole associationRole : this.getCollaboration().getAssociationRoles()) {
      for (AssociationEndRole associationEndRole : associationRole.getAssociationEndRoles()) {
        if (associationEndRole.getType().equals(this))
          associationEndRoles.add(associationEndRole);
      }
    }
    return associationEndRoles;
  }

  /**
   * @return classifier role's opposite association end roles
   */
  public Set<AssociationEndRole> getOppositeAssociationEndRoles() {
    Set<AssociationEndRole> oppositeAssociationEndRoles = new HashSet<AssociationEndRole>();
    for (AssociationRole associationRole : this.getCollaboration().getAssociationRoles()) {
      for (AssociationEndRole associationEndRole : associationRole.getAssociationEndRoles()) {
        if (associationEndRole.getType().equals(this))
          oppositeAssociationEndRoles.add(associationRole.getOppositeAssociationEndRole(associationEndRole));
      }
    }
    return oppositeAssociationEndRoles;
  }

  /**
   * @see java.lang.Object#toString()
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("ClassifierRole [name=");
    result.append(this.getName());
    result.append(", base = ");
    result.append(this.getBase() == null ? "null" : this.getBase().getName());
    result.append(", collaboration = ");
    result.append(this.getCollaboration() == null ? "null" : this.getCollaboration().getName());
    result.append("]");
    return result.toString();
  }
}
