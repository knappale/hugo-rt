package uml.parser.xmi.metamodel;


/**
 * UML/OCL constraint
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Constraint extends Element {
  private String body;

  public Constraint(String name, String body) {
    super(name);
    this.body = body;
  }

  /**
   * @return constraint's formula
   */
  public String getBody() {
    return this.body;
  }

  /**
   * @return constraint's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Constraint [name =");
    result.append(this.getName());
    result.append(", body =");
    result.append(this.getBody());
    result.append("]");
    return result.toString();
  }
}
