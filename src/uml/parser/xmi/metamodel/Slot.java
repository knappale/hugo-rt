package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * Contains data of a slot.
 *
 * @author Alexander Knapp (<A HREF="mailto:knapp@informatik.uni-muenchen.de">knapp@informatik.uni-muenchen.de</A>)
 */
public class Slot extends Element {
  private Attribute attribute = null;
  private List<String> values = new ArrayList<String>();

  /**
   * Create a slot for an attribute.
   *
   * @param attribute underlying attribute
   * @return a slot
   */
  public Slot(Attribute attribute) {
    super("");
    this.attribute = attribute;
  }

  /**
   * Create a slot for an attribute with a value
   *
   * @param attribute underlying attribute
   * @return a slot
   */
  public Slot(Attribute attribute, String value) {
    this(attribute);
    this.values.add(value);
  }

  /**
   * @return slot's underlying attribute
   */
  public Attribute getAttribute() {
    return attribute;
  }

  /**
   * Add a value to this slot.
   *
   * @param value a value expression
   */
  public void addValue(String value) {
    this.values.add(value);
  }

  /**
   * @return slot's values
   */
  public List<String> getValues() {
    return this.values;
  }

  /**
   * @return slots (artificial) name, i.e. the name of the
   * corresponding attribute
   */
  public String getName() {
    return getAttribute().getName();
  }

  /**
   * @return slot's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Slot [attribute = ");
    result.append(getAttribute().getName());
    result.append(", values = ");
    result.append(getValues());
    result.append("]");
    return result.toString();
  }
}
