package uml.parser.xmi.metamodel;

import java.util.*;


public class Link extends Element {
  private Model model;
  private Association association;
  private List<LinkEnd> linkEnds = new ArrayList<LinkEnd>();
  private Set<Stimulus> stimuli = new HashSet<Stimulus>();

  public Link(String name, Association association) {
    super(name);
    this.association = association;
  }

  /**
   * @return link's association
   */
  public Association getAssociation() {
    return this.association;
  }

  /**
   * Set link's association.
   *
   * @param association link's new association
   */
  public void setAssociation(Association association) {
    this.association = association;
  }

  /**
   * Set links's model.
   */
  public void setModel(Model model) {
    if (this.getModel() == null || !this.getModel().equals(model)) {
      this.model = model;
      if (!model.getLinks().contains(this))
        model.addLink(this);
    }    
  }

  /**
   * @return link's model
   */
  public Model getModel() {
    return this.model;
  }

  /**
   * Create a new link end, belonging to this link.
   * 
   * @param name new link end's name
   * @param object new link end's object
   * @return a new link end instance, belonging to this link
   */
  public LinkEnd createLinkEnd(String name, Object object) {
    LinkEnd linkEnd = new LinkEnd(name, object, this);
    this.addLinkEnd(linkEnd);
    return linkEnd;
  }

  /**
   * Add a link end to this link.
   *
   * @param linkEnd a link end
   */
  public void addLinkEnd(LinkEnd linkEnd) {
    if (!this.getLinkEnds().contains(linkEnd)) {
      this.linkEnds.add(linkEnd);
      linkEnd.setLink(this);
    }
  }

  /**
   * @return link's link ends
   */
  public List<LinkEnd> getLinkEnds() {
    return this.linkEnds;
  }

  /**
   * Determine opposite link end of a link end w.r.t. to this link.
   *
   * @param oppositeLinkEnd a link end of this link
   * @return opposite link end
   */
  public LinkEnd getOppositeLinkEnd(LinkEnd oppositeLinkEnd) {
    for (LinkEnd linkEnd : this.getLinkEnds())
      if (!linkEnd.equals(oppositeLinkEnd))
        return linkEnd;
    return null;
  }

  /**
   * Create a new stimulus, belonging to this link.
   * 
   * @param name new stimulus' name
   * @param sender new stimulus' sender
   * @param receiver new stimulus' receiver
   * @return a new stimulus, belonging to this link
   */
  public Stimulus createStimulus(String name, Object sender, Object receiver) {
    Stimulus stimulus = new Stimulus(name, sender, receiver, this);
    this.addStimulus(stimulus);
    return stimulus;
  }

  /**
   * Add a stimulus to this link.
   *
   * @param stimulus a stimulus
   */
  public void addStimulus(Stimulus linkEnd) {
    if (!this.getStimuli().contains(linkEnd)) {
      this.stimuli.add(linkEnd);
    }
  }

  /**
   * @return link's stimuli
   */
  public Set<Stimulus> getStimuli() {
    return this.stimuli;
  }

  /**
   * @return links's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Link [name = ");
    result.append(this.getName());
    result.append(", association = ");
    if (this.getAssociation() != null)
      result.append(this.getAssociation().getName());
    result.append(", #linkEnds = ");
    result.append(getLinkEnds().size());
    result.append("]");
    return result.toString();
  }
}
