package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * Contains data of a (UML) interaction.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Interaction extends Element {
  private Collaboration collaboration = null;
  private Set<Message> messages = new HashSet<Message>();
  private Set<String> assertions = new HashSet<String>();
  private Set<String> timings = new HashSet<String>();

  /**
   * Create a new interaction for a collaboration.
   *
   * @param collaboration containing collaboration
   * @return a new interaction
   * @pre name != null
   */
  Interaction(String name, Collaboration collaboration) {
    super(name);
    this.collaboration = collaboration;
  }

  /**
   * Set interaction's collaboration.
   *
   * @param collaboration interaction's new collaboration
   */
  void setCollaboration(Collaboration collaboration) {
    this.collaboration = collaboration;
  }

  /**
   * @return interaction's containing collaboration
   */
  public Collaboration getCollaboration() {
    return this.collaboration;
  }

  /**
   * Create a message, belonging to this interaction.
   *
   * @param name message's name
   * @param sender message's sending classifier role
   * @param action message's action
   * @param receiver message's receiving classifier role
   * @return a new message
   */
  public Message createMessage(String name, ClassifierRole sender, Action action, ClassifierRole receiver) {
    Message message = new Message(name, sender, action, receiver);
    this.addMessage(message);
    return message;
  }

  public void addMessage(Message message) {
    if (message != null && !this.messages.contains(message))
      this.messages.add(message);
  }

  /**
   * @return interaction's messages
   */
  public Set<Message> getMessages() {
    return this.messages;
  }

  /**
   * @return interaction's assertions
   */
  public Set<String> getAssertions() {
    return this.assertions;
  }

  /**
   * @return interaction's timings
   */
  public Set<String> getTimings() {
    return this.timings;
  }

  /**
   * @return interaction's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Interaction [name = ");
    result.append(this.getName());
    result.append(", messages = ");
    result.append(getMessages());
    result.append(", timings = ");
    result.append(getTimings());
    result.append("]");
    return result.toString();
  }
}
