package uml.parser.xmi.metamodel;

import java.util.*;

import org.eclipse.jdt.annotation.NonNull;


/**
 * Contains data of a (UML) message.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Message extends Element {
  private ClassifierRole sender;
  private ClassifierRole receiver;
  private Action action;
  /** @inv predecessors != null */
  private @NonNull Set<Message> predecessors = new HashSet<Message>();

  /**
   * Create a new message.
   *
   * @param name optional name
   * @param sender message's sending classifier role
   * @param action message's action
   * @param receiver message's receiving classifier role
   */
  Message(String name, ClassifierRole sender, Action action, ClassifierRole receiver) {
    super(name == null ? "" : name);
    this.sender = sender;
    this.action = action;
    this.receiver = receiver;
  }

  /**
   * @return message's sender
   */
  public ClassifierRole getSender() {
    return this.sender;
  }
  
  /**
   * @return message's action
   */
  public Action getAction() {
    return this.action;
  }

  /**
   * @return message's receiver
   */
  public ClassifierRole getReceiver() {
    return this.receiver;
  }

  /**
   * Add a predecessor message to this message.
   *
   * @param predecessor preceding message
   */
  public void addPredecessor(Message predecessor) {
    if (predecessor != null)
      this.predecessors.add(predecessor);
  }

  /**
   * @return message's predecessors
   */
  public Set<Message> getPredecessors() {
    return this.predecessors;
  }

  /**
   * @return message's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Message [name = ");
    result.append(this.getName());
    result.append(", sender = ");
    result.append(this.getSender() == null ? "null" : this.getSender().getName());
    result.append(", action = ");
    result.append(this.getAction() == null ? "null" : this.getAction().getScript());
    result.append(", receiver = ");
    result.append(this.getReceiver() == null ? "null" : this.getReceiver().getName());
    result.append(", #predecessors = ");
    result.append(this.getPredecessors().size());
    result.append("]");
    return result.toString();
  }
}
