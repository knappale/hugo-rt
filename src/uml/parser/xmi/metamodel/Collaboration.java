package uml.parser.xmi.metamodel;

import java.util.*;

import org.eclipse.jdt.annotation.NonNull;


/**
 * UML collaboration.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Collaboration extends Element {
  private Model model = null;
  private @NonNull Set<@NonNull ClassifierRole> classifierRoles = new HashSet<>();
  private @NonNull Set<@NonNull AssociationRole> associationRoles = new HashSet<>();
  private @NonNull Set<@NonNull Object> objects = new HashSet<>();
  private @NonNull Set<@NonNull Link> links = new HashSet<>();
  private @NonNull Set<@NonNull Interaction> interactions = new HashSet<>();

  /**
   * Create a new named collaboration in a model.
   *
   * @param name collaboration's name
   * @param model underlying UML model
   * @return a model
   */
  public Collaboration(String name, Model model) {
    super(name);
    this.model = model;
  }

  /**
   * Set collaboration's model.
   *
   * @param model collaboration's new model
   */
  void setModel(Model model) {
    this.model = model;
  }

  /**
   * Determine collaboration's underlying (UML) model.
   *
   * @return collaboration's underlying model
   */
  public Model getModel() {
    return this.model;
  }

  /**
   * Create a classifier role, belonging to this collaboration.
   *
   * @param name classifier role's name
   * @param base classifier role's base classifier
   * @return a new classifier role
   */
  public ClassifierRole createClassifierRole(String name, Classifier base) {
    ClassifierRole classifierRole = new ClassifierRole(name, base, this);
    this.addClassifierRole(classifierRole);
    return classifierRole;
  }

  /**
   * Add a classifier role to the collaboration.
   *
   * @param object an object
   */
  public void addClassifierRole(ClassifierRole classifierRole) {
    if (classifierRole != null && !this.getClassifierRoles().contains(classifierRole)) {
      this.classifierRoles.add(classifierRole);
      classifierRole.setCollaboration(this);
    }
  }

  /**
   * Determine collaboration's classifier roles.
   *
   * @return collaboration's classifier roles
   */
  public @NonNull Set<@NonNull ClassifierRole> getClassifierRoles() {
    return this.classifierRoles;
  }

  /**
   * Create an association role, belonging to this collaboration.
   *
   * @param name association role's name
   * @param base association role's base association
   * @return a new association role
   */
  public AssociationRole createAssociationRole(String name, Association base) {
    AssociationRole associationRole = new AssociationRole(name, base, this);
    this.addAssociationRole(associationRole);
    return associationRole;
  }

  /**
   * Add an association role to the collaboration.
   *
   * @param associationRole an association role
   */
  public void addAssociationRole(AssociationRole associationRole) {
    if (associationRole != null && !this.associationRoles.contains(associationRole)) {
      this.associationRoles.add(associationRole);
      associationRole.setCollaboration(this);
    }
  }

  /**
   * Determine collaboration's association roles.
   *
   * @return collaboration's association roles
   */
  public @NonNull Set<@NonNull AssociationRole> getAssociationRoles() {
    return this.associationRoles ;
  }

  /**
   * Determine collaboration's objects.
   *
   * @return collaboration's objects
   */
  public @NonNull Set<@NonNull Object> getObjects() {
    return this.objects;
  }

  /**
   * Determine collaboration's links.
   *
   * @return collaboration's links
   */
  public @NonNull Set<@NonNull Link> getLinks() {
    return this.links;
  }

  /**
   * Create an interaction, belonging to this collaboration.
   *
   * @param name interaction's name
   * @return a new interaction
   */
  public @NonNull Interaction createInteraction(String name) {
    Interaction interaction = new Interaction(name, this);
    this.addInteraction(interaction);
    return interaction;
  }

  /**
   * Add an interaction to the collaboration.
   *
   * @param interaction an interaction
   */
  public void addInteraction(Interaction interaction) {
    if (interaction != null && !this.interactions.contains(interaction)) {
      this.interactions.add(interaction);
      interaction.setCollaboration(this);
    }
  }

  /**
   * Determine collaboration's interactions.
   *
   * @return collaboration's interactions
   */
  public @NonNull Set<@NonNull Interaction> getInteractions() {
    return this.interactions;
  }

  /**
   * @return collaboration's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Collaboration [name = ");
    result.append(this.getName());
    result.append(", #classifier roles = ");
    result.append(this.getClassifierRoles().size());
    result.append(", #association roles = ");
    result.append(this.getAssociationRoles().size());
    result.append(", #interactions = ");
    result.append(this.getInteractions().size());
    result.append("]");
    return result.toString();
  }
}
