package uml.parser.xmi.metamodel;

import java.util.*;


/**
 * UML association.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class Association extends Element {
  private Model model;
  private List<AssociationEnd> associationEnds = new ArrayList<AssociationEnd>();

  Association(String name, Model model) {
    super(name);
    this.model = model;
  }

  /**
   * Set association's model.
   *
   * @param model association's new model
   */
  void setModel(Model model) {
    this.model = model;
  }

  /**
   * @return association's model
   */
  public Model getModel() {
    return this.model;
  }

  /**
   * Create a new association end instance, belonging to this association.
   * 
   * @param name new association end's name
   * @param type new association end's classifier type
   * @return a new association end instance
   */
  public AssociationEnd createAssociationEnd(String name, Classifier type) {
    AssociationEnd associationEnd = new AssociationEnd(name, type, this);
    this.addAssociationEnd(associationEnd);
    return associationEnd;
  }

  /**
   * Add an association end to this association.
   *
   * @param associationEnd association end to be added
   */
  public void addAssociationEnd(AssociationEnd associationEnd) {
    if (associationEnd != null && !this.getAssociationEnds().contains(associationEnd)) {
      if (associationEnd.getAssociation() != null)
        associationEnd.getAssociation().removeAssociationEnd(associationEnd);
      this.associationEnds.add(associationEnd);
      associationEnd.setAssociation(this);
    }
  }

  /**
   * Remove an association end from this association.
   *
   * @param associationEnd association end to be removed
   */
  public void removeAssociationEnd(AssociationEnd associationEnd) {
    if (associationEnd != null && this.getAssociationEnds().contains(associationEnd)) {
      this.associationEnds.remove(associationEnd);
      associationEnd.setAssociation(null);
    }
  }

  /**
   * @return association's association ends
   */
  public List<AssociationEnd> getAssociationEnds() {
    return this.associationEnds;
  }

  /**
   * Determine opposite association end of an association end w.r.t. to this association.
   *
   * @param oppositeAssociationEnd an association end of this association
   * @return opposite association end
   */
  public AssociationEnd getOppositeAssociationEnd(AssociationEnd oppositeAssociationEnd) {
    for (AssociationEnd associationEnd : this.getAssociationEnds())
      if (!associationEnd.equals(oppositeAssociationEnd))
        return associationEnd;
    return null;
  }

  /**
   * @return association's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Association [name = ");
    result.append(getName());
    result.append(", #associationEnds = ");
    result.append(getAssociationEnds().size());
    result.append("]");
    return result.toString();
  }
}
