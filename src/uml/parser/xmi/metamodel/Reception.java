package uml.parser.xmi.metamodel;


/**
 * Contains data of a reception.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class Reception extends Behavioural {
  private Signal signal = null;

  /**
   * Create a reception.
   *
   * @param name reception's name
   * @param signal reception's signal
   * @param owner reception's owning classifier
   */
  Reception(String name, Signal signal, Classifier owner) {
    super(name, owner);
    this.signal = signal;
  }

  /**
   * @return reception's signal
   */
  public Signal getSignal() {
    return signal;
  }

  /**
   * @return reception's string representation
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("Reception [name = ");
    result.append(getName());
    result.append(", owner = ");
    result.append(getOwner() == null ? "null" : getOwner().getName());
    result.append(", signal = ");
    result.append(getSignal() == null ? "null" : getSignal().getName());
    result.append(", parameters = (");
    for (int i = 0; i < getParameters().size(); i++) {
      Parameter parameter = getParameters().get(i);
      result.append(parameter.getName());
      result.append(" : ");
      result.append(parameter.getType().getName());
      if (i < getParameters().size()-1)
	result.append(", ");
    }
    result.append(")");
    result.append("]");
    return result.toString();
  }
}
