package uml.parser.xmi;

import java.io.*;

import javax.xml.parsers.*;

import org.eclipse.jdt.annotation.NonNull;
import org.w3c.dom.*;

import uml.UModelException;


/**
 * Reads data from a model input stream (XMI 1.0, XMI 1.1, XMI 1.2).
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class ModelReader extends uml.parser.ModelReader {
  uml.parser.xmi.metamodel.Model xmiModel = null;

  public ModelReader(BufferedInputStream modelInputStream, String xmiVersion) throws UModelException {
    DEBUG.info("Using UML 1.x/XMI 1.x reader");

    if ("1.0".equals(xmiVersion) ||
        "1.1".equals(xmiVersion) ||
        "1.2".equals(xmiVersion)) {
      Document document = null;
      try {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        documentBuilder.setErrorHandler(new org.xml.sax.ErrorHandler() {
          public void warning(org.xml.sax.SAXParseException spe) throws org.xml.sax.SAXException {
            util.Message.info("XML SAX parser warning: " + spe.getMessage());
          }

          public void error(org.xml.sax.SAXParseException spe) throws org.xml.sax.SAXException {
            util.Message.info("XML SAX parser error: " + spe.getMessage());
          }

          public void fatalError(org.xml.sax.SAXParseException spe) throws org.xml.sax.SAXException {
            throw spe;
          }
        });
        document = documentBuilder.parse(modelInputStream);
      }
      catch (Exception e) {
        throw new uml.UModelException("XML DOM parsing failed: " + e.getMessage());
      }
      if (document == null)
        throw new uml.UModelException("XML DOM parsing failed");

      if ("1.0".equals(xmiVersion)) {
        DEBUG.info("Using UML 1.x/XMI 1.0 reader");
        XMI10Parser xmiParser = new XMI10Parser();
        this.xmiModel = xmiParser.parse(document);
      }
      else {
        if ("1.1".equals(xmiVersion)) {
          DEBUG.info("Using UML 1.x/XMI 1.1 reader");
          XMI11Parser xmiParser = new XMI11Parser();
          this.xmiModel = xmiParser.parse(document);
        }
        else {
          if ("1.2".equals(xmiVersion)) {
            DEBUG.info("Using UML 1.x/XMI 1.2 reader");
            XMI12Parser xmiParser = new XMI12Parser();
            this.xmiModel = xmiParser.parse(document);
          }
        }
      }
      if (xmiModel == null)
        throw new uml.UModelException("XMI " + xmiVersion + " parsing failed");

      return;
    }

    throw new UModelException("Unable to determine input file type");
  }

  public String getModelName() {
    return this.xmiModel.getName();
  }

  public uml.@NonNull UModel getRawModel() throws UModelException {
    uml.parser.xmi.metamodel.Model normalisedXMIModel = XMINormaliser.normalise(this.xmiModel);
    if (normalisedXMIModel == null)
      throw new UModelException("Normalisation of XMI model representation failed");
    return Translator.translate(normalisedXMIModel);
  }
}
