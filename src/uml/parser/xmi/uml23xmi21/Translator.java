package uml.parser.xmi.uml23xmi21;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.ConnectionPointReference;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConsiderIgnoreFragment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Duration;
import org.eclipse.uml2.uml.DurationConstraint;
import org.eclipse.uml2.uml.DurationInterval;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.FinalState;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionConstraint;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.InteractionOperatorKind;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OccurrenceSpecification;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Reception;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.SignalEvent;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.TimeEvent;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.Vertex;

import uml.UAction;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UClass;
import uml.UClassifier;
import uml.UCollaboration;
import uml.UContext;
import uml.UDataType;
import uml.UExpression;
import uml.UMethod;
import uml.UModel;
import uml.UModelException;
import uml.UObject;
import uml.UOperation;
import uml.UParameter;
import uml.UReception;
import uml.USignal;
import uml.USlot;
import uml.UType;
import uml.interaction.UAlternativeFragment;
import uml.interaction.UBasicFragment;
import uml.interaction.UCombinedFragment;
import uml.interaction.UConsiderFragment;
import uml.interaction.UIgnoreFragment;
import uml.interaction.UInteraction;
import uml.interaction.UInteractionOperand;
import uml.interaction.ULifeline;
import uml.interaction.ULoopFragment;
import uml.interaction.UMessage;
import uml.interaction.UMessageOccurrenceSpecification;
import uml.interaction.UOccurrenceSpecification;
import uml.interaction.UOptionalFragment;
import uml.interaction.UParallelFragment;
import uml.interaction.USequentialFragment;
import uml.interaction.UStrictFragment;
import uml.interaction.UTiming;
import uml.ocl.OConstraint;
import uml.parser.ute.UMLParser;
import uml.statemachine.UEvent;
import uml.statemachine.UPseudoState;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UTransition;
import uml.statemachine.UVertex;
import util.Formatter;
import util.Identifier;
import util.Pair;
import util.UnlimitedNatural;

import static util.Formatter.quoted;
import static util.Objects.requireNonNull;
import static util.Objects.toType;
import static java.util.stream.Collectors.toList;


public class Translator {
  private @NonNull MModel mModel;
  private @NonNull UModel model;
  private Map<Classifier, UClassifier> classifiers = new HashMap<>();
  private Map<Element, UAttribute> attributes = new HashMap<>();
  private Map<Signal, USignal> signals = new HashMap<>();
  private Map<Operation, UOperation> operations = new HashMap<>();
  private Map<Reception, UReception> receptions = new HashMap<>();
  private Map<Property, UObject> objects = new HashMap<>();
  private Map<Message, UMessage> messages = new HashMap<>();
  private Map<OccurrenceSpecification, UOccurrenceSpecification> occurrenceSpecifications = new HashMap<>();

  private static java.util.logging.Logger DEBUG = util.Message.debug();

  @NonNullByDefault
  private Translator(MModel mModel) {
    this.mModel = mModel;
    this.model = new UModel(canonise(mModel.getName()));
  }

  @NonNullByDefault
  public static UModel translate(MModel mModel) throws UModelException {
    Translator translator = new Translator(mModel);
    translator.initialiseModel();
    return translator.model;
  }

  private void initialiseModel() throws UModelException {
    // Fill-in classes ...
    for (Class mClass : this.mModel.getMClasses()) {
      UClass uClass = new UClass(canonise(mClass.getName()), model);
      String mClassName = mClass.getName().trim().toLowerCase();

      if (mClassName.equals("int") || mClassName.equals("integer")) {
        classifiers.put(mClass, model.getInteger());
        util.Message.info("Treating class " + quoted(mClass.getName()) + " as data type `int'");
        continue;
      }
      else
        if (mClassName.equals("bool") || mClassName.equals("boolean")) {
          classifiers.put(mClass, model.getBoolean());
          util.Message.info("Treating class " + quoted(mClass.getName()) + " as data type `boolean'");
          continue;
        }
        else
          if (mClassName.equals("string")) {
            classifiers.put(mClass, model.getString());
            util.Message.info("Treating class " + quoted(mClass.getName()) + " as data type `string'");
            continue;
          }
          else
            if (mClassName.equals("void")) {
              classifiers.put(mClass, model.getVoid());
              util.Message.info("Treating class " + quoted(mClass.getName()) + " as data type `void'");
              continue;
            }
            else
              if (mClassName.equals("clock")) {
                classifiers.put(mClass, model.getClock());
                util.Message.info("Treating class " + quoted(mClass.getName()) + " as data type `clock'");
                continue;
              }

      classifiers.put(mClass, uClass);
      model.addClass(uClass);
      DEBUG.info("Added class " + quoted(uClass.getName()));
    }

    // ... data types ...
    for (DataType mDataType : this.mModel.getMDataTypes()) {
      // Register data type into classifiers
      UDataType dataType = getDataType(mDataType);
      if (dataType == null) {
        util.Message.warning("Unsupported data type " + quoted(mDataType.getName()));

        // TODO (AK180124) Test
        dataType = new UDataType(canonise(mDataType.getName()), model) {
          public @NonNull UExpression getInitialValue() {
            return UExpression.nullConst();
          }
        };
        classifiers.put(mDataType, dataType);
        model.addDataType(dataType);
        // util.Message.info("Treating data type as class " + quoted(uClass.getName()));
      }
    }

    // ... signals ...
    for (Signal mSignal : this.mModel.getMSignals()) {
      USignal signal = new USignal(canonise(mSignal.getName()), model);
      signals.put(mSignal, signal);
      model.addSignal(signal);
      DEBUG.info("Added signal " + quoted(signal.getName()));
    }

    // Initialise classes ...
    var classEntries = new HashSet<@NonNull Pair<@NonNull Class, @NonNull UClass>>();
    for (Class mClass : this.mModel.getMClasses()) {
      if (mClass == null)
        continue;
      UClassifier classifier = getClassifier(mClass);

      // If classifier does not denote a datatype (int, boolean, &c.)
      if (!(classifier instanceof UClass))
        continue;

      UClass uClass = (UClass)classifier;
      classEntries.add(new Pair<>(mClass, uClass));
      setAttributes(mClass, uClass);
      setOperations(mClass, uClass);
      setReceptions(mClass, uClass);
    }
    for (var classEntry : classEntries)
      setStateMachine(classEntry.getFirst(), classEntry.getSecond());

    for (Collaboration mCollaboration : this.mModel.getMCollaborations()) {
      UCollaboration collaboration = new UCollaboration(canonise(mCollaboration.getName()), model);

      setObjects(mCollaboration, collaboration);
      setSlots(mCollaboration, collaboration);
      setConstraints(mCollaboration, collaboration);
      setInteractions(mCollaboration, collaboration);
      model.addCollaboration(collaboration);
      DEBUG.info("Added collaboration " + quoted(collaboration));
    }
  }

  private UClassifier getClassifier(Classifier mClassifier) {
    if (mClassifier instanceof PrimitiveType) {
      // Possibly register primitive type in classifiers
      // This is necessary as primitive types are not "owned" by a model.
      getDataType((PrimitiveType)mClassifier);
    }
    return classifiers.get(mClassifier);
  }

  private UDataType getDataType(DataType mDataType) {
    if (classifiers.get(mDataType) != null)
      return (UDataType)(classifiers.get(mDataType));

    String mDataTypeName = null;
    UDataType result = null;
    
    if ((mDataType instanceof PrimitiveType) && mDataType.eIsProxy()) {
      mDataTypeName = ((InternalEObject)mDataType).eProxyURI().toString();
      mDataTypeName = mDataTypeName.substring(mDataTypeName.indexOf("#")+1).trim().toLowerCase();
    }
    else
      mDataTypeName = mDataType.getName().trim().toLowerCase();

    if (mDataTypeName.equals("int") || mDataTypeName.equals("integer"))
      result = model.getInteger();
    if (mDataTypeName.equals("bool") || mDataTypeName.equals("boolean"))
      result = model.getBoolean();
    if (mDataTypeName.equals("string"))
      result = model.getString();
    if (mDataTypeName.equals("void"))
      result = model.getVoid();
    if (mDataTypeName.equals("clock"))
      result = model.getClock();

    if (result != null) {
      classifiers.put(mDataType, result);
      DEBUG.info("Added data type " + quoted(result.getName()));
    }
    return result;
  }

  private UAttribute getAttribute(Property mProperty) {
    return attributes.get(mProperty);
  }

  private UOperation getOperation(Operation mOperation) {
    var operation = operations.get(mOperation);
    if (operation == null)
      util.Message.warning("No operation found for " + quoted(mOperation.getName()));
    return operation;
  }

  private UReception getReception(Reception mReception) {
    return receptions.get(mReception);
  }

  private USignal getSignal(Signal mSignal) {
    return signals.get(mSignal);
  }

  private @NonNull UObject getObject(ConnectableElement mConnectableElement, UCollaboration collaboration) throws UModelException {
    if (!(mConnectableElement instanceof Property))
      throw new UModelException("Connectable element " + quoted(mConnectableElement.getName()) + " in collaboration " + quoted(collaboration.getName()) + " does not refer to a property");
    Property mProperty = (Property)mConnectableElement;
    UObject object = objects.get(mProperty);
    if (object == null) {
      Type mPropertyType = mProperty.getType();
      if (   !(mPropertyType instanceof Classifier)
          || getClassifier((Classifier)mPropertyType) == null
          || !(getClassifier((Classifier)mPropertyType) instanceof UClass))
        throw new UModelException("Type " + quoted(mPropertyType) + " of attribute " + quoted(mProperty.getName()) + " in collaboration " + quoted(collaboration.getName() )+  " cannot be resolved to a known class");
      UClass uPropertyType = (UClass)getClassifier((Classifier)mPropertyType);
      assert (uPropertyType != null);
      object = new UObject(canonise(mProperty.getName()), uPropertyType);
      objects.put(mProperty, object);

      if (mProperty.getDefaultValue() != null) {
        DEBUG.info("Instance specification " + mProperty.getDefaultValue());
        if (mProperty.getDefaultValue() instanceof InstanceValue) {
          InstanceSpecification mInstanceSpecification = ((InstanceValue)mProperty.getDefaultValue()).getInstance();
          for (Slot mSlot : mInstanceSpecification.getSlots()) {
            if (mSlot.getDefiningFeature() != null && mSlot.getDefiningFeature() instanceof Property) {
              UAttribute attribute = getAttribute((Property)mSlot.getDefiningFeature());
              if (attribute == null)
                continue;
              if (mSlot.getValues() != null && mSlot.getValues().size() > 0) {
                UExpression value = UMLParser.expression(getValueSpecificationBody(Objects.requireNonNull(mSlot.getValues().get(0)), " "));
                if (value == null)
                  continue;
                object.addSlot(new USlot(attribute, value));
              }
            }
          }
        }
      }
    }
    return object;
  }

  private void setAttributes(@NonNull Class mClass, @NonNull UClass uClass) throws UModelException {
    Set<String> knownNames = new HashSet<String>();

    for (Property mProperty : mClass.getOwnedAttributes()) {
      String mPropertyName = mProperty.getName();
      if (mPropertyName == null || mPropertyName.trim().equals(""))
        mPropertyName = mProperty.getType().getName();
      mPropertyName = canonise(mPropertyName);
      if (knownNames.contains(mPropertyName))
        throw new UModelException("Double declaration of attribute " + quoted(mPropertyName) + " in class " + quoted(mClass.getName()));

      Type mPropertyType = mProperty.getType();
      if (   !(mPropertyType instanceof Classifier)
          || getClassifier((Classifier)mPropertyType) == null)
        throw new UModelException("Type " + quoted(mPropertyType) + " of property " + quoted(mPropertyName) + " in class " + quoted(mClass.getName() )+  " cannot be resolved to a known classifier");
      UClassifier uPropertyType = getClassifier((Classifier)mPropertyType);
      assert (uPropertyType != null);
      UAttribute attribute = new UAttribute(mPropertyName, uPropertyType, uClass);
      attributes.put(mProperty, attribute);
      uClass.addAttribute(attribute);

      if (mProperty.getUpper() > 1)
        attribute.setUpper(UExpression.intConst(mProperty.getUpper()));
      if (mProperty.getUpper() < 0)
        util.Message.warning("Ignoring multiplicity of attribute " + quoted(attribute.getName()) + " in class " + quoted(uClass.getName()));

      if (mProperty.getDefault() != null && !mProperty.getDefault().trim().equals("")) {
        if (UType.simple(model.getClock()).equals(attribute.getType()))
          util.Message.warning("Ignoring clock initialisation of attribute " + quoted(attribute.getName()) + " in class " + quoted(uClass.getName()));
        UExpression uExpression = requireNonNull(UMLParser.expression(mProperty.getDefault()));
        attribute.addInitialValue(uExpression);
      }

      if (mProperty.isReadOnly()) {
        attribute.setFinal(true);
        if (mProperty.getDefault() == null || mProperty.getDefault().trim().equals("")) {
          @NonNull List<@NonNull UExpression> addedDefaults = attribute.addDefaultInitialValues();
          util.Message.warning("Final (read-only) attribute " + quoted(attribute.getName()) + " in class " + quoted(uClass.getName()) + " shows no initial value, default value " + quoted(Formatter.setOrSingleton(addedDefaults)) + " added");
        }
      }

      if (mProperty.isStatic()) {
        attribute.setStatic(true);
      }

      DEBUG.info("Added property " + quoted(attribute) + " to class " + quoted(uClass.getName()));
    }

    for (Association mAssociation : this.mModel.getMAssociations()) {
      for (Property mProperty : mAssociation.getMemberEnds()) {
        if (!mProperty.getType().equals(mClass))
          continue;
        Property oppositeMProperty = mProperty.getOpposite();
        if (oppositeMProperty == null) {
          DEBUG.info("Property " + quoted(mProperty) + " in class " + quoted(mClass.getName()) + " has no opposite property");
          continue;
        }
        if (getAttribute(oppositeMProperty) != null) {
          DEBUG.info("Property " + quoted(oppositeMProperty) + " in class " + quoted(uClass.getName()) + " already known");
          continue;
        }

        String oppositeMPropertyName = oppositeMProperty.getName();
        if (oppositeMPropertyName == null || oppositeMPropertyName.trim().equals(""))
          oppositeMPropertyName = oppositeMProperty.getType().getName();
        oppositeMPropertyName = canonise(oppositeMPropertyName);
        if (knownNames.contains(oppositeMPropertyName))
          throw new UModelException("Double declaration of opposite association end (i.e. property) name " + quoted(oppositeMPropertyName) + " in class " + quoted(mClass.getName()));

        Type oppositeMPropertyType = oppositeMProperty.getType();
        if (   !(oppositeMPropertyType instanceof Classifier)
            || getClassifier((Classifier)oppositeMPropertyType) == null)
          throw new UModelException("Type " + quoted(oppositeMPropertyType) + " of property " + quoted(oppositeMPropertyName) + " in class " + quoted(mClass.getName() )+  " cannot be resolved to a known classifier");
        UClassifier oppositeUPropertyType = getClassifier((Classifier)oppositeMPropertyType);
        assert (oppositeUPropertyType != null);
        UAttribute attribute = new UAttribute(oppositeMPropertyName, oppositeUPropertyType, uClass);

        if (oppositeMProperty.getUpper() > 1)
          attribute.setUpper(UExpression.intConst(oppositeMProperty.getUpper()));
        if (oppositeMProperty.getUpper() < 0)
          util.Message.warning("Ignoring multiplicity of opposite association end " + quoted(attribute.getName()) + " in class " + quoted(uClass.getName()));

        if (mProperty.isReadOnly()) {
          attribute.setFinal(true);
          if (mProperty.getDefault() == null || mProperty.getDefault().trim().equals("")) {
            util.Message.warning("Final opposite association end (i.e. property) " + quoted(attribute.getName()) + " in class " + quoted(uClass.getName()) + " shows no initial value");
          }
        }

        if (mProperty.isStatic()) {
          attribute.setStatic(true);
        }

        attributes.put(oppositeMProperty, attribute);
        uClass.addAttribute(attribute);
        DEBUG.info("Added opposite association end (i.e. property) " + quoted(attribute) + " to class " + quoted(uClass.getName()));
      }
    }
  }

  @NonNullByDefault
  private void setOperations(Class mClass, UClass uClass) throws UModelException {
    for (Operation mOperation : mClass.getOwnedOperations()) {
      UOperation uOperation = new UOperation(canonise(mOperation.getName().substring(0, 1).toLowerCase() + mOperation.getName().substring(1)), uClass, mOperation.isStatic());
      this.setParameters(mClass, mOperation, uOperation);
      this.setMethods(mClass, mOperation, uOperation);
      this.operations.put(mOperation, uOperation);
      uClass.addOperation(uOperation);
      DEBUG.info("Added operation " + quoted(uOperation) + " to class " + quoted(uClass.getName()));
    }
  }

  @NonNullByDefault
  private void setReceptions(Class mClass, UClass uClass) throws UModelException {
    Set<USignal> knownSignals = new HashSet<>();

    for (Reception mReception : mClass.getOwnedReceptions()) {
      Signal mSignal = mReception.getSignal();
      String mName = mReception.getName();

      if (mSignal == null && (mName == null || mName.trim().equals("")))
        throw new UModelException("Reception with empty name and no signal in class " + quoted(mClass.getName()));

      // No signal (but name set)
      if (mSignal == null) {
        mSignal = this.mModel.getMSignals().stream().filter(mS -> mReception.getName().equals(mS.getName())).findFirst().orElse(null);
        if (mSignal == null)
          throw new UModelException("Reception " + quoted(mName) + " in class " + quoted(mClass.getName()) + " shows no signal and no signal could be determined by its name");

        mReception.setSignal(mSignal);
        DEBUG.info("Replacing empty signal of reception " + quoted(mName) + " in class " + quoted(mClass.getName()) + " to the equally named signal");
      }

      // No name (but signal set)
      if (mName == null || mName.trim().equals("")) {
        mName = mSignal.getName();

        mReception.setName(mName);
        DEBUG.info("Replacing empty name of reception in class " + quoted(mClass.getName()) + " by signal name " + quoted(mName));
      }

      USignal uSignal = getSignal(mSignal);
      if (uSignal == null)
        throw new UModelException("Reception " + quoted(mName) + " in class " + quoted(mClass.getName()) + " shows no known signal");
      if (knownSignals.contains(uSignal))
        throw new UModelException("Two receptions for signal " + quoted(uSignal.getName()) + " in class " + quoted(mClass.getName()));

      UReception uReception = new UReception(canonise(mName), uSignal, uClass);
      this.setParameters(mClass, mReception, uReception);
      this.setMethods(mClass, mReception, uReception);
      uClass.addReception(uReception);
      this.receptions.put(mReception, uReception);
      DEBUG.info("Added reception " + quoted(uReception) + " to class " + quoted(uClass.getName()));
    }
  }

  private void setParameters(Class mClass, BehavioralFeature mBehavioralFeature, UBehavioural uBehavioural) throws UModelException {
    for (Parameter mParameter : mBehavioralFeature.getOwnedParameters()) {
      if (   (mParameter.getDirection().getValue() != ParameterDirectionKind.RETURN)
          && (mParameter.getDirection().getValue() != ParameterDirectionKind.OUT)) {
        Type mParameterType = mParameter.getType();
        if (   !(mParameterType instanceof Classifier)
            || (getClassifier((Classifier)mParameterType) == null))
          throw new UModelException("Type " + quoted(mParameterType) + " of behavioural " + quoted(uBehavioural.getName()) + " in class " + quoted(mClass.getName()) +  " cannot be resolved to a known classifier");
        UClassifier uParameterType = getClassifier((Classifier)mParameterType);
        assert (uParameterType != null);
        uBehavioural.addParameter(new UParameter(canonise(mParameter.getName()), uParameterType, uBehavioural));
      }
      else
        util.Message.warning("Ignoring non-in parameter " + quoted(mParameter.getName()) + " of behavioural " + quoted(uBehavioural.getName()) + " in class " + quoted(mClass.getName()));
    }
  }

  private void setMethods(Class mClass, BehavioralFeature mBehavioralFeature, UBehavioural uBehavioural) {
    for (var mBehavior : mBehavioralFeature.getMethods()) {
      if (!(mBehavior instanceof OpaqueBehavior)) {
        util.Message.warning("Ignoring non-opaque behaviour " + quoted(mBehavior.getName()) + " of behavioural " + quoted(uBehavioural.getName()) + " in class " + quoted(mClass.getName()));
        continue;
      }
      var mOpaqueBehavior = (OpaqueBehavior)mBehavior;
      if (mOpaqueBehavior.getBodies() == null)
        continue;
      for (int i = 0; i < mOpaqueBehavior.getBodies().size(); i++) {
        var body = mOpaqueBehavior.getBodies().get(i);
        var language = mOpaqueBehavior.getLanguages().get(i);
        uBehavioural.addMethod(new UMethod(Arrays.asList(language), body));
      }
    }
  }

  @NonNullByDefault
  class StateMachineInfo {
    private UStateMachine uStateMachine;
    private Map<Vertex, UVertex> verticesTable;
    private Map<Region, UState> regionStatesTable;
    private Map<Region, URegion> regionsTable;
    private Map<Transition, UTransition> transitionsTable;
    private Map<Event, UEvent> eventsTable;
    private int regionCounter;

    StateMachineInfo(UStateMachine uStateMachine) {
      this.uStateMachine = uStateMachine;
      this.verticesTable = new HashMap<>();
      this.regionStatesTable = new HashMap<>();
      this.regionsTable = new HashMap<>();
      this.transitionsTable = new HashMap<>();
      this.eventsTable = new HashMap<>();
      this.regionCounter = 0;
    }

    String getClassName() {
      return this.uStateMachine.getC1ass().getName();
    }

    @Nullable UState getRegionState(Region mRegion) {
      return this.regionStatesTable.get(mRegion);
    }

    @Nullable URegion getRegion(Region mRegion) {
      return this.regionsTable.get(mRegion);
    }

    void putRegion(Region mRegion, URegion uRegion) {
      this.regionsTable.put(mRegion, uRegion);
    }

    void putRegionState(Region mRegion, UState state) {
      this.regionStatesTable.put(mRegion, state);
    }

    String freshRegionName() {
      return "region" + this.regionCounter++;
    }

    @Nullable UVertex getVertex(Vertex mVertex) {
      return this.verticesTable.get(mVertex);
    }

    Set<UVertex> getVertices() {
      return new HashSet<>(this.verticesTable.values());
    }
 
    void putVertex(Vertex mVertex, UVertex uVertex) {
      this.verticesTable.put(mVertex, uVertex);
    }

    @Nullable UTransition getTransition(Transition mTransition) {
      return this.transitionsTable.get(mTransition);
    }

    Set<UTransition> getTransitions() {
      return new HashSet<>(this.transitionsTable.values());
    }
 
    void putTransition(Transition mTransition, UTransition transition) {
      this.transitionsTable.put(mTransition, transition);
    }

    @Nullable UEvent getEvent(Event mEvent) {
      return this.eventsTable.get(mEvent);
    }

    @Nullable UReception getReception(USignal signal) {
      return this.uStateMachine.getC1ass().getReception(signal);
    }

    void putEvent(Event mEvent, UEvent event) {
      this.eventsTable.put(mEvent, event);
    }
  }

  @NonNullByDefault
  private void setStateMachine(Class mClass, UClass uClass) throws UModelException {
    if (mClass.getOwnedBehaviors() == null || mClass.getOwnedBehaviors().isEmpty()) {
      util.Message.info("Class " + quoted(mClass.getName()) + " shows no behaviour");
      return;
    }
    List<StateMachine> ownedMStateMachines = mClass.getOwnedBehaviors().stream().flatMap(toType(StateMachine.class)).collect(toList());
    if (ownedMStateMachines == null || ownedMStateMachines.isEmpty()) {
      util.Message.info("Class " + quoted(mClass.getName()) + " shows no state machine");
      return;
    }
    if (ownedMStateMachines.size() > 1)
      util.Message.warning("Class " + quoted(mClass.getName()) + " has more than one owned state machines, arbitrarily choosing the first");
    StateMachine mStateMachine = ownedMStateMachines.get(0);

    if (mStateMachine.getRegions() == null || mStateMachine.getRegions().isEmpty()) {
      util.Message.warning("State machine of " + quoted(mClass.getName()) + " shows no top-level regions");
      return;
    }

    UStateMachine uStateMachine = uClass.setStateMachine();
    StateMachineInfo stateMachineInfo = new StateMachineInfo(uStateMachine);

    for (Region mRegion : mStateMachine.getRegions()) {
      if (mRegion == null)
        continue;
      addRegion(uStateMachine, mRegion, stateMachineInfo);
      addTransitions(mRegion, stateMachineInfo);
    }

    /* TODO (AK180129) Correctly resolve connection point references
    if (mVertex instanceof ConnectionPointReference) {
      ConnectionPointReference mConnectionPointReference = (ConnectionPointReference)mVertex;
      if (mConnectionPointReference.getEntries() != null && mConnectionPointReference.getEntries().size() > 0 && mConnectionPointReference.getEntries().get(0) != null) {
        state = getState(Objects.requireNonNull(mConnectionPointReference.getEntries().get(0)), info);
        if (state != null)
          return state;
      }
      if (mConnectionPointReference.getExits() != null && mConnectionPointReference.getExits().size() > 0 && mConnectionPointReference.getExits().get(0) != null) {
        state = getState(Objects.requireNonNull(mConnectionPointReference.getExits().get(0)), info);
        if (state != null)
          return state;
      }
      throw new UModelException("No entry or exit point for connection point reference " + quoted(mConnectionPointReference.getName()) + " in state machine for " + quoted(info.getClassName()) + " found");
    }
    */
  }

  @NonNullByDefault
  private void addVertices(Region mRegion, StateMachineInfo info) throws UModelException {
    URegion uRegion = info.getRegion(mRegion);
    if (uRegion == null)
      return;

    for (Vertex mVertex : mRegion.getSubvertices()) {
      if (mVertex == null)
        continue;

      addVertex(uRegion, mVertex, info);
    }
  }

  @NonNullByDefault
  private void addVertex(URegion uRegion, Vertex mVertex, StateMachineInfo info) throws UModelException {
    String stateName = mVertex.getName();
    if (stateName == null || stateName.trim().equals("")) {
      util.Message.warning("Empty name for state vertex " + quoted(mVertex.toString()) + " in state machine for class " + quoted(info.getClassName()));
      stateName = canonise(mVertex.getClass().getName());
    }
    UVertex uVertex = null;
    if (mVertex instanceof FinalState)
      uVertex = uRegion.addFinalState(canonise(stateName));
    else {
      if (mVertex instanceof Pseudostate) {
        Pseudostate mPseudostate = (Pseudostate)mVertex;
        PseudostateKind mPseudostateKind = mPseudostate.getKind();
        switch (mPseudostateKind.getValue()) {
          case PseudostateKind.INITIAL:
            uVertex = uRegion.addPseudoState(stateName, UPseudoState.Kind.INITIAL);
            break;
          case PseudostateKind.SHALLOW_HISTORY:
            uVertex = uRegion.addPseudoState(stateName, UPseudoState.Kind.SHALLOWHISTORY);
            break;
          case PseudostateKind.DEEP_HISTORY:
            uVertex = uRegion.addPseudoState(stateName, UPseudoState.Kind.DEEPHISTORY);
            break;
          case PseudostateKind.JOIN:
            uVertex = uRegion.addPseudoState(stateName, UPseudoState.Kind.JOIN);
            break;
          case PseudostateKind.FORK:
            uVertex = uRegion.addPseudoState(stateName, UPseudoState.Kind.FORK);
            break;
          case PseudostateKind.JUNCTION:
            uVertex = uRegion.addPseudoState(stateName, UPseudoState.Kind.JUNCTION);
            break;
          case PseudostateKind.CHOICE:
            uVertex = uRegion.addPseudoState(stateName, UPseudoState.Kind.CHOICE);
            break;
          case PseudostateKind.ENTRY_POINT:
          case PseudostateKind.EXIT_POINT:
            util.Message.warning("Treating entry/exit point " + quoted(mVertex.getName()) + " as junction pseudo-state in state machine for class " + quoted(info.getClassName()));
            uVertex = uRegion.addPseudoState(stateName, UPseudoState.Kind.JUNCTION);
            break;
          default:
            throw new UModelException("Unknown pseudostate kind " + quoted(mPseudostateKind.getName()) + " in state machine for class " + quoted(info.getClassName()));
        }
      }
      else {
          uVertex = uRegion.addState(canonise(stateName));
      }
    }
    info.putVertex(mVertex, uVertex);

    if (!(uVertex instanceof UState))
      return;
    UState uState = (UState)uVertex;
      
    // Add entry and exit actions
    if (mVertex instanceof State) {
      State mState = (State)mVertex;
      Behavior mEntryBehavior = mState.getEntry();
      if (mEntryBehavior != null)
        uState.setEntryAction(parseAction(mEntryBehavior));
      Behavior mExitBehavior = mState.getExit();
      if (mExitBehavior != null)
        uState.setExitAction(parseAction(mExitBehavior));
    }

    // Add deferrable events
    if (mVertex instanceof State) {
      State mState = (State)mVertex;
      for (Trigger deferrableMTrigger : mState.getDeferrableTriggers()) {
        Event deferrableMEvent = deferrableMTrigger.getEvent();
        UEvent deferrableEvent = getEvent(uState, deferrableMEvent, info);
        if (deferrableEvent != null) {
          uState.addDeferrableEvent(deferrableEvent);
        }
      }
    }

    // Add regions for composite states
    if (mVertex instanceof State) {
      State mState = (State)mVertex;
      for (Region mRegion : mState.getRegions()) {
        if (mRegion == null)
          continue;

        addRegion(uState, mRegion, info);
      }
    }
  }

  @NonNullByDefault
  private void addRegion(UStateMachine uStateMachine, Region mRegion, StateMachineInfo info) throws UModelException {
    String regionName = mRegion.getName();
    if (regionName == null || regionName.trim().equals("")) {
      util.Message.warning("Empty name for region " + quoted(mRegion) + " in state machine for class " + quoted(info.getClassName()));
      regionName = mRegion.getClass().getName();
    }
    URegion uRegion = uStateMachine.addRegion(canonise(regionName));
    info.putRegion(mRegion, uRegion);

    addVertices(mRegion, info);

    // DEBUG.info("Added region " + quoted(uRegion) + " to state machine of class " + quoted(info.getClassName()));
  }

  @NonNullByDefault
  private void addRegion(UState uState, Region mRegion, StateMachineInfo info) throws UModelException {
    String uRegionName = mRegion.getName();
    if (uRegionName == null || uRegionName.trim().equals("")) {
      util.Message.warning("Empty name for region " + quoted(mRegion) + " in state machine for class " + quoted(info.getClassName()));
      uRegionName = uState.getName();
    }
    URegion uRegion = uState.addRegion(canonise(uRegionName));
    info.putRegion(mRegion, uRegion);

    addVertices(mRegion, info);

    // DEBUG.info("Added region " + quoted(uRegion) + " to state machine of class " + quoted(info.getClassName()));
  }

  @NonNullByDefault
  private void addTransitions(Region mRegion, StateMachineInfo info) throws UModelException {
    URegion uRegion = info.getRegion(mRegion);
    if (uRegion == null)
      return;

    for (Transition mTransition : mRegion.getTransitions())
      addTransition(uRegion, mTransition, info);

    for (Vertex mVertex : mRegion.getSubvertices()) {
      if (mVertex == null)
        continue;
      if (mVertex instanceof State) {
        State mState = (State)mVertex;
        for (Region subMRegion : mState.getRegions()) {
          if (subMRegion == null)
            continue;
          addTransitions(subMRegion, info);
        }
      }
    }
  }

  private void addTransition(URegion uRegion, Transition mTransition, StateMachineInfo info) throws UModelException {
    Vertex sourceMVertex = mTransition.getSource();
    Vertex targetMVertex = mTransition.getTarget();
    if (sourceMVertex == null || targetMVertex == null) {
      util.Message.warning("Ignoring a transition in state machine for class " + quoted(info.getClassName()) + " because source or target state are not specified.");
      return;
    }

    UVertex sourceUVertex = info.getVertex(sourceMVertex);
    UVertex targetUVertex = info.getVertex(targetMVertex);
    if (sourceUVertex == null || targetUVertex == null) {
      util.Message.warning("Ignoring a transition in state machine for class " + quoted(info.getClassName()) + " because source (" + quoted(sourceMVertex.getName()) + ") or target state (" + quoted(targetMVertex.getName()) + ") could not be determined.");
      return;
    }

    UEvent trigger = null;
    if (mTransition.getTriggers() != null && mTransition.getTriggers().size() > 0 && mTransition.getTriggers().get(0).getEvent() != null) {
      Event mEvent = mTransition.getTriggers().get(0).getEvent();
      if (mTransition.getTriggers().size() > 1)
        util.Message.warning("Ignoring all triggering events after " + quoted(mEvent.getName()) + " on transition " + quoted(mTransition.getName()) + " from " + quoted(sourceUVertex.getIdentifier().toString()) + " to " + quoted(targetUVertex.getIdentifier().toString()));

      if (sourceUVertex instanceof UState) {
        UState sourceUState = (UState)sourceUVertex;
        trigger = getEvent(sourceUState, mEvent, info);
        if (trigger == null) {
          util.Message.warning("Treating event " + quoted(mEvent.getName()) + " on transition " + quoted(mTransition.getName()) + " from " + quoted(sourceUVertex.getIdentifier().toString()) + " to " + quoted(targetUVertex.getIdentifier().toString()) + " as completion event in state machine for class " + quoted(info.getClassName()));
          trigger = UEvent.completion((UState)sourceUVertex);
        }
      }
      else {
        util.Message.warning("Ignoring event " + quoted(mEvent.getName()) + " on transition " + quoted(mTransition.getName()) + " from " + quoted(sourceUVertex.getIdentifier().toString()) + " to " + quoted(targetUVertex.getIdentifier().toString()) + " in state machine for class " + quoted(info.getClassName()));
        trigger = UEvent.pseudo();
      }
    }
    else {
      // Completion event
      if (sourceUVertex instanceof UState) {
        DEBUG.info("Treating transition " + quoted(mTransition.getName()) + " with triggers " + mTransition.getTriggers() + " as completion transition");
        trigger = UEvent.completion((UState)sourceUVertex);
      }
      else
        trigger = UEvent.pseudo();
    }

    UExpression guard = null;
    Constraint mGuard = mTransition.getGuard();
    if (mGuard != null && mGuard.getSpecification() != null) {
      String mGuardBody = getValueSpecificationBody(Objects.requireNonNull(mGuard.getSpecification()), " && ");
      if (mGuardBody == null || "".equals(mGuardBody)) {
        mGuardBody = "true";
        util.Message.warning("Uninterpretable guard on transition " + quoted(mTransition.getName()) + " from " + quoted(sourceUVertex.getName()) + " to " + quoted(targetUVertex.getName()) + ", taking it to be " + quoted(mGuardBody));
      }

      if (mGuardBody.equals("else")) {
        Vertex mState = mTransition.getSource();
        for (Transition outgoingMTransition : mState.getOutgoings()) {
          if (outgoingMTransition.equals(mTransition))
            continue;
          Constraint otherMGuard = outgoingMTransition.getGuard();
          if (otherMGuard == null)
            continue;

          UExpression otherGuard = parseConstraint(otherMGuard.getSpecification());
          if (guard == null)
            guard = UExpression.neg(otherGuard);
          else
            guard = UExpression.and(guard, UExpression.neg(otherGuard));
        }
        if (guard == null) {
          guard = UExpression.falseConst();
          util.Message.warning("Ignoring guard `else' on state " + quoted(mTransition.getSource().getName()) + " in state machine for class " + quoted(info.getClassName()));
        }
        else
          util.Message.info("Replacing guard `else' on state " + quoted(mTransition.getSource().getName()) + " in state machine for class " + quoted(info.getClassName()) + " by " + quoted(guard.toString()));
      }
      else
        guard = Objects.requireNonNull(UMLParser.expression(mGuardBody));
    }
    else
      guard = UExpression.trueConst();

    UAction effect = parseAction(mTransition.getEffect());

    UTransition transition = uRegion.addTransition(sourceUVertex, targetUVertex, trigger, guard, effect);

    info.putTransition(mTransition, transition);
    DEBUG.info("Added transition " + quoted(transition) + " to state machine of class " + quoted(info.getClassName()));
  }

  @NonNullByDefault
  private @Nullable UEvent getEvent(UState state, @Nullable Event mEvent, StateMachineInfo info) throws UModelException {
    if (mEvent == null)
      return null;

    UEvent event = info.getEvent(mEvent);
    if (event != null) {
      if (event.isTime()) {
        if (event.getState().equals(state))
          return event;
        else
          util.Message.warning("Time event " + quoted(event.getName()) + " also used on state " + quoted(state.getIdentifier()) + " in state machine for class " + quoted(info.getClassName()));
      }
    }

    if (mEvent instanceof SignalEvent) {
      Signal mSignal = ((SignalEvent)mEvent).getSignal();
      if (mSignal != null) {
        USignal signal = requireNonNull(getSignal(mSignal));
        event = UEvent.signal(signal);
        if (info.getReception(signal) == null)
          throw new UModelException("No reception for signal " + quoted(signal.getName()) + " in class " + quoted(info.getClassName()));
      }
    }

    if (mEvent instanceof CallEvent) {
      Operation mOperation = ((CallEvent)mEvent).getOperation();
      if (mOperation != null)
        event = UEvent.call(requireNonNull(getOperation(mOperation)));
    }

    if (mEvent instanceof TimeEvent) {
      var mTimeEvent = (TimeEvent)mEvent;
      String mtimeEventWhenBody = getValueSpecificationBody(mTimeEvent.getWhen().getExpr(), ", ");
      if (mtimeEventWhenBody != null) {
        if (!mTimeEvent.isRelative())
          util.Message.warning("Interpreting absolute time event " + quoted(mtimeEventWhenBody) + " in state machine for class " + quoted(info.getClassName()) + " as relative");

        if (mtimeEventWhenBody.startsWith("[")) {
          String timeEventWhenBody1 = mtimeEventWhenBody.substring(1, mtimeEventWhenBody.indexOf(","));
          String timeEventWhenBody2 = mtimeEventWhenBody.substring(mtimeEventWhenBody.indexOf(",") + 1, mtimeEventWhenBody.length() - 1);
          event = UEvent.time(state, requireNonNull(UMLParser.expression(timeEventWhenBody1)), requireNonNull(UMLParser.expression(timeEventWhenBody2)));
        }
        else {
          if (mtimeEventWhenBody.endsWith("ms")) {
            util.Message.info("Removing trailing `ms' in time expression " + quoted(mtimeEventWhenBody) + " in state machine for class " + quoted(info.getClassName()));
            mtimeEventWhenBody = mtimeEventWhenBody.replaceAll("ms$", "");
          }
          if (mtimeEventWhenBody.endsWith("s")) {
            util.Message.info("Removing trailing `s' in time expression " + quoted(mtimeEventWhenBody) + " in state machine for class " + quoted(info.getClassName()));
            mtimeEventWhenBody = mtimeEventWhenBody.replaceAll("s$", "");
          }
          event = UEvent.time(state, requireNonNull(UMLParser.expression(mtimeEventWhenBody)));
        }
      }
    }

    if (event != null)
      info.putEvent(mEvent, event);
    return event;   
  }

  private void setObjects(Collaboration mCollaboration, UCollaboration collaboration) throws UModelException {
    for (Property mProperty : mCollaboration.getOwnedAttributes()) {
      UObject object = getObject(mProperty, collaboration);
      collaboration.addObject(object);
    }
  }

  private void setSlots(Collaboration mCollaboration, UCollaboration collaboration) throws UModelException {
    for (Connector mConnector : mCollaboration.getOwnedConnectors()) {
      if (mConnector.getType() == null) {
        if (mConnector.getName() == null || mConnector.getName().trim().equals("")) {
          util.Message.warning("Ignoring connector with empty name and no type for collaboration " + quoted(collaboration.getName()));
          continue;
        }

        Association mAssociation = this.mModel.getMAssociations().stream().filter(mA -> mConnector.getName().equals(mA.getName())).findFirst().orElse(null);
        if (mAssociation == null) {
          util.Message.warning("Ignoring untyped connector " + quoted(mConnector.getName()) + " for collaboration " + quoted(collaboration.getName()) + " as no equally named association could be found");
          continue;
        }
        mConnector.setType(mAssociation);
        util.Message.info("Setting type of connector " + quoted(mConnector.getName()) + " for collaboration " + quoted(collaboration.getName()) + " to equally named association");
      }

      if (   mConnector.getEnds() == null
          || mConnector.getEnds().size() != 2
          || mConnector.getType() == null
          || mConnector.getType().getMemberEnds() == null
          || mConnector.getType().getMemberEnds().size() != 2) {
        util.Message.warning("Ignoring non-binary connector " + quoted(mConnector.getName()) + " for collaboration " + quoted(collaboration.getName()));
        continue;
      }

      if (   !(mConnector.getEnds().get(0).getRole() instanceof Property)
          || !(mConnector.getEnds().get(1).getRole() instanceof Property)) {
        util.Message.warning("Ignoring non-property binary connector " + quoted(mConnector.getName()) + " for collaboration " + quoted(collaboration.getName()));
        continue;
      }

      Property firstMProperty = (Property)mConnector.getEnds().get(0).getRole();
      Property secondMProperty = (Property)mConnector.getEnds().get(1).getRole();
      Property firstDefiningEndMProperty = mConnector.getType().getMemberEnds().get(0);
      Property secondDefiningEndMProperty = mConnector.getType().getMemberEnds().get(1);
      if (   !(firstDefiningEndMProperty.getType().equals(firstMProperty.getType()) && secondDefiningEndMProperty.getType().equals(secondMProperty.getType()))
          && !(firstDefiningEndMProperty.getType().equals(secondMProperty.getType()) && secondDefiningEndMProperty.getType().equals(firstMProperty.getType()))) {
        util.Message.warning("Ignoring connector" + quoted(mConnector.getName()) + " for collaboration " + quoted(collaboration.getName()) + " as it shows different types of connector end properties " + quoted(firstMProperty.getName()) + ", " + quoted(secondMProperty.getName()) + " and defining association ends " + quoted(firstDefiningEndMProperty.getName()) + ", " + quoted(secondDefiningEndMProperty.getName()));
        continue;
      }
      if (!(firstDefiningEndMProperty.getType().equals(firstMProperty.getType()) && secondDefiningEndMProperty.getType().equals(secondMProperty.getType()))) {
        Property swappingMProperty = firstMProperty;
        firstMProperty = secondMProperty;
        secondMProperty = swappingMProperty;
        util.Message.info("Swapping properties of binary connector " + quoted(mConnector.getName()) + " for collaboration " + quoted(collaboration.getName()));
      }

      UObject firstObject = getObject(firstMProperty, collaboration);
      UObject secondObject = getObject(secondMProperty, collaboration);
      UAttribute firstUAttribute = getAttribute(firstDefiningEndMProperty);
      UAttribute secondUAttribute = getAttribute(secondDefiningEndMProperty);
      if (firstUAttribute == null || secondUAttribute == null)
        continue;
      setSlot(firstObject, secondUAttribute, secondObject);
      setSlot(secondObject, firstUAttribute, firstObject);
    }
  }

  @NonNullByDefault
  private void setSlot(UObject uObject, UAttribute uAttribute, UObject valueUObject) {
    USlot uSlot = uObject.getSlot(uAttribute);
    if (uSlot == null) {
      uSlot = new USlot(uAttribute);
      uObject.addSlot(uSlot);
    }
    uSlot.addValue(UExpression.id(Identifier.id(valueUObject.getName())));
  }

  private void setConstraints(Collaboration mCollaboration, UCollaboration collaboration) throws UModelException {
    for (Constraint mConstraint : mCollaboration.getOwnedRules()) {
      OConstraint constraint = requireNonNull(UMLParser.constraint(canonise(mConstraint.getName()), getValueSpecificationBody(mConstraint.getSpecification(), "")));
      collaboration.addConstraint(constraint);
    }
  }

  private void setInteractions(Collaboration mCollaboration, @NonNull UCollaboration uCollaboration) throws UModelException {
    for (Behavior mBehavior : mCollaboration.getOwnedBehaviors()) {
      if (!(mBehavior instanceof Interaction))
        continue;
  
      Interaction mInteraction = (Interaction)mBehavior;
      UInteraction uInteraction = uCollaboration.addInteraction(canonise(mInteraction.getName()));
  
      setLifelines(mInteraction, uInteraction);
      setMessages(mInteraction, uInteraction);
      setFragments(mInteraction, uInteraction);
      setTimings(mInteraction, uInteraction);
    }
  }

  private void setLifelines(InteractionFragment mInteractionFragment, UInteraction interaction) throws UModelException {
    for (Lifeline mLifeline : mInteractionFragment.getCovereds()) {
      if (mLifeline.getRepresents() == null)
        throw new UModelException("Lifeline " + quoted(mLifeline.getName()) + " in interaction " + quoted(interaction.getName()) + " does not refer to a connectable element");

      UObject object = getObject(mLifeline.getRepresents(), interaction.getCollaboration());
      if (interaction.getLifeline(object) != null)
        continue;

      ULifeline lifeline = new ULifeline(object, interaction);
      interaction.addLifeline(lifeline);
    }

    if (mInteractionFragment instanceof CombinedFragment) {
      for (InteractionFragment subMInteractionFragment : ((CombinedFragment)mInteractionFragment).getOperands())
        setLifelines(subMInteractionFragment, interaction);
    }

    if (mInteractionFragment instanceof Interaction) {
      for (InteractionFragment subMInteractionFragment : ((Interaction)mInteractionFragment).getFragments())
        setLifelines(subMInteractionFragment, interaction);
    }
  }

  private void setMessages(Interaction mInteraction, UInteraction uInteraction) throws UModelException {
    for (Message mMessage : mInteraction.getMessages()) {
      setMessage(mMessage, uInteraction);
    }
  }
 
  private void setMessage(Message mMessage, UInteraction uInteraction) throws UModelException {
    String messageName = canonise(mMessage.getName());
    for (UMessage message : uInteraction.getMessages()) {
      if (message.getName().equals(messageName))
        throw new UModelException("Two different messages with same name " + quoted(messageName) + " in interaction " + quoted(uInteraction.getName()));
    }

    if (!(mMessage.getSendEvent() instanceof MessageOccurrenceSpecification)) {
      util.Message.warning("Ignoring message " + quoted(messageName) + " in interaction " + quoted(uInteraction.getName()) + " as it has no message occurrence specification as send event");
      return;
    }
    if (!(mMessage.getReceiveEvent() instanceof MessageOccurrenceSpecification)) {
      util.Message.warning("Ignoring message " + quoted(messageName) + " in interaction " + quoted(uInteraction.getName()) + " as it has no message occurrence specification as receive event");
      return;
    }
    MessageOccurrenceSpecification mMessageSendEvent = (MessageOccurrenceSpecification)mMessage.getSendEvent();
    MessageOccurrenceSpecification mMessageReceiveEvent = (MessageOccurrenceSpecification)mMessage.getReceiveEvent();
    if (!mMessage.equals(mMessageSendEvent.getMessage())) {
      mMessageSendEvent.setMessage(mMessage);
      util.Message.warning("Setting message " + quoted(messageName) + " in interaction " + quoted(uInteraction.getName()) + " as its send event's message");
    }
    if (!mMessage.equals(mMessageReceiveEvent.getMessage())) {
      mMessageReceiveEvent.setMessage(mMessage);
      util.Message.warning("Setting message " + quoted(messageName) + " in interaction " + quoted(uInteraction.getName()) + " as its receive event's message");
    }

    if (mMessageSendEvent.getCovereds() == null ||
        mMessageSendEvent.getCovereds().size() == 0 ||
        mMessageSendEvent.getCovereds().size() > 1) {
      util.Message.warning("Ignoring message " + quoted(messageName) + " in interaction " + quoted(uInteraction.getName()) + " as its send event has no unique covered lifeline");
      return;
    }
    if (mMessageReceiveEvent.getCovereds() == null ||
        mMessageReceiveEvent.getCovereds().size() == 0 ||
        mMessageReceiveEvent.getCovereds().size() > 1) {
      util.Message.warning("Ignoring message " + quoted(messageName) + " in interaction " + quoted(uInteraction.getName()) + " as its receive event has no unique covered lifeline");
      return;
    }

    UObject messageSender = objects.get(mMessageSendEvent.getCovereds().get(0).getRepresents());
    UObject messageReceiver = objects.get(mMessageReceiveEvent.getCovereds().get(0).getRepresents());
    if (messageReceiver == null) {
      util.Message.warning("Ignoring message " + quoted(messageName) + " in interaction " + quoted(uInteraction.getName()) + " as its receiver could not be determined");
      return;
    }
    UBehavioural messageBehavioural = null;
    if (mMessage.getSignature() instanceof Operation)
      messageBehavioural = getOperation((Operation)mMessage.getSignature());
    if (mMessage.getSignature() instanceof Reception)
      messageBehavioural = getReception((Reception)mMessage.getSignature());
    if (mMessage.getSignature() instanceof Signal) {
      USignal uSignal = getSignal((Signal)mMessage.getSignature());
      if (uSignal != null)
        messageBehavioural = messageReceiver.getC1ass().getReception(uSignal);
    }
    if (messageBehavioural == null)
      throw new UModelException("Message " + quoted(messageName) + " in interaction " + quoted(uInteraction.getName()) + " shows no behavioural");
    List<@NonNull UExpression> messageArguments = new ArrayList<>();
    for (ValueSpecification mValueSpecification : mMessage.getArguments()) {
      UExpression messageArgument = parseExpression(mValueSpecification);
      messageArguments.add(messageArgument);
    }
    UMessage message = new UMessage(messageName, messageSender, messageBehavioural, messageArguments, messageReceiver);

    /* TODO (AK121222) Is this needed? The occurrence specifications should be set in the interaction fragments.
    MessageOccurrenceSpecification sendMMessageOccurrenceSpecification = (MessageOccurrenceSpecification)mMessage.getSendEvent();
    MessageOccurrenceSpecification receiveMMessageOccurrenceSpecification = (MessageOccurrenceSpecification)mMessage.getReceiveEvent();
    UMessageOccurrenceSpecification sendMessageOccurrenceSpecification = UMessageOccurrenceSpecification.send(sendMMessageOccurrenceSpecification.getName(), message);
    UMessageOccurrenceSpecification receiveMessageOccurrenceSpecification = UMessageOccurrenceSpecification.receive(receiveMMessageOccurrenceSpecification.getName(), message);
    */
  
    messages.put(mMessage, message);
    uInteraction.addMessage(message);
  }

  private void setMessageOccurrenceSpecification(UInteraction uInteraction, MessageOccurrenceSpecification mMessageOccurrenceSpecification, UBasicFragment uBasicFragment) throws UModelException {
    if (mMessageOccurrenceSpecification.getCovereds() == null ||
        mMessageOccurrenceSpecification.getCovereds().size() == 0 ||
        mMessageOccurrenceSpecification.getCovereds().size() > 1) {
      util.Message.warning("Ignoring message occurrence specification " + quoted(mMessageOccurrenceSpecification.getName()) + " in interaction " + quoted(uInteraction.getName()) + " as it has no unique covered lifeline");
      return;
    }

    UObject uObject = getObject(((InteractionFragment)mMessageOccurrenceSpecification).getCovereds().get(0).getRepresents(), uInteraction.getCollaboration());
    ULifeline uLifeline = uInteraction.getLifeline(uObject);
    if (uLifeline == null) {
      util.Message.warning("Ignoring message occurrence specification " + quoted(mMessageOccurrenceSpecification.getName()) + " in interaction " + quoted(uInteraction.getName()) + " as no lifeline could be determined for it");
      return;
    }

    Message mMessage = mMessageOccurrenceSpecification.getMessage();
    UMessage uMessage = this.messages.get(mMessage);
    if (uMessage == null) {
      util.Message.warning("Ignoring message occurrence specification " + quoted(mMessageOccurrenceSpecification.getName()) + " in interaction " + quoted(uInteraction.getName()) + " as it shows none of the interaction's messages");
      return;
    }
    String uName = mMessageOccurrenceSpecification.getName();
    if (uName == null)
      uName = "null";

    UMessageOccurrenceSpecification.Kind uKind = null;
    if (mMessageOccurrenceSpecification.equals(mMessage.getSendEvent()))
      uKind = UMessageOccurrenceSpecification.Kind.SEND;
    if (mMessageOccurrenceSpecification.equals(mMessage.getReceiveEvent()))
      uKind = UMessageOccurrenceSpecification.Kind.RECEIVE;
    if (uKind == null) {
      util.Message.warning("Ignoring message occurrence specification " + quoted(mMessageOccurrenceSpecification.getName()) + " in interaction " + quoted(uInteraction.getName()) + " as it shows neither a send nor a receive event of any of the interaction's messages");
      return;
    }

    UMessageOccurrenceSpecification uMessageOccurrenceSpecification = uBasicFragment.addMessageOccurrenceSpecification(uName, uKind, uLifeline, uMessage);
    this.occurrenceSpecifications.put(mMessageOccurrenceSpecification, uMessageOccurrenceSpecification);
  }

  private void setCombinedFragment(@NonNull UInteraction interaction, @NonNull CombinedFragment mCombinedFragment, UInteractionOperand interactionOperand) throws UModelException {
    UCombinedFragment uCombinedFragment = null;

    switch (mCombinedFragment.getInteractionOperator()) {
      case IGNORE_LITERAL:
      case CONSIDER_LITERAL: {
        ConsiderIgnoreFragment mConsiderIgnoreFragment = (ConsiderIgnoreFragment)mCombinedFragment;
        @NonNull Set<@NonNull Identifier> messages = new HashSet<>();
        for (NamedElement uNamedElement : mConsiderIgnoreFragment.getMessages()) {
          if (uNamedElement != null) {
            String uName = uNamedElement.getName();
            if (uName != null)
              messages.add(Identifier.id(uName));
          }
        }
        if (mCombinedFragment.getInteractionOperator() == InteractionOperatorKind.IGNORE_LITERAL)
          uCombinedFragment = interactionOperand == null ? new UIgnoreFragment(messages, interaction) : new UIgnoreFragment(messages, interactionOperand);
        else
          uCombinedFragment = interactionOperand == null ? new UConsiderFragment(messages, interaction) : new UConsiderFragment(messages, interactionOperand);
        break;
      }
      case SEQ_LITERAL: {
        uCombinedFragment = interactionOperand == null ? new USequentialFragment(interaction) : new USequentialFragment(interactionOperand);
        break;
      }
      case STRICT_LITERAL: {
        uCombinedFragment = interactionOperand == null ? new UStrictFragment(interaction) : new UStrictFragment(interactionOperand);
        break;
      }
      case ALT_LITERAL: {
        uCombinedFragment = interactionOperand == null ? new UAlternativeFragment(interaction) : new UAlternativeFragment(interactionOperand);
        break;
      }
      case OPT_LITERAL: {
        uCombinedFragment = interactionOperand == null ? new UOptionalFragment(interaction) : new UOptionalFragment(interactionOperand);
        break;
      }
      case PAR_LITERAL: {
        uCombinedFragment = interactionOperand == null ? new UParallelFragment(interaction) : new UParallelFragment(interactionOperand);
        break;
      }
      case LOOP_LITERAL: {
        // TODO (AK160816) min and max should be read from the operand when adding it to the combined fragment
        UnlimitedNatural min = UnlimitedNatural.nat(0);
        UnlimitedNatural max = UnlimitedNatural.infty();
        List<InteractionOperand> mOperands = mCombinedFragment.getOperands();
        if (mOperands != null && !mOperands.isEmpty()) {
          InteractionOperand mOperand = mOperands.get(0);
          if (mOperand != null) {
            UContext uContext = interaction.getMessageContext();
            InteractionConstraint mGuard = mOperand.getGuard();
            if (mGuard != null) {
              ValueSpecification mMinint = mGuard.getMinint();
              if (mMinint != null)
                min = parseUnlimitedNatural(uContext, mMinint);
              ValueSpecification mMaxint = mGuard.getMaxint();
              if (mMaxint != null)
                max = parseUnlimitedNatural(uContext, mMaxint);
            }
          }
        }
        uCombinedFragment = interactionOperand == null ? new ULoopFragment(false, min, max, interaction) : new ULoopFragment(false, min, max, interactionOperand);
        break;
      }

      case ASSERT_LITERAL:
      case BREAK_LITERAL:
      case CRITICAL_LITERAL:
      case NEG_LITERAL:
      default:
        util.Message.warning("Ignoring combined fragment " + quoted(mCombinedFragment.getName()) + " of type " + quoted(mCombinedFragment.getInteractionOperator()) + " in interaction " + quoted(interaction.getName()));
    }

    if (uCombinedFragment != null) {
      // Conversion to @NonNull list...
      List<InteractionOperand> mOperands = new ArrayList<>();
      for (InteractionOperand mOperand : mCombinedFragment.getOperands())
        mOperands.add(mOperand);
      setOperands(interaction, mOperands, uCombinedFragment);
    }
  }

  private void setOperands(@NonNull UInteraction interaction, @NonNull List<InteractionOperand> mInteractionOperands, @NonNull UCombinedFragment combinedFragment) throws UModelException {
    for (InteractionOperand mInteractionOperand : mInteractionOperands) {
      List<InteractionFragment> mInteractionFragments = new ArrayList<>();
      for (InteractionFragment mInteractionFragment : mInteractionOperand.getFragments())
        mInteractionFragments.add(mInteractionFragment);
      setFragments(interaction, mInteractionFragments, new UInteractionOperand(combinedFragment));
    }
  }

  private void setFragments(@NonNull UInteraction interaction, @NonNull List<InteractionFragment> mInteractionFragments, UInteractionOperand interactionOperand) throws UModelException {
    for (InteractionFragment mInteractionFragment : mInteractionFragments) {
      if (mInteractionFragment == null)
        continue;

      if (mInteractionFragment instanceof MessageOccurrenceSpecification) {
        MessageOccurrenceSpecification mMessageOccurrenceSpecification = (MessageOccurrenceSpecification)mInteractionFragment;
        UBasicFragment basicInteraction = (interactionOperand == null ? interaction.getBasicFragment() : interactionOperand.getBasicFragment());
        setMessageOccurrenceSpecification(interaction, mMessageOccurrenceSpecification, basicInteraction);
        continue;
      }

      if (mInteractionFragment instanceof CombinedFragment) {
        CombinedFragment mCombinedFragment = (CombinedFragment)mInteractionFragment;
        setCombinedFragment(interaction, mCombinedFragment, interactionOperand);
        continue;
      }

      DEBUG.info("Skipping fragment " + mInteractionFragment);
    }
  }

  private void setFragments(Interaction mInteraction, @NonNull UInteraction interaction) throws UModelException {
    List<InteractionFragment> mInteractionFragments = new ArrayList<>();
    for (InteractionFragment mInteractionFragment : mInteraction.getFragments())
      mInteractionFragments.add(mInteractionFragment);
    setFragments(interaction, mInteractionFragments, null);
  }

  // TODO (AK130613) Complete me
  private void setTimings(Interaction mInteraction, @NonNull UInteraction uInteraction) throws UModelException {
    for (Constraint mConstraint : mInteraction.getOwnedRules()) {
      if (!(mConstraint instanceof DurationConstraint))
        continue;

      DurationConstraint mDurationConstraint = (DurationConstraint)mConstraint;

      ValueSpecification mValueSpecification = mDurationConstraint.getSpecification();
      if (!(mValueSpecification instanceof DurationInterval))
        continue;
      DurationInterval mDurationInterval = (DurationInterval)mValueSpecification;
      ValueSpecification mMinDurationValueSpecification = mDurationInterval.getMin();
      ValueSpecification mMaxDurationValueSpecification = mDurationInterval.getMax();
      if (mMinDurationValueSpecification == null || !(mMinDurationValueSpecification instanceof Duration) ||
          mMaxDurationValueSpecification == null || !(mMaxDurationValueSpecification instanceof Duration))
        continue;
      Duration mMinDuration = (Duration)mMinDurationValueSpecification;
      Duration mMaxDuration = (Duration)mMaxDurationValueSpecification;
      ValueSpecification mMinValueSpecification = mMinDuration.getExpr();
      ValueSpecification mMaxValueSpecification = mMaxDuration.getExpr();
      DEBUG.info(mMinValueSpecification.toString());
      DEBUG.info(mMaxValueSpecification.toString());

      EList<Element> mConstrainedElements = mDurationConstraint.getConstrainedElements();
      if (mConstrainedElements == null || mConstrainedElements.size() != 2)
        continue;

      if (!(mConstrainedElements.get(0) instanceof MessageOccurrenceSpecification) || !(mConstrainedElements.get(1) instanceof MessageOccurrenceSpecification))
        continue;

      MessageOccurrenceSpecification mFirstMessageOccurrenceSpecification = (MessageOccurrenceSpecification)mConstrainedElements.get(0);
      MessageOccurrenceSpecification mSecondMessageOccurrenceSpecification = (MessageOccurrenceSpecification)mConstrainedElements.get(1);
      if (mFirstMessageOccurrenceSpecification == null || mSecondMessageOccurrenceSpecification == null)
        continue;

      UOccurrenceSpecification firstOccurrenceSpecification = this.occurrenceSpecifications.get(mFirstMessageOccurrenceSpecification);
      UOccurrenceSpecification secondOccurrenceSpecification = this.occurrenceSpecifications.get(mSecondMessageOccurrenceSpecification);
      if (firstOccurrenceSpecification == null || secondOccurrenceSpecification == null)
        continue;

      UTiming.leq(secondOccurrenceSpecification, firstOccurrenceSpecification, this.parseExpression(mMaxValueSpecification), uInteraction);
      UTiming.geq(secondOccurrenceSpecification, firstOccurrenceSpecification, this.parseExpression(mMinValueSpecification), uInteraction);
    }
  }

  @NonNullByDefault
  private UExpression parseConstraint(@Nullable ValueSpecification mValueSpecification) throws UModelException {
    UExpression expression = UExpression.trueConst();

    if (mValueSpecification != null) {
      String mValueSpecificationBody = getValueSpecificationBody(mValueSpecification, " && ");
      if (mValueSpecificationBody == null || "".equals(mValueSpecificationBody)) {
        util.Message.warning("Uninterpretable constraint " + quoted(mValueSpecification) + ", taking it to be `true'");
      }
      else
        expression = Objects.requireNonNull(UMLParser.expression(mValueSpecificationBody));
    }
    else
      expression = UExpression.trueConst();

    return expression;
  }

  /**
   * Retrieve the bodies of a value specification as a single string, concatenating several bodies using <code>op</code>.
   * 
   * @param mValueSpecification a value specification
   * @param op a string for concatenating several value specification bodies
   * @return
   */
  @NonNullByDefault
  private @Nullable String getValueSpecificationBody(@Nullable ValueSpecification mValueSpecification, String op) {
    if (mValueSpecification == null)
      return null;

    if (mValueSpecification.isComputable()) {
      if (mValueSpecification.isNull())
        return "null";
      if (mValueSpecification.stringValue() != null && !"".equals(mValueSpecification.stringValue().trim()))
        return "" + mValueSpecification.stringValue().trim();
    }

    String body = null;
    if (mValueSpecification instanceof LiteralString) {
      body = ((LiteralString)mValueSpecification).stringValue();
    }
    else {
      if (mValueSpecification instanceof OpaqueExpression) {
        for (String subBody : ((OpaqueExpression)mValueSpecification).getBodies()) {
          if (body == null)
            body = subBody;
          else
            body = body + op + subBody;
        }
      }
      else {
        if (mValueSpecification instanceof InstanceValue) {
          InstanceValue mInstanceValue = (InstanceValue)mValueSpecification;
          InstanceSpecification mInstanceSpecification = mInstanceValue.getInstance();
          if (mInstanceSpecification != null)
            body = mInstanceSpecification.getName();
        }
      }
    }
    if (body != null)
      return body.trim();

    return null;
  }

  @NonNullByDefault
  private UExpression parseExpression(@Nullable ValueSpecification mValueSpecification) throws UModelException {
    UExpression uExpressionDefault = UExpression.nullConst();

    if (mValueSpecification == null)
      return uExpressionDefault;

    String mValueSpecificationBody = getValueSpecificationBody(mValueSpecification, " ");
    if (mValueSpecificationBody == null || "".equals(mValueSpecificationBody)) {
      util.Message.warning("Uninterpretable expression " + quoted(mValueSpecification) + ", taking it to be " + quoted(uExpressionDefault));
      return uExpressionDefault;
    }

    return requireNonNull(UMLParser.expression(mValueSpecificationBody));
  }

  @NonNullByDefault
  private UnlimitedNatural parseUnlimitedNatural(UContext uContext, @Nullable ValueSpecification mValueSpecification) throws UModelException {
    UnlimitedNatural unlimitedNaturalDefault = UnlimitedNatural.nat(0);
    if (mValueSpecification == null)
      return unlimitedNaturalDefault;

    String mValueSpecificationBody = getValueSpecificationBody(mValueSpecification, " ");
    if (mValueSpecificationBody == null || "".equals(mValueSpecificationBody)) {
      util.Message.warning("Uninterpretable unlimited natural " + quoted(mValueSpecification) + ", taking it to be " + quoted(unlimitedNaturalDefault));
      return unlimitedNaturalDefault;
    }

    if ("infty".equals(mValueSpecificationBody))
      return UnlimitedNatural.infty();

    int value = requireNonNull(UMLParser.expression(mValueSpecificationBody)).getIntegerValue(uContext);
    if (value < 0) {
      util.Message.warning("Negative integer " + quoted(mValueSpecification) + ", replacing it by " + quoted(unlimitedNaturalDefault));
      return unlimitedNaturalDefault;
    }
    return UnlimitedNatural.nat(value);
  }

  @NonNullByDefault
  private UAction parseAction(@Nullable Behavior mBehaviour) throws UModelException {
    UAction uActionDefault = UAction.skip();

    if (mBehaviour == null)
      return uActionDefault;
    
    String mBehaviorBody = getBehaviourBody(mBehaviour);
    if (mBehaviorBody == null || "".equals(mBehaviorBody)) {
      util.Message.warning("Uninterpretable behaviour " + quoted(mBehaviour) + ", taking it to be " + quoted(uActionDefault));
      return uActionDefault;
    }

    return Objects.requireNonNull(UMLParser.action(mBehaviorBody));
  }

  @NonNullByDefault
  private @Nullable String getBehaviourBody(Behavior mBehaviour) throws UModelException {
    String body = null;

    if (mBehaviour instanceof OpaqueBehavior) {
      for (String subBody : ((OpaqueBehavior)mBehaviour).getBodies()) {
        if (body == null)
          body = subBody;
        else
          body = body + " " + subBody;
      }
    }
    if (body != null)
      return body.trim();

    return null;
  }

  @NonNullByDefault
  private String canonise(@Nullable String string) {
    if (string == null || string.equals(""))
      string = "empty";
    char[] characters = string.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    if (Character.isDigit(characters[0]))
      return "_" + new String(characters);
    return new String(characters);
  }
}
