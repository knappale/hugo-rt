package uml.parser.xmi.uml23xmi21;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Signal;

import uml.UModelException;


public class MModel {
  private Model model;
  private Set<Class> mClasses = null;
  private Set<Association> mAssociations = null;
  private Set<DataType> mDataTypes = null;
  private Set<Signal> mSignals = null;
  private Set<Collaboration> mCollaborations = null;

  public MModel(Model model) throws UModelException {
    this.model = model;
  }

  public String getName() {
    return model.getName();
  }

  Set<Class> getMClasses() throws UModelException {
    if (this.mClasses == null)
      this.mClasses = getMClasses(model);
    return this.mClasses;
  }

  Set<Association> getMAssociations() throws UModelException {
    if (this.mAssociations == null)
      this.mAssociations = getMAssociations(model);
    return this.mAssociations;
  }

  Set<DataType> getMDataTypes() throws UModelException {
    if (this.mDataTypes == null)
      this.mDataTypes = getMDataTypes(model);
    return this.mDataTypes;
  }

  Set<Signal> getMSignals() throws UModelException {
    if (this.mSignals == null)
      this.mSignals = getMSignals(model);
    return this.mSignals;
  }

  Set<Collaboration> getMCollaborations() throws UModelException {
    if (this.mCollaborations == null)
      this.mCollaborations = getMCollaborations(model);
    return this.mCollaborations;
  }

  ConnectorEnd getOpposite(Connector connector, ConnectorEnd connectorEnd) {
    for (ConnectorEnd otherConnectorEnd : connector.getEnds()) {
      if (connectorEnd != otherConnectorEnd)
        return otherConnectorEnd;
    }
    return null;
  }

  /**
   * Initialise the set of classes in a namespace recursively.
   */
  private Set<Class> getMClasses(Namespace mNamespace) throws UModelException {
    Set<Class> mClasses = new HashSet<Class>();

    for (Element mElement : mNamespace.getOwnedElements()) {
      if ( (mElement instanceof Class) &&
          !(mElement instanceof Behavior)) {
        Class mClass = (Class)mElement;
        mClasses.add(mClass);
      }
      if (mElement instanceof Namespace)
        mClasses.addAll(getMClasses((Namespace)mElement));
    }

    return mClasses;
  }

  /**
   * Initialise the set of associations in a namespace recursively.
   */
  private Set<Association> getMAssociations(Namespace mNamespace) throws UModelException {
    Set<Association> mAssociations = new HashSet<Association>();

    for (Element mElement : mNamespace.getOwnedElements()) {
      if (mElement instanceof Association) {
        Association mAssociation = (Association)mElement;
        mAssociations.add(mAssociation);
      }
      if (mElement instanceof Namespace)
        mAssociations.addAll(getMAssociations((Namespace)mElement));
    }

    return mAssociations;
  }

  /**
   * Initialise the set of data types in a namespace recursively.
   */
  private Set<DataType> getMDataTypes(Namespace mNamespace) {
    Set<DataType> mDataTypes = new HashSet<DataType>();

    for (Element mElement : mNamespace.getOwnedElements()) {
      if (mElement instanceof DataType) {
        DataType mDataType = (DataType)mElement;
        mDataTypes.add(mDataType);
      }
      if (mElement instanceof Namespace)
        mDataTypes.addAll(getMDataTypes((Namespace)mElement));
    }

    return mDataTypes;
  }

  /**
   * Initialise the set of signals in a namespace recursively
   */
  private Set<Signal> getMSignals(Namespace mNamespace) {
    Set<Signal> mSignals = new HashSet<Signal>();

    for (Element mElement : mNamespace.getOwnedElements()) {
      if (mElement instanceof Signal) {
        Signal mSignal = (Signal)mElement;
        mSignals.add(mSignal);
      }
      if (mElement instanceof Namespace)
        mSignals.addAll(getMSignals((Namespace)mElement));
    }

    return mSignals;
  }

  /**
   * Initialise the set of collaborations in a namespace
   */
  private Set<Collaboration> getMCollaborations(Namespace mNamespace) {
    Set<Collaboration> mCollaborations = new HashSet<Collaboration>();

    for (Element mElement : mNamespace.getOwnedElements()) {
      if (mElement instanceof Collaboration) {
        Collaboration mCollaboration = (Collaboration)mElement;
        mCollaborations.add(mCollaboration);
      }
      if (mElement instanceof Namespace)
        mCollaborations.addAll(getMCollaborations((Namespace)mElement));
    }

    return mCollaborations;
  }
}
