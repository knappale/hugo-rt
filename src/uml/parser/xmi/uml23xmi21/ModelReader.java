package uml.parser.xmi.uml23xmi21;

import java.io.BufferedInputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UMLResource;

import uml.UModel;
import uml.UModelException;


public class ModelReader extends uml.parser.ModelReader {
  private org.eclipse.uml2.uml.@NonNull Model mModel;
  
  private static Resource makeUMLResource() {
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("uml", UMLResource.Factory.INSTANCE);
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("uml2", UMLResource.Factory.INSTANCE);

    ResourceSet resourceSet = new ResourceSetImpl();
    resourceSet.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
    resourceSet.getPackageRegistry().put("http://www.eclipse.org/uml2/3.0.0/UML", UMLPackage.eINSTANCE);
    resourceSet.getPackageRegistry().put("http://www.eclipse.org/uml2/4.0.0/UML", UMLPackage.eINSTANCE);
    resourceSet.getPackageRegistry().put("http://www.eclipse.org/uml2/5.0.0/UML", UMLPackage.eINSTANCE);

    URI uri = URI.createURI("file:///anonymous.uml");
    URIConverter.URI_MAP.put(URI.createURI(UMLResource.LIBRARIES_PATHMAP), uri.appendSegment("libraries").appendSegment(""));
    URIConverter.URI_MAP.put(URI.createURI(UMLResource.METAMODELS_PATHMAP), uri.appendSegment("metamodels").appendSegment(""));
    URIConverter.URI_MAP.put(URI.createURI(UMLResource.PROFILES_PATHMAP), uri.appendSegment("profiles").appendSegment(""));
    URIConverter.URI_MAP.put(URI.createURI(UMLResource.ECORE_PRIMITIVE_TYPES_LIBRARY_URI), uri.appendSegment("ecore_primitive_types").appendSegment(""));

    return resourceSet.createResource(uri);
  }

  public ModelReader(BufferedInputStream modelInputStream) throws UModelException {
    DEBUG.info("Parsing with UML 2.3/XMI 2.1 reader");

    Resource resource = makeUMLResource();

    org.eclipse.uml2.uml.Model mModel = null;
    try {
      // We first have to initialise UMLPackage, the following is one way of achieving this:
      EClass uModel = UMLPackage.eINSTANCE.getModel();

      Map<Object, Object> optionsMap = new HashMap<>();
      optionsMap.put("OPTION_RESOLVE", true);
      resource.load(modelInputStream, optionsMap);
      DEBUG.info("... loaded");
      EcoreUtil.resolveAll(resource);
      DEBUG.info("... resolved");
      mModel = (org.eclipse.uml2.uml.Model)EcoreUtil.getObjectByType(resource.getContents(), uModel);
      DEBUG.info("... done");
      // mModel = (org.eclipse.uml2.uml.Model)resource.getContents().get(0);
      // EcoreUtil.resolveAll(mModel);
    }
    catch (Exception e) {
      e.printStackTrace();
      throw new UModelException("XMI 2.1 parsing failed: " + e);
    }
    if (mModel == null)
      throw new UModelException("No model found in XMI 2.1 input");
    this.mModel = mModel;
  }

  @Override
  public String getModelName() {
    return this.mModel.getName();
  }

  @Override
  public @NonNull UModel getRawModel() throws UModelException {
    return Translator.translate(new MModel(mModel));
  }
}
