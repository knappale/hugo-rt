package uml;

import org.eclipse.jdt.annotation.NonNull;


/**
 * Contains data of a UML structural (feature).
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public abstract class UStructural extends UStatual {
  /**
   * Create a structural (feature)
   *
   * @param name structural (feature)'s name
   */
  public UStructural(@NonNull String name) {
    super(name);
  }
}
