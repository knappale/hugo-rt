package uml;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Identifier;

import static util.Objects.requireNonNull;


/**
 * UML descriptor
 *
 * A descriptor contains information for describing an element of a
 * UML model.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UDescriptor {
  static enum Kind {
    ATTRIBUTE,
    PARAMETER,
    BEHAVIOURAL,
    OBJECT,
    VERTEX;

    String getName() {
      switch (this) {
        case ATTRIBUTE: return "Attribute";
        case PARAMETER: return "Parameter";
        case BEHAVIOURAL: return "Behavioural";
        case OBJECT: return "Object";
        case VERTEX: return "Vertex";
      }
      throw new IllegalStateException("Unknown descriptor kind.");
    }
  }

  private Kind kind;
  private Identifier identifier;
  private @Nullable UClass uClass = null;
  private @Nullable List<UType> parameterTypes = null;

  private UDescriptor(Identifier identifier, Kind kind) {
    this.kind = kind;
    this.identifier = identifier;
  }

  /**
   * Create an attribute descriptor.
   *
   * @param id an identifier
   * @return an attribute descriptor
   */
  public static UDescriptor attribute(Identifier identifier) {
    return new UDescriptor(identifier, Kind.ATTRIBUTE);
  }

  /**
   * Create a parameter descriptor.
   *
   * @param id an identifier
   * @return a parameter descriptor
   */
  public static UDescriptor parameter(Identifier identifier) {
    return new UDescriptor(identifier, Kind.PARAMETER);
  }

  /**
   * Create a behavioural descriptor.
   *
   * @param uClass a class, the declaring class
   * @param identifier an identifier, the name of the behavioural of the behavioural
   * @return a behavioural descriptor
   */
  public static UDescriptor behavioural(UClass uClass, Identifier identifier) {
    UDescriptor d = new UDescriptor(identifier, Kind.BEHAVIOURAL);
    d.uClass = uClass;
    d.parameterTypes = new ArrayList<>();
    return d;
  }

  /**
   * Create a behavioural descriptor.
   *
   * @param uClass a class, the declaring class
   * @param identifier an identifier, the name of the behavioural
   * @param parameterTypes a list of parameter types, the parameter types of the behavioural
   * @return a behavioural descriptor
   */
  public static UDescriptor behavioural(UClass uClass, Identifier identifier, List<UType> parameterTypes) {
    UDescriptor d = new UDescriptor(identifier, Kind.BEHAVIOURAL);
    d.uClass = uClass;
    d.parameterTypes = new ArrayList<>();
    d.parameterTypes.addAll(parameterTypes);
    return d;
  }

  /**
   * Create an object descriptor.
   *
   * @param id an identifier
   * @return an object descriptor
   */
  public static UDescriptor object(Identifier identifier) {
    return new UDescriptor(identifier, Kind.OBJECT);
  }

  /**
   * Create a state descriptor.
   *
   * @param id an identifier
   * @return a state descriptor
   */
  public static UDescriptor state(UClass uClass, Identifier identifier) {
    UDescriptor d = new UDescriptor(identifier, Kind.VERTEX);
    d.uClass = uClass;
    return d;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.kind,
                        this.uClass,
                        this.identifier,
                        this.parameterTypes);
  }

  /**
   * Determine whether this descriptor matches another descriptor.
   *
   * @param other a descriptor
   * @return whether this descriptor matches the other descriptor
   */
  public boolean matches(UDescriptor other) {
    // Check kinds of elements
    if (this.kind != other.kind)
      return false;
    // Check identifier
    if (!this.identifier.matches(other.identifier))
      return false;

    UClass thisUClass = requireNonNull(this.uClass);
    UClass otherUClass = requireNonNull(other.uClass);
    switch (this.kind) {
      case BEHAVIOURAL: {
        // Check declaring class
        if (!thisUClass.equals(otherUClass))
          return false;

        // Check parameter list
        List<UType> thisParameterTypes = requireNonNull(this.parameterTypes);
        List<UType> otherParameterTypes = requireNonNull(other.parameterTypes);
        if (thisParameterTypes.size() != otherParameterTypes.size())
          return false;
        for (int i = 0; i < thisParameterTypes.size(); i++) {
          UType thisParameterType = thisParameterTypes.get(i);
          UType otherParameterType = otherParameterTypes.get(i);
          if (!thisParameterType.subsumes(otherParameterType))
            return false;
        }
        break;
      }
      case VERTEX: {
        // Check declaring class
        if (!thisUClass.equals(otherUClass))
          return false;
        break;
      }
      //$CASES-OMITTED
      default:
    }

      return true;
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UDescriptor other = (UDescriptor)object;
      return Objects.equals(this.kind, other.kind) &&
             Objects.equals(this.uClass, other.uClass) &&
             Objects.equals(this.identifier, other.identifier) &&
             Objects.equals(this.parameterTypes, other.parameterTypes);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("[");
    resultBuilder.append(this.kind.getName());
    resultBuilder.append("]");
    UClass uClass = this.uClass;
    if (uClass != null) {
      resultBuilder.append(uClass.getName());
      resultBuilder.append(".");
    }
    resultBuilder.append(this.identifier);
    List<UType> parameterTypes = this.parameterTypes;
    if (parameterTypes != null) {
      resultBuilder.append("(");
      resultBuilder.append(Formatter.separated(parameterTypes, parameterType -> parameterType.getName(), ", "));
      resultBuilder.append(")");
    }

    return resultBuilder.toString();
  }
}
