package uml;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.interaction.UInteraction;
import uml.statemachine.UEvent;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import util.Identifier;

import static java.util.stream.Collectors.toList;


/**
 * Context for UML expressions and actions.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UContext {
  private static Map<List<Object>, UContext> contexts = new HashMap<>();

  private final UModel model;
  private @Nullable UCollaboration collaboration = null;
  private @Nullable UInteraction interaction = null;
  private @Nullable UClass c1ass = null;
  private @Nullable UEvent event = null;
  private boolean isConstant = false;
  private Map<UDescriptor, UElement> elements = new HashMap<>();

  protected UContext(UModel model) {
    this.model = model;
  }

  /**
   * Creates a context for a model.
   *
   * @param model a UML model
   */
  public static UContext model(UModel model) {
    List<Object> key = new ArrayList<>();
    key.add(model);
    key.add(false);
    UContext c = contexts.get(key);
    if (c != null)
      return c;

    c = new UContext(model);
    c.isConstant = false;
    contexts.put(key, c);
    return c;
  }

  /**
   * Creates a constant context for a model.
   *
   * @param model a UML model
   */
  public static UContext modelConstant(UModel model) {
    List<Object> key = new ArrayList<>();
    key.add(model);
    key.add(true);
    UContext c = contexts.get(key);
    if (c != null)
      return c;

    c = new UContext(model);
    c.isConstant = true;
    contexts.put(key, c);
    return c;
  }

  /**
   * Creates a context for a collaboration.
   *
   * @param collaboration a UML collaboration
   */
  public static UContext collaboration(UCollaboration collaboration) {
    List<Object> key = new ArrayList<>();
    key.add(collaboration);
    key.add(false);
    UContext c = contexts.get(key);
    if (c != null)
      return c;

    c = new UContext(collaboration.getModel());
    c.collaboration = collaboration;
    c.isConstant = false;
    contexts.put(key, c);
    return c;
  }

  /**
   * Creates a constant context for a collaboration.
   *
   * @param collaboration a UML collaboration
   */
  public static UContext collaborationConstant(UCollaboration collaboration) {
    List<Object> key = new ArrayList<>();
    key.add(collaboration);
    key.add(true);
    UContext c = contexts.get(key);
    if (c != null)
      return c;

    c = new UContext(collaboration.getModel());
    c.collaboration = collaboration;
    c.isConstant = true;
    contexts.put(key, c);
    return c;
  }

  /**
   * Creates a context for an interaction (in a collaboration).
   *
   * @param interaction a UML interaction
   */
  public static UContext interaction(UInteraction interaction) {
    List<Object> key = new ArrayList<>();
    key.add(interaction);
    key.add(false);
    UContext c = contexts.get(key);
    if (c != null)
      return c;

    c = new UContext(interaction.getCollaboration().getModel());
    c.collaboration = interaction.getCollaboration();
    c.interaction = interaction;
    c.isConstant = false;
    contexts.put(key, c);
    return c;
  }

   /**
   * Creates a context for a classifier (in a model).
   *
   * @param c1ass (UML) classifier
   */
  public static UContext c1ass(UClass c1ass) {
    List<Object> key = new ArrayList<>();
    key.add(c1ass);
    key.add(false);
    UContext c = contexts.get(key);
    if (c != null)
      return c;

    c = new UContext(c1ass.getModel());
    c.c1ass = c1ass;
    c.isConstant = false;
    contexts.put(key, c);
    return c;
  }

  /**
   * Creates a constant context for a classifier (in a model).
   *
   * @param c1ass (UML) class
   */
  public static UContext classConstant(UClass c1ass) {
    List<Object> key = new ArrayList<>();
    key.add(c1ass);
    key.add(true);
    UContext c = contexts.get(key);
    if (c != null)
      return c;

    c = new UContext(c1ass.getModel());
    c.c1ass = c1ass;
    c.isConstant = true;
    contexts.put(key, c);
    return c;
  }

  /**
   * Creates a context for a class (in a model) and an event.
   *
   * @param uClass (UML) class
   * @param event event
   */
  public static UContext event(UClass uClass, UEvent event) {
    List<Object> key = new ArrayList<>();
    key.add(uClass);
    key.add(event);
    UContext c = contexts.get(key);
    if (c != null)
      return c;

    c = new UContext(uClass.getModel());
    c.c1ass = uClass;
    c.event = event;
    c.isConstant = false;
    contexts.put(key, c);
    return c;
  }

  /**
   * Determine the class denoted by an identifier.
   *
   * @param identifier identifier
   * @return (UML) class, or {@code null} if no matching class can be found
   */
  public @Nullable UClass getClass(@Nullable Identifier identifier) {
    if (identifier == null)
      return null;
    for (var uClass : this.model.getClasses()) {
      Identifier uClassIdentifier = Identifier.id(uClass.getName());
      if (uClassIdentifier.matches(identifier))
        return uClass;
    }
    return null;
  }

  /**
   * Determine the object denoted by an identifier.
   *
   * @param identifier identifier
   * @return (UML) object, or {@code null} if no matching object can be found
   */
  public @Nullable UObject getObject(@Nullable Identifier identifier) {
    if (identifier == null)
      return null;

    UDescriptor descriptor = UDescriptor.object(identifier);
    UElement element = this.elements.get(descriptor);
    if (element != null) {
      assert (element instanceof UObject) : "Element `" + element.getName() + "' should be an object";
      return (UObject)element;
    }

    UCollaboration uCollaboration = this.collaboration;
    if (uCollaboration == null || this.isConstant)
      return null;

    for (UObject uObject : uCollaboration.getObjects()) {
      Identifier objectIdentifier = Identifier.id(uObject.getName());
      if (objectIdentifier.matches(identifier)) {
        this.elements.put(descriptor, uObject);
        return uObject;
      }
    }
    return null;
  }

  /**
   * Determine the parameter denoted by an identifier.
   *
   * @param identifier identifier
   * @return (UML) parameter, or {@code null} if no matching parameter can be found
   */
  private @Nullable UParameter getParameter(@Nullable Identifier identifier) {
    if (identifier == null)
      return null;

    UDescriptor descriptor = UDescriptor.parameter(identifier);
    UElement element = this.elements.get(descriptor);
    if (element != null) {
      assert (element instanceof UParameter) : "Element `" + element.getName() + "' should be a parameter";
      return (UParameter)element;
    }

    UEvent uEvent = this.event;
    if (uEvent == null)
      return null;

    UClassifier uClassifier = this.c1ass;
    if (!(uClassifier instanceof UClass))
      return null;
    UClass uClass = (UClass)uClassifier;

    UBehavioural uBehavioural = uClass.getBehavioural(uEvent);
    if (uBehavioural == null)
      return null;

    for (UParameter uParameter : uBehavioural.getParameters()) {
      Identifier parameterIdentifier = Identifier.id(uParameter.getName());
      if (parameterIdentifier.matches(identifier)) {
        this.elements.put(descriptor, uParameter);
        return uParameter;
      }
    }
    return null;
  }

  /**
   * Determine the attribute denoted by an identifier.
   *
   * @param identifier identifier
   * @return (UML) attribute, or {@code null} if no matching attribute can be found
   */
  public @Nullable UAttribute getAttribute(@Nullable Identifier identifier) {
    if (identifier == null)
      return null;

    UDescriptor descriptor = UDescriptor.attribute(identifier);
    UElement element = this.elements.get(descriptor);
    if (element != null) {
      assert (element instanceof UAttribute) : "Element `" + element.getName() + "' should be an attribute";
      return (UAttribute)element;
    }

    // Check for class attributes
    for (var c1ass : this.model.getClasses()) {
      for (var attribute : c1ass.getAttributes()) {
        if (attribute.isStatic() && (!this.isConstant || attribute.isConstant())) {
          var attributeIdentifier = Identifier.id(Identifier.id(attribute.getOwner().getName()), attribute.getName());
          if (attributeIdentifier.matches(identifier)) {
            elements.put(descriptor, attribute);
            return attribute;
          }
        }
      }
    }

    UCollaboration uCollaboration = this.collaboration;
    if (uCollaboration != null) {
      // Check for attributes
      for (var uObject : uCollaboration.getObjects()) {
        for (var uAttribute : uObject.getC1ass().getAttributes()) {
          if (!this.isConstant || uAttribute.isConstant()) {
            var attributeIdentifier = Identifier.id(Identifier.id(uObject.getName()), uAttribute.getName());
            if (attributeIdentifier.matches(identifier)) {
              elements.put(descriptor, uAttribute);
              return uAttribute;
            }
          }
        }
      }
    }

    UClass c1ass = this.c1ass;
    if (c1ass != null) {
      // Check for instance attributes
      for (UAttribute uAttribute : c1ass.getAttributes()) {
        if (!this.isConstant || uAttribute.isConstant()) {
          Identifier attributeIdentifier = Identifier.id(uAttribute.getName());
          if (attributeIdentifier.matches(identifier)) {
            elements.put(descriptor, uAttribute);
            return uAttribute;
          }
        }
      }
      for (var uAttribute1 : c1ass.getAttributes()) {
        for (var uAttribute2 : uAttribute1.getClassifier().getAttributes().stream().filter(UAttribute::isStatic).collect(toList())) {
          Identifier attributeIdentifier = Identifier.id(Identifier.id(uAttribute1.getName()), uAttribute2.getName());
          if (attributeIdentifier.matches(identifier)) {
            elements.put(descriptor, uAttribute2);
            return uAttribute2;
          }
        }
      }
    }

    return null;
  }

  /**
   * Determine the statual denoted by an identifier.
   *
   * @param identifier identifier
   * @return (UML) statual, or {@code null} if no matching statual can be found
   */
  public @Nullable UStatual getStatual(@Nullable Identifier identifier) {
    if (identifier == null)
      return null;

    UObject object = getObject(identifier);
    if (object != null)
      return object;
    UParameter parameter = getParameter(identifier);
    if (parameter != null)
      return parameter;
    return getAttribute(identifier);
  }

  /**
   * Determine the behavioural of a class denoted by an identifier
   * showing a list of parameter types.
   *
   * @param uClass a (UML) class
   * @param identifier an identifier
   * @param parameterTypes a list of types
   * @return (UML) behavioural, or {@code null} if no matching behavioural can be found
   */
  public @Nullable UBehavioural getBehavioural(@Nullable UClass uClass, @Nullable Identifier identifier, @Nullable List<UType> parameterTypes, @Nullable UType returnType) {
    if (uClass == null || identifier == null || parameterTypes == null || returnType == null)
      return null;

    UDescriptor descriptor = UDescriptor.behavioural(uClass, identifier, parameterTypes);
    UElement element = this.elements.get(descriptor);
    if (element != null) {
      assert (element instanceof UBehavioural) :  "Element `" + element.getName() + "' should be a behavioural";
      return (UBehavioural)element;
    }

    if (this.collaboration == null && this.c1ass == null)
      return null;

    UType functionType = UType.function(parameterTypes, returnType);
    for (UOperation operation : uClass.getOperations()) {
      Identifier operationIdentifier = Identifier.id(operation.getName());
      if (operationIdentifier.matches(identifier) &&
          functionType.subsumes(operation.getType())) {
        this.elements.put(descriptor, operation);
        return operation;
      }
    }
    for (UReception reception : uClass.getReceptions()) {
      Identifier receptionIdentifier = Identifier.id(reception.getName());
      Identifier signalIdentifier = Identifier.id(reception.getSignal().getName());
      if ((receptionIdentifier.matches(identifier) || signalIdentifier.matches(identifier)) &&
          functionType.subsumes(reception.getType())) {
        elements.put(descriptor, reception);
        return reception;
      }
    }

    return null;
  }

  /**
   * Determine the state denoted by an identifier in a (UML) class.
   * 
   * @param uClass (UML) class
   * @param identifier an identifier
   * @return (UML) state, or {@code null} if no matching state can be found
   */
  public @Nullable UVertex getState(@Nullable UClass uClass, @Nullable Identifier identifier) {
    if (uClass == null || identifier == null)
      return null;

    UDescriptor descriptor = UDescriptor.state(uClass, identifier);
    UElement element = this.elements.get(descriptor);
    if (element != null) {
      assert (element instanceof UVertex) :  "Element `" + element.getName() + "' should be a vertex";
      return (UVertex)element;
    }

    UStateMachine uStateMachine = uClass.getStateMachine();
    if (uStateMachine == null)
      return null;

    for (UVertex uVertex : uStateMachine.getVertices()) {
      if (uVertex.getIdentifier().matches(identifier)) {
        elements.put(descriptor, uVertex);
        return uVertex;
      }
    }

    return null;
  }

  /**
   * @return context's class type or an error type if this context has no underlying class
   */
  public UType getThisType() {
    if (this.c1ass != null)
      return UType.simple(this.c1ass);
    return UType.error("No this element in context `" + this.description() + "'");
  }

  /**
   * Determine context's boolean type.
   *
   * @return boolean type
   */
  public UType getBooleanType() {
    return this.model.getBooleanType();
  }

  /**
   * Determine context's integer type.
   *
   * @return integer type
   */
  public UType getIntegerType() {
    return this.model.getIntegerType();
  }

  /**
   * Determine context's string type.
   *
   * @return string type
   */
  public UType getStringType() {
    return this.model.getStringType();
  }

  /**
   * Determine context's clock type.
   *
   * @return clock type
   */
  public UType getClockType() {
    return this.model.getClockType();
  }

  /**
   * Determine context's void type.
   *
   * @return void type
   */
  public UType getVoidType() {
    return this.model.getVoidType();
  }

  /**
   * Determine context's null type.
   *
   * @return null type
   */
  public UType getNullType() {
    return UType.nullType();
  }

  /**
   * @return context's description as a string
   */
  public String description() {
    StringBuilder result = new StringBuilder();

    result.append("model `");
    result.append(this.model.getName());
    result.append("'");
    UCollaboration uCollaboration = this.collaboration;
    if (uCollaboration != null) {
      result.append(", collaboration `");
      result.append(uCollaboration.getName());
      result.append("'");
      UInteraction uInteraction = this.interaction;
      if (uInteraction != null) {
        result.append(", interaction `");
        result.append(uInteraction.getName());
        result.append("'");
      }
    }
    UClassifier uClassifier = this.c1ass;
    if (uClassifier != null) {
      result.append(", class `");
      result.append(uClassifier.getName());
      result.append("'");
      UEvent uEvent = this.event;
      if (uEvent != null) {
        result.append(", event `");
        result.append(uEvent.getName());
        result.append("'");
      }
    }
    if (this.isConstant)
      result.append(", constant");

    return result.toString();
  }

  /**
   * @return context's string representation
   */
  public String toString() {
    StringBuilder result = new StringBuilder();

    result.append("Context [model = ");
    result.append(this.model.getName());
    UCollaboration uCollaboration = this.collaboration;
    if (uCollaboration != null) {
      result.append(", collaboration = ");
      result.append(uCollaboration.getName());
    }
    UInteraction uInteraction = this.interaction;
    if (uInteraction != null) {
      result.append(", interaction = ");
      result.append(uInteraction.getName());
    }
    UClassifier uClassifier = this.c1ass;
    if (uClassifier != null) {
      result.append(", class = ");
      result.append(uClassifier.getName());
    }
    UEvent uEvent = this.event;
    if (uEvent != null) {
      result.append(", event = ");
      result.append(uEvent.getName());
    }
    result.append(", constant = ");
    result.append(this.isConstant);
    result.append("]");

    return result.toString();
  }
}
