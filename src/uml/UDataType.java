package uml;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Contains data of a data type.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class UDataType extends UClassifier {
  /**
   * Create a new data type.
   *
   * @param name a string
   * @param model a UML model
   */
  public UDataType(String name, UModel model) {
    super(name, model);
  }

  /**
   * @return data type's type
   */
  public UType getType() {
    return UType.meta("DataType");
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("DataType [name = ");
    resultBuilder.append(getName());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
