package uml;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.statemachine.UEvent;
import uml.statemachine.UStateMachine;
import util.Formatter;


/**
 * UML class
 * 
 * @author <A HREF="mailto:majnu@gmx.net">Timm Schäfer</A>
 */
@NonNullByDefault
public class UClass extends UClassifier {
  private List<UAttribute> attributes = new ArrayList<>();
  private List<UOperation> operations = new ArrayList<>();
  private List<UReception> receptions = new ArrayList<>();
  private @Nullable UStateMachine stateMachine = null;

  /**
   * Create a (UML) class.
   *
   * @param name a string
   * @param model a UML model
   */
  public UClass(String name, UModel model) {
    super(name, model);
  }

  /**
   * Make a state machine for this class.
   *
   * @return the (UML) state machine created for this class
   */
  public UStateMachine setStateMachine() {
    UStateMachine stateMachine = new UStateMachine(this);
    this.stateMachine = stateMachine;
    return stateMachine;
  }

  /**
   * @return class's state machine
   */
  public @Nullable UStateMachine getStateMachine() {
    return this.stateMachine;
  }

  /**
   * Add an attribute to this classifier.
   * 
   * Only attributes with distinct names are kept.
   *
   * @param attribute an attribute
   */
  public void addAttribute(UAttribute attribute) {
    if (!this.attributes.stream().anyMatch(a -> attribute.getName().equals(a.getName())))
      this.attributes.add(attribute);
  }

  @Override
  public List<UAttribute> getAttributes() {
    return this.attributes;
  }

  /**
   * Retrieve an attribute of this class by its name.
   *
   * @param name a name
   * @return classifier's attribute named {@code name}
   */
  public @Nullable UAttribute getAttribute(final @Nullable String name) {
    return this.attributes.stream().filter(attribute -> attribute.getName().equals(name)).findAny().orElse(null);
  }

  /**
   * Add an operation to this class
   *
   * @param operation an <code>Operation</code>
   */
  public void addOperation(UOperation operation) {
    this.operations.add(operation);
  }

  @Override
  public List<UOperation> getOperations() {
    return this.operations;
  }

  /**
   * Add a reception to this class
   *
   * @param reception a <code>Reception</code>
   */
  public void addReception(UReception reception) {
    receptions.add(reception);
  }

  /**
   * @return class's receptions
   */
  public List<UReception> getReceptions() {
    return this.receptions;
  }

  /**
   * Determine (some) reception of this class for a given signal.
   * 
   * @param signal a UML signal
   * @return a reception for {@code signal} of this class; or {@code null}
   * if no such reception exists
   */
  public @Nullable UReception getReception(USignal signal) {
    return this.getReceptions().stream().filter(reception -> reception.getSignal().equals(signal)).findAny().orElse(null);
  }

  /**
   * @return class's behavioural (feature)s
   */
  public List<UBehavioural> getBehaviourals() {
    List<UBehavioural> behaviourals = new ArrayList<>();
    behaviourals.addAll(getOperations());
    behaviourals.addAll(getReceptions());
    return behaviourals;
  }

  /**
   * @return maximum number of parameters appearing in any behavioural of this class
   */
  public int getMaxParameters() {
    return this.getBehaviourals().stream().map(uBehavioural -> uBehavioural.getParameters().size()).reduce(0, (m1, m2) -> Integer.max(m1, m2));
  }

  /**
   * @return set of events this class can react to
   */
  public Set<UEvent> getEvents() {
    var stateMachine = this.stateMachine;
    if (stateMachine == null)
      return new HashSet<>();
    return stateMachine.getEvents();
  }

  /**
   * Make a call or signal event for this class given the name of an
   * operation resp. a reception.
   *
   * @param name name of an operation or a reception
   * @return call or signal event for {@code name}, or {@code null} if {@code name}
   * does not occur as the name of an operation or reception of this class
   */
  public @Nullable UEvent makeEvent(String name) {
    for (UOperation operation : this.getOperations()) {
      if (operation.getName().equals(name))
        return UEvent.call(operation);
    }

    for (UReception reception : this.getReceptions()) {
      if (reception.getName().equals(name))
        return UEvent.signal(reception.getSignal());
    }

    return null;
  }

  /**
   * Determine the behavioural of this class matched by a given event.
   *
   * @param event an event
   * @return the behavioural of this class matching {@code event}, or {@code null} if there is none
   */
  public @Nullable UBehavioural getBehavioural(UEvent event) {
    return event.new Cases<@Nullable UBehavioural>().otherwise(() -> null).
        call(uOperation -> this.getOperations().contains(uOperation) ? uOperation : null).
        signal(uSignal -> this.getReception(uSignal)).
        apply();
  }

  /**
   * @return initial value for this class (i.e., the expression "null")
   */
  @Override
  public UExpression getInitialValue() {
    return UExpression.nullConst();
  }

  /**
   * @param prefix prefix string
   * @return class's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";
    String next2Prefix = nextPrefix + "  ";
    boolean pending = false;

    resultBuilder.append(prefix);
    resultBuilder.append("class ");
    resultBuilder.append(this.getName());
    resultBuilder.append(" {\n");

    if (!this.getAttributes().isEmpty() || !this.getOperations().isEmpty() || !this.getReceptions().isEmpty()) {
      resultBuilder.append(nextPrefix);
      resultBuilder.append("signature {\n");

      if (!this.getAttributes().isEmpty()) {
        pending = true;
        resultBuilder.append(Formatter.separated(this.getAttributes(), uAttribute -> uAttribute.declaration(next2Prefix), "\n"));
      }

      if (!this.getOperations().isEmpty()) {
        if (pending)
          resultBuilder.append("\n\n");
        pending = true;
        resultBuilder.append(Formatter.separated(this.getOperations(), uOperation -> uOperation.declaration(next2Prefix), "\n"));
      }

      if (!this.getReceptions().isEmpty()) {
        if (pending)
          resultBuilder.append("\n\n");
        pending = true;
        resultBuilder.append(Formatter.separated(this.getReceptions(), uReception -> uReception.declaration(next2Prefix), "\n"));
      }

      resultBuilder.append("\n");
      resultBuilder.append(nextPrefix);
      resultBuilder.append("}");
    }

    UStateMachine stateMachine = this.getStateMachine();
    if (stateMachine != null) {
      if (pending)
        resultBuilder.append("\n\n");
      pending = true;
      resultBuilder.append(nextPrefix);
      resultBuilder.append("behaviour {\n");
      resultBuilder.append(stateMachine.declaration(next2Prefix));
      resultBuilder.append("\n");
      resultBuilder.append(nextPrefix);
      resultBuilder.append("}");
    }

    if (pending)
      resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  /**
   * @return class's representation in UTE format
   */
  public String declaration() {
    return this.declaration("");
  }

  /**
   * @return class's string representation
   */
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Class [name = ");
    resultBuilder.append(getName());
    resultBuilder.append(", #attributes = ");
    resultBuilder.append(getAttributes().size());
    resultBuilder.append(", #operations = ");
    resultBuilder.append(getOperations().size());
    resultBuilder.append(", #receptions = ");
    resultBuilder.append(getReceptions().size());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
