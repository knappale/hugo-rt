package uml;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;


/**
 * Contains data of a (UML) object.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UObject extends UStatual {
  private UClass c1ass;
  private List<USlot> uSlots = new ArrayList<>();

  /**
   * Create a new named object of a given class.
   *
   * @param name a name
   * @param c1ass a class
   * @return a new object
   */
  public UObject(String name, UClass c1ass) {
    super(name);
    this.c1ass = c1ass;
  }

  @Override
  @NonNullByDefault({})
  public <T> T cases(@NonNull Function<@NonNull UAttribute, T> attributeFun,
                     @NonNull Function<@NonNull UParameter, T> parameterFun,
                     @NonNull Function<@NonNull UObject, T> objectFun) {
    return objectFun.apply(this);
  }

  /**
   * Determine object's class
   *
   * @return object's class
   */
  public UClass getC1ass() {
    return this.c1ass;
  }

  /**
   * Add a slot to the object.
   *
   * @param slot a slot
   */
  public void addSlot(USlot slot) {
    uSlots.add(slot);
  }

  /**
   * Determine object's slots.
   *
   * @return object's slots
   */
  public List<USlot> getSlots() {
    return this.uSlots;
  }

  /**
   * Determine a slot in the object by attribute.
   *
   * @return object's slot for {@code attribute}, or {@code null} if there is no such slot
   */
  public @Nullable USlot getSlot(UAttribute attribute) {
    for (USlot slot : this.uSlots) {
      if (slot.getAttribute().equals(attribute))
        return slot;
    }
    return null;
  }

  @Override
  public boolean isConstant() {
    return true;
  }

  @Override
  public boolean isStatic() {
    return false;
  }

  /**
   * @return object's type
   */
  public UType getType() {
    return UType.simple(this.getC1ass());
  }

  /**
   * @param prefix prefix string
   * @return model's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";
    
    resultBuilder.append(prefix);
    resultBuilder.append("object ");
    resultBuilder.append(getName());
    resultBuilder.append(" : ");
    resultBuilder.append(this.c1ass.getName());
    resultBuilder.append(" {\n");
    for (USlot uSlot : this.uSlots) {
      resultBuilder.append(uSlot.declaration(nextPrefix));
      resultBuilder.append("\n");
    }
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  /**
   * @return object's representation in UTE format
   */
  public String declaration() {
    return this.declaration("");
  }
 
  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Object [name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append(", class = ");
    resultBuilder.append(this.c1ass.getName());
    resultBuilder.append(", slots = ");
    resultBuilder.append(Formatter.set(this.uSlots));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
