package uml.run;

import java.util.*;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UObject;


/**
 * State of a UML system.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class USystemState {
  private UNetworkState networkState = new UNetworkState(this);
  private Map<UObject, UObjectState> objectStates = new HashMap<>();
  
  public USystemState() {
  }

  public UNetworkState getNetworkState() {
    return this.networkState;
  }

  public UObjectState getObjectState(UObject object) {
    var objectState = objectStates.get(object);
    if (objectState != null)
      return objectState;

    objectState = new UObjectState(this, object);
    this.objectStates.put(object, objectState);
    return objectState;
  }

  public List<UObjectState> getDifferingObjectStates(USystemState other) {
    var difference = new ArrayList<UObjectState>();
    for (var object : this.objectStates.keySet()) {
      var objectState = this.getObjectState(object);
      if (!objectState.equals(other.getObjectState(object)))
        difference.add(objectState);
    }
    return difference;
  }

  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String separator = "";

    resultBuilder.append(prefix);
    resultBuilder.append("system {\n");
    if (!this.networkState.isEmpty()) {
      resultBuilder.append(networkState.declaration(prefix + "  "));
      resultBuilder.append("\n\n");
    }
    separator = "";
    for (var object : this.objectStates.keySet()) {
      resultBuilder.append(separator);
      var objectState = Objects.requireNonNull(this.objectStates.get(object));
      resultBuilder.append(objectState.declaration(prefix + "  "));
      resultBuilder.append("\n");
      separator = "\n";
    }
    resultBuilder.append(prefix);
    resultBuilder.append("}");
    return resultBuilder.toString();
  }

  public String declaration() {
    return this.declaration("");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.objectStates,
                        this.networkState);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;

    try {
      USystemState other = (USystemState)object;
      return Objects.equals(this.objectStates, other.objectStates) &&
             Objects.equals(this.networkState, other.networkState);
    }
    catch (ClassCastException cce) {
      return false;
    }    
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("SystemState [networkState = ");
    resultBuilder.append(this.networkState);
    resultBuilder.append(", objectStates = ");
    resultBuilder.append(util.Formatter.map(this.objectStates));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
