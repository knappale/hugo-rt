package uml.run;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * A UML run.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class URun {
  private List<USystemState> systemStates = new ArrayList<>();
  
  public URun() {
  }
  
  public void addSystemState(USystemState systemState) {
    this.systemStates.add(systemState);
  }

  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    int n = this.systemStates.size();
    if (n == 0)
      return resultBuilder.toString();

    int number = 1;
    resultBuilder.append(prefix);
    var systemState = this.systemStates.get(0);
    resultBuilder.append("-- " + number++ + " ---------------------\n");
    resultBuilder.append(systemState.declaration(prefix));
    resultBuilder.append("\n");
    for (int i = 1; i < n; i++) {
      var systemStateOld = systemState;
      systemState = this.systemStates.get(i);
      if (systemStateOld.equals(systemState))
        continue;

      resultBuilder.append("-- " + number++ + " ---------------------\n");
      var difference = systemState.getDifferingObjectStates(systemStateOld);
      if (difference == null || difference.isEmpty())
        resultBuilder.append(systemState.declaration(prefix));
      else {
        for (var differenceIt = difference.iterator(); differenceIt.hasNext(); ) {
          var objectState = differenceIt.next();
          resultBuilder.append(objectState.declaration(prefix));
          if (differenceIt.hasNext())
            resultBuilder.append("\n\n");
        }
      }
      resultBuilder.append("\n");
      var networkState = systemState.getNetworkState();
      if (!networkState.isEmpty()) {
        resultBuilder.append("\n");
        resultBuilder.append(networkState.declaration(prefix));
        resultBuilder.append("\n");
      }
    }

    return resultBuilder.toString();
  }

  public String declaration() {
    return this.declaration("");
  }

  public String toString() {
    var resultBuilder = new StringBuilder();
    resultBuilder.append("Run [systemStates = ");
    resultBuilder.append(util.Formatter.list(this.systemStates));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
