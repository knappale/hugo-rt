package uml.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UObject;
import uml.UAttribute;
import uml.UExpression;
import uml.statemachine.UEvent;
import uml.statemachine.semantics.UConfiguration;
import uml.statemachine.semantics.UVertexForest;
import util.Formatter;


/**
 * State of an object.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UObjectState {
  private USystemState systemState;
  private UObject object;
  private Map<UAttribute, List<UExpression>> attributeValues = new HashMap<>();
  private UQueueState queueState = new UQueueState();
  private @Nullable UVertexForest stateForest = null;
  private @Nullable UConfiguration configuration = null;
  private Map<UEvent, Integer> timers = new HashMap<>();
  private @Nullable UEvent stimulus = null;
  private List<UExpression> arguments = new ArrayList<>();
  private @Nullable UObject sender = null;

  /**
   * Create an object state for a given UML object.
   * 
   * @param systemState a UML system state
   * @param object a UML object
   */
  public UObjectState(USystemState systemState, UObject object) {
    this.object = object;
    this.systemState = systemState;
  }

  public UObject getObject() {
    return this.object;
  }

  public USystemState getSystemState() {
    return this.systemState;
  }

  public void addAttributeValue(UAttribute attribute, UExpression value) {
    List<UExpression> values = new ArrayList<>();
    values.add(value);
    this.attributeValues.put(attribute, values);
  }
  
  public void addAttributeValues(UAttribute attribute, List<UExpression> values) {
    this.attributeValues.put(attribute, values);
  }

  /**
   * @return queue state of this (UML) object
   */
  public UQueueState getQueueState() {
    return this.queueState;
  }
  
  public void setStimulus(UEvent stimulus, List<UExpression> arguments, @Nullable UObject sender) {
    this.stimulus = stimulus;
    this.arguments.addAll(arguments);
    this.sender = sender;
  }

  public void setStateForest(UVertexForest stateForest) {
    this.stateForest = stateForest;
  }

  public void setConfiguration(UConfiguration configuration) {
    this.configuration = configuration;
  }

  public void addTimer(UEvent event, int value) {
    this.timers.put(event, value);
  }

  private boolean inTransit() {
    var configuration = this.configuration;
    return (configuration == null || (configuration.isPseudo() && !configuration.isInitial()));
  }

  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    boolean pending = false;

    resultBuilder.append(prefix);
    resultBuilder.append("object ");
    resultBuilder.append(this.object.getName());
    resultBuilder.append(" {\n");

    resultBuilder.append(prefix + "  ");
    resultBuilder.append("attributes {\n");
    for (var attribute : this.attributeValues.keySet()) {
      resultBuilder.append(prefix + "    ");
      resultBuilder.append(attribute.getName());
      resultBuilder.append(" = ");
      resultBuilder.append(Formatter.setOrSingleton(this.attributeValues.get(attribute)));
      resultBuilder.append(";");
      resultBuilder.append("\n");
    }
    resultBuilder.append(prefix + "  ");
    resultBuilder.append("}\n");
    pending = true;

    if (pending) {
      resultBuilder.append("\n");
      pending = false;
    }
    resultBuilder.append(this.queueState.declaration(prefix + "  "));
    resultBuilder.append("\n");
    pending = true;

    if (pending) {
      resultBuilder.append("\n");
      pending = false;
    }
    resultBuilder.append(prefix + "  ");
    resultBuilder.append("configuration {\n");
    resultBuilder.append(prefix + "    ");
    if (inTransit()) {
      var stateForest = this.stateForest;
      resultBuilder.append("in transit");
      if (stateForest != null) {
        resultBuilder.append(" (");
        resultBuilder.append(stateForest);
        resultBuilder.append(")");
      }
    }
    else {
      var configuration = this.configuration;
      if (configuration != null)
      resultBuilder.append(configuration.declaration());
    }
    resultBuilder.append(";\n\n");
    resultBuilder.append(prefix + "    ");
    resultBuilder.append("timers {\n");
    for (UEvent timeEvent : timers.keySet()) {
      Integer timerValue = this.timers.get(timeEvent);
      resultBuilder.append(prefix + "      ");
      resultBuilder.append(timeEvent.getName());
      resultBuilder.append(" = ");
      resultBuilder.append(timerValue);
      resultBuilder.append(";\n");
    }
    resultBuilder.append(prefix + "    ");
    resultBuilder.append("}\n");
    
    resultBuilder.append(prefix + "  ");
    resultBuilder.append("}\n");
    pending = true;      

    var stimulus = this.stimulus;
    if (stimulus != null) {
      if (pending) {
        resultBuilder.append("\n");
        pending = false;
      }
      resultBuilder.append(prefix + "  ");
      resultBuilder.append("stimulus {\n");
      resultBuilder.append(prefix + "    ");
      resultBuilder.append(stimulus.getName());
      if (stimulus.isBehavioural()) {
        resultBuilder.append("(");
        resultBuilder.append(Formatter.separated(this.arguments, ", "));
        resultBuilder.append(")");
        var sender = this.sender;
        if (sender != null) {
          resultBuilder.append(" (from ");
          resultBuilder.append(sender.getName());
          resultBuilder.append(")");
        }
      }
      resultBuilder.append(";\n");
      resultBuilder.append(prefix + "  ");
      resultBuilder.append("}\n");
    }

    resultBuilder.append(prefix);
    resultBuilder.append("}");
    return resultBuilder.toString();
  }

  public String declaration() {
    return this.declaration("");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.object,
                        this.attributeValues,
                        this.queueState,
                        this.configuration,
                        this.timers,
                        this.stimulus,
                        this.arguments,
                        this.sender);
  }

  @Override
  public boolean equals(java.lang.@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UObjectState other = (UObjectState)object;
      return Objects.equals(this.object, other.object) &&
             Objects.equals(this.attributeValues, other.attributeValues) &&
             Objects.equals(this.queueState, other.queueState) &&
             ((this.inTransit() && other.inTransit()) || Objects.equals(this.configuration, other.configuration)) &&
             Objects.equals(this.timers, other.timers) &&
             Objects.equals(this.stimulus, other.stimulus) &&
             Objects.equals(this.arguments, other.arguments) &&
             Objects.equals(this.sender, other.sender);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("ObjectState [object =");
    resultBuilder.append(object);
    resultBuilder.append(", attributeValues = [");
    for (Iterator<UAttribute> attributesIt = attributeValues.keySet().iterator(); attributesIt.hasNext();) {
      UAttribute attribute = attributesIt.next();
      resultBuilder.append(attribute.getName());
      resultBuilder.append(" = ");
      resultBuilder.append(attributeValues.get(attribute));
      if (attributesIt.hasNext())
        resultBuilder.append(", ");
    }
    resultBuilder.append(", queueState = ");
    resultBuilder.append(queueState);
    resultBuilder.append(", configuration = ");
    resultBuilder.append(configuration);
    resultBuilder.append(", timers = [");
    for (Iterator<UEvent> timersIt = timers.keySet().iterator(); timersIt.hasNext(); ) {
      UEvent timeEvent = timersIt.next();
      Integer timerValue = timers.get(timeEvent);
      resultBuilder.append(timeEvent.getName());
      resultBuilder.append(" = ");
      resultBuilder.append(timerValue);
      if (timersIt.hasNext())
        resultBuilder.append(", ");
    }
    resultBuilder.append("], stimulus = ");
    resultBuilder.append(stimulus);
    resultBuilder.append(", arguments = [");
    resultBuilder.append(Formatter.separated(this.arguments, ", "));
    resultBuilder.append("], sender = ");
    resultBuilder.append(sender);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
