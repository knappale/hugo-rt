package uml.run;

import java.util.*;

import uml.interaction.UMessage;
import uml.statemachine.UEvent;


/**
 * State of a queue.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class UQueueState {
  private List<UEvent> internalEvents = new ArrayList<UEvent>();
  private List<UMessage> externalMessages = new ArrayList<UMessage>();
  private List<UMessage> deferredMessages = new ArrayList<UMessage>();
  private boolean isInternalOverflown = false;
  private boolean isExternalOverflown = false;
  private boolean isDeferredOverflown = false;
  
  public UQueueState() {
  }
  
  public void addInternalEvent(UEvent event) {
    this.internalEvents.add(event);
  }
  
  public void addExternalMessage(UMessage message) {
    this.externalMessages.add(message);
  }
  
  public void addDeferredMessage(UMessage message) {
    this.deferredMessages.add(message);
  }

  public void setInternalOverflown(boolean isOverflown) {
    this.isInternalOverflown = isOverflown;
  }

  public void setExternalOverflown(boolean isOverflown) {
    this.isExternalOverflown = isOverflown;
  }

  public void setDeferredOverflown(boolean isOverflown) {
    this.isDeferredOverflown = isOverflown;
  }

  public boolean isEmpty() {
    return this.internalEvents.isEmpty() &&
           this.externalMessages.isEmpty() &&
           this.deferredMessages.isEmpty() &&
           !this.isInternalOverflown &&
           !this.isExternalOverflown &&
           !this.isDeferredOverflown;
  }

  private String eventsDeclaration(String prefix, String name, List<UEvent> events, boolean isOverflown) {
    StringBuffer result = new StringBuffer();
    result.append(prefix);
    result.append("  ");
    result.append(name);
    result.append(" {\n");
    if (isOverflown) {
      result.append(prefix);
      result.append("    ");
      result.append("overflow;");
      result.append(";");
    }
    else {
      for (Iterator<UEvent> eventsIt = events.iterator(); eventsIt.hasNext();) {
        UEvent event = eventsIt.next();
        result.append(prefix);
        result.append("    ");
        result.append(event.getName());
        result.append(";\n");
      }
    }
    result.append(prefix);
    result.append("  }");
    return result.toString();
  }
  
  private String messagesDeclaration(String prefix, String name, List<UMessage> messages, boolean isOverflown) {
    StringBuffer result = new StringBuffer();
    result.append(prefix);
    result.append("  ");
    result.append(name);
    result.append(" {\n");
    if (isOverflown) {
      result.append(prefix);
      result.append("    ");
      result.append("overflow;");
      result.append("\n");
    }
    else {
      for (Iterator<UMessage> messagesIt = messages.iterator(); messagesIt.hasNext();) {
        UMessage message = messagesIt.next();
        result.append(prefix);
        result.append("    ");
        result.append(message.declaration());
        result.append("\n");
      }
    }
    result.append(prefix);
    result.append("  }");
    return result.toString();
  }
  
  public String declaration(String prefix) {
    StringBuffer result = new StringBuffer();
    result.append(prefix);
    result.append("queues {\n");
    result.append(this.eventsDeclaration(prefix, "internal", this.internalEvents, isInternalOverflown));
    result.append("\n");
    result.append(this.messagesDeclaration(prefix, "external", this.externalMessages, isExternalOverflown));
    result.append("\n");
    result.append(this.messagesDeclaration(prefix, "deferred", this.deferredMessages, isDeferredOverflown));
    result.append("\n");
    result.append(prefix);
    result.append("}");
    return result.toString();
  }

  public String declaration() {
    return this.declaration("");
  }

  public int hashCode() {
    return (this.internalEvents == null ? 0 : this.internalEvents.hashCode()) +
           (this.externalMessages == null ? 0 : this.externalMessages.hashCode()) +
           (this.deferredMessages == null ? 0 : this.deferredMessages.hashCode());
  }

  public boolean equals(java.lang.Object object) {
    if (object == null || !(object instanceof UQueueState))
      return false;
    UQueueState other = (UQueueState)object;
    return (this.internalEvents == null ? other.internalEvents == null : this.internalEvents.equals(other.internalEvents)) &&
           (this.externalMessages == null ? other.externalMessages == null : this.externalMessages.equals(other.externalMessages)) &&
           (this.deferredMessages == null ? other.deferredMessages == null : this.deferredMessages.equals(other.deferredMessages));
  }

  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("QueueState [internalEvents = [");
    for (Iterator<UEvent> eventsIt = internalEvents.iterator(); eventsIt.hasNext();) {
      UEvent event = eventsIt.next();
      result.append(event);
      if (eventsIt.hasNext())
        result.append(", ");
    }
    result.append("], externalMessage = [");
    for (Iterator<UMessage> messagesIt = externalMessages.iterator(); messagesIt.hasNext();) {
      UMessage message = messagesIt.next();
      result.append(message);
      if (messagesIt.hasNext())
        result.append(", ");
    }
    result.append("], deferredMessages = [");
    for (Iterator<UMessage> messagesIt = deferredMessages.iterator(); messagesIt.hasNext();) {
      UMessage message = messagesIt.next();
      result.append(message);
      if (messagesIt.hasNext())
        result.append(", ");
    }
    result.append("]]");
    return result.toString();
  }
}
