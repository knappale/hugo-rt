package uml.run;

import java.util.*;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UObject;
import uml.interaction.UMessage;


/**
 * State of the network.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UNetworkState {
  private USystemState systemState;
  private List<UMessage> messages = new ArrayList<UMessage>();
  private boolean isOverflown = false;
  private Map<UObject, List<UMessage>> objectMessages = new HashMap<UObject, List<UMessage>>();
  private Map<UObject, Boolean> objectOverflowns = new HashMap<UObject, Boolean>();
  
  /**
   * Create a network state.
   * 
   * @param systemState a surrounding system state
   * @pre systemState != null
   */
  public UNetworkState(USystemState systemState) {
    this.systemState = systemState;
  }
 
  public boolean isEmpty() {
    if (!this.messages.isEmpty())
      return false;
    if (this.isOverflown)
      return false;
    if (this.objectMessages.values().stream().anyMatch(msgs -> !msgs.isEmpty()))
      return false;
    if (this.objectOverflowns.values().stream().anyMatch(b -> b))
      return false;
    return true;
  }

  public void addMessage(UMessage message) {
    this.messages.add(message);
  }

  public void addMessage(UObject object, UMessage message) {
    List<UMessage> messages = this.objectMessages.get(object);
    if (messages == null)
      messages = new ArrayList<UMessage>();
    messages.add(message);
    this.objectMessages.put(object, messages);
  }

  public void setOverflown() {
    this.isOverflown = true;
  }

  public void setOverflown(UObject object) {
    this.objectOverflowns.put(object, true);
  }

  public String declaration(String prefix) {
    var resultBuilder = new StringBuilder();
    resultBuilder.append(prefix);
    resultBuilder.append("network {\n");
    for (var message : this.messages) {
      resultBuilder.append(prefix);
      resultBuilder.append("  ");
      resultBuilder.append(message.declaration());
      resultBuilder.append("\n");
    }
    if (this.isOverflown) {
      resultBuilder.append(prefix);
      resultBuilder.append("  overflow;\n");
    }
    var uObjects = new HashSet<UObject>();
    uObjects.addAll(this.objectMessages.keySet());
    uObjects.addAll(this.objectOverflowns.keySet());
    for (var uObject : uObjects) {
      var uMessages = this.objectMessages.get(uObject);
      if (uMessages == null)
        uMessages = new ArrayList<UMessage>();
      for (var uMessage : uMessages) {
        resultBuilder.append(prefix);
        resultBuilder.append("  ");
        resultBuilder.append(uMessage.declaration());
        resultBuilder.append("\n");
      }
      var objectOverflown = this.objectOverflowns.get(uObject);
      if (objectOverflown != null && objectOverflown.booleanValue()) {
        resultBuilder.append(prefix);
        resultBuilder.append("  overflow (");
        resultBuilder.append(uObject.getName());
        resultBuilder.append(");\n");
      }
    }
    resultBuilder.append(prefix);
    resultBuilder.append("}");
    return resultBuilder.toString();
  }

  public String declaration() {
    return this.declaration("");
  }

  public int hashCode() {
    return Objects.hash(this.messages,
                        this.isOverflown,
                        this.objectMessages,
                        this.objectOverflowns);
  }

  public boolean equals(java.lang.@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UNetworkState other = (UNetworkState)object;
      return Objects.equals(this.messages, other.messages) &&
             this.isOverflown == other.isOverflown &&
             Objects.equals(this.objectMessages, other.objectMessages) &&
             Objects.equals(this.objectOverflowns, other.objectOverflowns);
    }
    catch (ClassCastException cce) {
    }
    return false;
  }

  public String toString() {
    var resultBuilder = new StringBuilder();
    resultBuilder.append("NetworkState [systemState = ");
    resultBuilder.append(this.systemState.hashCode());
    resultBuilder.append(", messages = ");
    resultBuilder.append(util.Formatter.list(this.messages));
    resultBuilder.append(", isOverflown = ");
    resultBuilder.append(this.isOverflown);
    resultBuilder.append(", objectMessages = ");
    resultBuilder.append(util.Formatter.map(this.objectMessages));
    resultBuilder.append(", objectOverflowns = ");
    resultBuilder.append(util.Formatter.map(this.objectOverflowns));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
