package uml.ocl;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UObject;
import uml.UAttribute;
import uml.UContext;
import uml.UType;
import uml.statemachine.UVertex;
import util.Formatter;
import util.Identifier;

import static util.Objects.requireNonNull;
import static util.Formatter.context;
import static util.Formatter.quoted;
import static util.Formatter.type;


/**
 * OCL expression
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class OExpression {
  enum Kind {
    BOOLEAN,
    INTEGER,
    NULL,
    ID,
    STATE,
    ARRAY,
    UNARY,
    ARITHMETICAL,
    RELATIONAL;
  }

  private boolean hasPrecedence(OExpression other) {
    if ((this.kind == Kind.UNARY || this.kind == Kind.ARITHMETICAL || this.kind == Kind.RELATIONAL) &&
        (other.kind == Kind.UNARY || other.kind == Kind.ARITHMETICAL || other.kind == Kind.RELATIONAL)) {
      return OOperator.precedes(requireNonNull(this.op), requireNonNull(other.op));
    }
    else
      return true;
  }

  private Kind kind;
  private @Nullable Identifier object = null;
  private @Nullable Identifier qualifier = null;
  private @Nullable Identifier identifier = null;
  private int intConst;
  private @Nullable OExpression leftExpression = null;
  private @Nullable OOperator op = null;
  private @Nullable OExpression rightExpression = null;

  private static final OExpression TRUEEXPRESSION;
  private static final OExpression FALSEEXPRESSION;
  static {
    TRUEEXPRESSION = new OExpression(Kind.BOOLEAN);
    TRUEEXPRESSION.op = OOperator.TRUE;
    FALSEEXPRESSION = new OExpression(Kind.BOOLEAN);
    FALSEEXPRESSION.op = OOperator.FALSE;
  }

  public interface InContextVisitor<T> {
    public UContext getContext();
  
    public T onBooleanConstant(boolean booleanConstant);
    public T onIntegerConstant(int integerConstant);
    public T onNullConstant();
    public T onReference(UAttribute uAttribute);
    public T onReference(UObject uObject, UAttribute uAttribute);
    public T onArrayReference(UAttribute uAttribute, OExpression offset);
    public T onArrayReference(UObject uObject, UAttribute uAttribute, OExpression offset);
    public T onState(UObject uObject, UVertex uVertex);
    public T onUnary(OOperator op, OExpression expression);
    public T onArithmetical(OExpression left, OOperator op, OExpression right);
    public T onRelational(OExpression left, OOperator op, OExpression right);
  }

  private OExpression(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create an expression which represents a boolean constant.
   *
   * @param booleanConstant boolean constant
   * @return an expression
   */
  public static OExpression boolConst(boolean booleanConstant) {
    return (booleanConstant ? TRUEEXPRESSION : FALSEEXPRESSION);
  }

  /**
   * Create an expression which represents <CODE>true</CODE>.
   *
   * @return an expression
   */
  public static OExpression trueConst() {
    return TRUEEXPRESSION;
  }

  /**
   * Create an expression which represents <CODE>false</CODE>.
   *
   * @return an expression
   */
  public static OExpression falseConst() {
    return FALSEEXPRESSION;
  }

  /**
   * Create an expression which represents an integer constant.
   *
   * @param integerConstant integer
   * @return an expression
   */
  public static OExpression intConst(int integerConstant) {
    OExpression e = new OExpression(Kind.INTEGER);
    e.intConst = integerConstant;
    return e;
  }

  /**
   * Create an expression which represents <CODE>null</CODE>
   *
   * @return an expression
   */
  public static OExpression nullConst() {
    OExpression e = new OExpression(Kind.NULL);
    return e;
  }

  /**
   * Create an expression which represents an identifier
   *
   * @param identifier identifier
   * @return an expression
   */
  public static OExpression id(Identifier qualifier, Identifier identifier) {
    OExpression e = new OExpression(Kind.ID);
    e.qualifier = qualifier;
    e.identifier = identifier;
    return e;
  }

  /**
   * Create an expression which represents an array retrieval
   *
   * @param identifier array identifier
   * @param expression offset expression
   * @return an expression
   */
  public static OExpression array(Identifier qualifier, Identifier identifier, OExpression expression) {
    OExpression e = new OExpression(Kind.ARRAY);
    e.qualifier = qualifier;
    e.identifier = identifier;
    e.leftExpression = expression;
    return e;
  }

  /**
   * Create an expression which represents access to a model state.
   *
   * @param object object identifier
   * @param identifier state identifier
   * @return an expression
   */
  public static OExpression state(Identifier object, Identifier identifier) {
    OExpression e = new OExpression(Kind.STATE);
    e.object = object;
    e.identifier = identifier;
    return e;
  }

  /**
   * Create an expression which represents a unary operation
   *
   * @param unary unary operation (i.e. {@link #UPLUS UPLUS}, or {@link #UMINUS UMINUS})
   * @param expression expression
   * @return an expression
   */
  public static OExpression unary(OOperator unary, OExpression expression) {
    if (unary == OOperator.UPLUS)
      return expression;
    if (unary == OOperator.UMINUS) {
      if (expression.kind == Kind.UNARY &&
          expression.op == OOperator.UMINUS)
        return requireNonNull(expression.leftExpression);
    }
    OExpression e = new OExpression(Kind.UNARY);
    e.op = unary;
    e.leftExpression = expression;
    return e;
  }

  /**
   * Create an expression which represents an arithmetical expression
   *
   * @param leftExpression left expression
   * @param op binary operation (i.e. {@link #ADD ADD}, {@link #SUB SUB}, {@link #MULT MULT}, {@link #DIV DIV}, {@link #MOD MOD})
   * @param rightExpression right expression
   * @return an expression
   */
  public static OExpression arithmetical(OExpression leftExpression, OOperator op, OExpression rightExpression) {
    OExpression e = new OExpression(Kind.ARITHMETICAL);
    e.leftExpression = leftExpression;
    e.op = op;
    e.rightExpression = rightExpression;
    return e;
  }

  /**
   * Create an expression which represents a relational expression.
   *
   * @param leftExpression left expression
   * @param op binary relation
   * @param rightExpression right expression
   * @return an expression
   */
  public static OExpression relational(OExpression leftExpression, OOperator op, OExpression rightExpression) {
    OExpression e = new OExpression(Kind.RELATIONAL);
    e.leftExpression = leftExpression;
    e.op = op;
    e.rightExpression = rightExpression;
    return e;
  }

  /**
   * Determine the type of the expression.
   *
   * @param context expression's context
   * @return expression's type
   */
  public UType getType(UContext context) {
    switch (this.kind) {
      case BOOLEAN:
        return context.getBooleanType();

      case INTEGER:
        return context.getIntegerType();

      case NULL:
        return context.getNullType();

      case ID: {
        Identifier qualifier = requireNonNull(this.qualifier);
        Identifier identifier = requireNonNull(this.identifier);

        Identifier qualifiedIdentifier = Identifier.id(qualifier, identifier);
        UAttribute attribute = context.getAttribute(qualifiedIdentifier);
        if (attribute == null)
          return UType.error("No matching element found for " + context(qualifiedIdentifier, context.description()));
        UType qualifiedIdentifierType = attribute.getType();
        if (qualifiedIdentifierType.isArray())
          return UType.error("non-array access to array identifier " + type(qualifiedIdentifier, qualifiedIdentifierType));
        return qualifiedIdentifierType;
      }

      case ARRAY:
        Identifier qualifier = requireNonNull(this.qualifier);
        Identifier identifier = requireNonNull(this.identifier);
        OExpression offset = requireNonNull(this.leftExpression);

        Identifier qualifiedIdentifier = Identifier.id(qualifier, identifier);
        UAttribute attribute = context.getAttribute(qualifiedIdentifier);
        if (attribute == null)
          return UType.error("No matching element found for " + context(qualifiedIdentifier, context.description()));
        UType qualifiedIdentifierType = attribute.getType();
        if (!qualifiedIdentifierType.isArray())
          return UType.error("array access to non-array identifier " + type(qualifiedIdentifier, qualifiedIdentifierType));
        UType underlyingType = qualifiedIdentifierType.getUnderlyingType();
        UType offsetType = offset.getType(context);
        if (!context.getIntegerType().subsumes(offsetType))
          return UType.error("non-integer array offset " + type(offset, offsetType));
        return underlyingType;
        
      case STATE: {
        uml.UObject object = context.getObject(this.object);
        if (object == null)
          return UType.error("unknown \"object\" " + quoted(this.object));
        UVertex vertex = context.getState(object.getC1ass(), this.identifier);
        if (vertex == null)
          return UType.error("no state " + quoted(this.identifier) + " in state machine for object " + quoted(object.getName()));
        return context.getBooleanType();
      }
        
      case UNARY: {
        OOperator op = requireNonNull(this.op);
        OExpression inner = requireNonNull(this.leftExpression);
        UType innerType = inner.getType(context);
        if (innerType.isError())
          return innerType;
        switch (op) {
          case UPLUS:
          case UMINUS:
            if (!context.getIntegerType().subsumes(innerType))
              return UType.error(quoted(op.getName()) + " applied to non-integer expression " + type(inner, innerType));
            return innerType;
          //$CASES-OMITTED$
          default:
            return UType.error("[illegal unary operator]");
        }
      }
        
      case ARITHMETICAL: {
        OExpression leftExpression = requireNonNull(this.leftExpression);
        OOperator op = requireNonNull(this.op);
        OExpression rightExpression = requireNonNull(this.rightExpression);

        UType leftType = leftExpression.getType(context);
        if (leftType.isError())
          return leftType;
        UType rightType = rightExpression.getType(context);
          if (rightType.isError())
            return rightType;
          switch (op) {
            case ADD:
            case SUB:
            case MULT:
            case DIV:
            case MOD:
              if (!context.getIntegerType().subsumes(leftType))
                return UType.error("non-integer type in arithmetical expression " + type(leftExpression, leftType));
              if (!context.getIntegerType().subsumes(rightType))
                return UType.error("non-integer type in arithmetical expression " + type(rightExpression, rightType));
              return leftType;
            //$CASES-OMITTED$
            default:
              return UType.error("[illegal arithmetical operator]");
          }
      }

      case RELATIONAL: {
        OExpression leftExpression = requireNonNull(this.leftExpression);
        OOperator op = requireNonNull(this.op);
        OExpression rightExpression = requireNonNull(this.rightExpression);

        UType leftType = leftExpression.getType(context);
        if (leftType.isError())
          return leftType;
        UType rightType = rightExpression.getType(context);
        if (rightType.isError())
          return rightType;
        switch (op) {
          case LT:
          case LEQ:
          case GEQ:
          case GT:
            if (!context.getIntegerType().subsumes(leftType))
              return UType.error("non-integer type in comparison " + type(leftExpression, leftType));
            if (!context.getIntegerType().subsumes(rightType))
              return UType.error("non-integer type in comparison `" + type(rightExpression, rightType));
          return context.getBooleanType();
          
          case EQ:
          case NEQ:
            if (!(leftType.subsumes(rightType) || rightType.subsumes(leftType)))
              return UType.error("type incompatibility in (in-)equation " + type(leftExpression, leftType) + " - " + type(rightExpression, rightType));
            return context.getBooleanType();

          //$CASES-OMITTED$
          default:
            return UType.error("[illegal relational operator]");
        }
      }
        
      default:
        return UType.error("[illegal expression kind]");
    }
  }

  /**
   * Accept a visitor.
   *
   * @param visitor an annotation visitor
   */
  public <T> T accept(InContextVisitor<T> visitor) {
    final UContext context = visitor.getContext();

    switch (kind) {
      case BOOLEAN:
        return visitor.onBooleanConstant(this.op == OOperator.TRUE);

      case INTEGER:
        return visitor.onIntegerConstant(this.intConst);

      case NULL:
        return visitor.onNullConstant();

      case ID: {
        Identifier qualifier = requireNonNull(this.qualifier);
        Identifier identifier = requireNonNull(this.identifier);

        UAttribute uAttribute = requireNonNull(context.getAttribute(Identifier.id(qualifier, identifier)));
        UObject uObject = context.getObject(qualifier);
        if (uObject == null)
          return visitor.onReference(uAttribute);
        else
          return visitor.onReference(uObject, uAttribute);
      }
        
      case ARRAY: {
        Identifier qualifier = requireNonNull(this.qualifier);
        Identifier identifier = requireNonNull(this.identifier);
        OExpression offset = requireNonNull(this.leftExpression);

        UAttribute uAttribute = requireNonNull(context.getAttribute(Identifier.id(qualifier, identifier)));
        UObject uObject = context.getObject(qualifier);
        if (uObject == null)
          return visitor.onArrayReference(uAttribute, offset);
        else
          return visitor.onArrayReference(uObject, uAttribute, offset);
      }

      case STATE: {
        Identifier identifier = requireNonNull(this.identifier);

        UObject uObject = requireNonNull(context.getObject(this.object));
        UVertex uVertex = requireNonNull(context.getState(uObject.getC1ass(), identifier));
        return visitor.onState(uObject, uVertex);
      }
        
      case UNARY: {
        OOperator op = requireNonNull(this.op);
        OExpression inner = requireNonNull(this.leftExpression);

        return visitor.onUnary(op, inner);
      }
        
      case ARITHMETICAL: {
        OExpression leftExpression = requireNonNull(this.leftExpression);
        OOperator op = requireNonNull(this.op);
        OExpression rightExpression = requireNonNull(this.rightExpression);

        return visitor.onArithmetical(leftExpression, op, rightExpression);
      }

      case RELATIONAL: {
        OExpression leftExpression = requireNonNull(this.leftExpression);
        OOperator op = requireNonNull(this.op);
        OExpression rightExpression = requireNonNull(this.rightExpression);

        return visitor.onRelational(leftExpression, op, rightExpression);
      }

      default:
        throw new IllegalStateException("Unknown OCL expression kind.");
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.object,
                        this.qualifier,
                        this.identifier,
                        this.leftExpression,
                        this.rightExpression);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      OExpression other = (OExpression)object;
      return (this.kind == other.kind &&
              Objects.equals(this.object, other.object) &&
              Objects.equals(this.qualifier, other.qualifier) &&
              Objects.equals(this.identifier, other.identifier) &&
              this.intConst == other.intConst &&
              Objects.equals(this.leftExpression, other.leftExpression) & 
              this.op == other.op &&
              Objects.equals(this.rightExpression, other.rightExpression));
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case BOOLEAN:
        resultBuilder.append(this.op == OOperator.TRUE);
        break;

      case INTEGER:
        resultBuilder.append(this.intConst);
        break;

      case NULL:
        resultBuilder.append("null");
        break;

      case ID:
        resultBuilder.append(this.qualifier);
        resultBuilder.append(".");
        resultBuilder.append(this.identifier);
        break;

      case ARRAY:
        resultBuilder.append(this.identifier);
        resultBuilder.append("[");
        resultBuilder.append(this.leftExpression);
        resultBuilder.append("]");
        break;

      case STATE:
        resultBuilder.append(this.object);
        resultBuilder.append(".inState(");
        resultBuilder.append(this.identifier);
        resultBuilder.append(")");
        break;
        
      case UNARY:
        resultBuilder.append(requireNonNull(this.op).getName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.leftExpression), e -> e.hasPrecedence(this)));
        break;

      case ARITHMETICAL:
      case RELATIONAL:
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.leftExpression), e -> e.hasPrecedence(this)));
        resultBuilder.append(requireNonNull(this.op).getName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.rightExpression), e -> e.hasPrecedence(this)));
        break;
    }
    
    return resultBuilder.toString();
  }
}
