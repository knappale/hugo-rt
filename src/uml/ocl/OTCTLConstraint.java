package uml.ocl;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UType;
import uml.UContext;

import java.util.Objects;

import static util.Formatter.quoted;


/**
 * UML/OCL TCTL constraint
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class OTCTLConstraint extends OConstraint {
  enum Modality {
    AG,
    AF,
    EG,
    EF;

    public String getName() {
      switch (this) {
      case AG: return "AG";
      case AF: return "AF";
      case EG: return "EG";
      case EF: return "EF";
      default: return "[unknown]";
      }
    }
  }

  /** @invariant formula != null */
  private OTCTLFormula formula;
  private Modality modality;

  public static interface Visitor<T> {
    public T onAG(OTCTLFormula formula);
    public T onAF(OTCTLFormula formula);
    public T onEG(OTCTLFormula formula);
    public T onEF(OTCTLFormula formula);
  }

  private OTCTLConstraint(String name, Modality modality, OTCTLFormula formula) {
    super(name);
    this.modality = modality;
    this.formula = formula;
  }

  /**
   * Create a TCTL constraint which represents <CODE>AG</CODE>.
   *
   * @param name constraint name
   * @param formula a TCTL formula
   * @return an <CODE>AG</CODE> constraint
   */
  public static OTCTLConstraint ag(String name, OTCTLFormula formula) {
    return new OTCTLConstraint(name, Modality.AG, formula);
  }

  /**
   * Create a TCTL constraint which represents <CODE>AF</CODE>.
   *
   * @param name formula name
   * @param formula a TCTL formula
   * @return an <CODE>AF</CODE> constraint
   */
  public static OTCTLConstraint af(String name, OTCTLFormula formula) {
    return new OTCTLConstraint(name, Modality.AF, formula);
  }

  /**
   * Create a TCTL constraint which represents <CODE>EG</CODE>.
   *
   * @param name formula name
   * @param formula a TCTL formula
   * @return an <CODE>EG</CODE> constraint
   */
  public static OTCTLConstraint eg(String name, OTCTLFormula formula) {
    return new OTCTLConstraint(name, Modality.EG, formula);
  }

  /**
   * Create a TCTL constraint which represents <CODE>EF</CODE>.
   *
   * @param name formula name
   * @param formula a TCTL formula
   * @return an <CODE>EF</CODE> constraint
   */
  public static OTCTLConstraint ef(String name, OTCTLFormula formula) {
    return new OTCTLConstraint(name, Modality.EF, formula);
  }

  @Override
  public UType getType(UContext context) {
    UType formulaType = this.formula.getType(context);
    if (formulaType.isError())
      return formulaType;
    if (!context.getBooleanType().subsumes(formulaType))
      return UType.error("non-boolean formula " + quoted(formula) + " in constraint");
    return context.getBooleanType();
  }

  /**
   * Accept a TCTL constraint visitor.
   */
  public <T> T accept(Visitor<T> visitor) {
    switch (this.modality) {
      case AG: return visitor.onAG(this.formula);
      case AF: return visitor.onAF(this.formula);
      case EG: return visitor.onEG(this.formula);
      case EF: return visitor.onEF(this.formula);
    }
    throw new IllegalStateException("Unknown TCTL formula kind.");
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(prefix);
    resultBuilder.append("assertion ");
    resultBuilder.append(this.getName());
    resultBuilder.append(" {\n");
    resultBuilder.append(prefix + "  ");
    resultBuilder.append(this.toString());
    resultBuilder.append(";\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");
        
    return resultBuilder.toString();
  }

  @Override
  public String declaration() {
    return this.declaration("");
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(this.formula);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      OTCTLConstraint other = (OTCTLConstraint)object;
      return Objects.equals(this.getName(), other.getName()) &&
             this.modality == other.modality &&
             Objects.equals(this.formula, other.formula);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.modality.getName());
    resultBuilder.append(" ");
    resultBuilder.append(this.formula);

    return resultBuilder.toString();
  }
}
