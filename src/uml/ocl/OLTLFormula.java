package uml.ocl;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UContext;
import uml.UType;
import util.Formatter;
import util.Identifier;

import static util.Objects.requireNonNull;
import static util.Formatter.quoted;
import static util.Formatter.type;

import java.util.Objects;


/**
 * UML/OCL LTL formula
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class OLTLFormula {
  enum Kind {
    MODAL,
    DEADLOCK,
    EXPRESSION,
    UNARY,
    JUNCTIONAL;
  }

  enum Modality {
    F,
    G,
    U;

    public String getName() {
      switch (this) {
      case F: return "F ";
      case G: return "G ";
      case U: return " U ";
      default: return "[unknown]";
      }
    }
  }

  private boolean hasPrecedence(OLTLFormula other) {
    switch (this.kind) {
      case UNARY:
        switch (requireNonNull(this.op)) {
          case FNOT: return ((other.kind == Kind.UNARY) && (other.op == OOperator.FNOT));
          //$CASES-OMITTED$
          default:
        }
        break;

      case JUNCTIONAL:
        switch (requireNonNull(this.op)) {
          case FAND: return ((other.kind != Kind.JUNCTIONAL) || ((other.kind == Kind.JUNCTIONAL) && (other.op == OOperator.FAND)));
          case FOR: return ((other.kind != Kind.JUNCTIONAL) || ((other.kind == Kind.JUNCTIONAL) && (other.op == OOperator.FOR)));
          case FIMPL: return ((other.kind != Kind.JUNCTIONAL));
          //$CASES-OMITTED$
          default:
        }
        break;

      //$CASES-OMITTED$
      default:
    }
    return false;
  }

  private Kind kind;
  private @Nullable Modality modality = null;
  private @Nullable Identifier object = null;
  private @Nullable Identifier identifier = null;
  private @Nullable OExpression expression = null;
  private @Nullable OLTLFormula leftFormula = null;
  private @Nullable OOperator op = null;
  private @Nullable OLTLFormula rightFormula = null;

  private static final OLTLFormula TRUEFORMULA;
  private static final OLTLFormula FALSEFORMULA;
  static {
    TRUEFORMULA = new OLTLFormula(Kind.EXPRESSION);
    TRUEFORMULA.expression = OExpression.trueConst();
    FALSEFORMULA = new OLTLFormula(Kind.EXPRESSION);
    FALSEFORMULA.expression = OExpression.trueConst();
  }

  public interface InContextVisitor<T> {
    public UContext getContext();

    public T onDeadlock();
    public T onExpression(OExpression expression);
    public T onNeg(OLTLFormula formula);
    public T onJunctional(OLTLFormula leftFormula, OOperator op, OLTLFormula rightFormula);
    public T onSometime(OLTLFormula formula);
    public T onAlways(OLTLFormula formula);
    public T onUntil(OLTLFormula leftFormula, OLTLFormula rightFormula);
  }

  private OLTLFormula(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create an LTL formula which represents <CODE>true</CODE>.
   *
   * @return an LTL formula
   */
  public static OLTLFormula trueConst() {
    return TRUEFORMULA;
  }

  /**
   * Create an LTL formula which represents <CODE>false</CODE>.
   *
   * @return an LTL formula
   */
  public static OLTLFormula falseConst() {
    return FALSEFORMULA;
  }

  /**
   * Create an LTL formula which represents <CODE>deadlock</CODE>.
   *
   * @return an LTL formula
   */
  public static OLTLFormula deadlock() {
    OLTLFormula f = new OLTLFormula(Kind.DEADLOCK);
    return f;
  }

  /**
   * Create an LTL formula which represents an expression.
   *
   * @param expression an OCL expression
   * @return an LTL formula
   */
  public static OLTLFormula expression(OExpression expression) {
    OLTLFormula f = new OLTLFormula(Kind.EXPRESSION);
    f.expression = expression;
    return f;
  }

  /**
   * Create an LTL formula which represents negation of a formula.
   *
   * @param formula an LTL formula
   * @return an LTL formula
   */
  public static OLTLFormula neg(OLTLFormula formula) {
    if (formula.equals(OLTLFormula.trueConst()))
      return OLTLFormula.falseConst();
    if (formula.equals(OLTLFormula.falseConst()))
      return OLTLFormula.trueConst();
    OLTLFormula f = new OLTLFormula(Kind.UNARY);
    f.op = OOperator.FNOT;
    f.leftFormula = formula;
    return f;
  }

  /**
   * Create an LTL formula which represents a conjunction.
   *
   * @param leftFormula left LTL formula
   * @param rightFormula right LTL formula
   * @return an LTL formula
   */
  public static OLTLFormula and(OLTLFormula leftFormula, OLTLFormula rightFormula) {
    if (leftFormula.equals(OLTLFormula.trueConst()))
      return rightFormula;
    if (rightFormula.equals(OLTLFormula.trueConst()))
      return leftFormula;
    if (leftFormula.equals(OLTLFormula.falseConst()))
      return OLTLFormula.falseConst();
    if (rightFormula.equals(OLTLFormula.falseConst()))
      return OLTLFormula.falseConst();
    OLTLFormula f = new OLTLFormula(Kind.JUNCTIONAL);
    f.leftFormula = leftFormula;
    f.op = OOperator.FAND;
    f.rightFormula = rightFormula;
    return f;
  }

  /**
   * Create an LTL formula which represents a disjunction.
   *
   * @param leftFormula left LTL formula
   * @param rightFormula right LTL formula
   * @return an LTL formula
   */
  public static OLTLFormula or(OLTLFormula leftFormula, OLTLFormula rightFormula) {
    if (leftFormula.equals(OLTLFormula.falseConst()))
      return rightFormula;
    if (rightFormula.equals(OLTLFormula.falseConst()))
      return leftFormula;
    if (leftFormula.equals(OLTLFormula.trueConst()))
      return OLTLFormula.trueConst();
    if (rightFormula.equals(OLTLFormula.trueConst()))
      return OLTLFormula.trueConst();
    OLTLFormula f = new OLTLFormula(Kind.JUNCTIONAL);
    f.leftFormula = leftFormula;
    f.op = OOperator.FOR;
    f.rightFormula = rightFormula;
    return f;
  }

  /**
   * Create an LTL formula which represents an implication.
   *
   * @param leftFormula left LTL formula
   * @param rightFormula right LTL formula
   * @return an LTL formula
   */
  public static OLTLFormula implies(OLTLFormula leftFormula, OLTLFormula rightFormula) {
    if (leftFormula.equals(OLTLFormula.falseConst()))
      return OLTLFormula.trueConst();
    if (leftFormula.equals(OLTLFormula.trueConst()))
      return rightFormula;
    if (rightFormula.equals(OLTLFormula.trueConst()))
      return OLTLFormula.trueConst();
    OLTLFormula f = new OLTLFormula(Kind.JUNCTIONAL);
    f.leftFormula = leftFormula;
    f.op = OOperator.FIMPL;
    f.rightFormula = rightFormula;
    return f;
  }

  /**
   * Create an LTL formula which represents a sometime.
   *
   * @param formula an LTL formula
   * @return an LTL formula
   */
  public static OLTLFormula sometime(OLTLFormula formula) {
    OLTLFormula f = new OLTLFormula(Kind.MODAL);
    f.leftFormula = formula;
    f.modality = Modality.F;
    return f;
  }

  /**
   * Create an LTL formula which represents an always.
   *
   * @param formula an LTL formula
   * @return an LTL formula
   */
  public static OLTLFormula always(OLTLFormula formula) {
    OLTLFormula f = new OLTLFormula(Kind.MODAL);
    f.leftFormula = formula;
    f.modality = Modality.G;
    return f;
  }

  /**
   * Create an LTL formula which represents an until.
   *
   * @param leftFormula left LTL formula
   * @param rightFormula right LTL formula
   * @return an LTL formula
   */
  public static OLTLFormula until(OLTLFormula leftFormula, OLTLFormula rightFormula) {
    OLTLFormula f = new OLTLFormula(Kind.MODAL);
    f.leftFormula = leftFormula;
    f.modality = Modality.U;
    f.rightFormula = rightFormula;
    return f;
  }

  /**
   * Determine LTL formula's type.
   *
   * @param context LTL formula context
   * @return LTL formula's type
   */
  public UType getType(UContext context) {
    switch (kind) {
      case DEADLOCK:
        return context.getBooleanType();

      case EXPRESSION: {
        OExpression expression = requireNonNull(this.expression);

        UType expressionType = expression.getType(context);
        if (expressionType.isError())
          return expressionType;
        if (!context.getBooleanType().subsumes(expressionType))
          return UType.error("Non-boolean expression " + type(expression, expressionType));
        return context.getBooleanType();
      }

      case UNARY: {
        OOperator op = requireNonNull(this.op);
        OLTLFormula leftFormula = requireNonNull(this.leftFormula);

        UType leftType = leftFormula.getType(context);
        if (leftType.isError())
          return leftType;
        if (!context.getBooleanType().subsumes(leftType))
          return UType.error(quoted(op.getName()) + " applied to non-boolean formula " + type(leftFormula, leftType));
        return context.getBooleanType();
      }
      
      case JUNCTIONAL: {
        OLTLFormula leftFormula = requireNonNull(this.leftFormula);
        OOperator op = requireNonNull(this.op);
        OLTLFormula rightFormula = requireNonNull(this.rightFormula);

        UType leftType = leftFormula.getType(context);
        if (leftType.isError())
          return leftType;
        UType rightType = rightFormula.getType(context);
        if (rightType.isError())
          return rightType;
        switch (op) {
          case FAND:
          case FOR:
          case FIMPL:
            if (!context.getBooleanType().subsumes(leftType))
              return UType.error("non-boolean type in junctional formula " + type(leftFormula, leftType));
            if (!context.getBooleanType().subsumes(rightType))
              return UType.error("non-boolean type in junctional formula " + type(rightFormula, rightType));
            return context.getBooleanType();
          //$CASES-OMITTED$
          default:
            return UType.error("[illegal junctional operator]");
        }
      }

      case MODAL: {
        Modality modality = requireNonNull(this.modality);

        switch (modality) {
          case F:
          case G: {
            OLTLFormula formula = requireNonNull(this.leftFormula);

            UType type = formula.getType(context);
            if (type.isError())
              return type;
            if (!context.getBooleanType().subsumes(type))
              return UType.error("non-boolean type in modal formula " + type(formula, type));
            return type;
          }
          case U: {
            OLTLFormula leftFormula = requireNonNull(this.leftFormula);
            OLTLFormula rightFormula = requireNonNull(this.rightFormula);

            UType leftType = leftFormula.getType(context);
            if (leftType.isError())
              return leftType;
            UType rightType = rightFormula.getType(context);
            if (rightType.isError())
              return rightType;
            if (!context.getBooleanType().subsumes(leftType))
              return UType.error("non-boolean type in modal formula " + type(leftFormula, leftType));
            if (!context.getBooleanType().subsumes(rightType))
              return UType.error("non-boolean type in modal formula " + type(rightFormula, rightType));
            return context.getBooleanType();
          }
        }
      }
    }
      
    return UType.error("[unknown formula kind]");
  }

  /**
   * Accept a visitor.
   */
  public <T> T accept(InContextVisitor<T> visitor) {
    switch (kind) {
      case DEADLOCK:
        return visitor.onDeadlock();

      case EXPRESSION:
        return visitor.onExpression(requireNonNull(this.expression));

      case UNARY:
        return visitor.onNeg(requireNonNull(this.leftFormula));
      
      case JUNCTIONAL:
        return visitor.onJunctional(requireNonNull(this.leftFormula), requireNonNull(this.op), requireNonNull(this.rightFormula));

      case MODAL:
        switch (requireNonNull(this.modality)) {
          case F:
            return visitor.onSometime(requireNonNull(this.leftFormula));
          case G:
            return visitor.onAlways(requireNonNull(this.leftFormula));
          case U:
            return visitor.onUntil(requireNonNull(this.leftFormula), requireNonNull(this.rightFormula));
          default:
            throw new IllegalStateException("Unknown LTL modality.");
        }
 
      default:
        throw new IllegalStateException("Unknown LTL formula kind.");
    }
  }
 
  @Override
  public int hashCode() {
    return Objects.hash(this.object,
                        this.identifier,
                        this.expression,
                        this.leftFormula,
                        this.rightFormula);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      OLTLFormula other = (OLTLFormula)object;
      return this.kind == other.kind &&
             Objects.equals(this.object, other.object) &&
             Objects.equals(this.identifier, other.identifier) &&
             Objects.equals(this.leftFormula, other.leftFormula) &&
             Objects.equals(this.expression, other.expression) &&
             this.op == other.op &&
             Objects.equals(this.rightFormula, other.rightFormula);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (kind) {
      case DEADLOCK:
        resultBuilder.append("deadlock");
        break;

      case EXPRESSION:
        resultBuilder.append(this.expression);
        break;

      case UNARY:
        resultBuilder.append(requireNonNull(this.op).getName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.leftFormula), f -> f.hasPrecedence(this)));
        break;

      case JUNCTIONAL:
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.leftFormula), f -> f.hasPrecedence(this)));
        resultBuilder.append(requireNonNull(this.op).getName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.rightFormula), f -> f.hasPrecedence(this)));
        break;

      case MODAL:
        Modality modality = requireNonNull(this.modality);

        switch (modality) {
          case F:
          case G:
            resultBuilder.append(modality.getName());
            resultBuilder.append(this.leftFormula);
            break;
          case U:
            resultBuilder.append(this.leftFormula);
            resultBuilder.append(modality.getName());
            resultBuilder.append(this.rightFormula);
            break;
        }
    }

    return resultBuilder.toString();
  }
}
