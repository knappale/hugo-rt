package uml.ocl;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UContext;
import uml.UType;

import java.util.Objects;

import static util.Formatter.quoted;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class OLTLConstraint extends OConstraint {
  OLTLFormula formula;

  public interface Visitor<T> {
    public T onFormula(OLTLFormula formula);
  }

  public OLTLConstraint(String name, OLTLFormula formula) {
    super(name);
    this.formula = formula;
  }

  /**
   * Accept a LTL constraint visitor.
   */
  public <T> T accept(Visitor<T> visitor) {
    return visitor.onFormula(this.formula);
  }

  @Override
  public UType getType(UContext context) {
    UType formulaType = this.formula.getType(context);
    if (formulaType.isError())
      return formulaType;
    if (!context.getBooleanType().subsumes(formulaType))
      return UType.error("non-boolean formula " + quoted(formula) + " in constraint");
    return context.getBooleanType();
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(prefix);
    resultBuilder.append("assertion ");
    resultBuilder.append(this.getName());
    resultBuilder.append(" {\n");
    resultBuilder.append(prefix + "  ");
    resultBuilder.append(this.toString());
    resultBuilder.append(";\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");
        
    return resultBuilder.toString();
  }

  @Override
  public String declaration() {
    return this.declaration("");
  }
 
  @Override
  public int hashCode() {
    return Objects.hash(this.formula);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      OLTLConstraint other = (OLTLConstraint)object;
      return Objects.equals(this.getName(), other.getName()) &&
             Objects.equals(this.formula, other.formula);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.formula.toString();
  }
}
