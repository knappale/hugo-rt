package uml.ocl;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UType;
import util.Formatter;
import util.Identifier;
import uml.UObject;
import uml.UContext;

import static util.Formatter.quoted;
import static util.Formatter.type;
import static util.Objects.requireNonNull;

import java.util.Objects;


/**
 * UML/OCL TCTL state formula
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class OTCTLFormula {
  enum Kind {
    EXPRESSION,
    DEADLOCK,
    FAIL,
    OVERFLOW,
    UNARY,
    JUNCTIONAL;
  }

  private boolean hasPrecedence(OTCTLFormula other) {
    switch (this.kind) {
      case DEADLOCK:
        return true;

      case UNARY:
        switch (requireNonNull(this.op)) {
          case FNOT: return ((other.kind == Kind.UNARY) && (other.op == OOperator.FNOT));
          //$CASES-OMITTED$
          default:
        }
        break;

      case JUNCTIONAL:
        switch (requireNonNull(this.op)) {
          case FAND: return ((other.kind != Kind.JUNCTIONAL) || ((other.kind == Kind.JUNCTIONAL) && (other.op == OOperator.FAND)));
          case FOR: return ((other.kind != Kind.JUNCTIONAL) || ((other.kind == Kind.JUNCTIONAL) && (other.op == OOperator.FOR)));
          case FIMPL: return ((other.kind != Kind.JUNCTIONAL));
          //$CASES-OMITTED$
          default:
        }
        break;

      //$CASES-OMITTED$
      default:
    }
    return false;
  }

  private Kind kind;
  private @Nullable Identifier identifier = null;
  private @Nullable OExpression expression = null;
  private @Nullable OTCTLFormula leftFormula = null;
  private @Nullable OOperator op = null;
  private @Nullable OTCTLFormula rightFormula = null;

  private static final OTCTLFormula TRUEFORMULA;
  private static final OTCTLFormula FALSEFORMULA;
  static {
    TRUEFORMULA = new OTCTLFormula(Kind.EXPRESSION);
    TRUEFORMULA.expression = OExpression.trueConst();
    FALSEFORMULA = new OTCTLFormula(Kind.EXPRESSION);
    FALSEFORMULA.expression = OExpression.trueConst();
  }

  public static interface InContextVisitor<T> {
    public UContext getContext();
  
    public T onExpression(OExpression expression);
    public T onDeadlock();
    public T onFail();
    public T onOverflow(UObject object);
    public T onNeg(OTCTLFormula formula);
    public T onJunctional(OTCTLFormula leftFormula, OOperator op, OTCTLFormula rightFormula);
  }

  private OTCTLFormula(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create a TCTL formula which represents <CODE>true</CODE>.
   *
   * @return a TCTL formula
   */
  public static OTCTLFormula trueConst() {
    return TRUEFORMULA;
  }

  /**
   * Create a TCTL formula which represents <CODE>false</CODE>.
   *
   * @return a TCTL formula
   */
  public static OTCTLFormula falseConst() {
    return FALSEFORMULA;
  }

  /**
   * Create a TCTL formula which represents an expression.
   *
   * @param expression an OCL expression
   * @return a TCTL formula
   */
  public static OTCTLFormula expression(OExpression expression) {
    OTCTLFormula f = new OTCTLFormula(Kind.EXPRESSION);
    f.expression = expression;
    return f;
  }

  /**
   * Create a TCTL formula which represents a deadlock.
   *
   * @return a TCTL formula
   */
  public static OTCTLFormula deadlock() {
    OTCTLFormula f = new OTCTLFormula(Kind.DEADLOCK);
    return f;
  }

  /**
   * Create a TCTL formula which represents a failure.
   *
   * @return a TCTL formula
   */
  public static OTCTLFormula fail() {
    OTCTLFormula f = new OTCTLFormula(Kind.FAIL);
    return f;
  }

  /**
   * Create a TCTL formula which represents a queue overflow in the object named id.
   * 
   * @param id object name
   * @return a TCTL formula
   */
  public static OTCTLFormula overflow(Identifier id) {
    OTCTLFormula f = new OTCTLFormula(Kind.OVERFLOW);
    f.identifier = id;
    return f;
  }

  /**
   * Create a TCTL formula which represents negation of a formula.
   *
   * @param formula a TCTL formula
   * @return a TCTL formula
   */
  public static OTCTLFormula neg(OTCTLFormula formula) {
    if (formula.equals(OTCTLFormula.trueConst()))
      return OTCTLFormula.falseConst();
    if (formula.equals(OTCTLFormula.falseConst()))
      return OTCTLFormula.trueConst();
    OTCTLFormula f = new OTCTLFormula(Kind.UNARY);
    f.op = OOperator.FNOT;
    f.leftFormula = formula;
    return f;
  }

  /**
   * Create a TCTL formula which represents a conjunction.
   *
   * @param leftFormula left TCTL formula
   * @param rightFormula right TCTL formula
   * @return a TCTL formula
   */
  public static OTCTLFormula and(OTCTLFormula leftFormula, OTCTLFormula rightFormula) {
    if (leftFormula.equals(OTCTLFormula.trueConst()))
      return rightFormula;
    if (rightFormula.equals(OTCTLFormula.trueConst()))
      return leftFormula;
    if (leftFormula.equals(OTCTLFormula.falseConst()))
      return OTCTLFormula.falseConst();
    if (rightFormula.equals(OTCTLFormula.falseConst()))
      return OTCTLFormula.falseConst();
    OTCTLFormula f = new OTCTLFormula(Kind.JUNCTIONAL);
    f.leftFormula = leftFormula;
    f.op = OOperator.FAND;
    f.rightFormula = rightFormula;
    return f;
  }

  /**
   * Create a TCTL formula which represents a disjunction.
   *
   * @param leftFormula left TCTL formula
   * @param rightFormula right TCTL formula
   * @return a TCTL formula
   */
  public static OTCTLFormula or(OTCTLFormula leftFormula, OTCTLFormula rightFormula) {
    if (leftFormula.equals(OTCTLFormula.falseConst()))
      return rightFormula;
    if (rightFormula.equals(OTCTLFormula.falseConst()))
      return leftFormula;
    if (leftFormula.equals(OTCTLFormula.trueConst()))
      return OTCTLFormula.trueConst();
    if (rightFormula.equals(OTCTLFormula.trueConst()))
      return OTCTLFormula.trueConst();
    OTCTLFormula f = new OTCTLFormula(Kind.JUNCTIONAL);
    f.leftFormula = leftFormula;
    f.op = OOperator.FOR;
    f.rightFormula = rightFormula;
    return f;
  }

  /**
   * Create a TCTL formula which represents a implication.
   *
   * @param leftFormula left TCTL formula
   * @param rightFormula right TCTL formula
   * @return a TCTL formula
   */
  public static OTCTLFormula implies(OTCTLFormula leftFormula, OTCTLFormula rightFormula) {
    if (leftFormula.equals(OTCTLFormula.falseConst()))
      return OTCTLFormula.trueConst();
    if (leftFormula.equals(OTCTLFormula.trueConst()))
      return rightFormula;
    if (rightFormula.equals(OTCTLFormula.trueConst()))
      return OTCTLFormula.trueConst();
    OTCTLFormula f = new OTCTLFormula(Kind.JUNCTIONAL);
    f.leftFormula = leftFormula;
    f.op = OOperator.FIMPL;
    f.rightFormula = rightFormula;
    return f;
  }

  /**
   * Determine TCTL formula's type.
   *
   * @param context TCTL formula context
   * @return TCTL formula's type
   */
  public UType getType(UContext context) {
    switch (this.kind) {
      case EXPRESSION: {
        OExpression expression = requireNonNull(this.expression);

        UType expressionType = expression.getType(context);
        if (expressionType.isError())
          return expressionType;
        if (!context.getBooleanType().subsumes(expressionType))
          return UType.error("Non-boolean expression " + type(expression, expressionType));
        return context.getBooleanType();
      }

      case DEADLOCK:
        return context.getBooleanType();

      case FAIL:
        return context.getBooleanType();

      case OVERFLOW: {
        UObject object = context.getObject(this.identifier);
        if (object == null)
          return UType.error("unknown \"object\" " + quoted(this.identifier));
        return context.getBooleanType();
      }

      case UNARY: {
        OOperator op = requireNonNull(this.op);
        OTCTLFormula leftFormula = requireNonNull(this.leftFormula);

        UType leftType = leftFormula.getType(context);
        if (leftType.isError())
          return leftType;
        if (!context.getBooleanType().subsumes(leftType))
          return UType.error(quoted(op.getName()) + " applied to non-boolean formula " + type(leftFormula, leftType));
        return context.getBooleanType();
      }
  
      case JUNCTIONAL: {
        OTCTLFormula leftFormula = requireNonNull(this.leftFormula);
        OOperator op = requireNonNull(this.op);
        OTCTLFormula rightFormula = requireNonNull(this.rightFormula);

        UType leftType = leftFormula.getType(context);
        if (leftType.isError())
          return leftType;
        UType rightType = rightFormula.getType(context);
        if (rightType.isError())
          return rightType;
        switch (op) {
          case FAND:
          case FOR:
          case FIMPL:
            if (!context.getBooleanType().subsumes(leftType))
              return UType.error("non-boolean type in junctional formula " + type(leftFormula, leftType));
            if (!context.getBooleanType().subsumes(rightType))
              return UType.error("non-boolean type in junctional formula `" + type(rightFormula, rightType));
            return context.getBooleanType();
          //$CASES-OMITTED$
          default:
            return UType.error("[illegal junctional operator]");
        }
      }
    }
      
    return UType.error("[unknown formula kind]");
  }

  /**
   * Accept a TCTL visitor.
   */
  public <T> T accept(InContextVisitor<T> visitor) {
    switch (kind) {
      case EXPRESSION:
        return visitor.onExpression(requireNonNull(this.expression));
        
      case DEADLOCK:
        return visitor.onDeadlock();

      case FAIL:
        return visitor.onFail();

      case OVERFLOW:
        UObject object = visitor.getContext().getObject(this.identifier);
        return visitor.onOverflow(requireNonNull(object));

      case UNARY:
        return visitor.onNeg(requireNonNull(this.leftFormula));
        
      case JUNCTIONAL:
        return visitor.onJunctional(requireNonNull(this.leftFormula), requireNonNull(this.op), requireNonNull(this.rightFormula));

      //$CASES-OMITTED$
      default:
        throw new IllegalArgumentException("Unknown kind of TCTL formula.");
    }
  }
 
  /**
   * @return TCTL formula's representation in UTE format
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.toString());
    resultBuilder.append(";");

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.identifier,
                        this.expression,
                        this.leftFormula,
                        this.rightFormula);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      OTCTLFormula other = (OTCTLFormula)object;
      return this.kind == other.kind &&
             Objects.equals(this.identifier, other.identifier) &&
             Objects.equals(this.leftFormula, other.leftFormula) &&
             Objects.equals(this.expression, other.expression) &&
             this.op == other.op &&
             Objects.equals(this.rightFormula, other.rightFormula);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (kind) {
      case EXPRESSION:
        resultBuilder.append(this.expression);
        break;
        
      case DEADLOCK:
        resultBuilder.append("deadlock");
        break;

      case FAIL:
        resultBuilder.append("fail");
        break;

      case OVERFLOW:
        resultBuilder.append("overflow(");
        resultBuilder.append(this.identifier);
        resultBuilder.append(")");
        break;

      case UNARY:
        resultBuilder.append(requireNonNull(this.op).getName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.leftFormula), f -> f.hasPrecedence(this)));
        break;
      
      case JUNCTIONAL:
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.leftFormula), f -> f.hasPrecedence(this)));
        resultBuilder.append(requireNonNull(this.op).getName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.rightFormula), f -> f.hasPrecedence(this)));
        break;
    }

    return resultBuilder.toString();
  }
}
