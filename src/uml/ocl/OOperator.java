package uml.ocl;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * OCL operator
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public enum OOperator {
  TRUE,
  FALSE,
  UPLUS,
  UMINUS,
  ADD,
  SUB,
  MULT,
  DIV,
  MOD,
  LT,
  LEQ,
  EQ,
  NEQ,
  GEQ,
  GT,
  FNOT,
  FAND,
  FOR,
  FIMPL;
  
  public String getName() {
    switch (this) {
    case TRUE: return "true";
    case FALSE: return "false";
    case UPLUS: return "+";
    case UMINUS: return "-";
    case ADD: return "+";
    case SUB: return "-";
    case MULT: return "*";
    case DIV: return "/";
    case MOD: return "%";
    case LT: return " < ";
    case LEQ: return " <= ";
    case EQ: return " == ";
    case NEQ: return " != ";
    case GEQ: return " >= ";
    case GT: return " > ";
    case FNOT: return "not ";
    case FAND: return " and ";
    case FOR: return " or ";
    case FIMPL: return " implies ";
    default: return "[unknown]";
    }
  }

  public static boolean precedes(OOperator op1, OOperator op2) {
    return (op1.getPrecedence() > op2.getPrecedence()) ||
           (op1 == op2 && op1.isAssociative());
  }

  private int getPrecedence() {
    switch (this) {
      case TRUE:
      case FALSE:
        return 24;
      case UPLUS:
      case UMINUS:
        return 22;
      case MULT:
      case DIV:
      case MOD:
        return 20;
      case SUB:
        return 19;
      case ADD:
        return 18;
      case LT:
      case LEQ:
      case GEQ:
      case GT:
        return 16;
      case EQ:
      case NEQ:
        return 14;
      case FNOT:
        return 12;
      case FAND:
      case FOR:
      case FIMPL:
        return 10;
      default:
        return 0;
    }
  }

  public boolean isAssociative() {
    switch (this) {
      case UPLUS:
      case UMINUS:
      case MULT:
      case ADD:
      case FNOT:
      case FAND:
      case FOR:
        return true;
      //$CASES-OMITTED$
      default:
        return false;
    }
  }
}
