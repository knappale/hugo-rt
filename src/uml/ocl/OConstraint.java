package uml.ocl;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UContext;
import uml.UType;


/**
 * UML/OCL constraint
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class OConstraint {
  private String name = "";

  OConstraint(String name) {
    this.name = name.trim();
  }

  /**
   * @return constraint's name
   */
  public String getName() {
    return this.name.equals("") ? "anonymous" : this.name;
  }

  /**
   * @return constraint's type
   */
  public abstract UType getType(UContext context);

  /**
   * Determine constraint's representation in UTE format with a prefix prepended
   * to every line.
   * 
   * @param prefix prefix string
   * @return constraint's representation in UTE format with prefix
   */
 public abstract String declaration(String prefix);

  /**
   * @return constraint's representation in UTE format
   */
  public abstract String declaration();
}
