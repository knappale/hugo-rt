package uml.interaction;

import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UElement;
import util.Identifier;
import util.UnlimitedNatural;


/**
 * A UML2 sequence diagram is composed of basic interactions and fragments
 * composed by operators.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 * @since 2005-11-26
 */
@NonNullByDefault
public abstract class UInteractionFragment extends UElement {
  private UInteraction interaction;

  public static interface Visitor<T> {
    public T onBasic(UBasicFragment basic);
    public T onStateInvariant(UStateInvariant stateInvariant);

    public T onAlt(List<UInteractionOperand> operands);
    public T onPAlt(List<UInteractionOperand> operands);
    public T onIgnore(UInteractionOperand operand, Set<Identifier> ignores);
    public T onConsider(UInteractionOperand operand, Set<Identifier> ignores);
    public T onStrictLoop(UInteractionOperand operand, UnlimitedNatural min, UnlimitedNatural max);
    public T onLoop(UBasicFragment basic, UnlimitedNatural min, UnlimitedNatural max);
    public T onNot(UInteractionOperand operand);
    public T onOpt(UInteractionOperand operand);
    public T onPar(List<UInteractionOperand> operands);
    public T onSeq(List<UInteractionOperand> operands);
    public T onStrict(List<UInteractionOperand> operands);
  }

  /**
   * Create a new interaction fragment.
   * 
   * @param enclosingInteraction interaction fragment's enclosing interaction
   */
  public UInteractionFragment(UInteraction enclosingInteraction) {
    super("<anonymous interaction fragment>");
    enclosingInteraction.addFragment(this);
    this.interaction = enclosingInteraction;
  }

  /**
   * Create a new interaction fragment.
   * 
   * @param enclosingOperand interaction fragment's enclosing interaction operand
   */
  public UInteractionFragment(UInteractionOperand enclosingOperand) {
    super("<anonymous interaction fragment>");
    enclosingOperand.addFragment(this);
    this.interaction = enclosingOperand.getInteraction();
  }

  /**
   * @return interaction fragment's enclosing interaction
   */
  public UInteraction getInteraction() {
    return this.interaction;
  }

  /**
   * @return lifelines covered by this interaction fragment
   */
  public abstract Set<ULifeline> getLifelines();

  /**
   * @return interaction fragment's occurrence specifications
   */
  public abstract Set<UOccurrenceSpecification> getOccurrences();

  /**
   * Accept a visitor.
   * 
   * @param visitor an interaction visitor
   */
  public abstract <T> T accept(UInteractionFragment.Visitor<T> visitor);

  /**
   * Determine interaction fragment's representation in UTE format with prefix.
   *
   * @param prefix prefix string
   * @return interaction fragment's representation in UTE format with prefix
   */
  public abstract String declaration(String prefix);
}
