package uml.interaction;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * Seq(uential) combined fragment.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class USequentialFragment extends UCombinedFragment {
  /**
   * Create a new sequential fragment.
   *
   * @param enclosingInteraction sequential fragment's enclosing interaction
   */
  public USequentialFragment(UInteraction enclosingInteraction) {
    super(enclosingInteraction);
  }

  /**
   * Create a new sequential fragment.
   *
   * @param enclosingOperand sequential fragment's enclosing interaction operand
   */
  public USequentialFragment(UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {
    return visitor.onSeq(this.getOperands());
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("seq");
    for (UInteractionOperand operand : this.getOperands()) {
      resultBuilder.append(" {\n");
      resultBuilder.append(operand.declaration(nextPrefix));
      resultBuilder.append("\n");
      resultBuilder.append(prefix);
      resultBuilder.append("}");
    }

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("SeqFragment [operands = ");
    resultBuilder.append(Formatter.list(this.getOperands()));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
