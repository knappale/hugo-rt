package uml.interaction;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.UnlimitedNatural;


/**
 * Implements the loop combined fragment.
 *
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class ULoopFragment extends UCombinedFragment {
  private UnlimitedNatural min, max;
  private boolean isStrict;

  /**
   * Create a new loop fragment.
   *
   * @param enclosingInteraction loop fragments's enclosing interaction
   */
  public ULoopFragment(boolean isStrict, UnlimitedNatural min, UnlimitedNatural max, UInteraction enclosingInteraction) {
    super(enclosingInteraction);
    this.isStrict = isStrict;
    this.min = min;
    this.max = max;
  }

  /**
   * Create a new loop fragment.
   *
   * @param enclosingOperand loop fragments's enclosing interaction operand
   */
  public ULoopFragment(boolean isStrict, UnlimitedNatural min, UnlimitedNatural max, UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
    this.isStrict = isStrict;
    this.min = min;
    this.max = max;
  }

  /**
   * @return return whether looping is done by strict sequencing
   */
  public boolean isStrict() {
    return this.isStrict;
  }

  /**
   * @return the minimal number of iterations
   */
  public UnlimitedNatural getMinIterations() {
    return this.min;
  }

  /**
   * @return the maximal number of iterations
   * @pre !this.isUpperInfty()
   */
  public UnlimitedNatural getMaxIterations() {
    return this.max;
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {
    if (this.isStrict())
      return visitor.onStrictLoop(this.getOperands().get(0), this.min, this.max);
    else
      return visitor.onLoop((UBasicFragment)this.getOperands().get(0).getFragments().get(0), this.min, this.max);
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("loop <");
    resultBuilder.append(this.min);
    resultBuilder.append(", ");
    resultBuilder.append(this.max);
    resultBuilder.append("> {\n");
    resultBuilder.append(this.getOperands().get(0).declaration(nextPrefix));
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("LoopFragment [min = ");
    resultBuilder.append(this.min);
    resultBuilder.append(", max = ");
    resultBuilder.append(this.max);
    resultBuilder.append(", strict = ");
    resultBuilder.append(this.isStrict());
    resultBuilder.append(", operand = ");
    resultBuilder.append(this.getOperands().get(0));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
