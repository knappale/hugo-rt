package uml.interaction;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UElement;
import uml.UExpression;
import util.Formatter;


/**
 * The operands to the different operators are implemented in this class. It
 * represents a direct implementation of the class from the abstract syntax.
 *
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class UInteractionOperand extends UElement {
  private UCombinedFragment enclosingCombinedFragment;
  private @Nullable Double rate;
  private UExpression condition = UExpression.trueConst();
  private List<UInteractionFragment> fragments = new LinkedList<>();

  /**
   * Create an interaction operand of a combined fragment.
   *
   * @param enclosingCombinedFragment enclosing combined fragment
   */
  public UInteractionOperand(UCombinedFragment enclosingCombinedFragment) {
    super("");
    this.enclosingCombinedFragment = enclosingCombinedFragment;
    this.enclosingCombinedFragment.addOperand(this);
  }

  /**
   * Set interaction operand's enabling condition.
   *
   * @param condition a UML condition
   */
  public void setCondition(UExpression condition) {
    this.condition = condition;
  }

  /**
   * @return interaction operand's enabling condition
   */
  public UExpression getCondition() {
    return this.condition;
  }

  /**
   * Set interaction operand's rate.
   *
   * @param rate a rate
   */
  public void setRate(double rate) {
    this.rate = rate;
  }

  /**
   * @return interaction operand's rate
   */
  public @Nullable Double getRate() {
    return this.rate;
  }

  /**
   * Add an interaction fragment to this interaction operand.
   *
   * @param interationFragment an interaction fragment
   */
  void addFragment(UInteractionFragment interactionFragment) {
    this.fragments.add(interactionFragment);
  }

  /**
   * @return interaction operand's interaction fragments
   */
  public List<UInteractionFragment> getFragments() {
    return this.fragments;
  }

  /**
   * @return interaction operand's interaction fragments
   */
  public UInteraction getInteraction() {
    return this.enclosingCombinedFragment.getInteraction();
  }

  /**
   * @return lifelines covered by this interaction fragment
   */
  public Set<ULifeline> getLifelines() {
    Set<ULifeline> lifelines = new HashSet<>();
    for (UInteractionFragment fragment : this.getFragments())
      lifelines.addAll(fragment.getLifelines());
    return lifelines;
  }

  /**
   * @return interaction operand's occurrence specifications
   */
  public Set<UOccurrenceSpecification> getOccurrences() {
    Set<UOccurrenceSpecification> occurrences = new HashSet<>();
    for (UInteractionFragment fragment : this.getFragments())
      occurrences.addAll(fragment.getOccurrences());
    return occurrences;
  }

  /**
   * Factory method for obtaining a basic fragment for this interaction operand.
   * If the last fragment in this interaction operand is a basic fragment, this will
   * be returned; otherwise a new basic fragment for this interaction operand is
   * returned.
   * 
   * @return a basic interaction
   */
  public UBasicFragment getBasicFragment() {
    if (!this.fragments.isEmpty()) {
      UInteractionFragment lastFragment = this.fragments.get(this.fragments.size()-1);
      if (lastFragment instanceof UBasicFragment)
        return (UBasicFragment)lastFragment;
    }
    return new UBasicFragment(this);
  }

  /**
   * Determine interaction operand's representation in UTE format with prefix.
   *
   * @param prefix prefix string
   * @return interaction operand's representation in UTE format with prefix
   */
 public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(prefix);
    resultBuilder.append("[");
    resultBuilder.append(this.getCondition().toString());
    resultBuilder.append("]");
    String lineSep = "\n";
    for (UInteractionFragment fragment : this.getFragments()) {
      resultBuilder.append(lineSep);
      resultBuilder.append(fragment.declaration(prefix));
      lineSep = "\n\n";
    }

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("InteractionOperand [condition = ");
    resultBuilder.append(this.getCondition());
    Double rate = this.getRate();
    if (rate != null) {
      resultBuilder.append(", rate = ");
      resultBuilder.append(rate);
    }
    resultBuilder.append(", fragments = ");
    resultBuilder.append(Formatter.list(this.getFragments(), uFragment -> uFragment.getName()));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
