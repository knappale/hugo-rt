package uml.interaction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UBehavioural;
import uml.UContext;
import uml.UDescriptor;
import uml.UElement;
import uml.UExpression;
import uml.UObject;
import uml.UType;
import util.Formatter;
import util.Identifier;


/**
 * Contains data of a (UML) message.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UMessage extends UElement {
  private @Nullable UInteraction interaction;
  private @Nullable UObject sender;
  private UObject receiver;
  private Identifier behaviouralId;
  private List<UExpression> arguments = new ArrayList<>();
  private Set<UMessage> predecessors = new HashSet<>();
  private @Nullable UMessageOccurrenceSpecification sendEvent = null;
  private @Nullable UMessageOccurrenceSpecification receiveEvent = null;

  /**
   * Create a new message.
   *
   * @param name optional name
   * @param sender sender object
   * @param behaviouralId message's behaviour identifier
   * @param arguments arguments list
   * @param receiver receiver object
   * @param interaction message's interaction
   */
  public UMessage(String name, UObject sender, Identifier behaviouralId, List<UExpression> arguments, UObject receiver, UInteraction interaction) {
    super(name);
    this.sender = sender;
    this.behaviouralId = behaviouralId;
    this.arguments.addAll(arguments);
    this.receiver = receiver;
    this.interaction = interaction;
    this.interaction.addMessage(this);
  }

  /**
   * Create a new message.
   *
   * @param name optional name
   * @param sender sender object
   * @param behavioural message's behaviour
   * @param arguments arguments list
   * @param receiver receiver object
   */
  public UMessage(String name, @Nullable UObject sender, @Nullable UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
    super(name);
    this.sender = sender;
    this.behaviouralId = (behavioural == null ? Identifier.id("") : Identifier.id(behavioural.getName()));
    this.arguments.addAll(arguments);
    this.receiver = receiver;
  }

  /**
   * @return message's sending object
   */
  public @Nullable UObject getSender() {
    return this.sender;
  }

  /**
   * @return message's receiving object
   */
  public UObject getReceiver() {
    return this.receiver;
  }

  /**
   * @return message's behavioural identifier
   */
  public Identifier getBehaviouralId() {
    return this.behaviouralId;
  }

  /**
   * @return message's arguments
   */
  public List<UExpression> getArguments() {
    return this.arguments;
  }

  /**
   * Set message's send event.
   *
   * @param sendEvent sending message occurrence specification
   */
  public void setSendEvent(UMessageOccurrenceSpecification sendEvent) {
    this.sendEvent = sendEvent;
  }
  
  /**
   * @return sending message occurrence specification
   */
  public @Nullable UMessageOccurrenceSpecification getSendEvent() {
    return sendEvent;
  }
  
  /**
   * Set message's receive event.
   *
   * @param receiveEvent receiving message occurrence specification
   */
  public void setReceiveEvent(UMessageOccurrenceSpecification receiveEvent) {
    this.receiveEvent = receiveEvent;
  }

  /**
   * @return receiving message occurrence specification
   */
  public @Nullable UMessageOccurrenceSpecification getReceiveEvent() {
    return receiveEvent;
  }

  /**
   * Add a predecessor message to this message.
   *
   * @param message a message
   */
  public void addPredecessor(UMessage message) {
    this.predecessors.add(message);
  }

  /**
   * @return message's predecessors
   */
  public Set<UMessage> getPredecessors() {
    return this.predecessors;
  }

  /**
   * @return message's transitive predecessors
   */
  public Set<UMessage> getTransitivePredecessors() {
    return this.getTransitivePredecessors(new HashSet<>());
  }

 /**
   * Determine message's transitive predecessors avoiding a set of
   * already visited predecessors.
   *
   * @param visited set of visited messages
   * @return message's transitive predecessors avoiding visited
   * predecessors
   */
  private Set<UMessage> getTransitivePredecessors(Set<UMessage> visited) {
    for (UMessage predecessor : this.predecessors) {
      if (!visited.contains(predecessor)) {
        visited.add(predecessor);
        predecessor.getTransitivePredecessors(visited);
      }
    }
    return visited;
  }

  /**
   * @return whether this message precedes another message
   */
  public boolean precedes(@Nullable UMessage other) {
    if (other == null)
      return false;
    return other.getTransitivePredecessors().contains(this);
  }

 /**
   * Determine message's type.
   * 
   * @return message's type
   */
  public UType getType(UContext context) {
    // Check whether all arguments have types
    List<UType> argumentTypes = new ArrayList<>();
    for (UExpression argument : this.arguments) {
      UType argumentType = argument.getType(context);
      if (argumentType.isError())
        return argumentType;
      argumentTypes.add(argumentType);
    }

    // Check whether the callee class shows a behavioural feature
    // with appropriate name and parameter types
    UBehavioural behavioural = context.getBehavioural(this.receiver.getC1ass(), this.behaviouralId, argumentTypes, context.getVoidType());
    if (behavioural == null)
      return UType.error("No matching behavioural element found for `" + UDescriptor.behavioural(this.receiver.getC1ass(), this.behaviouralId, argumentTypes) + "' in context `" + context.description() + "'");
    UType invocationType = behavioural.getType();
    if (invocationType.isError())
      return invocationType;

    // Ignoring return type of invocation
    return context.getVoidType();
  }
  
  /**
   * @return message's representation in UTE format
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    if (!this.getName().trim().equals("")) {
      resultBuilder.append(this.getName());
      resultBuilder.append(": ");
    }
    UObject sender = this.sender;
    if (sender == null)
      resultBuilder.append("unknown");
    else
      resultBuilder.append(sender.getName());
    resultBuilder.append(" -> ");
    resultBuilder.append(this.receiver.getName());
    resultBuilder.append(" : ");
    resultBuilder.append(this.behaviouralId);
    resultBuilder.append("(");
    resultBuilder.append(Formatter.separated(this.arguments, ", "));
    resultBuilder.append(");");

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getName(),
                        this.sender,
                        this.receiver,
                        this.behaviouralId,
                        this.arguments);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UMessage other = (UMessage)object;
      return Objects.equals(this.getName(), other.getName()) &&
             Objects.equals(this.interaction, other.interaction) &&
             Objects.equals(this.sender, other.sender) &&
             Objects.equals(this.receiver, other.receiver) &&
             Objects.equals(this.behaviouralId, other.behaviouralId) &&
             Objects.equals(this.arguments, other.arguments);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
