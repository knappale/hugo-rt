package uml.interaction;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;
import util.Identifier;


/**
 * Consider combined fragment.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class UConsiderFragment extends UCombinedFragment {
  private Set<Identifier> considers;

  /**
   * Create a new consider fragment.
   *
   * @param enclosingInteraction consider fragment's enclosing interaction
   */
  public UConsiderFragment(Set<Identifier> considers, UInteraction enclosingInteraction) {
    super(enclosingInteraction);
    this.considers = considers;
  }

  /**
   * Create a new consider fragment.
   *
   * @param enclosingOperand consider fragment's enclosing interaction operand
   */
  public UConsiderFragment(Set<Identifier> considers, UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
    this.considers = considers;
  }

  /**
   * @return consider fragment's set of considered message identifiers
   */
  public Set<Identifier> getConsiders() {
    return this.considers;
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {
    return visitor.onConsider(this.getOperands().get(0), this.considers);
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("consider <");
    resultBuilder.append(Formatter.separated(this.getConsiders(), ", "));
    resultBuilder.append("> {\n");
    resultBuilder.append(this.getOperands().get(0).declaration(nextPrefix));
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("ConsiderFragment [considereds = ");
    resultBuilder.append(Formatter.set(this.getConsiders()));
    resultBuilder.append(", operand = ");
    resultBuilder.append(this.getOperands().get(0));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
