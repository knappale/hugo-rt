package uml.interaction;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * This class represents a basic fragment, i.e. an interaction fragment only containing
 * basic elements like messages and general orderings. There is no explicit element
 * like this in the abstract syntax. Following the abstract syntax a basic fragment
 * is an interaction that only contains occurrence specifications.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class UBasicFragment extends UInteractionFragment {
  private Set<UOccurrenceSpecification> occurrences = new HashSet<>();

  /**
   * Create a new basic fragment.
   *
   * @param enclosingInteraction basic fragment's enclosing interaction
   */
  public UBasicFragment(UInteraction enclosingInteraction) {
    super(enclosingInteraction);
  }

  /**
   * Create a new basic fragment.
   *
   * @param enclosingOperand basic fragment's enclosing interaction operand
   */
  public UBasicFragment(UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
  }

  /**
   * Add a message occurrence specification to this basic fragment.
   * 
   * @param name message occurrence specification's name
   * @param kind message occurrence specification's kind
   * @param lifeline message occurrence specification's lifeline
   * @param message message occurrence specification's message
   */
  public UMessageOccurrenceSpecification addMessageOccurrenceSpecification(String name, UMessageOccurrenceSpecification.Kind kind, ULifeline lifeline, UMessage message) {
    UMessageOccurrenceSpecification uMessageOccurrenceSpecification = new UMessageOccurrenceSpecification(kind, name, lifeline, message, this);
    this.occurrences.add(uMessageOccurrenceSpecification);
    return uMessageOccurrenceSpecification;
  }

  /**
   * Add a termination occurrence specification to this basic fragment.
   * 
   * @param name termination occurrence specification's name
   * @param lifeline termination occurrence specification's lifeline
   */
  public UTerminationOccurrenceSpecification addTerminationOccurrenceSpecification(String name, ULifeline lifeline) {
    UTerminationOccurrenceSpecification uTerminationOccurrenceSpecification = new UTerminationOccurrenceSpecification(name, lifeline, this);
    this.occurrences.add(uTerminationOccurrenceSpecification);
    return uTerminationOccurrenceSpecification;
  }

  @Override
  public Set<UOccurrenceSpecification> getOccurrences() {
    return this.occurrences;
  }

  @Override
  public Set<ULifeline> getLifelines() {
    Set<ULifeline> lifelines = new HashSet<>();
    for (UOccurrenceSpecification occurrence : this.getOccurrences()) {
      lifelines.add(occurrence.getLifeline());
    }
    return lifelines;
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {
    return visitor.onBasic(this);
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";
    String next2Prefix = nextPrefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("basic {\n");
    boolean pending = false;
    for (ULifeline lifeline : this.getLifelines()) {
      List<UOccurrenceSpecification> lifelineOccurrences = lifeline.getOccurrences().stream().
          filter(occurrence -> this.getOccurrences().contains(occurrence)).collect(Collectors.toList());
      if (lifelineOccurrences.isEmpty())
        continue;

      if (pending)
        resultBuilder.append("\n");
      resultBuilder.append(nextPrefix);
      resultBuilder.append(lifeline.getName());
      resultBuilder.append(" {\n");
      for (UOccurrenceSpecification occurrence : lifelineOccurrences) {
        resultBuilder.append(next2Prefix);
        resultBuilder.append(occurrence.declaration());
        if (!occurrence.getPredecessors().isEmpty()) {
          resultBuilder.append(" after ");
          resultBuilder.append(Formatter.separated(occurrence.getPredecessors(), predecessor -> predecessor.declaration(), ", "));
        }
        resultBuilder.append(";\n");
      }
      resultBuilder.append(nextPrefix);
      resultBuilder.append("}\n");
      pending = true;
    }
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Basic [occurrences = ");
    resultBuilder.append(Formatter.set(this.occurrences));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
