package uml.interaction;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UContext;
import uml.UExpression;


/**
 * Contains a timing constraint.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 * @since 2005-09-04
 */
@NonNullByDefault
public class UTiming {
  public static enum Order {
    LT,
    LEQ,
    GT,
    GEQ;

    public String getName() {
      switch (this) {
        case LT:
          return " < ";
        case LEQ:
          return " <= ";
        case GT:
          return " > ";
        case GEQ:
          return " >= ";
      }
      throw new IllegalStateException("Unknown timing order.");
    }
  }

  private UInteraction interaction;
  private Order order;
  private Set<UOccurrenceSpecification> uppers = new HashSet<>();
  private Set<UOccurrenceSpecification> lowers = new HashSet<>();
  private UExpression bound;
 
  public static interface InContextVisitor<T> {
    public UContext getContext();

    public T onLt(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound);
    public T onLeq(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound);
    public T onGt(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound);
    public T onGeq(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound);
  }

  private UTiming(Order order, UExpression bound, UInteraction uInteraction) {
    this.order = order;
    this.bound = bound;
    this.interaction = uInteraction;
  }
  
  /**
   * Create a timing constraint representing an order
   *
   * @param uppers set of upper event occurrences
   * @param lowers set of event occurrences
   * @param bound bound expression
   * @param interaction enclosing interaction
   * @return a timing constraint
   *
   * @pre !uppers.isEmpty()
   * @pre forall o1, o2 \in uppers . o1.getData() equals o2.getData()
   * @pre !lowers.isEmpty()
   * @pre forall o1, o2 \in lowers . o1.getData() equals o2.getData()
   */  
  private static UTiming op(Order order, Set<UOccurrenceSpecification> uppers, Set<UOccurrenceSpecification> lowers, UExpression bound, UInteraction interaction) {
    UTiming t = new UTiming(order, bound, interaction);
    t.uppers.addAll(uppers);
    t.lowers.addAll(lowers);
    t.interaction = interaction;
    t.interaction.addTiming(t);
    return t;
  }

  /**
   * Create a timing constraint representing an order
   *
   * @param upper upper event occurrence
   * @param lower event occurrence
   * @param bound bound expression
   * @param interaction enclosing interaction
   * @return a timing constraint
   */  
  private static UTiming op(Order order, UOccurrenceSpecification upper, UOccurrenceSpecification lower, UExpression bound, UInteraction interaction) {
    Set<UOccurrenceSpecification> uppers = new HashSet<>();
    Set<UOccurrenceSpecification> lowers = new HashSet<>();
    lowers.add(lower);
    uppers.add(upper);
    return UTiming.op(order, uppers, lowers, bound, interaction);
  }

  /**
   * Create a timing constraint representing less-than.
   *
   * @param upper upper event occurrence
   * @param lower event occurrence
   * @param bound bound expression
   * @param interaction enclosing interaction
   * @return a timing constraint
   */  
  public static UTiming lt(UOccurrenceSpecification upper, UOccurrenceSpecification lower, UExpression bound, UInteraction interaction) {
    return UTiming.op(Order.LT, upper, lower, bound, interaction);
  }

  /**
   * Create a timing constraint representing less-than.
   *
   * @param uppers set of upper event occurrences
   * @param lowers set of event occurrences
   * @param bound bound expression
   * @param interaction enclosing interaction
   * @return a timing constraint
   *
   * @pre !uppers.isEmpty()
   * @pre forall o1, o2 \in uppers . o1.getData() equals o2.getData()
   * @pre !lowers.isEmpty()
   * @pre forall o1, o2 \in lowers . o1.getData() equals o2.getData()
   */  
  public static UTiming lt(Set<UOccurrenceSpecification> uppers, Set<UOccurrenceSpecification> lowers, UExpression bound, UInteraction interaction) {
    return UTiming.op(Order.LT, uppers, lowers, bound, interaction);
  }

  /**
   * Create a timing constraint representing less-or-equal.
   * 
   * @param upper upper event occurrence
   * @param lower lower event occurrence
   * @param bound bound expression
   * @param interaction enclosing interaction
   * @return a timing constraint
   */  
  public static UTiming leq(UOccurrenceSpecification upper, UOccurrenceSpecification lower, UExpression bound, UInteraction interaction) {
    return UTiming.op(Order.LEQ, upper, lower, bound, interaction);
  }

  /**
   * Create a timing constraint representing less-or-equal.
   * 
   * @param uppers set of upper event occurrences
   * @param lowers set of lower event occurrences
   * @param bound bound expression
   * @param interaction enclosing interaction
   * @return a timing constraint
   *
   * @pre !uppers.isEmpty()
   * @pre forall o1, o2 \in uppers . o1.getData() equals o2.getData()
   * @pre !lowers.isEmpty()
   * @pre forall o1, o2 \in lowers . o1.getData() equals o2.getData()
   */  
  public static UTiming leq(Set<UOccurrenceSpecification> uppers, Set<UOccurrenceSpecification> lowers, UExpression bound, UInteraction interaction) {
    return UTiming.op(Order.LEQ, uppers, lowers, bound, interaction);
  }

  /**
   * Create a timing constraint representing greater-than.
   *
   * @param upper upper event occurrence
   * @param lower event occurrence
   * @param bound bound expression
   * @param interaction enclosing interaction
   * @return a timing constraint
   */  
  public static UTiming gt(UOccurrenceSpecification upper, UOccurrenceSpecification lower, UExpression bound, UInteraction interaction) {
    return UTiming.op(Order.GT, upper, lower, bound, interaction);
  }

  /**
   * Create a timing constraint representing greater-than.
   *
   * @param uppers set of upper event occurrences
   * @param lowers set of event occurrences
   * @param bound bound expression
   * @param interaction enclosing interaction
   * @return a timing constraint
   *
   * @pre !uppers.isEmpty()
   * @pre forall o1, o2 \in uppers . o1.getData() equals o2.getData()
   * @pre !lowers.isEmpty()
   * @pre forall o1, o2 \in lowers . o1.getData() equals o2.getData()
   */  
  public static UTiming gt(Set<UOccurrenceSpecification> uppers, Set<UOccurrenceSpecification> lowers, UExpression bound, UInteraction interaction) {
    return UTiming.op(Order.GT, uppers, lowers, bound, interaction);
  }

  /**
   * Create a timing constraint representing greater-or-equal.
   * 
   * @param upper upper event occurrence
   * @param lower lower event occurrence
   * @param bound bound expression
   * @param interaction enclosing interaction
   * @return a timing constraint
   */  
  public static UTiming geq(UOccurrenceSpecification upper, UOccurrenceSpecification lower, UExpression bound, UInteraction interaction) {
    return UTiming.op(Order.GEQ, upper, lower, bound, interaction);
  }

  /**
   * Create a timing constraint representing greater-or-equal.
   * 
   * @param uppers set of upper event occurrences
   * @param lowers set of lower event occurrences
   * @param bound bound expression
   * @param interaction enclosing interaction
   * @return a timing constraint
   *
   * @pre !uppers.isEmpty()
   * @pre forall o1, o2 \in uppers . o1.getData() equals o2.getData()
   * @pre !lowers.isEmpty()
   * @pre forall o1, o2 \in lowers . o1.getData() equals o2.getData()
   */  
  public static UTiming geq(Set<UOccurrenceSpecification> uppers, Set<UOccurrenceSpecification> lowers, UExpression bound, UInteraction interaction) {
    return UTiming.op(Order.GEQ, uppers, lowers, bound, interaction);
  }

  /**
   * @return timing's interaction
   */
  public UInteraction getInteraction() {
    return this.interaction;
  }

  /**
   * @return timing's lower bounds
   */
  public Set<UOccurrenceSpecification> getLowers() {
    return this.lowers;
  }

  /**
   * @return timing's upper bounds
   */
  public Set<UOccurrenceSpecification> getUppers() {
    return this.uppers;
  }

  /**
   * @return timing's order relatino
   */
  public Order getOrder() {
    return this.order;
  }

  /**
   * Determine bound.
   *
   * @return bound expression
   */
  public UExpression getBound() {
    return this.bound;
  }

  /**
   * Determine bound context.
   * 
   * @return bound expression context
   */
  public UContext getBoundContext() {
    return UContext.collaborationConstant(this.getInteraction().getCollaboration());
  }

  /**
   * @return timing's lower bounds common data
   */
  public UOccurrenceSpecification.Data getLowerData() {
    return this.lowers.iterator().next().getData();
  }

  /**
   * @return timing's upper bounds common data
   */
  private UOccurrenceSpecification.Data getUpperData() {
    return this.uppers.iterator().next().getData();
  }

  /**
   * Accept a timing visitor.
   *
   * @param visitor a timing visitor
   */
  public <T> T accept(InContextVisitor<T> visitor) {
    switch (this.order) {
      case LT:
        return visitor.onLt(this.getUpperData(), this.getLowerData(), this.bound);
      case LEQ:
        return visitor.onLeq(this.getUpperData(), this.getLowerData(), this.bound);
      case GT:
        return visitor.onGt(this.getUpperData(), this.getLowerData(), this.bound);
      case GEQ:
        return visitor.onGeq(this.getUpperData(), this.getLowerData(), this.bound);
      default:
        throw new IllegalStateException("Unknown timing order.");
    }
  }

  /**
   * @return timing's representation in UTE format
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.getUpperData());
    resultBuilder.append("-");
    resultBuilder.append(this.getLowerData());
    resultBuilder.append(this.order.getName());
    resultBuilder.append(this.bound);
    resultBuilder.append(";");

    return resultBuilder.toString();
  }
 
  @Override
  public String toString() {
    return this.declaration();
  }
}
