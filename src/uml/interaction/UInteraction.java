package uml.interaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UCollaboration;
import uml.UContext;
import uml.UElement;
import uml.UObject;
import uml.ida.IAutomaton;
import uml.ocl.OTCTLFormula;
import util.Formatter;


/**
 * Class Interaction represents the outermost enclosing interaction. It also
 * contains the information that refers to the entire chart.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class UInteraction extends UElement {
  private UCollaboration uCollaboration;
  private Set<ULifeline> uLifelines = new HashSet<>();
  private Set<UMessage> uMessages = new HashSet<>();
  private Set<OTCTLFormula> assertions = new HashSet<>();
  private Set<UTiming> timings = new HashSet<>();
  private List<UInteractionFragment> uFragments = new LinkedList<>();

  private control.@Nullable Properties properties = null;
  private @Nullable IAutomaton automaton = null;

  /**
   * Create an interaction in a collaboration.
   *
   * @param name interaction's name
   * @param uCollaboration enclosing (UML) collaboration
   */
  public UInteraction(String name, UCollaboration uCollaboration) {
    super(name);
    this.automaton = null;
    this.uCollaboration = uCollaboration;
  }

  /**
   * @return interaction's collaboration
   */
  public UCollaboration getCollaboration() {
    return this.uCollaboration;
  }

  /**
   * Add an interaction fragment to this interaction.
   * 
   * @param fragment an interaction fragment
   */
  void addFragment(UInteractionFragment fragment) {
    this.automaton = null;
    this.uFragments.add(fragment);
  }

  /**
   * @return interaction's interaction fragments
   */
  public List<UInteractionFragment> getFragments() {
    return this.uFragments;
  }

  /**
   * Add a lifeline to this interaction.
   *
   * @param lifeline a lifeline
   */
  public void addLifeline(ULifeline lifeline) {
    this.automaton = null;
    this.uLifelines.add(lifeline);
  }

  /**
   * @return interaction's lifelines
   */
  public Set<ULifeline> getLifelines() {
    return this.uLifelines;
  }

  /**
   * Factory method for obtaining a basic fragment for this interaction.
   * If the last fragment in this interaction is a basic fragment, this will
   * be returned; otherwise a new basic fragment for this interaction is
   * returned.
   * 
   * @return a basic fragment
   */
  public UBasicFragment getBasicFragment() {
    if (!this.uFragments.isEmpty()) {
      UInteractionFragment lastFragment = this.uFragments.get(this.uFragments.size()-1);
      if (lastFragment instanceof UBasicFragment)
        return (UBasicFragment)lastFragment;
    }
    this.automaton = null;
    return new UBasicFragment(this);
  }

  /**
   * @return lifeline representing an (UML) object in this interaction, if any
   */
  public @Nullable ULifeline getLifeline(UObject object) {
    for (ULifeline lifeline : this.getLifelines())
      if (lifeline.getObject().equals(object))
        return lifeline;
    return null;
  }

  /**
   * Add a message to this interaction.
   *
   * @param message a (UML) message
   */
  public void addMessage(UMessage message) {
    this.automaton = null;
    this.uMessages.add(message);
  }

  /**
   * Add a collection of messages to this interaction.
   *
   * @param messages a collection of (UML) message
   */
  public void addMessages(Collection<UMessage> messages) {
    this.automaton = null;
    this.uMessages.addAll(messages);
  }

  /**
   * @return interaction's messages
   */
  public Set<UMessage> getMessages() {
    return this.uMessages;
  }

  /**
   * @return interaction's occurrence specifications
   */
  public Set<UOccurrenceSpecification> getOccurrences() {
    Set<UOccurrenceSpecification> occurrences = new HashSet<>();
    for (UInteractionFragment fragment : this.getFragments()) {
      occurrences.addAll(fragment.getOccurrences());
    }
    return occurrences;
  }

  /**
   * Add an assertion to this interaction.
   *
   * @param assertion a TCTL (OCL) assertion
   */
  public void addAssertion(OTCTLFormula assertion) {
    this.automaton = null;
    this.assertions.add(assertion);
  }

  /**
   * @return interaction's assertions
   */
  public Set<OTCTLFormula> getAssertions() {
    return this.assertions;
  }

  /**
   * Add a timing to this interaction.
   *
   * @param timing a timing
   */
  void addTiming(UTiming timing) {
    this.automaton = null;
    this.timings.add(timing);
  }

  /**
   * @return interaction's timings
   */
  public Set<UTiming> getTimings() {
    return this.timings;
  }

  /**
   * @return interaction's messages sorted in topological order with respect to
   * the predecessor relation
   */
  public List<UMessage> getSortedMessages() {
    List<UMessage> messages = new ArrayList<>();
    messages.addAll(this.getMessages());
    return util.Lists.topologicalSort(messages, new Comparator<@Nullable UMessage>() {
                                                 public int compare(@Nullable UMessage m1, @Nullable UMessage m2) {
                                                   if (m1 == null)
                                                     return 0;
                                                   if (m1.precedes(m2))
                                                     return -1;
                                                   if (m2 == null)
                                                     return 0;
                                                   if (m2.precedes(m1))
                                                     return 1;
                                                   return 0;
                                                 }
                                               });
  }

  /**
   * Determine message context.
   * 
   * @return message context
   */
  public UContext getMessageContext() {
    return UContext.interaction(this);
  }

  /**
   * Determine assertion context.
   * 
   * @return message context
   */
  public UContext getAssertionContext() {
    return UContext.interaction(this);
  }

  /**
   * Determine interaction's properties.
   *
   * @return interaction's properties
   */
  public control.Properties getProperties() {
    control.Properties properties = this.properties;
    if (properties != null)
      return properties;

    properties = this.getCollaboration().getProperties().getCopy();
    this.properties = properties;
    return properties;
  }

  /**
   * @return interaction's Ida representation
   */
  public IAutomaton getAutomaton() {
    /* 
     * Running the translation in a specifically created thread allows us to increase the size 
     * of the stack. If we don't do this, the translation of large loops will fail because of 
     * the deep recursion.
     */
    final int THREAD_STACK_SIZE = 1000000; // define the desired stack size in bytes

    IAutomaton automaton = this.automaton;
    if (automaton != null)
      return automaton;

    uml.ida.translation.ITranslator translator = new uml.ida.translation.ITranslator(this);
    Thread translatorThread = new Thread(new ThreadGroup(""), translator, "translator", THREAD_STACK_SIZE);
    try {
      translatorThread.start();
      translatorThread.join();
      automaton = translator.getAutomaton();
    }
    catch (InterruptedException e) {
      e.printStackTrace();
    }

    if (automaton == null)
      throw new IllegalStateException("Cannot translate interaction " + this.getName() + " into an Ida automaton.");
    this.automaton = automaton;
    return automaton;
  }

  /**
   * Turn interaction (showing only messages, but no interaction fragments) into an
   * interaction with a basic fragment for all messages.
   */
  public void makeBasicFragment() {
    this.automaton = null;

    Map<UObject, ULifeline> lifelines = new HashMap<>();
    UBasicFragment uBasicFragment = new UBasicFragment(this);
    for (UMessage uMessage : this.getSortedMessages()) {
      UObject sender = uMessage.getSender();
      if (sender == null)
        continue;
      ULifeline senderLifeline = lifelines.get(sender);
      if (senderLifeline == null) {
        senderLifeline = new ULifeline(sender, this);
        lifelines.put(sender, senderLifeline);
      }

      UObject receiver = uMessage.getReceiver();
      ULifeline receiverLifeline = lifelines.get(receiver);
      if (receiverLifeline == null) {
        receiverLifeline = new ULifeline(receiver, this);
        lifelines.put(receiver, receiverLifeline);
      }

      uBasicFragment.addMessageOccurrenceSpecification("snd_" + uMessage.getName(), UMessageOccurrenceSpecification.Kind.SEND, senderLifeline, uMessage);
      uBasicFragment.addMessageOccurrenceSpecification("rcv_" + uMessage.getName(), UMessageOccurrenceSpecification.Kind.RECEIVE, receiverLifeline, uMessage);
    }
  }

  /**
   * @param prefix prefix string
   * @return interaction's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";
    String next2Prefix = nextPrefix + "  ";
    String lineSep = "";

    resultBuilder.append(prefix);
    resultBuilder.append("interaction ");
    resultBuilder.append(this.getName());
    resultBuilder.append(" {\n");

    if (!this.getProperties().equals(this.getCollaboration().getProperties())) {
      resultBuilder.append(this.getProperties().declarationOfDiffering(getCollaboration().getProperties(), nextPrefix));
      lineSep = "\n\n";
    }

    List<UMessage> sortedMessages = this.getSortedMessages();
    if (!sortedMessages.isEmpty()) {
      for (UMessage message : sortedMessages) {
        resultBuilder.append(lineSep);
        resultBuilder.append(nextPrefix);
        resultBuilder.append(message.declaration());
        lineSep = "\n";
      }
      lineSep = "\n\n";
    }

    if (!this.uFragments.isEmpty()) {
      for (UInteractionFragment fragment : this.uFragments) {
        resultBuilder.append(lineSep);
        resultBuilder.append(fragment.declaration(nextPrefix));
        lineSep = "\n\n";
      }
      lineSep = "\n\n";
    }

    if (!this.timings.isEmpty()) {
      resultBuilder.append(lineSep);
      resultBuilder.append(nextPrefix);
      resultBuilder.append("timing {\n");
      for (UTiming timing : this.timings) {
        resultBuilder.append(next2Prefix);
        resultBuilder.append(timing.declaration());
        resultBuilder.append("\n");
      }
      resultBuilder.append(nextPrefix);
      resultBuilder.append("}");
      lineSep = "\n\n";
    }

    if (!this.assertions.isEmpty()) {
      resultBuilder.append(lineSep);
      resultBuilder.append(nextPrefix);
      resultBuilder.append("assert {\n");
      for (OTCTLFormula assertion : this.assertions) {
        resultBuilder.append(next2Prefix);
        resultBuilder.append(assertion.declaration());
        resultBuilder.append("\n");
      }
      resultBuilder.append(nextPrefix);
      resultBuilder.append("}");
    }

    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("Interaction [name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append(", messages = ");
    resultBuilder.append(Formatter.set(this.uMessages, uMessage -> uMessage.getName()));
    resultBuilder.append(", lifelines = ");
    resultBuilder.append(Formatter.set(this.uLifelines, uLifeline -> uLifeline.getName()));
    resultBuilder.append(", assertions = ");
    resultBuilder.append(Formatter.set(this.assertions));
    resultBuilder.append(", timings = ");
    resultBuilder.append(Formatter.set(this.timings));
    resultBuilder.append(", fragments = ");
    resultBuilder.append(Formatter.list(this.uFragments, uFragment -> uFragment.getName()));
    resultBuilder.append("]");
    
    return resultBuilder.toString();
  }
}
