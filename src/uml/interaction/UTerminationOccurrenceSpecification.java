package uml.interaction;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * TerminationOccurrenceSpecifications denote the termination of a lifeline.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class UTerminationOccurrenceSpecification extends UOccurrenceSpecification {
  /**
   * Create a termination occurrence specification.
   *
   * @param name termination occurrence specification's name
   * @param uLifeline termination occurrence specification's lifeline
   * @param uBasicFragment termination occurrence specification's basic fragment
   */
  UTerminationOccurrenceSpecification(String name, ULifeline uLifeline, UBasicFragment uBasicFragment) {
    super(name, uLifeline, uBasicFragment);
  }

  @Override
  public UOccurrenceSpecification.Data getData() {
    return UOccurrenceSpecification.Data.term(this.getName(), this.getLifeline().getName());
  }

  @Override
  public boolean isBefore(UOccurrenceSpecification other) {
    return false;
  }

  @Override
  public boolean matches(UOccurrenceSpecification.Data data) {
    if (!data.isTermination())
      return false;
    if (this.getLifeline().getName().equals(data.getLifelineName()))
      return true;
    return false;
  }

  @Override
  public <T> T accept(UOccurrenceSpecification.InContextVisitor<T> visitor) {
    return visitor.onTermination(this.getLifeline().getObject());
  }

  @Override
  public String getName() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("term_");
    resultBuilder.append(this.getLifeline().getName());
    return resultBuilder.toString();
  }

  @Override
  public String declaration() {
    return "term";
  }

  @Override
  public String toString() {
    return this.getName();
  }
}
