package uml.interaction.analysis;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Pair;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static util.Objects.requireNonNull;


/**
 * Difference Bound Matrices (DBMs).
 *
 * For the definition and the algorithms see Johan Bengtsson, Wang Yi:
 * Timed Automata: Semantics, Algorithms and Tools.  ACPN 2003.
 * LNCS 3098, 2004, pp. 87-124.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @since 2016-06-03
 *
 * @param <T> difference bound matrix' underlying type
 */
@NonNullByDefault
public class DifferenceBoundMatrix<T> {
  static enum Order {
    LT("<"),
    LEQ("<=");

    private String representation;

    private Order(String representation) {
      this.representation = representation;
    }

    boolean lt(Order other) {
      return this == LT && other == LEQ;
    }

    @Override
    public String toString() {
      return this.representation;
    }
  }

  static class Bound {
    private boolean unlimited = false;
    private Order order = Order.LEQ;
    private int integer = 0;

    private Bound() {
    }

    static Bound limited(Order order, int integer) {
      Bound b = new Bound();
      b.unlimited = false;
      b.order = order;
      b.integer = integer;
      return b;
    }

    static Bound unlimited() {
      Bound b = new Bound();
      b.unlimited = true;
      return b;
    }

    /**
     * Determine whether this bound is less than another bound.
     *
     * For the definition of "less than", see J. Bengtsson, W. Yi, op. cit., p. 101.
     *
     * @param other another bound
     * @return whether this bound is less than {@code other}
     */
    boolean lt(Bound other) {
      if (other.unlimited)
        return !this.unlimited;
      if (this.unlimited)
        return false;
      if (this.integer < other.integer)
        return true;
      return this.integer <= other.integer && this.order.lt(other.order);
    }

    boolean leq(Bound other) {
      return this.equals(other) || this.lt(other);
    }

    static Bound min(Bound first, Bound second) {
      return first.leq(second) ? first : second;
    }

    /**
     * For the definition of "addition", see J. Bengtsson, W. Yi, op. cit., p. 101.
     *
     * @param first a bound
     * @param second another bound
     * @return added bounds
     */
    static Bound add(Bound first, Bound second) {
      if (first.unlimited)
        return first;
      if (second.unlimited)
        return second;
      if (first.order == second.order)
        return Bound.limited(first.order, first.integer + second.integer);
      return Bound.limited(Order.LT, first.integer + second.integer);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Bound other = (Bound)object;
        return this.unlimited == other.unlimited &&
               this.order == other.order &&
               this.integer == other.integer;
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      if (this.unlimited) {
        builder.append(Order.LEQ);
        builder.append(" infty");
      }
      else {
        builder.append(this.order);
        builder.append(" ");
        builder.append(this.integer);
      }
      return builder.toString();
    }
  }

  private Map<Pair<T, T>, Bound> bounds = new HashMap<>();
  private boolean isCanonical = false;

  public DifferenceBoundMatrix() {
  }

  public static <U> DifferenceBoundMatrix<U> merge(DifferenceBoundMatrix<U> matrix1, DifferenceBoundMatrix<U> matrix2) {
    DifferenceBoundMatrix<U> merged = new DifferenceBoundMatrix<>();
    for (Pair<U, U> entry : matrix1.bounds.keySet())
      merged.put(entry.getFirst(), entry.getSecond(), requireNonNull(matrix1.bounds.get(entry)));
    for (Pair<U, U> entry : matrix2.bounds.keySet())
      merged.put(entry.getFirst(), entry.getSecond(), requireNonNull(matrix2.bounds.get(entry)));
    return merged;
  }

  public Bound get(T first, T second) {
    Bound bound = this.bounds.get(new Pair<>(first, second));
    if (bound != null)
      return bound;
    if (first != null && first.equals(second))
      return Bound.limited(Order.LEQ, 0);
    return Bound.unlimited();
  }

  public void put(T first, T second, Bound bound) {
    this.bounds.put(new Pair<>(first, second), Bound.min(bound, this.get(first, second)));
    this.isCanonical = false;
  }

  public Set<T> getUnderlying() {
    return this.bounds.keySet().stream().flatMap(pair -> Stream.of(pair.getFirst(), pair.getSecond())).collect(toSet());
  }

  /**
   * Canonicalize this canonical difference bound matrix.
   *
   * We currently use the all-pairs shortest path algorithm by Floyd and Warshall
   * (https://en.wikipedia.org/wiki/Floyd-Warshall_algorithm).
   *
   * @return the canonicalized difference bound matrix
   */
  public DifferenceBoundMatrix<T> canonical() {
    if (this.isCanonical)
      return this;

    Set<T> underlying = this.getUnderlying();
    for (T k : underlying) {
      for (T i : underlying) {
        for (T j : underlying) {
          Bound ij = this.get(i, j);
          Bound ik = this.get(i, k);
          Bound kj = this.get(k, j);
          this.put(i, j, Bound.min(ij, Bound.add(ik, kj)));
        }
      }
    }
    this.isCanonical = true;
    return this;
  }

  public Set<T> getInconsistencies() {
    canonical();
    return this.getUnderlying().stream().filter(t -> this.get(t, t).lt(Bound.limited(Order.LEQ, 0))).collect(toSet());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.bounds);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      DifferenceBoundMatrix<?> other = (DifferenceBoundMatrix<?>)object;
      this.canonical();
      other.canonical();
      return Objects.equals(this.bounds, other.bounds);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (Pair<T, T> entry : this.bounds.keySet().stream().sorted((p1, p2) -> ("" + p1.getFirst()).compareTo("" + p2.getFirst())).collect(toList())) {
      builder.append(entry.getFirst());
      builder.append(" - ");
      builder.append(entry.getSecond());
      builder.append(" ");
      builder.append(this.bounds.get(entry));
      builder.append("\n");
    }
    return builder.toString();
  }
}
