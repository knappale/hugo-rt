package uml.interaction.analysis;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.interaction.UBasicFragment;
import uml.interaction.UInteraction;
import uml.interaction.UInteractionFragment;
import uml.interaction.UInteractionOperand;
import uml.interaction.UOccurrenceSpecification;
import uml.interaction.UStateInvariant;
import uml.interaction.UTiming;
import uml.interaction.analysis.DifferenceBoundMatrix.Bound;
import uml.interaction.analysis.DifferenceBoundMatrix.Order;
import util.Identifier;
import util.UnlimitedNatural;

import static java.util.stream.Collectors.toSet;

import java.util.Arrays;


@NonNullByDefault
public class Analysis {
  private UInteraction interaction;
  private Set<DifferenceBoundMatrix<UOccurrenceSpecification>> matrices = new HashSet<>();

  public Analysis(UInteraction interaction) {
    this.interaction = interaction;
    this.matrices = new FragmentTranslator().translate(interaction);
    if (this.matrices.isEmpty())
      this.matrices.add(new DifferenceBoundMatrix<>());
    for (DifferenceBoundMatrix<UOccurrenceSpecification> matrix : this.matrices)
      this.addTimings(matrix);
  }

  private Analysis addTimings(DifferenceBoundMatrix<UOccurrenceSpecification> matrix) {
    for (UTiming uTiming : this.interaction.getTimings()) {
      int bound = uTiming.getBound().getIntegerValue(uTiming.getBoundContext());
      boolean swap = false;
      Order order = Order.LEQ;
      switch (uTiming.getOrder()) {
        case LT: {
          swap = false;
          order = Order.LT;
          break;
        }
        case LEQ: {
          swap = false;
          order = Order.LEQ;
          break;
        }
        case GEQ: {
          swap = true;
          order = Order.LEQ;
          break;
        }
        case GT: {
          swap = true;
          order = Order.LT;
          break;
        }
      }
      for (UOccurrenceSpecification upper : uTiming.getUppers()) {
        if (!matrix.getUnderlying().contains(upper))
          continue;

        for (UOccurrenceSpecification lower : uTiming.getLowers()) {
          if (!matrix.getUnderlying().contains(lower))
            continue;

          if (swap)
            matrix.put(lower, upper, Bound.limited(order, -bound));
          else
            matrix.put(upper, lower, Bound.limited(order, bound));
        }
      }
    }
    return this;
  }

  private static class FragmentTranslator implements UInteractionFragment.Visitor<Set<DifferenceBoundMatrix<UOccurrenceSpecification>>> {
    FragmentTranslator() {
    }

    Set<DifferenceBoundMatrix<UOccurrenceSpecification>> translate(UInteraction interaction) {
      return this.fragments(interaction.getFragments());
    }

    private Set<DifferenceBoundMatrix<UOccurrenceSpecification>> fragments(List<UInteractionFragment> fragments) {
      return fragments.stream().map(fragment -> fragment.accept(this)).reduce(new HashSet<>(), (matrices1, matrices2) ->
                 (matrices1.isEmpty() ? matrices2 :
                                        (matrices2.isEmpty() ? matrices1 :
                                                               matrices1.stream().flatMap(matrix1 -> matrices2.stream().map(matrix2 -> sequentialMerge(matrix1, matrix2))).collect(toSet()))));
    }

    private Set<DifferenceBoundMatrix<UOccurrenceSpecification>> operand(UInteractionOperand operand) {
      return this.fragments(operand.getFragments());
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onBasic(UBasicFragment basic) {
      DifferenceBoundMatrix<UOccurrenceSpecification> matrix = new DifferenceBoundMatrix<>();
      for (UOccurrenceSpecification uOccurrence : basic.getOccurrences()) {
        matrix.put(uOccurrence, uOccurrence, Bound.limited(Order.LEQ, 0));
        for (UOccurrenceSpecification prerequisite : uOccurrence.getPrerequisites())
          matrix.put(prerequisite, uOccurrence, Bound.limited(Order.LT, 0));
      }
      return new HashSet<>(Arrays.asList(matrix));
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onStateInvariant(UStateInvariant stateInvariant) {
      return new HashSet<>();
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onAlt(List<UInteractionOperand> operands) {
      return operands.stream().flatMap(operand -> operand(operand).stream()).collect(toSet());
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onPAlt(List<UInteractionOperand> operands) {
      return operands.stream().flatMap(operand -> operand(operand).stream()).collect(toSet());
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onIgnore(UInteractionOperand operand, Set<Identifier> ignores) {
      return operand(operand);
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onConsider(UInteractionOperand operand, Set<Identifier> ignores) {
      return operand(operand);
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onStrictLoop(UInteractionOperand operand, UnlimitedNatural min, UnlimitedNatural max) {
      return operand(operand);
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onLoop(UBasicFragment basic, UnlimitedNatural min, UnlimitedNatural max) {
      return onBasic(basic);
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onNot(UInteractionOperand operand) {
      return new HashSet<>();
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onOpt(UInteractionOperand operand) {
      return Stream.concat(operand(operand).stream(), Stream.of(new DifferenceBoundMatrix<UOccurrenceSpecification>())).collect(toSet());
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onPar(List<UInteractionOperand> operands) {
      return operands.stream().map(operand -> operand(operand)).reduce(new HashSet<>(Arrays.asList(new DifferenceBoundMatrix<>())), (matrices1, matrices2) ->
                 matrices1.stream().flatMap(matrix1 -> matrices2.stream().map(matrix2 -> DifferenceBoundMatrix.merge(matrix1, matrix2))).collect(toSet()));
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onSeq(List<UInteractionOperand> operands) {
      return operands.stream().map(operand -> operand(operand)).reduce(new HashSet<>(Arrays.asList(new DifferenceBoundMatrix<>())), (matrices1, matrices2) ->
                 matrices1.stream().flatMap(matrix1 -> matrices2.stream().map(matrix2 -> sequentialMerge(matrix1, matrix2))).collect(toSet()));
    }

    private DifferenceBoundMatrix<UOccurrenceSpecification> sequentialMerge(DifferenceBoundMatrix<UOccurrenceSpecification> matrix1, DifferenceBoundMatrix<UOccurrenceSpecification> matrix2) {
      DifferenceBoundMatrix<UOccurrenceSpecification> merged = DifferenceBoundMatrix.merge(matrix1, matrix2);
      for (UOccurrenceSpecification uOccurrence1 : matrix1.getUnderlying()) {
        for (UOccurrenceSpecification uOccurrence2 : matrix2.getUnderlying()) {
          if (uOccurrence1.getLifeline().equals(uOccurrence2.getLifeline()))
            merged.put(uOccurrence1, uOccurrence2, Bound.limited(Order.LT, 0));
        }
      }
      return merged;
    }

    @Override
    public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> onStrict(List<UInteractionOperand> operands) {
      return operands.stream().map(operand -> operand(operand)).reduce(new HashSet<>(Arrays.asList(new DifferenceBoundMatrix<>())), (matrices1, matrices2) ->
                 matrices1.stream().flatMap(matrix1 -> matrices2.stream().map(matrix2 -> strictMerge(matrix1, matrix2))).collect(toSet()));
    }

    private DifferenceBoundMatrix<UOccurrenceSpecification> strictMerge(DifferenceBoundMatrix<UOccurrenceSpecification> matrix1, DifferenceBoundMatrix<UOccurrenceSpecification> matrix2) {
      DifferenceBoundMatrix<UOccurrenceSpecification> merged = DifferenceBoundMatrix.merge(matrix1, matrix2);
      for (UOccurrenceSpecification uOccurrence1 : matrix1.getUnderlying()) {
        for (UOccurrenceSpecification uOccurrence2 : matrix2.getUnderlying()) {
          merged.put(uOccurrence1, uOccurrence2, Bound.limited(Order.LT, 0));
        }
      }
      return merged;
    }
  }

  public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> getMatrices() {
    return this.matrices;
  }

  public Set<DifferenceBoundMatrix<UOccurrenceSpecification>> getCanonicals() {
    return this.matrices.stream().map(matrix -> matrix.canonical()).collect(toSet());
  }
}
