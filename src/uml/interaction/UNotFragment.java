package uml.interaction;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Not combined fragment as a replacement of the neg combined fragment.
 *
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class UNotFragment extends UCombinedFragment {
  /**
   * Create a new not fragment.
   * 
   * @param enclosingInteraction not fragment's enclosing interaction
   */
  public UNotFragment(UInteraction enclosingInteraction) {
    super(enclosingInteraction);
  }

  /**
   * Create a new not fragment.
   * 
   * @param enclosingOperand not fragment's enclosing interaction operand
   */
  public UNotFragment(UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {
    return visitor.onNot(this.getOperands().get(0));
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("not {\n");
    resultBuilder.append(this.getOperands().get(0).declaration(nextPrefix));
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("NotFragment [operand = ");
    resultBuilder.append(this.getOperands().get(0));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
