package uml.interaction;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * Strict (sequential) combined fragment.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class UStrictFragment extends UCombinedFragment {
  /**
   * Create a new strict fragment.
   * 
   * @param enclosingInteraction strict fragment's enclosing interaction
   */
  public UStrictFragment(UInteraction enclosingInteraction) {
    super(enclosingInteraction);
  }

  /**
   * Create a new strict fragment.
   * 
   * @param enclosingOperand strict fragment's enclosing interaction operand
   */
  public UStrictFragment(UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {
    return visitor.onStrict(this.getOperands());
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("strict");
    for (UInteractionOperand operand : this.getOperands()) {
      resultBuilder.append(" {\n");
      resultBuilder.append(operand.declaration(nextPrefix));
      resultBuilder.append("\n");
      resultBuilder.append(prefix);
      resultBuilder.append("}");
    }

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("StrictFragment [operands = ");
    resultBuilder.append(Formatter.list(this.getOperands()));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
