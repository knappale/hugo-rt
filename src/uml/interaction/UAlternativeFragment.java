package uml.interaction;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * Alt(ernative) combined fragment.
 *
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 * @since 2004-12-12
 */
@NonNullByDefault
public class UAlternativeFragment extends UCombinedFragment {
  /**
   * Create a new alternative fragment.
   *
   * @param enclosingInteraction alternative fragment's enclosing interaction
   */
  public UAlternativeFragment(UInteraction interaction) {
    super(interaction);
  }

  /**
   * Create a new alternative fragment.
   *
   * @param enclosingOperand alternative fragment's enclosing interaction operand
   */
  public UAlternativeFragment(UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {
    if (this.getOperands().stream().anyMatch(operand -> operand.getRate() != null))
      return visitor.onPAlt(this.getOperands());

    return visitor.onAlt(this.getOperands());
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("alt");
    for (UInteractionOperand operand : this.getOperands()) {
      resultBuilder.append(" {\n");
      resultBuilder.append(operand.declaration(nextPrefix));
      resultBuilder.append("\n");
      resultBuilder.append(prefix);
      resultBuilder.append("}");
    }

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("AltFragment [operands = ");
    resultBuilder.append(Formatter.list(this.getOperands()));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
