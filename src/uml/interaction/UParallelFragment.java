package uml.interaction;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * Par(allel) combined fragment.
 *
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class UParallelFragment extends UCombinedFragment {
  /**
   * Create a new parallel fragment.
   * 
   * @param enclosingInteraction parallel fragment's enclosing interaction
   */
  public UParallelFragment(UInteraction enclosingInteraction) {
    super(enclosingInteraction);
  }

  /**
   * Create a new parallel fragment.
   * 
   * @param enclosingOperand parallel fragment's enclosing interaction operand
   */
  public UParallelFragment(UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {
    return visitor.onPar(this.getOperands());
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("par");
    for (UInteractionOperand operand : this.getOperands()) {
      resultBuilder.append(" {\n");
      resultBuilder.append(operand.declaration(nextPrefix));
      resultBuilder.append("\n");
      resultBuilder.append(prefix);
      resultBuilder.append("}");
    }

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("ParFragment [operands = ");
    resultBuilder.append(Formatter.list(this.getOperands()));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
