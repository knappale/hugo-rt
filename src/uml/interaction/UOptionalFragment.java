package uml.interaction;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Opt(ional) combined fragment.
 *
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class UOptionalFragment extends UCombinedFragment {
  /**
   * Create a new optional fragment.
   * 
   * @param enclosingInteraction optional fragment's enclosing interaction
   */
  public UOptionalFragment(UInteraction enclosingInteraction) {
    super(enclosingInteraction);
  }

  /**
   * Create a new optional fragment.
   * 
   * @param enclosingOperand optional fragment's enclosing interaction operand
   */
  public UOptionalFragment(UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {
    return visitor.onOpt(this.getOperands().get(0));
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("opt {\n");
    resultBuilder.append(this.getOperands().get(0).declaration(nextPrefix));
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("OptFragment [operand = ");
    resultBuilder.append(this.getOperands().get(0));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
