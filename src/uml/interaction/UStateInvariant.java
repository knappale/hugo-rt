package uml.interaction;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UExpression;
import util.Formatter;


/**
 * State invariant.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 * @since 2006-07-22
 */
@NonNullByDefault
public class UStateInvariant extends UInteractionFragment {
  private UExpression invariant;
  private Set<ULifeline> covereds = new HashSet<>();

  /**
   * Create a state invariant inside an interaction.
   *
   * @param invariant state invariant's invariant
   * @param enclosingInteraction state invariant's enclosing interaction
   */
  public UStateInvariant(UExpression invariant, UInteraction enclosingInteraction) {
    super(enclosingInteraction);
    this.invariant = invariant;
  }

  /**
   * Create a state invariant inside an interaction operand.
   *
   * @param invariant state invariant's invariant
   * @param enclosingOperand state invariant's enclosing interaction operand
   */
  public UStateInvariant(UExpression invariant, UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
    this.invariant = invariant;
  }

  /**
   * @return state invariant's invariant expression
   */
  public UExpression getInvariant() {
    return this.invariant;
  }

  /**
   * Add a covered lifeline to this state invariant.
   *
   * @param covered a lifeline to be added as covered
   */
  public void addLifeline(ULifeline covered) {
    this.covereds.add(covered);
  }

  @Override
  public Set<ULifeline> getLifelines() {
    return this.covereds;
  }

  @Override
  public Set<UOccurrenceSpecification> getOccurrences() {
    return new HashSet<>();
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {
    return visitor.onStateInvariant(this);
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("invariant <");
    resultBuilder.append(Formatter.separated(this.covereds, lifeline -> lifeline.getName(), ", "));
    resultBuilder.append("> {\n");
    resultBuilder.append(nextPrefix);
    resultBuilder.append(this.invariant.toString());
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("StateInvariant [covereds = ");
    resultBuilder.append(Formatter.set(this.covereds));
    resultBuilder.append(", invariant = ");
    resultBuilder.append(this.invariant);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
