package uml.interaction;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UBehavioural;
import uml.UExpression;
import uml.UObject;
import uml.UType;

import static util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;


/**
 * MessageOccurrenceSpecifications denote the sending or receiving of messages
 * on a lifeline.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class UMessageOccurrenceSpecification extends UOccurrenceSpecification {
  public static enum Kind {
    SEND,
    RECEIVE;

    public String getName() {
      switch (this) {
        case SEND:
          return "snd";
        case RECEIVE:
          return "rcv";
      }
      throw new IllegalStateException("Unknown message occurrence specification kind.");
    }
  }

  private Kind kind;
  private UMessage message;

  UMessageOccurrenceSpecification(Kind kind, String name, ULifeline uLifeline, UMessage uMessage, UBasicFragment uBasicFragment) {
    super(name, uLifeline, uBasicFragment);
    this.kind = kind;
    this.message = uMessage;
    switch (kind) {
      case SEND:
        this.message.setSendEvent(this);
        break;
      case RECEIVE:
        this.message.setReceiveEvent(this);
        break;
      default:
       throw new IllegalStateException("Unknown message occurrence specification kind.");
    }
  }

  /**
   * @return underlying message
   */
  public UMessage getMessage() {
    return this.message;
  }

  /**
   * @return opposite end of underlying message which may be {@code null}
   */
  public @Nullable UMessageOccurrenceSpecification getCorrespondingEvent() {
    switch (this.kind) {
      case SEND:
        return this.message.getReceiveEvent();
      case RECEIVE:
        return this.message.getSendEvent();
      default:
       throw new IllegalStateException("Unknown message occurrence specification kind.");
    }
  }

  @Override
  public boolean isBefore(UOccurrenceSpecification other) {
    return this.isSending() && other.equals(this.getCorrespondingEvent());
  }

  /**
   * @return whether this message occurrence specification represents a send event
   */
  public boolean isSending() {
    if (this.message.getSendEvent() == this)
      return true;
    return false;
  }

  /**
   * @return whether this message occurrence specification represents a receive event
   */
  public boolean isReceiving() {
    if (this.message.getReceiveEvent() == this)
      return true;
    return false;
  }

  /**
   * @return message occurrence specification's active object
   */
  public @Nullable UObject getActiveObject() {
    if (this.isSending())
      return this.message.getSender();
    if (this.isReceiving())
      return this.message.getReceiver();
    return null;
  }

  /**
   * @return message occurrence specification's corresponding object
   */
  public @Nullable UObject getCorrespondingObject() {
    if (this.isSending())
      return this.message.getReceiver();
    if (this.isReceiving())
      return this.message.getSender();
    return null;
  }

  /**
   * @see uml.interaction.UOccurrenceSpecification#getPrerequisites()
   * 
   * <p>A receiving message occurrence specification has as prerequisites,
   * additional to its prerequisites as an occurrence specification, also its
   * corresponding sending occurrence specification and the corresponding
   * occurrence specification's prerequisites.</p>
   */
  @Override
  public Set<UOccurrenceSpecification> getPrerequisites() {
    Set<UOccurrenceSpecification> prerequisites = super.getPrerequisites();

    if (this.isReceiving()) {
      UBasicFragment uBasicFragment = this.getUBasicFragment();
      UOccurrenceSpecification correspondingOccurrence = this.getCorrespondingEvent();
      if (correspondingOccurrence != null && uBasicFragment != null && uBasicFragment.getOccurrences().contains(correspondingOccurrence)) {
        prerequisites.add(correspondingOccurrence);
        prerequisites.addAll(correspondingOccurrence.getPrerequisites());
      }
    }

    return prerequisites;
  }

  @Override
  public UOccurrenceSpecification.Data getData() {
    switch (this.kind) {
      case SEND:
        return UOccurrenceSpecification.Data.send(this.getName(), this.message.getName());
      case RECEIVE:
        return UOccurrenceSpecification.Data.receive(this.getName(), this.message.getName());
      default:
       throw new IllegalStateException("Unknown message occurrence specification kind.");
    }
  }

  @Override
  public boolean matches(UOccurrenceSpecification.Data data) {
    if (!data.isMessage())
      return false;
    if (data.getKind() == this.kind && this.message.getName().equals(data.getMessageName()))
      return true;
    return false;
  }

  @Override
  public <T> T accept(UOccurrenceSpecification.InContextVisitor<T> visitor) {
    UMessage uMessage = this.getMessage();
    List<UType> argumentTypes = new ArrayList<>();
    for (UExpression argument : uMessage.getArguments()) {
      UType argumentType = argument.getType(visitor.getContext());
      argumentTypes.add(argumentType);
    }
    UBehavioural uBehavioural = requireNonNull(visitor.getContext().getBehavioural(uMessage.getReceiver().getC1ass(), uMessage.getBehaviouralId(), argumentTypes, visitor.getContext().getVoidType()));
    List<UExpression> uArguments = uMessage.getArguments();
    if (this.isSending())
      return visitor.onSend(requireNonNull(uMessage.getSender()), uBehavioural, uArguments, requireNonNull(uMessage.getReceiver()));
    if (this.isReceiving())
      return visitor.onReceive(requireNonNull(uMessage.getSender()), uBehavioural, uArguments, requireNonNull(uMessage.getReceiver()));
    throw new IllegalStateException("Unknown message occurrence specification kind.");
  }

  @Override
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.kind.getName());
    resultBuilder.append("(");
    resultBuilder.append(this.message.getName());
    resultBuilder.append(")");
    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getLifeline());
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UMessageOccurrenceSpecification other = (UMessageOccurrenceSpecification)object;
      return Objects.equals(this.getLifeline(), other.getLifeline()) &&
             this.kind == other.kind &&
             Objects.equals(this.message, other.message);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
