package uml.interaction;

import java.util.*;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Base class for all combined fragments, which include an interaction operator.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 *
 */
@NonNullByDefault
public abstract class UCombinedFragment extends UInteractionFragment {
  private List<UInteractionOperand> interactionOperands = new LinkedList<>();

  /**
   * Create a new combined fragment.
   *
   * @param enclosingInteraction combined fragment's enclosing interaction
   */
  public UCombinedFragment(UInteraction enclosingInteraction) {
    super(enclosingInteraction);
  }

  /**
   * Create a new combined fragment.
   *
   * @param enclosingOperand combined fragment's enclosing interaction operand
   */
  public UCombinedFragment(UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
  }

  /**
   * Add an interaction operand to this operator.
   * 
   * @param interactionOperand
   */
  public void addOperand(UInteractionOperand interactionOperand) {
    this.interactionOperands.add(interactionOperand);
  }

  /**
   * @return combined fragment's interaction operands
   */
  public List<UInteractionOperand> getOperands() {
    return this.interactionOperands;
  }

  @Override
  public Set<UOccurrenceSpecification> getOccurrences() {
    Set<UOccurrenceSpecification> occurrences = new HashSet<>();
    for (UInteractionOperand operand : this.getOperands())
      occurrences.addAll(operand.getOccurrences());
    return occurrences;
  }

  @Override
  public Set<ULifeline> getLifelines() {
    Set<ULifeline> lifelines = new HashSet<>();
    for (UInteractionOperand operand : this.getOperands())
      lifelines.addAll(operand.getLifelines());
    return lifelines;
  }
}
