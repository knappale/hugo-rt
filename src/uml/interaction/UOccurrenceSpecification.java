package uml.interaction;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UBehavioural;
import uml.UContext;
import uml.UElement;
import uml.UExpression;
import uml.UObject;

import static util.Objects.requireNonNull;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;


/**
 * Occurrence specifications specify everything that can happen
 * on a lifeline.
 *
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public abstract class UOccurrenceSpecification extends UElement {
  private ULifeline uLifeline;
  private UBasicFragment uBasicFragment;
  private Set<UOccurrenceSpecification> predecessors = new HashSet<>();

  public static interface InContextVisitor<T> {
    public UContext getContext();

    public T onSend(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver);
    public T onReceive(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver);
    public T onTermination(UObject object);
  }

  public static class Data {
    private String name;
    private @Nullable String lifelineName;
    private UMessageOccurrenceSpecification.@Nullable Kind kind;
    private @Nullable String messageName;

    private Data(String name) {
      this.name = name;
    }

    public static Data send(String name, String messageName) {
      Data d = new Data(name);
      d.messageName = messageName;
      d.kind = UMessageOccurrenceSpecification.Kind.SEND;
      return d;
    }

    public static Data receive(String name, String messageName) {
      Data d = new Data(name);
      d.messageName = messageName;
      d.kind = UMessageOccurrenceSpecification.Kind.RECEIVE;
      return d;
    }

    public static Data term(String name, String lifelineName) {
      Data d = new Data(name);
      d.lifelineName = lifelineName;
      return d;
    }

    public String getName() {
      return this.name;
    }
  
    public boolean isTermination() {
      return this.messageName == null;
    }

    public boolean isMessage() {
      return this.messageName != null;
    }

    public UMessageOccurrenceSpecification.@Nullable Kind getKind() {
      return this.kind;
    }

    public @Nullable String getMessageName() {
      return this.messageName;
    }

    public @Nullable String getLifelineName() {
      return this.lifelineName;
    }

    public int hashCode() {
      return Objects.hash(this.lifelineName,
                          this.messageName);
    }
  
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Data other = (Data)object;
        return Objects.equals(this.lifelineName, other.lifelineName) &&
               Objects.equals(this.messageName, other.messageName) &&
               this.kind == other.kind;
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();

      if (this.isTermination()) {
        resultBuilder.append("term(");
        resultBuilder.append(this.getLifelineName());
        resultBuilder.append(")");
        return resultBuilder.toString();
      }

      if (this.isMessage()) {
        resultBuilder.append(requireNonNull(this.getKind()).getName());
        resultBuilder.append("(");
        resultBuilder.append(this.getMessageName());
        resultBuilder.append(")");
        return resultBuilder.toString();
      }

      resultBuilder.append("unknown");
      return resultBuilder.toString();
    }
  }

  UOccurrenceSpecification(String name, ULifeline uLifeline, UBasicFragment uBasicFragment) {
    super(name);
    this.uLifeline = uLifeline;
    this.uLifeline.addOccurrence(this);
    this.uBasicFragment = uBasicFragment;
  }

  /**
   * @return occurrence specification's enclosing interaction
   */
  public UInteraction getInteraction() {
    return this.getUBasicFragment().getInteraction();
  }

  /**
   * Set occurrence specification's lifeline.
   *
   * @param lifeline a lifeline
   */
  public void setLifeline(ULifeline lifeline) {
    if (!this.uLifeline.equals(lifeline)) {
      this.uLifeline.removeOccurrence(this);
      this.uLifeline = lifeline;
      this.uLifeline.addOccurrence(this);
    }
  }

  /**
   * @return occurrence specification's lifeline
   */
  public ULifeline getLifeline() {
    return this.uLifeline;
  }

  /**
   * @return occurrence specification's basic interaction
   */
  public UBasicFragment getUBasicFragment() {
    return this.uBasicFragment;
  }

  /**
   * Add an occurrence specification to the predecessors of this occurrence specification.
   *
   * @param occurrence an occurrence specification
   */
  public void addPredecessor(UOccurrenceSpecification occurrence) {
    this.predecessors.add(occurrence);
  }

  /**
   * @return occurrence specification's predecessors
   */
  public Set<UOccurrenceSpecification> getPredecessors() {
    return this.predecessors;
  }

  /**
   * @return occurrence specification's lifeline predecessor in the containing
   * basic fragment, or {@code null} if there is no such predecessor
   */
  public @Nullable UOccurrenceSpecification getLifelinePredecessor() {
    UBasicFragment uBasicFragment = this.getUBasicFragment();
    ULifeline lifeline = this.getLifeline();
    int index = lifeline.getOccurrences().indexOf(this);
    if (index > 0) {
      UOccurrenceSpecification predecessor = lifeline.getOccurrences().get(index-1);
      if (uBasicFragment != null && uBasicFragment.getOccurrences().contains(predecessor))
        return predecessor;
    }
    return null;
  }

  /**
   * @return true iff this event is the first event on the lifeline
   */
  public boolean isFirstEvent() {
    return this.getLifelinePredecessor() == null;
  }

  /**
   * @return occurrence specification's lifeline successor in the containing
   * basic fragment, or {@code null} if there is no such successor
   */
  public @Nullable UOccurrenceSpecification getLifelineSuccessor() {
    UBasicFragment uBasicFragment = this.getUBasicFragment();
    ULifeline lifeline = this.getLifeline();
    int index = lifeline.getOccurrences().indexOf(this);
    if (index+1 < lifeline.getOccurrences().size()) {
      UOccurrenceSpecification successor = lifeline.getOccurrences().get(index+1);
      if (uBasicFragment != null && uBasicFragment.getOccurrences().contains(successor))
        return successor;
    }
    return null;
  }

  /**
   * @return true iff this event is the last event on the lifeline
   */
  public boolean isLastEvent() {
    return this.getLifelineSuccessor() == null;
  }

  /**
   * Determine occurrence specification's prerequisites inside its basic
   * interaction, i.e., all those occurrence specifications that must have
   * occurred before this occurrence specification is allowed to happen.
   * 
   * <p>
   * Implementing subclasses generally should redefine this method.
   * 
   * @param occurrence an occurrence specification
   * @return prerequisites of <code>occurrence</code>
   */
  public Set<UOccurrenceSpecification> getPrerequisites() {
    UBasicFragment uBasicFragment = this.getUBasicFragment();
    Set<UOccurrenceSpecification> prerequisites = new HashSet<>();
    UOccurrenceSpecification lifelinePredecessor = this.getLifelinePredecessor();
    if (lifelinePredecessor != null)
      prerequisites.add(lifelinePredecessor);
    for (UOccurrenceSpecification predecessor : this.getPredecessors())
      if (uBasicFragment != null && uBasicFragment.getOccurrences().contains(predecessor))
        prerequisites.add(predecessor);
    return prerequisites;
  }

  /**
   * @return occurrence specification's prerequisites' lifelines
   */
  public Set<ULifeline> getPrerequisitesLifelines() {
    Set<ULifeline> lifelines = new HashSet<>();
    for (UOccurrenceSpecification prerequisite : this.getPrerequisites())
      lifelines.add(prerequisite.getLifeline());
    return lifelines;
  }

  /**
   * Determine whether this occurrence specification should occur before
   * another occurrence specification.
   *
   * @param other another occurrence specification
   * @return whether this occurrence specification should occur before
   * another occurrence specification
   */
  public abstract boolean isBefore(UOccurrenceSpecification other);

  /**
   * Determine this occurrence specification's data.
   * 
   * @return this occurrence specification's data
   */
  public abstract Data getData();

  /**
   * Determine whether this occurrence specification matches some occurrence
   * specification data.
   * 
   * @param data occurrence specification data
   * @return whether this occurrence specification matches some occurrence specification data
   */
  public abstract boolean matches(Data data);

  /**
   * Accept an occurrence specification in context visitor.
   *
   * @param visitor a visitor
   */
  public abstract <T> T accept(InContextVisitor<T> visitor);

  /**
   * Determine occurrence specification's string representation in UTE format.
   *
   * @return occurrence specification's string representation in UTE format
   */
  public abstract String declaration();

  @Override
  public abstract String toString();
}
