package uml.interaction;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UElement;
import uml.UObject;
import util.Formatter;


/**
 * A lifeline represents one instance in the sequence diagram. It
 * collects all relevant information of occurrence specifications on this instance.
 *
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class ULifeline extends UElement {
  private UInteraction uInteraction;
  private UObject uObject;
  private List<UOccurrenceSpecification> occurrences = new LinkedList<>();

  public ULifeline(UObject uObject, UInteraction interaction) {
    super(uObject.getName());
    this.uObject = uObject;
    this.uInteraction = interaction;
    this.uInteraction.addLifeline(this);
  }

  /**
   * @return lifeline's underlying (UML) object
   */
  public UObject getObject() {
    return this.uObject;
  }

  /**
   * @return lifeline's containing (UML) interaction
   */
  public UInteraction getUInteraction() {
    return this.uInteraction;
  }

  /**
   * Append an occurrence specification to the lifeline's list of events.
   * 
   * @param occurrence an occurrence specification
   */
  public void addOccurrence(UOccurrenceSpecification occurrence) {
    if (!this.getOccurrences().contains(occurrence)) {
      occurrence.setLifeline(this);
      this.occurrences.add(occurrence);
    }
  }

  /**
   * Remove an occurrence specification from the lifeline's list of events.
   *
   * @param occurrence an occurrence specification
   */
  public void removeOccurrence(UOccurrenceSpecification occurrence) {
    if (this.getOccurrences().contains(occurrence))
      this.occurrences.remove(occurrence);
  }

  /**
   * @return lifeline's occurrence specifications
   */
  public List<UOccurrenceSpecification> getOccurrences() {
    return this.occurrences;
  }

  /**
   * Determine the occurrence specifications on this lifeline in a set
   * of occurrence specifications.
   *
   * @param occurrences a set of occurrence specifications
   * @return the set of occurrence specifications on this lifeline in
   * the set <b>occurrences</b>
   */
  public Set<UOccurrenceSpecification> getOccurrences(Set<UOccurrenceSpecification> occurrences) {
    Set<UOccurrenceSpecification> lifelineOccurrences = new HashSet<>();
    for (UOccurrenceSpecification occurrence : occurrences)
      if (this.equals(occurrence.getLifeline()))
        lifelineOccurrences.add(occurrence);
    return lifelineOccurrences;
  }

  /**
   * Determine the first occurrence specification on this lifeline in a set of occurrence
   * specifications.
   *
   * @param occurrences a set of occurrence specifications
   * @return the first occurrence specification on this lifeline in the set {@code occurrences},
   * or {@code null} if there is none
   */
  public @Nullable UOccurrenceSpecification getFirst(Set<UOccurrenceSpecification> occurrences) {
    for (UOccurrenceSpecification occurrence : occurrences) {
      if (!this.equals(occurrence.getLifeline()))
        continue;
      if (occurrence.isFirstEvent())
        return occurrence;
    }
    return null;
  }

  /**
   * Determine the last occurrence specification on this lifeline in a set of occurrence
   * specifications.
   *
   * @param occurrences a set of occurrence specifications
   * @return the last occurrence specification on this lifeline in the set {@code occurrences},
   * or {@code null} if there is none
   */
  public @Nullable UOccurrenceSpecification getLast(Set<UOccurrenceSpecification> occurrences) {
    for (UOccurrenceSpecification occurrence : occurrences) {
      if (!this.equals(occurrence.getLifeline()))
        continue;
      if (occurrence.isLastEvent())
        return occurrence;
    }
    return null;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.uObject.getName());
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      ULifeline other = (ULifeline)object;
      return Objects.equals(this.uObject, other.uObject);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Lifeline [name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append(", occurrences = ");
    resultBuilder.append(Formatter.list(this.occurrences));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
