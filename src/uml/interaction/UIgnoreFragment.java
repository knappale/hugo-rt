package uml.interaction;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;
import util.Identifier;


/**
 * Ignore combined fragment.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de>Jochen Wuttke</A>
 *
 */
@NonNullByDefault
public class UIgnoreFragment extends UCombinedFragment {
  private Set<Identifier> ignores;

  /**
   * Create a new ignore fragment.
   *
   * @param enclosingInteraction ignore fragment's enclosing interaction
   */
  public UIgnoreFragment(Set<Identifier> ignores, UInteraction enclosingInteraction) {
    super(enclosingInteraction);
    this.ignores = ignores;
  }

  /**
   * Create a new ignore fragment.
   *
   * @param enclosingOperand ignore fragment's enclosing interaction operand
   */
  public UIgnoreFragment(Set<Identifier> ignores, UInteractionOperand enclosingOperand) {
    super(enclosingOperand);
    this.ignores = ignores;
  }

  /**
   * @return ignore fragment's set of ignored message identifiers
   */
  public Set<Identifier> getIgnores() {
    return this.ignores;
  }

  @Override
  public <T> T accept(UInteractionFragment.Visitor<T> visitor) {    
    return visitor.onIgnore(this.getOperands().get(0), this.ignores);
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("ignore <");
    resultBuilder.append(Formatter.separated(this.getIgnores(), ", "));
    resultBuilder.append("> {\n");
    resultBuilder.append(this.getOperands().get(0).declaration(nextPrefix));
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("IgnoreFragment [ignoreds = ");
    resultBuilder.append(Formatter.set(this.getIgnores()));
    resultBuilder.append(", operand = ");
    resultBuilder.append(this.getOperands().get(0));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
