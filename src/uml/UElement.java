package uml;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * A UML element.
 *
 * An element is a UML entity that can be referred to by a name.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class UElement {
  private String name = "";

  /**
   * Creates a new UML element
   *
   * @param name the name of the UML element
   */
  public UElement(String name) {
    this.name = name;
  }

  /**
   * @return element's name
   */
  public String getName() {
    return this.name;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    return (this == object);
  }
}
