package uml;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Contains data of a slot.
 *
 * @author Alexander Knapp (<A HREF="mailto:knapp@informatik.uni-muenchen.de">knapp@informatik.uni-muenchen.de</A>)
 */
@NonNullByDefault
public class USlot {
  private UAttribute attribute;
  private List<UExpression> values = new ArrayList<UExpression>();

  /**
   * Create a slot for an attribute
   *
   * @param attribute underlying attribute
   * @return a slot
   */
  public USlot(UAttribute attribute) {
    this.attribute = attribute;
  }

  /**
   * Create a slot for an attribute with a value
   *
   * @param attribute underlying attribute
   * @param value value expression
   * @return a slot
   */
  public USlot(UAttribute attribute, UExpression value) {
    this(attribute);
    this.values.add(value);
  }
  
  /**
   * Create a slot for an attribute with a list of values
   *
   * @param attribute underlying attribute
   * @param values list of value expressions
   * @return a slot
   */
  public USlot(UAttribute attribute, List<UExpression> values) {
    this(attribute);
    this.values.addAll(values);
  }

  /**
   * @return slot's underlying attribute
   */
  public UAttribute getAttribute() {
    return attribute;
  }

  /**
   * Add a value to this slot.
   *
   * @param value a value expression
   */
  public void addValue(UExpression value) {
    this.values.add(value);
  }

  /**
   * @return slot's values
   */
  public List<UExpression> getValues() {
    return this.values;
  }

  /**
   * @return slot's values context
   */
  public UContext getValuesContext() {
    UModel model = getAttribute().getOwner().getModel();
    for (UCollaboration collaboration : model.getCollaborations()) {
      for (UObject object : collaboration.getObjects()) {
        if (object.getSlots().contains(this))
          return UContext.collaboration(collaboration);
      }
    }
    return UContext.model(model);
  }

  /**
   * @return slots (artificial) name, i.e. the name of the
   * corresponding attribute
   */
  public String getName() {
    return getAttribute().getName();
  }

  /**
   * @param prefix prefix string
   * @return slot's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(prefix);
    resultBuilder.append(this.getAttribute().getName());
    resultBuilder.append(" = ");
    if (this.getValues().size() > 1 || this.getAttribute().getSize() > 1) {
      resultBuilder.append("{ ");
      String sep = "";
      for (UExpression value : this.getValues()) {
        resultBuilder.append(sep);
        resultBuilder.append(value.toString());
        sep = ", ";
      }
      resultBuilder.append(" };");
    }
    else {
      resultBuilder.append(this.getValues().get(0).toString());
      resultBuilder.append(";");
    }
   
    return resultBuilder.toString();
  }
    
  /**
   * @return slot's representation in UTE format
   */
  public String declaration() {
    return this.declaration("");
  }
  
  /**
   * @return slot's string representation
   */
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Slot [attribute = ");
    resultBuilder.append(getAttribute().getName());
    resultBuilder.append(", values = ");
    resultBuilder.append(getValues());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
