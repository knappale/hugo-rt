package uml;


/**
 * UML expression operators
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public enum UOperator {
  UPLUS,
  UMINUS,
  ADD,
  SUB,
  MULT,
  DIV,
  MOD,
  LT,
  LEQ,
  EQ,
  NEQ,
  GEQ,
  GT,
  TRUE,
  FALSE,
  UNEG,
  AND,
  OR;

  public String getName() {
    switch (this) {
      case UPLUS: return "+";
      case UMINUS: return "-";
      case ADD: return "+";
      case SUB: return "-";
      case MULT: return "*";
      case DIV: return "/";
      case MOD: return "%";
      case LT: return " < ";
      case LEQ: return " <= ";
      case EQ: return " == ";
      case NEQ: return " != ";
      case GEQ: return " >= ";
      case GT: return " > ";
      case TRUE: return "true";
      case FALSE: return "false";
      case UNEG: return "!";
      case AND: return " && ";
      case OR: return " || ";
      default: return "[unknown]";
    }
  }

  public static boolean precedes(UOperator op1, UOperator op2) {
    return (op1.getPrecedence() > op2.getPrecedence()) ||
           (op1 == op2 && op1.isAssociative());
  }

  private int getPrecedence() {
    switch (this) {
      case TRUE:
      case FALSE:
        return 24;
      case UPLUS:
      case UMINUS:
      case UNEG:
        return 22;
      case MULT:
      case DIV:
      case MOD:
        return 20;
      case SUB:
        return 19;
      case ADD:
        return 18;
      case LT:
      case LEQ:
      case GEQ:
      case GT:
        return 16;
      case EQ:
      case NEQ:
        return 14;
      case AND:
      case OR:
        return 10;
      default:
        return 0;
    }
  }

  public boolean isAssociative() {
    switch (this) {
      case UPLUS:
      case UMINUS:
      case UNEG:
      case MULT:
      case ADD:
      case AND:
      case OR:
        return true;
      //$CASES-OMITTED$
      default:
        return false;
    }
  }

  public boolean isComparison() {
    switch (this) {
      case LT:
      case LEQ:
      case EQ:
      case NEQ:
      case GEQ:
      case GT:
        return true;
      //$CASES-OMITTED$
      default:
        return false;
    }
  }

  public boolean isInequality() {
    switch (this) {
      case LT:
      case NEQ:
      case GT:
        return true;
      //$CASES-OMITTED$
      default:
        return false;
    }
  }

  /**
   * Determine whether this operator is the complement of another operator, i.e.,
   * whether {@code this} and {@code other} are relational operators such that
   * x {@code this} y implies that {@code !}x {@code other} y
   *
   * @param other another operator
   * @return whether {@code this} is a complement of {@code other}
   */
  public boolean isComplement(UOperator other) {
    switch (this) {
      case LT:
        return other == EQ || other == GEQ || other == GT;
      case LEQ:
        return other == GT;
      case EQ:
        return other == LT || other ==  NEQ || other == GT;
      case NEQ:
        return other ==  EQ;
      case GEQ:
        return other == LT;
      case GT:
        return other == LT || other == LEQ || other == EQ;
      default: return false;
      }
  }
}
