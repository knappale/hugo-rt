package uml;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Identifier;

import static java.util.stream.Collectors.toList;

import static util.Objects.requireNonNull;
import static util.Formatter.context;
import static util.Formatter.quoted;
import static util.Formatter.type;


/**
 * UML action
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UAction {
  static enum Kind {
    SKIP,
    SEQ,
    PAR,
    CHOICE,
    CONDITIONAL,
    ASSERTION,
    INVOCATION,
    ARRAYINVOCATION,
    ASSIGNMENT,
    ARRAYASSIGNMENT,
    POST,
    ARRAYPOST;
  }

  public static enum PostKind {
    INC,
    DEC;

    public String toString() {
      switch (this) {
        case INC: return "++";
        case DEC: return "--";
      }
      throw new IllegalStateException("Unknown post kind.");
    }
  }

  private Kind kind;
  private @Nullable Identifier target = null;
  private @Nullable Identifier op = null;
  private List<UExpression> parameters = new ArrayList<>();
  private @Nullable UExpression offset = null;
  private @Nullable UExpression expression = null;
  private @Nullable PostKind post = null;
  private @Nullable UAction leftAction = null;
  private @Nullable UAction rightAction = null;

  public static interface InContextVisitor<T> {
    public UContext getContext();
  
    public T onSkip();
    public T onChoice(UAction left, UAction right);
    public T onParallel(UAction left, UAction right);
    public T onSequential(UAction left, UAction right);
    public T onConditional(UExpression condition, UAction left, UAction right);
    public T onAssertion(UExpression expression);
    public T onSelfInvocation(UBehavioural behavioural, List<UExpression> parameters);
    public T onInvocation(UStatual statual, UBehavioural behavioural, List<UExpression> parameters);
    public T onArrayInvocation(UAttribute attribute, UExpression offset, UBehavioural behavioural, List<UExpression> parameters);
    public T onAssignment(UAttribute attribute, UExpression expression);
    public T onArrayAssignment(UAttribute attribute, UExpression offset, UExpression expression);
    public T onPost(UAttribute attribute, UAction.PostKind kind);
    public T onArrayPost(UAttribute attribute, UExpression offset, UAction.PostKind kind);
  }

  private UAction(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create an action which represents <CODE>skip</CODE>.
   */
  public static UAction skip() {
    UAction a = new UAction(Kind.SKIP);
    return a;
  }

  /**
   * Create an action representing sequential composition of actions.
   */
  public static UAction seq(UAction leftAction, UAction rightAction) {
    if (leftAction.kind == Kind.SKIP)
      return rightAction;
    if (rightAction.kind == Kind.SKIP)
      return leftAction;

    UAction a = new UAction(Kind.SEQ);
    a.leftAction = leftAction;
    a.rightAction = rightAction;
    return a;
  }

  /**
   * Create an action representing the sequential composition of a list of actions.
   * 
   * @param actions list of actions
   * @return action representing the sequential compositions of actions
   */
  public static UAction seq(List<UAction> actions) {
    if (actions.isEmpty())
      return UAction.skip();
    if (actions.size() == 1)
      return actions.get(1);
    UAction sequence = UAction.skip();
    for (UAction action : actions)
      sequence = UAction.seq(sequence, action);
    return sequence;
  }
  
  /**
   * Create an action representing parallel composition of actions.
   */
  public static UAction par(UAction leftAction, UAction rightAction) {
    if (leftAction.kind == Kind.SKIP)
      return rightAction;
    if (rightAction.kind == Kind.SKIP)
      return leftAction;

    UAction a = new UAction(Kind.PAR);
    a.leftAction = leftAction;
    a.rightAction = rightAction;
    return a;
  }

  /**
   * Create an action representing non-deterministic choice between
   * actions.
   */
  public static UAction choice(UAction leftAction, UAction rightAction) {
    if (leftAction.equals(rightAction))
      return leftAction;

    UAction a = new UAction(Kind.CHOICE);
    a.leftAction = leftAction;
    a.rightAction = rightAction;
    return a;
  }

  /**
   * Create an action representing a conditional.
   */
  public static UAction conditional(UExpression expression, UAction leftAction, UAction rightAction) {
    if (leftAction.equals(rightAction))
      return leftAction;
    if (expression.equals(UExpression.trueConst()))
      return leftAction;
    if (expression.equals(UExpression.falseConst()))
      return rightAction;

    UAction a = new UAction(Kind.CONDITIONAL);
    a.expression = expression;
    a.leftAction = leftAction;
    a.rightAction = rightAction;
    return a;
  }

  /**
   * Create an action representing an assertion.
   */
  public static UAction assertion(UExpression expression) {
    UAction a = new UAction(Kind.ASSERTION);
    a.expression = expression;
    return a;
  }

  /**
   * Create an action representing an operation or reception call on
   * the sending object.
   */
  public static UAction invocation(Identifier op, List<UExpression> parameters) {
    UAction a = new UAction(Kind.INVOCATION);
    a.op = op;
    a.parameters = parameters;
    return a;
  }

  /**
   * Create an action representing an operation or reception call on
   * non-array sending object.
   */
  public static UAction invocation(Identifier target, Identifier op, List<UExpression> parameters) {
    UAction a = new UAction(Kind.INVOCATION);
    a.target = target;
    a.op = op;
    a.parameters = parameters;
    return a;
  }

  /**
   * Create an action representing an operation or reception call on
   * array callee object.
   */
  public static UAction arrayInvocation(Identifier target, UExpression offset, Identifier op, List<UExpression> parameters) {
    UAction a = new UAction(Kind.ARRAYINVOCATION);
    a.target = target;
    a.op = op;
    a.offset = offset;
    a.parameters = parameters;
    return a;
  }

  /**
   * Create an action representing an assignment to an identifier.
   */
  public static UAction assign(Identifier target, UExpression expression) {
    UAction a = new UAction(Kind.ASSIGNMENT);
    a.target = target;
    a.expression = expression;
    return a;
  }

  /**
   * Create an action representing an assignment to an array identifier.
   */
  public static UAction assign(Identifier target, UExpression offset, UExpression expression) {
    UAction a = new UAction(Kind.ARRAYASSIGNMENT);
    a.target = target;
    a.offset = offset;
    a.expression = expression;
    return a;
  }

  /**
   * Create an action representing incrementation or decrementation of
   * a non-array identifier.
   */
  public static UAction post(Identifier target, PostKind post) {
    UAction a = new UAction(Kind.POST);
    a.target = target;
    a.post = post;
    return a;
  }

  /**
   * Create an action representing incrementation or decrementation of
   * an array identifier.
   */
  public static UAction post(Identifier target, UExpression offset, PostKind post) {
    UAction a = new UAction(Kind.ARRAYPOST);
    a.target = target;
    a.offset = offset;
    a.post = post;
    return a;
  }

  /**
   * @return whether this action represents a {@code skip} action
   */
  public boolean isSkip() {
    return this.kind == Kind.SKIP;
  }

  /**
   * Determine action's type.
   * 
   * @param context action context
   * @return action's type
   */
  public UType getType(UContext context) {
    switch (this.kind) {
      case SKIP:
        return context.getVoidType();

      case SEQ:
      case PAR:
      case CHOICE: {
        UAction leftAction = requireNonNull(this.leftAction);
        UAction rightAction = requireNonNull(this.rightAction);

        UType leftType = leftAction.getType(context);
        if (leftType.isError())
          return leftType;
        UType rightType = rightAction.getType(context);
        if (rightType.isError())
          return rightType;
        if (!context.getVoidType().subsumes(leftType))
          return UType.error("non-void type of action " + type(leftAction, leftType));
        if (!context.getVoidType().subsumes(rightType))
          return UType.error("non-void type of action " + type(rightAction, rightType));
        return leftType;
      }

      case CONDITIONAL: {
        UExpression expression = requireNonNull(this.expression);
        UAction leftAction = requireNonNull(this.leftAction);
        UAction rightAction = requireNonNull(this.rightAction);

        UType condType = expression.getType(context);
        if (condType.isError())
          return condType;
        if (!context.getBooleanType().subsumes(condType))
          return UType.error("non-boolean condition " + type(expression, condType));
        UType leftType = leftAction.getType(context);
        if (leftType.isError())
          return leftType;
        UType rightType = rightAction.getType(context);
        if (rightType.isError())
          return rightType;
        if (!context.getVoidType().subsumes(leftType))
          return UType.error("non-void conditional action " + type(leftAction, leftType));
        if (!context.getVoidType().subsumes(rightType))
          return UType.error("non-void conditional action " + type(rightAction, rightType));
        return context.getVoidType();
      }

      case ASSERTION: {
        UExpression expression = requireNonNull(this.expression);

        UType assertionType = expression.getType(context);
        if (assertionType.isError())
          return assertionType;
        if (!context.getBooleanType().subsumes(assertionType))
          return UType.error("non-boolean assertion " + type(expression, assertionType));
        return context.getVoidType();
      }

      case INVOCATION: {
        Identifier target = this.target; // can be null for invocations on this or a static operation
        Identifier op = requireNonNull(this.op);

        // Check whether callee has class non-array type
        UType calleeType = null;
        if (target == null)
          calleeType = context.getThisType();
        else {
          UStatual statual = context.getStatual(target);
          if (statual == null) {
            UClass clazz = context.getClass(target);
            if (clazz == null)
              return UType.error("No matching element found for " + context(target, context.description()));
            calleeType = UType.simple(clazz);
          }
          else
            calleeType = statual.getType();
        }
        if (calleeType.isError())
          return calleeType;
        if (!calleeType.isClass())
          return UType.error("non-class callee type " + target == null ? quoted(calleeType) : type(target, calleeType));
        UClass calleeClass = requireNonNull(calleeType.getC1ass());

        // Check whether all parameters have types
        List<UType> parameterTypes = new ArrayList<>();
        for (UExpression parameter : this.parameters) {
          UType parameterType = parameter.getType(context);
          if (parameterType.isError())
            return parameterType;
          parameterTypes.add(parameterType);
        }

        // Check whether the callee class shows a behavioural feature
        // with appropriate name and parameter types
        UBehavioural behavioural = context.getBehavioural(calleeClass, this.op, parameterTypes, context.getVoidType());
        if (behavioural == null)
          return UType.error("no matching behavioural element found for " + context(UDescriptor.behavioural(calleeClass, op, parameterTypes), context.description()));
        UType invocationType = behavioural.getType();
        if (invocationType.isError())
          return invocationType;

        // Ignoring return type of invocation
        return context.getVoidType();
      }

      case ARRAYINVOCATION: {
        Identifier target = requireNonNull(this.target);
        UExpression offset = requireNonNull(this.offset);
        Identifier op = requireNonNull(this.op);

        // Check whether callee has class array type
        UStatual statual = context.getStatual(target);
        if (statual == null)
          return UType.error("No matching element found for " + context(target, context.description()));
        UType calleeType = statual.getType();
        if (calleeType.isError())
          return calleeType;
        if (!calleeType.isArray() || !calleeType.getUnderlyingType().isClass())
          return UType.error("non-class-array callee " + type(target, calleeType));
        UClass calleeClass = requireNonNull(calleeType.getUnderlyingType().getC1ass());

        // Check whether offset expression has integer type
        UType offsetType = offset.getType(context);
        if (offsetType.isError())
          return offsetType;
        if (!context.getIntegerType().subsumes(offsetType))
          return UType.error("non-integer offset " + type(offset, offsetType));

        // Check whether all parameters have types
        List<UType> parameterTypes = new ArrayList<>();
        for (UExpression parameter : this.parameters) {
          UType parameterType = parameter.getType(context);
          if (parameterType.isError())
            return parameterType;
          parameterTypes.add(parameterType);
        }

        // Check whether the callee class shows a behavioural feature
        // with appropriate name and parameter types
        UBehavioural behavioural = context.getBehavioural(calleeClass, op, parameterTypes, context.getVoidType());
        if (behavioural == null)
          return UType.error("no matching behavioural element found for " + context(UDescriptor.behavioural(calleeClass, op, parameterTypes), context.description()));
        UType invocationType = behavioural.getType();
        if (invocationType.isError())
          return invocationType;

        // Ignoring return type of invocation
        return context.getVoidType();
      }

      case ASSIGNMENT: {
        Identifier target = requireNonNull(this.target);
        UExpression expression = requireNonNull(this.expression);

        UAttribute attribute = context.getAttribute(target);
        if (attribute == null)
          return UType.error("No matching element found for " + context(target, context.description()));
        UType identifierType = attribute.getType();
        if (identifierType.isError())
          return identifierType;
        if (!identifierType.isSimple())
          return UType.error("non-simple left-hand side in simple assignment " + type(target, identifierType));
        UType expressionType = expression.getType(context);
        if (expressionType.isError())
          return expressionType;
        if (!identifierType.subsumes(expressionType))
          return UType.error("type mismatch in simple assignment " + quoted(this) + ": expected right-hand side type " + quoted(identifierType) + ", found " + quoted(expressionType));
        return context.getVoidType();
      }

      case ARRAYASSIGNMENT: {
        Identifier target = requireNonNull(this.target);
        UExpression offset = requireNonNull(this.offset);
        UExpression expression = requireNonNull(this.expression);

        UAttribute attribute = context.getAttribute(target);
        if (attribute == null)
          return UType.error("No matching element found for " + context(target, context.description()));
        UType identifierType = attribute.getType();
        if (identifierType.isError())
          return identifierType;
        if (!identifierType.isArray())
          return UType.error("non-array left-hand side in array assignment " + type(target, identifierType));
        UType underlyingType = identifierType.getUnderlyingType();
        if (underlyingType.isError())
          return underlyingType;
        UType offsetType = offset.getType(context);
        if (offsetType.isError())
          return offsetType;
        if (!context.getIntegerType().subsumes(offsetType))
          return UType.error("non-integer offset " + type(offset, offsetType));
        UType expressionType = expression.getType(context);
        if (expressionType.isError())
          return expressionType;
        if (!underlyingType.subsumes(expressionType))
          return UType.error("type mismatch in array assignment " + quoted(this) + ": expected right-hand side type " + quoted(underlyingType) + ", found " + quoted(expressionType));
        return context.getVoidType();
      }

      case POST: {
        Identifier target = requireNonNull(this.target);

        UAttribute attribute = context.getAttribute(target);
        if (attribute == null)
          return UType.error("No matching element found for " + context(target, context.description()));
        UType identifierType = attribute.getType();
        if (identifierType.isError())
          return identifierType;
        if (!identifierType.isSimple())
          return UType.error("post operation on non-integer " + type(target, identifierType));
        if (!context.getIntegerType().subsumes(identifierType))
          return UType.error("post operation on non-integer " + type(target, identifierType));
        return context.getVoidType();
      }

      case ARRAYPOST: {
        Identifier target = requireNonNull(this.target);
        UExpression offset = requireNonNull(this.offset);

        UAttribute attribute = context.getAttribute(this.target);
        if (attribute == null)
          return UType.error("No matching element found for " + context(target, context.description()));
        UType identifierType = attribute.getType();
        if (identifierType.isError())
          return identifierType;
        if (!identifierType.isArray())
          return UType.error("array access to non-array " + type(target, identifierType));
        UType underlyingType = identifierType.getUnderlyingType();
        if (underlyingType.isError())
          return underlyingType;
        UType offsetType = offset.getType(context);
        if (!context.getIntegerType().subsumes(offsetType))
          return UType.error("non-integer offset " + type(offset, offsetType));
        if (!context.getIntegerType().subsumes(underlyingType))
          return UType.error("post operation on non-integer array " + type(target, identifierType));
        return context.getVoidType();
      }

      default:
        return UType.error("unknown action " + quoted(this));
    }
  }
  
  /**
   * Determine all behaviourals invoked by this action.
   * 
   * @param context action context
   * @return action's behaviourals
   */
  public Set<UBehavioural> getBehaviourals(UContext context) {
    var behaviourals = new HashSet<UBehavioural>();
    switch (this.kind) {
      case SKIP:
      case ASSERTION:
      case ASSIGNMENT:
      case ARRAYASSIGNMENT:
      case POST:
      case ARRAYPOST:
        return behaviourals;

      case SEQ:
      case PAR:
      case CHOICE:
      case CONDITIONAL:
        behaviourals.addAll(requireNonNull(this.leftAction).getBehaviourals(context));
        behaviourals.addAll(requireNonNull(this.rightAction).getBehaviourals(context));
        return behaviourals;

      case INVOCATION:
      case ARRAYINVOCATION: {
        Identifier target = this.target; // can be null for invocations on this or a static operation
        UType calleeType = null;
        if (target == null)
          calleeType = context.getThisType();
        else {
          var statual = context.getStatual(target);
          calleeType = statual == null ? UType.simple(requireNonNull(context.getClass(target))) : statual.getType();
        }
        var calleeClass = requireNonNull(calleeType.getC1ass());
        var parameterTypes = this.parameters.stream().map(parameter -> parameter.getType(context)).collect(toList());
        UBehavioural behavioural = context.getBehavioural(calleeClass, this.op, parameterTypes, context.getVoidType());
        if (behavioural != null)
          behaviourals.add(behavioural);
        return behaviourals;
      }

      default:
        return behaviourals;
    }
  }
  
  /**
   * Compute interleaving normal form as a choice of sequential
   * actions.
   * 
   * @return interleavings
   */
  public UAction getInterleavingNormalForm() {
    UAction interleaveds = null;
    for (List<UAction> interleaving : getInterleavings()) {
      UAction interleaved = UAction.seq(interleaving);
      interleaveds = (interleaveds == null ? interleaved : UAction.choice(interleaveds, interleaved));
    }
    return (interleaveds == null ? UAction.skip() : interleaveds);
  }

  /**
   * Compute a list of all possible interleavings (as a list of list of
   * non-compound actions) of this action.
   */
  private List<List<UAction>> getInterleavings() {
    List<List<UAction>> interleavings = new ArrayList<>();

    if (this.kind == Kind.SEQ) {
      List<UAction> sequentialActions = getSequentialActions();
      List<List<List<UAction>>> interleavingsList = new ArrayList<>();
      for (UAction sequentialAction : sequentialActions)
        interleavingsList.add(sequentialAction.getInterleavings());
      return util.Lists.mergings(interleavingsList);
    }

    if (this.kind == Kind.PAR) {
      List<UAction> parallelActions = getParallelActions();
      for (List<UAction> permutation : util.Lists.permutations(parallelActions)) {
        List<List<List<UAction>>> interleavingsList = new ArrayList<>();
        for (UAction parallelAction : permutation)
          interleavingsList.add(parallelAction.getInterleavings());
        interleavings.addAll(util.Lists.mergings(interleavingsList));
      }
      return interleavings;
    }

    if (this.kind == Kind.CHOICE) {
      List<UAction> choiceActions = getChoiceActions();
      for (UAction choiceAction : choiceActions) {
        interleavings.addAll(choiceAction.getInterleavings());
      }
      return interleavings;
    }

    List<UAction> interleaving = new ArrayList<>();
    interleaving.add(this);
    interleavings.add(interleaving);
    return interleavings;
  }

  /**
   * Compute a list of all choice actions that are on the same level as
   * this action.
   */
  private List<UAction> getChoiceActions() {
    List<UAction> choiceActions = new ArrayList<>();

    if (this.kind == Kind.CHOICE) {
      UAction leftAction = requireNonNull(this.leftAction);
      UAction rightAction = requireNonNull(this.rightAction);

      List<UAction> leftChoiceActions = leftAction.getChoiceActions();
      List<UAction> rightChoiceActions = rightAction.getChoiceActions();
      choiceActions.addAll(leftChoiceActions);
      choiceActions.addAll(rightChoiceActions);
      return choiceActions;
    }

    choiceActions.add(this);
    return choiceActions;
  }

  /**
   * Compute a list of all parallel actions that are on the same level
   * as this action.
   */
  private List<UAction> getParallelActions() {
    List<UAction> parallelActions = new ArrayList<>();

    if (this.kind == Kind.PAR) {
      UAction leftAction = requireNonNull(this.leftAction);
      UAction rightAction = requireNonNull(this.rightAction);

      List<UAction> leftParallelActions = leftAction.getParallelActions();
      List<UAction> rightParallelActions = rightAction.getParallelActions();
      parallelActions.addAll(leftParallelActions);
      parallelActions.addAll(rightParallelActions);
      return parallelActions;
    }

    parallelActions.add(this);
    return parallelActions;
  }

  /**
   * Compute a list of all sequential actions that are on the same
   * level as this action.
   */
  private List<UAction> getSequentialActions() {
    List<UAction> sequentialActions = new ArrayList<>();

    if (this.kind == Kind.SEQ) {
      UAction leftAction = requireNonNull(this.leftAction);
      UAction rightAction = requireNonNull(this.rightAction);

      List<UAction> leftSequentialActions = leftAction.getSequentialActions();
      List<UAction> rightSequentialActions = rightAction.getSequentialActions();
      sequentialActions.addAll(leftSequentialActions);
      sequentialActions.addAll(rightSequentialActions);
      return sequentialActions;
    }

    sequentialActions.add(this);
    return sequentialActions;
  }

  /**
   * Accept a visitor in context.
   * 
   * @param visitor a visitor in context
   */
  public <T> T accept(InContextVisitor<T> visitor) {
    final UContext context = visitor.getContext();

    switch (this.kind) {
      case SKIP: {
        return visitor.onSkip();
      }

      case SEQ: {
        return visitor.onSequential(requireNonNull(this.leftAction), requireNonNull(this.rightAction));
      }

      case PAR: {
        return visitor.onParallel(requireNonNull(this.leftAction), requireNonNull(this.rightAction));
      }

      case CHOICE: {
        return visitor.onChoice(requireNonNull(this.leftAction), requireNonNull(this.rightAction));
      }

      case CONDITIONAL: {
        return visitor.onConditional(requireNonNull(this.expression), requireNonNull(this.leftAction), requireNonNull(this.rightAction));
      }

      case ASSERTION: {
        return visitor.onAssertion(requireNonNull(this.expression));
      }

      case INVOCATION: {
        Identifier op = requireNonNull(this.op);

        List<UType> parameterTypes = new ArrayList<>();
        for (UExpression parameter : this.parameters) {
          UType parameterType = parameter.getType(context);
          parameterTypes.add(parameterType);
        }
        if (this.target == null) {
          // Call on this
          UClass onClass = requireNonNull(context.getThisType().getC1ass());
          UBehavioural behavioural = requireNonNull(context.getBehavioural(onClass, op, parameterTypes, context.getVoidType()));
          return visitor.onSelfInvocation(behavioural, this.parameters);
        }
        else {
          // Call not on this, maybe call of static method
          UClass onClass = context.getClass(this.target);
          if (onClass != null) {
            UBehavioural behavioural = requireNonNull(context.getBehavioural(onClass, op, parameterTypes, context.getVoidType()));
            return visitor.onSelfInvocation(behavioural, this.parameters);
          }
          // Call not on this and not on a static method
          UStatual on = requireNonNull(context.getStatual(this.target));
          onClass = requireNonNull(on.getType().getC1ass());
          UBehavioural behavioural = requireNonNull(context.getBehavioural(onClass, op, parameterTypes, context.getVoidType()));
          return visitor.onInvocation(on, behavioural, this.parameters);
        }
      }

      case ARRAYINVOCATION: {
        Identifier target = requireNonNull(this.target);
        UExpression offset = requireNonNull(this.offset);
        Identifier op = requireNonNull(this.op);

        List<UType> parameterTypes = new ArrayList<>();
        for (UExpression parameter : this.parameters) {
          UType parameterType = parameter.getType(context);
          parameterTypes.add(parameterType);
        }
        UAttribute targetAttribute = requireNonNull(context.getAttribute(target));
        UClass onClass = requireNonNull(targetAttribute.getType().getUnderlyingType().getC1ass());
        UBehavioural behavioural = requireNonNull(context.getBehavioural(onClass, op, parameterTypes, context.getVoidType()));
        return visitor.onArrayInvocation(targetAttribute, offset, behavioural, this.parameters);
      }

      case ASSIGNMENT: {
        Identifier target = requireNonNull(this.target);
        UExpression expression = requireNonNull(this.expression);

        UAttribute targetAttribute = requireNonNull(context.getAttribute(target));
        return visitor.onAssignment(targetAttribute, expression);
      }

      case ARRAYASSIGNMENT: {
        Identifier target = requireNonNull(this.target);
        UExpression offset = requireNonNull(this.offset);
        UExpression expression = requireNonNull(this.expression);

        UAttribute targetAttribute = requireNonNull(context.getAttribute(target));
        return visitor.onArrayAssignment(targetAttribute, offset, expression);
      }

      case POST: {
        Identifier target = requireNonNull(this.target);
        PostKind postKind = requireNonNull(this.post);

        UAttribute targetAttribute = requireNonNull(context.getAttribute(target));
        return visitor.onPost(targetAttribute, postKind);
      }

      case ARRAYPOST: {
        Identifier target = requireNonNull(this.target);
        UExpression offset = requireNonNull(this.offset);
        PostKind postKind = requireNonNull(this.post);

        UAttribute targetAttribute = requireNonNull(context.getAttribute(target));
        return visitor.onArrayPost(targetAttribute, offset, postKind);
      }
    }

    throw new IllegalStateException("Unknown action kind.");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.target,
                        this.op,
                        this.parameters,
                        this.offset,
                        this.expression,
                        this.leftAction,
                        this.rightAction);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;

    try {
      UAction other = (UAction)object;
      return ((this.kind == other.kind) &&
              (Objects.equals(this.target, other.target)) &&
              (Objects.equals(this.op, other.op)) &&
              (Objects.equals(this.parameters, other.parameters)) &&
              (Objects.equals(this.offset, other.offset)) &&
              (Objects.equals(this.expression, other.expression)) &&
              (this.post == other.post) &&
              (Objects.equals(this.leftAction, other.leftAction)) &&
              (Objects.equals(this.rightAction, other.rightAction)));
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  /**
   * @param container action containing this action
   * @return action's string representation, surrounded with curly brackets
   *         if the container action is a different block construct
   */
  private String blocked(UAction container) {
    boolean block = false;
    switch (container.kind) {
      case CHOICE: {
        block = (this.kind == Kind.SEQ || this.kind == Kind.PAR);
        break;
      }

      case SEQ: {
        block = (this.kind == Kind.PAR || this.kind == Kind.CHOICE);
        break;
      }

      case PAR: {
        block = (this.kind == Kind.SEQ || this.kind == Kind.CHOICE);
        break;
      }

      case CONDITIONAL: {
        block = (this.kind == Kind.CHOICE || this.kind == Kind.PAR || this.kind == Kind.SEQ);
        break;
      }

      default:
        break;
    }

    if (block) {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("{ ");
      resultBuilder.append(this.toString());
      resultBuilder.append(" }");
      return resultBuilder.toString();
    }
    else
      return this.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case SKIP: {
        resultBuilder.append(";");
        return resultBuilder.toString();
      }

      case CHOICE: {
        UAction leftAction = requireNonNull(this.leftAction);
        UAction rightAction = requireNonNull(this.rightAction);

        resultBuilder.append(leftAction.blocked(this));
        resultBuilder.append(" :: ");
        resultBuilder.append(rightAction.blocked(this));
        return resultBuilder.toString();
      }

      case PAR: {
        UAction leftAction = requireNonNull(this.leftAction);
        UAction rightAction = requireNonNull(this.rightAction);

        resultBuilder.append(leftAction.blocked(this));
        resultBuilder.append(" || ");
        resultBuilder.append(rightAction.blocked(this));
        return resultBuilder.toString();
      }

      case SEQ: {
        UAction leftAction = requireNonNull(this.leftAction);
        UAction rightAction = requireNonNull(this.rightAction);

        resultBuilder.append(leftAction.blocked(this));
        resultBuilder.append(" ");
        resultBuilder.append(rightAction.blocked(this));
        return resultBuilder.toString();
      }

      case CONDITIONAL: {
        UAction leftAction = requireNonNull(this.leftAction);
        UAction rightAction = requireNonNull(this.rightAction);

        resultBuilder.append("if (");
        resultBuilder.append(this.expression);
        resultBuilder.append(") ");
        resultBuilder.append(leftAction.blocked(this));
        if (rightAction.kind != Kind.SKIP) {
          resultBuilder.append(" else ");
          resultBuilder.append(rightAction.blocked(this));
        }
        return resultBuilder.toString();
      }

      case ASSERTION: {
        resultBuilder.append("assert(");
        resultBuilder.append(this.expression);
        resultBuilder.append(")");
        return resultBuilder.toString();
      }

      case INVOCATION: {
        if (this.target != null) {
          resultBuilder.append(this.target);
          resultBuilder.append(".");
        }
        resultBuilder.append(this.op);
        resultBuilder.append("(");
        String separator = "";
        for (UExpression parameter : this.parameters) {
          resultBuilder.append(separator);
          resultBuilder.append(parameter);
          separator = ", ";
        }
        resultBuilder.append(")");
        return resultBuilder.append(";").toString();
      }

      case ARRAYINVOCATION: {
        resultBuilder.append(this.target);
        resultBuilder.append("[");
        resultBuilder.append(this.offset);
        resultBuilder.append("]");
        resultBuilder.append(".");
        resultBuilder.append(this.op);
        resultBuilder.append("(");
        String separator = "";
        for (UExpression parameter : this.parameters) {
          resultBuilder.append(separator);
          resultBuilder.append(parameter);
          separator = ", ";
        }
        resultBuilder.append(")");
        return resultBuilder.append(";").toString();
      }

      case ASSIGNMENT: {
        resultBuilder.append(this.target);
        resultBuilder.append(" = ");
        resultBuilder.append(this.expression);
        return resultBuilder.append(";").toString();
      }

      case ARRAYASSIGNMENT: {
        resultBuilder.append(this.target);
        resultBuilder.append("[");
        resultBuilder.append(this.offset);
        resultBuilder.append("]");
        resultBuilder.append(" = ");
        resultBuilder.append(this.expression);
        return resultBuilder.append(";").toString();
      }

      case POST: {
        resultBuilder.append(this.target);
        resultBuilder.append(this.post);
        return resultBuilder.append(";").toString();
      }

      case ARRAYPOST: {
        resultBuilder.append(this.target);
        resultBuilder.append("[");
        resultBuilder.append(this.offset);
        resultBuilder.append("]");
        resultBuilder.append(this.post);
        return resultBuilder.append(";").toString();
      }
    }

    return resultBuilder.toString();
  }
}
