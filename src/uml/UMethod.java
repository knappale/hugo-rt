package uml;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


@NonNullByDefault
public class UMethod {
  private List<String> languages;
  private String body;

  public UMethod(List<String> languages, String body) {
    this.languages = languages;
    this.body = body;
  }

  public List<String> getLanguages() {
    return this.languages;
  }

  public List<String> getCode() {
    return Arrays.asList(this.body.split("\\n"));
  }

  /**
   * @param prefix prefix string
   * @return method's representation in UTE format with {@code prefix}
   */
  public String declaration(String prefix) {
    StringBuilder builder = new StringBuilder(prefix);
    builder.append("behaviour ");
    builder.append(Formatter.separated(this.languages, l -> "\"" + l + "\"", ", "));
    builder.append(" {\n");
    var nextPrefix = prefix + "  ";
    for (var line : this.getCode()) {
      builder.append(nextPrefix);         
      builder.append(line);
      builder.append("\n");
    }
    builder.append(prefix);
    builder.append("}");
    return builder.toString();
  }

  /**
   * @return method's representation in UTE format
   */
  public String declaration() {
    return this.declaration("");
  }
}
