package uml;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import control.Properties;
import uml.interaction.UInteraction;
import uml.ocl.OConstraint;
import uml.testcase.UTestCase;
import util.Formatter;


/**
 * UML collaboration
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UCollaboration extends UElement {
  private UModel model;
  private Set<UObject> objects = new HashSet<>();
  private Set<UInteraction> interactions = new HashSet<>();
  /** @inv emptyInteraction != null \implies interactions.contains(emptyInteraction) */
  private @Nullable UInteraction emptyInteraction = null;
  private Set<UTestCase> testCases = new HashSet<>();
  /** @inv emptyTestCase != null \implies testCases.contains(emptyTestCase) */
  private @Nullable UTestCase emptyTestCase = null;
  private Set<OConstraint> constraints = new HashSet<>();
  private @Nullable Properties properties = null;

  /**
   * Create a new named collaboration in a model.
   *
   * @param name collaboration's name
   * @param model underlying UML model
   * @return a model
   */
  public UCollaboration(String name, UModel model) {
    super(name);
    this.model = model;
  }

  /**
   * Determine collaboration's underlying (UML) model.
   *
   * @return collaboration's underlying model
   */
  public UModel getModel() {
    return this.model;
  }

  /**
   * Determine collaboration's properties.
   *
   * @return collaboration's properties
   */
  public Properties getProperties() {
    Properties properties = this.properties;
    if (properties != null)
      return properties;
    
    properties = getModel().getProperties().getCopy();
    this.properties = properties;
    return properties;
  }

  /**
   * @return whether this collaboration is empty, i.e., has no objects
   */
  public boolean isEmpty() {
    return this.objects.isEmpty();
  }

  /**
   * Add an object to the collaboration.
   *
   * @param object an object
   */
  public void addObject(UObject object) {
    this.objects.add(object);
  }

  /**
   * Determine collaboration's objects.
   *
   * @return collaboration's objects
   */
  public Set<UObject> getObjects() {
    return this.objects;
  }

  /**
   * Determine an object of the collaboration by name.
   *
   * @return object named <CODE>name</CODE> in collaboration, or <CODE>null</CODE> if no such
   * object exists
   */
  public @Nullable UObject getObject(@Nullable String name) {
    for (UObject object : this.objects) {
      if (object.getName().equals(name))
        return object;
    }
    return null;
  }

  /**
   * Add a new interaction to the collaboration.
   *
   * @param name interaction's name
   */
  public UInteraction addInteraction(String name) {
    UInteraction uInteraction = new UInteraction(name, this);
    this.interactions.add(uInteraction);
    return uInteraction;
  }

  /**
   * Determine collaboration's interactions.
   *
   * @return collaboration's interactions
   */
  public Set<UInteraction> getInteractions() {
    return this.interactions;
  }

  /**
   * Determine empty interaction for this collaboration.
   *
   * @return empty interaction for this collaboration.
   */
  public UInteraction getEmptyInteraction() {
    UInteraction emptyInteraction = this.emptyInteraction;
    if (emptyInteraction != null)
      return emptyInteraction;

    emptyInteraction = this.addInteraction("empty");
    this.emptyInteraction = emptyInteraction;
    return emptyInteraction;
  }

  /**
   * Add a test case to the collaboration.
   *
   * @param testCase a test case
   */
  public void addTestCase(UTestCase testCase) {
    this.testCases.add(testCase);
  }

  /**
   * Determine collaboration's test cases.
   *
   * @return collaboration's test cases
   */
  public Set<UTestCase> getTestCases() {
    return this.testCases;
  }

  /**
   * Determine empty test case for this collaboration.
   *
   * @return empty test case for this collaboration.
   */
  public UTestCase getEmptyTestCase() {
    UTestCase emptyTestCase = this.emptyTestCase;
    if (emptyTestCase != null)
      return emptyTestCase;

    emptyTestCase = new UTestCase("empty", this);
    this.addTestCase(emptyTestCase);
    this.emptyTestCase = emptyTestCase;
    return emptyTestCase;
  }

  /**
   * Add a constraint to the collaboration.
   *
   * @param constraint a constraint
   */
  public void addConstraint(OConstraint constraint) {
    this.constraints.add(constraint);
  }

  /**
   * Determine collaboration's constraints.
   *
   * @return collaboration's constraints
   */
  public Set<OConstraint> getConstraints() {
    return this.constraints;
  }

  /**
   * @return collaboration's constraints context
   */
  public UContext getConstraintsContext() {
    return UContext.collaboration(this);
  }

  /**
   * @return collaboration's constraints context
   */
  public UContext getTestCaseContext() {
    return UContext.collaboration(this);
  }

  /**
   * @param prefix prefix string
   * @return collaboration's representation in UTE format with prefix
   */  
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";
    String lineSep = "";

    resultBuilder.append("collaboration ");
    resultBuilder.append(this.getName());
    resultBuilder.append(" {\n");
    
    if (!this.getProperties().equals(getModel().getProperties())) {
      resultBuilder.append(this.getProperties().declarationOfDiffering(getModel().getProperties(), nextPrefix));
      lineSep = "\n\n";
    }

    for (uml.UObject object : this.getObjects()) {
      resultBuilder.append(lineSep);
      resultBuilder.append(object.declaration(nextPrefix));
      lineSep = "\n\n";
    }

    for (UInteraction interaction : this.getInteractions()) {
      resultBuilder.append(lineSep);
      resultBuilder.append(interaction.declaration(nextPrefix));
      lineSep = "\n\n";
    }

    for (uml.ocl.OConstraint constraint : this.getConstraints()) {
      resultBuilder.append(lineSep);
      resultBuilder.append(constraint.declaration(nextPrefix));
      lineSep = "\n\n";
    }

    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");
    
    String result = resultBuilder.toString();
    assert (result != null);
    return result;
  }

  /**
   * @return collaboration's representation in UTE format
   */  
  public String declaration() {
    return this.declaration("");
  }
  
  /**
   * @return string representation of this collaboration
   */
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("Collaboration [name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append(", objects = ");
    resultBuilder.append(Formatter.list(this.objects, object -> object.getName()));
    resultBuilder.append(", interactions = ");
    resultBuilder.append(Formatter.list(this.interactions, interaction -> interaction.getName()));
    resultBuilder.append(", constraints = ");
    resultBuilder.append(Formatter.list(this.constraints, constraint -> constraint.getName()));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
