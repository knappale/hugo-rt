package uml;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Contains data of a UML statual (element).
 *
 * Statuals are completely artificial - they are meant as: pertaining
 * to the state of a system.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class UStatual extends UTypedElement {
  /**
   * Create a statual (element)
   *
   * @param name statual (element)'s name
   */
  public UStatual(String name) {
    super(name);
  }

  /**
   * Make a case distinction on the sub-type.
   *
   * @param attributeFun function called when this statual is an attribute
   * @param parameterFun function called when this statual is a parameter
   * @param objectFun function called when this statual is an object
   * @return result of applying one of the sub-type functions
   */
  @NonNullByDefault({})
  public abstract <T> T cases(@NonNull Function<@NonNull UAttribute, T> attributeFun,
                              @NonNull Function<@NonNull UParameter, T> parameterFun,
                              @NonNull Function<@NonNull UObject, T> objectFun);

  /**
   * @return whether this statual (element) is a constant
   */
  public abstract boolean isConstant();

  /**
   * @return whether this statual (element) is static
   */
  public abstract boolean isStatic();

  /**
   * Determine the set of dependencies of this statual (element)
   *
   * @return statual's dependencies
   */
  public Set<UStatual> getDependencies(@SuppressWarnings("unused") UContext context) {
    return new HashSet<UStatual>();
  }
}
