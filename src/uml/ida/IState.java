package uml.ida;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import java.util.Objects;


/**
 * Ida state
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 * @since 2005-09-04
 */
@NonNullByDefault
public class IState {
  private static int counter = 0;
  private String name;

  IState() {
    this.name = "s_" + counter++;
  }

  IState(String prefix) {
    this.name = prefix + counter++;
  }

  public String getName() {
    return this.name;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try { 
      IState other = (IState)object;
      return Objects.equals(this.name, other.name);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.getName();
  }
}
