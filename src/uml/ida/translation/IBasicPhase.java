package uml.ida.translation;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.interaction.UBasicFragment;
import uml.interaction.UOccurrenceSpecification;


/**
 * Represents one phase of the unwinding algorithm (see Brill et al., 2004).
 *
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
class IBasicPhase extends IPhase {
  private @Nullable Set<UOccurrenceSpecification> readySet;
  private Set<UOccurrenceSpecification> history = new HashSet<>();

  /**
   * Create the start phase of the unwinding process for a basic interaction
   * (see Brill et al., 2004).
   * 
   * @param basicInteraction the basic interaction to unwind
   */
  IBasicPhase(UBasicFragment basicInteraction) {
    super(basicInteraction.getOccurrences());
  }

  /**
   * Create an intermediate phase in the unwinding process for a basic interaction
   * (see Brill et al., 2004).
   * 
   * @param occurrences a set of occurrences
   */
  private IBasicPhase(Set<UOccurrenceSpecification> occurrences) {
    super(occurrences);
  }

  /**
   * @see uml.ida.translation.IPhase#isAccepting()
   */
  boolean isAccepting() {
    return this.getReadySet().isEmpty();
  }

  /**
   * @see uml.ida.translation.IPhase#isRecurrent()
   */
  boolean isRecurrent() {
    return false;
  }

  /**
   * @see uml.ida.translation.IPhase#getSuccessors()
   */
  Set<IChange> getSuccessors() {
    Set<IChange> successors = new HashSet<IChange>();

    for (UOccurrenceSpecification occurrence : this.getReadySet()) {
      IBasicPhase nextPhase = new IBasicPhase(this.occurrences);
      nextPhase.history = new HashSet<>(this.history);
      nextPhase.history.add(occurrence);

      successors.add(new IChange(occurrence, null, null, nextPhase));
    }

    return successors;
  }

  /**
   * Determine the ready set of this phase (see Brill et al., 2004).
   * 
   * @return the ready set
   */
  private Set<UOccurrenceSpecification> getReadySet() {
    Set<UOccurrenceSpecification> readySet = this.readySet;
    if (readySet != null)
      return readySet;

    readySet = new HashSet<>();
    for (UOccurrenceSpecification occurrence : this.occurrences) {
      Set<UOccurrenceSpecification> prerequisites = occurrence.getPrerequisites();
      if (this.history.containsAll(prerequisites) && !this.history.contains(occurrence))
        readySet.add(occurrence);
    }
    this.readySet = readySet;
    return readySet;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.history);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;

    try {
      IBasicPhase other = (IBasicPhase)object;
      return super.equals(other) &&
             Objects.equals(this.history, other.history);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
}
