package uml.ida.translation;

import util.Pair;
import util.UnlimitedNatural;
import uml.interaction.UBasicFragment;
import uml.interaction.ULifeline;
import uml.interaction.UOccurrenceSpecification;
import uml.ida.IAction;
import uml.ida.ICondition;
import uml.ida.IExpression;
import uml.ida.IOperator;
import uml.ida.IVariable;

import static util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;


/**
 * Loop phase
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 * @since 2006-04-02
 */
@NonNullByDefault
class ILoopPhase extends IPhase {
  private UnlimitedNatural min;
  private UnlimitedNatural max;
  private Set<ULifeline> lifelines;
  private Map<ULifeline, IVariable> variables;
  private Map<ULifeline, List<UOccurrenceSpecification>> histories;
  private boolean recurrent;
  private boolean finished;

  ILoopPhase(UnlimitedNatural min, UnlimitedNatural max, UBasicFragment basic, Map<ULifeline, IVariable> variables) {
    super(basic.getOccurrences());
    this.min = min;
    this.max = max;
    this.lifelines = basic.getLifelines();
    this.variables = variables;
    this.histories = new HashMap<>();
    this.recurrent = false;
    this.finished = false;

    // Initialise history with empty history sets
    for (ULifeline lifeline : this.lifelines)
      this.histories.put(lifeline, new ArrayList<>());
  }

  /**
   * Copy constructor
   *
   * @param occurrences
   * @param min
   * @param max
   * @param lifelines
   * @param variables
   * @param histories
   */
  private ILoopPhase(Set<UOccurrenceSpecification> occurrences, UnlimitedNatural min, UnlimitedNatural max, Set<ULifeline> lifelines,
                     Map<ULifeline, IVariable> variables, Map<ULifeline, List<UOccurrenceSpecification>> histories) {
    super(occurrences);
    this.min = min;
    this.max = max;
    this.lifelines = lifelines;
    this.variables = variables;
    this.histories = histories;
  }

  /**
   * @see uml.ida.translation.IPhase#isAccepting()
   */
  boolean isAccepting() {
    return this.finished;
  }

  /**
   * @see uml.ida.translation.IPhase#isRecurrent()
   */
  boolean isRecurrent() {
    return this.recurrent;
  }

  /**
   * @see uml.ida.translation.IPhase#getSuccessors()
   */
  Set<IChange> getSuccessors() {
    Set<IChange> successors = new HashSet<>();
    if (this.finished)
      return successors;

    if (this.recurrent) {
      ILoopPhase nextPhase = this.clone();
      nextPhase.recurrent = false;
      IAction recurrenceEffect = IAction.skip();
      for (ULifeline lifeline : this.lifelines) {
        recurrenceEffect = IAction.seq(recurrenceEffect, IAction.assign(requireNonNull(this.variables.get(lifeline)), IExpression.intConst(this.min.isInfty() ? 0 : this.min.getNatural())));
      }
      successors.add(new IChange(null, new ICondition(IExpression.trueConst(), new HashSet<>()), recurrenceEffect, nextPhase));
      return successors;
    }

    // If all histories are empty, we might be finished
    if (this.hasEmptyHistories()) {
      // If the minimum number of iterations is infinite, we cannot terminate
      if (!this.min.isInfty()) {
        ILoopPhase nextPhase = this.clone();
        nextPhase.finished = true;
        IExpression betweenMinAndMax = IExpression.trueConst();
        for (ULifeline lifeline : this.lifelines) {
          IVariable lifelineVariable = requireNonNull(this.variables.get(lifeline));
          betweenMinAndMax = IExpression.and(betweenMinAndMax, IExpression.relational(IExpression.variable(lifelineVariable), IOperator.GEQ, IExpression.intConst(min.getNatural())));
          if (!this.max.isInfty())
            betweenMinAndMax = IExpression.and(betweenMinAndMax, IExpression.relational(IExpression.variable(lifelineVariable), IOperator.LEQ, IExpression.intConst(max.getNatural())));
        }
        // No objects are participating in this transition
        Set<ULifeline> finishedLifelines = new HashSet<>();
        ICondition finishedGuard = new ICondition(betweenMinAndMax, finishedLifelines);
        IAction resetVariables = IAction.skip();
        for (ULifeline lifeline : this.lifelines) {
          resetVariables = IAction.seq(resetVariables, IAction.reset(requireNonNull(this.variables.get(lifeline))));
        }
        successors.add(new IChange(null, finishedGuard, resetVariables, nextPhase));
      }
    }

    for (Pair<UOccurrenceSpecification, IExpression> readyPair : this.getReadySet()) {
      UOccurrenceSpecification occurrence = readyPair.getFirst();
      IExpression occurrenceGuard = readyPair.getSecond();
      ULifeline lifeline = occurrence.getLifeline();
      IVariable lifelineVariable = requireNonNull(this.variables.get(lifeline));

      Set<ULifeline> activeLifelines = new HashSet<>();
      activeLifelines.add(lifeline);

      // If the occurrence specification's lifeline is not finishing
      // relative progress does not change on its occurrence
      if (!occurrence.isLastEvent()) {
        ILoopPhase nextPhase = this.clone();
        requireNonNull(nextPhase.histories.get(occurrence.getLifeline())).add(occurrence);
        ICondition occurrenceCondition = new ICondition(occurrenceGuard, activeLifelines);
        successors.add(new IChange(occurrence, occurrenceCondition, null, nextPhase));
        continue;
      }

      // The lifeline of the occurrence has finished, i.e., occurrence.isLastEvent()
      // is true.
      ILoopPhase nextPhase = this.clone();
      nextPhase.histories.put(lifeline, new ArrayList<>());
      IAction occurrenceEffect = IAction.inc(requireNonNull(this.variables.get(occurrence.getLifeline())));
      if ((min.isInfty() || max.isInfty()) && nextPhase.hasEmptyHistories()) {
        IExpression recurrenceGuard = IExpression.trueConst();
        for (ULifeline otherLifeline : this.lifelines) {
          if (!lifeline.equals(otherLifeline)) {
            IVariable otherLifelineVariable = requireNonNull(this.variables.get(otherLifeline));
            recurrenceGuard = IExpression.and(recurrenceGuard, IExpression.relational(IExpression.arithmetical(IExpression.variable(lifelineVariable), IOperator.PLUS, IExpression.intConst(1)), IOperator.EQ, IExpression.variable(otherLifelineVariable)));
          }
        }
        if (!min.isInfty())
          recurrenceGuard = IExpression.and(recurrenceGuard, IExpression.relational(IExpression.arithmetical(IExpression.variable(lifelineVariable), IOperator.PLUS, IExpression.intConst(1)), IOperator.GEQ, IExpression.intConst(min.getNatural())));

        ILoopPhase recurrencePhase = nextPhase.clone();
        recurrencePhase.recurrent = true;
        ICondition recurrenceCondition = new ICondition(IExpression.and(occurrenceGuard, recurrenceGuard), activeLifelines);
        successors.add(new IChange(occurrence, recurrenceCondition, occurrenceEffect, recurrencePhase));

        ICondition nonRecurrenceCondition = new ICondition(IExpression.and(occurrenceGuard, IExpression.neg(recurrenceGuard)), activeLifelines);
        successors.add(new IChange(occurrence, nonRecurrenceCondition, occurrenceEffect, nextPhase));
      }
      else {
        ICondition occurrenceCondition = new ICondition(occurrenceGuard, activeLifelines);
        successors.add(new IChange(occurrence, occurrenceCondition, occurrenceEffect, nextPhase));
      }
    }

    return successors;
  }

  private Set<Pair<UOccurrenceSpecification, IExpression>> getReadySet() {
    Set<Pair<UOccurrenceSpecification, IExpression>> readySet = new HashSet<>();
    if (this.finished || this.recurrent)
      return readySet;

    for (UOccurrenceSpecification occurrence : this.occurrences) {
      List<UOccurrenceSpecification> occurrenceHistory = requireNonNull(this.histories.get(occurrence.getLifeline()));
      if (occurrenceHistory.contains(occurrence))
        continue;

      IVariable occurrenceLifelineVariable = requireNonNull(this.variables.get(occurrence.getLifeline()));
      Set<UOccurrenceSpecification> prerequisites = occurrence.getPrerequisites();
      IExpression occurrenceGuard = IExpression.trueConst();
      boolean isReady = true;
      for (UOccurrenceSpecification prerequisite : prerequisites) {
        if (occurrence.getLifeline().equals(prerequisite.getLifeline())) {
          if (!occurrenceHistory.contains(prerequisite)) {
            isReady = false;
            break;
          }
        }
        else {
          IVariable prerequisiteLifelineVariable = requireNonNull(this.variables.get(prerequisite.getLifeline()));
          IExpression beforeGuard = IExpression.relational(IExpression.variable(occurrenceLifelineVariable), IOperator.LT, IExpression.variable(prerequisiteLifelineVariable));
          IExpression equalGuard = IExpression.relational(IExpression.variable(occurrenceLifelineVariable), IOperator.EQ, IExpression.variable(prerequisiteLifelineVariable));
          if (requireNonNull(this.histories.get(prerequisite.getLifeline())).contains(prerequisite))
            occurrenceGuard = IExpression.and(occurrenceGuard, IExpression.or(beforeGuard, equalGuard));
          else
            occurrenceGuard = IExpression.and(occurrenceGuard, beforeGuard);
        }
      }
      if (isReady) {
        // We may only start a new cycle, if the maximum has not been
        // reached yet
        if (occurrence.isFirstEvent() && !this.max.isInfty() && !this.min.isInfty()) {
          occurrenceGuard = IExpression.and(occurrenceGuard, IExpression.relational(IExpression.variable(occurrenceLifelineVariable), IOperator.LT, IExpression.intConst(this.max.getNatural())));
        }
        readySet.add(new Pair<>(occurrence, occurrenceGuard));
      }
    }
    return readySet;
  }

  /**
   * @return whether all lifeline histories are empty
   */
  private boolean hasEmptyHistories() {
    for (ULifeline lifeline : this.lifelines) {
      if (!requireNonNull(this.histories.get(lifeline)).isEmpty())
        return false;
    }
    return true;
  }

  @Override
  public ILoopPhase clone() {
    ILoopPhase clone = new ILoopPhase(this.occurrences, this.min, this.max, new HashSet<>(this.lifelines), new HashMap<>(this.variables), new HashMap<>());
    for (ULifeline lifeline : this.lifelines)
      clone.histories.put(lifeline, new ArrayList<>(this.histories.get(lifeline)));
    clone.finished = this.finished;
    clone.recurrent = this.recurrent;
    return clone;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.histories);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      ILoopPhase other = (ILoopPhase)object;
      return super.equals(other) &&
             Objects.equals(this.min, other.min) &&
             Objects.equals(this.max, other.max) &&
             Objects.equals(this.histories, other.histories) &&
             this.recurrent == other.recurrent &&
             this.finished == other.finished;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("LoopPhase [histories = ");
    resultBuilder.append(Formatter.set(this.lifelines, lifeline -> lifeline.getName() + " : " + this.histories.get(lifeline)));
    resultBuilder.append(", finished = ");
    resultBuilder.append(this.finished);
    resultBuilder.append(", recurrent = ");
    resultBuilder.append(this.recurrent);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
