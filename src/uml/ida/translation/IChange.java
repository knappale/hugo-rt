package uml.ida.translation;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.ida.IAction;
import uml.ida.ICondition;
import uml.interaction.UOccurrenceSpecification;

/**
 * Phase change description
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @since June 4, 2013
 */
@NonNullByDefault
public class IChange {
  private @Nullable UOccurrenceSpecification occurrence;
  private @Nullable ICondition condition;
  private @Nullable IAction action;
  private IPhase successor;

  IChange(@Nullable UOccurrenceSpecification occurrence, @Nullable ICondition condition, @Nullable IAction action, IPhase successor) {
    this.occurrence = occurrence;
    this.condition = condition;
    this.action = action;
    this.successor = successor;
  }

  public @Nullable UOccurrenceSpecification getOccurrence() {
    return this.occurrence;
  }

  public @Nullable ICondition getCondition() {
    return this.condition;
  }

  public @Nullable IAction getAction() {
    return this.action;
  }

  public IPhase getSuccessor() {
    return this.successor;
  }
}
