package uml.ida.translation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.interaction.ULifeline;
import uml.interaction.UBasicFragment;
import uml.interaction.UStateInvariant;
import uml.interaction.UOccurrenceSpecification;
import uml.interaction.UInteraction;
import uml.interaction.UInteractionFragment;
import uml.interaction.UInteractionOperand;
import uml.interaction.UTiming;
import uml.UBehavioural;
import uml.UClass;
import uml.UContext;
import uml.UExpression;
import uml.UModel;
import uml.UObject;
import uml.ida.IAction;
import uml.ida.IAutomaton;
import uml.ida.IBranch;
import uml.ida.IClock;
import uml.ida.ICondition;
import uml.ida.IExpression;
import uml.ida.IEvent;
import uml.ida.IObservation;
import uml.ida.IOperator;
import uml.ida.IState;
import uml.ida.IClockBound;
import uml.ida.ITransition;
import uml.ida.IVariable;
import util.Maps;
import util.Pair;
import util.Triple;
import util.Identifier;
import util.UnlimitedNatural;

import static java.util.stream.Collectors.toSet;
import static util.Objects.requireNonNull;


/**
 * Translate a UML 2.0 interaction into an Ida automaton.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de">Jochen Wuttke</A>
 */
@NonNullByDefault
public class ITranslator implements UInteractionFragment.Visitor<IAutomaton>, Runnable {
  private UInteraction interaction;
  private IAutomaton automaton;
  private Set<IVariable> variables = new HashSet<>();
  private Map<UOccurrenceSpecification.Data, IClock> clocks = new HashMap<>();

  public ITranslator(UInteraction interaction) {
    this.interaction = interaction;
    this.automaton = new IAutomaton();
  }

  /**
   * @return the resulting automaton
   */
  public IAutomaton getAutomaton() {
    return this.automaton;
  }

  /**
   * @return translator's underlying context
   */
  private UContext getContext() {
    return UContext.collaboration(this.interaction.getCollaboration());
  }
  
  /**
   * Needed to be able to run this in a separate thread. See explanation in Interaction.java.
   *
   * @see java.lang.Runnable#run()
   */
  public void run() {
    this.automaton = fragmentsAutomaton(this.interaction.getFragments());

    this.automaton.addVariables(this.variables);
    this.automaton.addLifelines(this.interaction.getLifelines());
    this.automaton.addMessages(this.interaction.getMessages());
    this.automaton.addTimings(this.interaction.getTimings());

    // Clocks
    for (UTiming timing : interaction.getTimings()) {
      UOccurrenceSpecification.Data lowerData = timing.getLowerData();
      if (clocks.get(lowerData) == null) {
        IClock clock = this.automaton.addClock();
        clocks.put(lowerData, clock);
      }
    }
    for (ITransition transition : this.automaton.getTransitions()) {
      IEvent trigger = transition.getTrigger();
      if (trigger == null)
        continue;
      UOccurrenceSpecification occurrence = trigger.getUOccurrenceSpecification();
      if (occurrence == null)
        continue;

      for (UTiming timing : interaction.getTimings()) {
        for (UOccurrenceSpecification lower : timing.getLowers()) {
          if (lower.equals(occurrence))
            transition.addEffect(IAction.reset(requireNonNull(this.clocks.get(timing.getLowerData()))));
        }
        for (UOccurrenceSpecification upper : timing.getUppers()) {
          if (occurrence.equals(upper))
            transition.addClockBound(translateTiming(timing));
        }
      }
    }
  }

  private IAutomaton fragmentsAutomaton(List<UInteractionFragment> fragments) {
    if (fragments.size() == 0)
      return new IAutomaton();

    // Create the automaton for the first fragment
    IAutomaton result = fragments.get(0).accept(this);

    // Now sequentially merge the other fragments step by step
    for (int i = 1; i < fragments.size(); i++)
      result = sequentialMerge(result, fragments.get(i).accept(this));

    return result;
  }

  private IAutomaton operandAutomaton(UInteractionOperand operand) {
    // Compute automaton for the operand's fragments
    IAutomaton result = fragmentsAutomaton(operand.getFragments());

    // If the operand's enabling condition is true, we are done
    UExpression operandCondition = operand.getCondition();
    if (UExpression.trueConst().equals(operandCondition))
      return result;

    // Prepare data for entering and not entering the operand
    Set<ULifeline> coveredLifelines = operand.getLifelines();
    IExpression conditionExpression = IExpression.external(operandCondition, this.getContext());
    ICondition entering = new ICondition(conditionExpression, coveredLifelines);
    ICondition notEntering = new ICondition(IExpression.neg(conditionExpression), coveredLifelines);

    // Add a new initial state and a new accepting state and ...
    IState oldInitialState = requireNonNull(result.getInitialState());
    IState newInitialState = result.addState();
    result.setInitialState(newInitialState);
    IState newAcceptingState = result.addState();
    result.addAcceptingState(newAcceptingState);

    // ... make a case distinction whether the operand is entered or not
    // If it's not entered the operand has been traversed successfully
    result.addTransition(newInitialState, entering, null, oldInitialState);
    result.addTransition(oldInitialState, notEntering, null, newAcceptingState);

    return result;
  }

  @Override
  public IAutomaton onBasic(UBasicFragment basic) {
    IAutomaton result = new IAutomaton();

    if (this.interaction.getProperties().isPhaseBased()) {
      result.setInitialState(unwindPhase(new IBasicPhase(basic), result, new HashMap<>()));
      return result;
    }

    if (basic.getOccurrences().isEmpty()) {
      IState accept = result.addState();
      result.addAcceptingState(accept);
      result.setInitialState(accept);
      return result;
    }

    IVariable history = result.addBitArrayVariable(basic.getOccurrences().size());
    this.variables.add(history);
    Map<UOccurrenceSpecification, Integer> numbers = new HashMap<>();
    int number = 0;
    for (UOccurrenceSpecification occurrence : basic.getOccurrences())
      numbers.put(occurrence, number++);
    result.setInitialState(unwindLifeline(basic, result, history, numbers, new HashSet<>(), new HashMap<>()));

    return result;
  }

  private IState unwindLifeline(UBasicFragment basic, IAutomaton result, IVariable history, Map<UOccurrenceSpecification, Integer> numbers, Set<ULifeline> lifelines, Map<Set<ULifeline>, IState> lifelinesMap) {
    IState lifelinesState = lifelinesMap.get(lifelines);
    if (lifelinesState != null)
      return lifelinesState;

    lifelinesState = result.addState();
    lifelinesMap.put(lifelines, lifelinesState);

    if (lifelines.size() == basic.getLifelines().size()) {
      IState accept = result.addState();
      result.addAcceptingState(accept);
      IExpression occurrenceGuard = IExpression.trueConst();
      for (UOccurrenceSpecification occurrence : basic.getOccurrences()) {
        occurrenceGuard = IExpression.and(occurrenceGuard, IExpression.array(history, requireNonNull(numbers.get(occurrence))));
      }
      Set<ULifeline> occurrenceLifelines = new HashSet<>();
      IAction occurrenceEffect = IAction.skip();
      result.addTransition(lifelinesState, new ICondition(occurrenceGuard, occurrenceLifelines), occurrenceEffect, accept);
    }

    for (UOccurrenceSpecification occurrence : basic.getOccurrences()) {
      Set<ULifeline> predecessorsLifelines = occurrence.getPrerequisitesLifelines();
      // The lifelines of all predecessors must show some event
      if (!lifelines.containsAll(predecessorsLifelines))
        continue;
      // For the first event of a lifeline, this lifeline must not have been covered before
      if (occurrence.isFirstEvent() && lifelines.contains(occurrence.getLifeline()))
        continue;

      IExpression occurrenceGuard = IExpression.neg(IExpression.array(history, requireNonNull(numbers.get(occurrence))));
      for (UOccurrenceSpecification prerequisite : occurrence.getPrerequisites())
        occurrenceGuard = IExpression.and(occurrenceGuard, IExpression.array(history, requireNonNull(numbers.get(prerequisite))));
      IAction occurrenceEffect = IAction.set(history, requireNonNull(numbers.get(occurrence)));
      Set<ULifeline> occurrenceLifelines = new HashSet<>();
      occurrenceLifelines.add(occurrence.getLifeline());

      Set<ULifeline> nextLifelines = new HashSet<>(lifelines);
      nextLifelines.add(occurrence.getLifeline());
      IState nextState = unwindLifeline(basic, result, history, numbers, nextLifelines, lifelinesMap);

      result.addTransition(lifelinesState, IEvent.occurrence(occurrence), new ICondition(occurrenceGuard, occurrenceLifelines), occurrenceEffect, nextState);
    }

    return lifelinesState;
  }

  @Override
  public IAutomaton onStateInvariant(UStateInvariant stateInvariant) {
    IAutomaton result = new IAutomaton();

    IState initial = result.addState();
    result.setInitialState(initial);

    IState accept = result.addState();
    result.addAcceptingState(accept);

    IExpression invariant = IExpression.external(stateInvariant.getInvariant(), this.getContext());
    ICondition condition = new ICondition(invariant, stateInvariant.getLifelines());
    result.addTransition(initial, condition, null, accept);

    return result;
  }

  @Override
  public IAutomaton onLoop(UBasicFragment basic, UnlimitedNatural min, UnlimitedNatural max) {
    IAutomaton result = new IAutomaton();

    Map<ULifeline, IVariable> variables = new HashMap<>();
    for (ULifeline lifeline : basic.getLifelines()) {
      variables.put(lifeline, result.addIntegerVariable());
      this.variables.add(requireNonNull(variables.get(lifeline)));
    }
    if (this.interaction.getProperties().isPhaseBased()) {
      result.setInitialState(unwindPhase(new ILoopPhase(min, max, basic, variables), result, new HashMap<>()));
      return result;
    }

    if (basic.getOccurrences().isEmpty()) {
      if (min.isInfty()) {
        IState recurrence = result.addState();
        result.addRecurrenceState(recurrence);
        result.setInitialState(recurrence);
        return result;        
      }

      IState accept = result.addState();
      result.addAcceptingState(accept);
      if (max.isInfty()) {
        IState recurrence = result.addState();
        result.addRecurrenceState(recurrence);
        result.setInitialState(recurrence);
        result.addTransition(recurrence, new ICondition(IExpression.trueConst(), new HashSet<>()), null, accept);
        return result;
      }

      result.setInitialState(accept);
      return result;
    }

    IVariable history = result.addBitArrayVariable(basic.getOccurrences().size());
    this.variables.add(history);
    Map<UOccurrenceSpecification, Integer> numbers = new HashMap<>();
    int number = 0;
    for (UOccurrenceSpecification occurrence : basic.getOccurrences())
      numbers.put(occurrence, number++);

    IState execute = result.addState();
    result.setInitialState(execute);
    IState recurrence = null;
    if (min.isInfty() || max.isInfty()) {
      recurrence = result.addState();
      result.addRecurrenceState(recurrence);
    }
    for (UOccurrenceSpecification occurrence : basic.getOccurrences()) {
      IExpression occurrenceGuard = IExpression.neg(IExpression.array(history, requireNonNull(numbers.get(occurrence))));
      IVariable occurrenceLifelineVariable = requireNonNull(variables.get(occurrence.getLifeline()));
      for (UOccurrenceSpecification prerequisite : occurrence.getPrerequisites()) {
        IExpression historyGuard = IExpression.array(history, requireNonNull(numbers.get(prerequisite)));
        if (occurrence.getLifeline().equals(prerequisite.getLifeline())) {
          occurrenceGuard = IExpression.and(occurrenceGuard, historyGuard);
        }
        else {
          IVariable prerequisiteLifelineVariable = requireNonNull(variables.get(prerequisite.getLifeline()));
          IExpression beforeGuard = IExpression.relational(IExpression.variable(occurrenceLifelineVariable), IOperator.LT, IExpression.variable(prerequisiteLifelineVariable));
          IExpression equalGuard = IExpression.relational(IExpression.variable(occurrenceLifelineVariable), IOperator.EQ, IExpression.variable(prerequisiteLifelineVariable));
          occurrenceGuard = IExpression.and(occurrenceGuard, IExpression.or(beforeGuard, IExpression.and(equalGuard, historyGuard)));
        }
      }
      if (occurrence.isFirstEvent() && !max.isInfty() && !min.isInfty()) {
        occurrenceGuard = IExpression.and(occurrenceGuard, IExpression.relational(IExpression.variable(occurrenceLifelineVariable), IOperator.LT, IExpression.intConst(max.getNatural())));
      }
      Set<ULifeline> occurrenceLifelines = new HashSet<>();
      occurrenceLifelines.add(occurrence.getLifeline());

      IAction occurrenceEffect = IAction.set(history, requireNonNull(numbers.get(occurrence)));
      if (occurrence.isLastEvent()) {
        occurrenceEffect = IAction.inc(requireNonNull(variables.get(occurrence.getLifeline())));
        for (UOccurrenceSpecification lifelineOccurrence : basic.getOccurrences()) {
          if (lifelineOccurrence.getLifeline().equals(occurrence.getLifeline()))
            occurrenceEffect = IAction.seq(occurrenceEffect, IAction.reset(history, requireNonNull(numbers.get(lifelineOccurrence))));
        }

        if (min.isInfty() || max.isInfty()) {
          assert (recurrence != null);
          IExpression recurrenceGuard = IExpression.trueConst();
          for (UOccurrenceSpecification otherOccurrence : basic.getOccurrences()) {
            if (!otherOccurrence.getLifeline().equals(occurrence.getLifeline()))
              recurrenceGuard = IExpression.and(recurrenceGuard, IExpression.neg(IExpression.array(history, requireNonNull(numbers.get(otherOccurrence)))));
          }
          List<ULifeline> lifelines = new ArrayList<>(basic.getLifelines());
          for (int i = 0; i < lifelines.size(); i++) {
            if (!lifelines.get(i).equals(occurrence.getLifeline()))
              recurrenceGuard = IExpression.and(recurrenceGuard, IExpression.relational(IExpression.arithmetical(IExpression.variable(occurrenceLifelineVariable), IOperator.PLUS, IExpression.intConst(1)), IOperator.EQ, IExpression.variable(requireNonNull(variables.get(lifelines.get(i))))));
          }
          if (!min.isInfty())
            recurrenceGuard = IExpression.and(recurrenceGuard, IExpression.relational(IExpression.arithmetical(IExpression.variable(occurrenceLifelineVariable), IOperator.PLUS, IExpression.intConst(1)), IOperator.GEQ, IExpression.intConst(min.getNatural())));
          Set<ULifeline> recurrenceLifelines = new HashSet<>();
          recurrenceLifelines.add(occurrence.getLifeline());
          result.addTransition(execute, IEvent.occurrence(occurrence), new ICondition(IExpression.and(occurrenceGuard, recurrenceGuard), recurrenceLifelines), occurrenceEffect, recurrence);
          occurrenceGuard = IExpression.and(occurrenceGuard, IExpression.neg(recurrenceGuard));
        }
      }

      result.addTransition(execute, IEvent.occurrence(occurrence), new ICondition(occurrenceGuard, occurrenceLifelines), occurrenceEffect, execute);
    }

    if (!min.isInfty()) {
      IState accept = result.addState();
      result.addAcceptingState(accept);

      IExpression acceptanceGuard = IExpression.trueConst();
      for (UOccurrenceSpecification otherOccurrence : basic.getOccurrences()) {
        acceptanceGuard = IExpression.and(acceptanceGuard, IExpression.neg(IExpression.array(history, requireNonNull(numbers.get(otherOccurrence)))));
      }
      List<ULifeline> lifelines = new ArrayList<>(basic.getLifelines());
      for (int i = 0; i < lifelines.size()-1; i++) {
        acceptanceGuard = IExpression.and(acceptanceGuard, IExpression.relational(IExpression.variable(requireNonNull(variables.get(lifelines.get(i)))), IOperator.EQ, IExpression.variable(requireNonNull(variables.get(lifelines.get(i+1))))));
      }
      acceptanceGuard = IExpression.and(acceptanceGuard, IExpression.relational(IExpression.variable(requireNonNull(variables.get(lifelines.get(0)))), IOperator.GEQ, IExpression.intConst(min.getNatural())));
      Set<ULifeline> acceptanceLifelines = new HashSet<>();
      // if (min > 0) {
      //   for (OccurrenceSpecification occurrence : basic.getOccurrences()) {
      //     acceptanceLifelines.add(occurrence.getLifeline());
      //   }
      // }
      IAction acceptanceAction = IAction.reset(requireNonNull(variables.get(lifelines.get(0))));
      for (int i = 1; i < lifelines.size(); i++) {
        acceptanceAction = IAction.seq(acceptanceAction, IAction.reset(requireNonNull(variables.get(lifelines.get(i)))));
      }
      result.addTransition(execute, new ICondition(acceptanceGuard, acceptanceLifelines), null, accept);
    }

    if (min.isInfty() || max.isInfty()) {
      assert (recurrence != null);
      IAction recurrenceEffect = IAction.skip();
      for (ULifeline lifeline : basic.getLifelines()) {
        recurrenceEffect = IAction.seq(recurrenceEffect, IAction.assign(requireNonNull(variables.get(lifeline)), IExpression.intConst(min.isInfty() ? 0 : min.getNatural())));
      }
      result.addTransition(recurrence, new ICondition(IExpression.trueConst(), new HashSet<>()), recurrenceEffect, execute);
    }

    return result;
  }

  /**
   * Unwind a phase and return the state representing that phase.
   *
   * @param phase a phase
   * @param result result automaton
   * @return a state representing <b>phase</b>
   */
  private IState unwindPhase(IPhase phase, IAutomaton result, Map<IPhase, IState> phaseMap) {
    // Check if an equivalent state already exists
    IState phaseState = phaseMap.get(phase);
    if (phaseState != null)
      return phaseState;

    phaseState = result.addState();
    phaseMap.put(phase, phaseState);

    if (phase.isAccepting()) {
      result.addAcceptingState(phaseState);
    }
    if (phase.isRecurrent()) {
      result.addRecurrenceState(phaseState);
    }
    
    for (IChange successors : phase.getSuccessors()) {
      UOccurrenceSpecification successorsOccurrence = successors.getOccurrence();
      IEvent trigger = (successorsOccurrence == null ? null : IEvent.occurrence(successorsOccurrence));
      ICondition guard = successors.getCondition();
      IAction effect = successors.getAction();
      IPhase nextPhase = successors.getSuccessor();
      IState nextState = unwindPhase(nextPhase, result, phaseMap);

      result.addTransition(phaseState, trigger, guard, effect, nextState);
    }
    
    return phaseState;
  }

  @Override
  public IAutomaton onAlt(List<UInteractionOperand> operands) {
    IAutomaton result = new IAutomaton();
    IState resultInitialState = result.addState();
    result.setInitialState(resultInitialState);
    for (UInteractionOperand operand : operands) {
      IAutomaton operandAutomaton = fragmentsAutomaton(operand.getFragments());
      result.addAutomaton(operandAutomaton);
      ICondition condition = new ICondition(IExpression.external(operand.getCondition(), this.getContext()), operand.getLifelines());
      result.addTransition(resultInitialState, condition, null, requireNonNull(operandAutomaton.getInitialState()));
    }
    
    // TODO (JW060512) which lifelines are covered by this transition?
    UExpression negCondition = UExpression.neg(operands.stream().map(operand -> operand.getCondition()).
                                               reduce(UExpression.trueConst(), UExpression::and));
    if (!negCondition.equals(UExpression.falseConst())) {
      IState acceptingState = result.addState();
      ICondition condition = new ICondition(IExpression.external(negCondition, this.getContext()), new HashSet<>());
      result.addTransition(resultInitialState, condition, null, acceptingState);
      result.addAcceptingState(acceptingState);
    }

    return result;
  }

  @Override
  public IAutomaton onPAlt(List<UInteractionOperand> operands) {
    IAutomaton result = new IAutomaton();
    IState resultInitialState = result.addState();
    result.setInitialState(resultInitialState);

    List<IBranch> operandBranches = new ArrayList<>();
    for (UInteractionOperand operand : operands) {
      IAutomaton operandAutomaton = fragmentsAutomaton(operand.getFragments());
      result.addAutomaton(operandAutomaton);
      operandBranches.add(new IBranch(new IBranch.Label(operand.getRate(), null), requireNonNull(operandAutomaton.getInitialState())));
    }
    result.addTransition(resultInitialState, new ITransition.Label(null, null, null), operandBranches);

    return result;   
  }

  @Override
  public IAutomaton onConsider(UInteractionOperand operand, Set<Identifier> considers) {
    IAutomaton result = this.operandAutomaton(operand);

    // Determine all behaviourals whose name matches one of the considered message identifiers
    var uBehaviourals = new HashSet<UBehavioural>();
    for (UClass uClass : this.interaction.getCollaboration().getModel().getClasses())
      uBehaviourals.addAll(uClass.getBehaviourals().stream().filter(uBehavioural -> considers.stream().anyMatch(uIdentifier -> Identifier.id(uBehavioural.getName()).matches(uIdentifier))).collect(toSet()));

    // Determine all possible objects
    var uObjects = this.interaction.getCollaboration().getObjects();

    // Determine all objects that are unaffected
    var sendings = new HashMap<UObject, Set<UBehavioural>>();
    var receivings = new HashMap<UObject, Set<UBehavioural>>();
    for (UObject uObject : uObjects) {
      for (UBehavioural uBehavioural : uBehaviourals) {
        var uClass = uObject.getC1ass();
        if (uClass.getBehaviourals().contains(uBehavioural))
          Maps.update(sendings, uObject, uBehavioural);
        var uStateMachine = uClass.getStateMachine();
        if (uStateMachine != null && uStateMachine.getInvokeds().contains(uBehavioural))
          Maps.update(receivings, uObject, uBehavioural);
      }
    }

    // Add in the automaton transitions to every state for ignoring
    // any observation on an active object
    for (IState iState : result.getStates()) {
      for (var uObject : uObjects) {
        var iObservation = IObservation.expression(IExpression.trueConst());
        @Nullable Set<UBehavioural> receiving = receivings.get(uObject);
        if (receiving != null) {
          for (var uBehavioural : receiving)
            iObservation = IObservation.and(iObservation, IObservation.not(IObservation.and(IObservation.receive(), IObservation.behavioural(uBehavioural))));
        }
        @Nullable Set<UBehavioural> sending = sendings.get(uObject);
        if (sending != null) {
          for (var uBehavioural : sending)
            iObservation = IObservation.and(iObservation, IObservation.not(IObservation.and(IObservation.send(), IObservation.behavioural(uBehavioural))));
        }
        result.addTransition(iState, IEvent.observation(uObject, iObservation), null, null, iState);
      }
    }

    return result;
  }

  public IAutomaton onConsiderOld(UInteractionOperand operand, Set<Identifier> considers) {
    IAutomaton result = this.operandAutomaton(operand);

    // Determine all behaviourals whose name matches none of the considered
    // message identifiers
    Set<UBehavioural> uBehaviourals = new HashSet<>();
    UModel uModel = this.interaction.getCollaboration().getModel();
    for (UClass uClass : uModel.getClasses()) {
      for (UBehavioural uBehavioural : uClass.getBehaviourals()) {
        boolean matched = false;
        for (Identifier uIdentifier : considers) {
          if (Identifier.id(uBehavioural.getName()).matches(uIdentifier)) {
            matched = true;
            break;
          }
        }
        if (!matched)
          uBehaviourals.add(uBehavioural);
      }
    }

    // Determine all possible objects
    Set<UObject> uObjects = new HashSet<>(this.interaction.getCollaboration().getObjects());

    // Add in the automaton transitions to every state for ignoring
    // a behavioural with a given active object
    for (IState iState : result.getStates()) {
      for (UObject uObject : uObjects) {
        for (UBehavioural uBehavioural : uBehaviourals) {
          var uClass = uObject.getC1ass();
          var uStateMachine = uClass.getStateMachine();
          if (uClass.getBehaviourals().contains(uBehavioural) ||
              (uStateMachine != null && uStateMachine.getInvokeds().contains(uBehavioural))) {
            System.out.println(uObject);
            System.out.println(uBehavioural);
            result.addTransition(iState, IEvent.behavioural(uObject, uBehavioural), null, null, iState);
          }
        }
      }
    }

    return result;
  }

  @Override
  public IAutomaton onIgnore(UInteractionOperand operand, Set<Identifier> ignores) {
    IAutomaton result = this.operandAutomaton(operand);

    // Determine all behaviourals whose name matches one of the ignored message identifiers
    var uBehaviourals = new HashSet<UBehavioural>();
    for (UClass uClass : this.interaction.getCollaboration().getModel().getClasses())
      uBehaviourals.addAll(uClass.getBehaviourals().stream().filter(uBehavioural -> ignores.stream().anyMatch(uIdentifier -> Identifier.id(uBehavioural.getName()).matches(uIdentifier))).collect(toSet()));

    // Determine all possible objects
    var uObjects = this.interaction.getCollaboration().getObjects();

    // Determine all objects that are unaffected
    var sendings = new HashMap<UObject, Set<UBehavioural>>();
    var receivings = new HashMap<UObject, Set<UBehavioural>>();
    for (UObject uObject : uObjects) {
      for (UBehavioural uBehavioural : uBehaviourals) {
        var uClass = uObject.getC1ass();
        if (uClass.getBehaviourals().contains(uBehavioural))
          Maps.update(sendings, uObject, uBehavioural);
        var uStateMachine = uClass.getStateMachine();
        if (uStateMachine != null && uStateMachine.getInvokeds().contains(uBehavioural))
          Maps.update(receivings, uObject, uBehavioural);
      }
    }

    // Add in the automaton transitions to every state for ignoring
    // any observation on an active object
    for (IState iState : result.getStates()) {
      for (var uObject : uObjects) {
        var iObservation = IObservation.expression(IExpression.falseConst());
        @Nullable Set<UBehavioural> receiving = receivings.get(uObject);
        if (receiving != null) {
          for (var uBehavioural : receiving)
            iObservation = IObservation.or(iObservation, IObservation.and(IObservation.receive(), IObservation.behavioural(uBehavioural)));
        }
        @Nullable Set<UBehavioural> sending = sendings.get(uObject);
        if (sending != null) {
          for (var uBehavioural : sending)
            iObservation = IObservation.or(iObservation, IObservation.and(IObservation.send(), IObservation.behavioural(uBehavioural)));
        }
        result.addTransition(iState, IEvent.observation(uObject, iObservation), null, null, iState);
      }
    }

    return result;
  }

  @Override
  public IAutomaton onNot(UInteractionOperand operand) {
    IAutomaton result = this.operandAutomaton(operand);

    Set<IState> states = new HashSet<>(result.getStates());
    Set<UObject> objects = new HashSet<>(this.interaction.getCollaboration().getObjects());

    // Complement accepting and non-accepting states
    Set<IState> acceptingStates = new HashSet<IState>(result.getAcceptingStates());
    for (IState state : result.getStates()) {
      if (acceptingStates.contains(state))
        result.removeAcceptingState(state);
      else
        result.addAcceptingState(state);
    }

    // Add a final, accepting state with self-loops for events for all objects;
    // this is also a recurrence state as we can dwell there infinitely long
    IState finalState = result.addState();
    result.addAcceptingState(finalState);
    result.addRecurrenceState(finalState);
    for (UObject object : objects)
      result.addTransition(finalState, IEvent.active(object), null, null, finalState);

    // Add transitions from every old state to the final state firing on any
    // occurrence not satisfying an outgoing transition from the state 
    for (IState state : states) {
      Set<ITransition> triggeredTransitions = new HashSet<>();
      Set<ITransition> triggerlessTransitions = new HashSet<>();
      for (ITransition transition : result.getOutgoings(state)) {
        if (transition.getTrigger() == null)
          triggerlessTransitions.add(transition);
        else
          triggeredTransitions.add(transition);
      }

      if (!triggerlessTransitions.isEmpty()) {
        IExpression notExpression = IExpression.falseConst();
        for (ITransition transition : triggerlessTransitions) {
          ICondition transitionGuard = transition.getGuard();
          if (transitionGuard != null)
            notExpression = IExpression.or(notExpression, transitionGuard.getCondition());
        }
        ICondition notCondition = new ICondition(IExpression.neg(notExpression), operand.getLifelines());
        result.addTransition(state, notCondition, null, finalState);
      }

      IObservation notSpecification = null;
      for (ITransition transition : triggeredTransitions) {
        IEvent transitionTrigger = requireNonNull(transition.getTrigger());
        IObservation triggerSpecification = transitionTrigger.getObservation();
        ICondition transitionGuard = transition.getGuard();
        if (transitionGuard != null)
          triggerSpecification = IObservation.and(triggerSpecification, IObservation.expression(transitionGuard.getCondition()));
        notSpecification = (notSpecification == null ? triggerSpecification : IObservation.or(notSpecification, triggerSpecification));
      }
      for (UObject object : objects) {
        IEvent objectTrigger = IEvent.active(object);
        if (notSpecification != null)
          objectTrigger = IEvent.observation(object, IObservation.not(notSpecification));
        result.addTransition(state, objectTrigger, null, null, finalState);
      }
    }

    return result;
  }

  @Override
  public IAutomaton onOpt(UInteractionOperand operand) {
    IAutomaton result = fragmentsAutomaton(operand.getFragments());

    // Add a new final state
    IState finalState = result.addState();
    result.addAcceptingState(finalState);

    // This can be reached by running through the operand's automaton
    IExpression conditionExpression = IExpression.external(operand.getCondition(), this.getContext());
    if (!conditionExpression.equals(IExpression.trueConst())) {
      IState initialState = result.addState();
      ICondition condition = new ICondition(conditionExpression, operand.getLifelines());
      result.addTransition(initialState, condition, null, requireNonNull(result.getInitialState()));
      result.setInitialState(initialState);
      // If the operand's condition is not true, skip the fragment's automaton
      condition = new ICondition(IExpression.neg(conditionExpression), operand.getLifelines());
      result.addTransition(initialState, condition, null, finalState);
    }

    // Or the operand automaton can be skipped (empty alternative)
    ICondition condition = new ICondition(IExpression.trueConst(), operand.getLifelines());
    result.addTransition(requireNonNull(result.getInitialState()), condition, null, finalState);

    return result;
  }

  @Override
  public IAutomaton onPar(List<UInteractionOperand> operands) {
    // Create the automaton for the first operand
    IAutomaton result = this.operandAutomaton(operands.get(0));
      
    // Now merge the other operands step by step
    for (int i = 1; i < operands.size(); i++)
      result = parallelMerge(result, this.operandAutomaton(operands.get(i)));

    return result;
  }

  private IAutomaton parallelMerge(IAutomaton first, IAutomaton second) {
    IAutomaton result = new IAutomaton();
    result.setInitialState(parallelMerge(first, second, result, 0, new HashMap<>()));
    return result;
  }

  private IState parallelMerge(IAutomaton first, IAutomaton second, IAutomaton result, int phase, Map<Triple<IState, IState, Integer>, IState> statesMap) {
    IState firstInitialState = requireNonNull(first.getInitialState());
    IState secondInitialState = requireNonNull(second.getInitialState());
    IState mergedInitialState = statesMap.get(new Triple<>(firstInitialState, secondInitialState, phase));
    if (mergedInitialState != null)
      return mergedInitialState;
    
    mergedInitialState = result.addState();
    statesMap.put(new Triple<>(firstInitialState, secondInitialState, phase), mergedInitialState);

    if (first.isAcceptingState(firstInitialState) && second.isAcceptingState(secondInitialState)) {
      result.addAcceptingState(mergedInitialState);
    }
    if (phase == 2) {
      result.addRecurrenceState(mergedInitialState);
    }

    for (ITransition firstTransition : first.getOutgoings(firstInitialState)) {
      List<IBranch> translatedBranches = new ArrayList<>();
      for (IBranch firstTransitionBranch : firstTransition.getBranches()) {
        first.setInitialState(firstTransitionBranch.getTo());
        // Accepting states are treated as recurrent states, if the other
        // automaton shows recurrence states
        int nextPhase = phase;
        if (phase == 0 &&
            (first.isRecurrenceState(firstTransitionBranch.getTo()) ||
            (first.isAcceptingState(firstTransitionBranch.getTo()) && !second.getRecurrenceStates().isEmpty())))
          nextPhase = 1;
        else
          if (phase == 2)
            nextPhase = 0;
        translatedBranches.add(new IBranch(firstTransitionBranch.getLabel(), parallelMerge(first, second, result, nextPhase, statesMap)));
      }
      first.setInitialState(firstInitialState);
      result.addTransition(mergedInitialState, firstTransition.getLabel(), translatedBranches);
    }

    for (ITransition secondTransition : second.getOutgoings(secondInitialState)) {
      List<IBranch> translatedBranches = new ArrayList<>();
      for (IBranch secondTransitionBranch : secondTransition.getBranches()) {
        second.setInitialState(secondTransitionBranch.getTo());
        // Accepting states are treated as recurrent states, if the other
        // automaton shows recurrence states
        int nextPhase = phase;
        if (phase == 1 &&
            (second.isRecurrenceState(secondTransitionBranch.getTo()) ||
            (second.isAcceptingState(secondTransitionBranch.getTo()) && !first.getRecurrenceStates().isEmpty())))
          nextPhase = 2;
        else
          if (phase == 2)
            nextPhase = 0;
        translatedBranches.add(new IBranch(secondTransitionBranch.getLabel(), parallelMerge(first, second, result, nextPhase, statesMap)));
      }
      second.setInitialState(secondInitialState);
      result.addTransition(mergedInitialState, secondTransition.getLabel(), translatedBranches);
    }

    return mergedInitialState;
  }

  @Override
  public IAutomaton onSeq(List<UInteractionOperand> operands) {
    // Create the automaton for the first operand
    IAutomaton result = this.operandAutomaton(operands.get(0));
      
    // Now merge the other operands step by step
    for (int i = 1; i < operands.size(); i++)
      result = sequentialMerge(result, this.operandAutomaton(operands.get(i)));

    return result;
  }

  private IAutomaton sequentialMerge(IAutomaton first, IAutomaton second) {
    IAutomaton result = new IAutomaton();
    result.setInitialState(sequentialMerge(first, second, result, 0, new HashMap<>()));
    return result;
  }

  private IState sequentialMerge(IAutomaton first, IAutomaton second, IAutomaton result, int phase, Map<Triple<IState, IState, Integer>, IState> statesMap) {
    IState firstInitialState = requireNonNull(first.getInitialState());
    IState secondInitialState = requireNonNull(second.getInitialState());
    IState mergedInitialState = statesMap.get(new Triple<>(firstInitialState, secondInitialState, phase));
    if (mergedInitialState != null)
      return mergedInitialState;
    
    mergedInitialState = result.addState();
    statesMap.put(new Triple<>(firstInitialState, secondInitialState, phase), mergedInitialState);

    if (first.isAcceptingState(firstInitialState) && second.isAcceptingState(secondInitialState))
      result.addAcceptingState(mergedInitialState);
    if (phase == 2)
      result.addRecurrenceState(mergedInitialState);

    for (ITransition firstTransition : first.getOutgoings(firstInitialState)) {
      List<IBranch> translatedBranches = new ArrayList<>();
      for (IBranch firstTransitionBranch : firstTransition.getBranches()) {
        first.setInitialState(firstTransitionBranch.getTo());
        // Accepting states are treated as recurrent states, if the other
        // automaton shows recurrence states
        int nextPhase = phase;
        if (phase == 0 &&
            (first.isRecurrenceState(firstTransitionBranch.getTo()) ||
            (first.isAcceptingState(firstTransitionBranch.getTo()) && !second.getRecurrenceStates().isEmpty())))
          nextPhase = 1;
        else
          if (phase == 2)
            nextPhase = 0;
        translatedBranches.add(new IBranch(firstTransitionBranch.getLabel(), sequentialMerge(first, second, result, nextPhase, statesMap)));
      }

      first.setInitialState(firstInitialState);
      result.addTransition(mergedInitialState, firstTransition.getLabel(), translatedBranches);
    }

    for (ITransition secondTransition : second.getOutgoings(secondInitialState)) {
      // Get restricted first automaton
      IAutomaton restrictedFirst = restricted(first, secondTransition, statesMap);
      if (!(restrictedFirst.isAccepting() || restrictedFirst.isRecurring()))
        continue;

      List<IBranch> translatedBranches = new ArrayList<>();
      for (IBranch secondTransitionBranch : secondTransition.getBranches()) {
        second.setInitialState(secondTransitionBranch.getTo());
        // Accepting states are treated as recurrent states, if the other
        // automaton shows recurrence states
        int nextPhase = phase;
        if (phase == 1 &&
            (second.isRecurrenceState(secondTransitionBranch.getTo()) ||
            (second.isAcceptingState(secondTransitionBranch.getTo()) && !restrictedFirst.getRecurrenceStates().isEmpty())))
          nextPhase = 2;
        else
          if (phase == 2)
            nextPhase = 0;
        translatedBranches.add(new IBranch(secondTransitionBranch.getLabel(), sequentialMerge(restrictedFirst, second, result, nextPhase, statesMap)));
      }

      second.setInitialState(secondInitialState);
      result.addTransition(mergedInitialState, secondTransition.getLabel(), translatedBranches);
    }

    return mergedInitialState;
  }

  /**
   * Restrict an Ida automaton by a transition building a map into
   * which states existing states of the automaton are mapped.
   *
   * If the restricted automaton is neither accepting nor recurring the empty
   * automaton is returned and the states map will not be changed.
   *
   * @param automaton an Ida automaton
   * @param restrictingTransition an Ida transition
   * @param statesMap map of changed states
   * @return restricted automaton
   */
  private IAutomaton restricted(IAutomaton automaton, ITransition restrictingTransition, Map<Triple<IState, IState, Integer>, IState> statesMap) {
    IAutomaton result = new IAutomaton();

    IState initialState = automaton.getInitialState();
    if (initialState == null)
      return result;
    Map<IState, IState> restrictionMap = new HashMap<>();
    result.setInitialState(restricted(automaton, initialState, restrictingTransition, result, restrictionMap));
    if (!(result.isAccepting() || result.isRecurring()))
      return new IAutomaton();

    // Update states map for copied states
    Set<Triple<IState, IState, Integer>> statesMapKeys = new HashSet<>(statesMap.keySet());
    for (Triple<IState, IState, Integer> stateKey : statesMapKeys) {
      IState state = requireNonNull(statesMap.get(stateKey));
      IState restrictedState = restrictionMap.get(stateKey.getFirst());
      if (restrictedState != null && automaton.getOutgoings(state).equals(result.getOutgoings(restrictedState))) {
        statesMap.put(new Triple<>(restrictedState, stateKey.getSecond(), stateKey.getThird()), state);
      }
    }

    return result;
  }

  /**
   * Determine a restricted automaton from an automaton, starting in a state.
   *
   * @param automaton an automaton
   * @param state a state of {@code automaton}
   * @param restrictingTransition a transition possibly restricting {@code automaton}
   * @param result a (partially restricted) automaton
   * @param statesMap a map from state to state, connecting {@code automaton} with {@code result}
   * @return initial state of {@code result}
   */
  private IState restricted(IAutomaton automaton, IState state, ITransition restrictingTransition, IAutomaton result, Map<IState, IState> statesMap) {
    IState restrictedState = statesMap.get(state);
    if (restrictedState != null)
      return restrictedState;

    restrictedState = result.addState();
    statesMap.put(state, restrictedState);

    if (automaton.isAcceptingState(state))
      result.addAcceptingState(restrictedState);
    if (automaton.isRecurrenceState(state))
      result.addRecurrenceState(restrictedState);

    for (ITransition transition : automaton.getOutgoings(state)) {
      if (transition.isInConflict(restrictingTransition))
        continue;

      List<IBranch> restrictedBranches = new ArrayList<>();
      for (IBranch branch : transition.getBranches())
        restrictedBranches.add(new IBranch(branch.getLabel(), restricted(automaton, branch.getTo(), restrictingTransition, result, statesMap)));
      result.addTransition(restrictedState, transition.getLabel(), restrictedBranches);
    }

    return restrictedState;
  }

  @Override
  public IAutomaton onStrict(List<UInteractionOperand> operands) {
    // Create the automaton for the first operand
    IAutomaton result = this.operandAutomaton(operands.get(0));
      
    // Now merge the other operands step by step
    for (int i = 1; i < operands.size(); i++)
      result = strictMerge(result, this.operandAutomaton(operands.get(i)));

    return result;
  }

  private IAutomaton strictMerge(IAutomaton first, IAutomaton second) {
    IAutomaton result = new IAutomaton();
    result.setInitialState(strictMerge(first, second, result, new HashMap<>()));
    return result;
  }

  private IState strictMerge(IAutomaton first, IAutomaton second, IAutomaton result, Map<Pair<IState, IState>, IState> statesMap) {
    IState firstState = requireNonNull(first.getInitialState());
    IState secondState = requireNonNull(second.getInitialState());
    IState mergedState = statesMap.get(new Pair<>(firstState, secondState));
    if (mergedState != null)
      return mergedState;
    
    mergedState = result.addState();
    statesMap.put(new Pair<>(firstState, secondState), mergedState);

    if (second.isAcceptingState(secondState)) {
      result.addAcceptingState(mergedState);
    }
    if (first.isRecurrenceState(firstState) || second.isRecurrenceState(secondState)) {
      result.addRecurrenceState(mergedState);
    }

    for (ITransition firstTransition : first.getOutgoings(firstState)) {
      List<IBranch> translatedBranches = new ArrayList<>();
      for (IBranch firstTransitionBranch : firstTransition.getBranches()) {
        first.setInitialState(firstTransitionBranch.getTo());
        translatedBranches.add(new IBranch(firstTransitionBranch.getLabel(), strictMerge(first, second, result, statesMap)));
      }
      first.setInitialState(firstState);
      result.addTransition(mergedState, firstTransition.getLabel(), translatedBranches);
    }

    if (first.isAcceptingState(firstState)) {
      for (ITransition secondTransition : second.getOutgoings(secondState)) {
        List<IBranch> translatedBranches = new ArrayList<>();
        for (IBranch secondTransitionBranch : secondTransition.getBranches()) {
          second.setInitialState(secondTransitionBranch.getTo());
          translatedBranches.add(new IBranch(secondTransitionBranch.getLabel(), strictMerge(first, second, result, statesMap)));
        }
        second.setInitialState(secondState);
        result.addTransition(mergedState, secondTransition.getLabel(), translatedBranches);
      }
    }

    return mergedState;
  }

  @Override
  public IAutomaton onStrictLoop(UInteractionOperand operand, UnlimitedNatural min, UnlimitedNatural max) {
    IAutomaton result = this.operandAutomaton(operand);

    if (!min.isInfty()) {
      // Add a counter
      IVariable counterVariable = result.addIntegerVariable();
      this.variables.add(counterVariable);

      // Add new initial and final states
      IState newInitialState = result.addState();
      IState newFinalState = result.addState();

      IExpression finishedGuard = IExpression.relational(IExpression.variable(counterVariable), IOperator.GEQ, IExpression.intConst(min.getNatural()));
      if (!max.isInfty())
        finishedGuard = IExpression.and(finishedGuard, IExpression.relational(IExpression.variable(counterVariable), IOperator.LEQ, IExpression.intConst(max.getNatural())));
      IAction finishedEffect = IAction.reset(counterVariable);
      ICondition finishedCondition = new ICondition(finishedGuard, new HashSet<>());
      result.addTransition(newInitialState, finishedCondition, finishedEffect, newFinalState);
      ICondition notFinishedCondition = new ICondition(IExpression.neg(finishedGuard), operand.getLifelines());
      result.addTransition(newInitialState, notFinishedCondition, null, requireNonNull(result.getInitialState()));
      if (max.isInfty()) {
        IAction notFinishedEffect = IAction.set(counterVariable, min.getNatural());
        result.addTransition(newInitialState, finishedCondition, notFinishedEffect, requireNonNull(result.getInitialState()));
      }

      result.setInitialState(newInitialState);

      IAction iterationEffect = IAction.inc(counterVariable);
      @NonNull Set<IState> acceptingStates = new HashSet<>(result.getAcceptingStates());
      for (IState acceptingState : acceptingStates) {
        result.addTransition(acceptingState, null, null, iterationEffect, newInitialState);
        result.removeAcceptingState(acceptingState);
      }
      result.addAcceptingState(newFinalState);
      if (max.isInfty()) {
        result.addRecurrenceState(newInitialState);
      }
    }
    else {
      result.addRecurrenceState(requireNonNull(result.getInitialState()));
      @NonNull Set<IState> acceptingStates = new HashSet<>(result.getAcceptingStates());
      for (IState acceptingState : acceptingStates) {
        result.addTransition(acceptingState, null, null, null, requireNonNull(result.getInitialState()));
        result.removeAcceptingState(acceptingState);
      }
    }

    return result;
  }

  private class TimingTranslator implements UTiming.InContextVisitor<IClockBound> {
    private UContext context;

    TimingTranslator(UContext context) {
      this.context = context;
    }
    
    IClockBound clockBound(UTiming timing) {
      return timing.accept(this);
    }

    public @NonNull UContext getContext() {
      return this.context;
    }

    IClock getClock(UOccurrenceSpecification.Data occurrenceData) {
      IClock clock = clocks.get(occurrenceData);
      if (clock == null)
        throw new RuntimeException("No clock for occurrence specification `" + occurrenceData.toString() + "'");
      return clock;
    }

    public IClockBound onLt(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound) {
      return IClockBound.lt(getClock(lowerData), IExpression.external(bound, this.getContext()));
    }

    public IClockBound onLeq(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound) {
      return IClockBound.leq(getClock(lowerData), IExpression.external(bound, this.getContext()));
    }

    public IClockBound onGt(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound) {
      return IClockBound.gt(getClock(lowerData), IExpression.external(bound, this.getContext()));
    }

    public IClockBound onGeq(UOccurrenceSpecification.Data upperData, UOccurrenceSpecification.Data lowerData, UExpression bound) {
      return IClockBound.geq(getClock(lowerData), IExpression.external(bound, this.getContext()));
    }
  }

  private IClockBound translateTiming(UTiming timing) {
    UContext uContext = UContext.interaction(this.interaction);
    return new TimingTranslator(uContext).clockBound(timing);
  }
}
