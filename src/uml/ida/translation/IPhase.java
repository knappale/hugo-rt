package uml.ida.translation;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.interaction.UOccurrenceSpecification;


/**
 * Abstract phase
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
abstract class IPhase {
  Set<UOccurrenceSpecification> occurrences = new HashSet<>();

  IPhase(Set<UOccurrenceSpecification> occurrences) {
    this.occurrences = occurrences;
  }

  abstract boolean isAccepting();

  abstract boolean isRecurrent();

  abstract Set<IChange> getSuccessors();

  @Override
  public int hashCode() {
    return Objects.hash(this.occurrences);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      IPhase other = (IPhase)object;
      return Objects.equals(this.occurrences, other.occurrences);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
}
