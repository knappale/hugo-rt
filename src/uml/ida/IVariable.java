package uml.ida;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class IVariable {
  enum Kind {
    INTEGER,
    BITARRAY;
  }

  private Kind kind;
  private String name = "";
  private int length;

  private IVariable(Kind kind) {
    this.kind = kind;
  }

  public static IVariable integer(String name) {
    IVariable v = new IVariable(Kind.INTEGER);
    v.name = name;
    return v;
  }

  public static IVariable bitArray(String name, int length) {
    IVariable v = new IVariable(Kind.BITARRAY);
    v.name = name;
    v.length = length;
    return v;
  }

  /**
   * @return variable's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return variable's length
   */
  public int getLength() {
    return this.length;
  }

  /**
   * @return whether this variable is an integer variable
   */
  public boolean isInteger() {
    return this.kind == Kind.INTEGER;
  }

  /**
   * @return whether this variable is a bit array variable
   */
  public boolean isBitArray() {
    return this.kind == Kind.BITARRAY;
  }

  /**
   * @return variable declaration
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    switch (this.kind) {
      case INTEGER:
        resultBuilder.append("int");
        resultBuilder.append(" ");
        resultBuilder.append(this.getName());
        resultBuilder.append(";");
        break;
      case BITARRAY:
        resultBuilder.append("bit[");
        resultBuilder.append(length);
        resultBuilder.append("] ");
        resultBuilder.append(this.getName());
        resultBuilder.append(";");
        break;
    }
    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      IVariable other = (IVariable)object;
      return Objects.equals(this.name, other.name);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
