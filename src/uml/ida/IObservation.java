package uml.ida;

import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UBehavioural;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import util.Formatter;

import static util.Objects.requireNonNull;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class IObservation {
  enum Kind {
    SEND,
    RECEIVE,
    TERMINATION,
    SENDER,
    BEHAVIOURAL,
    ARGUMENTS,
    RECEIVER,
    EXPRESSION,
    NOT,
    AND,
    OR;

    int getPrecedence() {
      switch (this) {
        case SEND:
        case RECEIVE:
        case TERMINATION:
        case SENDER:
        case BEHAVIOURAL:
        case ARGUMENTS:
        case RECEIVER:
        case EXPRESSION:
          return 20;

        case NOT:
          return 15;

        case AND:
        case OR:
          return 10;

        default:
            return 0;
      }
    }

    boolean isAssociative() {
      switch (this) {
        case AND:
        case OR:
          return true;
        //$CASES-OMITTED$
        default:
          return false;
      }
    }

    boolean hasPrecedence(Kind other) {
      if ((this == NOT || this == AND || this == OR) &&
          (other == NOT || other == AND || other == OR))
        return ((this.getPrecedence() > other.getPrecedence()) ||
                (this == other && this.isAssociative()));
      else
        return true;
    }

    public String toString() {
      switch (this) {
        case SEND:
          return "snd";
        case RECEIVE:
          return "rcv";
        case TERMINATION:
          return "term";
        case SENDER:
          return "sender = ";
        case BEHAVIOURAL:
          return "behavioural = ";
        case ARGUMENTS:
          return "args = ";
        case RECEIVER:
          return "receiver = ";
        case EXPRESSION:
          return "";
        case NOT:
          return "!";
        case AND:
          return " && ";
        case OR:
          return " || ";
        default:
          return "[unknown]";
      }
    }
  }

  public boolean hasPrecedence(IObservation other) {
    return this.kind.hasPrecedence(other.kind);
  }

  private Kind kind;
  private @Nullable UObject object;
  private @Nullable UBehavioural behavioural;
  private @Nullable List<UExpression> arguments;
  private @Nullable IExpression expression;
  private @Nullable IObservation left, right;

  public interface Visitor<T> {
    public T onSend();
    public T onReceive();
    public T onTermination();
    public T onSender(UObject object);
    public T onReceiver(UObject object);
    public T onBehavioural(UBehavioural behavioural);
    public T onArguments(List<UExpression> arguments);
    public T onExpression(IExpression expression);
    public T onNot(IObservation specification);
    public T onAnd(IObservation leftSpecification, IObservation rightSpecification);
    public T onOr(IObservation leftSpecification, IObservation rightSpecification);
  }

  public interface InContextVisitor<T> extends Visitor<T> {
    public UContext getContext();
  }

  private IObservation(Kind kind) {
    this.kind = kind;
  }

  public static IObservation send() {
    return new IObservation(Kind.SEND);
  }

  public static IObservation receive() {
    return new IObservation(Kind.RECEIVE);
  }

  public static IObservation termination() {
    return new IObservation(Kind.TERMINATION);
  }

  public static IObservation sender(UObject object) {
    IObservation o = new IObservation(Kind.SENDER);
    o.object = object;
    return o;
  }

  public static IObservation receiver(UObject object) {
    IObservation o = new IObservation(Kind.RECEIVER);
    o.object = object;
    return o;
  }

  public static IObservation active(UObject object) {
    return IObservation.or(IObservation.and(IObservation.termination(), IObservation.sender(object)),
           IObservation.or(IObservation.and(IObservation.receive(), IObservation.receiver(object)),
                                 IObservation.and(IObservation.send(), IObservation.sender(object))));
  }

  public static IObservation behavioural(UBehavioural behavioural) {
    IObservation o = new IObservation(Kind.BEHAVIOURAL);
    o.behavioural = behavioural;
    return o;
  }

  public static IObservation arguments(List<uml.UExpression> arguments) {
    IObservation o = new IObservation(Kind.ARGUMENTS);
    o.arguments = arguments;
    return o;
  }

  public static IObservation expression(IExpression expression) {
    IObservation o = new IObservation(Kind.EXPRESSION);
    o.expression = expression;
    return o;
  }

  public static IObservation not(IObservation occurrence) {
    IObservation o = new IObservation(Kind.NOT);
    o.left = occurrence;
    return o;
  }

  public static IObservation and(IObservation left, IObservation right) {
    IObservation o = new IObservation(Kind.AND);
    o.left = left;
    o.right = right;
    return o;
  }

  public static IObservation or(IObservation left, IObservation right) {
    IObservation o = new IObservation(Kind.OR);
    o.left = left;
    o.right = right;
    return o;
  }

  public <T> T accept(Visitor<T> visitor) {
    switch (this.kind) {
      case SEND:
        return visitor.onSend();
      case RECEIVE:
        return visitor.onReceive();
      case TERMINATION:
        return visitor.onTermination();
      case SENDER:
        return visitor.onSender(requireNonNull(this.object));
      case BEHAVIOURAL:
        return visitor.onBehavioural(requireNonNull(this.behavioural));
      case ARGUMENTS:
        return visitor.onArguments(requireNonNull(this.arguments));
      case RECEIVER:
        return visitor.onReceiver(requireNonNull(this.object));
      case EXPRESSION:
        return visitor.onExpression(requireNonNull(this.expression));
      case NOT:
        return visitor.onNot(requireNonNull(this.left));
      case AND:
        return visitor.onAnd(requireNonNull(this.left), requireNonNull(this.right));
      case OR:
        return visitor.onOr(requireNonNull(this.left), requireNonNull(this.right));
      default:
        throw new IllegalStateException("Unknown observation kind.");
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    if (this.kind == Kind.SEND || this.kind == Kind.RECEIVE || this.kind == Kind.TERMINATION) {
      resultBuilder.append(this.kind);
      return resultBuilder.toString();
    }

    if (this.kind == Kind.SENDER || this.kind == Kind.RECEIVER) {
      resultBuilder.append(this.kind);
      resultBuilder.append(requireNonNull(this.object).getName());
      return resultBuilder.toString();
    }

    if (this.kind == Kind.BEHAVIOURAL) {
      resultBuilder.append(this.kind);
      resultBuilder.append(requireNonNull(this.behavioural).getName());
      return resultBuilder.toString();
    }

    if (this.kind == Kind.ARGUMENTS) {
      resultBuilder.append(this.kind);
      resultBuilder.append(Formatter.list(this.arguments));
      return resultBuilder.toString();
    }

    if (this.kind == Kind.EXPRESSION) {
      resultBuilder.append(this.expression);
      return resultBuilder.toString();
    }

    if (this.kind == Kind.NOT) {
      resultBuilder.append(this.kind);
      resultBuilder.append(Formatter.parenthesised(requireNonNull(this.left), e -> e.hasPrecedence(this)));
      return resultBuilder.toString();
    }

    if (this.kind == Kind.AND || this.kind == Kind.OR) {
      resultBuilder.append(Formatter.parenthesised(requireNonNull(this.left), e -> e.hasPrecedence(this)));
      resultBuilder.append(this.kind);
      resultBuilder.append(Formatter.parenthesised(requireNonNull(this.right), e -> e.hasPrecedence(this)));
      return resultBuilder.toString();
    }

    return resultBuilder.toString();
  }
}
