package uml.ida;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.interaction.ULifeline;
import uml.interaction.UMessage;
import uml.interaction.UTiming;

import util.Pair;


/**
 * Ida automaton.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 * @since 2005-09-04
 */
@NonNullByDefault
public class IAutomaton {
  private static int variableCount = 0;
  private static int clockCount = 0;

  private Set<ULifeline> lifelines = new HashSet<>();
  private Set<UMessage> messages = new HashSet<>();
  private Set<IVariable> variables = new HashSet<>();
  private Set<IClock> clocks = new HashSet<>();
  private Set<IState> states = new HashSet<>();
  private @Nullable IState initialState = null;
  private Set<IState> acceptingStates = new HashSet<>();
  private Set<IState> recurrenceStates = new HashSet<>();
  private Set<ITransition> transitions = new HashSet<>();
  private Set<UTiming> timings = new HashSet<>();

  public IAutomaton() {
  }

  public static IAutomaton trivial() {
    IAutomaton trivial = new IAutomaton();
    IState initialState = trivial.addState();
    trivial.setInitialState(initialState);
    trivial.addAcceptingState(initialState);
    return trivial;
  }

  /**
   * Add another Ida automaton to this automaton.
   *
   * All information from the other automaton is copied into this
   * automaton, with the exception of the initial state which is only
   * changed to the initial state of the other automaton if this
   * automaton shows no initial state.
   *
   * @param other another Ida automaton
   * @return this Ida automaton with the other automaton added
   */
  public IAutomaton addAutomaton(IAutomaton other) {
    this.lifelines.addAll(other.lifelines);
    this.messages.addAll(other.messages);
    this.variables.addAll(other.variables);
    this.clocks.addAll(other.clocks);
    this.states.addAll(other.states);
    this.acceptingStates.addAll(other.acceptingStates);
    this.recurrenceStates.addAll(other.recurrenceStates);
    this.transitions.addAll(other.transitions);
    this.timings.addAll(other.timings);
    if (this.initialState == null)
      this.initialState = other.initialState;
    return this;
  }
    
  /**
   * @param initialState the initial state to set
   */
  public void setInitialState(IState initialState) {
    this.initialState = initialState;
    this.states.add(initialState);
  }

  /**
   * @return automaton's initial state
   */
  public @Nullable IState getInitialState() {
    return this.initialState;
  }

  /**
   * Checks whether a state is an initial state of this automaton.
   *
   * @param state a state
   * @return <code>true</code> if the state is an initial state, <code>false</code> otherwise
   */
  public boolean isInitialState(IState state) {
    IState initialState = this.getInitialState();
    if (initialState == null)
      return false;
    return initialState.equals(state);
  }
  
  /**
   * Add an accepting state to this automaton.  If the state is not
   * already a state of this automaton, it will be added to this automaton.
   *
   * @param acceptingState the accepting state to add
   */
  public void addAcceptingState(IState acceptingState) {
    this.acceptingStates.add(acceptingState);
    this.states.add(acceptingState);
  }

  /**
   * Remove a state from the accepting states of this automaton.  The
   * state will remain a state of this automaton, if it has been before.
   *
   * @param state a state of this automaton
   */
  public void removeAcceptingState(IState state) {
    this.acceptingStates.remove(state);
  }

  /**
   * @return automaton's accepting states
   */
  public Set<IState> getAcceptingStates() {
    return this.acceptingStates;
  }

  /**
   * Check whether a state is an accepting state of this automaton.
   *
   * @param state a state
   * @return <code>true</code> if the state is an accepting state, <code>false</code> otherwise.
   */
  public boolean isAcceptingState(IState state) {
    return this.acceptingStates.contains(state);
  }

  /**
   * Add a recurrence state to this automaton.  If the state is not
   * already a state of this automaton, it will be added to this automaton.
   *
   * @param recurrenceState the recurrence state to add
   */
  public void addRecurrenceState(IState recurrenceState) {
    this.recurrenceStates.add(recurrenceState);
    this.states.add(recurrenceState);
  }

  /**
   * Remove a state from the recurrence states of this automaton.  The
   * state will remain a state of this automaton, if it has been before.
   *
   * @param state a state of this automaton
   */
  public void removeRecurrenceState(IState state) {
    this.recurrenceStates.remove(state);
  }

  /**
   * @return automaton's recurrence states
   */
  public Set<IState> getRecurrenceStates() {
    return this.recurrenceStates;
  }

  /**
   * Check whether a state is an accepting state of this automaton.
   *
   * @param state a state
   * @return <code>true</code> if the state is an accepting state, <code>false</code> otherwise.
   */
  public boolean isRecurrenceState(IState state) {
    return this.recurrenceStates.contains(state);
  }

  /**
   * Add a state to this automaton.
   *
   * @return state added
   */
  public IState addState() {
    IState state = new IState();
    this.states.add(state);
    return state;
  }

  /**
   * Add a state to this automaton.
   *
   * @param prefix a naming prefix
   * @return state added
   */
  public IState addState(String prefix) {
    IState state = new IState(prefix);
    this.states.add(state);
    return state;
  }

  /**
   * @return automaton's state (including initial and end state)
   */
  public Set<IState> getStates() {
    return this.states;
  }

  /**
   * Determine whether this automaton shows a given state.
   *
   * @param state a state
   * @return whether <b>state</b> is contained in this automaton
   */
  public boolean hasState(IState state) {
    return this.getStates().contains(state);
  }

  /**
   * Add a transition to this automaton.
   *
   * @param from transition's source state
   * @param guard transition's guard
   * @param effect transitions's effect
   * @param to transition's target state
   * @return transition added
   */
  public ITransition addTransition(IState from, @Nullable ICondition guard, @Nullable IAction effect, IState to) {
    ITransition transition = new ITransition(from, null, guard, effect, to);
    this.transitions.add(transition);
    return transition;
  }

  /**
   * Add a transition to this automaton.
   *
   * @param from transition's source state
   * @param trigger transition's trigger
   * @param guard transition's guard
   * @param effect transitions's effect
   * @param to transition's target state
   * @return transition added
   */
  public ITransition addTransition(IState from, @Nullable IEvent trigger, @Nullable ICondition guard, @Nullable IAction effect, IState to) {
    ITransition transition = new ITransition(from, trigger, guard, effect, to);
    this.transitions.add(transition);
    return transition;
  }

  /**
   * Add a transition to this automaton.
   *
   * @param from transition's source state
   * @param label transition's label
   * @param to transition's target state
   * @return transition added
   */
  public ITransition addTransition(IState from, ITransition.Label label, List<IBranch> iBranches) {
    ITransition transition = new ITransition(from, label, iBranches);
    this.transitions.add(transition);
    return transition;
  }

  /**
   * @return automaton's transitions
   */
  public Set<ITransition> getTransitions() {
    return this.transitions;
  }

  /**
   * Determine the set of transitions outgoing from a state.
   * 
   * @param state a state
   * @return the set of transitions outgoing from {@code state} (which is
   *         empty if the state is not contained in the automaton)
   */
  public Set<ITransition> getOutgoings(IState state) {
    Set<ITransition> outgoings = new HashSet<>();
    for (ITransition t : this.getTransitions()) {
      if (t.getFrom().equals(state)) {
        outgoings.add(t);
      }
    }
    return outgoings;
  }

  /**
   * Add a collection of timings.
   * 
   * @param timings a collection of timings
   * @return the resulting set of timings
   */
  public Set<UTiming> addTimings(Collection<UTiming> timings) {
    this.timings.addAll(timings);
    return this.timings;
  }

  /**
   * @return automaton's timings
   */
  public Set<UTiming> getTimings() {
    return this.timings;
  }

  /**
   * Add a collection of lifelines.
   * 
   * @param lifelines a set of lifelines
   * @return the resulting set of lifelines
   */
  public Set<ULifeline> addLifelines(Collection<ULifeline> lifelines) {
    this.lifelines.addAll(lifelines);
    return this.lifelines;
  }

  /**
   * @return automaton's lifelines
   */
  public Set<ULifeline> getLifelines() {
    return this.lifelines;
  }

  /**
   * Add a collection of messages.
   * 
   * @param messages a set of messages
   * @return the resulting set of messages
   */
  public Set<UMessage> addMessages(Collection<UMessage> messages) {
    this.messages.addAll(messages);
    return this.messages;
  }

  /**
   * @return automaton's messages
   */
  public Set<UMessage> getMessages() {
    return this.messages;
  }

  /**
   * Add an integer variable to this automaton.
   *
   * @return variable added
   */
  public IVariable addIntegerVariable() {
    IVariable variable = IVariable.integer("v_" + ++variableCount);
    this.variables.add(variable);
    return variable;
  }

  /**
   * Add a bit array variable to this automaton.
   *
   * @return variable added
   */
  public IVariable addBitArrayVariable(int length) {
    IVariable variable = IVariable.bitArray("v_" + ++variableCount, length);
    this.variables.add(variable);
    return variable;
  }

  /**
   * Add a collection of variables to this automaton.
   *
   * @param variabes a collection of variables
   */
  public void addVariables(Collection<IVariable> variables) {
    this.variables.addAll(variables);
  }

  /**
   * @return automaton's variables
   */
  public Set<IVariable> getVariables() {
    return this.variables;
  }

  /**
   * Add a clock to this automaton.
   *
   * @return clock added
   */
  public IClock addClock() {
    IClock clock = new IClock("c_" + ++clockCount);
    this.clocks.add(clock);
    return clock;
  }

  /**
   * Add a collection of clocks to this automaton.
   *
   * @param clocks a collection of clocks
   */
  public void addClocks(Collection<IClock> clocks) {
    this.clocks.addAll(clocks);
  }

  /**
   * @return automaton's clocks
   */
  public Set<IClock> getClocks() {
    return this.clocks;
  }

  /**
   * Determine if an accepting state is reachable from the initial state of this
   * automaton.
   * 
   * @return whether an accepting state is reachable from the initial state of
   *         this automaton
   */
  public boolean isAccepting() {
    IState initialState = this.getInitialState();
    if (initialState == null || this.getAcceptingStates().isEmpty())
      return false;
    return this.isAccepting(initialState, new HashSet<>());
  }

  /**
   * Determine if an accepting state is reachable from a state in this automaton
   * avoiding states already visited.
   *
   * @param iState path's starting state
   * @param visited a set of already visited states
   * @return whether an accepting state is reachable from <b>state</b> avoiding
   * the states in <b>visited</b>
   */
  private boolean isAccepting(IState iState, Set<IState> visited) {
    visited.add(iState);
    if (this.isAcceptingState(iState))
      return true;
    for (ITransition iTransition : this.getOutgoings(iState)) {
      for (IBranch iBranch : iTransition.getBranches()) {
        if (visited.contains(iBranch.getTo()))
          continue;
        if (this.isAccepting(iBranch.getTo(), visited))
          return true;
      }
    }
    return false;
  }

  /**
   * Determine if there is a cycle with a recurrence state reachable from the
   * initial state of this automaton.
   * 
   * @return whether there is a cycle with a recurrence state reachable from the
   *         initial state of this automaton
   */
  public boolean isRecurring() {
    IState initialState = this.getInitialState();
    if (initialState == null || this.getRecurrenceStates().isEmpty())
      return false;
    return this.isRecurring(initialState, new Stack<>(), null, new HashSet<>());
  }

  /**
   * Determine if there is a cycle with a recurrence state reachable from a
   * state in this automaton avoiding states already visited.
   * 
   * This method implements the CVWY-algorithm (see Courcoubetis, Vardi, Wolper,
   * Yannakakis 1990).  In fact, the stack is not needed...
   * 
   * @param iState path's starting state
   * @param stack state stack (leading to {@code iState})
   * @param seed cycle seed (or {@code null}, if in the first depth-first-search)
   * @param visited a set of already visited states with a flag for cycles
   * @return whether there is a cycle with a recurrence state reachable from
   *         {@code state} avoiding the states in {@code visited}
   */
  private boolean isRecurring(IState iState, Stack<IState> stack, @Nullable IState seed, Set<Pair<IState, Boolean>> visited) {
    stack.push(iState);
    for (ITransition iTransition : this.getOutgoings(iState)) {
      for (IBranch iBranch : iTransition.getBranches()) {
        if (seed != null && iBranch.getTo().equals(seed))
          return true;
        if (!visited.contains(new Pair<>(iBranch.getTo(), seed != null))) {
          visited.add(new Pair<>(iState, seed != null));
          if (isRecurring(iBranch.getTo(), stack, seed, visited))
            return true;
          if (seed == null && this.isRecurrenceState(iState)) {
            if (this.isRecurring(iState, stack, iState, visited))
              return true;
          }
        }
      }
    }
    stack.pop();
    return false;
  }

  public Set<IState> getReachableStates() {
    Set<IState> visited = new HashSet<>();
    IState initialState = this.getInitialState();
    if (initialState == null)
      return visited;
    return this.getReachableStates(initialState, visited);
  }

  private Set<IState> getReachableStates(IState from, Set<IState> visited) {
    if (visited.contains(from))
      return visited;
    visited.add(from);
    for (ITransition transition : this.getOutgoings(from)) {
      for (IBranch branch : transition.getBranches())
        visited = this.getReachableStates(branch.getTo(), visited);
    }
    return visited;
  }

  /**
   * Restrict this automaton to its reachable states.
   *
   * @return restricted automaton
   */
  public IAutomaton reachable() {
    this.states = this.getReachableStates();
    this.transitions = this.transitions.stream().filter(t -> this.states.contains(t.getFrom())).collect(Collectors.toSet());
    // No need to change the initial state: Either it is {@code null} and
    // hence no state is reachable or it is reachable
    this.acceptingStates = this.acceptingStates.stream().filter(s -> this.states.contains(s)).collect(Collectors.toSet());
    this.recurrenceStates = this.recurrenceStates.stream().filter(s -> this.states.contains(s)).collect(Collectors.toSet());
    return this;
  }

  /**
   * Declaration of this Ida automaton in Ida format.
   *
   * @return declaration of this Ida automaton
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    String postfix = "";

    resultBuilder.append("automaton {\n");

    if (!this.lifelines.isEmpty()) {
      resultBuilder.append(postfix);
      postfix = "";
      resultBuilder.append("  lifelines {\n");
      for (ULifeline lifeline : this.lifelines) {
        resultBuilder.append(postfix);
        resultBuilder.append("    ");
        resultBuilder.append(lifeline.getName());
        postfix = ",\n";
      }
      resultBuilder.append(";\n  }\n");
      postfix = "\n";
    }

    if (!this.messages.isEmpty()) {
      resultBuilder.append(postfix);
      postfix = "";
      resultBuilder.append("  messages {\n");
      for (UMessage message : this.messages) {
        resultBuilder.append(postfix);
        resultBuilder.append("    ");
        resultBuilder.append(message.toString());
        postfix = "\n";
      }
      resultBuilder.append("\n  }\n");
      postfix = "\n";
    }

    if (!this.variables.isEmpty()) {
      resultBuilder.append(postfix);
      postfix = "";
      resultBuilder.append("  variables {\n");
      for (IVariable variable : this.variables) {
        resultBuilder.append(postfix);
        resultBuilder.append("    ");
        resultBuilder.append(variable.declaration());
        postfix = "\n";
      }
      resultBuilder.append("\n  }\n");
      postfix = "\n";
    }

    if (!this.clocks.isEmpty()) {
      resultBuilder.append(postfix);
      postfix = "";
      resultBuilder.append("  clocks {\n");
      for (IClock clock : this.clocks) {
        resultBuilder.append(postfix);
        resultBuilder.append("    ");
        resultBuilder.append(clock.getName());
        postfix = ",\n";
      }
      resultBuilder.append(";\n  }\n");
      postfix = "\n";
    }

    if (!this.states.isEmpty()) {
      resultBuilder.append(postfix);
      postfix = "";
      resultBuilder.append("  states {\n");
      postfix = "";
      for (IState state : this.states) {
        resultBuilder.append(postfix);
        resultBuilder.append("    ");
        resultBuilder.append(state.getName());
        postfix = ",\n";
      }
      resultBuilder.append(";\n  }\n");
      postfix = "\n";
    }

    IState initialState = this.initialState;
    if (initialState != null) {
      resultBuilder.append(postfix);
      postfix = "";
      resultBuilder.append("  initial {\n");
      resultBuilder.append("    ");
      resultBuilder.append(initialState.getName());
      resultBuilder.append(";\n  }\n");
      postfix = "\n";
    }

    if (!this.acceptingStates.isEmpty()) {
      resultBuilder.append(postfix);
      postfix = "";
      resultBuilder.append("  accepting {\n");
      postfix = "";
      for (IState state : this.acceptingStates) {
        resultBuilder.append(postfix);
        resultBuilder.append("    ");
        resultBuilder.append(state.toString());
        postfix = ",\n";
      }
      resultBuilder.append(";\n  }\n");
      postfix = "\n";
    }

    if (!this.recurrenceStates.isEmpty()) {
      resultBuilder.append(postfix);
      postfix = "";
      resultBuilder.append("  recurrent {\n");
      postfix = "";
      for (IState state : this.recurrenceStates) {
        resultBuilder.append(postfix);
        resultBuilder.append("    ");
        resultBuilder.append(state.toString());
        postfix = ",\n";
      }
      resultBuilder.append(";\n  }\n");
      postfix = "\n";
    }

    if (!this.transitions.isEmpty()) {
      resultBuilder.append(postfix);
      postfix = "";
      resultBuilder.append("  transitions {\n");
      postfix = "";
      for (ITransition transition : this.transitions) {
        resultBuilder.append(postfix);
        resultBuilder.append("    ");
        resultBuilder.append(transition.declaration("    "));
        postfix = "\n\n";
      }
      resultBuilder.append("\n  }\n");
      postfix = "";
    }

    resultBuilder.append(postfix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Automaton [initial state = ");
    resultBuilder.append(this.initialState);
    resultBuilder.append(", accepting states = ");
    resultBuilder.append(this.acceptingStates);
    resultBuilder.append(", recurrence states = ");
    resultBuilder.append(this.recurrenceStates);
    resultBuilder.append(", states = ");
    resultBuilder.append(this.states);
    resultBuilder.append(", transitions = ");
    resultBuilder.append(this.transitions);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
