package uml.ida;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static util.Objects.requireNonNull;

import java.util.Objects;


/**
 * Ida action
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 * @since 05/09/04
 */
@NonNullByDefault
public class IAction {
  enum Kind {
    SKIP,
    ASSIGN,
    RESET,
    INC,
    DEC,
    RESETARRAY,
    SETARRAY,
    RESETCLOCK,
    SEQ;
  }

  private Kind kind;
  private @Nullable IVariable variable;
  private int offset;
  private @Nullable IClock clock;
  private @Nullable IExpression expression;
  private @Nullable IAction leftAction;
  private @Nullable IAction rightAction;

  public static interface Visitor<T> {
    public T onSkip();
    public T onAssign(IVariable variable, IExpression expression);
    public T onReset(IVariable variable);
    public T onInc(IVariable variable);
    public T onDec(IVariable variable);
    public T onSet(IVariable variable, int offset);
    public T onReset(IVariable variable, int offset);
    public T onReset(IClock clock);
    public T onSeq(IAction leftAction, IAction rightAction);
  }

  private IAction(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create an action which represents <CODE>skip</CODE>.
   */
  public static IAction skip() {
    IAction a = new IAction(Kind.SKIP);
    return a;
  }

  /**
   * Create an action representing assigning an expression to a variable.
   *
   * @param variable a variable
   * @return an action representing reset of <b>variable</b>
   */
  public static IAction assign(IVariable variable, IExpression expression) {
    IAction a = new IAction(Kind.ASSIGN);
    a.variable = variable;
    a.expression = expression;
    return a;
  }

  /**
   * Create an action representing resetting of a variable.
   *
   * @param variable a variable
   * @return an action representing reset of <b>variable</b>
   */
  public static IAction reset(IVariable variable) {
    IAction a = new IAction(Kind.RESET);
    a.variable = variable;
    return a;
  }

  /**
   * Create an action representing incrementation of a variable.
   */
  public static IAction inc(IVariable variable) {
    IAction a = new IAction(Kind.INC);
    a.variable = variable;
    return a;
  }

  /**
   * Create an action representing decrementation of a variable.
   */
  public static IAction dec(IVariable variable) {
    IAction a = new IAction(Kind.DEC);
    a.variable = variable;
    return a;
  }

  /**
   * Create an action representing resetting a bit array variable.
   *
   * @param variable a variable
   * @return an action representing reset of <b>variable</b>
   */
  public static IAction reset(IVariable variable, int offset) {
    IAction a = new IAction(Kind.RESETARRAY);
    a.variable = variable;
    a.offset = offset;
    return a;
  }

  /**
   * Create an action representing setting a bit array variable.
   *
   * @param variable a variable
   * @return an action representing reset of <b>variable</b>
   */
  public static IAction set(IVariable variable, int offset) {
    IAction a = new IAction(Kind.SETARRAY);
    a.variable = variable;
    a.offset = offset;
    return a;
  }

  /**
   * Create an action representing resetting a clock.
   *
   * @param variable a variable
   * @return an action representing reset of <b>variable</b>
   */
  public static IAction reset(IClock clock) {
    IAction a = new IAction(Kind.RESETCLOCK);
    a.clock = clock;
    return a;
  }

  /**
   * Create an action representing sequential composition of actions.
   */
  public static IAction seq(@Nullable IAction leftAction, @Nullable IAction rightAction) {
    if (leftAction == null || leftAction.kind == Kind.SKIP) {
      if (rightAction == null)
        return IAction.skip();
      return rightAction;
    }
    if (rightAction == null || rightAction.kind == Kind.SKIP)
      return leftAction;

    IAction a = new IAction(Kind.SEQ);
    a.leftAction = leftAction;
    a.rightAction = rightAction;
    return a;
  }

  /**
   * Check whether this action is syntactically equivalent to skip.
   *
   * @return whether this action is syntactically equivalent to skip
   */
  public boolean isSkip() {
    if (this.kind == Kind.SKIP)
      return true;
    if (this.kind == Kind.SEQ)
      return (requireNonNull(this.leftAction).isSkip() && requireNonNull(this.rightAction).isSkip());
    return false;
  }

  /**
   * Accept an action visitor.
   *
   * @param visitor an action visitor
   * @return result of visit
   */
  public <T> T accept(Visitor<T> visitor) {
    switch (this.kind) {
      case SKIP:
        return visitor.onSkip();

      case ASSIGN:
        return visitor.onAssign(requireNonNull(this.variable), requireNonNull(this.expression));

      case RESET:
        return visitor.onReset(requireNonNull(this.variable));

      case INC:
        return visitor.onInc(requireNonNull(this.variable));

      case DEC:
        return visitor.onDec(requireNonNull(this.variable));

      case SETARRAY:
        return visitor.onSet(requireNonNull(this.variable), this.offset);

      case RESETARRAY:
        return visitor.onReset(requireNonNull(this.variable), this.offset);

      case RESETCLOCK:
        return visitor.onReset(requireNonNull(this.clock));

      case SEQ:
        return visitor.onSeq(requireNonNull(this.leftAction), requireNonNull(this.rightAction));
    }

    throw new IllegalStateException("Unknown Ida action");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.variable, this.clock, this.leftAction, this.rightAction);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      IAction other = (IAction)object;
      if (this.kind != other.kind)
        return false;
      switch (this.kind) {
        case SKIP:
          return true;
        case ASSIGN:
          return Objects.equals(this.variable, other.variable) && Objects.equals(this.expression, other.expression);
        case RESET:
        case INC:
        case DEC:
          return Objects.equals(this.variable, other.variable);
        case SETARRAY:
        case RESETARRAY:
          return Objects.equals(this.variable, other.variable) && this.offset == other.offset;
        case RESETCLOCK:
          return Objects.equals(this.clock, other.clock);
        case SEQ:
          return Objects.equals(this.leftAction, other.leftAction) && Objects.equals(this.rightAction, other.rightAction); 
      }
      return false;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case SKIP:
        resultBuilder.append(";");
        break;

      case ASSIGN :
        resultBuilder.append(requireNonNull(this.variable).getName());
        resultBuilder.append(" = ");
        resultBuilder.append(requireNonNull(this.expression).toString());
        resultBuilder.append(";");
        break;

      case RESET:
        resultBuilder.append(requireNonNull(this.variable).getName());
        resultBuilder.append(" = 0;");
        break;

      case INC:
        resultBuilder.append(requireNonNull(this.variable).getName());
        resultBuilder.append("++;");
        break;

      case DEC:
        resultBuilder.append(requireNonNull(this.variable).getName());
        resultBuilder.append("--;");
        break;

      case SETARRAY :
        resultBuilder.append(requireNonNull(this.variable).getName());
        resultBuilder.append("[");
        resultBuilder.append(this.offset);
        resultBuilder.append("]");
        resultBuilder.append(" = 1;");
        break;

      case RESETARRAY :
        resultBuilder.append(requireNonNull(this.variable).getName());
        resultBuilder.append("[");
        resultBuilder.append(this.offset);
        resultBuilder.append("]");
        resultBuilder.append(" = 0;");
        break;

      case RESETCLOCK :
        resultBuilder.append(requireNonNull(this.clock).getName());
        resultBuilder.append(" = 0;");
        break;

      case SEQ :
        resultBuilder.append(requireNonNull(this.leftAction).toString());
        resultBuilder.append(" ");
        resultBuilder.append(requireNonNull(this.rightAction).toString());
        break;
    }

    return resultBuilder.toString();
  }
}
