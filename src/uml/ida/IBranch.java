package uml.ida;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Ida transition branch.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @since 2016-04-16
 */
@NonNullByDefault
public class IBranch {
  public static class Label {
    private @Nullable Double rate;
    private @Nullable IAction effect;

    public Label(@Nullable Double rate, @Nullable IAction effect) {
      this.rate = rate;
      this.effect = effect;
    }

    Label(Label other) {
      this.rate = other.rate;
      this.effect = other.effect;
    }

    public @Nullable Double getRate() {
      return this.rate;
    }

    public @Nullable IAction getEffect() {
      return this.effect;
    }

    /**
     * Add an effect sequentially to this label.
     *
     * @param effect an effect action
     * @return label
     */
    public Label addEffect(@Nullable IAction effect) {
      this.effect = IAction.seq(this.effect, effect);
      return this;
    }

    public String declaration() {
      return this.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.rate, this.effect);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Label other = (Label)object;
        return Objects.equals(this.rate, other.rate) &&
               Objects.equals(this.effect, other.effect);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      if (this.rate != null) {
        resultBuilder.append(rate);
        resultBuilder.append(" ");
      }
      resultBuilder.append("/ ");
      if (this.effect == null)
        resultBuilder.append(";");
      else
        resultBuilder.append(this.effect);
      return resultBuilder.toString();
    }
  }

  private Label label;
  private IState to;

  public IBranch(@Nullable IAction effect, IState to) {
    this.label = new Label(null, effect);
    this.to = to;
  }

  public IBranch(Label label, IState to) {
    this.label = new Label(label);
    this.to = to;
  }

  /**
   * Add an effect sequentially to this branch.
   * 
   * @param effect an effect action
   */
  public void addEffect(@Nullable IAction effect) {
    this.label.addEffect(effect);
  }

  /**
   * @return branch's label
   */
  public Label getLabel() {
    return this.label;
  }

  /**
   * @return branch's target state
   */
  public IState getTo() {
    return this.to;
  }

  /**
   * Declaration of this Ida transition in Ida format.
   *
   * @param prefix a prefix for each line
   * @return declaration of this Ida transition
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    
    resultBuilder.append(this.label.declaration());
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append(this.to.getName());

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.to);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      IBranch other = (IBranch)object;
      return Objects.equals(this.to, other.to) &&
             Objects.equals(this.label, other.label);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.label);
    resultBuilder.append(" -> ");
    resultBuilder.append(this.to.getName());
    return resultBuilder.toString();
  }
}
