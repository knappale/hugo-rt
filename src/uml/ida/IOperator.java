package uml.ida;

import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * Ida operators
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public enum IOperator {
  FALSE,
  TRUE,
  INFTY,
  PLUS,
  MINUS,
  NEG,
  AND,
  OR,
  EQ,
  NEQ,
  LT,
  LEQ,
  GEQ,
  GT;

  public String getOpName() {
    switch (this) {
      case FALSE: return "false";
      case TRUE: return "true";
      case INFTY: return "infty";
      case PLUS: return "+";
      case MINUS: return "-";
      case NEG: return "!";
      case AND: return " && ";
      case OR: return " || ";
      case EQ: return " == ";
      case NEQ: return " != ";
      case LT: return " < ";
      case LEQ: return " <= ";
      case GEQ: return " >= ";
      case GT: return " > ";
      default: throw new IllegalStateException("Unknown Ida operator");
    }
  }

  private int getOpPrecedence() {
    switch (this) {
      case FALSE:
      case TRUE:
      case INFTY: return 24;
      case NEG: return 22;
      case MINUS: return 19;
      case PLUS: return 18;
      case EQ:
      case NEQ: return 14;
      case LT:
      case LEQ:
      case GEQ:
      case GT: return 12;
      case AND: return 10;
      case OR: return 8;
      default: return 0;
    }
  }

  public static boolean precedes(IOperator op1, IOperator op2) {
    return op1.getOpPrecedence() >= op2.getOpPrecedence();
  }
}
