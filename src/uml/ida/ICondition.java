package uml.ida;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UObject;
import uml.interaction.ULifeline;


/**
 * Models a condition on a transition.
 * 
 * @author <A HREF="mailto:Jochen.Wuttke@gmx.de>Jochen Wuttke</A>
 * @since May 10, 2006
 *
 */
@NonNullByDefault
public class ICondition {
  private IExpression condition;
  private Set<UObject> coveredLifelines;

  public ICondition(IExpression condition, Set<ULifeline> uLifelines) {
    this.condition = condition;
    this.coveredLifelines = new HashSet<>(uLifelines.stream().map(uLifeline -> uLifeline.getObject()).collect(Collectors.toSet()));
  }

  /**
   * @return condition expression
   */
  public IExpression getCondition() {
    return this.condition;
  }

  /**
   * @return Returns the coveredLifelines.
   */
  public Set<UObject> getCoveredLifelines() {
    return this.coveredLifelines;
  }

  /**
   * Determine whether the active object of an event is covered by this condition.
   *
   * @param event an event
   * @return true if the active object of {@code event} is covered by this condition
   */
  public boolean covers(IEvent event) {
    return this.coveredLifelines.contains(event.getUObject());
  }

  /**
   * @param condition condition expression
   */
  public void setCondition(IExpression condition) {
    this.condition = condition;
  }

  @Override
  public String toString() {
    return this.condition.toString();
  }
}
