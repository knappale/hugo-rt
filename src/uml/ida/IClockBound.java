package uml.ida;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static util.Objects.requireNonNull;


/**
 * Timing condition
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @since June 4, 2013
 */
@NonNullByDefault
public class IClockBound {
  static enum TimingKind {
    SIMPLE,
    CONJUNCTION;
  }

  private TimingKind kind;
  private @Nullable IClock clock;
  private @Nullable IOperator bop;
  private @Nullable IExpression bound;
  private @Nullable IClockBound leftClockBound;
  private @Nullable IClockBound rightClockBound;

  public static interface Visitor<T> {
    public T onLt(IClock clock, IExpression bound);
    public T onLeq(IClock clock, IExpression bound);
    public T onGeq(IClock clock, IExpression bound);
    public T onGt(IClock clock, IExpression bound);
    public T onAnd(IClockBound leftClockBound, IClockBound rightClockBound);
  }

  private IClockBound(IClock clock, IOperator bop, IExpression bound) {
    this.kind = TimingKind.SIMPLE;
    this.clock = clock;
    this.bop = bop;
    this.bound = bound;
  }

  private IClockBound(IClockBound leftTiming, IClockBound rightTiming) {
    this.kind = TimingKind.CONJUNCTION;
    this.leftClockBound = leftTiming;
    this.rightClockBound = rightTiming;
  }

  public static IClockBound lt(IClock clock, IExpression bound) {
    return new IClockBound(clock, IOperator.LT, bound);
  }

  public static IClockBound leq(IClock clock, IExpression bound) {
    return new IClockBound(clock, IOperator.LEQ, bound);
  }

  public static IClockBound geq(IClock clock, IExpression bound) {
    return new IClockBound(clock, IOperator.GEQ, bound);
  }

  public static IClockBound gt(IClock clock, IExpression bound) {
    return new IClockBound(clock, IOperator.GT, bound);
  }

  public static IClockBound and(IClockBound leftTiming, IClockBound rightTiming) {
    return new IClockBound(leftTiming, rightTiming);
  }

  public <T> T accept(Visitor<T> visitor) {
    switch (this.kind) {
      case SIMPLE: {
        IOperator bop = requireNonNull(this.bop);
        IClock clock = requireNonNull(this.clock);
        IExpression bound = requireNonNull(this.bound);
        switch (bop) {
          case LT:
            return visitor.onLt(clock, bound);
          case LEQ:
            return visitor.onLeq(clock, bound);
          case GEQ:
            return visitor.onGeq(clock, bound);
          case GT:
            return visitor.onGt(clock, bound);
          //$CASES-OMITTED$
          default:
        }
        break;
      }

      case CONJUNCTION: {
        IClockBound leftClockBound = requireNonNull(this.leftClockBound);
        IClockBound rightClockBound = requireNonNull(this.rightClockBound);
        return visitor.onAnd(leftClockBound, rightClockBound);
      }
    }

    throw new IllegalStateException("Unknown Ida clock bound kind.");
  }

  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case SIMPLE:
        resultBuilder.append(requireNonNull(this.clock).getName());
        resultBuilder.append(requireNonNull(this.bop).getOpName());
        resultBuilder.append(this.bound);
        break;

      case CONJUNCTION:
        resultBuilder.append(this.leftClockBound);
        resultBuilder.append(IOperator.AND.getOpName());
        resultBuilder.append(this.rightClockBound);
    }

    return resultBuilder.toString();
  }
}
