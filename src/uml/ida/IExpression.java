package uml.ida;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UContext;
import uml.UExpression;
import util.Formatter;

import static util.Objects.requireNonNull;

import java.util.Objects;


/**
 * Ida expression
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 * @since 2005-10-01
 */
@NonNullByDefault
public class IExpression {
  enum Kind {
    BOOLEAN,
    INTEGER,
    VARIABLE,
    ARRAY,
    EXTERNAL,
    UNARY,
    BINARY;
  }

  private boolean hasPrecedence(IExpression other) {
    if ((this.kind == Kind.UNARY || this.kind == Kind.BINARY) && (other.kind == Kind.UNARY || other.kind == Kind.BINARY))
      return IOperator.precedes(requireNonNull(this.op), requireNonNull(other.op));
    else
      return true;
  }

  private static final IExpression TRUEEXPRESSION;
  private static final IExpression FALSEEXPRESSION;
  private static final IExpression INFTYEXPRESSION;
  static {
    TRUEEXPRESSION = new IExpression(Kind.BOOLEAN);
    TRUEEXPRESSION.op = IOperator.TRUE;
    FALSEEXPRESSION = new IExpression(Kind.BOOLEAN);
    FALSEEXPRESSION.op = IOperator.FALSE;
    INFTYEXPRESSION = new IExpression(Kind.INTEGER);
    INFTYEXPRESSION.op = IOperator.INFTY;
  }

  private Kind kind;
  private int intConst;
  private @Nullable IVariable variable = null;
  private @Nullable IClock clock = null;
  private @Nullable UExpression external = null;
  private @Nullable UContext context = null;
  private @Nullable IOperator op = null;
  private @Nullable IExpression leftExpression = null;
  private @Nullable IExpression rightExpression = null;

  public interface Visitor<T> {
    public T onBooleanConstant(boolean booleanConstant);
    public T onIntegerConstant(int integerConstant);
    public T onInfinity();
    public T onVariable(IVariable variable);
    public T onArray(IVariable variable, int offset);
    public T onExternal(UExpression external, UContext context);
    public T onArithmetical(IExpression leftExpression, IOperator op, IExpression rightExpression);
    public T onNegation(IExpression expression);
    public T onRelational(IExpression leftExpression, IOperator op, IExpression rightExpression);
    public T onJunctional(IExpression leftExpression, IOperator op, IExpression rightExpression);
  }

  private IExpression(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create an expression which represents a boolean constant.
   * 
   * @param booleanConstant a boolean
   * @return an expression
   */
  public static IExpression boolConst(boolean booleanConstant) {
    return (booleanConstant ? TRUEEXPRESSION : FALSEEXPRESSION);
  }

  /**
   * Create an expression which represents the boolean constant <CODE>true</CODE>.
   * 
   * @return an expression
   */
  public static IExpression trueConst() {
    return TRUEEXPRESSION;
  }

  /**
   * Create an expression which represents the boolean constant <CODE>false</CODE>.
   * 
   * @return an expression
   */
  public static IExpression falseConst() {
    return FALSEEXPRESSION;
  }
  
  /**
   * Create an expression which represents the constant <CODE>infinity</CODE>.
   * 
   * @return an expression
   */
  public static IExpression infty() {
    return INFTYEXPRESSION;
  }

  /**
   * Create an expression which represents an integer constant.
   * 
   * @param integerConstant integer
   * @return an expression
   */
  public static IExpression intConst(int integerConstant) {
    IExpression e = new IExpression(Kind.INTEGER);
    e.intConst = integerConstant;
    return e;
  }

  /**
   * Create an expression which represents a variable access.
   * 
   * @param variable variable
   * @return an expression
   */
  public static IExpression variable(IVariable variable) {
    IExpression e = new IExpression(Kind.VARIABLE);
    e.variable = variable;
    return e;
  }

  /**
   * Create an expression which represents an array variable access.
   * 
   * @param variable array variable
   * @param offset integer offset
   * @return an expression
   * @pre variable != null
   */
  public static IExpression array(IVariable variable, int offset) {
    IExpression e = new IExpression(Kind.ARRAY);
    e.variable = variable;
    e.intConst = offset;
    return e;
  }

  /**
   * Create an expression which represents an external (UML) expression.
   * 
   * @param external external (UML) expression
   * @param context (UML) context for external (UML) expression
   * @return an expression
   */
  public static IExpression external(uml.UExpression external, uml.UContext context) {
    IExpression e = new IExpression(Kind.EXTERNAL);
    e.external = external;
    e.context = context;
    return e;
  }

  /**
   * Create an expression which represents an arithmetical expression.
   * 
   * @param leftExpression left expression
   * @param op arithmetical operator (i.e. {@link #AND AND}, {@link #OR OR})
   * @param rightExpression right expression
   * @return an expression
   */
  public static IExpression arithmetical(IExpression leftExpression, IOperator op, IExpression rightExpression) {
    IExpression e = new IExpression(Kind.BINARY);
    e.op = op;
    e.leftExpression = leftExpression;
    e.rightExpression = rightExpression;
    return e;
  }

  /**
   * Create an expression which represents negation of an expression.
   * 
   * @param expression expression
   * @return an expression
   */
  public static IExpression neg(IExpression expression) {
    if (expression.equals(falseConst()))
      return trueConst();
    if (expression.equals(trueConst()))
      return falseConst();
    if (expression.kind == Kind.UNARY) {
      if (expression.op == IOperator.NEG)
        return requireNonNull(expression.leftExpression);
    }
    if (expression.kind == Kind.BINARY) {
      switch (requireNonNull(expression.op)) {
        case AND:
          return or(neg(requireNonNull(expression.leftExpression)), neg(requireNonNull(expression.rightExpression)));
        case OR:
          return and(neg(requireNonNull(expression.leftExpression)), neg(requireNonNull(expression.rightExpression)));
        case EQ:
          return relational(requireNonNull(expression.leftExpression), IOperator.NEQ, requireNonNull(expression.rightExpression));
        case NEQ:
          return relational(requireNonNull(expression.leftExpression), IOperator.EQ, requireNonNull(expression.rightExpression));
        case LT:
          return relational(requireNonNull(expression.leftExpression), IOperator.GEQ, requireNonNull(expression.rightExpression));
        case LEQ:
          return relational(requireNonNull(expression.leftExpression), IOperator.GT, requireNonNull(expression.rightExpression));
        case GEQ:
          return relational(requireNonNull(expression.leftExpression), IOperator.LT, requireNonNull(expression.rightExpression));
        case GT:
          return relational(requireNonNull(expression.leftExpression), IOperator.LEQ, requireNonNull(expression.rightExpression));
        //$CASES-OMITTED$
        default:
      }
    }
    IExpression e = new IExpression(Kind.UNARY);
    e.op = IOperator.NEG;
    e.leftExpression = expression;
    return e;
  }

  /**
   * Create an expression which represents a relational expression.
   * 
   * @param leftExpression left expression
   * @param op relational operator
   * @param rightExpression right expression
   * @return an expression
   */
  public static IExpression relational(IExpression leftExpression, IOperator op, IExpression rightExpression) {
    IExpression e = new IExpression(Kind.BINARY);
    e.op = op;
    e.leftExpression = leftExpression;
    e.rightExpression = rightExpression;
    return e;
  }

  /**
   * Create an expression which represents a conjunction.
   * 
   * @param leftExpression left expression
   * @param rightExpression right expression
   * @return an expression
   */
  public static IExpression and(IExpression leftExpression, IExpression rightExpression) {
    if (leftExpression.equals(trueConst()))
      return rightExpression;
    if (rightExpression.equals(trueConst()))
      return leftExpression;
    if (leftExpression.equals(falseConst()))
      return falseConst();
    if (rightExpression.equals(falseConst()))
      return falseConst();
    IExpression e = new IExpression(Kind.BINARY);
    e.op = IOperator.AND;
    e.leftExpression = leftExpression;
    e.rightExpression = rightExpression;
    return e;
  }

  /**
   * Create an expression which represents a disjunction.
   * 
   * @param leftExpression left expression
   * @param rightExpression right expression
   * @return an expression
   */
  public static IExpression or(IExpression leftExpression, IExpression rightExpression) {
    if (leftExpression.equals(falseConst()))
      return rightExpression;
    if (rightExpression.equals(falseConst()))
      return leftExpression;
    if (leftExpression.equals(trueConst()))
      return trueConst();
    if (rightExpression.equals(trueConst()))
      return trueConst();
    IExpression e = new IExpression(Kind.BINARY);
    e.op = IOperator.OR;
    e.leftExpression = leftExpression;
    e.rightExpression = rightExpression;
    return e;
  }

  /**
   * Accept a visitor.
   */
  public <T> T accept(Visitor<T> visitor) {
    switch (this.kind) {
      case BOOLEAN:
        return visitor.onBooleanConstant(this.op == IOperator.TRUE);

      case INTEGER:
        if (this.op == IOperator.INFTY)
          return visitor.onInfinity();
        return visitor.onIntegerConstant(this.intConst);

      case VARIABLE:
        return visitor.onVariable(requireNonNull(this.variable));

      case ARRAY:
        return visitor.onArray(requireNonNull(this.variable), this.intConst);

      case EXTERNAL:
        return visitor.onExternal(requireNonNull(this.external), requireNonNull(this.context));

      case UNARY:
        if (this.op == IOperator.NEG)
          return visitor.onNegation(requireNonNull(this.leftExpression));
        throw new IllegalStateException("Unknown Ida operator.");

      case BINARY:
        if (this.op == IOperator.PLUS || this.op == IOperator.MINUS)
          return visitor.onArithmetical(requireNonNull(this.leftExpression), requireNonNull(this.op), requireNonNull(this.rightExpression));

        if (this.op == IOperator.AND || this.op == IOperator.OR)
          return visitor.onJunctional(requireNonNull(this.leftExpression), requireNonNull(this.op), requireNonNull(this.rightExpression));

        return visitor.onRelational(requireNonNull(this.leftExpression), requireNonNull(this.op), requireNonNull(this.rightExpression));

      default:
        throw new IllegalStateException("Unknown Ida expression.");
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.kind,
                        this.intConst,
                        this.op,
                        this.variable,
                        this.clock,
                        this.external,
                        this.leftExpression,
                        this.rightExpression);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      IExpression other = (IExpression)object;
      return this.kind == other.kind &&
             this.intConst == other.intConst &&
             this.op == other.op &&
             Objects.equals(this.variable, other.variable) &&
             Objects.equals(this.clock, other.clock) &&
             Objects.equals(this.external, other.external) &&
             Objects.equals(this.leftExpression, other.leftExpression) &&
             Objects.equals(this.rightExpression, other.rightExpression);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public @NonNull String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
    case BOOLEAN:
      resultBuilder.append(this.op == IOperator.TRUE);
      break;

    case INTEGER:
      if (this.op == IOperator.INFTY) {
        resultBuilder.append("infty");
      }
      else {
        resultBuilder.append(this.intConst);
      }
      break;

    case VARIABLE:
      resultBuilder.append(requireNonNull(this.variable).getName());
      break;

    case ARRAY:
      resultBuilder.append(requireNonNull(this.variable).getName());
      resultBuilder.append("[");
      resultBuilder.append(this.intConst);
      resultBuilder.append("]");
      break;

    case EXTERNAL:
      resultBuilder.append("eval(");
      resultBuilder.append(requireNonNull(this.external).toString());
      resultBuilder.append(")");
      break;

    case UNARY:
      resultBuilder.append(requireNonNull(this.op).getOpName());
      resultBuilder.append(Formatter.parenthesised(requireNonNull(this.leftExpression), e -> e.hasPrecedence(this)));
      break;

    case BINARY:
      resultBuilder.append(Formatter.parenthesised(requireNonNull(this.leftExpression), e -> e.hasPrecedence(this)));
      resultBuilder.append(requireNonNull(this.op).getOpName());
      resultBuilder.append(Formatter.parenthesised(requireNonNull(this.rightExpression), e -> e.hasPrecedence(this)));
      break;
    }

    return resultBuilder.toString();
  }
}
