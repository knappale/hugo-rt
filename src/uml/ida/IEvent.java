package uml.ida;

import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UBehavioural;
import uml.UContext;
import uml.UExpression;
import uml.UObject;
import uml.interaction.UOccurrenceSpecification;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class IEvent {
  private UObject uObject;
  private IObservation observation;
  private @Nullable UOccurrenceSpecification uOccurrence;

  private IEvent(UObject uObject, IObservation observation) {
    this.uObject = uObject;
    this.observation = observation;
  }

  public static IEvent active(UObject uObject) {
    return new IEvent(uObject, IObservation.or(IObservation.and(IObservation.receive(), IObservation.receiver(uObject)),
                                               IObservation.and(IObservation.send(), IObservation.sender(uObject))));
  }
  
  public static IEvent behavioural(UObject uObject, UBehavioural uBehavioural) {
    return new IEvent(uObject, IObservation.and(IObservation.or(IObservation.and(IObservation.receive(), IObservation.receiver(uObject)),
                                                                IObservation.and(IObservation.send(), IObservation.sender(uObject))),
                                                IObservation.behavioural(uBehavioural)));

  }

  public static IEvent occurrence(UOccurrenceSpecification uOccurrence) {
    IEvent e = new IEvent(uOccurrence.getLifeline().getObject(), new OccurrenceTranslator(uOccurrence.getInteraction().getMessageContext()).getSpecification(uOccurrence));
    e.uOccurrence = uOccurrence;
    return e;
  }

  public static IEvent observation(UObject uObject, IObservation observation) {
    return new IEvent(uObject, IObservation.and(observation,
                                                IObservation.or(IObservation.and(IObservation.receive(), IObservation.receiver(uObject)),
                                                                IObservation.and(IObservation.send(), IObservation.sender(uObject)))));
  }

  private static class OccurrenceTranslator implements UOccurrenceSpecification.InContextVisitor<IObservation> {
    private UContext uContext;

    OccurrenceTranslator(UContext uContext) {
      this.uContext = uContext;
    }

    IObservation getSpecification(UOccurrenceSpecification uOccurrence) {
      return uOccurrence.accept(this);
    }

    public UContext getContext() {
      return this.uContext;
    }

    public IObservation onSend(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return IObservation.and(IObservation.send(),
             IObservation.and(IObservation.sender(sender),
             IObservation.and(IObservation.behavioural(behavioural),
             IObservation.and(IObservation.arguments(arguments),
                              IObservation.receiver(receiver)))));
    }

    public IObservation onReceive(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver) {
      return IObservation.and(IObservation.receive(),
             IObservation.and(IObservation.sender(sender),
             IObservation.and(IObservation.behavioural(behavioural),
             IObservation.and(IObservation.arguments(arguments),
                              IObservation.receiver(receiver)))));
    }

    public IObservation onTermination(UObject object) {
      return IObservation.and(IObservation.termination(),
                              IObservation.sender(object));
    }
  }

  /**
   * @return event's active object
   */
  public UObject getUObject() {
    return this.uObject;
  }

  /**
   * @return event's observation
   */
  public IObservation getObservation() {
    return this.observation;
  }

  /**
   * @return event's underlying occurrence specification in a UML interaction
   */
  public @Nullable UOccurrenceSpecification getUOccurrenceSpecification() {
    return this.uOccurrence;
  }

  /**
   * Determine whether this event and another event have to be ordered.
   *
   * Two events are ordered if, and only if they share an active lifeline.
   *
   * @param other an event
   * @return whether this event and {@code other} are ordered
   */
  public boolean isOrdered(IEvent other) {
    return Objects.equals(this.uObject, other.uObject);
  }

  /**
   * Determine whether this event should occur before another event.
   *
   * An event should occur before another event if, and only if, the first
   * event represents the sending and the second the receiving of a message.
   *
   * @param other an event
   * @return whether this event and {@code other} are ordered
   */
  public boolean isBefore(IEvent other) {
    UOccurrenceSpecification thisUOccurrence = this.uOccurrence;
    if (thisUOccurrence == null)
      return false;
    UOccurrenceSpecification otherUOccurrence = other.uOccurrence;
    if (otherUOccurrence == null)
      return false;

    return thisUOccurrence.isBefore(otherUOccurrence);
  }

  @Override
  public String toString() {
    UOccurrenceSpecification uOccurrence = this.uOccurrence;
    if (uOccurrence != null)
      return uOccurrence.toString();
    return this.observation.toString();
  }
}
