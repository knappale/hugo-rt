package uml.ida;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UObject;
import util.Formatter;


/**
 * Ida transition
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 * @since 05/09/04
 */
@NonNullByDefault
public class ITransition {
  public static class Label {
    private @Nullable IEvent trigger;
    private @Nullable ICondition guard;
    private @Nullable IClockBound clockBound;

    public Label(@Nullable IEvent trigger, @Nullable ICondition guard, @Nullable IClockBound clockBound) {
      this.trigger = trigger;
      this.guard = guard;
      this.clockBound = clockBound;
    }

    public Label(@Nullable IEvent trigger, @Nullable ICondition guard) {
      this(trigger, guard, null);
    }

    /**
     * @return transition label's triggering event
     */
    public @Nullable IEvent getTrigger() {
      return this.trigger;
    }

    /**
     * @return transition label's guard expression
     */
    public @Nullable ICondition getGuard() {
      return this.guard;
    }

    /**
     * @return transition label's clock bound
     */
    public @Nullable IClockBound getClockBound() {
      return this.clockBound;
    }

    /**
     * Add a clock bound to this transition label.
     * 
     * @param otherClockBound a clock bound
     */
    public void addClockBound(@Nullable IClockBound otherClockBound) {
      if (otherClockBound == null)
        return;

      IClockBound clockBound = this.clockBound;
      if (clockBound == null) {
        this.clockBound = otherClockBound;
        return;
      }

      this.clockBound = IClockBound.and(clockBound, otherClockBound);
    }

    /**
     * Declaration of this Ida transition label in Ida format.
     *
     * @param prefix a prefix for each line
     * @return declaration of this Ida transition label
     */
    public String declaration(String prefix) {
      StringBuilder resultBuilder = new StringBuilder();

      if (this.trigger != null) {
        resultBuilder.append(prefix);
        resultBuilder.append("observation ");
        resultBuilder.append(this.trigger);
        resultBuilder.append(";\n");
      }

      if (this.guard != null) {
        resultBuilder.append(prefix);
        resultBuilder.append("guard ");
        resultBuilder.append(this.guard);
        resultBuilder.append(";\n");
      }

      if (this.clockBound != null) {
        resultBuilder.append(prefix);
        resultBuilder.append("timing ");
        resultBuilder.append(this.clockBound);
        resultBuilder.append(";\n");
      }

      return resultBuilder.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.trigger, this.guard, this.clockBound);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;

      try {
        Label other = (Label)object;
        return Objects.equals(this.trigger, other.trigger) &&
               Objects.equals(this.guard, other.guard) &&
               Objects.equals(this.clockBound, other.clockBound);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      if (this.trigger != null) {
        resultBuilder.append(this.trigger);
        resultBuilder.append(" ");
      }
      resultBuilder.append("[");
      if (this.guard == null)
        resultBuilder.append("true");
      else
        resultBuilder.append(this.guard);
      resultBuilder.append("]");
      if (this.clockBound != null) {
        resultBuilder.append(" { ");
        resultBuilder.append(this.clockBound);
        resultBuilder.append(" }");
      }
      return resultBuilder.toString();
    }
  }

  private IState from;
  private Label label;
  private List<IBranch> iBranches = new ArrayList<>();

  ITransition(IState from, Label label, @Nullable IAction effect, IState to) {
    this.from = from;
    this.label = label;
    this.iBranches.add(new IBranch(effect, to));
  }

  ITransition(IState from, Label label, List<IBranch> iBranches) {
    this.from = from;
    this.label = label;
    this.iBranches.addAll(iBranches);
  }

  ITransition(IState from, @Nullable IEvent trigger, @Nullable ICondition guard, @Nullable IAction effect, IState to) {
    this(from, new Label(trigger, guard), effect, to);
  }

  /**
   * @return transition's source state
   */
  public IState getFrom() {
    return this.from;
  }

  /**
   * @return transition's label
   */
  public Label getLabel() {
    return this.label;
  }

  /**
   * @return transition's triggering event
   */
  public @Nullable IEvent getTrigger() {
    return this.label.getTrigger();
  }

  /**
   * @return transition's guard expression
   */
  public @Nullable ICondition getGuard() {
    return this.label.getGuard();
  }

  /**
   * @return transition's branches
   */
  public List<IBranch> getBranches() {
    return this.iBranches;
  }

  /**
   * Add an effect sequentially to all branches of this transition.
   * 
   * @return branch's effect action
   */
  public void addEffect(@Nullable IAction effect) {
    for (IBranch iBranch : this.iBranches)
      iBranch.addEffect(effect);
  }

  /**
   * Add a clock bound to this transition.
   * 
   * @param otherClockBound a clock bound
   */
  public void addClockBound(@Nullable IClockBound otherClockBound) {
    this.label.addClockBound(otherClockBound);
  }

  /**
   * @return transition's clock bound
   */
  public @Nullable IClockBound getClockBound() {
    return this.label.getClockBound();
  }

  /**
   * Determine whether this transition is in conflict with another transition.
   *
   * A transition is in conflict with another transition if the trigger of the first
   * transition is ordered * with the second transition's trigger or the trigger of
   * the first transition has to occur before the second transition's trigger or the
   * first transition's trigger and guard cover a lifeline also covered by the
   * condition of the second transition.
   *
   * @param other another transition
   * @return whether this transition is in conflict with {@link other}
   */
  public boolean isInConflict(ITransition other) {
    IEvent otherTrigger = other.getTrigger();
    if (otherTrigger != null) {
      IEvent thisTrigger = this.getTrigger();
      if (thisTrigger != null && (thisTrigger.isOrdered(otherTrigger) || thisTrigger.isBefore(otherTrigger)))
        return true;
      ICondition thisGuard = this.getGuard();
      if (thisGuard != null && thisGuard.covers(otherTrigger)) 
        return true;
    }

    // Conditions can cover several lifelines and we must check them all.
    ICondition otherGuard = other.getGuard();
    if (otherGuard != null) {
      IEvent thisTrigger = this.getTrigger();
      if (thisTrigger != null && otherGuard.covers(thisTrigger)) 
        return true;
      ICondition thisGuard = this.getGuard();
      if (thisGuard != null) {
        for (UObject lifeline : thisGuard.getCoveredLifelines()) {
          if (otherGuard.getCoveredLifelines().contains(lifeline))
            return true;
        }
      }
    }

    return false;
  }

  /**
   * Declaration of this Ida transition in Ida format.
   *
   * @param prefix a prefix for each line
   * @return declaration of this Ida transition
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(this.from.getName());
    resultBuilder.append(" {\n");
    resultBuilder.append(this.label.declaration(nextPrefix));
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    resultBuilder.append(Formatter.separated(this.iBranches, iBranch -> iBranch.declaration(nextPrefix), "\n" + prefix + "| "));

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.from, this.iBranches);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;

    try {
      ITransition other = (ITransition)object;
      return Objects.equals(this.from, other.from) &&
             Objects.equals(this.label, other.label) &&
             Objects.equals(this.iBranches, other.iBranches);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.from.getName());
    resultBuilder.append(" -> ");
    resultBuilder.append(this.label);
    resultBuilder.append(" ");
    resultBuilder.append(Formatter.separated(iBranches, " | "));
    return resultBuilder.toString();
  }
}
