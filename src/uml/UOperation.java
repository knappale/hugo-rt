package uml;

import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * Contains data of a UML operation
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UOperation extends UBehavioural {
  private boolean isStatic = false;

  /**
   * Create a new operation.
   *
   * @param name operation's name
   * @param owner operation's owning class
   */
  public UOperation(String name, UClass owner, boolean isStatic) {
    super(name, owner);
    this.isStatic = isStatic;
  }

  @Override
  @NonNullByDefault({})
  public <T> T cases(@NonNull Function<@NonNull UOperation, T> operationFun,
                     @NonNull Function<@NonNull UReception, T> receptionFun) {
    return operationFun.apply(this);
  }

  @Override
  public boolean isStatic() {
    return this.isStatic;
  }

  /**
   * @param prefix prefix string
   * @return operation's representation in UTE format with prefix
   */
  @Override
  public String declaration(String prefix) {
    StringBuilder builder = new StringBuilder();

    builder.append(prefix);
    if (this.isStatic)
      builder.append("static ");
    builder.append("operation ");
    builder.append(this.getName());
    builder.append("(");
    builder.append(Formatter.separated(this.getParameters(), p -> p.declaration(), ", "));
    builder.append(")");
    if (this.getMethods().isEmpty())
      builder.append(";");
    else {
      builder.append(" {\n");
      builder.append(Formatter.separated(this.getMethods(), m -> m.declaration(prefix + "  "), "\n"));
      builder.append("\n");
      builder.append(prefix);
      builder.append("}");
    }

    return builder.toString();
  }

  /**
   * @return operation's string representation
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Operation [name = ");
    builder.append(this.getName());
    builder.append(", #parameters = ");
    builder.append(this.getParameters().size());
    builder.append(", isStatic = ");
    builder.append(this.isStatic);
    builder.append("]");
    return builder.toString();
  }
}
