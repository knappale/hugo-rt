package uml.smile;

import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Sets;


@NonNullByDefault
public class SValue {
  private @Nullable SConstant constant;

  private static final SValue UNKNOWNVALUE;
  private static final SValue FALSEVALUE;
  private static final SValue TRUEVALUE;
  private static final SValue EMPTYVALUE;
  static {
    UNKNOWNVALUE = new SValue(null);
    FALSEVALUE = new SValue(null);
    TRUEVALUE = new SValue(null);
    EMPTYVALUE = new SValue(SConstant.empty());
  }

  @NonNullByDefault({})
  public class Cases<T> {
    private Function<@NonNull Boolean, T> booleanFun = null;
    private Function<@NonNull SConstant, T> constantFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases<T> booleanConstant(Function<@NonNull Boolean, T> booleanFun) {
      this.booleanFun = booleanFun;
      return this;
    }
    
    public Cases<T> constant(Function<@NonNull SConstant, T> constantFun) {
      this.constantFun = constantFun;
      return this;
    }
    
    public Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply() {
      if (SValue.this == TRUEVALUE && this.booleanFun != null)
        return this.booleanFun.apply(true);
      if (SValue.this == FALSEVALUE && this.booleanFun != null)
        return this.booleanFun.apply(false);
      SConstant constant = SValue.this.constant;
      if (constant != null && this.constantFun != null)
        return this.constantFun.apply(constant);

      assert (this.otherwiseFun != null) : "No default for case distinction on Smile value type";
      return this.otherwiseFun.get();
    }
  }

  private SValue(@Nullable SConstant constant) {
    this.constant = constant;
  }

  public static SValue constant(SConstant constant) {
    return new SValue(constant);
  }

  public static SValue unknownValue() {
    return UNKNOWNVALUE;
  }

  public static SValue falseValue() {
    return FALSEVALUE;
  }

  public static SValue trueValue() {
    return TRUEVALUE;
  }

  public static SValue booleanValue(boolean b) {
    return (b ? TRUEVALUE : FALSEVALUE);
  }

  public static SValue emptyValue() {
    return EMPTYVALUE;
  }

  public @Nullable SConstant getConstant() {
    return this.constant;
  }

  public SValue neg() {
    if (this == TRUEVALUE)
      return FALSEVALUE;
    if (this == FALSEVALUE)
      return TRUEVALUE;
    return UNKNOWNVALUE;
  }

  public SValue eq(SValue other) {
    if (this != UNKNOWNVALUE && other != UNKNOWNVALUE)
      return SValue.booleanValue(this.equals(other));
    return UNKNOWNVALUE;
  }

  public SValue neq(SValue other) {
    if (this != UNKNOWNVALUE && other != UNKNOWNVALUE)
      return SValue.booleanValue(!this.equals(other));
    return UNKNOWNVALUE;
  }

  public SValue and(SValue other) {
    if (this == FALSEVALUE || other == FALSEVALUE)
      return FALSEVALUE;
    if (this == TRUEVALUE && other == TRUEVALUE)
      return TRUEVALUE;
    return UNKNOWNVALUE;
  }
      
  /**
   * empty and X = empty
   * { true } and X = X
   * { false } and X = { false }
   * { true, false } and X = { true, false }
   *
   * @param s1 a first set of (boolean) values
   * @param s2 a second set of (boolean) values
   * @return disjunctive combination of {@code s1} and {@code s2}
   */
  public static Set<SValue> and(Set<SValue> s1, Set<SValue> s2) {
    if (!Sets.elements(FALSEVALUE, TRUEVALUE).containsAll(Sets.union(s1, s2)))
      return Sets.singleton(UNKNOWNVALUE);
    if (s1.isEmpty() || s2.isEmpty())
      return Sets.empty();
    if (s1.equals(Sets.singleton(FALSEVALUE)) || s2.equals(Sets.singleton(FALSEVALUE)))
      return Sets.singleton(FALSEVALUE);
    if (s1.equals(Sets.singleton(TRUEVALUE)))
      return s2;
    if (s2.equals(Sets.singleton(TRUEVALUE)))
      return s1;
    return Sets.union(s1, s2);
  }

  public SValue or(SValue other) {
    if (this == TRUEVALUE || other == TRUEVALUE)
      return TRUEVALUE;
    if (this == FALSEVALUE && other == FALSEVALUE)
      return FALSEVALUE;
    return UNKNOWNVALUE;
  }

  /**
   * empty or X = empty
   * { true } or X = { true }
   * { false } or X = X
   * { true, false } or X = { true, false }
   *
   * @param s1 a first set of (boolean) values
   * @param s2 a second set of (boolean) values
   * @return disjunctive combination of {@code s1} and {@code s2}
   */
  public static Set<SValue> or(Set<SValue> s1, Set<SValue> s2) {
    if (!Sets.elements(FALSEVALUE, TRUEVALUE).containsAll(Sets.union(s1, s2)))
      return Sets.singleton(UNKNOWNVALUE);
    if (s1.isEmpty() || s2.isEmpty())
      return Sets.empty();
    if (s1.equals(Sets.singleton(TRUEVALUE)) || s2.equals(Sets.singleton(TRUEVALUE)))
      return Sets.singleton(TRUEVALUE);
    if (s1.equals(Sets.singleton(FALSEVALUE)))
      return s2;
    if (s2.equals(Sets.singleton(FALSEVALUE)))
      return s1;
    return Sets.union(s1, s2);
  }

  public Set<SConstant> getConstants() {
    SConstant constant = this.constant;
    if (constant != null)
      return Sets.singleton(constant);
    return Sets.empty();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.constant);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SValue other) {
      if (this == UNKNOWNVALUE)
        return other == UNKNOWNVALUE;
      if (this == FALSEVALUE)
        return other == FALSEVALUE;
      if (this == TRUEVALUE)
        return other == TRUEVALUE;
      return Objects.equals(this.constant, other.constant);
    }
    return false;
  }

  @Override
  public String toString() {
    if (this == FALSEVALUE)
      return "false";
    if (this == TRUEVALUE)
      return "true";
    SConstant constant = this.constant;
    if (constant != null)
      return "<" + constant.getName() + ">";
    return "unknown";
  }
}
