package uml.smile;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Sets;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toList;

import static util.Objects.requireNonNull;


@NonNullByDefault
public class SCommand extends SStatement {
  enum Kind {
    SKIP,
    BREAK,
    AWAIT,
    ASSIGN;
  }

  private Kind kind;
  private @Nullable SVariable variable;
  private @Nullable SValue value;
  private @Nullable SVariable otherVariable;
  private @Nullable SGuard guard;

  public interface Visitor<T> {
    public T onSkip();
    public T onBreak();
    public T onAwait(SGuard sGuard);
    public T onAssignment(SVariable sVariable, SValue sValue);
    public T onAssignment(SVariable sVariable, SVariable otherSVariable);
  }

  private SCommand(Kind kind) {
    this.kind = kind;
  }

  /**
   * Copy constructor.
   *
   * @param command a command
   * @return a new copy of {@code command}
   */
  private SCommand(SCommand command) {
    this.kind = command.kind;
    this.variable = command.variable;
    this.value = command.value;
    this.otherVariable = command.otherVariable;
    this.guard = command.guard;
  }

  /**
   * Create a command representing a nop.
   *
   * @return a Smile command
   */
  public static SCommand skip() {
    return new SCommand(Kind.SKIP);
  }

  /**
   * Create a command representing a break of a loop.
   *
   * @return a Smile command
   */
  public static SCommand breakStm() {
    return new SCommand(Kind.BREAK);
  }

  /**
   * Create a command representing awaiting a condition.
   *
   * @param guard a condition
   * @return a Smile control statement
   */
  public static SCommand await(SGuard guard) {
    SCommand c = new SCommand(Kind.AWAIT);
    c.guard = guard;
    return c;
  }

  /**
   * Create a command representing the assignment of a value
   * to a variable.
   *
   * @param variable a variable
   * @param value a value
   * @return a Smile command
   */
  public static SCommand assign(SVariable variable, SValue value) {
    SCommand c = new SCommand(Kind.ASSIGN);
    c.variable = variable;
    c.value = value;
    return c;
  }

  /**
   * Create a command representing the assignment of a constant
   * to a variable.
   *
   * @param variable a variable
   * @param value a value
   * @return a Smile command
   */
  public static SCommand assign(SVariable variable, SConstant constant) {
    return SCommand.assign(variable, SValue.constant(constant));
  }

  /**
   * Create a command representing the assignment of a variable
   * to a variable.
   *
   * @param variable a variable
   * @param otherVariable a variable
   * @return a Smile command
   */
  public static SCommand assign(SVariable variable, SVariable otherVariable) {
    SCommand c = new SCommand(Kind.ASSIGN);
    c.variable = variable;
    c.otherVariable = otherVariable;
    return c;
  }

  @Override
  boolean isEmpty() {
    return this.kind == Kind.SKIP;
  }

  @Override
  int getComplexity() {
    return this.kind == Kind.SKIP ? 0 : 1;
  }

  @Override
  public Set<SPrimitive> getPrimitives() {
    return Sets.empty();
  }

  @Override
  public Set<SConstant> getConstants() {
    Set<SConstant> constants = new HashSet<>();

    switch (this.kind) {
      case ASSIGN: {
        SValue value = this.value;
        if (value != null)
          constants.addAll(value.getConstants());
        return constants;
      }

      case AWAIT:
        return requireNonNull(this.guard).getConstants();

      //$CASES-OMITTED$
      default:
        return constants;
    }
  }

  @Override
  public Set<SConstant> getConstantsAssigned(Collection<SVariable> variables) {
    Set<SConstant> constants = new HashSet<>();

    switch (this.kind) {
      case ASSIGN: {
        SValue value = this.value;
        if (value != null && variables.contains(this.variable))
          constants.addAll(value.getConstants());
        return constants;
      }

      //$CASES-OMITTED$
      default:
        return constants;
    }
  }

  @Override
  public @Nullable SStatement getResumption(SPrimitive from, SVariable breakFlag) {
    return null;
  }

  @Override
  public SStatement getCycle(SVariable suspendFlag) {
    return this;
  }

  @Override
  Set<SVariable> getAssignedVariables() {
    switch (this.kind) {
      case ASSIGN: {
        return Sets.singleton(requireNonNull(this.variable));
      }

      //$CASES-OMITTED$
      default:
        return Sets.empty();
    }
  }

  @Override
  Set<SVariable> getUsedVariables() {
    switch (this.kind) {
      case ASSIGN: {
        SVariable otherVariable = this.otherVariable;
        if (otherVariable != null)
          return Sets.singleton(otherVariable);
        return Sets.empty();
      }

      case AWAIT:
        return requireNonNull(this.guard).getVariables();

      //$CASES-OMITTED$
      default:
        return Sets.empty();
    }
  }

  @Override
  public List<SCommand> getUnusedAssignments(final List<SCommand> assignments, List<SCommand> overwrittenAssignments) {
    switch (this.kind) {
      case ASSIGN: {
        Set<SVariable> usedVariables = this.otherVariable != null ? Sets.singleton(this.otherVariable) : Sets.empty();
        overwrittenAssignments.addAll(assignments.stream().filter(assignment -> assignment.getAssignedVariables().contains(this.variable)).collect(toList()));
        List<SCommand> unusedAssignments = assignments.stream().filter(a1 -> overwrittenAssignments.stream().noneMatch(a2 -> a1 == a2) &&
                                                                             Sets.intersection(usedVariables, a1.getAssignedVariables()).isEmpty()).collect(toList());
        unusedAssignments.add(this);
        return unusedAssignments;
      }

      case AWAIT: {
        Set<SVariable> usedVariables = requireNonNull(this.guard).getVariables();
        return assignments.stream().filter(assignment -> Sets.intersection(usedVariables, assignment.getAssignedVariables()).isEmpty()).collect(toList());
      }

      //$CASES-OMITTED$
      default:
        return assignments;
    }
  }

  @Override
  public SCommand replace(Collection<SCommand> commands, SCommand newCommand) {
    if (commands.stream().anyMatch(s -> this == s)) {
      this.kind = newCommand.kind;
      this.variable = newCommand.variable;
      this.value = newCommand.value;
      this.otherVariable = newCommand.otherVariable;
      this.guard = newCommand.guard;
    }
    return this;
  }

  @Override
  public SCommand replaceTopLevelBreaks(SCommand newCommand) {
    if (this.kind == Kind.BREAK)
      return this.replace(Sets.singleton(this), newCommand);
    return this;
  }

  /**
   * Accept a visitor.
   *
   * @param visitor a visitor
   * @return visit's result
   */
  public <T> T accept(Visitor<T> visitor) {
    switch (this.kind) {
      case SKIP:
        return visitor.onSkip();

      case AWAIT:
        return visitor.onAwait(requireNonNull(this.guard));

      case BREAK:
        return visitor.onBreak();
     
      case ASSIGN:
        if (this.value != null)
          return visitor.onAssignment(requireNonNull(this.variable), requireNonNull(this.value));
        else
          return visitor.onAssignment(requireNonNull(this.variable), requireNonNull(this.otherVariable));

      default:
        throw new IllegalStateException("Unknown Smile command kind.");
    }
  }

  @Override
  public <T> T accept(SStatement.Visitor<T> visitor) {
    return this.accept((Visitor<T>)visitor);
  }

  @Override
  public SState execute(SState state) {
    switch (this.kind) {
      case ASSIGN: {
        SVariable variable = requireNonNull(this.variable);
        SValue value = this.value;
        if (value != null)
          return state.withAssignment(variable, value);
        return state.withAssignment(variable, state.getValues(requireNonNull(this.otherVariable)));
      }

      case AWAIT:
        return state.withConstraint(requireNonNull(this.guard));

      //$CASES-OMITTED$
      default:
        return state;
    }
  }

  @Override
  public boolean isTerminating(SState state) {
    switch (this.kind) {
      case AWAIT:
        return requireNonNull(this.guard).evaluate(state).equals(Sets.singleton(SValue.trueValue()));

      //$CASES-OMITTED$
      default:
        return true;
    }
  }

  @Override
  public boolean isBreaking(SState state) {
    switch (this.kind) {
      case BREAK:
        return true;

      //$CASES-OMITTED$
      default:
        return false;
    }
  }

  @Override
  public SStatement partiallyEvaluate(SState state) {
    switch (this.kind) {
      case ASSIGN: {
        SVariable variable = requireNonNull(this.variable);
        SValue value = this.value;
        if (value != null) {
          if (state.getValues(variable).equals(Sets.singleton(value)))
            return SCommand.skip();
          return new SCommand(this);
        }
        SVariable otherVariable = requireNonNull(this.otherVariable);
        var otherValues = state.getValues(otherVariable);
        if (otherValues.size() != 1)
          return new SCommand(this);
        var values = state.getValues(variable);
        if (values.size() == 1 && values.equals(otherValues))
          return SCommand.skip();
        return SCommand.assign(variable, otherValues.iterator().next());
      }

      case AWAIT: {
        SGuard reducedGuard = requireNonNull(this.guard).reduce(state);
        if (reducedGuard.evaluate(state).equals(Sets.singleton(SValue.trueValue())))
          return SCommand.skip();
        return SCommand.await(reducedGuard);
      }

      //$CASES-OMITTED$
      default:
        return new SCommand(this);
    }
  }

  @Override
  public SStatement getSimplified() {
    return new SCommand(this);
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case SKIP:
        resultBuilder.append(";");
        return resultBuilder.toString();

      case BREAK:
        resultBuilder.append("break;");
        return resultBuilder.toString();

      case AWAIT:
        resultBuilder.append("await(");
        resultBuilder.append(this.guard);
        resultBuilder.append(");");
        return resultBuilder.toString();

      case ASSIGN:
        resultBuilder.append(requireNonNull(this.variable).getName());
        resultBuilder.append(" = ");
        resultBuilder.append(this.value != null ? requireNonNull(this.value) : requireNonNull(this.otherVariable).getName());
        return resultBuilder.append(";").toString();
    }

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.kind,
                        this.variable,
                        this.value,
                        this.otherVariable,
                        this.guard);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SCommand other)
      return this.kind == other.kind &&
             Objects.equals(this.variable, other.variable) &&
             Objects.equals(this.value, other.value) &&
             Objects.equals(this.otherVariable, other.otherVariable) &&
             Objects.equals(this.guard, other.guard);
    return false;
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
