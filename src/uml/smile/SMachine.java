package uml.smile;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.smile.translation.SSymbolTable;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import util.Formatter;
import util.Pair;

import static java.util.stream.Collectors.toSet;
import static java.util.stream.Collectors.toList;


/**
 * Smile machine, representing a (simple) UML state machine
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class SMachine {
  private SSymbolTable symbolTable;
  private SStatement initialisation;
  private SStatement main;
  private SGuard terminated;
  private Set<SConstant> constants = new HashSet<>();
  private Set<SVariable> variables = new HashSet<>();
  private SState initialState;
  private SState intermediateState;
  private @Nullable SStatement fullInitialisation = null;
  private Map<UEvent, SStatement> fullReactions = new HashMap<>();
  private @Nullable Pair<SStatement, IdentityHashMap<SPrimitive, SStatement>> initialisationCycles = null;
  private Map<UEvent, Pair<SStatement, IdentityHashMap<SPrimitive, SStatement>>> reactionCyclesMap = new HashMap<>();

  /**
   * @param uStateMachine UML state machine which this machine represents
   */
  public SMachine(SSymbolTable symbolTable, SStatement initialisation, SStatement main, SGuard terminated) {
    this.symbolTable = symbolTable;
    this.variables.addAll(initialisation.getVariables());
    this.variables.addAll(main.getVariables());
    this.variables.addAll(terminated.getVariables());
    this.constants.addAll(initialisation.getConstants());
    this.constants.addAll(main.getConstants());
    this.constants.addAll(terminated.getConstants());
    this.initialisation = initialisation;
    this.main = main;
    this.terminated = terminated;
    this.initialState = SState.initial(this);
    this.intermediateState = SState.intermediate(this).withSomeState();
  }

  /**
   * @return machine's underlying state machine
   */
  public UStateMachine getStateMachine() {
    return this.symbolTable.getStateMachine();
  }

  /**
   * @return machine's underlying symbol table
   */
  public SSymbolTable getSymbolTable() {
    return this.symbolTable;
  }

  /**
   * @return machine's name
   */
  public String getName() {
    return this.getStateMachine().getC1ass().getName();
  }

  /**
   * @return machine's variables
   */
  public Set<SVariable> getVariables() {
    return this.variables;
  }

  /**
   * Determine state variable for a vertex's container state.
   * 
   * @param vertex a vertex
   * @return Smile variable
   * @pre vertex.getContainer() != null
   */
  public SVariable getStateVariable(UVertex vertex) {
    return this.symbolTable.getVertexVariable(vertex);
  }

  /**
   * @return this machine's initialisation statement
   */
  private SStatement getFullInitialisation() {
    var fullInitialisation = this.fullInitialisation;
    if (fullInitialisation != null)
      return fullInitialisation;
    return this.fullInitialisation = this.initialisation.simplify(this.initialState);
  }

  /**
   * Determine the Smile statement representing the initialisation of this Smile machine.
   *
   * @return Smile statement representing the initialisation of this machine
   */
  public SStatement getInitialisation() {
    return this.getFullInitialisation();
  }

  /**
   * Determine the Smile cycles, i.e., a statement and resumptions, representing
   * the initialisation of this Smile machine.
   *
   * @param uEvent a UML event
   * @return Smile cycles representing the initialisation of this machine
   */
  public Pair<SStatement, IdentityHashMap<SPrimitive, SStatement>> getInitialisationCycles() {
    var initialisationCycles = this.initialisationCycles;
    if (initialisationCycles != null)
      return initialisationCycles;
    return this.initialisationCycles = this.getCycles(this.getFullInitialisation(), null);
  }

  /**
   * @return (unsimplified) overall event processing
   */
  public SStatement getMain() {
    return this.main;
  }

  /**
   * @return this machine's termination condition
   */
  public SGuard getTerminated() {
    return this.terminated;
  }

  /**
   * @return this machine's termination branch
   */
  public SBranch getTermination() {
    return SBranch.branch(this.terminated, SPrimitive.success());
  }

  /**
   * Determine the full reaction Smile statement of this machine to an event.
   *
   * @param uEvent a UML event for this machine
   * @return the machine's full reaction to  {@code uEvent}
   */
  private SStatement getFullReaction(UEvent uEvent) {
    var fullReaction = this.fullReactions.get(uEvent);
    if (fullReaction != null)
      return fullReaction;

    fullReaction = this.main.simplify(this.intermediateState.withEvent(uEvent));
    this.fullReactions.put(uEvent, fullReaction);
    return fullReaction;
  }
    
  /**
   * Determine the Smile statement representing the reaction of this
   * Smile machine to a given UML event.
   *
   * @param uEvent a UML event
   * @return Smile statement representing the reaction to {@code uEvent}
   */
  public SStatement getReaction(UEvent uEvent) {
    return this.getFullReaction(uEvent);
  }

  /**
   * Determine the Smile cycles, i.e., a statement and resumptions, representing
   * the reaction of this Smile machine to a given UML event.
   *
   * @param uEvent a UML event
   * @return Smile cycles representing the reaction to {@code uEvent}
   */
  public Pair<SStatement, IdentityHashMap<SPrimitive, SStatement>> getReactionCycles(UEvent uEvent) {
    var reactionCycles = this.reactionCyclesMap.get(uEvent);
    if (reactionCycles != null)
      return reactionCycles;
    reactionCycles = this.getCycles(this.getFullReaction(uEvent), uEvent);
    this.reactionCyclesMap.put(uEvent, reactionCycles);
    return reactionCycles;
  }

  private SStatement getCycle(SStatement sStatement, @Nullable UEvent uEvent, IdentityHashMap<SPrimitive, SPrimitive> markersMap) {
    var suspendFlag = this.getSymbolTable().getSuspendFlag();
    var cycle = sStatement.getCycle(suspendFlag);
    if (cycle != sStatement)
      cycle = SControl.seq(cycle, SCommand.assign(suspendFlag, SValue.falseValue()));
    var sState = this.intermediateState.withAssignment(suspendFlag, SValue.falseValue());
    if (uEvent != null)
      sState = sState.withEvent(uEvent);
    cycle = cycle.simplify(sState);
    cycle.getPrimitives().stream().forEach(sPrimitive -> {
      var marker = sPrimitive.getMarker();
      if (marker != null)
        markersMap.put(marker, sPrimitive);
    });
    return cycle;
  }

  private Pair<SStatement, IdentityHashMap<SPrimitive, SStatement>> getCycles(SStatement sStatement, @Nullable UEvent uEvent) {
    var breakFlag = this.getSymbolTable().getBreakFlag();
    var markersMap = new IdentityHashMap<SPrimitive, SPrimitive>();
    var resumptionCyclesMap = new IdentityHashMap<SPrimitive, SStatement>();

    var firstCycle = this.getCycle(sStatement, uEvent, markersMap);

    for (var suspender : sStatement.getPrimitives().stream().filter(p -> p.suspended() != null).collect(toList())) {
      var resumption = sStatement.getResumption(suspender, breakFlag);
      if (resumption == null)
        continue;

      var resumptionSState = this.intermediateState.withAssignment(breakFlag, SValue.falseValue());
      if (uEvent != null)
        resumptionSState = resumptionSState.withEvent(uEvent);
      resumption = resumption.simplify(resumptionSState);
      var resumptionCycle = this.getCycle(resumption, uEvent, markersMap);
      resumptionCyclesMap.put(suspender, resumptionCycle);
    };

    var translatedResumptionCyclesMap = new IdentityHashMap<SPrimitive, SStatement>();
    for (var suspender : resumptionCyclesMap.keySet()) {
      var marker = markersMap.get(suspender);
      var resumptionCycle = resumptionCyclesMap.get(suspender);
      if (marker != null && resumptionCycle != null)
        translatedResumptionCyclesMap.put(marker, resumptionCycle);
    } 
    return new Pair<>(firstCycle, translatedResumptionCyclesMap);
  }

  /**
   * Determine the Smile statement representing the reaction of this
   * Smile machine to some UML event.
   *
   * @return Smile statement representing the reaction to some event
   */
  public SStatement getReaction() {
    if (this.symbolTable.getProperties().isEventBased())
      return SBranching.choice(this.getStateMachine().getEvents().stream().map(uEvent ->
                               SBranch.branch(SGuard.literal(SLiteral.match(uEvent)), this.getReaction(uEvent))).collect(toSet()));
    var reaction = this.main.simplify(this.intermediateState.withSomeEvent());
    return reaction;
  }

  /**
   * @return machine's (compound) statement
   */
  public SStatement getStatement() {
    return SControl.seq(this.getInitialisation(),
                        SBranching.loop(Arrays.asList(this.getTermination()),
                                        SControl.atomic(SControl.seq(SPrimitive.fetch(this.getStateMachine().getWaitStates()),
                                                        this.getReaction()))));
  }

  /**
   * @return machine's full (compound) statement, without optimisations
   */
  public SStatement getFullStatement() {
    return SControl.seq(this.initialisation,
                        SBranching.loop(Arrays.asList(this.getTermination()),
                                        SControl.atomic(SControl.seq(SPrimitive.fetch(this.getStateMachine().getWaitStates()),
                                                                     this.main))));
  }

  /**
   * Determine the set of Smile constants that are used in this machine.
   * 
   * @return set of used Smile constants
   */
  public Set<SConstant> getConstants() {
    return this.constants;
  }

  /**
   * Determine the set of possible events this machine can handle.
   *
   * @return set of possible events
   */
  public Set<UEvent> getEventDomain() {
    return this.getStateMachine().getEvents();
  }

  /**
   * Determine the set of possible completion states this machine can handle.
   *
   * @return set of possible events
   */
  public Set<UState> getCompletionStates() {
    return this.getStateMachine().getCompletionStates();
  }

  /**
   * @param prefix line prefix
   * @return machine declaration, every new line prepended by {@code prefix}
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("machine ");
    resultBuilder.append(this.getName());
    resultBuilder.append(" {\n");

    var statement = this.getStatement();
    var variables = statement.getVariables();

    String nextPrefix = prefix + "  ";
    String sep = "";
    for (var variable : variables) {
      resultBuilder.append(nextPrefix);
      resultBuilder.append(variable.declaration());
      resultBuilder.append("\n");
      sep = "\n";
    }
    resultBuilder.append(sep);

    resultBuilder.append(nextPrefix);
    resultBuilder.append(statement.declaration(nextPrefix));
    resultBuilder.append("\n");

    resultBuilder.append(prefix);
    resultBuilder.append("}");
    return resultBuilder.toString();
  }

  /**
   * @return machine declaration
   */
  public String declaration() {
    return declaration("");
  }

  /**
   * @return machine representation as string
   */
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Machine [name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append(", variables = ");
    resultBuilder.append(Formatter.list(this.getVariables()));
    resultBuilder.append(", statement = ");
    resultBuilder.append(this.getStatement());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
