package uml.smile;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class SAtomic extends SControl {
  private SStatement statement;

  SAtomic(SStatement statement) {
    this.statement = statement;
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  int getComplexity() {
    return this.getComplexity();
  }

  @Override
  public Set<SPrimitive> getPrimitives() {
    return this.statement.getPrimitives();
  }

  @Override
  public Set<SConstant> getConstants() {
    return this.statement.getConstants();
  }

  @Override
  public Set<SConstant> getConstantsAssigned(Collection<SVariable> variables) {
    return this.statement.getConstantsAssigned(variables);
  }

  @Override
  public SStatement replace(Collection<SCommand> commands, SCommand newCommand) {
    this.statement.replace(commands, newCommand);
    return this;
  }

  @Override
  public SStatement replaceTopLevelBreaks(SCommand newCommand) {
    this.statement.replaceTopLevelBreaks(newCommand);
    return this;
  }

  @Override
  public @Nullable SStatement getResumption(SPrimitive from, SVariable breakFlag) {
    var fromStatement = this.statement.getResumption(from, breakFlag);
    if (fromStatement != null)
      return atomic(fromStatement);
    return null;
  }

  @Override
  public SStatement getCycle(SVariable suspendFlag) {
    return SControl.atomic(this.statement.getCycle(suspendFlag));
  }

  @Override
  Set<SVariable> getAssignedVariables() {
    return this.statement.getAssignedVariables();
  }

  @Override
  Set<SVariable> getUsedVariables() {
    return this.statement.getUsedVariables();
  }

  @Override
  public List<SCommand> getUnusedAssignments(final List<SCommand> assignments, List<SCommand> overwrittenAssignments) {
    return this.statement.getUnusedAssignments(assignments, overwrittenAssignments);
  }

  /**
   * Accept a visitor
   */
  public <T> T accept(Visitor<T> visitor) {
    return visitor.onAtomic(this.statement);
  }

  @Override
  public <T> T accept(SStatement.Visitor<T> visitor) {
    return this.accept((Visitor<T>)visitor);
  }

  @Override
  public SState execute(SState state) {
    return this.statement.execute(state);
  }

  @Override
  public boolean isTerminating(SState state) {
    return this.statement.isTerminating(state);
  }

  @Override
  public boolean isBreaking(SState state) {
    return this.statement.isBreaking(state);
  }

  @Override
  public SStatement partiallyEvaluate(SState state) {
    return SControl.atomic(this.statement.partiallyEvaluate(state));
  }

  @Override
  public SStatement getSimplified() {
    return SControl.atomic(this.statement.getSimplified());
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("atomic {\n");
    resultBuilder.append(prefix);
    resultBuilder.append("  ");
    resultBuilder.append(this.statement.declaration(prefix + "  "));
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append("}");
    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return this.statement.hashCode();
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SAtomic other)
      return Objects.equals(this.statement, other.statement);
    return false;
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
