package uml.smile;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Lists;
import util.Pair;
import util.Sets;


@NonNullByDefault
public class SLoop extends SBranching {
  SLoop(Collection<SBranch> branches, @Nullable SStatement elseStatement) {
    super(branches, elseStatement);
  }

  @Override
  SLoop make(Collection<SBranch> branches, @Nullable SStatement elseStatement) {
    return new SLoop(branches, elseStatement); 
  }

  @Override
  public List<SCommand> getUnusedAssignments(final List<SCommand> assignments, List<SCommand> overwrittenAssignments) {
    List<SBranch> branches = this.branches;
    SStatement elseSStatement = this.elseStatement;
    // TODO (AK180216) This is rather global.  Can it be improved?
    Set<SVariable> usedVariables = this.getUsedVariables();
    List<SCommand> localAssignments = assignments;

    List<List<SCommand>> unusedAssignmentsList = new ArrayList<>();
    List<List<SCommand>> overwrittenAssignmentsList = new ArrayList<>();
    for (SBranch branch : branches) {
      List<SCommand> branchOverwrittenAssignments = new ArrayList<>();
      overwrittenAssignmentsList.add(branchOverwrittenAssignments);
      unusedAssignmentsList.add(branch.getUnusedAssignments(localAssignments, branchOverwrittenAssignments));
    }
    if (elseSStatement != null) {
      List<SCommand> elseOverwrittenAssignments = new ArrayList<>();
      overwrittenAssignmentsList.add(elseOverwrittenAssignments);
      unusedAssignmentsList.add(elseSStatement.getUnusedAssignments(localAssignments, elseOverwrittenAssignments));
    }

    // Declare as overwritten all assignments that are overwritten in a branch but have not been present at entry
    overwrittenAssignments.addAll(overwrittenAssignmentsList.stream().map(aL -> aL.stream().filter(a1 -> localAssignments.stream().noneMatch(a2 -> a1 == a2)).collect(toList())).reduce(Lists.empty(), Lists::append));
    // Declare as overwritten all assignments on entry that are overwritten in all branches
    overwrittenAssignments.addAll(localAssignments.stream().filter(a1 -> overwrittenAssignmentsList.stream().allMatch(assignmentsList -> assignmentsList.stream().anyMatch(a2 -> a1 == a2))).collect(toList()));

    // Declare as unused all assignments that are unused in a branch but have not been present at entry, are not overwritten, and not used in the loop body
    List<SCommand> unusedAssignments = unusedAssignmentsList.stream().map(aL -> aL.stream().filter(a1 -> Sets.intersection(usedVariables, a1.getAssignedVariables()).equals(Sets.empty()) &&
                                                                                                           overwrittenAssignments.stream().noneMatch(a2 -> a1 == a2) &&
                                                                                                           localAssignments.stream().noneMatch(a2 -> a1 == a2)).collect(toList())).reduce(Lists.empty(), Lists::append);
    // Declare as unused all assignments on entry that are still unused in all branches and are not overwritten
    unusedAssignments.addAll(localAssignments.stream().filter(a1 -> overwrittenAssignments.stream().noneMatch(a2 -> a1 == a2) &&
                                                                    unusedAssignmentsList.stream().allMatch(aL -> aL.stream().anyMatch(a2 -> a1 == a2))).collect(toList()));

    return unusedAssignments;
  }

  @Override
  public SLoop replaceTopLevelBreaks(SCommand newCommand) {
    return this;
  }

  @Override
  public @Nullable SStatement getResumption(SPrimitive from, SVariable breakFlag) {
    SStatement fromStatement = null;
    for (var branch : this.branches) {
      fromStatement = branch.getResumption(from, breakFlag);
      if (fromStatement != null)
        break;
    }
    var elseStatement = this.elseStatement;
    if (fromStatement == null && elseStatement != null)
      fromStatement = elseStatement.getResumption(from, breakFlag);
    if (fromStatement == null)
      return null;
    fromStatement = SCommand.assign(breakFlag, SValue.falseValue()).merge(fromStatement.replaceTopLevelBreaks(SCommand.assign(breakFlag, SValue.trueValue())));
    return SControl.seq(fromStatement, SControl.ifThen(SGuard.literal(SLiteral.eq(breakFlag, false)), this), SCommand.assign(breakFlag, SValue.falseValue()));
  }

  @Override
  public SStatement getCycle(SVariable suspendFlag) {
    var found = false;
    var cycleBranches = new ArrayList<SBranch>();
    for (var branch : this.branches) {
      var cycleBranch = branch.getCycle(suspendFlag);
      if (cycleBranch != branch) {
        cycleBranches.add(cycleBranch.merge(SCommand.breakStm()));
        found = true;
      }
      else
        cycleBranches.add(branch);
    }
    var elseStatement = this.elseStatement;
    var cycleElseStatement = elseStatement != null ? elseStatement.getCycle(suspendFlag) : null;
    if (cycleElseStatement != elseStatement && cycleElseStatement != null) {
      cycleElseStatement = cycleElseStatement.merge(SCommand.breakStm());
      found = true;
    }
    if (!found)
      return this;
    return SBranching.loop(cycleBranches, cycleElseStatement);
  }

  @Override
  public <T> T accept(Visitor<T> visitor) {
    return visitor.onLoop(this.branches, this.elseStatement);
  }

  @Override
  public SState execute(SState state) {
    Set<SState> preStates = Sets.empty();
    Set<SState> postStates = Sets.empty();
    SChoice choice = SBranching.choice(this.branches, this.elseStatement);

    SState preState = state;
    while (true) {
      preStates.add(preState);
      SState postSState = choice.execute(preState);
      if (preStates.stream().anyMatch(pre -> pre.subsumes(postSState)))
        break;
      postStates.add(postSState);
      preState = postSState;
    }

    return postStates.stream().reduce(state.empty(), (s1, s2) -> s1.join(s2));
  }

  @Override
  public boolean isTerminating(SState state) {
    Set<SState> preStates = new HashSet<>();
    SChoice choice = SBranching.choice(this.branches, this.elseStatement);

    SState preState = state;
    while (true) {
      if (choice.isBreaking(preState))
        return true;
      preStates.add(preState);
      SState postState = choice.execute(preState);
      if (preStates.stream().anyMatch(pre -> pre.subsumes(postState)))
        break;
      preState = postState;
    }

    return false;
  }

  @Override
  public boolean isBreaking(SState state) {
    return false;
  }

  @Override
  public SStatement partiallyEvaluate(SState state) {
    List<Pair<SState, SStatement>> evaluatedChoices = new ArrayList<>();
    {
      SChoice choice = SBranching.choice(this.branches, this.elseStatement);
      SState preState = state;
      while (true) {
        SStatement evaluatedChoice = choice.partiallyEvaluate(preState);
        evaluatedChoices.add(new Pair<>(preState, evaluatedChoice));
        if (evaluatedChoice.isBreaking(preState))
          break;
        SState postState = evaluatedChoice.execute(preState);
        if (evaluatedChoices.stream().map(p -> p.getFirst()).anyMatch(pre -> pre.subsumes(postState)))
          break;
        preState = postState;
      }
    }

    if (evaluatedChoices.size() == 0 ||
        Lists.last(evaluatedChoices).getSecond().execute(Lists.last(evaluatedChoices).getFirst()).isInconsistent())
      return SCommand.await(SGuard.falseConst());

    if (// evaluatedChoices.stream().allMatch(p -> p.getSecond().isTerminating(p.getFirst())) &&
        Lists.last(evaluatedChoices).getSecond().isBreaking(Lists.last(evaluatedChoices).getFirst()))
      // return SControl.seq(evaluatedChoices.stream().map(p -> p.getSecond().replaceTopLevelBreaks(SCommand.skip())).collect(toList()));
      return evaluatedChoices.stream().map(p -> p.getSecond().replaceTopLevelBreaks(SCommand.skip())).reduce(SCommand.skip(), SStatement::merge);

    SState loopPreState = evaluatedChoices.stream().map(p -> p.getFirst()).reduce(state.empty(), (s1, s2) -> s1.join(s2));
    return SBranching.loop(evaluatedChoices.stream().map(p -> p.getSecond().propagate(p.getFirst().guard().reduce(loopPreState))).reduce(Sets.empty(), Sets::union));
  }

  @Override
  public String declaration(String prefix) {
    return super.declaration(prefix, "do", "od");
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SLoop other)
      return super.equals(other);
    return false;
  }
}