package uml.smile;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Collectors.partitioningBy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Lists;
import util.Sets;


@NonNullByDefault
public class SChoice extends SBranching {
  SChoice(Collection<SBranch> branches, @Nullable SStatement elseStatement) {
    super(branches, elseStatement);
  }

  @Override
  SChoice make(Collection<SBranch> branches, @Nullable SStatement elseStatement) {
    return new SChoice(branches, elseStatement); 
  }

  @Override
  public List<SCommand> getUnusedAssignments(final List<SCommand> assignments, List<SCommand> overwrittenAssignments) {
    List<SBranch> branches = this.branches;
    SStatement elseStatement = this.elseStatement;
    Set<SVariable> usedVariables = new HashSet<>();
    List<SCommand> localAssignments = assignments;

    List<List<SCommand>> unusedAssignmentsList = new ArrayList<>();
    List<List<SCommand>> overwrittenAssignmentsList = new ArrayList<>();
    for (SBranch branch : branches) {
      usedVariables.addAll(branch.getGuard().getVariables());
      List<SCommand> branchOverwrittenAssignments = new ArrayList<>();
      overwrittenAssignmentsList.add(branchOverwrittenAssignments);
      unusedAssignmentsList.add(branch.getUnusedAssignments(localAssignments, branchOverwrittenAssignments));
    }
    if (elseStatement != null) {
      List<SCommand> elseOverwrittenAssignments = new ArrayList<>();
      overwrittenAssignmentsList.add(elseOverwrittenAssignments);
      // An else branch "uses" all variables used in branch guards
      unusedAssignmentsList.add(elseStatement.getUnusedAssignments(localAssignments.stream().filter(a -> Sets.intersection(usedVariables, a.getAssignedVariables()).equals(Sets.empty())).collect(toList()), elseOverwrittenAssignments));
    }

    // Declare as overwritten all assignments that are overwritten in a branch but have not been present at entry
    overwrittenAssignments.addAll(overwrittenAssignmentsList.stream().map(aL -> aL.stream().filter(a1 -> localAssignments.stream().noneMatch(a2 -> a1 == a2)).collect(toList())).reduce(Lists.empty(), Lists::append));
    // Declare as overwritten all assignments on entry that are overwritten in all branches
    overwrittenAssignments.addAll(localAssignments.stream().filter(a1 -> overwrittenAssignmentsList.stream().allMatch(assignmentsList -> assignmentsList.stream().anyMatch(a2 -> a1 == a2))).collect(toList()));

    // Declare as unused all assignments that are unused in a branch but have not been present at entry and are not overwritten
    List<SCommand> unusedAssignments = unusedAssignmentsList.stream().map(aL -> aL.stream().filter(a1 -> overwrittenAssignments.stream().noneMatch(a2 -> a1 == a2) &&
                                                                                                           localAssignments.stream().noneMatch(a2 -> a1 == a2)).collect(toList())).reduce(Lists.empty(), Lists::append);
    // Declare as unused all assignments on entry that are still unused in all branches and are not overwritten
    unusedAssignments.addAll(localAssignments.stream().filter(a1 -> overwrittenAssignments.stream().noneMatch(a2 -> a1 == a2) &&
                                                                    unusedAssignmentsList.stream().allMatch(aL -> aL.stream().anyMatch(a2 -> a1 == a2))).collect(toList()));

    return unusedAssignments;
  }

  @Override
  Set<SBranch> propagate(SGuard guard) {
    Set<SBranch> propagatedBranches = this.branches.stream().map(branch -> branch.propagate(guard)).collect(toSet());
    SStatement elseStatement = this.elseStatement;
    if (elseStatement != null) {
      SGuard propagatedElseGuard = SGuard.and(guard, propagatedBranches.stream().map(branch -> SGuard.neg(branch.getGuard())).reduce(SGuard.trueConst(), SGuard::and));
      propagatedBranches.addAll(elseStatement.propagate(propagatedElseGuard));
    }

    return propagatedBranches;
  }

  @Override
  public SChoice replaceTopLevelBreaks(SCommand newCommand) {
    for (SBranch branch : this.branches)
      branch.replaceTopLevelBreaks(newCommand);
    @Nullable SStatement elseStatement = this.elseStatement;
    if (elseStatement != null)
      elseStatement.replaceTopLevelBreaks(newCommand);
    return this;
  }

  /**
   * Flatten this choice, i.e., propagate the guard of each branch to its
   * effect such that nested choices are flattened.
   *
   * @return flattened choice
   */
  SStatement flatten() {
    if (this.getAllBranches().stream().map(b -> b.getGuard().size()).reduce(0, (a, b) -> Integer.max(a, b)) >= 2)
      return this;
    var flattenedBranches = new HashSet<SBranch>();
    for (var branch : this.branches)
      flattenedBranches.addAll(branch.getEffect().propagate(branch.getGuard()));
    var elseStatement = this.elseStatement;
    if (elseStatement != null) {
      var elseGuard = flattenedBranches.stream().map(b -> SGuard.neg(b.getGuard())).reduce(SGuard.trueConst(), SGuard::and);
      var elseBranches = elseStatement.propagate(elseGuard);
      flattenedBranches.addAll(elseBranches);
    }
    return SBranching.choice(flattenedBranches).reconstructElse();
  }

  @Override
  SStatement merge(SStatement statement) {
    Set<SBranch> mergedBranches = this.branches.stream().map(branch -> branch.merge(statement)).collect(toSet());
    SStatement elseStatement = this.elseStatement;
    if (elseStatement != null)
      return SBranching.choice(mergedBranches, SControl.seq(elseStatement, statement));
    return SBranching.choice(mergedBranches).reconstructElse();  
  }

  @Override
  public @Nullable SStatement getResumption(SPrimitive from, SVariable breakFlag) {
    for (var branch : this.branches) {
      var fromStatement = branch.getResumption(from, breakFlag);
      if (fromStatement != null)
        return fromStatement;
    }
    var elseStatement = this.elseStatement;
    if (elseStatement != null)
      return elseStatement.getResumption(from, breakFlag);
    return null;
  }

  @Override
  public SStatement getCycle(SVariable suspendFlag) {
    var found = false;
    var cycleBranches = new ArrayList<SBranch>();
    for (var branch : this.branches) {
      var cycleBranch = branch.getCycle(suspendFlag);
      if (cycleBranch != branch) {
        cycleBranches.add(cycleBranch);
        found = true;
      }
      else
        cycleBranches.add(branch);
    }
    var elseStatement = this.elseStatement;
    var cycleElseStatement = elseStatement != null ? elseStatement.getCycle(suspendFlag) : null;
    if (cycleElseStatement != elseStatement)
      found = true;
    if (!found)
      return this;
    return SBranching.choice(cycleBranches, cycleElseStatement);
  }

  @Override
  public <T> T accept(Visitor<T> visitor) {
    return visitor.onChoice(this.branches, this.elseStatement);
  }

  @Override
  public SState execute(SState state) {
    return this.getAllBranches().stream().map(b -> b.execute(state)).reduce(state.empty(), (s1, s2) -> s1.join(s2));
  }

  @Override
  public boolean isTerminating(SState state) {
    Set<SBranch> enabledBranches = this.getAllBranches().stream().filter(b -> b.getGuard().isTrue(state)).collect(toSet());
    return !enabledBranches.isEmpty() && enabledBranches.stream().allMatch(b -> b.getEffect().isTerminating(state));
  }

  @Override
  public boolean isBreaking(SState state) {
    Set<SBranch> enabledBranches = this.getAllBranches().stream().filter(b -> b.getGuard().isTrue(state)).collect(toSet());
    return !enabledBranches.isEmpty() && enabledBranches.stream().allMatch(b -> b.getEffect().isBreaking(state));
  }

  @Override
  public SStatement partiallyEvaluate(SState state) {
    if (this.branches.isEmpty() && this.elseStatement == null)
      return SCommand.await(SGuard.falseConst());

    SChoice peChoice = SBranching.choice();
    for (SBranch branch : this.branches)
      peChoice.addBranch(branch.partiallyEvaluate(state));
    @Nullable SStatement elseStatement = this.elseStatement;
    if (elseStatement != null) {
      var peElseBranch = SBranch.branch(peChoice.getElseGuard(), elseStatement).partiallyEvaluate(state);
      if (!peElseBranch.getGuard().isFalse(state)) {
        var peElseStatement = peElseBranch.getEffect();
        if (peElseStatement instanceof SChoice) {
          var peElseChoice = (SChoice)peElseStatement;
          var peElseChoiceSplitMap = peElseChoice.branches.stream().collect(partitioningBy(branch -> branch.getGuard().isFalse(state.withConstraint(SGuard.neg(peChoice.getElseGuard())))));
          var peElseChoiceComplementaryBranches = peElseChoiceSplitMap.get(true);
          if (peElseChoiceComplementaryBranches != null)
            peChoice.addBranches(peElseChoiceComplementaryBranches);
          var peElseChoiceRemainingBranches = peElseChoiceSplitMap.get(false);
          if (peElseChoiceRemainingBranches != null && !peElseChoiceRemainingBranches.isEmpty())
            peChoice.setElseStatement(new SChoice(peElseChoiceRemainingBranches, peElseChoice.elseStatement));
          else {
            if (peElseChoice.elseStatement != null)
              peChoice.setElseStatement(peElseChoice.elseStatement);
          }
        }
        else
          peChoice.setElseStatement(peElseStatement);
      }
    }

    Set<SBranch> peBranches = peChoice.getAllBranches();
    if (peBranches.isEmpty())
      return SCommand.await(SGuard.falseConst());
    if (peBranches.size() == 1) {
      var peBranch = peBranches.iterator().next();
      if (peBranch.getGuard().isTrue(state))
        return peBranch.getEffect();
      return SCommand.await(peBranch.getGuard()).merge(peBranch.getEffect());
    }

    return peChoice.flatten();
  }

  @Override
  public String declaration(String prefix) {
    return super.declaration(prefix, "if", "fi");
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SChoice other)
      return super.equals(other);
    return false;
  }
}
