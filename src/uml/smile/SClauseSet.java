package uml.smile;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Sets;

import static java.util.stream.Collectors.toList;

import java.util.Collections;


/**
 * Disjunctive clause sets
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 *
 */
@NonNullByDefault
class SClauseSet {
  Set<SClause> clauses = new HashSet<>();

  private static final SClauseSet EMPTYCLAUSESET = new SClauseSet();
  private static final SClauseSet TRUECLAUSESET = new SClauseSet(SClause.trueClause());

  /**
   * Create an empty clause set representing false.
   */
  private SClauseSet() {
  }

  SClauseSet(SClause clause) {
    this.clauses.add(clause);
  }

  SClauseSet(Set<SClause> clauses) {
    this.clauses.addAll(SClause.propagate(SClause.join(clauses)));
  }

  /**
   * @return clause set representing false
   */
  static SClauseSet falseClauseSet() {
    return EMPTYCLAUSESET;
  }

  /**
   * @return clause set representing true
   */
  static SClauseSet trueClauseSet() {
    return TRUECLAUSESET;
  }

  /**
   * @return this clause set's size
   */
  int size() {
    return this.clauses.size();
  }

  SClauseSet neg() {
    return this.clauses.stream().map(c -> c.neg()).reduce(SClauseSet.trueClauseSet(), (s1, s2) -> s1.and(s2));
  }

  SClauseSet and(SLiteral literal) {
    // return this.clauses.stream().map(c -> c.and(literal)).reduce(SClauseSet.falseClauseSet(), (s1, s2) -> s1.or(s2));
    return new SClauseSet(this.clauses.stream().map(c -> c.and(literal).clauses).reduce(Sets.empty(), Sets::union));
  }

  SClauseSet and(SClause clause) {
    // return this.clauses.stream().map(c -> c.and(clause)).reduce(SClauseSet.falseClauseSet(), (s1, s2) -> s1.or(s2));
    return new SClauseSet(this.clauses.stream().map(c -> c.and(clause).clauses).reduce(Sets.empty(), Sets::union));
  }

  SClauseSet and(SClauseSet clauseSet) {
    // return this.clauses.stream().map(c -> clauseSet.and(c)).reduce(SClauseSet.falseClauseSet(), (s1, s2) -> s1.or(s2));
    return new SClauseSet(this.clauses.stream().map(c -> clauseSet.and(c).clauses).reduce(Sets.empty(), Sets::union));
  }

  SClauseSet or(SLiteral literal) {
    // return new SClauseSet(this.clauses.stream().map(c -> c.or(literal).clauses).reduce(Sets.empty(), Sets::union));
    return new SClauseSet(Sets.union(this.clauses, SClause.trueClause().and(literal).clauses));
  }

  SClauseSet or(SClause clause) {
    // return new SClauseSet(this.clauses.stream().map(c -> c.or(clause).clauses).reduce(Sets.empty(), Sets::union));
    return new SClauseSet(Sets.union(this.clauses, Sets.singleton(clause)));
  }

  SClauseSet or(SClauseSet clauseSet) {
    return new SClauseSet(Sets.union(this.clauses, clauseSet.clauses));
  }

  /**
   * @return whether this clause set is true
   */
  public boolean isTrue() {
    return this.clauses.stream().anyMatch(SClause::isTrue);
  }

  /**
   * @return whether this clause set is false
   */
  public boolean isFalse() {
    return this.clauses.isEmpty();
  }

  /**
   * Determine whether this clause set subsumes another clause set, i.e., whether
   * {@code this} clause set is true if the {@code other} clause set is true.
   * @param other another clause set
   * @return whether {@code this} clause set subsumes the {@code other} clause set
   */
  public boolean subsumes(SClauseSet other) {
    return this.clauses.stream().allMatch(thisClause -> other.clauses.stream().anyMatch(otherClause -> thisClause.subsumes(otherClause)));
  }

  /**
   * @return set of variables used in this clause set
   */
  public Set<SVariable> getVariables() {
    return this.clauses.stream().map(g -> g.getVariables()).reduce(Sets.empty(), Sets::union);
  }

  /**
   * @return set of constants used in this clause set
   */
  public Set<SConstant> getConstants() {
    return this.clauses.stream().map(g -> g.getConstants()).reduce(Sets.empty(), Sets::union);
  }

  /**
   * Accept a visitor.
   *
   * @param visitor a visitor
   * @return visit's result
   */
  public <T> T accept(SGuard.Visitor<T> visitor) {
    if (this.clauses.isEmpty())
      return visitor.onLiteral(SLiteral.falseConst());
    if (this.clauses.size() == 1)
      return this.clauses.iterator().next().accept(visitor);
    SClause firstClause = this.clauses.iterator().next();
    Set<SClause> restClauses = new HashSet<>(this.clauses);
    restClauses.remove(firstClause);
    return visitor.onOr(SGuard.clause(firstClause), SGuard.clauseSet(new SClauseSet(restClauses)));
  }
    
  /**
   * Evaluate this clause set in a state.
   *
   * @param state a state
   * @return set of values
   */
  public Set<SValue> evaluate(SState state) {
    return this.clauses.stream().map(c -> c.evaluate(state)).
                                 reduce(Sets.singleton(SValue.falseValue()), (s1, s2) -> SValue.or(s1, s2));
  }

  /**
   * Reduce this clause set in a state.
   *
   * @param state a state
   * @return reduced clause set
   */
  public SClauseSet reduce(SState state) {
    // return this.clauses.stream().map(c -> c.reduce(state)).reduce(SClauseSet.empty(), (s1, s2) -> s1.or(s2));
    return new SClauseSet(this.clauses.stream().map(c -> c.reduce(state).clauses).reduce(Sets.empty(), Sets::union));
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.clauses);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SClauseSet other)
      return Objects.equals(this.clauses, other.clauses);
    return false;
  }

  @Override
  public String toString() {
    if (this.clauses.isEmpty())
      return SLiteral.falseConst().toString();
    List<String> clauseStrings = this.clauses.stream().map(SClause::toString).collect(toList());
    Collections.sort(clauseStrings, (s1, s2) -> s1.compareTo(s2));
    return Formatter.separated(clauseStrings, " || ");
  }
}
