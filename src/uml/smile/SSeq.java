package uml.smile;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class SSeq extends SControl {
  private SStatement leftStatement;
  private SStatement rightStatement;

  SSeq(SStatement leftStatement, SStatement rightStatement) {
    this.leftStatement = leftStatement;
    this.rightStatement = rightStatement;
  }

  @Override
  boolean isEmpty() {
    return leftStatement.isEmpty() && rightStatement.isEmpty();
  }

  @Override
  int getComplexity() {
     return this.leftStatement.getComplexity() + this.rightStatement.getComplexity();
  }

  @Override
  public Set<SPrimitive> getPrimitives() {
    Set<SPrimitive> primitives = new HashSet<>();
    primitives.addAll(this.leftStatement.getPrimitives());
    primitives.addAll(this.rightStatement.getPrimitives());
    return primitives;
  }

  @Override
  public Set<SConstant> getConstants() {
    Set<SConstant> constants = new HashSet<>();
    constants.addAll(this.leftStatement.getConstants());
    constants.addAll(this.rightStatement.getConstants());
    return constants;
  }

  @Override
  public Set<SConstant> getConstantsAssigned(Collection<SVariable> variables) {
    Set<SConstant> constants = new HashSet<>();
    constants.addAll(this.leftStatement.getConstantsAssigned(variables));
    constants.addAll(this.rightStatement.getConstantsAssigned(variables));
    return constants;
  }

  @Override
  public SStatement replace(Collection<SCommand> commands, SCommand newCommand) {
    this.leftStatement.replace(commands, newCommand);
    this.rightStatement.replace(commands, newCommand);
    return this;
  }

  @Override
  public SStatement replaceTopLevelBreaks(SCommand newCommand) {
    this.leftStatement.replaceTopLevelBreaks(newCommand);
    this.rightStatement.replaceTopLevelBreaks(newCommand);
    return this;
  }

  @Override
  public @Nullable SStatement getResumption(SPrimitive from, SVariable breakFlag) {
    var fromStatement = this.leftStatement.getResumption(from, breakFlag);
    if (fromStatement != null)
      return seq(fromStatement, this.rightStatement);
    return this.rightStatement.getResumption(from, breakFlag);
  }

  @Override
  public SStatement getCycle(SVariable suspendFlag) {
    var cycleLeftStatement = this.leftStatement.getCycle(suspendFlag);
    if (cycleLeftStatement != this.leftStatement)
      return cycleLeftStatement.merge(SControl.ifThen(SGuard.literal(SLiteral.eq(suspendFlag, false)), this.rightStatement.getCycle(suspendFlag)));
    var cycleRightStatement = this.rightStatement.getCycle(suspendFlag);
    if (cycleRightStatement != this.rightStatement)
      return SControl.seq(this.leftStatement, cycleRightStatement);
    return this;
  }

  @Override
  Set<SVariable> getAssignedVariables() {
    Set<SVariable> assignedVariables = new HashSet<>();
    assignedVariables.addAll(this.leftStatement.getAssignedVariables());
    assignedVariables.addAll(this.rightStatement.getAssignedVariables());
    return assignedVariables;
  }

  @Override
  Set<SVariable> getUsedVariables() {
    Set<SVariable> usedVariables = new HashSet<>();
    usedVariables.addAll(this.leftStatement.getUsedVariables());
    usedVariables.addAll(this.rightStatement.getUsedVariables());
    return usedVariables;
  }

  @Override
  public List<SCommand> getUnusedAssignments(final List<SCommand> assignments, List<SCommand> overwrittenAssignments) {
    return this.rightStatement.getUnusedAssignments(this.leftStatement.getUnusedAssignments(assignments, overwrittenAssignments), overwrittenAssignments);
  }

  /**
   * Accept a visitor
   */
  public <T> T accept(Visitor<T> visitor) {
    return visitor.onSequential(this.leftStatement, this.rightStatement);
  }

  @Override
  public <T> T accept(SStatement.Visitor<T> visitor) {
    return this.accept((Visitor<T>)visitor);
  }

  @Override
  public SState execute(SState state) {
    return this.rightStatement.execute(this.leftStatement.execute(state));
  }

  @Override
  public boolean isTerminating(SState state) {
    return this.leftStatement.isTerminating(state) && this.rightStatement.isTerminating(this.leftStatement.execute(state));
  }

  @Override
  public boolean isBreaking(SState state) {
    return this.leftStatement.isBreaking(state) || (this.leftStatement.isTerminating(state) && this.rightStatement.isBreaking(leftStatement.execute(state)));
  }

  @Override
  public SStatement partiallyEvaluate(SState state) {
    SStatement leftResult = this.leftStatement.partiallyEvaluate(state);
    if (leftResult.execute(state).isInconsistent())
      return leftResult;
    SStatement rightResult = this.rightStatement.partiallyEvaluate(leftResult.execute(state));
    return SControl.seq(leftResult, rightResult);
  }

  @Override
  public SStatement getSimplified() {
    SStatement leftResult = this.leftStatement.getSimplified();
    SStatement rightResult = this.rightStatement.getSimplified();
    // return SControl.seq(leftResult, rightResult);
    return leftResult.merge(rightResult);
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.leftStatement.declaration(prefix));
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append(this.rightStatement.declaration(prefix));

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.leftStatement,
                        this.rightStatement);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SSeq other)
      return Objects.equals(this.leftStatement, other.leftStatement) &&
             Objects.equals(this.rightStatement, other.rightStatement);
    return false;
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
