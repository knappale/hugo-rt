package uml.smile;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UContext;
import uml.UExpression;
import uml.UOperator;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import util.Formatter;
import util.Maps;
import util.Sets;

import static java.util.stream.Collectors.toSet;


@NonNullByDefault
public class SState {
  private SMachine machine;
  private Map<SVariable, Set<SValue>> variablesMap = new HashMap<>();
  private Set<UEvent> uEvents = new HashSet<>();
  private UExpression uParametersConstraint = UExpression.trueConst();
  private Map<UState, Set<SValue>> uCompletionsMap = new HashMap<>();

  private static final Set<SValue> BOOLEANVALUES = Sets.elements(SValue.falseValue(), SValue.trueValue());

  private SState(SMachine machine) {
    this.machine = machine;
  }

  private SState copy() {
    SState s = new SState(this.machine);
    s.variablesMap = new HashMap<>(this.variablesMap);
    s.uEvents = new HashSet<>(this.uEvents);
    s.uParametersConstraint = this.uParametersConstraint;
    s.uCompletionsMap = new HashMap<>(this.uCompletionsMap);
    return s;
  }

  public static SState initial(SMachine machine) {
    SState s = new SState(machine);
    for (SVariable variable : machine.getVariables())
      s.variablesMap.put(variable, Sets.singleton(variable.getInitialValue()));
    s.uEvents = new HashSet<>();
    s.uParametersConstraint = UExpression.trueConst();
    for (UState uState : machine.getCompletionStates())
      s.uCompletionsMap.put(uState, Sets.singleton(SValue.falseValue()));
    return s;
  }

  public static SState intermediate(SMachine machine) {
    SState s = new SState(machine);
    for (SVariable variable : machine.getVariables()) {
      if (variable.isState())
        s.variablesMap.put(variable, variable.getDomain());
      else
        s.variablesMap.put(variable, Sets.singleton(variable.getInitialValue()));
    }
    s.uEvents = new HashSet<>();
    s.uParametersConstraint = UExpression.trueConst();
    for (UState uState : machine.getCompletionStates())
      s.uCompletionsMap.put(uState, BOOLEANVALUES);
    return s;
  }

  public SState empty() {
    SState s = this.copy();
    s.machine = this.machine;
    for (SVariable variable : this.machine.getVariables())
      s.variablesMap.put(variable, new HashSet<>());
    s.uEvents = new HashSet<>();
    s.uParametersConstraint = UExpression.trueConst();
    for (UState uState : this.machine.getCompletionStates())
      s.uCompletionsMap.put(uState, new HashSet<>());
    return s;
  }

  public SState withAssignment(SVariable variable, Set<SValue> values) {
    SState s = this.copy();
    s.variablesMap.put(variable, values);
    return s;
  }

  public SState withAssignment(SVariable variable, SValue value) {
    return this.withAssignment(variable, Sets.singleton(value));
  }

  public SState withUnknownAssignment(SVariable variable) {
    return this.withAssignment(variable, variable.getDomain());
  }

  public SState withEvents(Set<UEvent> uEvents) {
    SState s = this.copy();
    s.uEvents = new HashSet<>(uEvents);
    s.uParametersConstraint = UExpression.trueConst();
    return s;
  }

  public SState withEvent(UEvent uEvent) {
    return this.withEvents(Sets.singleton(uEvent));
  }

  public SState withNoEvent() {
    return this.withEvents(new HashSet<>());
  }

  public SState withSomeEvent() {
    return this.withEvents(this.machine.getEventDomain());
  }

  public SState withSomeState() {
    /*
    return this.copy().withConstraint(this.machine.getStateMachine().getConfigurations().stream().filter(c -> !c.isPseudo()).map(uConfiguration ->
      uConfiguration.getVertices().stream().map(uVertex -> this.machine.getSymbolTable().checkVertex(uVertex)).reduce(SGuard.trueConst(), SGuard::and)).reduce(SGuard.falseConst(), SGuard::or));
    */

    SState s = this.copy();
    SGuard notEmptyGuard = SGuard.trueConst();
    for (SVariable variable : this.machine.getVariables()) {
      if (!variable.isState() || variable.isHistory())
        continue;
      s.variablesMap.put(variable, variable.getDomain().stream().filter(value -> { var constant = value.getConstant();
                                                                                   return (constant != null) &&
                                                                                          (constant.getKind() == SConstant.Kind.VERTEX) &&
                                                                                          (constant.getVertex().getKind() == null); }).collect(toSet()));
      notEmptyGuard = SGuard.or(notEmptyGuard, SGuard.literal(SLiteral.neq(variable, SConstant.empty())));
    }
    return s.withConstraint(notEmptyGuard);
  }

  private SState withCompleted(UState uState, Set<SValue> values) {
    SState s = this.copy();
    s.uCompletionsMap.put(uState, values);
    return s;
  }

  public SState withCompleted(UState uState) {
    return this.withCompleted(uState, Sets.singleton(SValue.trueValue()));
  }

  public SState withUncompleted(UState uState) {
    return this.withCompleted(uState, Sets.singleton(SValue.falseValue()));
  }

  public SState withUnknownCompleted(UState uState) {
    return this.withCompleted(uState, BOOLEANVALUES);
  }

  public SState withParametersConstraints(UExpression uExpression, UContext uContext) {
    if (this.uEvents.size() != 1)
      return this;
    SState s = this.copy();
    s.uParametersConstraint = uExpression.getTopLevelLiterals(UOperator.AND, uContext).stream().filter(uE -> uE.getDependencies(uContext).stream().allMatch(uS -> uS.isConstant())).reduce(this.uParametersConstraint, UExpression::and);
    return s;
  }

  public SState withConstraint(SGuard guard) {
    return guard.new Cases<SState>().
             literal(literal -> literal.new Cases<SState>().
                                   booleanConstant(positive -> positive ? this : this.empty()).
                                   eq((positive, variable, values) ->
                                        this.withAssignment(variable, Sets.intersection(this.getValues(variable), positive ? values
                                                                                                                           : Sets.difference(variable.getDomain(), values)))).
                                   match((positive, uEvents) ->
                                           this.withEvents(Sets.intersection(this.getEvents(), positive ? uEvents
                                                                                                        : Sets.difference(this.machine.getEventDomain(), uEvents)))).
                                   completed((positive, uState) ->
                                               this.withCompleted(uState, Sets.intersection(this.getCompleteds(uState), positive ? Sets.singleton(SValue.trueValue())
                                                                                                                                 : Sets.singleton(SValue.falseValue())))).
                                   external((positive, uExpression, uContext) -> positive ? this.withParametersConstraints(uExpression, uContext)
                                                                                          : this.withParametersConstraints(UExpression.neg(uExpression), uContext)).
                                   otherwise(() -> this).
                                   apply()).
             and((leftGuard, rightGuard) -> this.withConstraint(leftGuard).meet(this.withConstraint(rightGuard))).
             or((leftGuard, rightGuard) -> {
                                             if (leftGuard.isFalse(this))
                                               return this.withConstraint(rightGuard);
                                             if (rightGuard.isFalse(this))
                                               return this.withConstraint(leftGuard);
                                             return this.withConstraint(leftGuard).join(this.withConstraint(rightGuard));
                                           }).
             otherwise(() -> this).
             apply();
  }

  public SState join(SState other) {
    SState s = this.copy();
    s.variablesMap = Maps.combine(this.variablesMap, other.variablesMap, Sets::union);
    s.uEvents.addAll(other.uEvents);
    s.uParametersConstraint = UExpression.or(this.uParametersConstraint, other.uParametersConstraint);
    s.uCompletionsMap = Maps.combine(this.uCompletionsMap, other.uCompletionsMap, Sets::union);
    return s;
  }

  public SState meet(SState other) {
    SState s = this.copy();
    s.variablesMap = Maps.combine(this.variablesMap, other.variablesMap, Sets::intersection);
    s.uEvents = Sets.intersection(this.uEvents, other.uEvents);
    s.uParametersConstraint = UExpression.and(this.uParametersConstraint, other.uParametersConstraint);
    s.uCompletionsMap = Maps.combine(this.uCompletionsMap, other.uCompletionsMap, Sets::intersection);
    return s;
  }

  /**
   * Check whether this state subsumes another state.
   *
   * This returns {@code true} if, and only if, any concrete state
   * (with values for the variables, the current event, &c.) that satisfies
   * the other state also satisfies this state.
   *
   * @param other another state
   * @return whether {@code this} state subsumes the {@code other} state
   */
  public boolean subsumes(SState other) {
    return other.isInconsistent() || this.join(other).equals(this);
  }

  /**
   * Determine the values of a variable in this state.
   *
   * @param variable a variable
   * @return the values of {@code variable} in this state
   */
  public Set<SValue> getValues(SVariable variable) {
    Set<SValue> values = this.variablesMap.get(variable);
    if (values == null)
      return new HashSet<>();
    return values;
  }

  /**
   * @return the events in this state
   */
  public Set<UEvent> getEvents() {
    return this.uEvents;
  }

  /**
   * @return the boolean values in this state
   */
  public Set<SValue> getBooleans() {
    return BOOLEANVALUES;
  }

  /**
   * Determine the completion status of a UML state in this state.
   *
   * @param uState a UML state
   * @return the completion status of {@code uState} in this state
   */
  public Set<SValue> getCompleteds(UState uState) {
    Set<SValue> values = this.uCompletionsMap.get(uState);
    if (values == null)
      return new HashSet<>();
    return values;
  }

  /**
   * Determine whether this state is inconsistent, i.e., whether there
   * is no concrete state that can satisfy this abstract state.
   *
   * @return whether this state is inconsistent
   */
  public boolean isInconsistent() {
    for (SVariable variable : this.machine.getVariables()) {
      Set<SValue> values = this.variablesMap.get(variable);
      if ((values == null || values.isEmpty()) && !variable.getDomain().isEmpty())
        return true;
    }

    // The events part cannot contribute to inconsistency.

    if (this.uEvents.size() == 1) {
      var uClass = this.machine.getStateMachine().getC1ass();
      var uEvent = this.uEvents.iterator().next();
      if (this.uParametersConstraint.isContradictory(UContext.event(uClass, uEvent)))
        return true;
    }

    for (UState uState : this.machine.getCompletionStates()) {
      Set<SValue> values = this.uCompletionsMap.get(uState);
      if (values == null || values.isEmpty())
        return true;
    }
    return false;
  }

  /**
   * Compute a guard from this state.
   *
   * @return a guard representing this state
   */
  public SGuard guard() {
    if (this.isInconsistent())
      return SGuard.falseConst();

    SGuard guard = SGuard.trueConst();
    for (SVariable variable : this.machine.getVariables()) {
      Set<SValue> values = this.variablesMap.get(variable);
      if (values != null && !values.isEmpty() && !values.equals(variable.getDomain()))
        guard = SGuard.and(guard, SGuard.literal(SLiteral.eq(variable, values)));
    }

    if (!this.uEvents.isEmpty() && !this.uEvents.equals(this.machine.getEventDomain()))
      guard = SGuard.and(guard, SGuard.literal(SLiteral.match(this.uEvents)));

    if (this.uEvents.size() == 1) {
      var uClass = this.machine.getStateMachine().getC1ass();
      var uEvent = this.uEvents.iterator().next();
      guard = SGuard.and(guard, SGuard.literal(SLiteral.external(this.uParametersConstraint, UContext.event(uClass, uEvent))));
    }

    for (UState uState : this.machine.getCompletionStates()) {
      Set<SValue> values = this.uCompletionsMap.get(uState);
      if (values == null || values.size() != 1)
        continue;
      guard = SGuard.and(guard, SValue.falseValue().equals(values.iterator().next()) ? SGuard.neg(SGuard.literal(SLiteral.isCompleted(uState))) : SGuard.literal(SLiteral.isCompleted(uState)));
    }

    return guard;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.machine,
                        this.variablesMap,
                        this.uEvents,
                        this.uParametersConstraint,
                        this.uCompletionsMap);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SState other)
      return Objects.equals(this.machine, other.machine) &&
             Objects.equals(this.variablesMap, other.variablesMap) &&
             Objects.equals(this.uEvents, other.uEvents) &&
             Objects.equals(this.uParametersConstraint, other.uParametersConstraint) &&
             Objects.equals(this.uCompletionsMap, other.uCompletionsMap);
    return false;
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.machine.getName());
    resultBuilder.append(": variables = ");
    resultBuilder.append(Formatter.map(this.variablesMap, (k -> k.getName()), Formatter::set));
    resultBuilder.append(", events = ");
    resultBuilder.append(Formatter.set(this.uEvents));
    resultBuilder.append(", parameter constraint = ");
    resultBuilder.append(this.uParametersConstraint);
    resultBuilder.append(", completions = ");
    resultBuilder.append(Formatter.map(this.uCompletionsMap, (k -> k.getIdentifier().toString()), Formatter::set));
    return resultBuilder.toString();
  }
}
