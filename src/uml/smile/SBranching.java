package uml.smile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Sets;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;


@NonNullByDefault
public abstract class SBranching extends SControl {
  List<SBranch> branches = new ArrayList<>();
  @Nullable SStatement elseStatement;

  public interface Visitor<T> {
    public T onChoice(List<SBranch> branches, @Nullable SStatement elseStm);
    public T onLoop(List<SBranch> branches, @Nullable SStatement elseStm);
  }

  SBranching(Collection<SBranch> branches, @Nullable SStatement elseStatement) {
    for (SBranch branch : branches)
      this.addBranch(branch);
    if (elseStatement != null)
      this.setElseStatement(elseStatement);
  }

  /**
   * Make a branching of the same concrete type as this branching.
   *
   * @param branches a collection of branches
   * @param elseStatement a statement
   * @return a new branching of the same concrete type as this branching
   */
  abstract SBranching make(Collection<SBranch> branches, @Nullable SStatement elseStatement);

  /**
   * Create a choice branches and an else-clause.
   * 
   * @param branches branches
   * @param elseStatement else statement
   * @return a choice
   */
  public static SChoice choice(Collection<SBranch> branches, @Nullable SStatement elseStatement) {
    return new SChoice(branches, elseStatement);
  }

  /**
   * Create a choice between branches.
   * 
   * @param sBranches branches
   * @return a choice
   */
  public static SChoice choice(@NonNull SBranch... branches) {
    return SBranching.choice(Arrays.asList(branches), null);
  }

  /**
   * Create a choice between branches.
   * 
   * @param sBranches branches
   * @return a choice
   */
  public static SChoice choice(Collection<SBranch> branches) {
    return SBranching.choice(branches, null);
  }

  /**
   * Create a choice between (empty) branches.
   *
   * @return a choice
   */
  public static SChoice choice() {
    return SBranching.choice(Sets.empty());
  }

  /**
   * Create a loop through branches and an else-clause.
   * 
   * @param branches branches
   * @param elseStatement else clause statement
   * @return a loop
   */
  public static SLoop loop(Collection<SBranch> branches, @Nullable SStatement elseStatement) {
    return new SLoop(branches, elseStatement);
  }

  /**
   * Create a loop through branches.
   * 
   * @param branches branches
   * @return a loop
   */
  public static SLoop loop(Collection<SBranch> branches) {
    return SBranching.loop(branches, null);
  }

  /**
   * Create a loop through (empty) branches.
   * 
   * @return a loop
   */
  public static SLoop loop() {
    return SBranching.loop(Sets.empty());
  }

  /**
   * Add a branch to this branching.
   *
   * @param branch a branch
   * @return this branching extended by {@code branch}
   */
  public SBranching addBranch(SBranch branch) {
    SGuard branchGuard = branch.getGuard();
    if (branchGuard.equals(SGuard.falseConst()))
      return this;

    SStatement branchEffect = branch.getEffect();
    if (branchEffect.equals(this.elseStatement))
      return this;

    Set<SBranch> branchesWithSameEffect = this.branches.stream().filter(b -> b.getEffect().equals(branchEffect)).collect(toSet());
    if (!branchesWithSameEffect.isEmpty()) {
      this.branches.removeAll(branchesWithSameEffect);
      SGuard sameEffectGuard = Stream.concat(branchesWithSameEffect.stream().map(SBranch::getGuard), Stream.of(branchGuard)).reduce(SGuard.falseConst(), SGuard::or);
      this.branches.add(SBranch.branch(sameEffectGuard, branchEffect));
    }
    else
      this.branches.add(SBranch.branch(branchGuard, branchEffect));
    return this;
  }

  /**
   * Add branches to this branching.
   *
   * @param branches a collection of branches
   * @return this branching extended by {@code branches}
   */
  public SBranching addBranches(Collection<SBranch> branches) {
    for (SBranch branch : branches)
      this.addBranch(branch);
    return this;
  }

  /**
   * Set else branch statement.
   *
   * @param statement a statement
   */
  public SBranching setElseStatement(SStatement sStatement) {
    Set<SBranch> branchesWithSameEffect = this.branches.stream().filter(b -> b.getEffect().equals(sStatement)).collect(toSet());
    if (!branchesWithSameEffect.isEmpty())
      this.branches.removeAll(branchesWithSameEffect);
    this.elseStatement = sStatement;
    return this;
  }

  /**
   * If there is no else branch in this choice, determine whether
   * the "shortest" branch has a guard that is exactly the negation of
   * the conjunction of the remaining branches.  If this is the case,
   * consider this "shortest" branch as the new else branch.
   *
   * @return a choice with an else branch if possible
   */
  SBranching reconstructElse() {
    if (this.elseStatement != null)
      return this;

    @Nullable SBranch simplestBranch = this.branches.stream().sorted((b1, b2) -> b1.getComplexity() - b2.getComplexity()).findFirst().orElse(null);
    List<SBranch> otherBranches = this.branches.stream().filter(b -> !b.equals(simplestBranch)).collect(toList());
    if (simplestBranch == null)
      return this;
    if (!simplestBranch.getGuard().equals(SGuard.neg(SGuard.and(otherBranches.stream().map(SBranch::getGuard).collect(toSet())))))
      return this;

    return this.make(otherBranches, simplestBranch.getEffect());
  }

  /**
   * @return the guard for the else-branch of this branching
   */
  public SGuard getElseGuard() {
    return this.branches.stream().map(b -> SGuard.neg(b.getGuard())).reduce(SGuard.trueConst(), SGuard::and);
  }

  /**
   * @return all branches of this branching including the else branch if present
   */
  public Set<SBranch> getAllBranches() {
    Set<SBranch> allBranches = new HashSet<>(this.branches);
    SStatement elseStatement = this.elseStatement;
    if (elseStatement != null)
      allBranches.add(SBranch.branch(this.getElseGuard(), elseStatement));
    return allBranches;
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  int getComplexity() {
    @SuppressWarnings("null")
    int branchesComplexity = this.branches.stream().map(branch -> branch.getComplexity()).reduce(0, Math::max);
    return Math.max(branchesComplexity, this.elseStatement != null ? this.elseStatement.getComplexity() : 0) + 1;
  }

  @Override
  public Set<SPrimitive> getPrimitives() {
    Set<SPrimitive> primitives = new HashSet<>();
    for (var branch : this.branches)
      primitives.addAll(branch.getPrimitives());
    var elseStatement = this.elseStatement;
    if (elseStatement != null)
      primitives.addAll(elseStatement.getPrimitives());
    return primitives;
  }

  @Override
  public Set<SConstant> getConstants() {
    return this.getAllBranches().stream().map(b -> b.getConstants()).reduce(Sets.empty(), Sets::union);
  }

  @Override
  public Set<SConstant> getConstantsAssigned(Collection<SVariable> variables) {
    return this.getAllBranches().stream().map(b -> b.getConstantsAssigned(variables)).reduce(Sets.empty(), Sets::union);
  }

  @Override
  Set<SVariable> getAssignedVariables() {
    return this.getAllBranches().stream().map(b -> b.getAssignedVariables()).reduce(Sets.empty(), Sets::union);
  }

  @Override
  Set<SVariable> getUsedVariables() {
    return this.getAllBranches().stream().map(b -> b.getUsedVariables()).reduce(Sets.empty(), Sets::union);
  }

  @Override
  public SBranching replace(Collection<SCommand> commands, SCommand newCommand) {
    for (SBranch branch : this.branches)
      branch.replace(commands, newCommand);
    SStatement elseStatement = this.elseStatement;
    if (elseStatement != null)
      elseStatement.replace(commands, newCommand);
    return this;
  }

  /**
   * Determine whether this branching is deterministic in a state,
   * i.e., at most one branch is executable.
   *
   * @param sState a state
   * @return whether this branching is deterministic
   */
  public boolean isDeterministic(SState state) {
    return this.getAllBranches().stream().map(b -> b.getGuard()).filter(g -> g.isTrue(state)).count() <= 1;
  }

  @Override
  public SStatement getSimplified() {
    Set<SBranch> simplifiedBranches = this.branches.stream().map(branch -> branch.getSimplified()).collect(toSet());
    @Nullable SStatement elseStatement = this.elseStatement;
    if (elseStatement != null)
      return this.make(simplifiedBranches, elseStatement.getSimplified());
    return this.make(simplifiedBranches, null).reconstructElse();
  }

  /**
   * Accept a visitor.
   *
   * @param visitor a visitor
   * @return visit's result
   */
  abstract <T> T accept(Visitor<T> visitor);

  @Override
  public <T> T accept(SControl.Visitor<T> visitor) {
    return this.accept((Visitor<T>)visitor);
  }

  String declaration(String prefix, String opening, String closing) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(opening);
    // List<SBranch> branches = new ArrayList<>(this.branches);
    // Collections.sort(branches, (b1, b2) -> b1.toString().compareTo(b2.toString()));
    for (SBranch branch : this.branches) {
      resultBuilder.append("\n");
      resultBuilder.append(prefix);
      resultBuilder.append(branch.declaration(prefix));
    }
    SStatement elseStatement = this.elseStatement;
    if (elseStatement != null) {
      resultBuilder.append("\n");
      resultBuilder.append(prefix);
      resultBuilder.append(":: else ->");
      resultBuilder.append("\n");
      resultBuilder.append(prefix);
      resultBuilder.append("   ");
      resultBuilder.append(elseStatement.declaration(prefix + "   "));
    }
    resultBuilder.append("\n");
    resultBuilder.append(prefix);
    resultBuilder.append(closing);
    resultBuilder.append(";");

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(new HashSet<>(this.branches),
                        this.elseStatement);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SBranching other)
      return Objects.equals(new HashSet<>(this.branches), new HashSet<>(other.branches)) &&
             Objects.equals(this.elseStatement, other.elseStatement);
    return false;
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
