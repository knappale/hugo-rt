package uml.smile;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Sets;

import static java.util.stream.Collectors.toList;

import java.util.Collection;


@NonNullByDefault
public class SBranch {
  private SGuard guard;
  private SStatement effect;

  public interface Visitor<T> {
    public T onBranch(SGuard guard, SStatement statement);
  }

  private SBranch(SGuard guard, SStatement statement) {
    this.guard = guard;
    this.effect = statement;
  }

  /**
   * Create a Smile branch.
   *
   * @param guard a guard
   * @param statement a statement
   * @return a Smile branch
   */
  public static SBranch branch(SGuard guard, SStatement statement) {
    return new SBranch(guard, statement);
  }

  /**
   * @return branch's guard
   */
  public SGuard getGuard() {
    return this.guard;
  }

  /**
   * @return branch's effect
   */
  public SStatement getEffect() {
    return this.effect;
  }

  /**
   * Determine the "complexity" of this branch.
   *
   * @return this branch's complexity
   */
  int getComplexity() {
    return this.effect.getComplexity();
  }

  /**
   * The statement when starting {@code this} branch from statement {@code from}.
   *
   * @param from a statement
   * @param breakFlag a variable for indicating that a following loop shall not be executed
   * @return {@code this} branch after statement {@code from}, or {@code null} if
   * {@code from} does not occur in {@code this} branch
   */
  @Nullable SStatement getResumption(SPrimitive from, SVariable breakFlag) {
    return this.effect.getResumption(from, breakFlag);
  }

  /**
   * The branch when stopping {@code this} branch at some suspending statement.
   *
   * @param suspendFlag a variable for indicating that the statement got suspended
   * @return {@code this} branch up to (all) suspending statements
   */
  SBranch getCycle(SVariable suspendFlag) {
    var cycleEffect = this.effect.getCycle(suspendFlag);
    if (cycleEffect != this.effect)
      return SBranch.branch(this.guard, cycleEffect);
    return this;
  }
  
  /**
   * Determine the set of primitive statements that occur in this branch.
   * 
   * @return set of Smile primitives occurring in this branch
   */
  public Set<SPrimitive> getPrimitives() {
    return this.effect.getPrimitives();
  }

  /**
   * Determine the set of Smile constants that are used in this branch.
   * 
   * @return set of Smile constants used in this branch
   */
  public Set<SConstant> getConstants() {
    Set<SConstant> constants = new HashSet<>();
    constants.addAll(this.guard.getConstants());
    constants.addAll(this.effect.getConstants());
    return constants;
  }

  /**
   * Determine the set of Smile constants assigned to some variables in this branch.
   * 
   * @return set of Smile constants used in this branch
   */
  public Set<SConstant> getConstantsAssigned(Collection<SVariable> variables) {
    return this.effect.getConstantsAssigned(variables);
  }

  /**
   * Determine the set of variables assigned in this branch.
   *
   * @return set of variables assigned in this branch
   */
  Set<SVariable> getAssignedVariables() {
    return this.effect.getAssignedVariables();
  }

  /**
   * Determine the set of variables used in this branch.
   *
   * @return set of variables used in this branch
   */
  Set<SVariable> getUsedVariables() {
    Set<SVariable> usedVariables = new HashSet<>();
    usedVariables.addAll(this.guard.getVariables());
    usedVariables.addAll(this.effect.getUsedVariables());
    return usedVariables;
  }

  /**
   * Determine the set of unused assignments in this branch.
   *
   * @param assignments unused assignments flowing into this branch
   * @param overwrittenAssignments previously overwritten assignments
   * @return unused assignments flowing out of this branch; the list of overwritten assignments is updated
   */
  public List<SCommand> getUnusedAssignments(final List<SCommand> assignments, List<SCommand> overwrittenAssignments) {
    Set<SVariable> usedVariables = this.guard.getVariables();
    return this.effect.getUnusedAssignments(assignments.stream().filter(assignment -> Sets.intersection(usedVariables, assignment.getAssignedVariables()).equals(Sets.empty())).collect(toList()), overwrittenAssignments);
  }

  /**
   * Replace commands in this branch by a new command.
   *
   * @param commands a collection of commands
   * @param newCommand a command
   * @return this branch with replaced commands
   */
  public SBranch replace(Collection<SCommand> commands, SCommand newCommand) {
    this.effect.replace(commands, newCommand);
    return this;
  }

  /**
   * Replace top-level break commands in this branch by a new command.
   *
   * @param newCommand a command
   * @return this branch without top-level breaks
   */
  public SBranch replaceTopLevelBreaks(SCommand newCommand) {
    this.effect.replaceTopLevelBreaks(newCommand);
    return this;
  }

  /**
   * Propagate a guard to this branch, i.e., add a new guard conjunctively
   * to the guard of this branch.
   *
   * @param guard a guard
   * @return branch with propagated guard
   */
  public SBranch propagate(SGuard guard) {
    return SBranch.branch(SGuard.and(guard, this.guard), this.effect);
  }

  /**
   * Merge a statement into this branch, i.e., create a new branch with the same
   * guard as this branch but first executing this branch's effect, then the statement.
   *
   * @param statement a statement
   * @return merged branch
   */
  SBranch merge(SStatement statement) {
    return SBranch.branch(this.guard, SControl.seq(this.effect, statement));
  }

  /**
   * Execute this branch in a state.
   *
   * @param state a state
   * @return state after execution
   */
  public SState execute(SState state) {
    if (this.guard.evaluate(state).equals(Sets.singleton(SValue.falseValue())))
      return state.empty();
    state = state.withConstraint(this.guard);
    if (state.isInconsistent())
      return state.empty();
    return this.effect.execute(state);
  }

  /**
   * Partially evaluate this branch in a state.
   *
   * @param state a state
   * @return partially evaluated branch
   */
  public SBranch partiallyEvaluate(SState state) {
    SGuard reducedGuard = this.guard.reduce(state);
    SStatement reducedStatement = this.effect.partiallyEvaluate(state.withConstraint(reducedGuard));
    return SBranch.branch(reducedGuard, reducedStatement);
  }

  /**
   * Provide a simplified copy of this branch.
   *
   * @return simplified branch
   */
  public SBranch getSimplified() {
    return SBranch.branch(this.guard, this.effect.getSimplified());
  }

  /**
   * Accept a visitor.
   *
   * @param visitor a visitor
   * @return visit's result
   */
  public <T> T accept(Visitor<T> visitor) {
    return visitor.onBranch(this.guard, this.effect);
  }

  /**
   * Accept a statement visitor.
   *
   * @param visitor a visitor
   * @return visit's result
   */
  public <T> T accept(SStatement.Visitor<T> visitor) {
    return this.accept((Visitor<T>)visitor);
  }

  /**
   * Determine the declaration of this branch in Smile format with every
   * new line prepended by a prefix.
   *
   * @param prefix line prefix
   * @return branch's declaration with every new line prepended by {@code prefix}
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(":: ");
    resultBuilder.append(this.guard.toString());
    resultBuilder.append(" ->");
    resultBuilder.append("\n" + prefix + "   ");
    resultBuilder.append(this.effect.declaration(prefix + "   "));

    return resultBuilder.toString();
  }

  /**
   * Determine the declaration of this branch in Smile format.
   *
   * @return branch's declaration
   */
  public String declaration() {
    return declaration("");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.guard,
                        this.effect);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SBranch other)
      return Objects.equals(this.guard, other.guard) &&
             Objects.equals(this.effect, other.effect);
    return false;
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
