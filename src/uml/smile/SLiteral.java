package uml.smile;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UContext;
import uml.UExpression;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uml.statemachine.UVertex;
import util.Formatter;
import util.Sets;
import util.TriFunction;

import static java.util.stream.Collectors.toSet;

import static util.Objects.requireNonNull;


@NonNullByDefault
public class SLiteral {
  enum Kind {
    BOOLEAN,
    EQ,
    MATCH,
    COMPLETED,
    TIMEDOUT,
    EXTERNAL;
  }

  static final SLiteral FALSEEXPRESSION = new SLiteral(Kind.BOOLEAN, false);
  static final SLiteral TRUEEXPRESSION = new SLiteral(Kind.BOOLEAN, true);

  private Kind kind;
  private boolean positive;
  private @Nullable SVariable variable = null;
  private Set<SValue> values = new HashSet<>();
  private @Nullable UExpression uExternal = null;
  private @Nullable UContext uContext = null;

  private SLiteral(Kind kind, boolean positive) {
    this.kind = kind;
    this.positive = positive;
  }

  private SLiteral(SLiteral literal) {
    this(literal.kind, literal.positive);
    this.variable = literal.variable;
    this.values = new HashSet<>(literal.values);
    this.uExternal = literal.uExternal;
    this.uContext = literal.uContext;
  }

  public interface Visitor<T> {
    public T onBooleanConstant(boolean booleanConstant);
    public T onEq(boolean positive, SVariable variable, Set<SValue> values);
    public T onMatch(boolean positive, Set<UEvent> uEvents);
    public T onIsCompleted(boolean positive, UState uState);
    public T onIsTimedOut(boolean positive, UEvent uEvent);
    public T onExternal(boolean positive, uml.UExpression uExpression, uml.UContext uContext);
  }

  @NonNullByDefault({})
  public class Cases<T> implements Visitor<T> {
    private Function<Boolean, T> constantFun = null;
    private TriFunction<Boolean, @NonNull SVariable, @NonNull Set<@NonNull SValue>, T> eqFun = null;
    private BiFunction<Boolean, @NonNull Set<@NonNull UEvent>, T> matchFun = null;
    private BiFunction<Boolean, @NonNull UState, T> completedFun = null;
    private BiFunction<Boolean, @NonNull UEvent, T> timedFun = null;
    private TriFunction<Boolean, @NonNull UExpression, @NonNull UContext, T> externalFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases() {
    }

    public Cases<T> booleanConstant(Function<Boolean, T> constantFun) {
      this.constantFun = constantFun;
      return this;
    }
    
    public Cases<T> eq(TriFunction<Boolean, @NonNull SVariable, @NonNull Set<@NonNull SValue>, T> eqFun) {
      this.eqFun = eqFun;
      return this;
    }
    
    public Cases<T> match(BiFunction<Boolean, @NonNull Set<@NonNull UEvent>, T> matchFun) {
      this.matchFun = matchFun;
      return this;
    }
    
    public Cases<T> completed(BiFunction<Boolean, @NonNull UState, T> completedFun) {
      this.completedFun = completedFun;
      return this;
    }
    
    public Cases<T> timed(BiFunction<Boolean, @NonNull UEvent, T> timedFun) {
      this.timedFun = timedFun;
      return this;
    }
    
    public Cases<T> external(TriFunction<Boolean, @NonNull UExpression, @NonNull UContext, T> externalFun) {
      this.externalFun = externalFun;
      return this;
    }
    
    public Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply() {
      return SLiteral.this.accept(this);
    }

    private T otherwise() {
      assert (this.otherwiseFun != null) : "No default for case distinction on Smile literal type";
      return this.otherwiseFun.get();
    }

    @Override
    public T onBooleanConstant(boolean positive) {
      return this.constantFun != null ? this.constantFun.apply(positive) : otherwise();
    }

    @Override
    public T onEq(boolean positive, @NonNull SVariable variable, @NonNull Set<@NonNull SValue> values) {
      return this.eqFun != null ? this.eqFun.apply(positive, variable, values) : otherwise();
    }

    @Override
    public T onMatch(boolean positive, @NonNull Set<@NonNull UEvent> uEvents) {
      return this.matchFun != null ? this.matchFun.apply(positive, getUEvents()) : otherwise();
    }

    @Override
    public T onIsCompleted(boolean positive, @NonNull UState uState) {
      return this.completedFun != null ? this.completedFun.apply(positive, uState) : otherwise();
    }

    @Override
    public T onIsTimedOut(boolean positive, @NonNull UEvent uEvent) {
      return this.timedFun != null ? this.timedFun.apply(positive, uEvent) : otherwise();
    }

    @Override
    public T onExternal(boolean positive, @NonNull UExpression uExpression, @NonNull UContext uContext) {
      return this.externalFun != null ? this.externalFun.apply(positive, uExpression, uContext) : otherwise();
    }
  }

  /**
   * Create a literal which represents the boolean constant {@code false}.
   * 
   * @return a literal
   */
  public static SLiteral falseConst() {
    return FALSEEXPRESSION;
  }

  /**
   * Create a literal which represents the boolean constant {@code true}.
   * 
   * @return a guard
   */
  public static SLiteral trueConst() {
    return TRUEEXPRESSION;
  }

  /**
   * Create a literal which represents a boolean constant.
   *
   * @param booleanConst a boolean constant
   * @return a literal
   */
  public static SLiteral booleanConstant(boolean booleanConst) {
    if (booleanConst)
      return TRUEEXPRESSION;
    return FALSEEXPRESSION;
  }

  /**
   * Create a literal which represents matching of a set of events.
   * 
   * @param uEvents a set of (UML) events
   * @return a literal
   */
  public static SLiteral match(Collection<UEvent> uEvents) {
    Set<SValue> values = uEvents.stream().map(uEvent -> SValue.constant(SConstant.event(uEvent))).collect(toSet());
    if (values.isEmpty())
      return SLiteral.falseConst();

    SLiteral l = new SLiteral(Kind.MATCH, true);
    l.values.addAll(values);
    return l;
  }

  /**
   * Create a literal which represents matching of an event.
   * 
   * @param uEvent a (UML) event
   * @return a literal
   */
  public static SLiteral match(UEvent uEvent) {
    return SLiteral.match(Sets.singleton(uEvent));
  }

  /**
   * Create a literal which represents checking whether a state
   * is completed.
   * 
   * @param uState a (UML) state
   * @return a literal
   */
  public static SLiteral isCompleted(UState uState) {
    SLiteral l = new SLiteral(Kind.COMPLETED, true);
    l.values.add(SValue.constant(SConstant.vertex(uState)));
    return l;
  }

  /**
   * Create a literal which represents checking whether a state
   * is timed-out for an event.
   * 
   * @param uEvent (UML time) event
   * @return a literal
   */
  public static SLiteral isTimedOut(UEvent uEvent) {
    SLiteral l = new SLiteral(Kind.TIMEDOUT, true);
    l.values.add(SValue.constant(SConstant.event(uEvent)));
    return l;
  }

  /**
   * Create a literal which represents an external (UML) expression.
   * 
   * @param uExpression external (UML) expression
   * @param uContext (UML) context for external (UML) expression
   * @return a literal
   */
  public static SLiteral external(UExpression uExpression, UContext uContext) {
    UExpression externalEvaluated = uExpression.getEvaluated(uContext);
    if (UExpression.trueConst().equals(externalEvaluated))
      return SLiteral.trueConst();
    if (UExpression.falseConst().equals(externalEvaluated))
      return SLiteral.falseConst();
    SLiteral l = new SLiteral(Kind.EXTERNAL, true);
    l.uExternal = uExpression;
    l.uContext = uContext;
    return l;
  }

  /**
   * Create a literal which represents negation of a literal.
   * 
   * @param literal a literal
   * @return negated {@code literal}
   */
  public static SLiteral neg(SLiteral literal) {
    if (literal.equals(SLiteral.falseConst()))
      return SLiteral.trueConst();
    if (literal.equals(SLiteral.trueConst()))
      return SLiteral.falseConst();

    SLiteral l = new SLiteral(literal);
    l.positive = !literal.positive;
    return l;
  }

  /**
   * Create a literal which represents a test whether a variable is
   * equal to some value in a set of values.
   * 
   * @param variable a variable
   * @param values a set of values
   * @return a literal
   */
  public static SLiteral eq(SVariable variable, Set<SValue> values) {
    if (values.isEmpty())
      return SLiteral.falseConst();

    SLiteral l = new SLiteral(Kind.EQ, true);
    l.variable = variable;
    l.values.addAll(values);
    return l;
  }

  /**
   * Create a literal which represents a test whether a variable is
   * equal to a value.
   * 
   * @param variable a variable
   * @param value a value
   * @return a literal
   */
  public static SLiteral eq(SVariable variable, SValue value) {
    return SLiteral.eq(variable, Sets.singleton(value));
  }

  /**
   * Create a literal which represents a test whether a variable is
   * equal to a boolean constant.
   * 
   * @param variable a variable
   * @param booleanConstant a boolean
   * @return a literal
   */
  public static SLiteral eq(SVariable variable, boolean booleanConstant) {
    return SLiteral.eq(variable, SValue.booleanValue(booleanConstant));
  }

  /**
   * Create a literal which represents a test whether a variable is
   * equal to a constant.
   * 
   * @param variable a variable
   * @param constant a constant
   * @return a literal
   */
  public static SLiteral eq(SVariable variable, SConstant constant) {
    return SLiteral.eq(variable, SValue.constant(constant));
  }

  /**
   * Create a literal which represents a test whether a flag variable is true.
   * 
   * @param variable a flag variable
   * @return a literal
   */
  public static SLiteral isTrue(SVariable variable) {
    return SLiteral.eq(variable, true);
  }

  /**
   * Create a literal which represents a test whether a flag variable is false.
   * 
   * @param variable a flag variable
   * @return a literal
   */
  public static SLiteral isFalse(SVariable variable) {
    return SLiteral.eq(variable, false);
  }

  /**
   * Create a literal which represents a test whether a variable is
   * not equal to any of a set of values.
   * 
   * @param variable a variable
   * @param values a set of values
   * @return a literal
   */
  public static SLiteral neq(SVariable variable, Set<SValue> values) {
    if (values.isEmpty())
      return SLiteral.trueConst();

    SLiteral l = new SLiteral(Kind.EQ, false);
    l.variable = variable;
    l.values.addAll(values);
    return l;
  }

  /**
   * Create a literal which represents a test whether a variable is
   * not equal to some value.
   * 
   * @param variable a variable
   * @param value a value
   * @return a literal
   */
  public static SLiteral neq(SVariable variable, SValue value) {
    return SLiteral.neq(variable, Sets.singleton(value));
  }

  /**
   * Create a literal which represents a test whether a variable is
   * not equal to some constant.
   * 
   * @param variable a variable
   * @param constant a constant
   * @return a literal
   */
  public static SLiteral neq(SVariable variable, SConstant constant) {
    return SLiteral.neq(variable, SValue.constant(constant));
  }

  /**
   * @return whether this literal is true
   */
  public boolean isTrue() {
    return this == TRUEEXPRESSION;
  }

  /**
   * @return whether this literal is false
   */
  public boolean isFalse() {
    return this == FALSEEXPRESSION;
  }

  /**
   * Compute the meet of this literal with another literal.
   *
   * @param other another literal
   * @return the meet of {@code this} and {@code other}, or {@code null} if the meet is not a single literal
   */
  public @Nullable SLiteral meet(SLiteral other) {
    if (this.equals(SLiteral.trueConst()))
      return other;
    if (other.equals(SLiteral.trueConst()))
      return this;
    if (this.equals(SLiteral.falseConst()) || other.equals(SLiteral.falseConst()))
      return SLiteral.falseConst();
    if (this.kind != other.kind)
      return null;

    switch (this.kind) {
      case EQ:
        SVariable thisVariable = requireNonNull(this.variable);
        SVariable otherVariable = requireNonNull(other.variable);
        if (!thisVariable.equals(otherVariable))
          return null;
        if (this.positive && other.positive) // x : { 1, 2 } && x : { 1, 3 } <-> x == 1
          return SLiteral.eq(thisVariable, Sets.intersection(this.values, other.values));
        if (this.positive && !other.positive) // x : { 1, 2 } && x !: { 1, 3 } <-> x == 2
          return SLiteral.eq(thisVariable, Sets.difference(this.values, other.values));
        if (!this.positive && other.positive) // x !: { 1, 2 } && x : { 1, 3 } <-> x == 3
          return SLiteral.eq(thisVariable, Sets.difference(other.values, this.values));
        if (!this.positive && !other.positive) // x !: { 1, 2 } && x !: { 1, 3 } <-> x !: { 1, 2, 3 }
          return SLiteral.neq(thisVariable, Sets.union(other.values, this.values));
        return null;

      case MATCH:
        if (this.positive && other.positive)
          return SLiteral.match(Sets.intersection(this.getUEvents(), other.getUEvents()));
        if (this.positive && !other.positive)
          return SLiteral.match(Sets.difference(this.getUEvents(), other.getUEvents()));
        if (!this.positive && other.positive)
          return SLiteral.match(Sets.difference(other.getUEvents(), this.getUEvents()));
        if (!this.positive && !other.positive)
          return SLiteral.neg(SLiteral.match(Sets.union(other.getUEvents(), this.getUEvents())));
        return null;

      case COMPLETED:
        if (!this.getUVertices().equals(other.getUVertices()))
          return null;
        if ((this.positive && other.positive) || (!this.positive && !other.positive))
          return this;
        if ((this.positive && !other.positive) || (!this.positive && other.positive))
          return SLiteral.falseConst();
        return null;

      case TIMEDOUT:
        if (!this.getUEvents().equals(other.getUEvents()))
          return null;
        if ((this.positive && other.positive) || (!this.positive && !other.positive))
          return this;
        if ((this.positive && !other.positive) || (!this.positive && other.positive))
          return SLiteral.falseConst();
        return null;

      case EXTERNAL:
        var thisContext = requireNonNull(this.uContext);
        var otherContext = requireNonNull(other.uContext);
        if (!thisContext.equals(otherContext))
          return null;
        var thisExternal = requireNonNull(this.uExternal);
        if (!this.positive)
          thisExternal = UExpression.neg(thisExternal);
        var otherExternal = requireNonNull(other.uExternal);
        if (!other.positive)
          otherExternal = UExpression.neg(otherExternal);
        var conjExternal = UExpression.and(thisExternal, otherExternal);
        if (conjExternal.isContradictory(thisContext))
          return SLiteral.falseConst();
        return null;
        // return SLiteral.external(conjExternal, thisContext);

      default:
    }

    return null;
  }

  /**
   * Compute a reduced set of literals equivalent to the conjunction
   * of the given literals.
   *
   * @param literals a set of literals
   * @return reduced set of literals
   */
  public static Set<SLiteral> meet(Set<SLiteral> literals) {
    if (literals.isEmpty())
      return literals;
    for (SLiteral literal : literals) {
      if (literal.isFalse())
        return Sets.singleton(SLiteral.falseConst());
      Set<SLiteral> otherLiterals = new HashSet<>(literals);
      otherLiterals.remove(literal);
      if (literal.isTrue())
        return meet(otherLiterals);
      for (SLiteral otherLiteral : otherLiterals) {
        @Nullable SLiteral metLiteral = literal.meet(otherLiteral);
        if (metLiteral != null) {
          if (metLiteral.isFalse())
            return Sets.singleton(SLiteral.falseConst());
          otherLiterals.remove(otherLiteral);
          if (!metLiteral.isTrue())
            otherLiterals.add(metLiteral);
          return meet(otherLiterals);
        }
      }
    }
    return literals;
  }

  /**
   * Compute the join of this literal with another literal.
   *
   * @param other another literal
   * @return the join of {@code this} and {@code other}, or {@code null} if the join is not a single literal
   */
  public @Nullable SLiteral join(SLiteral other) {
    if (this.equals(SLiteral.falseConst()))
      return other;
    if (other.equals(SLiteral.falseConst()))
      return this;
    if (this.equals(SLiteral.trueConst()) || other.equals(SLiteral.trueConst()))
      return SLiteral.trueConst();
    if (this.kind != other.kind)
      return null;

    switch (this.kind) {
      case EQ:
        SVariable thisVariable = requireNonNull(this.variable);
        SVariable otherVariable = requireNonNull(other.variable);
        if (!thisVariable.equals(otherVariable))
          return null;
        if (this.positive && other.positive) // x : { 1, 2 } || x : { 1, 3 } <-> x : { 1, 2, 3 }
          return SLiteral.eq(thisVariable, Sets.union(this.values, other.values));
        if (this.positive && !other.positive) // x : { 1, 2 } || x !: { 1, 3 } <-> x != 3
          return SLiteral.neq(thisVariable, Sets.difference(other.values, this.values));
        if (!this.positive && other.positive) // x !: { 1, 2 } || x : { 1, 3 } <-> x != 2
          return SLiteral.neq(thisVariable, Sets.difference(this.values, other.values));
        if (!this.positive && !other.positive) // x !: { 1, 2 } || x !: { 1, 3 } <-> x != 1
          return SLiteral.neq(thisVariable, Sets.intersection(other.values, this.values));
        return null;

      case MATCH:
        if (this.positive && other.positive)
          return SLiteral.match(Sets.union(this.getUEvents(), other.getUEvents()));
        if (this.positive && !other.positive)
          return SLiteral.neg(SLiteral.match(Sets.difference(other.getUEvents(), this.getUEvents())));
        if (!this.positive && other.positive)
          return SLiteral.neg(SLiteral.match(Sets.difference(this.getUEvents(), other.getUEvents())));
        if (!this.positive && !other.positive)
          return SLiteral.neg(SLiteral.match(Sets.intersection(other.getUEvents(), this.getUEvents())));
        return null;

      case COMPLETED:
        if (!this.getUVertices().equals(other.getUVertices()))
          return null;
        if ((this.positive && other.positive) || (!this.positive && !other.positive))
          return this;
        if ((this.positive && !other.positive) || (!this.positive && other.positive))
          return SLiteral.trueConst();
        return null;

      case TIMEDOUT:
        if (!this.getUEvents().equals(other.getUEvents()))
          return null;
        if ((this.positive && other.positive) || (!this.positive && !other.positive))
          return this;
        if ((this.positive && !other.positive) || (!this.positive && other.positive))
          return SLiteral.trueConst();
        return null;

      case EXTERNAL:
        var thisContext = requireNonNull(this.uContext);
        var otherContext = requireNonNull(other.uContext);
        if (!thisContext.equals(otherContext))
          return null;
        var thisExternal = requireNonNull(this.uExternal);
        if (!this.positive)
          thisExternal = UExpression.neg(thisExternal);
        var otherExternal = requireNonNull(other.uExternal);
        if (!other.positive)
          otherExternal = UExpression.neg(otherExternal);
        var disjExternal = UExpression.or(thisExternal, otherExternal);
        if (disjExternal.isTautology(thisContext))
          return SLiteral.trueConst();
        return null;
        // return SLiteral.external(disjExternal, thisContext);

      default:
    }

    return null;
  }

  /**
   * Compute a reduced set of literals equivalent to the disjunction
   * of the given literals.
   *
   * @param literals a set of literals
   * @return reduced set of literals
   */
  public static Set<SLiteral> join(Set<SLiteral> literals) {
    if (literals.isEmpty())
      return literals;
    for (SLiteral literal : literals) {
      if (literal.isTrue())
        return Sets.singleton(SLiteral.trueConst());
      Set<SLiteral> otherLiterals = new HashSet<>(literals);
      otherLiterals.remove(literal);
      if (literal.isFalse())
        return join(otherLiterals);
      for (SLiteral otherLiteral : otherLiterals) {
        @Nullable SLiteral joinedLiteral = literal.join(otherLiteral);
        if (joinedLiteral != null) {
          if (joinedLiteral.isTrue())
            return Sets.singleton(SLiteral.trueConst());
          otherLiterals.remove(otherLiteral);
          if (!joinedLiteral.isFalse())
            otherLiterals.add(joinedLiteral);
          return join(otherLiterals);
        }
      }
    }
    return literals;
  }

  /**
   * Determine the set of variables that are used in this literal.
   * 
   * @return set of variables used in this literal
   */
  public Set<SVariable> getVariables() {
    Set<SVariable> variables = new HashSet<>();

    switch (this.kind) {
      case EQ:
        variables.add(requireNonNull(this.variable));
        return variables;

      //$CASES-OMITTED$
      default:
        return variables;
    }
  }

  /**
   * Determine the set of constants that are used in this literal.
   * 
   * @return set of constants used in this literal
   */
  public Set<SConstant> getConstants() {
    return this.values.stream().map(value -> value.new Cases<Set<SConstant>>().constant(constant -> Sets.singleton(constant)).otherwise(Sets::empty).apply()).reduce(Sets.empty(), Sets::union);
  }

  /**
   * Determine the (UML) events of this literal.
   *
   * @return literal's events
   */
  private Set<UEvent> getUEvents() {
    return this.getConstants().stream().map(constant -> constant.new Cases<Set<UEvent>>().event(uEvent -> Sets.singleton(uEvent)).otherwise(Sets::empty).apply()).reduce(Sets.empty(), Sets::union);
  }

  /**
   * Determine the (UML) vertices of this literal.
   *
   * @return literal's vertices
   */
  private Set<UVertex> getUVertices() {
    return this.getConstants().stream().map(constant -> constant.new Cases<Set<UVertex>>().vertex(uVertex -> Sets.singleton(uVertex)).otherwise(Sets::empty).apply()).reduce(Sets.empty(), Sets::union);
  }

  /**
   * Accept a visitor.
   *
   * @param visitor a visitor
   * @return visit's result
   */
  public <T> T accept(Visitor<T> visitor) {
    switch (this.kind) {
      case BOOLEAN:
        return visitor.onBooleanConstant(this.positive);
      case MATCH:
        return visitor.onMatch(this.positive, this.getUEvents());
      case COMPLETED:
        return visitor.onIsCompleted(this.positive, (UState)requireNonNull(this.getUVertices().iterator().next()));
      case TIMEDOUT:
        return visitor.onIsTimedOut(this.positive, requireNonNull(this.getUEvents().iterator().next()));
      case EQ:
        return visitor.onEq(this.positive, requireNonNull(this.variable), this.values);
      case EXTERNAL:
        return visitor.onExternal(this.positive, requireNonNull(this.uExternal), requireNonNull(this.uContext));
      default:
        throw new IllegalStateException("Unknown Smile literal kind");
    }
  }

  private static class Evaluator implements Visitor<Set<SValue>> {
    private SState sState;

    public Evaluator(SState sState) {
      this.sState = sState;
    }

    @Override
    public Set<SValue> onBooleanConstant(boolean booleanConstant) {
      return Sets.singleton(SValue.booleanValue(booleanConstant));
    }

    @Override
    public Set<SValue> onEq(boolean positive, SVariable variable, Set<SValue> values) {
      if (values.containsAll(sState.getValues(variable)))     // literal: x : { 1, 2, 3 }, state: x : { 1, 2 } -> true
        return Sets.singleton(SValue.booleanValue(positive)); // literal: x !: { 1, 2, 3 }, state: x : { 1, 2 } -> false
      if (Sets.intersection(this.sState.getValues(variable), values).equals(Sets.empty()))
        return Sets.singleton(SValue.booleanValue(!positive));
      return this.sState.getBooleans();
    }

    @Override
    public Set<SValue> onMatch(boolean positive, Set<UEvent> uEvents) {
      if (uEvents.containsAll(this.sState.getEvents()))
        return Sets.singleton(SValue.booleanValue(positive));
      if (Sets.intersection(this.sState.getEvents(), uEvents).equals(Sets.empty()))
        return Sets.singleton(SValue.booleanValue(!positive));
      return this.sState.getBooleans();
    }

    @Override
    public Set<SValue> onIsCompleted(boolean positive, UState uState) {
      Set<SValue> isCompleted = this.sState.getCompleteds(uState);
      return positive ? isCompleted : isCompleted.stream().map(booleanSValue -> booleanSValue.neg()).collect(toSet());
    }

    @Override
    public Set<SValue> onIsTimedOut(boolean positive, UEvent uEvent) {
      return this.sState.getBooleans();
    }

    @Override
    public Set<SValue> onExternal(boolean positive, UExpression uExpression, UContext uContext) {
      if (uContext.getBooleanType().subsumes(uExpression.getType(uContext))) {
        if (this.sState.withParametersConstraints(positive ? uExpression : UExpression.neg(uExpression), uContext).isInconsistent())
          return Sets.singleton(SValue.falseValue());
        return this.sState.getBooleans();
      }
      return Sets.empty();
    }
  }

  /**
   * Evaluate this literal in a state.
   *
   * @param state a state
   * @return set of values
   */
  public Set<SValue> evaluate(SState sState) {
    return this.accept(new Evaluator(sState));
  }

  /**
   * Determine whether this literal is definitely true in a state.
   *
   * @param state a state
   * @return whether this literal is definitely true in {@code state}
   */
  public boolean isTrue(SState state) {
    return this.evaluate(state).equals(Sets.singleton(SValue.trueValue()));
  }

  /**
   * Determine whether this literal is definitely false in a state.
   *
   * @param state a state
   * @return whether this literal is definitely false in {@code state}
   */
  public boolean isFalse(SState state) {
    return this.evaluate(state).equals(Sets.singleton(SValue.falseValue()));
  }

  /**
   * Reduce this literal in a state.
   *
   * @param state a state
   * @return reduced guard
   */
  public SLiteral reduce(SState state) {
    Set<SValue> resultValues = this.evaluate(state);
    if (resultValues.size() == 1)
      return SLiteral.booleanConstant(resultValues.contains(SValue.trueValue()));

    switch (this.kind) {
      case MATCH: {
        var possible = this.positive ? Sets.intersection(this.getUEvents(), state.getEvents()) : Sets.difference(state.getEvents(), this.getUEvents());
        var notPossible = Sets.difference(state.getEvents(), possible);
        if (possible.size() <= notPossible.size())
          return SLiteral.match(possible);
        return SLiteral.neg(SLiteral.match(notPossible));
      }

      case EQ: {
        var variable = requireNonNull(this.variable);
        var possible = this.positive ? Sets.intersection(this.values, state.getValues(variable)) : Sets.difference(state.getValues(variable), this.values);
        var notPossible = Sets.difference(state.getValues(variable), possible);
        if (possible.size() <= notPossible.size())
          return SLiteral.eq(variable, possible);
        return SLiteral.neg(SLiteral.eq(variable, notPossible));
      }

      /*
      case EXTERNAL: {
        var uExternal = requireNonNull(this.uExternal);
        var uContext = requireNonNull(this.uContext);
        if (this.positive && state.withParametersConstraints(uExternal, uContext).isInconsistent())
          return SLiteral.falseConst();
        if (!this.positive && state.withParametersConstraints(UExpression.neg(uExternal), uContext).isInconsistent())
          return SLiteral.falseConst();
        return this;
      }
      */

      default:
        return this;
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.kind,
                        this.positive,
                        this.variable,
                        this.values,
                        this.uExternal,
                        this.uContext);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SLiteral other)
      return this.kind == other.kind &&
             this.positive == other.positive &&
             Objects.equals(this.variable, other.variable) &&
             Objects.equals(this.values, other.values) &&
             Objects.equals(this.uExternal, other.uExternal) &&
             Objects.equals(this.uContext, other.uContext);
    return false;
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case BOOLEAN: {
        resultBuilder.append(this.positive);
        return resultBuilder.toString();
      }

      case MATCH: {
        if (!this.positive)
          resultBuilder.append("!");
        resultBuilder.append("match(currentEvent, ");
        resultBuilder.append(Formatter.setOrSingleton(this.getUEvents(), uEvent -> "<" + uEvent.getName() + ">"));
        resultBuilder.append(")");
        return resultBuilder.toString();
      }
    
      case COMPLETED: {
        if (!this.positive)
          resultBuilder.append("!");
        resultBuilder.append("isCompleted(<");
        resultBuilder.append(requireNonNull(this.getUVertices().iterator().next()).getName());
        resultBuilder.append(">)");
        return resultBuilder.toString();
      }

      case TIMEDOUT: {
        if (!this.positive)
          resultBuilder.append("!");
        resultBuilder.append("isTimedOut(<");
        resultBuilder.append(requireNonNull(this.getUEvents().iterator().next()).getName());
        resultBuilder.append(">)");
        return resultBuilder.toString();
      }

      case EXTERNAL: {
        if (!this.positive)
          resultBuilder.append("!");
        resultBuilder.append("eval(");
        resultBuilder.append(requireNonNull(this.uExternal).toString());
        resultBuilder.append(")");
        return resultBuilder.toString();
      }

      case EQ: {
        SVariable variable = requireNonNull(this.variable);

        if (variable.isFlag() && this.values.size() == 1) {
          SValue value = requireNonNull(this.values.iterator().next());
          if (!this.positive)
            value = value.neg();
          if (value.equals(SValue.falseValue()))
            resultBuilder.append("!");
          resultBuilder.append(variable.getName());
          return resultBuilder.toString();
        }

        if (this.values.size() == 1) {
          SValue value = requireNonNull(this.values.iterator().next());
          resultBuilder.append(variable.getName());
          resultBuilder.append(this.positive ? " == " : " != ");
          resultBuilder.append(value);
          return resultBuilder.toString();
        }

        if (!this.positive)
          resultBuilder.append("!(");
        resultBuilder.append(variable.getName());
        resultBuilder.append(" : ");
        resultBuilder.append(Formatter.set(this.values));
        if (!this.positive)
          resultBuilder.append(")");
        return resultBuilder.toString();
      }
    }

    return resultBuilder.toString();
  }
}
