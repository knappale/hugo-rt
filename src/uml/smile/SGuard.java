package uml.smile;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Sets;


/**
 * Smile guard
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class SGuard {
  public interface Visitor<T> {
    public T onAnd(SGuard leftGuard, SGuard rightGuard);
    public T onOr(SGuard leftGuard, SGuard rightGuard);
    public T onLiteral(SLiteral literal);
  }

  @NonNullByDefault({})
  public class Cases<T> implements Visitor<T> {
    private Function<@NonNull SLiteral, T> literalFun = null;
    private BiFunction<@NonNull SGuard, @NonNull SGuard, T> andFun = null;
    private BiFunction<@NonNull SGuard, @NonNull SGuard, T> orFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases<T> literal(Function<@NonNull SLiteral, T> literalFun) {
      this.literalFun = literalFun;
      return this;
    }
    
    public Cases<T> and(BiFunction<@NonNull SGuard, @NonNull SGuard, T> andFun) {
      this.andFun = andFun;
      return this;
    }
    
    public Cases<T> or(BiFunction<@NonNull SGuard, @NonNull SGuard, T> orFun) {
      this.orFun = orFun;
      return this;
    }
    
    public Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply() {
      return SGuard.this.accept(this);
    }

    private T otherwise() {
      assert (this.otherwiseFun != null) : "No default for case distinction on Smile guard type";
      return this.otherwiseFun.get();
    }

    @Override
    public T onLiteral(@NonNull SLiteral literal) {
      return this.literalFun != null ? this.literalFun.apply(literal) : otherwise();
    }

    @Override
    public T onAnd(@NonNull SGuard leftGuard, @NonNull SGuard rightGuard) {
      return this.andFun != null ? this.andFun.apply(leftGuard, rightGuard) : otherwise();
    }

    @Override
    public T onOr(@NonNull SGuard leftGuard, @NonNull SGuard rightGuard) {
      return this.orFun != null ? this.orFun.apply(leftGuard, rightGuard) : otherwise();
    }
  }

  private SClauseSet clauseSet = SClauseSet.falseClauseSet();

  private static SGuard FALSEGUARD = new SGuard(SClauseSet.falseClauseSet());
  private static SGuard TRUEGUARD = new SGuard(new SClauseSet(SClause.trueClause()));

  private SGuard() {
  }

  private SGuard(SClauseSet clauseSet) {
    this.clauseSet = clauseSet;
  }

  public static SGuard falseConst() {
    return FALSEGUARD;
  }

  public static SGuard trueConst() {
    return TRUEGUARD;
  }

  /**
   * Create a guard from a literal.
   * 
   * @param literal a literal
   * @return a guard
   */
  public static SGuard literal(SLiteral literal) {
    if (literal.isTrue())
      return SGuard.trueConst();
    if (literal.isFalse())
      return SGuard.falseConst();
    SGuard g = new SGuard();
    g.clauseSet = SClause.trueClause().and(literal);
    return g;
  }

  /**
   * Create a guard from a clause.
   * 
   * @param clause a clause
   * @return a guard
   */
  public static SGuard clause(SClause clause) {
    if (clause.isTrue())
      return SGuard.trueConst();
    SGuard g = new SGuard();
    g.clauseSet = new SClauseSet(clause);
    return g;
  }

  /**
   * Create a guard from a clause set.
   * 
   * @param clauseSet a clause set
   * @return a guard
   */
  public static SGuard clauseSet(SClauseSet clauseSet) {
    if (clauseSet.isFalse())
      return SGuard.falseConst();
    SGuard g = new SGuard();
    g.clauseSet = clauseSet;
    return g;
  }

  /**
   * Create a guard which represents negation of a guard.
   * 
   * @param guard a guard
   * @return a guard
   */
  public static SGuard neg(SGuard guard) {
    return SGuard.clauseSet(guard.clauseSet.neg());
  }

  /**
   * Create a conjunction of two guards.
   * 
   * @param leftGuard left guard
   * @param rightGuard right guard
   * @return conjunction of {@code leftGuard} and {@code rightGuard}
   */
  public static SGuard and(SGuard leftGuard, SGuard rightGuard) {
    return SGuard.clauseSet(leftGuard.clauseSet.and(rightGuard.clauseSet));
  }

  /**
   * Create a conjunction of guards.
   * 
   * @param guards collection of guards
   * @return conjunction of {@code guards}
   */
  public static SGuard and(Collection<SGuard> guards) {
    return guards.stream().reduce(SGuard.trueConst(), SGuard::and);
  }

  /**
   * Create a conjunction of guards.
   * 
   * @param guards some guards
   * @return conjunction of {@code guards}
   */
  public static SGuard and(@NonNull SGuard... guards) {
    return SGuard.and(Arrays.asList(guards));
  }

  /**
   * Create a disjunction of two guards.
   * 
   * @param leftGuard left guard
   * @param rightGuard right guard
   * @return disjunction of {@code leftGuard} and {@code rightGuard}
   */
  public static SGuard or(SGuard leftGuard, SGuard rightGuard) {
    return SGuard.clauseSet(leftGuard.clauseSet.or(rightGuard.clauseSet));
  }

  /**
   * Create a disjunction of guards.
   * 
   * @param guards collection of guards
   * @return disjunction of {@code guards}
   */
  public static SGuard or(Collection<SGuard> guards) {
    return guards.stream().reduce(SGuard.falseConst(), SGuard::or);
  }

  /**
   * Create a disjunction of guards.
   * 
   * @param guards some guards
   * @return disjunction of {@code guards}
   */
  public static SGuard or(@NonNull SGuard... guards) {
    return SGuard.or(Arrays.asList(guards));
  }

  /**
   * @return whether this guard is true
   */
  public boolean isTrue() {
    return this.clauseSet.isTrue();
  }

  /**
   * @return whether this guard is false
   */
  public boolean isFalse() {
    return this.clauseSet.isFalse();
  }

  /**
   * @return this guard's size
   */
  public int size() {
    return this.clauseSet.size();
  }

  /**
   * Determine the set of variables that are used in this guard.
   * 
   * @return set of variables used in this guard
   */
  public Set<SVariable> getVariables() {
    return this.clauseSet.getVariables();
  }


  /**
   * Determine the set of constants that are used in this guard.
   * 
   * @return set of constants used in this guard
   */
  public Set<SConstant> getConstants() {
    return this.clauseSet.getConstants();
  }

  /**
   * Accept a visitor.
   *
   * @param visitor a visitor
   * @return visit's result
   */
  public <T> T accept(Visitor<T> visitor) {
    return this.clauseSet.accept(visitor);
  }

  /**
   * Evaluate this guard in a state.
   *
   * @param state a state
   * @return set of values
   */
  public Set<SValue> evaluate(SState state) {
    return this.clauseSet.evaluate(state);
  }

  /**
   * Determine whether this guard is definitely true in a state.
   *
   * @param state a state
   * @return whether this guard is definitely true in {@code state}
   */
  public boolean isTrue(SState state) {
    return this.evaluate(state).equals(Sets.singleton(SValue.trueValue()));
  }

  /**
   * Determine whether this guard is definitely false in a state.
   *
   * @param state a state
   * @return whether this guard is definitely false in {@code state}
   */
  public boolean isFalse(SState state) {
    return this.evaluate(state).equals(Sets.singleton(SValue.falseValue()));
  }

  /**
   * Reduce this guard in a state.
   *
   * @param state a state
   * @return reduced guard
   */
  public SGuard reduce(SState state) {
    return SGuard.clauseSet(this.clauseSet.reduce(state));
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.clauseSet);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SGuard other)
      return Objects.equals(this.clauseSet, other.clauseSet);
    return false;
  }

  @Override
  public String toString() {
    return this.clauseSet.toString();
  }
}
