package uml.smile.translation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAction;
import uml.UAttribute;
import uml.UBehavioural;
import uml.UContext;
import uml.UExpression;
import uml.UStatual;
import uml.smile.SBranch;
import uml.smile.SBranching;
import uml.smile.SCommand;
import uml.smile.SConstant;
import uml.smile.SControl;
import uml.smile.SGuard;
import uml.smile.SLiteral;
import uml.smile.SMachine;
import uml.smile.SPrimitive;
import uml.smile.SStatement;
import uml.statemachine.UEvent;
import uml.statemachine.UFinalState;
import uml.statemachine.UPseudoState;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UTransition;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UCompoundTransition;
import util.Lists;
import util.Sets;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static util.Formatter.quoted;
import static util.Objects.assertNonNull;
import static util.Objects.requireNonNull;
import static util.Objects.toType;


/**
 * Translation of (simple) UML models into Smile.
 *
 * TODO (AK060311) do activities are missing
 * TODO (AK160410) Completion events are only produced, when there is an outgoing completion transition.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class STranslator {
  private static java.util.logging.Logger DEBUG = util.Message.debug();

  private control.Properties properties;
  private UStateMachine uStateMachine;
  private SSymbolTable symbolTable;

  private STranslator(SSymbolTable symbolTable) {
    this.symbolTable = symbolTable;
    this.uStateMachine = symbolTable.getStateMachine();
    this.properties = symbolTable.getProperties();
  }

  /**
   * Translation of UML state machines into Smile.
   *
   * @param uStateMachine a (UML) state machine
   * @param properties translation control properties
   * @return a Smile machine representing {@code uStateMachine}
   */
  public static SMachine translateStateMachine(UStateMachine uStateMachine, control.Properties properties) {
    DEBUG.info("Translating state machine for class " + quoted(uStateMachine.getC1ass().getName()));
    var symbolTable = new SSymbolTable(uStateMachine, properties);
    var translator = new STranslator(symbolTable);

    var initialisation = translator.initialisation();
    var main = SControl.seq(translator.transitionSelection(),
                            translator.resetChosenFlags(),
                            translator.defer(),
                            translator.transitionFiring(),
                            translator.resetChosenFlags(),
                            translator.acknowledge());
    var terminated = translator.terminated();
    return new SMachine(symbolTable, initialisation, main, terminated);
  }

  /**
   * Generate initialisation statement.
   *
   * @return initialisation statement
   */
  private SStatement initialisation() {
    return SControl.atomic(
               SControl.seq(SPrimitive.initialisation(),
                            resetChosenFlags(),
                            SControl.seq(this.uStateMachine.getRegions().stream().map(uRegion ->
                                             SControl.seq(this.symbolTable.updateVertex(requireNonNull(uRegion.getInitial())),
                                                          // this.symbolTable.updateTransition(requireNonNull(uRegion.getInitial()).getOutgoingCompounds().get(0)))).collect(toList())),
                                                          transitionFiring(requireNonNull(uRegion.getInitial()).getOutgoingCompounds().get(0)))).collect(toList())),
                            resetChosenFlags(),
                            transitionFiring(this.uStateMachine.getRegions().stream().flatMap(uRegion -> requireNonNull(uRegion.getInitial()).getInitialisingCompounds().stream()).collect(toSet())),
                            resetChosenFlags()));
  }

  /**
   * Generate termination condition.
   *
   * @return termination condition
   */
  private SGuard terminated() {
    // No termination possible if one of the top level regions has no final state.
    if (this.uStateMachine.getRegions().stream().anyMatch(uRegion -> uRegion.getFinal() == null))
      return SGuard.falseConst();

    return SGuard.and(this.uStateMachine.getRegions().stream().map(uRegion -> this.symbolTable.checkVertex(requireNonNull(uRegion.getFinal()))).collect(toList()));
  }

  /**
   * Generate acknowledgement.
   *
   * @return acknowledgement statement
   */
  private SStatement acknowledge() {
    var acknowledgeGuard = SGuard.or(this.uStateMachine.getSynchronousCallEvents().stream().map(uEvent -> SGuard.literal(SLiteral.match(uEvent))).collect(toSet()));
    // Only acknowledge if the event has not been deferred
    if (this.uStateMachine.getDeferrableEvents().isEmpty())
      return SControl.ifThen(acknowledgeGuard,
                             SPrimitive.acknowledge());
    return SControl.seq(SControl.ifThen(SGuard.and(SGuard.neg(this.symbolTable.checkIsDeferred()), acknowledgeGuard),
                                        SPrimitive.acknowledge()),
                        this.symbolTable.resetIsDeferred());
  }

  /**
   * Generate event deferring.
   *
   * @return event deferring statement
   */
  private SStatement defer() {
    // If there are no deferred events, just signal that some
    // behavioural event has been consumed.
    if (this.uStateMachine.getDeferrableEvents().isEmpty())
      return SControl.ifThen(SGuard.or(this.uStateMachine.getEvents().stream().filter(UEvent::isBehavioural).map(uEvent -> SGuard.literal(SLiteral.match(uEvent))).collect(toSet())),
                             SPrimitive.chosen());

    // If the current event is deferred, then defer the current event;
    // otherwise signal that some event has been consumed;
    // afterwards reset all transition variables, but keep deferred flag
    // for acknowledgements
    return SControl.ifThenElse(this.symbolTable.checkIsDeferred(),
                               SControl.seq(SPrimitive.defer(), SControl.seq(this.uStateMachine.getAllRegions().stream().map(uRegion -> this.symbolTable.resetTransition(uRegion)).collect(toList()))),
                               SPrimitive.chosen());
  }

  /**
   * Reset chosen flags.
   *
   * @return reset statement
   */
  private SStatement resetChosenFlags() {
    return SControl.seq(this.uStateMachine.getAllRegions().stream().map(uRegion -> this.symbolTable.resetChosen(uRegion)).distinct().collect(toList()));
  }

  /**
   * Generate statement for overall transition selection.
   *
   * @return transition selection statement
   */
  private SStatement transitionSelection() {
    return transitionSelectionRegions(this.uStateMachine.getRegions());
  }

  /**
   * Generate statement for transition selection for all persistent
   * sub-states of a region.
   *
   * @param uRegion a region
   * @return region's states' transition selection statement
   */
  private SStatement transitionSelectionRegion(URegion uRegion) {
    return SControl.seq(uRegion.getVertices().stream().map(uVertex -> transitionSelectionVertex(uVertex)).collect(toList()));
  }

  /**
   * Generate statement for transition selection for all persistent
   * sub-states of a list of regions.
   *
   * @param uRegions a list of regions
   * @return regions' states' transition selection statement
   */
  private SStatement transitionSelectionRegions(List<URegion> uRegions) {
    if (uRegions.size() == 0)
      return SCommand.skip();
 
    if (uRegions.size() == 1)
      return transitionSelectionRegion(uRegions.get(0));

    if (this.properties.isFixedOrderRegions())
      return SControl.seq(uRegions.stream().map(uRegion -> transitionSelectionRegion(uRegion)).collect(toList()));

    return SControl.seq(SBranching.loop(uRegions.stream().map(uRegion ->
                                              SBranch.branch(this.symbolTable.checkRegionNotSet(uRegion),
                                                                    SControl.seq(this.symbolTable.setRegion(uRegion), transitionSelectionRegion(uRegion)))).collect(toList()),
                                                             SCommand.breakStm()),
                                          SControl.seq(uRegions.stream().map(uRegion -> this.symbolTable.resetRegion(uRegion)).collect(toList())));
  }

  /**
   * Generate statement for transition selection for a vertex.
   *
   * Every run-to-completion step starts in a simple or a composite
   * state.  Although also final states are proper states in the sense
   * of UML, there are no transitions outgoing from a final state.
   *
   * @param uVertex a vertex
   * @return vertex's transition selection statement
   */
  private SStatement transitionSelectionVertex(UVertex uVertex) {
    return uVertex.new Cases<SStatement>().
             pseudoState(uPseudoState -> SCommand.skip()).
             finalState(uFinalState -> SCommand.skip()).
             state(uState -> transitionSelectionState(uState)).
             apply();
  }

  /**
   * Generate statement for transition selection for a given state.
   *
   * Every run-to-completion step starts in a simple or a composite
   * state.  Although also final states are proper states in the sense
   * of UML, there are no transitions outgoing from a final state.
   *
   * @param uState state for transition selection
   * @return state's transition selection statement
   */
  private SStatement transitionSelectionState(UState uState) {
    URegion uRegion = uState.getContainer();

    // Whether any outgoing compound transition can be selected at all
    SGuard condition = SGuard.and(this.symbolTable.checkVertex(uState), this.symbolTable.checkUnchosen(uRegion));

    // Process inner transition selection
    SStatement inner = SCommand.skip();
    if (uState.getRegions().size() > 0)
      inner = transitionSelectionRegions(uState.getRegions());

    List<SBranch> outgoingsBranches = new ArrayList<>();
    // Process transitions outgoing from this state
    if (!uState.getOutgoingCompounds().isEmpty())
      outgoingsBranches.addAll(uState.getOutgoingCompounds().stream().map(uCompound -> transitionSelectionBranch(uState, uCompound)).collect(toList()));
    // If the state has deferrable events, the isDeferred flag may be set
    SStatement defer = SCommand.skip();
    if (!uState.getDeferrableEvents().isEmpty()) {
      Set<URegion> chosens = Sets.singleton(uRegion);
      if (uRegion.isOrthogonal()) {
        chosens.addAll(uRegion.getSiblings());
        var uContainer = uRegion.getContainer();
        if (uContainer != null)
          chosens.add(uContainer.getContainer());
      }
      defer = SControl.ifThen(SGuard.literal(SLiteral.match(uState.getDeferrableEvents())),
                              SControl.seq(this.symbolTable.setIsDeferred(),
                                           SControl.seq(chosens.stream().map(r -> this.symbolTable.setChosen(r)).collect(toList()))));
    }

    return SControl.ifThen(condition, SControl.seq(inner, SBranching.choice(outgoingsBranches, defer)));
  }

  /**
   * Generate branch for an outgoing compound transition of a state.
   * 
   * @param uState state for transition selection
   * @param uCompound compound transition outgoing from {@code uState}
   * @return transition branch
   */
  private SBranch transitionSelectionBranch(UState uState, UCompoundTransition uCompound) {
    // Generate transition guard
    SGuard guard = SGuard.trueConst();
    if (uCompound.isJoin()) {
      guard = SGuard.and(guard, SGuard.literal(SLiteral.match(UEvent.completion(uState))),
                         SGuard.and(uCompound.getSources().stream().flatMap(toType(UState.class)).map(source -> SGuard.literal(SLiteral.isCompleted(source))).collect(toSet())));
    }
    else {
      UEvent trigger = uCompound.getTrigger();
      if (trigger.isWait())
        trigger = UEvent.completion(trigger.getStates());
      guard = SGuard.and(guard, SGuard.literal(SLiteral.match(trigger)));
      if (uCompound.getTrigger().isCompletion() && this.properties.doDoubleChecks())
        guard = SGuard.and(guard, SGuard.literal(SLiteral.isCompleted(uState)));
      if (uCompound.getTrigger().isTime() && this.properties.doDoubleChecks())
        guard = SGuard.and(guard, SGuard.literal(SLiteral.isTimedOut(trigger)));
    }
    guard = SGuard.and(guard, SGuard.literal(SLiteral.external(uCompound.getGuard(), uCompound.getGuardContext())));

    // The main source of a compound transition originating from a state is a proper state
    UState mainSource = (UState)uCompound.getMainSourceVertex();

    // Generate code to test if one of the inner regions of the main source
    // state has already been chosen (activated); if an inner transition has
    // done so, then it takes priority.
    guard = SGuard.and(guard, SGuard.and(mainSource.getAllRegions().stream().filter(uRegion -> !uRegion.equals(uState.getContainer())).
                                                                             map(uRegion -> this.symbolTable.checkUnchosen(uRegion)).collect(toList())));

    // Generate code to set all exited regions chosen: these are the regions
    // of all states in between the explicit source states and the main source
    // state (including) and all orthogonal regions thereof.
    SStatement exited = SControl.seq(mainSource.getIntermediateVertices(uCompound.getSources()).stream().flatMap(intermediate ->
                                            Stream.concat((intermediate instanceof UState) && ((UState)intermediate).getRegions().size() > 1
                                                              ? ((UState)intermediate).getRegions().stream()
                                                              : Stream.empty(),
                                                          Stream.of(intermediate.getContainer()))).distinct().map(uRegion ->
                                        this.symbolTable.setChosen(uRegion)).collect(toList()));
    return SBranch.branch(guard,
                          SControl.seq(this.symbolTable.updateTransition(uCompound),
                                       exited));
  }

  /**
   * Generate overall transition firing statement.
   *
   * @return transition firing statement
   */
  private SStatement transitionFiring() {
    return transitionFiring(this.uStateMachine.getCompounds());
  }

  /**
   * Generate transition firing statement for a set of compound transitions.
   *
   * @return transition firing statement
   */
  private SStatement transitionFiring(Set<UCompoundTransition> uCompounds) {
    Function<UCompoundTransition, SBranch> firingBranch = uCompound ->
        SBranch.branch(this.symbolTable.checkTransition(uCompound),
                       transitionFiring(uCompound));
    
    if (uCompounds.size() == 0)
      return SCommand.skip();

    if (uCompounds.size() == 1)
      return SBranching.choice(Arrays.asList(firingBranch.apply(uCompounds.iterator().next())),
                               SCommand.skip());

    if (!this.properties.isFixedOrderTransitionFiring())
      return SBranching.loop(uCompounds.stream().map(uCompound -> firingBranch.apply(uCompound)).collect(toList()),
                             SCommand.breakStm());

    var transitionVariables = uCompounds.stream().map(uCompound -> this.symbolTable.getTransitionVariable(uCompound)).collect(toSet());
    var branches = uCompounds.stream().collect(toMap(Function.identity(), uCompound -> firingBranch.apply(uCompound)));
    var branchAssigneds = uCompounds.stream().collect(toMap(Function.identity(), uCompound -> assertNonNull(branches.get(uCompound)).getConstantsAssigned(transitionVariables)));

    // Compute all compounds that show no repetitions
    var acyclic = new HashSet<UCompoundTransition>();
    for (var changed = true; changed; ) {
      var covered = Stream.concat(Stream.of(SConstant.empty()), acyclic.stream().map(uCompound -> SConstant.transition(uCompound))).collect(toSet());
      var remaining = Sets.difference(uCompounds, acyclic);
      changed = false;
      for (var uCompound : remaining) {
        var branchAssigned = assertNonNull(branchAssigneds.get(uCompound));
        if (covered.containsAll(branchAssigned)) {
          acyclic.add(uCompound);
          covered.add(SConstant.transition(uCompound));
          changed = true;
        }
      }
    }

    // Compute all compounds that show repetitions including those that are re-iterated
    var cyclic = Sets.difference(uCompounds, acyclic);
    var assigneds = cyclic.stream().map(uCompound -> (@NonNull Set<SConstant>)assertNonNull(branchAssigneds.get(uCompound))).reduce(Sets.empty(), Sets::union);
    for (var changed = true; changed; ) {
      changed = false;
      var uncovered = acyclic.stream().filter(uCompound -> assigneds.contains(SConstant.transition(uCompound))).collect(toSet());
      if (!uncovered.isEmpty()) {
        acyclic.removeAll(uncovered);
        cyclic.addAll(uncovered);
        assigneds.addAll(uncovered.stream().map(uCompound -> (@NonNull Set<SConstant>)assertNonNull(branchAssigneds.get(uCompound))).reduce(Sets.empty(), Sets::union));
        changed = true;
      }
    }

    // Sort acyclic compound transitions according to their dependencies
    var sortedAcyclic = Lists.topologicalSort(new ArrayList<>(acyclic),
                                              (c1, c2) -> {
                                                            if (assertNonNull(branchAssigneds.get(c2)).contains(SConstant.transition(c1)))
                                                              return 1;
                                                            if (assertNonNull(branchAssigneds.get(c1)).contains(SConstant.transition(c2)))
                                                              return -1;
                                                            return 0;
                                                          });

    return SControl.seq(SBranching.loop(cyclic.stream().map(uCompound -> (@NonNull SBranch)assertNonNull(branches.get(uCompound))).collect(toList()),
                                        SCommand.breakStm()),
                        SControl.seq(sortedAcyclic.stream().map(uCompound -> SControl.ifThen(assertNonNull(branches.get(uCompound)))).collect(toList())));

    /*
    // Store compound transitions in a map with the not-empty check for
    // the transition variable of the main source state region as key
    Map<SGuard, List<SBranch>> branchesMap = new HashMap<>();
    for (UCompoundTransition uCompound : uCompounds) {
      SGuard notEmptyCheck = this.symbolTable.checkTransitionNotEmpty(uCompound.getMainSourceVertex().getContainer());
      List<SBranch> branches = branchesMap.get(notEmptyCheck);
      if (branches == null) {
        branches = new ArrayList<>();
        branchesMap.put(notEmptyCheck, branches);
      }
      branches.add(transitionFiringBranch(uCompound));
    }

    // Loop while some not-empty check is not satisfied
    SBranching firing = SBranching.loop(Arrays.asList(SBranch.branch(SGuard.and(branchesMap.keySet().stream().map(check -> SGuard.neg(check)).collect(toSet())),
                                        SCommand.breakStm())));

    // Add blocks by modifying the else-branch of the previous block
    SBranching previousFiringBlock = firing;
    for (Entry<SGuard, List<SBranch>> branchesEntry : branchesMap.entrySet()) {
      SBranching firingBlock = SBranching.ifThen(branchesEntry.getKey(),
                                                 SBranching.choice(branchesEntry.getValue(), SCommand.skip()));
      previousFiringBlock.setElseStatement(firingBlock);
      previousFiringBlock = firingBlock;
    }
    return firing;
    */
  }

  /**
   * Generate firing statement for a compound transition.
   *
   * @param uCompound a compound transition
   * @return compound transition firing statement
   */
  private SStatement transitionFiring(UCompoundTransition uCompound) {
    UVertex mainSource = uCompound.getMainSourceVertex();
    UVertex mainTarget = uCompound.getMainTargetVertex();
    // For completion transitions we must be in a final state configuration
    Set<UVertex> sources = uCompound.getFinalSources();
    Set<UVertex> targets = uCompound.getTargets();

    // Deactivate source states, execute effects, activate target states
    return SControl.seq(this.symbolTable.resetTransition(uCompound),
                        deactivateRegion(mainSource.getContainer(), mainSource.getIntermediateVertices(sources)),
                        translateUAction(uCompound.getEffect(), uCompound.getEffectContext()),
                        activateRegion(mainTarget.getContainer(), mainTarget.getIntermediateVertices(targets)));
  }

  /**
   * Generate deactivation statement for a region (including active sub-vertices).
   *
   * @param uRegion a region
   * @param subUVertices a set of sub-vertices
   * @return deactivation statement
   */
  private SStatement deactivateRegion(URegion uRegion, Set<UVertex> subUVertices) {
    UVertex subUVertex = uRegion.chooseVertex(subUVertices);
    if (subUVertex != null)
      return deactivateVertex(subUVertex, subUVertices);

    return SBranching.choice(uRegion.getVertices().stream().filter(UVertex::isTarget).map(targetUVertex ->
                                 SBranch.branch(this.symbolTable.checkVertex(targetUVertex),
                                                deactivateVertex(targetUVertex, subUVertices))).collect(toList()));
  }

  /**
   * Generate deactivation statement for given vertex (including active sub-vertices).
   *
   * @param uVertex vertex to be deactivated
   * @param subUVertices list of sub-states, that should be deactivated
   * @return deactivation statement
   */
  private SStatement deactivateVertex(UVertex uVertex, Set<UVertex> subUVertices) {
    return uVertex.new Cases<SStatement>().
        pseudoState(uPseudoState -> deactivatePseudoState(uPseudoState)).
        finalState(uFinalState -> deactivateFinalState(uFinalState)).
        state(uState -> deactivateState(uState, subUVertices)).
        apply();
  }

  /**
   * Generate deactivation statement for given state (including active sub-vertices).
   *
   * @param uState state to be deactivated
   * @param subUVertices list of sub-states, that should be deactivated
   * @return deactivation statement
   */
  private SStatement deactivateState(UState uState, Set<UVertex> subUVertices) {
    // Simple state
    if (uState.getRegions().size() == 0)
      return deactivationActions(uState);

    // Simple composite state
    if (uState.getRegions().size() == 1)
      return SControl.seq(deactivateRegion(uState.getRegions().get(0), subUVertices),
                          deactivationActions(uState));

    // Orthogonal composite, i.e., concurrent state
    if (this.properties.isFixedOrderRegions())
      return SControl.seq(SControl.seq(uState.getRegions().stream().map(uRegion -> deactivateRegion(uRegion, subUVertices)).collect(toList())),
                                       deactivationActions(uState));

    return SControl.seq(SBranching.loop(uState.getRegions().stream().map(uRegion ->
                                              SBranch.branch(this.symbolTable.checkRegionNotSet(uRegion),
                                                                SControl.seq(this.symbolTable.setRegion(uRegion),
                                                                               deactivateRegion(uRegion, subUVertices)))).collect(toList()),
                                          SCommand.breakStm()),
                          SControl.seq(uState.getRegions().stream().map(uRegion ->
                                             this.symbolTable.resetRegion(uRegion)).collect(toList())),
                          deactivationActions(uState));
  }

  /**
   * Deactivate a final state.
   *
   * @param uFinalState a final state
   * @return deactivation statement
   */
  private SStatement deactivateFinalState(UFinalState uFinalState) {
    // Possibly reinitialise history variable to container region's initial state
    // return this.symbolTable.resetHistory(uFinalState.getContainer());
    return this.symbolTable.resetVertex(uFinalState); // -AK
  }

  /**
   * Deactivate a (persistent) pseudo-state.
   *
   * @param uPseudoState a (persistent) pseudo-state
   * @return deactivation statement
   */
  private SStatement deactivatePseudoState(UPseudoState uPseudoState) {
    return this.symbolTable.resetVertex(uPseudoState);
  }

  /**
   * Generate statement for executing actions on exiting a state.
   *
   * Exiting a state initiates the following actions:
   * - uncomplete state
   * - stop timers for time events for this state
   * - execute exit action
   * - possibly save state in region's history
   * - remove state from active configuration
   *
   * @param uState state to be exited
   * @return deactivation statement
   */
  private SStatement deactivationActions(UState uState) {
    SStatement deactivate = SCommand.skip();

    // Uncomplete state
    if (uState.isCompletable() && (this.properties.doDoubleChecks() || uState.hasOutgoingJoinCompletion()))
      deactivate = SControl.seq(deactivate,
                                SPrimitive.uncomplete(uState));

    // Stop timers
    if (uState.hasOutgoingTime())
      deactivate = SControl.seq(deactivate, 
                                SControl.seq(uState.getOutgoingTimes().stream().map(UTransition::getTrigger).distinct().
                                                 map(uTimeEvent -> SPrimitive.stopTimer(uTimeEvent)).collect(toList())));

    // Execute exit action
    if (uState.hasExitAction())
      deactivate = SControl.seq(deactivate,
                                translateUAction(uState.getExitAction(), uState.getExitContext()));

    // Possibly save state in container region's history
    // deactivate = SControl.seq(deactivate,
    //                           this.symbolTable.updateHistory(uState.getContainer())); // -AK

    // Remove state from active configuration
    deactivate = SControl.seq(deactivate,
                              this.symbolTable.resetVertex(uState));

    return deactivate;
  }

  /**
   * Activate a region by activating one of its sub-vertices or its initial state,
   * if there is none.
   * 
   * @param uRegion a region
   * @param subUVertices a set of sub-vertices
   * @return activation statement
   */
  private SStatement activateRegion(URegion uRegion, Set<UVertex> subUVertices) {
    UVertex subUVertex = uRegion.chooseVertex(subUVertices);
    if (subUVertex == null)
      subUVertex = requireNonNull(uRegion.getInitial());
    return SControl.seq(// SCommand.await(this.symbolTable.checkNotChosen(uRegion)),
                        // this.symbolTable.setChosen(uRegion),
                        activateVertex(subUVertex, subUVertices));
  }

  /**
   * Generate activation statement for given vertex (including active sub-states).
   *
   * @param uVertex vertex to be deactivated
   * @param subUVertices set of sub-vertices that should be deactivated
   * @return activation statement
   */
  private SStatement activateVertex(UVertex uVertex, Set<UVertex> subUVertices) {
    return uVertex.new Cases<SStatement>().
        pseudoState(uPseudoState -> activatePseudoState(uPseudoState)).
        finalState(uFinalState -> activateFinalState(uFinalState)).
        state(uState -> activateState(uState, subUVertices)).
        apply();
  }

  /**
   * Generate activation statement for given state (including sub-states).
   *
   * @param state state to be activated
   * @param subUVertices sub-vertices to be activated instead of initial pseudo-states
   * @return activation statement
   */
  private SStatement activateState(UState uState, Set<UVertex> subUVertices) {
    URegion container = uState.getContainer();
    // SStatement chooseContainer = SControl.seq(SCommand.await(this.symbolTable.checkUnchosen(container)),
    //                                           this.symbolTable.setChosen(container)); // this.symbolTable.setChosenUpwards(container));
    SStatement chooseContainer = SControl.seq(SCommand.await(this.symbolTable.checkUnchosenUpwards(container)),
                                              this.symbolTable.setChosenUpwards(container));

    // Simple state
    if (uState.getRegions().size() == 0)
      return SControl.seq(chooseContainer,
                          activationActions(uState));

    // Simple composite state
    if (uState.getRegions().size() == 1)
      return SControl.seq(// chooseContainer,
                          activationActions(uState),
                          activateRegion(uState.getRegions().get(0), subUVertices));

    // Orthogonal composite, i.e., concurrent state
    if (this.properties.isFixedOrderRegions())
      return SControl.seq(// chooseContainer,
                          activationActions(uState),
                          SControl.seq(uState.getRegions().stream().map(uRegion -> activateRegion(uRegion, subUVertices)).collect(toList())));

    return SControl.seq(// chooseContainer,
                        activationActions(uState),
                        SBranching.loop(uState.getRegions().stream().map(uRegion ->
                                            SBranch.branch(this.symbolTable.checkRegionNotSet(uRegion),
                                                              SControl.seq(this.symbolTable.setRegion(uRegion),
                                                                           activateRegion(uRegion, subUVertices)))).collect(toList()),
                                        SCommand.breakStm()),
                        SControl.seq(uState.getRegions().stream().map(uRegion -> this.symbolTable.resetRegion(uRegion)).collect(toList())));
  }

  /**
   * Activate a final state by setting the completion flag for
   * its enclosing composite state.
   *
   * @param uFinalState a final state
   * @return activation statement
   */
  private SStatement activateFinalState(UFinalState uFinalState) {
    URegion container = uFinalState.getContainer();
    UState uCompositeState = container.getContainer();
    SStatement chooseContainer = SControl.seq(SCommand.await(this.symbolTable.checkUnchosen(container)),
                                              this.symbolTable.setChosen(container));
    SStatement activate = SControl.seq(chooseContainer,
                                       this.symbolTable.updateVertex(uFinalState),
                                       this.symbolTable.resetHistory(container)); // -AK

    // Is the final state a vertex of one of the top-level regions
    // or can its enclosing composite state not be completed or
    // left by a completion transition?
    if (uCompositeState == null ||
        !uCompositeState.isCompletable() ||
        !uCompositeState.hasOutgoingCompletion())
      return activate;

    // Is the final state a vertex of a completable simple composite state?
    if (uCompositeState.getRegions().size() == 1)
      return SControl.seq(activate, SPrimitive.complete(this.properties.doDoubleChecks() || uCompositeState.hasOutgoingJoinCompletion(), uCompositeState));

    // Since the containing composite state is completable, all its
    // regions show a final state.  If all other final states have already
    // been reached, the containing composite state is complete.
    return SControl.ifThenElse(SGuard.and(container.getSiblings().stream().map(uRegion -> this.symbolTable.checkVertex(requireNonNull(uRegion.getFinal()))).collect(toSet())),
                               SControl.seq(activate, SPrimitive.complete(this.properties.doDoubleChecks() || uCompositeState.hasOutgoingJoinCompletion(), uCompositeState)),
                               activate);
  }

  /**
   * Activate a (persistent) pseudo-state.
   *
   * @param uPseudoState a (persistent) pseudo-state
   * @return activation statement
   */
  private SStatement activatePseudoState(UPseudoState uPseudoState) {
    // Helper function for choosing a compound transition providing
    // short-cuts for no compound transition at all and a single
    // compound transition with guard {@code true}
    Function<Collection<UCompoundTransition>, SStatement> chooseCompound = uCompoundTransitions -> {
      if (uCompoundTransitions.isEmpty())
        return SPrimitive.fail();
 
      if (uCompoundTransitions.size() == 1) {
        UCompoundTransition uCompoundTransition = uCompoundTransitions.iterator().next();
        if (uCompoundTransition.getGuard().equals(UExpression.falseConst()))
          return SPrimitive.fail();
        if (uCompoundTransition.getGuard().equals(UExpression.trueConst()))
          // return this.symbolTable.updateTransition(uCompoundTransition);
          return transitionFiring(uCompoundTransition);
      }
 
      return SBranching.choice(uCompoundTransitions.stream().map(uCompoundTransition ->
                               SBranch.branch(SGuard.literal(SLiteral.external(uCompoundTransition.getGuard(), uCompoundTransition.getGuardContext())),
                                              // this.symbolTable.updateTransition(uCompoundTransition))).collect(toList()),
                                              transitionFiring(uCompoundTransition))).collect(toList()),
                               SPrimitive.fail());
    };

    // Update state
    SStatement activate = SControl.seq(this.symbolTable.updateVertex(uPseudoState),
                                       this.symbolTable.resetChosen(uPseudoState.getContainer()));

    switch (uPseudoState.getKind()) {
      case INITIAL:
        return SControl.seq(activate, chooseCompound.apply(uPseudoState.getOutgoingCompounds()));

      case SHALLOWHISTORY: {
        List<SBranch> historyBranches = new ArrayList<>();
        List<UCompoundTransition> uOutgoingCompounds = uPseudoState.getOutgoingCompounds();
        if (uOutgoingCompounds.isEmpty()) {
          var uInitial = uPseudoState.getContainer().getInitial();
          if (uInitial != null)
            uOutgoingCompounds = uInitial.getOutgoingCompounds();
        }

        // If the history has not been set before, process outgoing compound transitions
        // of history (pseudo-)state or, if there are none, the outgoing compound transitions
        // of the neighbouring initial pseudostate (default entry)
        historyBranches.add(SBranch.branch(this.symbolTable.checkHistoryEmpty(uPseudoState),
                                           chooseCompound.apply(uOutgoingCompounds)));

        // Otherwise, check which vertex has to be activated
        historyBranches.addAll(uPseudoState.getContainer().getVertices().stream().filter(UVertex::isSource).map(subUVertex ->
                                   SBranch.branch(this.symbolTable.checkHistory(subUVertex),
                                                  activateVertex(subUVertex, new HashSet<>()))).collect(toList()));

        // If still no branch is enabled, something went wrong badly
        SStatement history = SBranching.choice(historyBranches, SPrimitive.fail());

        return SControl.seq(activate, history);
      }

      case CHOICE:
        return SControl.seq(activate, chooseCompound.apply(uPseudoState.getOutgoingCompounds()));

      //$CASES-OMITTED$
      default:
        return activate;
    }
  }

  /**
   * Generate statement for executing actions on entering a state.
   *
   * Entering a state initiates the following actions
   * - execute entry action
   * - start timers for time events for this state
   * - update state variable
   * - complete a simple state
   *
   * @param uState state to be entered
   * @return enter statement
   */
  private SStatement activationActions(UState uState) {
    SStatement activate = SCommand.skip();

    if (uState.hasEntryAction())
      activate = SControl.seq(activate,
                              translateUAction(uState.getEntryAction(), uState.getEntryContext()));

    if (uState.hasOutgoingTime())
      activate = SControl.seq(activate,
                              SControl.seq(uState.getOutgoingTimes().stream().map(UTransition::getTrigger).distinct().
                                               map(uTimeEvent -> SPrimitive.startTimer(this.properties.doDoubleChecks(), uTimeEvent)).collect(toList())));

    // Update state and possibly update history -AK
    activate = SControl.seq(activate,
                            this.symbolTable.updateVertex(uState),
                            this.symbolTable.updateHistory(uState.getContainer())); // -AK

    // A simple state is immediately completed
    if (uState.getRegions().size() == 0 &&
        uState.isCompletable() &&
        uState.hasOutgoingCompletion())
      activate = SControl.seq(activate,
                              SPrimitive.complete(this.properties.doDoubleChecks() || uState.hasOutgoingJoinCompletion(), uState));

    return activate;
  }

  /**
   * Translate UML action into Smile
   *
   * @param uAction UML action
   * @param uContext UML context
   * @param symbolTable symbol table for translation into Smile
   * @return translated Smile statement
   */
  static SStatement translateUAction(UAction uAction, UContext uContext) {
    return new UActionTranslator(uContext).statement(uAction);
  }
  
  /**
   * Translator of UML actions
   */
  private static final class UActionTranslator implements UAction.InContextVisitor<SStatement> {
    private UContext uContext;
    private @Nullable UAction uAction;
    
    private UActionTranslator(UContext uContext) {
      this.uContext = uContext;
    }
    
    private SStatement statement(UAction uAction) {
      this.uAction = uAction;
      return uAction.accept(this);
    }
    
    public UContext getContext() {
      return this.uContext;
    }
    
    public SStatement onSkip() {
      return SCommand.skip();
    }
    
    public SStatement onChoice(UAction left, UAction right) {
      return SBranching.choice(SBranch.branch(SGuard.trueConst(), this.statement(left)),
                               SBranch.branch(SGuard.trueConst(), this.statement(right)));
    }
    
    public SStatement onParallel(UAction left, UAction right) {
      return this.statement(UAction.par(left, right).getInterleavingNormalForm());
    }
    
    public SStatement onSequential(UAction left, UAction right) {
      return SControl.seq(this.statement(left), this.statement(right));
    }
    
    public SStatement onConditional(UExpression condition, UAction left, UAction right) {
      return SControl.ifThenElse(SGuard.literal(SLiteral.external(condition, this.uContext)), this.statement(left), this.statement(right));
    }
    
    public SStatement onAssertion(UExpression uExpression) {
      return SPrimitive.external(requireNonNull(this.uAction), this.uContext);
    }
    
    public SStatement onSelfInvocation(UBehavioural uBehavioural, List<UExpression> uArguments) {
      return this.invocation(uBehavioural);
    }

    public SStatement onInvocation(UStatual uStatual, UBehavioural uBehavioural, List<UExpression> uArguments) {
      return this.invocation(uBehavioural);
    }

    public SStatement onArrayInvocation(UAttribute uAttribute, UExpression uOffset, UBehavioural uBehavioural, List<UExpression> uArguments) {
      return this.invocation(uBehavioural);
    }

    private SStatement invocation(UBehavioural uBehavioural) {
      // Only non-static operation calls are suspenders
      return SPrimitive.external(requireNonNull(this.uAction), this.uContext, uBehavioural.cases(o -> true, r -> false) && !uBehavioural.isStatic());
    }

    public SStatement onAssignment(UAttribute uAttribute, UExpression uExpression) {
      return SPrimitive.external(requireNonNull(this.uAction), this.uContext);
    }
    
    public SStatement onArrayAssignment(UAttribute uAttribute, UExpression uOffset, UExpression uExpression) {
      return SPrimitive.external(requireNonNull(this.uAction), this.uContext);
    }
    
    public SStatement onPost(UAttribute uAttribute, UAction.PostKind kind) {
      return SPrimitive.external(requireNonNull(this.uAction), this.uContext);
    }
    
    public SStatement onArrayPost(UAttribute uAttribute, UExpression uOffset, UAction.PostKind kind) {
      return SPrimitive.external(requireNonNull(this.uAction), this.uContext);
    }
  }
}
