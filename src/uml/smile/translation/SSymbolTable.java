package uml.smile.translation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import control.Properties;
import uml.smile.SCommand;
import uml.smile.SConstant;
import uml.smile.SControl;
import uml.smile.SGuard;
import uml.smile.SLiteral;
import uml.smile.SStatement;
import uml.smile.SValue;
import uml.smile.SVariable;
import uml.statemachine.UPseudoState;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UCompoundTransition;

import static util.Objects.requireNonNull;

import static java.util.stream.Collectors.toSet;


/**
 * Symbol table for translation of (simple) UML models to Smile
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class SSymbolTable {
  private UStateMachine uStateMachine;
  private control.Properties properties;
  private Map<URegion, String> uRegionNames = new HashMap<>();
  private Map<String, SVariable> stateVariables = new HashMap<>();
  private Map<String, SVariable> transitionVariables = new HashMap<>();
  private Map<String, SVariable> chosenVariables = new HashMap<>();
  private Map<UPseudoState, SVariable> historyVariables = new HashMap<>();
  private Map<URegion, SVariable> regionVariables = new HashMap<>();
  private @Nullable SVariable isDeferredVariable = null;
  private @Nullable SVariable breakFlag = null;
  private @Nullable SVariable suspendFlag = null;
  private Set<String> names = new HashSet<String>();
  private int namesCounter = 0;

  /**
   * Create a new symbol table for translating a state machine into Smile.
   *
   * @param uStateMachine (UML) state machine
   * @param machine corresponding (Smile) machine
   */
  public SSymbolTable(UStateMachine uStateMachine, control.Properties properties) {
    this.uStateMachine = uStateMachine;
    this.properties = properties;
    for (var uRegion : uStateMachine.getAllRegions())
      this.uRegionNames.put(uRegion, uniqueName(uRegion.getName()));
    // this.uRegionNames = URegionTree.create(uStateMachine).getRegionNames();
  }

  /**
   * @return symbol table's underlying (UML) state machine
   */
  public UStateMachine getStateMachine() {
    return this.uStateMachine;
  }

  /**
   * @return symbol table's underlying properties
   */
  public Properties getProperties() {
    return this.properties;
  }

  /**
   * Determine Smile vertex variable for a vertex's container region.
   *
   * @param uVertex a (UML) vertex
   * @return Smile variable
   */
  public SVariable getVertexVariable(UVertex uVertex) {
    return this.getVertexVariable(uVertex.getContainer());
  }

  /**
   * Determine the set of possible values a Smile vertex variable
   * for a region can take.
   * 
   * @param uRegion a UML region
   * @return set of possible Smile values
   */
  private Set<SValue> getVertexDomain(URegion uRegion) {
    return Stream.concat(Stream.of(SValue.constant(SConstant.empty())),
                                   this.getStateMachine().getVertices().stream().filter(uVertex -> uRegion.equals(uVertex.getContainer())).
                                                                                 map(uVertex -> SValue.constant(SConstant.vertex(uVertex)))).collect(toSet());
  }

  /**
   * Determine Smile vertex variable for a region.
   *
   * @param uRegion (UML) region
   * @return Smile variable
   */
  SVariable getVertexVariable(URegion uRegion) {
    String uRegionName = requireNonNull(this.uRegionNames.get(uRegion));
    SVariable stateVariable = this.stateVariables.get(uRegionName);
    if (stateVariable != null)
      return stateVariable;

    stateVariable = SVariable.state(uniqueName(uRegionName + "_state"), this.getVertexDomain(uRegion));
    this.stateVariables.put(uRegionName, stateVariable);
    return stateVariable;
  }

  /**
   * Determine a Smile expression for checking that the vertex variable
   * for a vertex is currently set to this vertex.
   *
   * @param uVertex a (UML) vertex
   * @return Smile expression
   */
  public SGuard checkVertex(UVertex uVertex) {
    return SGuard.literal(SLiteral.eq(this.getVertexVariable(uVertex), SConstant.vertex(uVertex)));
  }

  /**
   * Determine a Smile statement for update the vertex variable for a vertex
   * to this vertex.
   *
   * @param uVertex a (UML) vertex
   * @return Smile statement
   */
  SStatement updateVertex(UVertex uVertex) {
    return SCommand.assign(this.getVertexVariable(uVertex), SConstant.vertex(uVertex));
  }

  /**
   * Determine a Smile statement for resetting the vertex variable for a vertex
   * to empty.
   *
   * @param uVertex a (UML) vertex
   * @return Smile statement
   */
  SStatement resetVertex(UVertex uVertex) {
    return SCommand.assign(this.getVertexVariable(uVertex), SConstant.empty());
  }

  /**
   * Determine Smile transition variable for a compound transition.
   *
   * @param uCompoundTransition a (UML) compound transition
   * @return Smile variable
   */
  public SVariable getTransitionVariable(UCompoundTransition uCompoundTransition) {
    return this.getTransitionVariable(uCompoundTransition.getMainSourceVertex().getContainer());
  }

  /**
   * Determine the set of possible values a Smile transition variable
   * for a region can take.
   * 
   * @param uRegion a UML region
   * @return set of possible Smile values
   */
  private Set<SValue> getTransitionDomain(URegion uRegion) {
    return Stream.concat(Stream.of(SValue.constant(SConstant.empty())),
                         this.getStateMachine().getCompounds().stream().filter(uCompoundTransition -> uRegion.equals(uCompoundTransition.getMainSourceVertex().getContainer())).
                                                                        map(uCompoundTransition -> SValue.constant(SConstant.transition(uCompoundTransition)))).collect(toSet());
  }

  /**
   * Determine Smile transition variable for a region.
   *
   * @param uRegion a (UML) region
   * @return Smile variable
   */
  SVariable getTransitionVariable(URegion uRegion) {
    String uRegionName = requireNonNull(this.uRegionNames.get(uRegion));
    SVariable transitionVariable = this.transitionVariables.get(uRegionName);
    if (transitionVariable != null)
      return transitionVariable;

    transitionVariable = SVariable.transition(uniqueName(uRegionName + "_transition"), this.getTransitionDomain(uRegion));
    this.transitionVariables.put(uRegionName, transitionVariable);
    return transitionVariable;
  }

  /**
   * Determine Smile statement for updating a transition variable to
   * a compound transition.
   *
   * @param uCompoundTransition a (UML) compound transition
   * @return Smile statement
   */
  SStatement updateTransition(UCompoundTransition uCompoundTransition) {
    return SCommand.assign(this.getTransitionVariable(uCompoundTransition), SValue.constant(SConstant.transition(uCompoundTransition)));
  }

  /**
   * Determine Smile statement for resetting a transition variable to
   * empty.
   *
   * @param uCompoundTransition a (UML) compound transition
   * @return Smile statement
   */
  SStatement resetTransition(UCompoundTransition uCompoundTransition) {
    return SCommand.assign(this.getTransitionVariable(uCompoundTransition), SConstant.empty());
  }

  /**
   * Determine Smile statement for resetting a transition variable to
   * empty.
   *
   * @param uRegion a (UML) region
   * @return Smile statement
   */
  SStatement resetTransition(URegion uRegion) {
    return SCommand.assign(this.getTransitionVariable(uRegion), SConstant.empty());
  }

  /**
   * Determine Smile expression for checking a transition variable for
   * a compound transition.
   *
   * @param uCompoundTransition a (UML) compound transition
   * @return Smile statement
   */
  SGuard checkTransition(UCompoundTransition uCompoundTransition) {
    return SGuard.literal(SLiteral.eq(this.getTransitionVariable(uCompoundTransition), SConstant.transition(uCompoundTransition)));
  }

  /**
   * Determine Smile expression for checking whether a transition variable for
   * a region is empty.
   *
   * @param uCompoundTransition a (UML) compound transition
   * @return Smile statement
   */
  SGuard checkTransitionNotEmpty(URegion uRegion) {
    return SGuard.literal(SLiteral.neq(this.getTransitionVariable(uRegion), SConstant.empty()));
  }

  /**
   * Determine Smile chosen flag for a region.
   *
   * @param uRegion a (UML) region
   * @return Smile variable
   */
  private SVariable getChosenFlag(URegion uRegion) {
    var uRegionName = requireNonNull(this.uRegionNames.get(uRegion));
    var chosenVariable = this.chosenVariables.get(uRegionName);
    if (chosenVariable != null)
      return chosenVariable;

    chosenVariable = SVariable.flag(uniqueName(uRegionName + "_chosen"));
    this.chosenVariables.put(uRegionName, chosenVariable);
    return chosenVariable;
  }

  /**
   * Determine a Smile statement for resetting the chosen flag for a region.
   *
   * @param uRegion a (UML) region
   * @return Smile statement
   */
  SStatement resetChosen(URegion uRegion) {
    return SCommand.assign(this.getChosenFlag(uRegion), SValue.falseValue());
  }

  /**
   * Determine a Smile statement for setting the chosen flag for a region.
   *
   * @param uRegion a (UML) region
   * @return Smile statement
   */
  SStatement setChosen(URegion uRegion) {
    return SCommand.assign(this.getChosenFlag(uRegion), SValue.trueValue());
  }

  SStatement setChosenUpwards(URegion uRegion) {
    var chosen = SCommand.assign(this.getChosenFlag(uRegion), SValue.trueValue());
    var uRegionContainer = uRegion.getContainer();
    if (uRegionContainer == null) 
      return chosen;
    return SControl.seq(chosen, setChosenUpwards(uRegionContainer.getContainer()));
  }

  /**
   * Determine a Smile guard for checking whether the chosen flag for a region is not set.
   *
   * @param uRegion a (UML) region
   * @return Smile guard
   */
  SGuard checkUnchosen(URegion uRegion) {
    return SGuard.literal(SLiteral.eq(this.getChosenFlag(uRegion), false));
  }

  SGuard checkUnchosenUpwards(URegion uRegion) {
    var check = SGuard.literal(SLiteral.eq(this.getChosenFlag(uRegion), false));
    if (!uRegion.isOrthogonal())
      return check;
    var uRegionContainer = uRegion.getContainer();
    if (uRegionContainer == null)
      return check;
    return SGuard.and(check, SGuard.or(this.checkUnchosenUpwards(uRegionContainer.getContainer()),
                                       uRegion.getSiblings().stream().map(uR -> SGuard.neg(checkUnchosen(uR))).reduce(SGuard.falseConst(), SGuard::or)));
  }

  /**
   * Determine the set of possible values a Smile history variable
   * for a region can take.
   * 
   * @param uRegion a UML region
   * @return set of possible Smile values
   */
  private Set<SValue> getHistoryDomain(URegion uRegion) {
    return Stream.concat(Stream.of(SValue.constant(SConstant.empty())),
                                   this.getStateMachine().getVertices().stream().filter(uVertex -> uVertex.new Cases<Boolean>().state(__ -> true).otherwise(() -> false).apply() && uRegion.equals(uVertex.getContainer())).
                                                                                 map(uVertex -> SValue.constant(SConstant.vertex(uVertex)))).collect(toSet());
  
        }

  /**
   * Determine Smile variable for a history pseudo-state.
   *
   * @param uHistory a (UML) history pseudo-state
   * @return Smile variable
   */
  private SVariable getHistoryVariable(UPseudoState uHistory) {
    SVariable historyVariable = this.historyVariables.get(uHistory);
    if (historyVariable != null)
      return historyVariable;

    var uHistoryRegion = uHistory.getContainer();
    historyVariable = SVariable.history(uniqueName(uHistoryRegion.getName() + "_history"), this.getHistoryDomain(uHistoryRegion));
    this.historyVariables.put(uHistory, historyVariable);
    return historyVariable;
  }

  /**
   * Determine a Smile statement for updating the history variable for a
   * region if there is a (shallow) history state in this region.
   *
   * @param uRegion a (UML) region
   * @return Smile statement
   */
  SStatement updateHistory(URegion uRegion) {
    UPseudoState uHistory = uRegion.getShallowHistory();
    if (uHistory == null)
      return SCommand.skip();

    return SCommand.assign(this.getHistoryVariable(uHistory), this.getVertexVariable(uRegion));
  }

  /**
   * Determine a Smile statement for resetting the history variable for a
   * region to empty if there is a (shallow) history state in this region.
   *
   * @param uRegion a (UML) region
   * @return Smile statement
   */
  SStatement resetHistory(URegion uRegion) {
    UPseudoState uHistory = uRegion.getShallowHistory();
    if (uHistory == null)
      return SCommand.skip();

    return SCommand.assign(this.getHistoryVariable(uHistory), SConstant.empty());
  }

  /**
   * Determine a Smile statement for checking whether a history variable
   * is set to a given vertex.
   *
   * @param uVertex a (UML) vertex
   * @return Smile expression
   */
  SGuard checkHistory(UVertex uVertex) {
    UPseudoState uHistory = uVertex.getContainer().getShallowHistory();
    if (uHistory == null)
      return SGuard.falseConst();

    return SGuard.literal(SLiteral.eq(this.getHistoryVariable(uHistory), SConstant.vertex(uVertex)));
  }

  /**
   * Determine a Smile statement for checking whether a history variable
   * is empty.
   *
   * @param uPseudoState a (UML) pseudo-state
   * @return Smile expression
   */
  SGuard checkHistoryEmpty(UPseudoState uPseudoState) {
    if (uPseudoState.getKind() != UPseudoState.Kind.SHALLOWHISTORY)
      return SGuard.trueConst();
    return SGuard.literal(SLiteral.eq(this.getHistoryVariable(uPseudoState), SConstant.empty()));
  }

  /**
   * Determine Smile region variable for a region in an orthogonal
   * composite state.  If the region actually is top-level, then {@code null} is
   * returned.
   *
   * @param uRegion a (UML) region (of an orthogonal composite state)
   * @return Smile region variable and index
   */
  private @Nullable SVariable getRegionVariable(URegion uRegion) {
    SVariable regionVariable = this.regionVariables.get(uRegion);
    if (regionVariable != null)
      return regionVariable;

    UState containerUState = uRegion.getContainer();
    if (containerUState == null)
      return null;

    List<URegion> uRegions = containerUState.getRegions();
    int index = 0;
    while (index < uRegions.size()) {
      if (uRegion.equals(uRegions.get(index)))
        break;
      index++;
    }

    regionVariable = SVariable.flag(uniqueName(containerUState.getName() + "_region" + index));
    this.regionVariables.put(uRegion, regionVariable);
    return regionVariable;
  }

  /**
   * Determine a Smile statement for setting a region variable to {@code true}
   * for the index of the given region.
   *
   * @param uRegion a (UML) region
   * @return Smile statement
   */
  SStatement setRegion(URegion uRegion) {
    SVariable regionVariable = this.getRegionVariable(uRegion);
    if (regionVariable == null)
      return SCommand.skip();
    return SCommand.assign(regionVariable, SValue.trueValue());
  }

  /**
   * Determine a Smile statement for setting a region variable to {@code false}
   * for the index of the given region.
   *
   * @param uRegion a (UML) region
   * @return Smile statement
   */
  SStatement resetRegion(URegion uRegion) {
    SVariable regionVariable = this.getRegionVariable(uRegion);
    if (regionVariable == null)
      return SCommand.skip();
    return SCommand.assign(regionVariable, SValue.falseValue());
  }

  /**
   * Determine a Smile expression for checking whether a region variable for the index
   * of the given region is not set, i.e., equal to {@code false}.
   *
   * @param uRegion a (UML) region
   * @return Smile expression
   */
  SGuard checkRegionNotSet(URegion uRegion) {
    SVariable regionVariable = this.getRegionVariable(uRegion);
    if (regionVariable == null)
      return SGuard.trueConst();
    return SGuard.literal(SLiteral.eq(regionVariable, false));
  }

  /**
   * Determine Smile is-deferred variable.
   *
   * @return Smile variable
   */
  private SVariable getIsDeferredVariable() {
    SVariable isDeferredVariable = this.isDeferredVariable;
    if (isDeferredVariable != null)
      return isDeferredVariable;

    isDeferredVariable = SVariable.flag(uniqueName("isDeferred"));
    this.isDeferredVariable = isDeferredVariable;
    return isDeferredVariable;
  }

  /**
   * Determine a Smile statement for setting the is-deferred variable to {@code true}.
   *
   * @return Smile statement
   */
  SStatement setIsDeferred() {
    return SCommand.assign(this.getIsDeferredVariable(), SValue.trueValue());
  }

  /**
   * Determine a Smile statement for setting the is-deferred variable to {@code false}.
   *
   * @return Smile statement
   */
  SStatement resetIsDeferred() {
    return SCommand.assign(this.getIsDeferredVariable(), SValue.falseValue());
  }

  /**
   * Determine a Smile expression for checking whether the is-deferred variable
   * is set, i.e., equal to {@code true}.
   *
   * @return Smile expression
   */
  SGuard checkIsDeferred() {
    return SGuard.literal(SLiteral.eq(this.getIsDeferredVariable(), true));
  }

  /**
   * Determine Smile variable indicating that a loop shall be exited.
   *
   * @return a Smile variable
   */
  public SVariable getBreakFlag() {
    SVariable breakFlag = this.breakFlag;
    if (breakFlag != null)
      return breakFlag;

    breakFlag = SVariable.flag(uniqueName("BREAK"));
    this.breakFlag = breakFlag;
    return breakFlag;
  }

  /**
   * Determine Smile variable indicating that execution shall be suspended.
   *
   * @return a Smile variable
   */
  public SVariable getSuspendFlag() {
    SVariable suspendFlag = this.suspendFlag;
    if (suspendFlag != null)
      return suspendFlag;

    suspendFlag = SVariable.flag(uniqueName("SUSPEND"));
    this.suspendFlag = suspendFlag;
    return suspendFlag;
  }

  /**
   * Determine a unique name for a name.
   * 
   * @param name a name
   * @return a unique name
   */
  private String uniqueName(String name) {
    char[] characters = name.toCharArray();
    for (int i = 0; i < characters.length; i++) {
      if (!Character.isLetterOrDigit(characters[i]))
        characters[i] = '_';
    }
    String uniqueName = new String(characters);
    if (uniqueName.equals("") || this.names.contains(uniqueName)) {
      String tmpName;
      do {
        tmpName = uniqueName + this.namesCounter++;
      } while (this.names.contains(tmpName));
      uniqueName = tmpName;
    }
    this.names.add(uniqueName);
    return uniqueName;
  }
}
