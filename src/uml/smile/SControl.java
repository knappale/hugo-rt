package uml.smile;

import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Sets;


@NonNullByDefault
public abstract class SControl extends SStatement {
  public interface Visitor<T> extends SBranching.Visitor<T> {
    public T onAtomic(SStatement statement);
    public T onSequential(SStatement leftStatement, SStatement rightStatement);
  }

  /**
   * Create a statement which represents an atomic block.
   *
   * @return atomic block of {@code statement}
   */
  public static SStatement atomic(SStatement statement) {
    return new SAtomic(statement);
  }

  /**
   * Create a statement representing sequential composition of two statements.
   * 
   * @param leftStatement first statement
   * @param rightStatement second statement
   * @return sequential composition of {@code leftStatement} and {@code rightStatement}
   */
  public static SStatement seq(SStatement leftStatement, SStatement rightStatement) {
    if (leftStatement.isEmpty())
      return rightStatement;
    if (rightStatement.isEmpty())
      return leftStatement;
    return new SSeq(leftStatement, rightStatement);
  }

  /**
   * Create a statement representing sequential composition of a list of statements.
   * 
   * @param statements a list of statements
   * @return sequential composition of {@code statements}
   */
  public static SStatement seq(@NonNull SStatement... statements) {
    SStatement seq = SCommand.skip();
    for (SStatement statement : statements)
      seq = SControl.seq(seq, statement);
    return seq;
  }

  /**
   * Create a control statement representing sequential composition of a list of statements.
   * 
   * @param statements a list of statements
   * @return sequential composition of {@code statements}
   */
  public static SStatement seq(List<SStatement> statements) {
    SStatement seq = SCommand.skip();
    for (SStatement statement : statements)
      seq = SControl.seq(seq, statement);
    return seq;
  }

  /**
   * Create a control statement representing an if-then-else.
   * 
   * @param test test expression
   * @param thenStatement then clause statement
   * @param elseStatement else clause statement
   * @return a statement
   */
  public static SStatement ifThenElse(SGuard test, SStatement thenStatement, SStatement elseStatement) {
    if (test.equals(SGuard.trueConst()))
      return thenStatement;
    if (test.equals(SGuard.falseConst()))
      return elseStatement;
    return SBranching.choice(Sets.singleton(SBranch.branch(test, thenStatement)), elseStatement);
  }

  /**
   * Create a statement representing an if-then.
   * 
   * @param test test expression
   * @param thenStatement then clause statement
   * @return a statement
   */
  public static SStatement ifThen(SGuard test, SStatement thenStatement) {
    return SControl.ifThenElse(test, thenStatement, SCommand.skip());
  }

  /**
   * Create a statement representing an if-then from a single branch.
   * 
   * @param branch a branch
   * @return a statement
   */
  public static SStatement ifThen(SBranch branch) {
    return SBranching.choice(Sets.singleton(branch), SCommand.skip());
  }

  /**
   * Accept a visitor.
   *
   * @param visitor a visitor
   * @return visit's result
   */
  abstract <T> T accept(Visitor<T> visitor);

  @Override
  public <T> T accept(SStatement.Visitor<T> visitor) {
    return this.accept((Visitor<T>)visitor);
  }
}
