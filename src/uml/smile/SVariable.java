package uml.smile;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Sets;


/**
 * Smile variable
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class SVariable {
  static enum Kind {
    STATE,
    TRANSITION,
    HISTORY,
    FLAG;

    public String getTypeName() {
      switch (this) {
        case STATE: return "state";
        case TRANSITION: return "transition";
        case HISTORY: return "history";
        case FLAG: return "flag";
        default: throw new IllegalStateException("Unknown Smile variable kind.");
      }
    }

    public SValue getInitialValue() {
      switch (this) {
        case STATE: return SValue.emptyValue();
        case TRANSITION: return SValue.emptyValue();
        case HISTORY: return SValue.emptyValue();
        case FLAG: return SValue.falseValue();
        default: throw new IllegalStateException("Unknown Smile variable kind.");
      }
    }
  }

  private Kind kind;
  private String name = "";
  private Set<SValue> domain = new HashSet<>();

  @NonNullByDefault({})
  public class Cases<T> {
    private Function<@NonNull SVariable, T> stateFun = null;
    private Function<@NonNull SVariable, T> transitionFun = null;
    private Function<@NonNull SVariable, T> historyFun = null;
    private Function<@NonNull SVariable, T> flagFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases<T> state(Function<@NonNull SVariable, T> stateFun) {
      this.stateFun = stateFun;
      return this;
    }
    
    public Cases<T> transition(Function<@NonNull SVariable, T> transitionFun) {
      this.transitionFun = transitionFun;
      return this;
    }
    
    public Cases<T> history(Function<@NonNull SVariable, T> historyFun) {
      this.historyFun = historyFun;
      return this;
    }
    
    public Cases<T> flag(Function<@NonNull SVariable, T> flagFun) {
      this.flagFun = flagFun;
      return this;
    }
    
    public Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply() {
      switch (SVariable.this.kind) {
        case STATE:
          if (this.stateFun != null)
            return this.stateFun.apply(SVariable.this);
          break;
        case TRANSITION:
          if (this.transitionFun != null)
            return this.transitionFun.apply(SVariable.this);
          break;
        case HISTORY:
          if (this.historyFun != null)
            return this.historyFun.apply(SVariable.this);
          break;
        case FLAG:
          if (this.flagFun != null)
            return this.flagFun.apply(SVariable.this);
          break;
        default:
          break;
      }

      assert (this.otherwiseFun != null) : "No default for case distinction on Smile variable type";
      return this.otherwiseFun.get();
    }
  }

  private SVariable(Kind kind, String name, Set<SValue> domain) {
    this.kind = kind;
    this.name = name;
    this.domain.addAll(domain);
  }

  /**
   * Create a state variable.
   */
  public static SVariable state(String name, Set<SValue> domain) {
    return new SVariable(Kind.STATE, name, domain);
  }

  /**
   * Create a transition variable.
   */
  public static SVariable transition(String name, Set<SValue> domain) {
    return new SVariable(Kind.TRANSITION, name, domain);
  }

  /**
   * Create a history variable.
   */
  public static SVariable history(String name, Set<SValue> domain) {
    return new SVariable(Kind.HISTORY, name, domain);
  }

  /**
   * Create a flag variable.
   */
  public static SVariable flag(String name) {
    return new SVariable(Kind.FLAG, name, Sets.elements(SValue.falseValue(), SValue.trueValue()));
  }

  /**
   * @return whether variable represents a state
   */
  public boolean isState() {
    return (this.kind == Kind.STATE) || (this.kind == Kind.HISTORY);
  }

  /**
   * @return whether variable represents a transition
   */
  public boolean isTransition() {
    return (this.kind == Kind.TRANSITION);
  }

  /**
   * @return whether variable represents a history
   */
  public boolean isHistory() {
    return (this.kind == Kind.HISTORY);
  }

  /**
   * @return whether variable represents a flag
   */
  public boolean isFlag() {
    return (this.kind == Kind.FLAG);
  }

  /**
   * @return variable's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return variable's initial values
   */
  public SValue getInitialValue() {
    return this.kind.getInitialValue();
  }

  /**
   * @return variable's domain
   */
  public Set<SValue> getDomain() {
    return this.domain;
  }

  /**
   * @return variable declaration
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.kind.getTypeName());
    resultBuilder.append(" ");
    resultBuilder.append(this.name);
    resultBuilder.append(";");
    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SVariable other)
      return this.kind == other.kind &&
             Objects.equals(this.name, other.name);
    return false;
  }

  @Override
  public String toString(){
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Variable [name = ");
    resultBuilder.append(this.name);
    resultBuilder.append(", type = ");
    resultBuilder.append(this.kind.getTypeName());
    resultBuilder.append(", domain = ");
    resultBuilder.append(Formatter.set(this.domain));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
