package uml.smile;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.statemachine.UEvent;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UCompoundTransition;

import static util.Objects.requireNonNull;


/**
 * Smile constant
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class SConstant {
  public enum Kind {
    EMPTY,
    VERTEX,
    TRANSITION,
    EVENT;
 
    String getName() {
      switch (this) {
        case EMPTY: return "empty";
        case VERTEX: return "vertex";
        case TRANSITION: return "transition";
        case EVENT: return "event";
      }
      throw new IllegalStateException("Unknown Smile constant kind.");
    }
  }

  @NonNullByDefault({})
  public class Cases<T> {
    private Supplier<T> emptyFun = null;
    private Function<@NonNull UVertex, T> vertexFun = null;
    private Function<@NonNull UCompoundTransition, T> transitionFun = null;
    private Function<@NonNull UEvent, T> eventFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases<T> empty(Supplier<T> emptyFun) {
      this.emptyFun = emptyFun;
      return this;
    }

    public Cases<T> vertex(Function<@NonNull UVertex, T> stateFun) {
      this.vertexFun = stateFun;
      return this;
    }
    
    public Cases<T> transition(Function<@NonNull UCompoundTransition, T> transitionFun) {
      this.transitionFun = transitionFun;
      return this;
    }
    
    public Cases<T> event(Function<@NonNull UEvent, T> eventFun) {
      this.eventFun = eventFun;
      return this;
    }

    public Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply() {
      switch (kind) {
        case EMPTY: return (emptyFun != null ? emptyFun.get() : otherwiseFun.get());
        case VERTEX: return (vertexFun != null && vertex != null ? vertexFun.apply(vertex) : otherwiseFun.get());
        case TRANSITION: return (transitionFun != null && transition != null ? transitionFun.apply(transition) : otherwiseFun.get());
        case EVENT: return (eventFun != null && event != null ? eventFun.apply(event) : otherwiseFun.get());
      }
      throw new IllegalStateException("Unknown Smile constant kind.");
    }
  }

  private Kind kind;
  private @Nullable UVertex vertex = null;
  private @Nullable UCompoundTransition transition = null;
  private @Nullable UEvent event = null;

  private SConstant(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create constant representing a UML state.
   */
  public static SConstant vertex(UVertex state) {
    SConstant c = new SConstant(Kind.VERTEX);
    c.vertex = state;
    return c;
  }

  /**
   * Create constant representing a UML compound transition.
   */
  public static SConstant transition(UCompoundTransition transition) {
    SConstant c = new SConstant(Kind.TRANSITION);
    c.transition = transition;
    return c;
  }

  /**
   * Create constant representing a UML event.
   */
  public static SConstant event(UEvent event) {
    SConstant c = new SConstant(Kind.EVENT);
    c.event = event;
    return c;
  }

  /**
   * Create constant representing empty.
   */
  public static SConstant empty() {
    SConstant c = new SConstant(Kind.EMPTY);
    return c;
  }

  /**
   * @return constant's kind
   */
  public Kind getKind() {
    return this.kind;
  }

  /**
   * @return constant's underlying vertex
   */
  public UVertex getVertex() {
    UVertex this_vertex = this.vertex;
    if (this_vertex == null)
      throw new IllegalStateException("Requesting a state from the non-vertex constant `" + this.toString() + "'");
    return this_vertex;
  }

  /**
   * @return constant's underlying transition
   */
  public UCompoundTransition getTransition() {
    UCompoundTransition this_transition = this.transition;
    if (this_transition == null)
      throw new IllegalStateException("Requesting a transition from the non-transition constant `" + this.toString() + "'");
    return this_transition;
  }

  /**
   * @return constant's underlying event
   */
  public UEvent getEvent() {
    UEvent this_event = this.event;
    if (this_event == null)
      throw new IllegalStateException("Requesting an event from the non-event constant `" + this.toString() + "'");
    return this_event;
  }

  /**
   * @return constant's name
   */
  public String getName() {
    return this.new Cases<String>().
             empty(() -> "empty").
             vertex(uVertex -> uVertex.getIdentifier().toString()). 
             transition(uCompound -> uCompound.getFullName()).
             event(uEvent -> uEvent.getName()).
             apply();
  }

  /**
   * Determine the set of Smile constants that are used in this
   * constant.
   * 
   * @return set of Smile constants used in this constant
   */
  public Set<SConstant> getConstants() {
    Set<SConstant> constants = new HashSet<>();
    constants.add(this);
    return constants;
  }
  
  /**
   * @return variable declaration
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("const ");
    resultBuilder.append(this.getKind().getName());
    resultBuilder.append(" ");
    resultBuilder.append(this.getName());
    resultBuilder.append(";");
    return requireNonNull(resultBuilder.toString());
  }

  /**
   * @return variable's hash code
   */
  public int hashCode() {
    return Objects.hash(this.event,
                        this.vertex,
                        this.transition);
  }

  /**
   * Determine whether an object is equals to this constant
   *
   * Equality of constants is defined to be extensional, as constants
   * are defined from model elements
   *
   * @return whether object is equal to this constant
   */
  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SConstant other)
      return (Objects.equals(this.event, other.event) &&
              Objects.equals(this.vertex, other.vertex) &&
              Objects.equals(this.transition, other.transition));
    return false;
  }

  @Override
  public String toString(){
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("<");
    resultBuilder.append(this.getName());
    resultBuilder.append(">");
    return resultBuilder.toString();
  }
}
