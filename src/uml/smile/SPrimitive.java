package uml.smile;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAction;
import uml.UContext;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import util.Formatter;
import util.Sets;

import static util.Objects.requireNonNull;


@NonNullByDefault
public class SPrimitive extends SStatement {
  enum Kind {
    SUCCESS,
    FAIL,
    EXTERNAL,
    INITIALISATION,
    FETCH,
    COMPLETE,
    UNCOMPLETE,
    CHOSEN,
    DEFER,
    ACKNOWLEDGE,
    STARTTIMER,
    STOPTIMER;
  }

  private Kind kind;
  private boolean keep = false;
  private @Nullable UAction uAction;
  private @Nullable UContext uContext;
  private Set<UState> uStates = new HashSet<>();
  private @Nullable UEvent uEvent;
  private @Nullable SPrimitive marker;

  public interface Visitor<T> {
    public T onSuccess();
    public T onFail();
    public T onExternal(UAction external, UContext context);
    public T onFetch(Set<UState> waitUStates);
    public T onInitialisation();
    public T onComplete(boolean keep, UState uState);
    public T onUncomplete(UState uState);
    public T onChosen();
    public T onDefer();
    public T onAcknowledge();
    public T onStartTimer(boolean keep, UEvent event);
    public T onStopTimer(UEvent event);
  }

  private SPrimitive(Kind kind) {
    this.kind = kind;
  }

  /**
   * Copy constructor.
   *
   * @param primitive a primitive
   * @return a fresh copy of {@code primitive}
   */
  private SPrimitive(SPrimitive primitive) {
    this.kind = primitive.kind;
    this.uAction = primitive.uAction;
    this.uContext = primitive.uContext;
    this.uStates.addAll(primitive.uStates);
    this.uEvent = primitive.uEvent;
    this.keep = primitive.keep;
    this.marker = primitive.marker;
  }

  /**
   * Create a primitive which represents success.
   *
   * @return a Smile primitive
   */
  public static SPrimitive success() {
    return new SPrimitive(Kind.SUCCESS);
  }

  /**
   * Create a primitive which represents failing.
   *
   * @return a Smile primitive
   */
  public static SPrimitive fail() {
    return new SPrimitive(Kind.FAIL);
  }

  /**
   * Create a primitive representing an external UML action.
   *
   * @param uAction a UML action
   * @param uContext the action's context
   * @param isSuspending whether this action suspends execution
   * @return a Smile primitive
   */
  public static SPrimitive external(UAction uAction, UContext uContext, boolean isSuspending) {
    SPrimitive p = new SPrimitive(Kind.EXTERNAL);
    p.uAction = uAction;
    p.uContext = uContext;
    p.keep = isSuspending;
    return p;
  }

  /**
   * Create a primitive representing an external UML action.
   *
   * @param uAction a UML action
   * @param uContext the action's context
   * @return a Smile primitive
   */
  public static SPrimitive external(UAction uAction, UContext uContext) {
    return external(uAction, uContext, false);
  }

  /**
   * Create a primitive representing initialisation.
   *
   * @return a Smile primitive
   */
  public static SPrimitive initialisation() {
    return new SPrimitive(Kind.INITIALISATION);
  }

  /**
   * Create a primitive representing fetching a (UML) event.
   * 
   * @param waitUStates (UML) states for which fetch will produce completion events
   * @return a Smile primitive
   */
  public static SPrimitive fetch(Set<UState> waitUStates) {
    SPrimitive p = new SPrimitive(Kind.FETCH);
    p.uStates.addAll(waitUStates);
    return p;
  }

  /**
   * Create a primitive representing raising a completion event for a
   * (UML) state.
   *
   * @param keep whether completion shall be recorded until uncompletion
   * @param uState a (UML) state
   * @return a Smile primitive
   */
  public static SPrimitive complete(boolean keep, UState uState) {
    SPrimitive p = new SPrimitive(Kind.COMPLETE);
    p.keep = keep;
    p.uStates.add(uState);
    return p;
  }

  /**
   * Create a primitive representing rendering a completion event for a
   * (UML) state invalid.
   *
   * @param uState a (UML) state
   * @return a Smile primitive
   */
  public static SPrimitive uncomplete(UState state) {
    SPrimitive p = new SPrimitive(Kind.UNCOMPLETE);
    p.uStates.add(state);
    return p;
  }

  /**
   * Create a primitive representing that an event has been processed.
   *
   * @return a Smile primitive
   */
  public static SPrimitive chosen() {
    return new SPrimitive(Kind.CHOSEN);
  }

  /**
   * Create a primitive representing deferring a (UML) event.
   *
   * @return a Smile primitive
   */
  public static SPrimitive defer() {
    return new SPrimitive(Kind.DEFER);
  }

  /**
   * Create a primitive representing acknowledgement of a (UML) call
   * event.
   *
   * @return a Smile primitive
   */
  public static SPrimitive acknowledge() {
    return new SPrimitive(Kind.ACKNOWLEDGE);
  }

  /**
   * Create a primitive representing starting a timer for a (UML) time event
   * on a (UML) state.
   *
   * @param keep whether time-outs shall be recorded until stop timer
   * @param uEvent a (UML) time event
   * @return a Smile primitive
   */
  public static SPrimitive startTimer(boolean keep, UEvent uEvent) {
    SPrimitive p = new SPrimitive(Kind.STARTTIMER);
    p.keep = keep;
    p.uEvent = uEvent;
    return p;
  }

  /**
   * Create a primitive representing stopping a timer for a (UML) time event
   * on a (UML) state.
   *
   * @param uEvent a (UML) time event
   * @return a Smile primitive
   */
  public static SPrimitive stopTimer(UEvent uEvent) {
    SPrimitive p = new SPrimitive(Kind.STOPTIMER);
    p.uEvent = uEvent;
    return p;
  }

  public @Nullable UAction suspended() {
    if (this.kind == Kind.EXTERNAL && this.keep)
      return this.uAction;
    return null;
  }

  public @Nullable SPrimitive getMarker() {
    return this.marker;
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  int getComplexity() {
    return 1;
  }

  @Override
  public @Nullable SStatement getResumption(SPrimitive from, SVariable breakFlag) {
    if (this == from)
      return SCommand.skip();
    return null;
  }

  @Override
  public SStatement getCycle(SVariable suspendFlag) {
    if (this.suspended() != null) {
      var marked = new SPrimitive(this);
      marked.marker = this;
      return marked.merge(SCommand.assign(suspendFlag, SValue.trueValue()));
    }
    return this;
  }

  @Override
  Set<SVariable> getAssignedVariables() {
    return new HashSet<>();
  }

  @Override
  Set<SVariable> getUsedVariables() {
    return new HashSet<>();
  }

  @Override
  public Set<SPrimitive> getPrimitives() {
    return Sets.singleton(this);
  }

  @Override
  public Set<SConstant> getConstants() {
    return new HashSet<>();
  }

  @Override
  public Set<SConstant> getConstantsAssigned(Collection<SVariable> variables) {
    return new HashSet<>();
  }

  @Override
  public List<SCommand> getUnusedAssignments(final List<SCommand> assignments, List<SCommand> overwrittenAssignments) {
    return assignments;
  }

  @Override
  public SPrimitive replace(Collection<SCommand> commands, SCommand newCommand) {
    return this;
  }

  @Override
  public SPrimitive replaceTopLevelBreaks(SCommand newCommand) {
    return this;
  }

  /**
   * Accept a visitor.
   *
   * @param visitor a visitor
   * @return visit's result
   */
  public <T> T accept(Visitor<T> visitor) {
    switch (this.kind) {
      case SUCCESS:
        return visitor.onSuccess();

      case FAIL:
        return visitor.onFail();

      case EXTERNAL:
        return visitor.onExternal(requireNonNull(this.uAction), requireNonNull(this.uContext));

      case FETCH:
        return visitor.onFetch(this.uStates);

      case INITIALISATION:
        return visitor.onInitialisation();

      case COMPLETE:
        return visitor.onComplete(this.keep, this.uStates.iterator().next());

      case UNCOMPLETE:
        return visitor.onUncomplete(this.uStates.iterator().next());

      case CHOSEN:
        return visitor.onChosen();

      case DEFER:
        return visitor.onDefer();

      case ACKNOWLEDGE:
        return visitor.onAcknowledge();

      case STARTTIMER:
        return visitor.onStartTimer(this.keep, requireNonNull(this.uEvent));

      case STOPTIMER:
        return visitor.onStopTimer(requireNonNull(this.uEvent));
 
      default:
        throw new IllegalStateException("Unknown Smile primitive kind.");
    }
  }

  @Override
  public <T> T accept(SStatement.Visitor<T> visitor) {
    return this.accept((Visitor<T>)visitor);
  }

  @Override
  public SState execute(SState state) {
    switch (this.kind) {
      case FAIL:
      case SUCCESS:
        return state.empty();

      case FETCH:
        return state.withSomeEvent();

      case COMPLETE:
        return state.withCompleted(this.uStates.iterator().next());

      case UNCOMPLETE:
        return state.withUncompleted(this.uStates.iterator().next());

      //$CASES-OMITTED$
      default:
        return state;
    }
  }

  @Override
  public boolean isTerminating(SState sState) {
    switch (this.kind) {
      case FAIL:
      case SUCCESS:
        return false;

      //$CASES-OMITTED$
      default:
        return true;
    }
  }

  @Override
  public boolean isBreaking(SState sState) {
    return false;
  }

  @Override
  public SStatement partiallyEvaluate(SState state) {
    switch (this.kind) {
      case COMPLETE: {
        UState uState = this.uStates.iterator().next();
        if (state.getCompleteds(uState).equals(Sets.singleton(SValue.trueValue())))
          return SCommand.skip();
        return new SPrimitive(this);
      }

      case UNCOMPLETE: {
        UState uState = this.uStates.iterator().next();
        if (state.getCompleteds(uState).equals(Sets.singleton(SValue.falseValue())))
          return SCommand.skip();
        return new SPrimitive(this);
      }

      case CHOSEN: {
        if (!state.getEvents().stream().anyMatch(uEvent -> uEvent.isBehavioural()))
          return SCommand.skip();
        return new SPrimitive(this);
      }

      //$CASES-OMITTED$
      default:
        return new SPrimitive(this);
    }
  }

  @Override
  public SStatement getSimplified() {
    return new SPrimitive(this);
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case SUCCESS:
        resultBuilder.append("success();");
        return resultBuilder.toString();

      case FAIL:
        resultBuilder.append("fail();");
        return resultBuilder.toString();

      case EXTERNAL:
        resultBuilder.append("execute(");
        resultBuilder.append(this.uAction);
        resultBuilder.append(");");
        return resultBuilder.toString();

      case INITIALISATION:
        resultBuilder.append("initialisation();");
        return resultBuilder.toString();

      case FETCH:
        resultBuilder.append("currentEvent = fetch(");
        if (!this.uStates.isEmpty())
          resultBuilder.append(Formatter.set(this.uStates, uState -> uState.getIdentifier().toString()));
        resultBuilder.append(");");
        return resultBuilder.toString();

      case COMPLETE:
        resultBuilder.append("complete(");
        resultBuilder.append(this.keep);
        resultBuilder.append(", <");
        resultBuilder.append(this.uStates.iterator().next().getIdentifier());
        resultBuilder.append(">);");
        return resultBuilder.toString();

      case UNCOMPLETE:
        resultBuilder.append("uncomplete(<");
        resultBuilder.append(this.uStates.iterator().next().getIdentifier());
        resultBuilder.append(">);");
        return resultBuilder.toString();

      case CHOSEN:
        resultBuilder.append("chosen(currentEvent);");
        return resultBuilder.toString();

      case DEFER:
        resultBuilder.append("defer(currentEvent);");
        return resultBuilder.toString();

      case ACKNOWLEDGE:
        resultBuilder.append("acknowledge(currentEvent);");
        return resultBuilder.toString();

      case STARTTIMER:
        resultBuilder.append("starttimer(<");
        resultBuilder.append(this.keep);
        resultBuilder.append(", ");
        resultBuilder.append(requireNonNull(this.uEvent).getName());
        resultBuilder.append(">);");
        return resultBuilder.toString();

      case STOPTIMER:
        resultBuilder.append("stoptimer(<");
        resultBuilder.append(requireNonNull(this.uEvent).getName());
        resultBuilder.append(">);");
        return resultBuilder.toString();
    }

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.kind,
                        this.keep,
                        this.uAction,
                        this.uContext,
                        this.uStates,
                        this.uEvent);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SPrimitive other)
      return this.kind == other.kind &&
             this.keep == other.keep &&
             Objects.equals(this.uAction, other.uAction) &&
             Objects.equals(this.uContext, other.uContext) &&
             Objects.equals(this.uStates, other.uStates) &&
             Objects.equals(this.uEvent, other.uEvent);
    return false;
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
