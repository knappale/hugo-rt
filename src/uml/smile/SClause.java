package uml.smile;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Sets;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.Collections;


/**
 * Conjunctive clauses
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 *
 */
@NonNullByDefault
class SClause {
  Set<SLiteral> literals = new HashSet<>();

  private static final SClause TRUECLAUSE = new SClause();

  /**
   * Create the empty clause representing true
   */
  private SClause() {
  }

  /**
   * Copy constructor
   */
  private SClause(SClause clause) {
    this.literals.addAll(clause.literals);
  }

  /**
   * @return clause representing true
   */
  static SClause trueClause() {
    return TRUECLAUSE;
  }

  /**
   * Compute a reduced clause equivalent to the disjunction
   * of this and another clause, or return {@code null} if no such
   * clause can be found.
   *
   * @param other another clause
   * @return reduced clause, or {@code null}
   */
  @Nullable SClause join(SClause other) {
    if (this.literals.containsAll(other.literals))
      return other;
    if (other.literals.containsAll(this.literals))
      return this;
    Set<SLiteral> intersectionLiterals = Sets.intersection(this.literals, other.literals);
    Set<SLiteral> remainingLiterals1 = Sets.difference(this.literals, intersectionLiterals);
    Set<SLiteral> remainingLiterals2 = Sets.difference(other.literals, intersectionLiterals);
    if (remainingLiterals1.size() != 1 || remainingLiterals2.size() != 1)
      return null;
    @Nullable SLiteral joinedLiteral = remainingLiterals1.iterator().next().join(remainingLiterals2.iterator().next());
    if (joinedLiteral == null || joinedLiteral.isFalse())
      return null;
    SClause joinedClause = new SClause();
    joinedClause.literals.addAll(intersectionLiterals);
    if (!joinedLiteral.isTrue())
      joinedClause.literals.add(joinedLiteral);
    return joinedClause;
  }

  /**
   * Compute a reduced set of clauses equivalent to the disjunction
   * of the given clauses.
   *
   * @param clauses a set of clauses
   * @return reduced set of clauses
   */
  public static Set<SClause> join(Set<SClause> clauses) {
    if (clauses.isEmpty())
      return clauses;
    for (SClause clause : clauses)
      if (clause.isTrue())
        return Sets.singleton(SClause.trueClause());
    if (clauses.size() == 1)
      return clauses;

    CHANGE1:
    while (true) {
      for (SClause clause : clauses) {
        for (SClause otherClause : clauses) {
          if (clause.equals(otherClause))
            continue;
          @Nullable SClause joinedClause = clause.join(otherClause);
          if (joinedClause == null)
            continue;
          if (joinedClause.isTrue())
            return Sets.singleton(SClause.trueClause());
          if (!joinedClause.equals(clause) && !joinedClause.equals(otherClause)) {
            Set<SClause> newClauses = new HashSet<>(clauses);
            newClauses.remove(clause);
            newClauses.remove(otherClause);
            newClauses.add(joinedClause);
            clauses = newClauses;
            continue CHANGE1;
            // return join(newClauses);
          }
        }
      }
      break;
    }

    CHANGE2:
    while (true) {
      for (var clause : clauses) {
        for (var otherClause : clauses) {
          if (clause.equals(otherClause))
            continue;
          if (otherClause.literals.containsAll(clause.literals)) {
            Set<SClause> newClauses = new HashSet<>(clauses);
            newClauses.remove(otherClause);
            clauses = newClauses;
            continue CHANGE2;
          }
        }
      }
      break;
    }

    return clauses;
  }

  /**
   * Propagate {@code otherLiteral} to this clause: Discard all literals
   * {@code thisLiteral} with {@code thisLiteral || otherLiteral <-> true} or
   * {@code thisLiteral == otherLiteral} from this clause.
   * 
   * @param otherLiteral another literal
   * @return propagated clause
   */
  SClause propagate(SLiteral otherLiteral) {
    SClause propagatedClause = new SClause();
    for (SLiteral thisLiteral : this.literals) {
      if (thisLiteral.equals(otherLiteral))
        continue;
      @Nullable SLiteral joinedLiteral = thisLiteral.join(otherLiteral);
      if (joinedLiteral != null && joinedLiteral.isTrue())
        continue;
      propagatedClause.literals.add(thisLiteral);
    }
    return propagatedClause;
  }

  /**
   * Propagate literal clauses of {@code clauses} to all clauses in {@code clauses}.
   *
   * @param clauses a set of clauses (to be read disjunctively)
   * @return propagated set of clauses
   */
  static Set<SClause> propagate(Set<SClause> clauses) {
    if (clauses.size() <= 1)
      return clauses;

    Set<SClause> literalClauses = clauses.stream().filter(c -> c.literals.size() == 1).collect(toSet());
    Set<SClause> otherClauses = new HashSet<>(clauses);
    otherClauses.removeAll(literalClauses);
    Set<SClause> propagatedClauses = new HashSet<>(literalClauses);
    for (SClause otherClause : otherClauses) {
      SClause propagatedClause = new SClause(otherClause);
      for (SClause literalClause : literalClauses)
        propagatedClause = propagatedClause.propagate(literalClause.literals.iterator().next());
      propagatedClauses.add(propagatedClause);
    }
    return propagatedClauses;
  }

  private static SClauseSet and(Set<SLiteral> literals) {
    Set<SLiteral> metLiterals = SLiteral.meet(literals);
    if (metLiterals.stream().anyMatch(SLiteral::isFalse))
      return SClauseSet.falseClauseSet();
    SClause andClause = new SClause();
    andClause.literals.addAll(metLiterals);
    return new SClauseSet(andClause);
  }

  @SuppressWarnings("null")
  SClauseSet neg() {
    if (this.isTrue())
      return SClauseSet.falseClauseSet();
    Set<SLiteral> joinedNegatedLiterals = SLiteral.join(this.literals.stream().map(l -> SLiteral.neg(l)).collect(toSet()));
    if (joinedNegatedLiterals.stream().anyMatch(SLiteral::isTrue))
      return SClauseSet.trueClauseSet();
    return new SClauseSet(joinedNegatedLiterals.stream().map(l -> { SClause c = new SClause(); c.literals.add(l); return c; }).collect(toSet()));
  }

  SClauseSet and(SLiteral literal) {
    return SClause.and(Sets.union(this.literals, Sets.singleton(literal)));
  }

  SClauseSet and(SClause clause) {
    return SClause.and(Sets.union(this.literals, clause.literals));
  }

  /**
   * @return whether this clause is true
   */
  public boolean isTrue() {
    return this.literals.isEmpty();
  }

  /**
   * @return whether this clause is false
   */
  public boolean isFalse() {
    return false;
  }

  /**
   * Determine whether this clause subsumes another clause, i.e., whether
   * {@code this} clause is true if the {@code other} clause is true.
   * @param other another clause
   * @return whether {@code this} clause subsumes the {@code other} clause
   */
  public boolean subsumes(SClause other) {
    return this.literals.containsAll(other.literals);
  }

  /**
   * Determine the set of variables that are used in this clause.
   * 
   * @return set of variables used in this clause
   */
  public Set<SVariable> getVariables() {
    return this.literals.stream().map(g -> g.getVariables()).reduce(Sets.empty(), Sets::union);
  }

  /**
   * Determine the set of variables that are used in this clause.
   * 
   * @return set of variables used in this clause
   */
  public Set<SConstant> getConstants() {
    return this.literals.stream().map(g -> g.getConstants()).reduce(Sets.empty(), Sets::union);
  }

  /**
   * Accept a visitor.
   *
   * @param visitor a visitor
   * @return visit's result
   */
  public <T> T accept(SGuard.Visitor<T> visitor) {
    if (this.literals.isEmpty())
      return visitor.onLiteral(SLiteral.trueConst());
    if (this.literals.size() == 1)
      return visitor.onLiteral(this.literals.iterator().next());
    SLiteral firstLiteral = this.literals.iterator().next();
    Set<SLiteral> restLiterals = new HashSet<>(this.literals);
    restLiterals.remove(firstLiteral);
    SClause restClause = new SClause();
    restClause.literals.addAll(restLiterals);
    return visitor.onAnd(SGuard.literal(firstLiteral), SGuard.clause(restClause));
  }
    
  /**
   * Evaluate this clause in a state.
   *
   * @param state a state
   * @return set of values
   */
  public Set<SValue> evaluate(SState state) {
    return this.literals.stream().map(l -> l.evaluate(state)).
                                  reduce(Sets.singleton(SValue.trueValue()), (s1, s2) -> SValue.and(s1, s2));
  }

  /**
   * Reduce this clause in a state.
   *
   * @param state a state
   * @return reduced clause set
   */
  public SClauseSet reduce(SState state) {
    return SClause.and(SLiteral.meet(this.literals.stream().map(l -> l.reduce(state)).collect(toSet())));
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.literals);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof SClause other)
      return Objects.equals(this.literals, other.literals);
    return false;
  }

  @Override
  public String toString() {
    if (this.literals.isEmpty())
      return SLiteral.trueConst().toString();
    List<String> literalStrings = this.literals.stream().map(SLiteral::toString).collect(toList());
    Collections.sort(literalStrings, (s1, s2) -> s1.compareTo(s2));
    return Formatter.separated(literalStrings, " && ");
  }
}
