package uml.smile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Sets;



/**
 * Smile statement.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class SStatement {
  public interface Visitor<T> extends SCommand.Visitor<T>, SPrimitive.Visitor<T>, SControl.Visitor<T>, SBranch.Visitor<T> {
  }

  /**
   * Determine whether this statement is empty, i.e., has no effect.
   *
   * @return whether this statement is empty
   */
  abstract boolean isEmpty();

  /**
   * Determine the set of primitive statements that occur in this statement.
   * 
   * @return set of Smile primitives occurring in this statement
   */
  public abstract Set<SPrimitive> getPrimitives();

  /**
   * Determine the set of constants that are used in this statement.
   * 
   * @return set of Smile constants used in this statement
   */
  public abstract Set<SConstant> getConstants();

  /**
   * Determine the set of variables that are used in this statement.
   * 
   * @return set of Smile variables used in this statement
   */
  public Set<SVariable> getVariables() {
    return Sets.union(this.getAssignedVariables(), this.getUsedVariables());
  }

  /**
   * Determine the constants assigned to a collection of variables in
   * this statement.
   * 
   * @param variables collection of Smile variables
   * @return set of Smile constants assigned to {@code variables} in this statement
   */
  public abstract Set<SConstant> getConstantsAssigned(Collection<SVariable> variables);

  /**
   * Replace commands in this statement by a new command.
   *
   * @param commands a collection of commands
   * @param newCommand a command
   * @return this statement with replaced commands
   */
  public abstract SStatement replace(Collection<SCommand> commands, SCommand newCommand);

  /**
   * Replace top-level break commands in this branch by a new command.
   *
   * @param newCommand a command
   * @return this branch without top-level breaks
   */
  public abstract SStatement replaceTopLevelBreaks(SCommand newCommand);

  /**
   * Propagate a guard to this statement, i.e., produce a set of guarded branches.
   *
   * @param guard a guard
   * @return branches guarded by {@code guard}
   */
  Set<SBranch> propagate(SGuard guard) {
    if (guard.equals(SGuard.falseConst()))
      return new HashSet<>();

    return Sets.singleton(SBranch.branch(guard, this));
  }

  /**
   * Merge a sequentially following statement into this statement.
   *
   * @param statement a statement
   * @return merged statement
   */
  SStatement merge(SStatement statement) {
    return SControl.seq(this, statement);
  }

  /**
   * The statement when resuming {@code this} statement {@code from} statement.
   *
   * @param from a statement
   * @param breakFlag a variable for indicating that a following loop shall not be executed
   * @return {@code this} statement {@code from} statement, or {@code null} if
   * {@code from} does not occur in {@code this} statement
   */
  public abstract @Nullable SStatement getResumption(SPrimitive from, SVariable breakFlag);

  /**
   * The statement when stopping {@code this} statement at some suspending statement.
   *
   * @param suspendFlag a variable for indicating that the statement got suspended
   * @return {@code this} statement up to (all) suspending statements
   */
  public abstract SStatement getCycle(SVariable suspendFlag);

  /**
   * Determine the set of variables assigned in this statement.
   *
   * @return set of variables assigned in this statement
   */
  abstract Set<SVariable> getAssignedVariables();

  /**
   * Determine the set of Smile variables that are used in this statement.
   * 
   * @return set of Smile variables used in this statement
   */
  abstract Set<SVariable> getUsedVariables();

  /**
   * Determine the list of unused assignments in this statement.
   *
   * @param assignments unused assignments flowing into this statement
   * @param overwrittenAssignments previously overwritten assignments
   * @return unused assignments flowing out of this statement; the list of overwritten assignments is updated
   */
  public abstract List<SCommand> getUnusedAssignments(final List<SCommand> assignments, List<SCommand> overwrittenAssignments);

  /**
   * Determine the list of assignments that are definitely overwritten in this statement.
   *
   * @return overwritten assignments
   */
  public List<SCommand> getOverwrittenAssignments() {
    List<SCommand> overwrittenAssignments = new ArrayList<>();
    this.getUnusedAssignments(new ArrayList<>(), overwrittenAssignments);
    return overwrittenAssignments;
  }

  /**
   * Remove commands that are useless in a given state from this statement.
   * If there are no useless commands, the identical statement is returned.
   *
   * TODO (AK190210) Better replace the identity requirement with associative sequencing.
   *
   * @param sState a state
   * @return simplified statement with useless commands removed, or {@code this} if
   *         there are no useless commands
   */
  public SStatement removeUselessCommands(SState sState) {
    List<SCommand> uselessCommands = this.getOverwrittenAssignments();
    if (uselessCommands.isEmpty())
      return this;
    SStatement removed = this.replace(uselessCommands, SCommand.skip());
    return removed.getSimplified();
  }

  /**
   * Accept a statement visitor.
   *  
   * @param visitor a statement visitor
   * @return visit's result
   */
  public abstract <T> T accept(Visitor<T> visitor);

  /**
   * Execute this statement in a state.
   *
   * @param state a state
   * @return state after execution
   */
  public abstract SState execute(SState sState);

  /**
   * Determine whether this statement is definitely terminating in a state.
   *
   * @param state a state
   * @return whether this statement definitely terminates in {@code state}
   */
  public abstract boolean isTerminating(SState sState);

  /**
   * Determine whether this statement definitely reaches a break in a state.
   *
   * @param state a state
   * @return whether this statement definitely reaches a break in {@code state}
   */
  public abstract boolean isBreaking(SState sState);

  /**
   * Partially evaluate this statement in a state.
   *
   * @param state a state
   * @return partially evaluated statement
   */
  public abstract SStatement partiallyEvaluate(SState sState);

  /**
   * Provide a simplified copy of this statement.
   *
   * @return simplified statement
   */
  public abstract SStatement getSimplified();

  /**
   * Determine the "complexity" of this statement.
   *
   * @return this statement's complexity
   */
  abstract int getComplexity();

  /**
   * Compute a partially evaluated and accordingly simplified version of this statement.
   *
   * @param state a state
   * @return simplified statement
   */
  public final SStatement simplify(SState state) {
    SStatement simplified = this;
    while (true) {
      SStatement partiallyEvaluated = simplified.partiallyEvaluate(state);
      SStatement overwritten = partiallyEvaluated.removeUselessCommands(state);
      if (partiallyEvaluated.equals(overwritten))
        return partiallyEvaluated; // simplified.partiallyEvaluate(state);
      simplified = overwritten;
    }
  }

  /**
   * @param prefix line prefix
   * @return statement's declaration with every new line prepended by
   *         {@code prefix}
   */
  public abstract String declaration(String prefix);

  /**
   * @return statement's declaration
   */
  public String declaration() {
    return declaration("");
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
