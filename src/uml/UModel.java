package uml;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import control.Properties;


/**
 * UML model
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UModel extends UElement {
  private List<UClass> classes = new ArrayList<>();
  private List<UDataType> datatypes = new ArrayList<>();
  private List<USignal> signals = new ArrayList<>();
  private List<UCollaboration> collaborations = new ArrayList<>();
  private @Nullable UCollaboration emptyCollaboration = null;
  private UDataType integerType;
  private UDataType booleanType;
  private UDataType stringType;
  private UDataType voidType;
  private UDataType clockType;
  private Properties properties;
  
  /**
   * Create a new named model.
   *
   * @param name model's name
   * @return a model
   */
  public UModel(String name) {
    super(name);

    this.integerType = new UDataType("int", this) {
      public @NonNull UExpression getInitialValue() {
        return UExpression.intConst(0);
      }
    };
    this.datatypes.add(this.integerType);

    this.booleanType = new UDataType("boolean", this) {
      public @NonNull UExpression getInitialValue() {
        return UExpression.falseConst();
      }
    };
    this.datatypes.add(this.booleanType);

    this.stringType = new UDataType("string", this) {
      public @NonNull UExpression getInitialValue() {
        return UExpression.emptyStringConst();
      }
    };
    this.datatypes.add(this.stringType);

    this.clockType = new UDataType("clock", this) {
      public @NonNull UExpression getInitialValue() {
        return UExpression.intConst(0);
      }
    };
    this.datatypes.add(this.clockType);

    this.voidType = new UDataType("void", this) {
      public @NonNull UExpression getInitialValue() {
        return UExpression.nullConst();
      }
    };
    this.datatypes.add(this.voidType);

    this.properties = Properties.getDefault().getCopy();
  }

  /**
   * Determine model's properties.
   *
   * @return model's properties
   */
  public Properties getProperties() {
    return this.properties;
  }

  /**
   * Add a class to the model.
   *
   * @param hclass a class
   */
  public UClass addClass(String name) {
    UClass uClass = new UClass(name, this);
    this.classes.add(uClass);
    return uClass;
  }

  /**
   * Add a class to the model.
   *
   * @param uClass a class
   */
  public void addClass(UClass uClass) {
    this.classes.add(uClass);
  }

  /**
   * @return model's classes
   */
  public List<UClass> getClasses() {
    return this.classes;
  }

  /**
   * Retrieve a class of this model by name.
   *
   * @param name classes name
   * @return model's class named {@code name}, or {@code null} if there is none
   */
  public @Nullable UClass getC1ass(String name) {
    return this.classes.stream().filter(c1ass -> c1ass.getName().equals(name)).findAny().orElse(null);
  }

  /**
   * Add a data type to the model.
   *
   * @param datatype a data type
   */
  public void addDataType(UDataType datatype) {
    this.datatypes.add(datatype);
  }

  /**
   * @return model's data types
   */
  public List<UDataType> getDataTypes() {
    return this.datatypes;
  }

  /**
   * Add a signal to the model.
   *
   * @param signal a signal
   */
  public void addSignal(USignal signal) {
    this.signals.add(signal);
  }

  /**
   * @return model's signals
   */
  public List<USignal> getSignals() {
    return this.signals;
  }

  /**
   * @return a unique, non-negative number for a signal
   */
  public int getNumber(USignal signal) {
    for (int i = 0; i < this.signals.size(); i++)
      if (this.signals.get(i).equals(signal))
        return i+1;
    assert false : "not applicable";
    return 0;
  }

  /**
   * Get maximal number of UML parameters appearing in UML model
   *
   * @return maximal number of UML parameters appearing in UML model
   */
  public int getMaxParameters() {
    return this.getClasses().stream().map(uClass -> uClass.getMaxParameters()).reduce(0, (m1, m2) -> Integer.max(m1, m2));
  }

  /**
   * Add a collaboration to the model.
   *
   * @param collaboration a collaboration
   */
  public void addCollaboration(UCollaboration collaboration) {
    this.collaborations.add(collaboration);
  }

  /**
   * @return model's collaborations
   */
  public List<UCollaboration> getCollaborations() {
    return this.collaborations;
  }

  /**
   * @return model's integer data type
   */
  public UDataType getInteger() {
    return this.integerType;
  }

  /**
   * @return model's integer type
   */
  public UType getIntegerType() {
    return UType.simple(this.getInteger());
  }

  /**
   * @return model's boolean data type
   */
  public UDataType getBoolean() {
    return this.booleanType;
  }

  /**
   * @return model's boolean type
   */
  public UType getBooleanType() {
    return UType.simple(this.getBoolean());
  }

  /**
   * @return model's string data type
   */
  public UDataType getString() {
    return this.stringType;
  }

  /**
   * @return model's string type
   */
  public UType getStringType() {
    return UType.simple(this.getString());
  }

  /**
   * @return model's clock data type
   */
  public UDataType getClock() {
    return this.clockType;
  }

  /**
   * @return model's clock type
   */
  public UType getClockType() {
    return UType.simple(this.getClock());
  }

  /**
   * @return model's void data type
   */
  public UDataType getVoid() {
    return this.voidType;
  }

  /**
   * @return model's void type
   */
  public UType getVoidType() {
    return UType.simple(this.getVoid());
  }

  /**
   * @return an empty collaboration for this model
   */
  public UCollaboration getEmptyUCollaboration() {
    UCollaboration emptyCollaboration = this.emptyCollaboration;
    if (emptyCollaboration != null)
      return emptyCollaboration;

    emptyCollaboration = new UCollaboration("empty", this);
    this.addCollaboration(emptyCollaboration);
    this.emptyCollaboration = emptyCollaboration;
    return emptyCollaboration;
  }

  /**
   * @return model's representation in UTE format
   */  
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    String prefix = "  ";
    String lineSep = "";
    
    resultBuilder.append("model ");
    resultBuilder.append(this.getName());
    resultBuilder.append(" {\n");

    resultBuilder.append(this.getProperties().declaration(prefix));
    lineSep = "\n\n";

    for (UClass uClass : this.getClasses()) {
      resultBuilder.append(lineSep);
      resultBuilder.append(uClass.declaration(prefix));
      lineSep = "\n\n";
    }

    for (UCollaboration uCollaboration : this.getCollaborations()) {
      resultBuilder.append(lineSep);
      resultBuilder.append(prefix);
      resultBuilder.append(uCollaboration.declaration(prefix));
      lineSep = "\n\n";
    }

    resultBuilder.append("\n}");
    
    return resultBuilder.toString();
  }
}
