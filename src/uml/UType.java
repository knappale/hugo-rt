package uml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * UML type
 *
 * A type either refers directly to a classifier (class and datatype),
 * or represents an array (with determinate bounds) over a classifier,
 * or represents a function type from types to a type.
 *
 * An array type for an array of size 1 is identified with a simple
 * type over the same classifier.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UType {
  enum Kind {
    NULL,
    ANY,
    SIMPLE,
    ARRAY,
    FUNCTION,
    META,
    ERROR;
  }

  private final Kind kind;
  private final @Nullable UClassifier classifier;
  private final int size;
  private final @Nullable List<UType> parameterTypes;
  private final @Nullable UType returnType;
  private final @Nullable String meta;
  private final @Nullable String errorMessage;

  private UType(Kind kind, @Nullable UClassifier classifier, int size, @Nullable List<UType> parameterTypes, @Nullable UType returnType, @Nullable String string) {
    this.kind = kind;
    this.classifier = classifier;
    this.size = size;
    this.parameterTypes = parameterTypes;
    this.returnType = returnType;
    switch (kind) {
      case META: {
        this.meta = string;
        this.errorMessage = null;
        break;
      }
      case ERROR: {
        this.meta = null;
        this.errorMessage = string;
        break;
      }
      default: {
        this.meta = null;
        this.errorMessage = null;
      }
    }
  }

  // Keep a canonical table of all known types
  private static Map<UType, UType> types = new HashMap<>();

  /**
   * Determine canonical instance of a type.
   *
   * @param type a type
   * @return canonical type instance for {@code type}
   */
  private static UType canonical(UType type) {
    UType canonicalType = types.get(type);
    if (canonicalType != null)
      return canonicalType;
    types.put(type, type);
    return type;
  }

  /**
   * Create the null type.
   */
  public static UType nullType() {
    return canonical(new UType(Kind.NULL, null, 0, null, null, null));
  }

  /**
   * Create the any type.
   */
  public static UType anyType() {
    return canonical(new UType(Kind.ANY, null, 0, null, null, null));
  }

  /**
   * Create a simple type for a classifier.
   *
   * @return simple type
   */
  public static UType simple(UClassifier classifier) {
    return canonical(new UType(Kind.SIMPLE, classifier, 1, null, null, null));
  }

  /**
   * Create an array type for a classifier and a given size of the array.
   *
   * An array type for an array of size 1 is identified with a simple
   * type for the same classifier
   *
   * @pre size > 0
   * @return array type
   */
  public static UType array(UClassifier classifier, int size) {
    return canonical(new UType((size > 1 ? Kind.ARRAY : Kind.SIMPLE), classifier, size, null, null, null));
  }

  /**
   * Create a function type for a list of parameter types and a return
   * type.
   *
   * @return function type
   */
  public static UType function(List<UType> parameterTypes, UType returnType) {
    return canonical(new UType(Kind.FUNCTION, null, 0, parameterTypes, returnType, null));
  }

  /**
   * Create an error type for an error message
   *
   * @return error type
   */
  public static UType error(String errorMessage) {
    return new UType(Kind.ERROR, null, 0, null, null, errorMessage);
  }

  /**
   * Create a meta type for a string
   *
   * @return error type
   */
  public static UType meta(String meta) {
    return new UType(Kind.META, null, 0, null, null, meta);
  }

  /**
   * @return whether this type represents the null type
   */
  public boolean isNull() {
    return this.kind == Kind.NULL;
  }

  /**
   * @return whether this type represents the any type
   */
  public boolean isAny() {
    return this.kind == Kind.ANY;
  }

  /**
   * @return whether this type represents a simple type (for a
   * classifier)
   */
  public boolean isSimple() {
    return this.kind == Kind.SIMPLE;
  }

  /**
   * @return whether this type represents a simple type for a class
   */
  public boolean isClass() {
    return (isSimple() && (classifier instanceof UClass));
  }

  /**
   * @return whether this type represents a simple type for a datatype
   */
  public boolean isDataType() {
    return (isSimple() && (classifier instanceof UDataType));
  }

  /**
   * @return whether this type represents an array type
   */
  public boolean isArray() {
    return this.kind == Kind.ARRAY;
  }

  /**
   * @return whether this type represents a function type
   */
  public boolean isFunction() {
    return this.kind == Kind.FUNCTION;
  }

  /**
   * @return whether this type represents a meta type
   */
  public boolean isMeta() {
    return this.kind == Kind.META;
  }

  /**
   * @return whether this type represents an error
   */
  public boolean isError() {
    return this.kind == Kind.ERROR;
  }

  /**
   * @return type's class (meaningful only if simple type or array type)
   * @pre this.isSimple() || this.isArray()
   */
  public @Nullable UClass getC1ass() {
    try {
      return (UClass)this.classifier;
    }
    catch (ClassCastException cce) {
      return null;
    }
  }

  /**
   * @return type's classifier (meaningful only if simple type or array type)
   * @pre this.isSimple() || this.isArray()
   */
  public @Nullable UClassifier getClassifier() {
    return this.classifier;
  }

  /**
   * @return type's error message (meaningful only if error type)
   * @pre this.isError()
   */
  public @Nullable String getErrorMessage() {
    return this.errorMessage;
  }

  /**
   * @return type's underlying type, i.e., a simple type if this type is an array, and this type otherwise
   */
  public UType getUnderlyingType() {
    UClassifier classifier = this.classifier;
    if (classifier == null)
      return this;
    return UType.simple(classifier);
  }

  /**
   * @return type's size (meaningful only if array type)
   * @pre this.isArray()
   */
  public int getSize() {
    return this.size;
  }

  /**
   * @return type's parameter types (meaningful only if function type)
   * @pre this.isFunction()
   */
  public @Nullable List<UType> getParameterTypes() {
    return this.parameterTypes;
  }

  /**
   * @return type's return types (meaningful only if function type)
   * @pre this.isFunction()
   */
  public @Nullable UType getReturnType() {
    return this.returnType;
  }

  /**
   * @return type's name
   */
  public String getName() {
    return this.toString();
  }

  /**
   * Determine the initial value of this type.
   *
   * Determining the initial value of simple types is delegated to the
   * underlying classifier.  All other types have no initial value.
   *
   * @return type's initial value, or <CODE>null</CODE> if this type
   * has no initial value
   */
  public @Nullable UExpression getInitialValue() {
    if (isSimple() && this.classifier != null)
      return this.classifier.getInitialValue();
    return null;
  }

  /**
   * Determine whether this type subsumes another type, i.e., whether any inhabitant
   * of the other type can be considered an inhabitant of this type.
   *
   * The type subsumption hierarchy (other <= this) is as follows:
   * <UL>
   * <LI>Any subsumes any class type (not the data types).
   * <LI>Null is subsumed by any class type (not the data types).
   * <LI>Arrays are invariant.
   * <LI>Functions are contra-variant in their argument types and co-variant in their return type:
   * <UL>
   * <LI>A -> B <= A' -> B' if A' <= A & B <= B'
   * <LI>e.g., for Integer <= Real, Real -> Integer <= Integer -> Real
   * </UL>
   * <LI>All error types are considered equal to each other.
   * </UL> 
   *
   * @param other a type
   * @return whether this type subsumes the other type
   */
  public boolean subsumes(UType other) {
    if (this.isError() || other.isError())
      return this.isError() && other.isError();

    if (this.isMeta() || other.isMeta()) {
      if (!(this.isMeta() && other.isMeta()))
        return false;
      final String thisMeta = this.meta; assert (thisMeta != null);
      final String otherMeta = other.meta; assert (otherMeta != null);
      return thisMeta.equals(otherMeta);
    }

    if (this.isSimple() || other.isSimple()) {
      if (anyType().equals(this))
	return (other.isClass());

      if (nullType().equals(other))
	return (this.isClass());

      return (this.equals(other));
    }

    if (this.isArray() || other.isArray()) {
      if (!(this.isArray() && other.isArray()))
	return false;
      if (this.getSize() != other.getSize())
	return false;
      return this.getUnderlyingType().equals(other.getUnderlyingType());
    }

    if (this.isFunction() || other.isFunction()) {
      if (!(this.isFunction() && other.isFunction()))
	return false;
      final List<UType> thisParameterTypes = this.getParameterTypes(); assert (thisParameterTypes != null);
      final List<UType> otherParameterTypes = other.getParameterTypes(); assert (otherParameterTypes != null);
      final UType thisReturnType = this.getReturnType(); assert (thisReturnType != null);
      final UType otherReturnType = other.getReturnType(); assert (otherReturnType != null);
      if (thisParameterTypes.size() != otherParameterTypes.size())
	return false;
      for (int i = 0; i < thisParameterTypes.size(); i++) {
	final UType thisParameterType = thisParameterTypes.get(i);
	final UType otherParameterType = otherParameterTypes.get(i);
	if (!otherParameterType.subsumes(thisParameterType))
	  return false;
      }
      return (thisReturnType.subsumes(otherReturnType));
    }
    
    return (this.equals(other));
  }

  /**
   * @return type's hash code
   */
  public int hashCode() {
    return Objects.hash(this.classifier, this.parameterTypes, this.returnType, this.meta);
  }
 
  /**
   * Determine whether an object is equal to this type.
   *
   * All error types are considered to be equal.  The equality of
   * types is defined to be extensional as types are a derived
   * concept.
   *
   * @return whether object is equal to this type
   */
  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UType other = (UType)object;
      if (this.isError() && other.isError())
        return true;
      return ((this.kind == other.kind) &&
              (Objects.equals(this.classifier, other.classifier)) &&
              (this.size == other.size) &&
              (Objects.equals(this.parameterTypes, other.parameterTypes)) &&
              (Objects.equals(this.returnType, other.returnType)) &&
              (Objects.equals(this.meta, other.meta)));
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  /**
   * @return type's string representation
   */
  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case NULL:
        return "Null";

      case ANY:
        return "Any";

      case SIMPLE: {
        final UClassifier this_classifier = this.classifier; assert (this_classifier != null);
        return this_classifier.getName();
      }

      case ARRAY: {
        resultBuilder.append(this.getUnderlyingType().toString());
        resultBuilder.append("[");
        resultBuilder.append(this.getSize());
        resultBuilder.append("]");
        final String result = resultBuilder.toString(); assert (result != null);
        return result;
      }

      case FUNCTION: {
        final List<UType> this_parameterTypes = this.parameterTypes; assert (this_parameterTypes != null);
        final UType this_returnType = this.returnType; assert (this_returnType != null);
        resultBuilder.append("(");
        String sep = "";
        for (UType parameterType : this_parameterTypes) {
          resultBuilder.append(sep);
          resultBuilder.append(parameterType.toString());
          sep = ", ";
        }
        resultBuilder.append(") -> ");
        resultBuilder.append(this_returnType.toString());
        final String result = resultBuilder.toString(); assert (result != null);
        return result;
      }

      case META: {
        resultBuilder.append("[");
        resultBuilder.append(this.meta);
        resultBuilder.append("]");
        final String result = resultBuilder.toString(); assert (result != null);
        return result;
      }

      case ERROR: {
        resultBuilder.append("Error: ");
        resultBuilder.append(this.errorMessage);
        final String result = resultBuilder.toString(); assert (result != null);
        return result;
      }
    }

    return "[empty]";
  }
}
