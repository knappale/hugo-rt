package uml.statemachine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UElement;
import uml.statemachine.semantics.UCompoundTransition;
import uml.statemachine.semantics.UVertexTree;
import uml.statemachine.semantics.UTransitionTree;
import util.Identifier;

import static java.util.stream.Collectors.toList;
import static util.Objects.requireNonNull;


/**
 * Contains data of a (UML state machine) vertex.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class UVertex extends UElement {
  private URegion container;
  private List<UTransition> incomings = new ArrayList<>();
  private List<UTransition> outgoings = new ArrayList<>();

  @NonNullByDefault({})
  public class Cases<T> {
    private Function<@NonNull UState, T> stateFun = null;
    private Function<@NonNull UFinalState, T> finalStateFun = null;
    private Function<@NonNull UPseudoState, T> pseudoStateFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases<T> state(Function<@NonNull UState, T> stateFun) {
      this.stateFun = stateFun;
      return this;
    }
    
    public Cases<T> finalState(Function<@NonNull UFinalState, T> finalStateFun) {
      this.finalStateFun = finalStateFun;
      return this;
    }
    
    public Cases<T> pseudoState(Function<@NonNull UPseudoState, T> pseudoStateFun) {
      this.pseudoStateFun = pseudoStateFun;
      return this;
    }
    
    public Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply() {
      if (UVertex.this instanceof UPseudoState && this.pseudoStateFun != null)
        return this.pseudoStateFun.apply((UPseudoState)UVertex.this);
      if (UVertex.this instanceof UFinalState && this.finalStateFun != null)
        return this.finalStateFun.apply((UFinalState)UVertex.this);
      if (UVertex.this instanceof UState && this.stateFun != null)
        return this.stateFun.apply((UState)UVertex.this);

      assert (this.otherwiseFun != null) : "No default for case distinction on vertex type";
      return this.otherwiseFun.get();
    }
  }

  /**
   * Make a case distinction on the sub-type.
   *
   * @param stateFun function called when this vertex is a proper state
   * @param finalStateFun function called when this vertex is a final state
   * @param pseudoStateFun function called when this vertex is a pseudo-state
   * @return result of applying one of the sub-type functions
   */
  @NonNullByDefault({})
  public abstract <T> T cases(Function<@NonNull UState, T> stateFun, Function<@NonNull UFinalState, T> finalStateFun, Function<@NonNull UPseudoState, T> pseudoStateFun);

  /**
   * Create a new vertex contained in a region.
   *
   * @param name vertex's name
   * @param container vertex's containing region
   */
  UVertex(String name, URegion container) {
    super(name);
    this.container = container;
  }

  /**
   * @return vertex's owning state machine
   */
  public UStateMachine getStateMachine() {
    return this.container.getStateMachine();
  }

  /**
   * @return vertex's containing region
   */
  public URegion getContainer() {
    return this.container;
  }

  /**
   * @return vertex's identifier
   */
  public Identifier getIdentifier() {
    URegion container = this.getContainer();
    if (container.isOrthogonal())
      return Identifier.id(container.getIdentifier(), this.getName());
    UState containerContainer = container.getContainer();
    if (containerContainer == null)
      return Identifier.id(this.getName());
    return Identifier.id(containerContainer.getIdentifier(), this.getName());
  }

  /**
   * Add an incoming transition to this vertex.
   *
   * @param transition a transition
   */
  void addIncoming(UTransition transition) {
    this.incomings.add(transition);
  }

  /**
   * @return vertex's incoming transitions
   */
  public List<UTransition> getIncomings() {
    return this.incomings;
  }

  /**
   * Add an outgoing transition to this vertex.
   *
   * @param transition a transition
   */
  void addOutgoing(UTransition transition) {
    this.outgoings.add(transition);
  }

  /**
   * @return vertex's outgoing transitions
   */
  public List<UTransition> getOutgoings() {
    return this.outgoings;
  }

  /**
   * @return vertex's kind
   */
  @NonNullByDefault({})
  public UPseudoState.Kind getKind() {
    return null;
  }

  /**
   * Determine whether this vertex can be left via a completion event
   * or is an intermediate state.
   *
   * @return whether vertex is completable
   */
  public abstract boolean isCompletable();

  /**
   * Determine whether this vertex is a source.
   *
   * A vertex is called a source if it may be the start-point of a
   * {@link uml.statemachine.semantics.UCompoundTransition compound transition}.
   *
   * @return whether this vertex is a source
   */
  public abstract boolean isSource();

  /**
   * Determine whether this vertex is a target.
   *
   * A vertex is called target if it may be the end-point of a
   * {@link uml.statemachine.semantics.UCompoundTransition compound transition}.
   *
   * @return whether this vertex is a target
   */
  public abstract boolean isTarget();

  /**
   * Determine whether this vertex is a reflexive ancestor vertex of
   * another vertex.
   *
   * @param other another vertex
   * @return whether this vertex is a reflexive ancestor vertex of {@code other}
   */
  public boolean isAncestor(UVertex other) {
    if (this.equals(other))
      return true;
    UState otherContainer = other.getContainer().getContainer();
    if (otherContainer == null)
      return false;
    return this.isAncestor(otherContainer);
  }

  /**
   * @return nesting level of this vertex
   */
  public int getLevel() {
    return this.getContainer().getLevel();
  }

  /**
   * Determine all intermediate vertices between this vertex and the
   * vertex in {@code subVertices}.
   *
   * @param subVertices collection of vertices, each of which must be a
   * (transitive) sub-vertex of this state
   * @return intermediate vertices between (and including) this vertex
   * and the vertices in {@code subVertices} (inclusive).
   *
   * @pre \forall v \in subVertices . this.isAncestor(v)
   */
  public Set<UVertex> getIntermediateVertices(Collection<UVertex> subVertices) {
    Set<UVertex> intermediateVertices = new HashSet<>();
    intermediateVertices.add(this);
    for (UVertex subVertex : subVertices) {
      while (!subVertex.equals(this)) {
        intermediateVertices.add(subVertex);
        subVertex = subVertex.getContainer().getContainer();
      }
    }
    return intermediateVertices;
  }

  /**
   * @return vertex's state trees
   */
  public abstract List<UVertexTree> getVertexTrees();

  /**
   * Determine the target state tree for this vertex.
   *
   * @param vertices a set of vertices
   * @return the target state tree for this vertex
   */
  public abstract UVertexTree getTargetVertexTree(Set<UVertex> vertices);

  /**
   * @return vertex's initial state tree, and {@code null} if this vertex
   *         is not the ancestor of an initial pseudo-state
   */
  public abstract @Nullable UVertexTree getInitialVertexTree();

  /**
   * @return vertex's final state tree, and {@code null} if this vertex
   *         is not the ancestor of a final state
   */
  public abstract @Nullable UVertexTree getFinalVertexTree();

  /**
   * Compound transitions consist of an optional join prefix and a
   * transition tree.
   *
   * The outgoing compound transitions are determined using {@link
   * #getForwardTrees forward trees}.  If the vertex shows some
   * outgoing transitions targeting a join pseudo-state, the forward
   * trees from this join pseudo-state are determined, and each such
   * forward tree is prepended by all transitions targeting the join
   * point.
   *
   * @see uml.statemachine.semantics.UCompoundTransition
   * @see uml.statemachine.semantics.UTransitionTree
   *
   * @return vertex's outgoing compound transitions
   */
  public List<UCompoundTransition> getOutgoingCompounds() {
    List<UCompoundTransition> outgoingCompoundTransitions = new ArrayList<>();

    Set<UVertex> joinTargets = new HashSet<>();
    for (UTransitionTree forwardTree : this.getForwardPaths(new HashSet<>())) {
      if (forwardTree.hasJoinTarget()) {
        UVertex joinTarget = requireNonNull(forwardTree.getTargets().iterator().next());
        if (joinTargets.contains(joinTarget))
          continue;
        joinTargets.add(joinTarget);
        for (UTransitionTree joinForwardTree : joinTarget.getForwardTrees(new HashSet<>())) {
          outgoingCompoundTransitions.add(new UCompoundTransition(joinTarget.getIncomings(), joinForwardTree));
        }
      }
      else
        outgoingCompoundTransitions.add(new UCompoundTransition(new ArrayList<>(), forwardTree));
    }
    return outgoingCompoundTransitions;
  }

  /**
   * Determine all compound transitions outgoing from this vertex that are
   * possibly used during an initialisation.
   *
   * @param visited already visited vertices
   * @return initialising compound transitions
   */
  protected abstract Set<UCompoundTransition> getInitialisingCompounds(Set<UVertex> visited);

  /**
   * Determine all paths of transitions outgoing from this vertex.
   *
   * A forward transition tree joins together transitions through
   * junction pseudo-states; moreover, branches are added at fork
   * pseudo-states.
   *
   * Loops through junction (pseudo-)states are caught.
   *
   * @see uml.statemachine.semantics.UTransitionTree
   *
   * @param visited visited junction pseudo-states
   * @return list of forward paths outgoing from this state
   */
  protected List<UTransitionTree> getForwardPaths(Set<UVertex> visited) {
    return this.getOutgoings().stream().flatMap(outgoing ->
               outgoing.getTarget().getForwardPaths(outgoing, visited).stream()).collect(toList());
  }

  /**
   * Determine all paths of transitions outgoing from this vertex when
   * it is reached by an incoming transition.
   *
   * Loops through junction (pseudo-)states are caught.
   *
   * @see uml.statemachine.UVertex#getForwardPaths
   *
   * @param incoming a transition incoming to this vertex
   * @param visited visited junction pseudo-states
   * @return list of forward paths outgoing from this vertex with
   *         {@code incoming} prepended
   */
  protected abstract List<UTransitionTree> getForwardPaths(UTransition incoming, Set<UVertex> visited);

  /**
   * Determine all trees of transitions outgoing from this vertex.
   *
   * A forward transition tree joins together transitions through
   * junction pseudo-states; moreover, branches are added at fork
   * pseudo-states.  Whenever, a composite state is targeted by some
   * transition on the way, it is properly initialised by taking the
   * default transitions from the initial state(s) inside the
   * composite state (there may only be several, if the composite
   * state is concurrent).
   *
   * Loops through junction pseudo-states are caught.
   *
   * @see uml.statemachine.semantics.UTransitionTree
   *
   * @param visited visited junction pseudo-states
   * @return list of forward trees outgoing from this vertex
   */
  protected List<UTransitionTree> getForwardTrees(Set<UVertex> visited) {
    return this.getOutgoings().stream().flatMap(outgoing ->
               outgoing.getTarget().getForwardTrees(outgoing, visited).stream()).collect(toList());
  }

  /**
   * Determine all trees of transitions outgoing from this vertex when
   * it is reached by an incoming transition.
   *
   * Loops through junction (pseudo-)states are caught.
   *
   * @see uml.statemachine.UVertex#getForwardTrees
   *
   * @param incoming a transition incoming to this vertex
   * @param visited visited junction pseudo-states
   * @return list of forward trees outgoing from this vertex with
   *         {@code incoming} prepended
   */
  protected abstract List<UTransitionTree> getForwardTrees(UTransition incoming, Set<UVertex> visited);

  /**
   * Determine indented UTE declaration of this vertex.
   * 
   * @param indent indent string
   * @return vertex's UTE declaration
   */
  public abstract String declaration(String indent);
}
