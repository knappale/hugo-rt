package uml.statemachine;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAction;
import uml.UContext;
import uml.UExpression;


/**
 * Contains data of a transition.
 *
 * @author <A HREF="mailto:majnu@gmx.net">Timm Sch�fer</A>
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UTransition {
  private URegion container;
  private UVertex source;
  private UVertex target;
  private UEvent trigger;
  private UExpression guard;
  private UAction effect;

  UTransition(UVertex source, UVertex target, UEvent trigger, UExpression guard, UAction effect, URegion container) {
    this.source = source;
    this.target = target;
    this.trigger = trigger;
    this.guard = guard;
    this.effect = effect;
    this.container = container;
    source.addOutgoing(this);
    target.addIncoming(this);
  }

  /**
   * @return transition's state machine
   */
  public UStateMachine getStateMachine() {
    return this.container.getStateMachine();
  }

  /**
   * @return transition's source vertex
   */
  public UVertex getSource() {
    return this.source;
  }
    
  /**
   * @return transition's target state
   */
  public UVertex getTarget() {
    return this.target;
  }

  /**
   * @return transition's triggering event
   */
  public UEvent getTrigger() {
    return this.trigger;
  }

  /**
   * @return transition's guard
   */
  public UExpression getGuard() {
    return this.guard;
  }

  /**
   * @return transition's effect action
   */
  public UAction getEffect() {
    return this.effect;
  }

  /**
   * @return context for the triggering event of this transition
   */
  public UContext getTriggerContext() {
    return UContext.c1ass(this.getStateMachine().getC1ass());
  }

  /**
   * @return context for the guard of this transition
   */
  public UContext getGuardContext() {
    UEvent initialTrigger = this.getInitialTrigger();
    if (initialTrigger != null)
      return UContext.event(this.getStateMachine().getC1ass(), initialTrigger);
    else
      return UContext.c1ass(getStateMachine().getC1ass());
  }

  /**
   * @return context for the effect of this transition
   */
  public UContext getEffectContext() {
    UEvent initialTrigger = this.getInitialTrigger();
    if (initialTrigger != null)
      return UContext.event(this.getStateMachine().getC1ass(), initialTrigger);
    else
      return UContext.c1ass(this.getStateMachine().getC1ass());
  }

  /**
   * @return event by which this transition will be ultimately
   * triggered, or {@code null} if there is no such unique
   * triggering event
   */
  private @Nullable UEvent getInitialTrigger() {
    return this.getInitialTrigger(new HashSet<>());
  }

  /**
   * Search for event that will ultimately trigger this transition
   * avoiding cycles through pseudo-states.
   *
   * TODO (AK160528) This currently ignores join pseudo-states.
   *
   * @param visited set of all transitions that have already been visited
   * @return event by which this transition will be ultimately
   * triggered disregarding all transitions in {@code visited}, or
   * {@code null} if there is no such unique triggering event
   */
  private @Nullable UEvent getInitialTrigger(Set<UTransition> visited) {
    UEvent initialTrigger = this.getTrigger();
    if (visited.contains(this))
      return initialTrigger;
    visited.add(this);

    UVertex source = this.getSource();
    if (source.getKind() == null)
      return this.getTrigger();

    for (UTransition incomingTransition : source.getIncomings()) {
      UEvent incomingTrigger = incomingTransition.getInitialTrigger(visited);
      if (incomingTrigger != null && !incomingTrigger.isPseudo() && !incomingTrigger.isCompletion() &&
          initialTrigger != null && !initialTrigger.isPseudo() && !initialTrigger.isCompletion() &&
          !incomingTrigger.equals(initialTrigger)) {
        util.Message.debug().info("Ill-formed transition with two triggering events `" + incomingTrigger.getName() + "', `" + initialTrigger.getName() + "'");
        return null;
      }
      else
        initialTrigger = incomingTrigger;
    }
    return initialTrigger;
  }

  /**
   * @return transition's (artificial) name consisting of source and
   * target state names
   */
  public String getName() {
    return this.source.getName() + "_" + this.target.getName();
  }

  /**
   * @param prefix prefix string
   * @return transition's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append(this.source.getIdentifier().toString());
    resultBuilder.append(" -> ");
    resultBuilder.append(this.target.getIdentifier().toString());

    boolean hasExplicitTrigger = !this.getTrigger().isPseudo() && (!this.getTrigger().isCompletion() || this.getTrigger().isWait());
    boolean hasExplicitGuard = !this.guard.equals(UExpression.trueConst());
    boolean hasExplicitEffect = !this.effect.equals(UAction.skip());

    if (!hasExplicitTrigger && !hasExplicitGuard && !hasExplicitEffect) {
      resultBuilder.append(";");
      return resultBuilder.toString();
    }
      
    resultBuilder.append(" {\n");
    if (hasExplicitTrigger) {
      resultBuilder.append(nextPrefix);
      resultBuilder.append("trigger ");
      resultBuilder.append(this.trigger.declaration());
      resultBuilder.append(";\n");
    }
    if (hasExplicitGuard) {
      resultBuilder.append(nextPrefix);
      resultBuilder.append("guard ");
      resultBuilder.append(this.guard);
      resultBuilder.append(";\n");
    }
    if (hasExplicitEffect) {
      resultBuilder.append(nextPrefix);
      resultBuilder.append("effect ");
      resultBuilder.append(this.effect);
      resultBuilder.append("\n");
    }
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  /**
   * @return transition's hash code
   */
  public int hashCode() {
    return Objects.hash(this.source,
                        this.target);
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Transition [source = ");
    resultBuilder.append(source.getIdentifier().toString());
    resultBuilder.append(", target = ");
    resultBuilder.append(this.target.getIdentifier().toString());
    resultBuilder.append(", trigger = ");
    resultBuilder.append(this.trigger);
    resultBuilder.append(", guard = ");
    resultBuilder.append(this.guard);
    resultBuilder.append(", effect = ");
    resultBuilder.append(this.effect);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
