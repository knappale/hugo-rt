package uml.statemachine;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.statemachine.semantics.UCompoundTransition;
import uml.statemachine.semantics.UVertexTree;
import uml.statemachine.semantics.UTransitionTree;


@NonNullByDefault
public class UFinalState extends UVertex {
  UFinalState(String name, URegion container) {
    super(name, container);
  }

  @Override
  @NonNullByDefault({})
  public <T> T cases(Function<@NonNull UState, T> stateFun, Function<@NonNull UFinalState, T> finalStateFun, Function<@NonNull UPseudoState, T> pseudoStateFun) {
    return finalStateFun.apply(this);
  }

  @Override
  public boolean isCompletable() {
    return false;
  }

  @Override
  public boolean isSource() {
    return false;
  }

  @Override
  public boolean isTarget() {
    return true;
  }

  @Override
  public List<UVertexTree> getVertexTrees() {
    return Arrays.asList(UVertexTree.simple(this));
  }

  @Override
  public UVertexTree getTargetVertexTree(Set<UVertex> vertices) {
    return UVertexTree.simple(this);
  }

  @Override
  public @Nullable UVertexTree getInitialVertexTree() {
    return null;
  }

  @Override
  public @Nullable UVertexTree getFinalVertexTree() {
    return UVertexTree.simple(this);
  }

  @Override
  protected Set<UCompoundTransition> getInitialisingCompounds(Set<UVertex> visited) {
    return new HashSet<>();
  }

  @Override
  protected List<UTransitionTree> getForwardPaths(UTransition incoming, Set<UVertex> visited) {
    return Arrays.asList(new UTransitionTree(incoming));
  }

  @Override
  protected List<UTransitionTree> getForwardTrees(UTransition incoming, Set<UVertex> visited) {
    return Arrays.asList(new UTransitionTree(incoming));
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(prefix);
    resultBuilder.append("final ");
    resultBuilder.append(this.getName());
    resultBuilder.append(";");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("FinalState [name = ");
    resultBuilder.append(getIdentifier().toString());
    resultBuilder.append(", #incomings = ");
    resultBuilder.append(this.getIncomings().size());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
