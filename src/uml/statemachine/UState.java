package uml.statemachine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAction;
import uml.UContext;
import uml.statemachine.semantics.UCompoundTransition;
import uml.statemachine.semantics.UVertexTree;
import uml.statemachine.semantics.UTransitionTree;
import util.Formatter;
import util.Lists;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static util.Objects.requireNonNull;
import static util.Objects.ignoreNull;


/**
 * Contains data of a (UML state machine) state.
 *
 * @author <A HREF="mailto:majnu@gmx.net">Timm Schäfer</A>
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UState extends UVertex {
  private List<URegion> uRegions = new ArrayList<>();
  private UAction entryAction = UAction.skip();
  private UAction exitAction = UAction.skip();
  private List<UEvent> deferrableEvents = new ArrayList<>();

  /**
   * Create a new state contained in a region.
   *
   * @param name state's name
   * @param container state's containing region
   */
  UState(String name, URegion container) {
    super(name, container);
  }

  @Override
  @NonNullByDefault({})
  public <T> T cases(Function<@NonNull UState, T> stateFun, Function<@NonNull UFinalState, T> finalStateFun, Function<@NonNull UPseudoState, T> pseudoStateFun) {
    return stateFun.apply(this);
  }

  /**
   * Add a sub-region to this state
   *
   * @param name sub-region's name
   * @return the newly added sub-region
   */
  public URegion addRegion(String name) {
    URegion uRegion = new URegion(name, this);
    this.uRegions.add(uRegion);
    return uRegion;
  }

  /**
   * @return state's regions
   */
  public List<URegion> getRegions() {
    return this.uRegions;
  }

  /**
   * @return state's kind name
   */
  private String getKindName() {
    switch (this.getRegions().size()) {
      case 0: return "simple";
      case 1: return "composite";
      default: return "concurrent";
    }
  }

  @Override
  public boolean isSource() {
    return true;
  }

  @Override
  public boolean isTarget() {
    return true;
  }

  /**
   * Determine all vertices reflexively and transitively contained in this state.
   * 
   * @return state's (recursive) sub-vertices
   */
  Set<UVertex> getAllVertices() {
    return Stream.<UVertex>concat(Stream.of(this),
                                           this.getRegions().stream().flatMap(region -> region.getAllUVertices().stream())).
                  collect(toSet());
  }

  /**
   * Determine all regions transitively contained in this state.
   * 
   * @return state's (recursive) sub-regions
   */
  public Set<URegion> getAllRegions() {
    return this.getRegions().stream().flatMap(region -> region.getAllRegions().stream()).collect(toSet());
  }

  /**
   * Determine whether this state can be left via a completion event
   * or is an intermediate state.
   *
   * @return whether state is completable
   */
  public boolean isCompletable() {
    return this.getRegions().stream().allMatch(region -> region.getFinal() != null);
  }

  /**
   * Set state's entry action
   *
   * @param entryAction state's new entry action
   */
  public void setEntryAction(UAction entryAction) {
    this.entryAction = entryAction;
  }

  /**
   * @return whether state shows an entry action
   */
  public boolean hasEntryAction() {
    return !this.entryAction.equals(UAction.skip());
  }

  /**
   * @return state's entry action
   */
  public UAction getEntryAction() {
    return this.entryAction;
  }

  /**
   * Set state's exit action
   *
   * @param entryAction state's new exit action
   */
  public void setExitAction(UAction exitAction) {
    this.exitAction = exitAction;
  }

  /**
   * @return whether state shows an exit action
   */
  public boolean hasExitAction() {
    return !this.exitAction.equals(UAction.skip());
  }

  /**
   * @return state's exit action
   */
  public UAction getExitAction() {
    return this.exitAction;
  }

  /**
   * @return whether state shows a do-activity
   */
  public boolean hasDoActivity() {
    return false;
  }

  /**
   * @return whether state shows an outgoing completion transition
   */
  public boolean hasOutgoingCompletion() {
    return this.getOutgoings().stream().anyMatch(transition -> transition.getTrigger().isCompletion());
  }

  /**
   * @return state's outgoing completion transitions
   */
  public Set<UTransition> getOutgoingCompletions() {
    return this.getOutgoings().stream().filter(transition -> transition.getTrigger().isCompletion()).collect(toSet());
  }

  /**
   * @return whether state shows an outgoing wait transition
   */
  public boolean hasOutgoingWait() {
    return this.getOutgoings().stream().anyMatch(transition -> transition.getTrigger().isWait());
  }

  /**
   * @return state's outgoing wait transitions
   */
  public Set<UTransition> getOutgoingWaits() {
    return this.getOutgoings().stream().filter(transition -> transition.getTrigger().isWait()).collect(toSet());
  }

  /**
   * @return whether state shows an outgoing completion transition to
   * a join (pseudo-)state
   */
  public boolean hasOutgoingJoinCompletion() {
    return this.getOutgoings().stream().anyMatch(transition -> transition.getTarget().getKind() == UPseudoState.Kind.JOIN);
  }

  /**
   * @return whether state shows an outgoing time triggered transition
   */
  public boolean hasOutgoingTime() {
    return this.getOutgoings().stream().anyMatch(transition -> transition.getTrigger().isTime());
  }

  /**
   * @return outgoing time triggered transitions
   */
  public Set<UTransition> getOutgoingTimes() {
    return this.getOutgoings().stream().filter(transition -> transition.getTrigger().isTime()).collect(toSet());
  }

  /**
   * Add a deferrable event to this state.
   *
   * @param event an event
   */
  public void addDeferrableEvent(UEvent event) {
    this.deferrableEvents.add(event);
  }

  /**
   * Add deferrable events to this state.
   *
   * @param events events
   */
  public void addDeferrableEvents(Collection<UEvent> events) {
    this.deferrableEvents.addAll(events);
  }

  /**
   * @return state's deferrable events
   */
  public List<UEvent> getDeferrableEvents() {
    return this.deferrableEvents;
  }

  @Override
  protected Set<UCompoundTransition> getInitialisingCompounds(Set<UVertex> visited) {
    if (!visited.add(this))
      return new HashSet<>();

    return this.getRegions().stream().flatMap(region -> region.getInitialisingCompounds().stream()).collect(toSet());
  }

  @Override
  protected List<UTransitionTree> getForwardPaths(UTransition incoming, Set<UVertex> visited) {
    List<UTransitionTree> forwardPaths = new ArrayList<>();
    forwardPaths.add(new UTransitionTree(incoming));
    return forwardPaths;
  }

  @Override
  protected List<UTransitionTree> getForwardTrees(UTransition incoming, Set<UVertex> visited) {
    if (this.getRegions().size() == 0)
      return Arrays.asList(new UTransitionTree(incoming));

    // On entering a simple composite state, the default entry is taken.
    if (this.getRegions().size() == 1) {
      UPseudoState initial = requireNonNull(this.getRegions().get(0).getInitial());
      return initial.getForwardTrees(visited).stream().map(nextForwardTree ->
                 new UTransitionTree(incoming, nextForwardTree)).collect(toList());
    }

    // For a concurrent composite state, the default entry is the
    // product of the default entries of every of its regions.
    List<List<UTransitionTree>> nextForwardTreeLists = this.getRegions().stream().map(nextRegion ->
                                                           requireNonNull(nextRegion.getInitial()).getForwardTrees(visited)).collect(toList());
    return Lists.product(nextForwardTreeLists).stream().map(nextForwardTreeBranches ->
               new UTransitionTree(incoming, nextForwardTreeBranches)).collect(toList());
  }

  /**
   * @return state's entry action context
   */
  public UContext getEntryContext() {
    return UContext.c1ass(getStateMachine().getC1ass());
  }

  /**
   * @return state's exit action context
   */
  public UContext getExitContext() {
    return UContext.c1ass(getStateMachine().getC1ass());
  }

  @Override
  public List<UVertexTree> getVertexTrees() {
    @NonNull List<UVertexTree> stateTrees = new ArrayList<>();

    if (this.getRegions().size() == 0)
      stateTrees.add(UVertexTree.simple(this));

    if (this.getRegions().size() == 1) {
      for (UVertexTree subStateTree : this.getRegions().get(0).getVertexTrees())
        stateTrees.add(UVertexTree.hierarchical(this, Arrays.asList(subStateTree)));
    }

    if (this.getRegions().size() > 1) {
      List<List<UVertexTree>> subStateTrees = this.getRegions().stream().map(URegion::getVertexTrees).collect(toList());
      for (List<UVertexTree> productList : util.Lists.product(subStateTrees))
        stateTrees.add(UVertexTree.hierarchical(this, productList));
    }

    if (this.isCompletable() && this.hasOutgoingCompletion()) {
      UVertexTree finalStateTree = this.getFinalVertexTree();
      if (finalStateTree != null)
        stateTrees.add(finalStateTree);
    }

    if (this.hasOutgoingTime()) {
      Set<UEvent> timeEvents = this.getOutgoingTimes().stream().map(UTransition::getTrigger).collect(toSet());
      List<UVertexTree> timelessStateTrees = new ArrayList<>(stateTrees);
      for (Set<UEvent> timeEventSet : util.Sets.powerSet(timeEvents)) {
        for (UVertexTree timelessStateTree : timelessStateTrees) {
          stateTrees.add(UVertexTree.timedOut(timeEventSet, timelessStateTree));
        }
      }
    }

    return stateTrees;
  }

  @Override
  public @Nullable UVertexTree getInitialVertexTree() {
    return null;
  }

  @Override
  public @Nullable UVertexTree getFinalVertexTree() {
    if (!this.isCompletable())
      return null;

    if (this.getRegions().size() == 0)
      return UVertexTree.completed(UVertexTree.simple(this));

    if (this.getRegions().size() == 1) {
      UFinalState finalState = this.getRegions().get(0).getFinal();
      if (finalState == null)
        return null;
      return UVertexTree.completed(UVertexTree.hierarchical(this, UVertexTree.simple(finalState)));
    }

    List<UVertexTree> finalSubStateTrees = new ArrayList<>();
    for (URegion uRegion : this.getRegions()) {
      UVertexTree finalSubStateTree = uRegion.getFinalVertexTree();
      if (finalSubStateTree == null)
        return null;
      finalSubStateTrees.add(finalSubStateTree);
    }
    return UVertexTree.completed(UVertexTree.hierarchical(this, finalSubStateTrees));
  }

  @Override
  public UVertexTree getTargetVertexTree(Set<UVertex> vertices) {
    Stream<UVertexTree> stateTrees = this.getRegions().stream().flatMap(region -> ignoreNull(region.getTargetVertexTree(vertices)));
    return UVertexTree.hierarchical(this, stateTrees.collect(toList()));
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";
    String nextNextPrefix = nextPrefix + "  ";
    String sep = "";

    resultBuilder.append(prefix);
    resultBuilder.append(this.getKindName());
    resultBuilder.append(" ");
    resultBuilder.append(this.getName());
    if (this.getRegions().size() > 0 ||
        this.hasEntryAction() ||
        this.hasExitAction() ||
        this.hasDoActivity() ||
        !this.getDeferrableEvents().isEmpty()) {
      resultBuilder.append(" {\n");
      if (!this.getDeferrableEvents().isEmpty()) {
        resultBuilder.append(nextPrefix);
        resultBuilder.append("defer ");
        resultBuilder.append(Formatter.separated(this.getDeferrableEvents(), uEvent -> uEvent.declaration(), ", "));
        resultBuilder.append(";\n");
        sep = "\n";
      }
      if (this.hasEntryAction()) {
        resultBuilder.append(nextPrefix);
        resultBuilder.append("entry ");
        resultBuilder.append(this.getEntryAction());
        resultBuilder.append("\n");
        sep = "\n";
      }
      if (this.hasExitAction()) {
        resultBuilder.append(nextPrefix);
        resultBuilder.append("exit ");
        resultBuilder.append(this.getExitAction());
        resultBuilder.append("\n");
        sep = "\n";
      }
      if (this.getRegions().size() == 1) {
        resultBuilder.append(sep);
        for (UVertex subVertex : this.getRegions().get(0).getVertices()) {
          resultBuilder.append(subVertex.declaration(nextPrefix));
          resultBuilder.append("\n");
        }
      }
      if (this.getRegions().size() > 1) {
        resultBuilder.append(sep);
        for (URegion region : this.getRegions()) {
          resultBuilder.append(nextPrefix);
          resultBuilder.append("composite ");
          resultBuilder.append(region.getName());
          resultBuilder.append(" {\n");
          for (UVertex subVertex : region.getVertices()) {
            resultBuilder.append(subVertex.declaration(nextNextPrefix));
            resultBuilder.append("\n");
          }
          resultBuilder.append(nextPrefix);
          resultBuilder.append("}\n");
        }
      }
      resultBuilder.append(prefix);
      resultBuilder.append("}");
    }
    else {
      resultBuilder.append(";");
    }

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("State [name = ");
    resultBuilder.append(getIdentifier().toString());
    resultBuilder.append(", kind = ");
    resultBuilder.append(this.getKindName());
    resultBuilder.append(", #regions = ");
    resultBuilder.append(this.getRegions().size());
    resultBuilder.append(", #incomings = ");
    resultBuilder.append(this.getIncomings().size());
    resultBuilder.append(", #outgoings = ");
    resultBuilder.append(this.getOutgoings().size());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
