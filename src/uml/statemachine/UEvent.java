package uml.statemachine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UBehavioural;
import uml.UClass;
import uml.UContext;
import uml.UExpression;
import uml.UOperation;
import uml.UParameter;
import uml.UReception;
import uml.USignal;

import util.Formatter;
import util.TriFunction;

import static util.Objects.requireNonNull;


/**
 * Contains data of an event.
 *
 * @author <A HREF="mailto:majnu@gmx.net">Timm Sch�fer</A>
 */
@NonNullByDefault
public class UEvent {
  enum Kind {
    PSEUDO,
    WAIT,
    COMPLETION,
    TIME,
    SIGNAL,
    CALL;
  }

  private static Map<UEvent, UEvent> events = new HashMap<>();
  private static UEvent canonical(UEvent event) {
    UEvent canonical = events.get(event);
    if (canonical != null)
      return canonical;
    events.put(event, event);
    return event;
  }

  private Kind kind;
  private Set<UState> states = new HashSet<>();
  private @Nullable UOperation operation = null;
  private @Nullable USignal signal = null;
  private @Nullable UExpression timeLow = null;
  private @Nullable UExpression timeHigh = null;

  @NonNullByDefault({})
  public class Cases<T> {
    private Function<@NonNull UState, T> waitFun = null;
    private Function<Set<@NonNull UState>, T> completionFun = null;
    private TriFunction<@NonNull UState, @NonNull UExpression, @NonNull UExpression, T> timeFun = null;
    private Function<@NonNull USignal, T> signalFun = null;
    private Function<@NonNull UOperation, T> callFun = null;
    private Supplier<T> pseudoFun = null;
    private Supplier<T> otherwiseFun = null;

    public Cases<T> wait(Function<@NonNull UState, T> waitFun) {
      this.waitFun = waitFun;
      return this;
    }
    
    public Cases<T> completion(Function<Set<@NonNull UState>, T> completionFun) {
      this.completionFun = completionFun;
      return this;
    }
    
    public Cases<T> time(TriFunction<@NonNull UState, @NonNull UExpression, @NonNull UExpression, T> timeFun) {
      this.timeFun = timeFun;
      return this;
    }
    
    public Cases<T> signal(Function<@NonNull USignal, T> signalFun) {
      this.signalFun = signalFun;
      return this;
    }

    public Cases<T> call(Function<@NonNull UOperation, T> callFun) {
      this.callFun = callFun;
      return this;
    }

    public Cases<T> pseudo(Supplier<T> pseudoFun) {
      this.pseudoFun = pseudoFun;
      return this;
    }

    public Cases<T> otherwise(Supplier<T> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public T apply() {
      switch (kind) {
        case WAIT: return (waitFun != null ? waitFun.apply(requireNonNull(states.iterator().next())) : otherwiseFun.get());
        case COMPLETION: return (completionFun != null ? completionFun.apply(states) : otherwiseFun.get());
        case TIME: return (timeFun != null ? timeFun.apply(requireNonNull(states.iterator().next()), requireNonNull(timeLow), requireNonNull(timeHigh)) : otherwiseFun.get());
        case SIGNAL: return (signalFun != null ? signalFun.apply(requireNonNull(signal)) : otherwiseFun.get());
        case CALL: return (callFun != null ? callFun.apply(requireNonNull(operation)) : otherwiseFun.get());
        case PSEUDO: return (pseudoFun != null ? pseudoFun.get() : otherwiseFun.get());
      }
      throw new IllegalStateException("Unknown UML event kind.");
    }
  }

  private UEvent(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create a pseudo event (for transitions outgoing from pseudo-states).
   *
   * @return an event
   */
  public static UEvent pseudo() {
    return canonical(new UEvent(Kind.PSEUDO));
  }

  /**
   * Create a completion event for a state.
   *
   * @param state state
   * @return an event
   */
  public static UEvent completion(UState state) {
    UEvent e = new UEvent(Kind.COMPLETION);
    e.states.add(state);
    return canonical(e);
  }

  /**
   * Create a completion event for a set of states.
   *
   * @param states states
   * @return an event
   */
  public static UEvent completion(Collection<UState> states) {
    UEvent e = new UEvent(Kind.COMPLETION);
    e.states.addAll(states);
    return canonical(e);
  }

  /**
   * Create a wait event for a state.
   *
   * @param state state
   * @return an event
   */
  public static UEvent wait(UState state) {
    UEvent e = new UEvent(Kind.WAIT);
    e.states.add(state);
    return canonical(e);
  }

  /**
   * Create a behavioural (signal/call) event
   *
   * @param behavioural behavioural
   * @return an event
   */
  public static UEvent behavioural(UBehavioural behavioural) {
    if (behavioural instanceof UOperation)
      return UEvent.call((UOperation)behavioural);
    return UEvent.signal((UReception)behavioural);
  }

  /**
   * Create a signal event
   *
   * @param reception a reception
   * @return an event for the {@link #reception reception}'s signal
   */
  public static UEvent signal(UReception reception) {
    UEvent e = new UEvent(Kind.SIGNAL);
    e.signal = reception.getSignal();
    return canonical(e);
  }

  /**
   * Create a signal event
   *
   * @param signal signal
   * @return an event
   */
  public static UEvent signal(USignal signal) {
    UEvent e = new UEvent(Kind.SIGNAL);
    e.signal = signal;
    return canonical(e);
  }

  /**
   * Create a call event for an operation
   *
   * @param operation operation
   * @return an event
   */
  public static UEvent call(UOperation operation) {
    UEvent e = new UEvent(Kind.CALL);
    e.operation = operation;
    return canonical(e);
  }

  /**
   * Create a time event
   *
   * @param state state for which the time event may occur
   * @param time time
   * @return an event
   */
  public static UEvent time(UState state, UExpression time) {
    UEvent e = new UEvent(Kind.TIME);
    e.states.add(state);
    e.timeLow = time;
    e.timeHigh = time;
    return canonical(e);
  }

  /**
   * Create a time event with an upper and a lower bound
   *
   * @param state state for which the time event may occur
   * @param timeLow lower time bound
   * @param timeHigh upper time bound
   * @return an event
   */
  public static UEvent time(UState state, UExpression timeLow, UExpression timeHigh) {
    UEvent e = new UEvent(Kind.TIME);
    e.states.add(state);
    e.timeLow = timeLow;
    e.timeHigh = timeHigh;
    return canonical(e);
  }

  public boolean isPseudo() {
    return (this.kind == Kind.PSEUDO);
  }

  public boolean isWait() {
    return (this.kind == Kind.WAIT);
  }

  public boolean isCompletion() {
    return (this.kind == Kind.COMPLETION || this.isWait());
  }

  public boolean isBehavioural() {
    return (this.kind == Kind.SIGNAL || this.kind == Kind.CALL);
  }

  public boolean isTime() {
    return (this.kind == Kind.TIME);
  }

  public UState getState() {
    return requireNonNull(this.states.iterator().next());
  }

  public Set<UState> getStates() {
    return this.states;
  }

  public UExpression getTimeLowExpression() {
    return requireNonNull(this.timeLow);
  }

  public UExpression getTimeHighExpression() {
    return requireNonNull(this.timeHigh);
  }

  public UContext getTimeContext() {
    return UContext.c1ass(getState().getStateMachine().getC1ass());
  }

  public boolean matches(UClass c1ass) {
    switch (this.kind) {
      case SIGNAL:
        return c1ass.getReception(requireNonNull(this.signal)) != null;
      case CALL:
        return c1ass.getOperations().contains(this.operation);
      default:
        return false;
    }
  }

  public boolean matches(UStateMachine stateMachine) {
    switch (this.kind) {
      case WAIT:
      case COMPLETION:
      case TIME:
        UState state = states.iterator().next();
        return state != null && state.getStateMachine().equals(stateMachine);
      case SIGNAL:
      case CALL:
        return this.matches(stateMachine.getC1ass());
      default:
        return false;
    }
  }

  public String getName() {
    return this.new Cases<String>().
      signal(signal -> "send " + signal.getName()).
      call(operation -> "call " + operation.getName()).
      time((state, timeLow, timeHigh) -> "time " + state.getIdentifier().toString() + " after [" + timeLow.toString() + ", " + timeHigh.toString() + "]").
      completion(states -> "completion " + Formatter.setOrSingleton(states, state -> state.getIdentifier().toString())).
      wait(state -> "wait " + state.getIdentifier()).
      pseudo(() -> "pseudo").
      apply();
  }

  public List<UParameter> getParameters(UStateMachine stateMachine) {
    return this.new Cases<List<UParameter>>().
      signal(signal -> requireNonNull(stateMachine.getC1ass().getReception(signal)).getParameters()).
      call(operation -> operation.getParameters()).
      otherwise(() -> new ArrayList<>()).
      apply();
  }

  /**
   * @param prefix prefix string
   * @return event's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    return prefix + this.new Cases<String>().
      wait(states -> "wait").
      completion(states -> "").
      pseudo(() -> "").
      signal(signal -> signal.getName()).
      call(operation -> operation.getName()).
      time((state, timeLow, timeHigh) -> "after [" + timeLow.toString() + ", " + timeHigh.toString() + "]").
      apply();   
  }

  /**
   * @return event's representation in UTE format
   */
  public String declaration() {
    return this.declaration("");
  }
 
  @Override
  public int hashCode() {
    return Objects.hash(this.states,
                        this.operation,
                        this.signal,
                        this.timeLow,
                        this.timeHigh);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UEvent other = (UEvent)object;
      return (this.kind == other.kind &&
              Objects.equals(this.states, other.states) &&
              Objects.equals(this.operation, other.operation) &&
              Objects.equals(this.signal, other.signal) &&
              Objects.equals(this.timeLow, other.timeLow) &&
              Objects.equals(this.timeHigh, other.timeHigh));
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return "Event [" + this.new Cases<String>().
      signal(signal -> "signal = " + signal.getName()).
      call(operation -> "operation = " + operation.getName()).
      time((state, timeLow, timeHigh) -> "state = " + state.toString() +
                                         ", time = [" + timeLow.toString() +
                                         ", " + timeHigh.toString() + "]").
      wait(state -> "wait = " + state.getName()).
      completion(states -> "completion = " + Formatter.setOrSingleton(states, state -> state.getName())).
      pseudo(() -> "pseudo").
      apply() + "]";
  }
}
