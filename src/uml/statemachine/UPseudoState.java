package uml.statemachine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.statemachine.semantics.UCompoundTransition;
import uml.statemachine.semantics.UVertexTree;
import uml.statemachine.semantics.UTransitionTree;
import util.Formatter;
import util.Lists;
import util.Message;

import static java.util.stream.Collectors.toList;
import static util.Objects.requireNonNull;


/**
 * Contains data of a (UML state machine) state.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @since 2016-03-18
 */
@NonNullByDefault
public class UPseudoState extends UVertex {
  public enum Kind {
    INITIAL,
    SHALLOWHISTORY,
    DEEPHISTORY,
    JOIN,
    FORK,
    JUNCTION,
    CHOICE,
    SYNCH;

    public String getName() {
      switch (this) {
        case INITIAL:
          return "initial";
        case SHALLOWHISTORY:
          return "history";
        case DEEPHISTORY:
          return "DeepHistory";
        case JOIN:
          return "join";
        case FORK:
          return "fork";
        case JUNCTION:
          return "junction";
        case CHOICE:
          return "choice";
        case SYNCH:
          return "Synchronisation";
        default:
          throw new IllegalStateException("Unknown pseudo-state kind.");
      }
    }
  }

  private Kind kind;

  /**
   * Create a new pseudo-stated contained in a region.
   *
   * @param name pseudo-state's name
   * @param kind pseudo-state's kind
   * @param container pseudo-state's containing region
   */
  UPseudoState(String name, Kind kind, URegion container) {
    super(name, container);
    this.kind = kind;
  }

  @Override
  @NonNullByDefault({})
  public <T> T cases(Function<@NonNull UState, T> stateFun, Function<@NonNull UFinalState, T> finalStateFun, Function<@NonNull UPseudoState, T> pseudoStateFun) {
    return pseudoStateFun.apply(this);
  }

  /**
   * @return pseudo-state's kind
   */
  public Kind getKind() {
    return this.kind;
  }

  /**
   * @return pseudo-state's kind name
   */
  public String getKindName() {
    return this.kind.getName();
  }

  @Override
  public boolean isSource() {
    switch (this.kind) {
      case INITIAL:
        return true;
      case JOIN:
      case FORK:
      case JUNCTION:
      case SHALLOWHISTORY:
      case DEEPHISTORY:
        return false;
      //$CASES-OMITTED$
      default:
        return true;
    }
  }

  @Override
  public boolean isTarget() {
    switch (this.kind) {
      case INITIAL:
        // An initial pseudo-state is a target iff it is on the top level
        return this.getContainer().getContainer() == null;
      case JOIN:
      case FORK:
      case JUNCTION:
      case SHALLOWHISTORY:
      case DEEPHISTORY:
        return false;
      //$CASES-OMITTED$
      default:
        return true;
    }
  }

  @Override
  public boolean isCompletable() {
    return false;
  }

  @Override
  public List<UVertexTree> getVertexTrees() {
    // Forks and joins do not occur in state trees, and we currently
    // cannot handle choice and history states
    if (this.kind != Kind.INITIAL)
      return new ArrayList<>();

    return Arrays.asList(UVertexTree.simple(this));
  }

  @Override
  public UVertexTree getTargetVertexTree(Set<UVertex> vertices) {
    return UVertexTree.simple(this);
  }

  @Override
  public @Nullable UVertexTree getInitialVertexTree() {
    if (this.kind != Kind.INITIAL)
      return null;
    return UVertexTree.simple(this);
  }

  @Override
  public @Nullable UVertexTree getFinalVertexTree() {
    return null;
  }

  public Set<UCompoundTransition> getInitialisingCompounds() {
    if (this.kind != Kind.INITIAL)
      return new HashSet<>();
    return this.getInitialisingCompounds(new HashSet<>());
  }

  @Override
  protected Set<UCompoundTransition> getInitialisingCompounds(Set<UVertex> visited) {
    if (!visited.add(this))
      return new HashSet<>();

    Set<UCompoundTransition> compounds = new HashSet<>();
    for (UCompoundTransition compound : this.getOutgoingCompounds()) {
      compounds.add(compound);
      for (UVertex vertex : compound.getTargets())
        compounds.addAll(vertex.getInitialisingCompounds(visited));
    }
    return compounds;
  }

  @Override
  protected List<UTransitionTree> getForwardPaths(UTransition incoming, Set<UVertex> visited) {
    switch (this.kind) {
      case JUNCTION: {
        if (visited.contains(this)) {
          Message.warning("Junction state " + Formatter.quoted(this.getIdentifier()) + " on a cycle");
          return new ArrayList<>();
        }
        Set<UVertex> nextVisited = new HashSet<>(visited);
        nextVisited.add(this);

        return this.getForwardPaths(nextVisited).stream().map(nextForwardPath -> new UTransitionTree(incoming, nextForwardPath)).collect(toList());
      }

      case FORK:
        return Arrays.asList(new UTransitionTree(incoming, this.getOutgoings().stream().map(outgoing -> new UTransitionTree(outgoing)).collect(toList())));

      // Stop at all other kinds of pseudo-states
      default:
        return Arrays.asList(new UTransitionTree(incoming));
    }
  }

  protected List<UTransitionTree> getForwardTrees(UTransition incoming, Set<UVertex> visited) {
    switch (this.kind) {
      case JUNCTION: {
        if (visited.contains(this)) {
          Message.warning("Junction state " + Formatter.quoted(this.getIdentifier()) + " on a cycle");
          return new ArrayList<>();
        }
        Set<UVertex> nextVisited = new HashSet<>(visited);
        nextVisited.add(this);

        return this.getForwardTrees(nextVisited).stream().map(nextForwardTree -> new UTransitionTree(incoming, nextForwardTree)).collect(toList());
      }

      case FORK: {
        List<List<UTransitionTree>> nextForwardTreeLists = new ArrayList<>();

        // The target of a transition outgoing from a fork must be a
        // state inside some orthogonal region
        UState targetsContainer = requireNonNull(this.getOutgoings().iterator().next().getTarget().getContainer().getContainer());
        for (URegion targetsContainerRegion : targetsContainer.getRegions()) {
          UTransition targetingOutgoing = this.getOutgoings().stream().filter(outgoing ->
                                              targetsContainerRegion.equals(outgoing.getTarget().getContainer())).findAny().orElse(null);
          if (targetingOutgoing == null)
            nextForwardTreeLists.add(requireNonNull(targetsContainerRegion.getInitial()).getForwardTrees(visited));
          else
            nextForwardTreeLists.add(targetingOutgoing.getTarget().getForwardTrees(targetingOutgoing, visited));
        }
        return Lists.product(nextForwardTreeLists).stream().map(nextForwardTreeBranches -> new UTransitionTree(incoming, nextForwardTreeBranches)).collect(toList());
      }

      default:
        return Arrays.asList(new UTransitionTree(incoming));
    }
  }

  @Override
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(prefix);
    resultBuilder.append(this.kind.getName());
    resultBuilder.append(" ");
    resultBuilder.append(this.getName());
    resultBuilder.append(";");
    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("PseudoState [name = ");
    resultBuilder.append(this.getIdentifier().toString());
    resultBuilder.append(", kind = ");
    resultBuilder.append(this.kind.getName());
    resultBuilder.append(", #incomings = ");
    resultBuilder.append(this.getIncomings().size());
    resultBuilder.append(", #outgoings = ");
    resultBuilder.append(this.getOutgoings().size());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
