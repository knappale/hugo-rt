package uml.statemachine;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAction;
import uml.UElement;
import uml.UExpression;
import uml.statemachine.semantics.UCompoundTransition;
import uml.statemachine.semantics.UVertexTree;
import util.Identifier;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static util.Objects.requireNonNull;
import static util.Objects.toType;


/**
 * Contains data of a (UML state machine) region.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @since 2016-03-17
 */
@NonNullByDefault
public class URegion extends UElement {
  private UStateMachine stateMachine;
  private @Nullable UState container;
  private Set<UVertex> vertices;
  private Set<UTransition> transitions;

  /**
   * Create a new top (UML) region for a state machine.
   *
   * @param name name of the region
   * @param stateMachine region's state machine
   */
  URegion(String name, UStateMachine stateMachine) {
    super(name);
    this.stateMachine = stateMachine;
    this.vertices = new HashSet<>();
    this.transitions = new HashSet<>();
  }

  /**
   * Create a new (UML) region for a state.
   *
   * @param name name of the region
   * @param container region's containing state
   */
  URegion(String name, UState container) {
    super(name);
    this.container = container;
    this.stateMachine = container.getStateMachine();
    this.vertices = new HashSet<>();
    this.transitions = new HashSet<>();
  }

  /**
   * Add a new (UML) state to this region.
   *
   * @param name state's name
   * @return the new state in this region
   */
  public UState addState(String name) {
    UState uState = new UState(name, this);
    this.vertices.add(uState);
    return uState;
  }

  /**
   * Add a new (UML) final state to this region.
   *
   * @param name final state's name
   * @return the new final state in this region
   */
  public UFinalState addFinalState(String name) {
    UFinalState finalState = new UFinalState(name, this);
    this.vertices.add(finalState);
    return finalState;
  }

  /**
   * Add a new (UML) pseudo-state to this region.
   *
   * @param name pseudo-state's name
   * @param kind pseudo-state's kind
   * @return the new pseudo-state in this region
   */
  public UPseudoState addPseudoState(String name, UPseudoState.Kind kind) {
    UPseudoState pseudoState = new UPseudoState(name, kind, this);
    this.vertices.add(pseudoState);
    return pseudoState;
  }

  /**
   * Add a new (UML) transition to this region.
   *
   * @param source transition's source vertex
   * @param target transition's target vertex
   * @param trigger transition's trigger
   * @param guard transition's guard
   * @param effect transition's effect
   * @return the new transition in this region
   */
  public UTransition addTransition(UVertex source, UVertex target, UEvent trigger, UExpression guard, UAction effect) {
    UTransition uTransition = new UTransition(source, target, trigger, guard, effect, this);
    this.transitions.add(uTransition);
    return uTransition;
  }

  /**
   * @return region's owning state machine
   */
  public UStateMachine getStateMachine() {
    return this.stateMachine;
  }

  /**
   * @return region's containing state; if this region is a top region
   * of its state machine, then {@code null} is returned
   */
  public @Nullable UState getContainer() {
    return this.container;
  }

  /**
   * @return whether this region is an orthogonal region
   */
  public boolean isOrthogonal() {
    var container = this.container;
    if (container != null) 
      return container.getRegions().size() > 1;
    return this.stateMachine.getRegions().size() > 1;
  }

  /**
   * @return region's (orthogonal) siblings
   */
  public Set<URegion> getSiblings() {
    var container = this.container;
    var regions = container != null ? container.getRegions() : this.stateMachine.getRegions();
    return regions.stream().filter(region -> !this.equals(region)).collect(toSet());
  }

  /**
   * @return region's least containing orthogonal region, or {@code null} if there is none
   */
  public @Nullable URegion getLeastOrthogonal() {
    if (this.isOrthogonal())
      return this;
    UState container = this.getContainer();
    if (container == null)
      return null;
    return container.getContainer().getLeastOrthogonal();
  }

  /**
   * Determine whether this region is a reflexive ancestor region of
   * another region.
   *
   * @param other another region
   * @return whether this region is a reflexive ancestor region of {@code other}
   */
  public boolean isAncestor(URegion other) {
    if (this.equals(other))
      return true;
    UState otherContainer = other.getContainer();
    if (otherContainer == null)
      return false;
    return this.isAncestor(otherContainer.getContainer());
  }

  /**
   * @return nesting level of this region
   */
  public int getLevel() {
    UState container = this.container;
    if (container == null)
      return 0;
    return container.getLevel()+1;
  }

  /**
   * @return region's identifier
   */
  public Identifier getIdentifier() {
    UState uState = this.container;
    if (uState == null) {
      if (this.isOrthogonal())
        return Identifier.id(this.getName());
      return Identifier.empty();
    }
    if (!this.isOrthogonal())
      return uState.getIdentifier();
    return Identifier.id(uState.getIdentifier(), this.getName());
  }

  /**
   * @return region's vertices
   */
  public Set<UVertex> getVertices() {
    return new HashSet<>(this.vertices);
  }

  /**
   * @return region's transitively contained vertices
   */
  public Set<UVertex> getAllUVertices() {
    return Stream.concat(this.vertices.stream(),
                         this.vertices.stream().flatMap(toType(requireNonNull(UState.class))).
                                                flatMap(state -> state.getAllVertices().stream())).collect(toSet());
  }

  /**
   * @return region's transitively contained regions including this region
   */
  @SuppressWarnings("null")
  public Set<URegion> getAllRegions() {
    return Stream.concat(Stream.of(this),
                         this.vertices.stream().flatMap(toType(requireNonNull(UState.class))).
                                                flatMap(state -> state.getAllRegions().stream())).
           collect(toSet());
  }

  /**
   * @return region's transitions
   */
  public Set<UTransition> getTransitions() {
    return this.transitions;
  }

  /**
   * @return region's initial pseudo-state, or {@code null} if there is none
   */
  public @Nullable UPseudoState getInitial() {
    return this.vertices.stream().flatMap(toType(requireNonNull(UPseudoState.class))).filter(vertex -> vertex.getKind() == UPseudoState.Kind.INITIAL).
                                  findAny().orElse(null);
  }

  /**
   * @return region's final sub-state, or {@code null} if there is none
   */
  public @Nullable UFinalState getFinal() {
    return this.getVertices().stream().flatMap(toType(requireNonNull(UFinalState.class))).findAny().orElse(null);
  }

  /**
   * @return region's shallow history pseudo-state, or {@code null} if there is none
   */
  public @Nullable UPseudoState getShallowHistory() {
    return this.vertices.stream().flatMap(toType(requireNonNull(UPseudoState.class))).filter(vertex -> vertex.getKind() == UPseudoState.Kind.SHALLOWHISTORY).
                                  findAny().orElse(null);
  }

  /**
   * Choose a state vertex of this region from a collection of candidates.
   *
   * @param candidates collection of state vertices
   * @return a vertex which is in the collection of candidates, or {@code null} if there is none
   */
  public @Nullable UVertex chooseVertex(Set<UVertex> candidates) {
    return this.vertices.stream().filter(vertex -> candidates.contains(vertex)).findAny().orElse(null);
  }

  /**
   * @return region's vertex trees
   */
  public List<UVertexTree> getVertexTrees() {
    return this.vertices.stream().flatMap(vertex -> vertex.getVertexTrees().stream()).collect(toList());
  }

  /**
   * @return region's target vertex tree for a set of vertices
   */
  public @Nullable UVertexTree getTargetVertexTree(Set<UVertex> vertices) {
    UVertex contained = this.vertices.stream().filter(state -> vertices.contains(state)).findAny().orElse(null);
    if (contained == null)
      return this.getInitialVertexTree();
    return contained.getTargetVertexTree(vertices);
  }

  /**
   * @return region's initial vertex tree, and {@code null} if this region has no initial pseudo-state
   */
  public @Nullable UVertexTree getInitialVertexTree() {
    UPseudoState initial = this.getInitial();
    if (initial == null)
      return null;
    return UVertexTree.simple(initial);
  }

  /**
   * @return region's final state tree, and {@code null} if this region has no final pseudo-state
   */
  public @Nullable UVertexTree getFinalVertexTree() {
    UFinalState finalState = this.getFinal();
    if (finalState == null)
      return null;
    return UVertexTree.simple(finalState);
  }

  /**
   * @return all compound transitions used to initialise this region
   */
  public Set<UCompoundTransition> getInitialisingCompounds() {
    UPseudoState initial = this.getInitial();
    if (initial == null)
      return new HashSet<>();
    return initial.getInitialisingCompounds();
  }

  /**
   * @param prefix prefix string
   * @return region's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    for (UVertex uVertex : this.vertices) {
      resultBuilder.append(uVertex.declaration(prefix));
      resultBuilder.append("\n");
    }
    if (!this.vertices.isEmpty() && !this.transitions.isEmpty())
      resultBuilder.append("\n");
    for (UTransition uTransition : this.transitions) {
      resultBuilder.append(uTransition.declaration(prefix));
      resultBuilder.append("\n");
    }

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Region [name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append(", #vertices = ");
    resultBuilder.append(this.vertices.size());
    resultBuilder.append(", #transitions = ");
    resultBuilder.append(this.transitions.size());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
