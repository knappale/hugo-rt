package uml.statemachine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UBehavioural;
import uml.UClass;
import uml.UContext;
import uml.smile.SMachine;
import uml.statemachine.semantics.UCompoundTransition;
import uml.statemachine.semantics.UConfiguration;
import uml.statemachine.semantics.UVertexForest;
import uml.statemachine.semantics.UVertexTree;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static util.Objects.ignoreNull;
import static util.Objects.requireNonNull;
import static util.Objects.toType;


/**
 * UML state machine
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UStateMachine {
  private Map<control.Properties, SMachine> sMachines = new HashMap<>();

  private UClass c1ass;
  private List<URegion> regions = new ArrayList<>();

  /**
   * Create a new empty state machine.
   *
   * @param c1ass state machine's (UML) class
   */
  public UStateMachine(UClass c1ass) {
    this.c1ass = c1ass;
  }

  /**
   * @return class to which this state machine belongs
   */
  public UClass getC1ass() {
    return this.c1ass;
  }

  /**
   * @return state machine's top-level regions
   */
  public List<URegion> getRegions() {
    return this.regions;
  }

  /**
   * Add a top-level region to this state machine.
   *
   * @param name region's name
   * @return new top-level region
   */
  public URegion addRegion(String name) {
    URegion region = new URegion(name, this);
    this.regions.add(region);
    return region;
  }

  /**
   * @return all regions of this state machine
   */
  public List<URegion> getAllRegions() {
    return this.regions.stream().flatMap(region -> region.getAllRegions().stream()).collect(toList());
  }

  /**
   * @return state machine's vertices
   */
  public Set<UVertex> getVertices() {
    return this.regions.stream().flatMap(region -> region.getAllUVertices().stream()).collect(toSet());
  }

  /**
   * @return state machine's configurations
   */
  public Set<UConfiguration> getConfigurations() {
    List<List<UVertexTree>> subStateTrees = this.getRegions().stream().map(URegion::getVertexTrees).collect(toList());
    List<UVertexForest> stateForests = util.Lists.product(subStateTrees).stream().map(productList -> new UVertexForest(productList)).collect(toList());
    return stateForests.stream().map(stateForest -> UConfiguration.create(stateForest)).collect(toSet());
  }

  /**
   * Build the state forest for a set of vertices, i.e., the minimal set
   * of state trees containing the vertices.
   *
   * @param vertices a set of vertices
   * @return the state forest for {@code vertices}
   */
  public UVertexForest getTargetStateForest(Set<UVertex> vertices) {
    Stream<UVertexTree> subStateTrees = this.regions.stream().flatMap(region -> ignoreNull(region.getTargetVertexTree(vertices)));
    return new UVertexForest(subStateTrees.collect(toList()));
  }

  /**
   * @return state machine's transitions
   */
  public Set<UTransition> getTransitions() {
    return this.getAllRegions().stream().flatMap(uRegion -> uRegion.getTransitions().stream()).collect(toSet());
  }

  /**
   * @return state machine's compound transitions
   */
  public Set<UCompoundTransition> getCompounds() {
    Set<UCompoundTransition> compoundTransitions = new HashSet<>();
    for (UVertex vertex : this.getVertices()) {
      if (vertex.isSource())
        compoundTransitions.addAll(vertex.getOutgoingCompounds());
    }
    return compoundTransitions;
  }

  /**
   * @return state machine's states
   */
  private Set<UState> getStates() {
    return this.getVertices().stream().flatMap(toType(UState.class)).collect(toSet());
  }

  /**
   * @return set of events this state machine can react to
   */
  public Set<UEvent> getEvents() {
    Set<UEvent> events = new HashSet<>();
    events.addAll(this.getExternalEvents());
    events.addAll(this.getInternalEvents());
    return events;
  }

  /**
   * @return set of external events this state machine can react to
   */
  public Set<UEvent> getExternalEvents() {
    Set<UEvent> events = new HashSet<>();
    events.addAll(this.c1ass.getOperations().stream().filter(o -> !o.isStatic()).map(o -> UEvent.call(o)).collect(toSet()));
    events.addAll(this.c1ass.getReceptions().stream().map(r -> UEvent.signal(r)).collect(toSet()));
    return events;
  }

  /**
   * @return set of internal events this state machine can react to
   */
  public Set<UEvent> getInternalEvents() {
    Set<UEvent> events = new HashSet<>();
    events.addAll(this.getCompletionStates().stream().map(s -> UEvent.completion(s)).collect(toSet()));
    events.addAll(this.getWaitStates().stream().map(s -> UEvent.completion(s)).collect(toSet()));
    events.addAll(this.getTimeds().stream().map(t -> t.getTrigger()).collect(toSet()));
    return events;
  }
    
  /**
   * @return state machine's deferrable events
   */
  public Set<UEvent> getDeferrableEvents() {
    return this.getStates().stream().flatMap(state -> state.getDeferrableEvents().stream()).collect(toSet());
  }

  /**
   * @return set of states with completion transitions of this state machine
   */
  public Set<UState> getCompletionStates() {
    return this.getStates().stream().filter(state -> state.hasOutgoingCompletion()).collect(toSet());
  }

  /**
   * @return set of states with wait transitions of this state machine
   */
  public Set<UState> getWaitStates() {
    return this.getStates().stream().filter(state -> !state.getOutgoingWaits().isEmpty()).collect(toSet());
  }

  /**
   * @return set of timed transitions of this state machine
   */
  public Set<UTransition> getTimeds() {
    return this.getStates().stream().flatMap(state -> state.getOutgoingTimes().stream()).collect(toSet());
  }

  /**
   * @return call events this state machine may react to
   */
  public Set<UEvent> getSynchronousCallEvents() {
    return this.c1ass.getOperations().stream().filter(o -> !o.isStatic()).map(o -> UEvent.call(o)).collect(toSet());
  }

  /**
   * @return set of behaviourals that may be invoked by this state machine
   */
  public Set<UBehavioural> getInvokeds() {
    var context = UContext.c1ass(this.getC1ass());
    var calls = new HashSet<UBehavioural>();
    for (var state : this.getStates()) {
      calls.addAll(state.getEntryAction().getBehaviourals(context));
      calls.addAll(state.getExitAction().getBehaviourals(context));
    }
    for (var transition : this.getTransitions())
      calls.addAll(transition.getEffect().getBehaviourals(context));
    return calls;
  }

  /**
   * Least common ancestor algorithm from UML 2.5 specification, p. 357,
   * extended to vertices.
   *
   * @param vertex1 a state vertex
   * @param vertex2 a state vertex
   * @return least reflexive common ancestor state of {@code vertex1} and {@code vertex2}
   * @precondition there is a common ancestor state of {@code vertex1} and {@code vertex2}
   */
  private static UVertex leastReflexiveCommonAncestorVertex(UVertex vertex1, UVertex vertex2) {
    if (vertex1.isAncestor(vertex2))
      return vertex1;
    if (vertex2.isAncestor(vertex1))
      return vertex2;

    UState containerState1 = requireNonNull(vertex1.getContainer().getContainer());
    UState containerState2 = requireNonNull(vertex2.getContainer().getContainer());
    return leastReflexiveCommonAncestorState(containerState1, containerState2);
  }

  /**
   * Least common ancestor algorithm from UML 2.5 specification, p. 357.
   *
   * This algorithm does not necessarily result in a composite state,
   * as {@code state1} and {@code state2} may be equal.
   *
   * @param state1 a state
   * @param state2 a state
   * @return least reflexive common ancestor state of {@code state1} and {@code state2}
   * @precondition there is a common ancestor state of {@code state1} and {@code state2}
   */
  private static UState leastReflexiveCommonAncestorState(UState state1, UState state2) {
    if (state1.isAncestor(state2))
      return state1;
    if (state2.isAncestor(state1))
      return state2;

    UState containerState1 = requireNonNull(state1.getContainer().getContainer());
    UState containerState2 = requireNonNull(state2.getContainer().getContainer());
    return leastReflexiveCommonAncestorState(containerState1, containerState2);
  }

  /**
   * Determine the least reflexive common ancestor of a collection of vertices.
   *
   * @see #leastReflexiveCommonAncestorState
   *
   * @param vertices a collection of vertices
   * @return least reflexive common ancestor of {@code vertices}
   * @precondition vertices.size() > 0
   */
  public static UVertex leastReflexiveCommonAncestorVertex(Collection<UVertex> vertices) {
    assert (!vertices.isEmpty());

    Iterator<UVertex> verticesIt = vertices.iterator();
    UVertex lca = verticesIt.next();
    while (verticesIt.hasNext())
      lca = leastReflexiveCommonAncestorVertex(lca, verticesIt.next());
    return lca;
  }

  /**
   * Least common ancestor state algorithm from UML 2.5 specification,
   * p. 357 extended by really resulting in a composite state.
   *
   * @param vertex1 a vertex
   * @param vertex2 a vertex
   * @return the composite state that is the least common ancestor
   * of {@code vertex1} and {@code vertex2}
   * @precondition there is a common ancestor state of {@code vertex1} and {@code vertex2}
   */
  public static UState leastCommonAncestorState(UVertex vertex1, UVertex vertex2) {
    if (vertex1.isAncestor(vertex2)) {
      UState state1 = (vertex1 instanceof UState) ? (UState)vertex1 : requireNonNull(vertex1.getContainer().getContainer());
      if (state1.getRegions().size() > 0)
        return state1;
      else
        return requireNonNull(state1.getContainer().getContainer());
    }
    if (vertex2.isAncestor(vertex1)) {
      UState state2 = (vertex2 instanceof UState) ? (UState)vertex2 : requireNonNull(vertex2.getContainer().getContainer());
      if (state2.getRegions().size() > 0)
        return state2;
      else
        return requireNonNull(state2.getContainer().getContainer());
    }

    UState containerState1 = requireNonNull(vertex1.getContainer().getContainer());
    UState containerState2 = requireNonNull(vertex2.getContainer().getContainer());
    return leastCommonAncestorState(containerState1, containerState2);
  }

  /**
   * Determine the least common ancestor state of a collection of states.
   *
   * @see #leastCommonAncestorState
   *
   * @param states a collection of states
   * @return the least common ancestor state of {@code states}
   * @precondition {@code states.size() > 0}
   * @precondition {@code states} have a common ancestor state
   */
  public static UState leastCommonAncestorState(Collection<UState> states) {
    assert (!states.isEmpty());

    Iterator<UState> statesIt = states.iterator();
    UState lca = statesIt.next();
    while (statesIt.hasNext())
      lca = leastReflexiveCommonAncestorState(lca, statesIt.next());
    if (lca.getRegions().size() == 0)
      lca = requireNonNull(lca.getContainer().getContainer());
    return lca;
  }

  /**
   * Least common ancestor algorithm from UML 2.5 specification,
   * p. 356 extended by really resulting in the least common ancestor region.
   *
   * @param vertex1 a state vertex
   * @param vertex2 a state vertex
   * @return the least common ancestor region of {@code vertex1} and {@code vertex2}
   * @precondition there is a common ancestor region of {@code vertex1} and {@code vertex2}
   */
  public static URegion leastCommonAncestorRegion(UVertex vertex1, UVertex vertex2) {
    URegion region1 = vertex1.getContainer();
    URegion region2 = vertex2.getContainer();

    if (region1.isAncestor(region2))
      return region1;
    if (region2.isAncestor(region1))
      return region2;

    UState containerState1 = requireNonNull(region1.getContainer());
    UState containerState2 = requireNonNull(region2.getContainer());
    return leastCommonAncestorRegion(containerState1, containerState2);
  }

  /**
   * @return state machine's Smile representation
   */
  public SMachine getMachine(control.Properties properties) {
    SMachine sMachine = this.sMachines.get(properties);
    if (sMachine != null)
      return sMachine;

    sMachine = uml.smile.translation.STranslator.translateStateMachine(this, properties);
    this.sMachines.put(properties, sMachine);
    return sMachine;
  }

  /**
   * @param prefix prefix string
   * @return state machine's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder result = new StringBuilder();
    String nextPrefix = prefix + "  ";

    result.append(prefix);
    result.append("states {\n");
    for (UVertex vertex : this.getRegions().stream().flatMap(region -> region.getVertices().stream()).collect(toList())) {
      result.append(vertex.declaration(nextPrefix));
      result.append("\n");
    }
    result.append(prefix);
    result.append("}\n\n");

    result.append(prefix);
    result.append("transitions {\n");
    for (UTransition transition : this.getTransitions()) {
      result.append(transition.declaration(nextPrefix));
      result.append("\n");
    }
    result.append(prefix);
    result.append("}");

    return result.toString();
  }

  /**
   * @return state machine's representation in UTE format
   */
  public String declaration() {
    return this.declaration("");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.c1ass, this.regions);
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("StateMachine [class = ");
    resultBuilder.append(this.c1ass.getName());
    resultBuilder.append(", #regions = ");
    resultBuilder.append(this.regions.size());
    resultBuilder.append(", #transitions = ");
    resultBuilder.append(this.getTransitions().size());
    resultBuilder.append("]");

    return resultBuilder.toString();
  }
}
