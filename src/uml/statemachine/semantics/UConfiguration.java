package uml.statemachine.semantics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UExpression;
import uml.statemachine.UEvent;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;

import static util.Objects.requireNonNull;


/**
 * State machine configuration
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UConfiguration {
  // Keep a set of canonical instances
  private static Map<UVertexForest, UConfiguration> instances = new HashMap<>();
  // Cache outgoing maximal conflict free sets of compound transitions
  private @Nullable Set<UConflictFreeSet> outgoingMaximalConflictFrees;

  private UVertexForest vertexForest;

  private UConfiguration(UVertexForest stateForest) {
    this.vertexForest = stateForest;
  }

  /**
   * Create a configuration for a state forest.
   *
   * A canonical list of configurations for state forests is maintained;
   * if there is already a configuration for a state forest, this
   * configuration is returned.
   *
   * @param stateForest a state forest
   * @return configuration for a state forest
   */
  public static UConfiguration create(UVertexForest stateForest) {
    UConfiguration knownInstance = instances.get(stateForest);
    if (knownInstance != null)
      return knownInstance;

    knownInstance = new UConfiguration(stateForest);
    instances.put(stateForest, knownInstance);
    return knownInstance;
  }

  /**
   * @return configuration's name
   */
  public String getName() {
    return this.vertexForest.getName();
  }

  /**
   * @param vertex a vertex
   * @return whether configuration contains {@code vertex}
   */
  public boolean contains(UVertex vertex) {
    return this.vertexForest.contains(vertex);
  }

  /**
   * @return this configuration's vertices
   */
  public Set<UVertex> getVertices() {
    return this.vertexForest.getVertices();
  }

  /**
   * @return whether configuration is initial
   */
  public boolean isInitial() {
    return this.vertexForest.isInitial();
  }

  /**
   * @return whether configuration contains a pseudo-state
   */
  public boolean isPseudo() {
    return this.vertexForest.isPseudo();
  }

  /**
   * Determine all compound transitions outgoing from this
   * configuration.
   *
   * @return set of outgoing compound transitions
   */
  private Set<UCompoundTransition> getOutgoingCompounds() {
    return this.vertexForest.getOutgoingCompounds();
  }

  /**
   * Determine all prioritised compound transitions outgoing from this
   * configuration.
   *
   * If some compound transition outgoing from this configuration
   * takes priority over another outgoing compound transition, the
   * latter can only be fired if the first is not enabled.  The
   * outgoing compound transition with lower priority therefore has a
   * new priority guard added.
   *
   * @return set of outgoing prioritised compound transitions
   */
  private Set<UCompoundTransition> getOutgoingPrioritisedCompounds() {
    Set<UCompoundTransition> outgoingPrioritisedCompounds = new HashSet<>();

    // Prioritise the outgoing compound transitions: some transition chains
    // may get a new guard, some transition chains may have to be omitted
    Set<UCompoundTransition> outgoingCompounds = this.getOutgoingCompounds();
    for (UCompoundTransition outgoingCompound1 : outgoingCompounds) {
      UExpression priorityGuard = UExpression.trueConst();
      for (UCompoundTransition outgoingCompound2 : outgoingCompounds) {
        if (outgoingCompound1.getTrigger().equals(outgoingCompound2.getTrigger())) {
          if (outgoingCompound2.hasPriority(outgoingCompound1)) {
            UExpression outgoingCompound2NegatedGuard = UExpression.neg(outgoingCompound2.getGuard());
            priorityGuard = UExpression.and(priorityGuard, outgoingCompound2NegatedGuard);
          }
        }
      }

      if (!priorityGuard.equals(UExpression.falseConst()))
        outgoingPrioritisedCompounds.add(new UCompoundTransition(priorityGuard, outgoingCompound1));
    }
    return outgoingPrioritisedCompounds;
  }

  /**
   * Determine the set of maximal conflict free sets of compound
   * transitions outgoing from this configuration.
   *
   * A maximal conflict free set of outgoing (prioritised) compound
   * transitions is a set of outgoing (prioritised) compound
   * transitions showing the same event such that all compound
   * transitions in this set are enabled and no enabled outgoing
   * (prioritised) compound transition can be added to this set
   * without violating conflict freedom.  The maximal sets that can be
   * inferred statically may contain disabled compound transitions.
   * Thus, conflict guards are added to the subsets of these static
   * maximal conflict free sets to ensure that they only can be taken
   * when indeed only these conflict free prioritised compound
   * transitions are enabled.
   *
   * @return set of outgoing maximal conflict free sets
   */
  public Set<UConflictFreeSet> getOutgoingMaximalConflictFrees() {
    // Take a look at the cache
    Set<UConflictFreeSet> maximalConflictFreeSets = this.outgoingMaximalConflictFrees; 
    if (maximalConflictFreeSets != null)
      return maximalConflictFreeSets;

    maximalConflictFreeSets = new HashSet<>();

    // Compute all triggering events of outgoing compound transitions,
    // recording all prioritised outgoing compound transitions for a
    // given triggering event
    Map<UEvent, Set<UCompoundTransition>> eventCompoundsMap = new HashMap<>();
    for (UCompoundTransition outgoingCompound : this.getOutgoingPrioritisedCompounds()) {
      UEvent trigger = outgoingCompound.getTrigger();
      Set<UCompoundTransition> eventCompounds = eventCompoundsMap.get(trigger);
      if (eventCompounds == null)
        eventCompounds = new HashSet<>();
      eventCompounds.add(outgoingCompound);
      eventCompoundsMap.put(trigger, eventCompounds);
    }

    // Compute all conflict free sets of outgoing compound transitions
    // for a given triggering event
    Set<Set<UCompoundTransition>> outgoingConflictFreeSets = new HashSet<>();
    for (UEvent event : eventCompoundsMap.keySet()) {
      Set<UCompoundTransition> outgoingCompounds = requireNonNull(eventCompoundsMap.get(event));
      outgoingConflictFreeSets.addAll(this.conflictFreeSubsets(outgoingCompounds));
    }

    // Compute all maximal prioritised conflict free sets
    Set<Set<UCompoundTransition>> maximalOutgoingConflictFreeSets = new HashSet<>();
    for (Set<UCompoundTransition> outgoingConflictFreeSet1 : outgoingConflictFreeSets) {
      boolean contained = false;
      for (Set<UCompoundTransition> outgoingConflictFreeSet2 : outgoingConflictFreeSets) {
        contained |= (!outgoingConflictFreeSet2.equals(outgoingConflictFreeSet1)) && outgoingConflictFreeSet2.containsAll(outgoingConflictFreeSet1);
      }
      if (!contained && !outgoingConflictFreeSet1.isEmpty())
        maximalOutgoingConflictFreeSets.add(outgoingConflictFreeSet1);
    }

    // Compute all maximal prioritised conflict free sets with adding
    // conflict guards
    for (Set<UCompoundTransition> outgoingConflictFreeSet : outgoingConflictFreeSets) {
      Set<UCompoundTransition> maximumDifference = new HashSet<>();
      for (Set<UCompoundTransition> maximalOutgoingConflictFreeSet : maximalOutgoingConflictFreeSets) {
        if (maximalOutgoingConflictFreeSet.containsAll(outgoingConflictFreeSet))
          maximumDifference.addAll(util.Sets.difference(maximalOutgoingConflictFreeSet, outgoingConflictFreeSet));
      }
      UExpression conflictGuard = UExpression.trueConst();
      for (UCompoundTransition transition : maximumDifference) {
        conflictGuard = UExpression.and(conflictGuard, UExpression.neg(transition.getGuard()));
      }
      maximalConflictFreeSets.add(new UConflictFreeSet(outgoingConflictFreeSet, conflictGuard));
    }

    // Cache outgoing maximal conflict free sets of compound transitions
    this.outgoingMaximalConflictFrees = maximalConflictFreeSets;
    return maximalConflictFreeSets;
  }

  /**
   * Determine the set of conflict free subsets of a set of compound
   * transitions.
   *
   * It is presupposed that all compound transitions in the set of
   * compound transitions show the same triggering event.
   *
   * @param compounds set of compound transitions
   * @return set of conflict free subsets of set of compound
   * transitions
   *
   * @pre \forall compound1, compound2 \in compounds .
   *        compound1.getTrigger().equals(compound2.getTrigger())
   */
  private Set<Set<UCompoundTransition>> conflictFreeSubsets(Set<UCompoundTransition> compounds) {
    Set<Set<UCompoundTransition>> conflictFreeSubsets = new HashSet<>();

    // Every singleton set of compound transitions is conflict free
    if (compounds.size() == 1) {
      Set<UCompoundTransition> conflictFreeSubset = new HashSet<>();
      conflictFreeSubset.addAll(compounds);
      conflictFreeSubsets.add(conflictFreeSubset);
      return conflictFreeSubsets;
    }

    for (UCompoundTransition compound : compounds) {
      // Determine the conflict free subsets of all compound
      // transitions omitting one
      Set<UCompoundTransition> remainingCompounds = new HashSet<>();
      remainingCompounds.addAll(compounds);
      remainingCompounds.remove(compound);
      Set<Set<UCompoundTransition>> remainingConflictFreeSubsets = this.conflictFreeSubsets(remainingCompounds);
      conflictFreeSubsets.addAll(remainingConflictFreeSubsets);

      // Again the singleton \{ compound \} is conflict free
      {
        Set<UCompoundTransition> conflictFreeSubset = new HashSet<>();
        conflictFreeSubset.add(compound);
        conflictFreeSubsets.add(conflictFreeSubset);
      }

      // Consider all conflict free subsets missing the singled-out
      // compound transition and look whether the singled-out compound
      // transition can be added without incurring a conflict
      for (Set<UCompoundTransition> remainingConflictFreeSubset : remainingConflictFreeSubsets) {
        if (!compound.isInConflict(remainingConflictFreeSubset)) {
          Set<UCompoundTransition> conflictFreeSubset = new HashSet<>();
          conflictFreeSubset.addAll(remainingConflictFreeSubset);
          conflictFreeSubset.add(compound);
          conflictFreeSubsets.add(conflictFreeSubset);
        }
      }
    }

    return conflictFreeSubsets;
  }

  /**
   * Determine the configuration targeted by a set of maximal conflict
   * free outgoing compound transitions.
   *
   * @param outgoingCompounds a maximal conflict free set of compound
   * transition outgoing from this configuration
   * @return target configuration of outgoing compound transitions
   *
   * @precondition getOutgoingCompounds().containsAll(outgoingCompounds) &&
   *               outgoingCompounds "is a maximal conflict free set"
   */
  UConfiguration getCompoundTarget(Set<UCompoundTransition> outgoingCompounds) {
    UConfiguration compoundTarget = this;
    for (UCompoundTransition outgoingCompound : outgoingCompounds) {
      compoundTarget = compoundTarget.getTarget(outgoingCompound);
    }
    return compoundTarget;
  }

  /**
   * Determine the configuration targeted by an outgoing compound
   * transition.
   *
   * -AK This method could also be placed on CompoundTransition if we
   * would know the source configuration of a compound transition.
   *
   * @param outgoingCompound a compound transition outgoing from this configuration
   * @return target configuration of outgoing compound transition
   *
   * @precondition getOutgoingCompounds().contains(outgoingCompound)
   */
  private UConfiguration getTarget(UCompoundTransition outgoingCompound) {
    UVertex mainSource = outgoingCompound.getMainSourceVertex();
    UVertex mainTarget = outgoingCompound.getMainTargetVertex();
    Set<UVertex> targets = outgoingCompound.getTargets();
    URegion lca = UStateMachine.leastCommonAncestorRegion(mainSource, mainTarget);
    UVertexTree target = lca.getTargetVertexTree(targets);
    if (target == null)
      return this;
    return UConfiguration.create(this.vertexForest.getSubstituted(target));
  }

  /**
   * Determine the configuration that arises when completing this
   * configuration via a completion event.
   *
   * Note that the completion event need not be a trigger for an
   * outgoing compound.  In particular, only a single state to be
   * completed is handled (and several completions are needed for a
   * join).
   *
   * @param completionEvent completion event
   * @return completed configuration for completion event, or
   * {@code null} if there is no such configuration
   * @pre completionEvent.isCompletion()
   */
  public @Nullable UConfiguration getCompletion(UEvent completionEvent) {
    if (!this.getCompletables().contains(completionEvent))
      return null;

    UState completionState = completionEvent.getStates().iterator().next();
    UVertexTree completedSubStateTree = completionState.getFinalVertexTree();
    if (completedSubStateTree == null)
      return null;

    return UConfiguration.create(vertexForest.getSubstituted(completedSubStateTree));
  }

  /**
   * Determine the configuration that arises when timing out this
   * configuration via a time event.
   *
   * @param timeEvent time event
   * @return timed-out configuration for time event, or
   * {@code null} if there is no such configuration
   * @pre timeEvent.isTime()
   */
  public @Nullable UConfiguration getTimedOut(UEvent timeEvent) {
    if (!this.getTimeOutables().contains(timeEvent))
      return null;

    UVertexTree subStateTree = vertexForest.getVertexTree(timeEvent.getState());
    if (subStateTree == null)
      return null;

    Set<UEvent> timedOuts = new HashSet<>();
    timedOuts.add(timeEvent);
    return UConfiguration.create(vertexForest.getSubstituted(UVertexTree.timedOut(timedOuts, subStateTree)));
  }

  /**
   * @see UVertexTree#getCompletables()
   *
   * @return completion events that are open in this configuration
   */
  public Set<UEvent> getCompletables() {
    return vertexForest.getCompletables();
  }

  /**
   * @see UVertexTree#getTimeOutables()
   *
   * @return time events that are open in this configuration
   */
  public Set<UEvent> getTimeOutables() {
    return vertexForest.getTimeOutables();
  }

  /**
   * @see UVertexTree#getDeferrables()
   *
   * @return deferred events that are open in this configuration
   */
  public Set<UEvent> getDeferrables() {
    return vertexForest.getDeferrables();
  }

  /**
   * @return declaration of this configuration as a string
   */
  public String declaration() {
    return this.vertexForest.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.vertexForest);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UConfiguration other = (UConfiguration)object;
      return Objects.equals(this.vertexForest, other.vertexForest);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Configuration [stateForest = ");
    resultBuilder.append(this.vertexForest);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
