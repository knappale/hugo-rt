package uml.statemachine.semantics;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.statemachine.UPseudoState;
import uml.statemachine.UVertex;


@NonNullByDefault
public class UPersistentState {
  private UVertex uVertex;

  public UPersistentState(UVertex uVertex) {
    this.uVertex = uVertex;
  }

  public UPseudoState.@Nullable Kind getKind() {
    return uVertex.getKind();
  }
}
