package uml.statemachine.semantics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import util.AndOrTree;

import static java.util.stream.Collectors.toList;


/**
 * Region tree
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class URegionTree {
  private static Map<AndOrTree<Optional<URegion>>, URegionTree> regionTrees = new HashMap<>();
  private AndOrTree<Optional<URegion>> andOrTree;

  private URegionTree(AndOrTree<Optional<URegion>> andOrTree) {
    this.andOrTree = andOrTree;
  }

  private static URegionTree create(AndOrTree<Optional<URegion>> andOrTree) {
    URegionTree regionTree = regionTrees.get(andOrTree);
    if (regionTree != null)
      return regionTree;

    regionTree = new URegionTree(andOrTree);
    regionTrees.put(andOrTree, regionTree);
    return regionTree;
  }

  public static URegionTree leaf(Optional<URegion> region) {
    return create(AndOrTree.leaf(region));
  }

  public static URegionTree and(Optional<URegion> region, List<URegionTree> subRegionTrees) {
    List<AndOrTree<Optional<URegion>>> andOrTrees = subRegionTrees.stream().map(subRegionTree -> subRegionTree.andOrTree).collect(toList());
    return create(AndOrTree.and(region, andOrTrees));
  }

  public static URegionTree or(Optional<URegion> region, List<URegionTree> subRegionTrees) {
    List<AndOrTree<Optional<URegion>>> andOrTrees = subRegionTrees.stream().map(subRegionTree -> subRegionTree.andOrTree).collect(toList());
    return create(AndOrTree.or(region, andOrTrees));
  }

  public Optional<URegion> getLabel() {
    return this.andOrTree.getLabel();
  }

  public List<URegionTree> getSubTrees() {
    return this.andOrTree.getSubTrees().stream().map(URegionTree::create).collect(toList());
  }

  public boolean isLeaf() {
    return this.andOrTree.isLeaf();
  }

  public boolean isAnd() {
    return this.andOrTree.isAnd();
  }

  public boolean isOr() {
    return this.andOrTree.isOr();
  }

  /**
   * @return state machine's vertex tree
   */
  public static URegionTree create(UStateMachine stateMachine) {
    return URegionTree.and(Optional.empty(), stateMachine.getRegions().stream().map(URegionTree::create).collect(toList()));
  }

  /**
   * @return vertex's region tree
   */
  public static URegionTree create(UVertex uVertex) {
    return uVertex.new Cases<URegionTree>().
             pseudoState(uPseudoState -> URegionTree.leaf(Optional.empty())).
             finalState(uFinalState -> URegionTree.leaf(Optional.empty())).
             state(uState -> create(uState)).
             apply();
  }

  /**
   * @return state's region tree
   */
  private static URegionTree create(UState uState) {
    if (uState.getRegions().size() == 0)
      return URegionTree.leaf(Optional.empty());

    if (uState.getRegions().size() == 1)
      return create(uState.getRegions().get(0));

    return URegionTree.and(Optional.empty(), uState.getRegions().stream().map(URegionTree::create).collect(toList()));
  }

  /**
   * @return region's region tree
   */
  private static URegionTree create(URegion region) {
    return URegionTree.or(Optional.of(region), region.getVertices().stream().map(URegionTree::create).collect(toList()));
  }

  /**
   * Assign a representing name to each region such that regions active
   * (only) at different times may share the same name.
   * 
   * @return mapping region to region name
   */
  public Map<URegion, String> getRegionNames() {
    Map<URegion, String> regionNames = new HashMap<>();
    this.getRegionNames(regionNames, 0);
    return regionNames;
  }

  private int getRegionNames(Map<URegion, String> regionNames, int n) {
    URegion region = this.getLabel().orElse(null);
    if (region != null && regionNames.get(region) == null)
      regionNames.put(region, "region" + n++);

    if (this.isLeaf())
      return n;

    if (this.isOr()) {
      int max = n;
      for (URegionTree subVertexTree : this.getSubTrees())
        max = Math.max(max, subVertexTree.getRegionNames(regionNames, n));
      return max;
    }

    if (this.isAnd()) {
      for (URegionTree subVertexTree : this.getSubTrees())
        n = subVertexTree.getRegionNames(regionNames, n);
      return n;
    }

    return 0;
  }
}
