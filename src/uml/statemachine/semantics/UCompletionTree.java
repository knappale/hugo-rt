package uml.statemachine.semantics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import util.AndOrTree;


/**
 * Completion tree, representing the set of states completed in a state tree.
 * 
 * A state is called a completion state if it is completable
 * and has an outgoing completion transition.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UCompletionTree {
  private static Map<AndOrTree<Set<UState>>, UCompletionTree> completionTrees = new HashMap<>();
  private AndOrTree<Set<UState>> andOrTree;

  private @Nullable Set<UState> cachedCompletionStates = null;
  private @Nullable Map<UState, Integer> cachedCompletionNumbers = null;

  private UCompletionTree(AndOrTree<Set<UState>> andOrTree) {
    this.andOrTree = andOrTree;
  }

  private static UCompletionTree create(AndOrTree<Set<UState>> andOrTree) {
    UCompletionTree completionTree = completionTrees.get(andOrTree);
    if (completionTree != null)
      return completionTree;

    completionTree = new UCompletionTree(andOrTree);
    completionTrees.put(andOrTree, completionTree);
    return completionTree;
  }

  private static UCompletionTree leaf(Set<UState> completionStates) {
    return create(AndOrTree.leaf(completionStates));
  }

  private static UCompletionTree and(Set<UState> completionStates, List<UCompletionTree> subCompletionTrees) {
    List<AndOrTree<Set<UState>>> andOrTrees = subCompletionTrees.stream().map(subCompletionTree -> subCompletionTree.andOrTree).collect(Collectors.toList());
    return create(AndOrTree.and(completionStates, andOrTrees));
  }

  private static UCompletionTree or(Set<UState> completionStates, List<UCompletionTree> subCompletionTrees) {
    List<AndOrTree<Set<UState>>> andOrTrees = subCompletionTrees.stream().map(subCompletionTree -> subCompletionTree.andOrTree).collect(Collectors.toList());
    return create(AndOrTree.or(completionStates, andOrTrees));
  }

  /**
   * @return state machine's completion tree
   */
  public static UCompletionTree create(UStateMachine uStateMachine) {
    return UCompletionTree.and(new HashSet<>(), create(uStateMachine.getRegions()));
  }

  /**
   * @return vertex's completion tree
   */
  private static UCompletionTree create(UVertex uVertex) {
    return uVertex.cases(uState -> create(uState),
                         uFinalState -> UCompletionTree.leaf(new HashSet<>()),
                         uPseudoState -> UCompletionTree.leaf(new HashSet<>()));
  }

  /**
   * @return state's completion tree
   */
  private static UCompletionTree create(UState uState) {
    Set<UState> completionStates = new HashSet<>();

    if (uState.isCompletable() && uState.hasOutgoingCompletion())
      completionStates.add(uState);

    if (uState.getRegions().size() == 0)
      return UCompletionTree.leaf(completionStates);

    if (uState.getRegions().size() == 1)
      return UCompletionTree.or(completionStates, create(uState.getRegions().get(0)));

    return UCompletionTree.and(completionStates, create(uState.getRegions()));
  }

  /**
   * @return region's completion trees
   */
  private static List<UCompletionTree> create(URegion uRegion) {
    return uRegion.getVertices().stream().map(uVertex -> create(uVertex)).filter(completionTree -> !completionTree.isEmpty()).collect(Collectors.toList());
  }

  /**
   * @return a list of region's completion trees
   */
  private static List<UCompletionTree> create(List<URegion> uRegions) {
    return uRegions.stream().map(uRegion -> UCompletionTree.or(new HashSet<>(), create(uRegion))).collect(Collectors.toList());
  }

  /**
   * @return completion tree's label
   */
  private Set<UState> getLabel() {
    return this.andOrTree.getLabel();
  }

  /**
   * @return timer tree's sub-trees
   */
  private List<UCompletionTree> getSubTrees() {
    List<UCompletionTree> subTimerTrees = new ArrayList<UCompletionTree>();
    for (AndOrTree<Set<UState>> subAndOrTree : this.andOrTree.getSubTrees()) {
      subTimerTrees.add(create(subAndOrTree));
    }
    return subTimerTrees;
  }

  /**
   * @return whether this timer tree is empty
   */
  private boolean isEmpty() {
    return (this.getLabel().isEmpty() && this.getSubTrees().isEmpty());
  }

  /**
   * @return whether this timer tree is a leaf
   */
  private boolean isLeaf() {
    return this.andOrTree.isLeaf();
  }

  /**
   * @return whether this timer tree is an and-timer tree
   */
  private boolean isAnd() {
    return this.andOrTree.isAnd();
  }

  /**
   * @return whether this timer tree is an or-timer tree
   */
  private boolean isOr() {
    return this.andOrTree.isOr();
  }

  /**
   * Determine all states occurring in this completion tree.
   * 
   * @return set of completion states
   */
  public Set<UState> getCompletionStates() {
    Set<UState> completionStates = this.cachedCompletionStates;
    if (completionStates != null)
      return completionStates;

    completionStates = new HashSet<>();
    completionStates.addAll(this.getLabel());
    if (this.isOr() || this.isAnd()) {
      for (UCompletionTree subCompletionTree : getSubTrees()) {
        completionStates.addAll(subCompletionTree.getCompletionStates());
      }
    }

    this.cachedCompletionStates = completionStates;
    return completionStates;
  }

  /**
   * Determine whether a state is a completion state.
   * 
   * @param state a (UML) state
   * @return whether <CODE>state</CODE> is  completion state
   */
  public boolean isCompletionState(UState state) {
    return this.getCompletionStates().contains(state);
  }

  /**
   * Assign a number to each completion state such that completion
   * states active at different times may share the same number.
   * 
   * @return mapping completion state to a number
   */
  private Map<UState, Integer> getCompletionNumbers() {
    Map<UState, Integer> completionNumbers = this.cachedCompletionNumbers;
    if (completionNumbers != null)
      return completionNumbers;
 
    completionNumbers = new HashMap<>();
    int n = this.getJoinCompletionNumbers(completionNumbers, 0);
    this.getCompletionNumbers(completionNumbers, n+1);

    this.cachedCompletionNumbers = completionNumbers;
    return completionNumbers;
  }

  /**
   * Assign a number to each completion state such that completion states
   * active at different times may share the same number.
   * 
   * @return maximal completion number used in this completion tree
   */
  private int getJoinCompletionNumbers(Map<UState, Integer> completionNumbers, int n) {
    for (UState completionState : this.getLabel()) {
      boolean targetsJoin = false;
      for (UCompoundTransition outgoingCompound : completionState.getOutgoingCompounds()) {
        if (outgoingCompound.isJoin()) {
          targetsJoin = true;
          break;
        }
      }
      if (targetsJoin)
        if (completionNumbers.get(completionState) == null)
          completionNumbers.put(completionState, n++);
    }

    for (UCompletionTree subCompletionTree : this.getSubTrees())
      n = subCompletionTree.getJoinCompletionNumbers(completionNumbers, n);

    return n;
  }

  /**
   * Assign a number to each completion state such that completion states
   * active at different times may share the same number.
   * 
   * @return maximal completion number used in this completion tree
   */
  private int getCompletionNumbers(Map<UState, Integer> completionNumbers, int n) {
    for (UState completionState : this.getLabel()) {
      if (completionNumbers.get(completionState) == null)
        completionNumbers.put(completionState, n++);
    }

    if (this.isLeaf())
      return n;

    if (this.isOr()) {
      int max = n;
      for (UCompletionTree subCompletionTree : this.getSubTrees()) {
        int k = subCompletionTree.getCompletionNumbers(completionNumbers, n);
        if (k > max)
          max = k;
      }
      return max;
    }

    if (this.isAnd()) {
      for (UCompletionTree subCompletionTree : this.getSubTrees()) {
        n = subCompletionTree.getCompletionNumbers(completionNumbers, n);
      }
      return n;
    }

    return n;
  }

  /**
   * Determine the shared number of a completion state.
   * 
   * @param completionState a (UML) state which can be completed
   * @return the shared number of the completion state, or -1 if the state has
   * no number assigned in this completion tree
   */
  public int getCompletionNumber(UState completionState) {
    Integer completionNumber = this.getCompletionNumbers().get(completionState);
    if (completionNumber != null)
      return completionNumber.intValue();
    return -1;
  }

  /**
   * Determine maximal completion number increased by one.
   * 
   * @return maximal completion number increased by one
   */
  public int getMaxCompletionsNumber() {
    Collection<Integer> completionNumbers = this.getCompletionNumbers().values();
    if (completionNumbers.isEmpty())
      return 0;
    @Nullable Integer max = Collections.max(completionNumbers);
    return max+1;
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("CompletionTree [andOrTree = ");
    resultBuilder.append(this.andOrTree);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
