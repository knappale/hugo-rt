package uml.statemachine.semantics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.statemachine.UEvent;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import util.AndOrTree;


/**
 * Timer tree, representing the set of timers active in a state tree.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UTimerTree {
  private static Map<AndOrTree<Set<UEvent>>, UTimerTree> timerTrees = new HashMap<>();

  private AndOrTree<Set<UEvent>> andOrTree;

  private @Nullable List<UEvent> cachedTimeEvents = null;
  private @Nullable Set<Set<UEvent>> cachedTimerSets = null;
  private @Nullable Map<UEvent, Integer> cachedTimerNumbers = null;

  private UTimerTree(AndOrTree<Set<UEvent>> andOrTree) {
    this.andOrTree = andOrTree;
  }

  private static UTimerTree create(AndOrTree<Set<UEvent>> andOrTree) {
    UTimerTree timerTree = timerTrees.get(andOrTree);
    if (timerTree != null)
      return timerTree;

    timerTree = new UTimerTree(andOrTree);
    timerTrees.put(andOrTree, timerTree);
    return timerTree;
  }

  private static UTimerTree leaf(Set<UEvent> timeEvents) {
    return create(AndOrTree.leaf(timeEvents));
  }

  private static UTimerTree leaf() {
    return UTimerTree.leaf(new HashSet<>());
  }

  private static UTimerTree and(Set<UEvent> timeEvents, List<UTimerTree> subTimerTrees) {
    List<AndOrTree<Set<UEvent>>> andOrTrees = new ArrayList<>();
    for (UTimerTree subTimerTree : subTimerTrees)
      andOrTrees.add(subTimerTree.andOrTree);
    return create(AndOrTree.and(timeEvents, andOrTrees));
  }

  private static UTimerTree and(List<UTimerTree> subTimerTrees) {
    return UTimerTree.and(new HashSet<>(), subTimerTrees);
  }

  private static UTimerTree or(Set<UEvent> timeEvents, List<UTimerTree> subTimerTrees) {
    List<AndOrTree<Set<UEvent>>> andOrTrees = new ArrayList<>();
    for (UTimerTree subTimerTree : subTimerTrees)
      andOrTrees.add(subTimerTree.andOrTree);
    return create(AndOrTree.or(timeEvents, andOrTrees));
  }

  private static UTimerTree or(List<UTimerTree> subTimerTrees) {
    return UTimerTree.or(new HashSet<>(), subTimerTrees);
  }

  /**
   * @return state machine's timer tree
   */
  public static UTimerTree create(UStateMachine uStateMachine) {
    return UTimerTree.and(uStateMachine.getRegions().stream().map(uRegion -> UTimerTree.or(create(uRegion))).collect(Collectors.toList()));
  }

  /**
   * @return vertex's timer tree
   */
  private static UTimerTree create(UVertex uVertex) {
    return uVertex.new Cases<UTimerTree>().
      pseudoState(uPseudoState -> UTimerTree.leaf()).
      finalState(uFinalState -> UTimerTree.leaf()).
      state(uState -> create(uState)).
      apply();
  }

  /**
   * @return state's timer tree
   */
  private static UTimerTree create(UState uState) {
    Set<UEvent> timeEvents = uState.getOutgoingTimes().stream().map(outgoingTime -> outgoingTime.getTrigger()).collect(Collectors.toSet());

    if (uState.getRegions().size() == 0)
      return UTimerTree.leaf(timeEvents);

    if (uState.getRegions().size() == 1)
      return UTimerTree.or(timeEvents, create(uState.getRegions().get(0)));

    return UTimerTree.and(timeEvents, uState.getRegions().stream().map(uRegion -> UTimerTree.or(create(uRegion))).collect(Collectors.toList()));
  }

  /**
   * @return region's timer trees
   */
  private static List<UTimerTree> create(URegion uRegion) {
    return uRegion.getVertices().stream().map(uVertex -> create(uVertex)).filter(timerTree -> !timerTree.isEmpty()).collect(Collectors.toList());
  }

  /**
   * @return timer tree's label
   */
  private Set<UEvent> getLabel() {
    return this.andOrTree.getLabel();
  }

  /**
   * @return timer tree's sub-trees
   */
  private List<UTimerTree> getSubTrees() {
    return this.andOrTree.getSubTrees().stream().map(subAndOrTree -> create(subAndOrTree)).collect(Collectors.toList());
  }

  /**
   * @return whether this timer tree is empty
   */
  private boolean isEmpty() {
    return (this.getLabel().isEmpty() && this.getSubTrees().isEmpty());
  }

  /**
   * @return whether this timer tree is a leaf
   */
  private boolean isLeaf() {
    return this.andOrTree.isLeaf();
  }

  /**
   * @return whether this timer tree is an and-timer tree
   */
  private boolean isAnd() {
    return this.andOrTree.isAnd();
  }

  /**
   * @return whether this timer tree is an or-timer tree
   */
  private boolean isOr() {
    return this.andOrTree.isOr();
  }

  /**
   * Determine all time events occurring in this timer tree.
   * 
   * @return list of time events
   */
  public List<UEvent> getTimeEvents() {
    List<UEvent> timeEvents = this.cachedTimeEvents;
    if (timeEvents != null)
      return timeEvents;

    timeEvents = new ArrayList<UEvent>();
    timeEvents.addAll(this.getLabel());
    if (this.isOr() || this.isAnd()) {
      for (UTimerTree subTimerTree : getSubTrees()) {
        timeEvents.addAll(subTimerTree.getTimeEvents());
      }
    }
    this.cachedTimeEvents = timeEvents;
    return timeEvents;
  }

  /**
   * The returned set of sets of time events is subset-closed and
   * non-empty for every non-empty and-or-tree.
   */
  public Set<Set<UEvent>> getTimerSets() {
    Set<Set<UEvent>> timerSets = this.cachedTimerSets;
    if (timerSets != null)
      return timerSets;

    timerSets = new HashSet<>();

    if (this.isLeaf()) {
      timerSets.addAll(util.Sets.powerSet(this.getLabel()));
      this.cachedTimerSets = timerSets;
      return timerSets;
    }

    if (this.isOr()) {
      for (UTimerTree subTimerTree : getSubTrees()) {
        for (Set<UEvent> subTimerSet : subTimerTree.getTimerSets()) {
          Set<UEvent> timerSet = new HashSet<>();
          timerSet.addAll(subTimerSet);
          timerSet.addAll(this.getLabel());
          timerSets.add(timerSet);
        }
      }
      timerSets.addAll(util.Sets.powerSet(this.getLabel()));
      this.cachedTimerSets = timerSets;
      return timerSets;
    }

    if (this.isAnd()) {
      Set<Set<Set<UEvent>>> subTimerSetsSet = new HashSet<>();
      for (UTimerTree subTimerTree : getSubTrees()) {
        Set<Set<UEvent>> subTimerSets = subTimerTree.getTimerSets();
        subTimerSetsSet.add(subTimerSets);
      }
      for (Set<UEvent> unionProductSet : util.Sets.unionProduct(subTimerSetsSet)) {
        Set<Set<UEvent>> unionProductSetPowerSet = util.Sets.powerSet(unionProductSet);
        for (Set<UEvent> timerSet : unionProductSetPowerSet) {
          timerSet.addAll(this.getLabel());
          timerSets.add(timerSet);
        }
      }
      timerSets.addAll(util.Sets.powerSet(this.getLabel()));
      this.cachedTimerSets = timerSets;
      return timerSets;
    }

    this.cachedTimerSets = timerSets;
    return timerSets;
  }

  /**
   * Assign a number to each time event such that time events active at
   * different times may share the same number.
   * 
   * @return mapping time event to a number
   */
  private Map<UEvent, Integer> getTimerNumbers() {
    Map<UEvent, Integer> timerNumbers = this.cachedTimerNumbers;
    if (timerNumbers != null)
      return timerNumbers;

    timerNumbers = new HashMap<>();
    this.getTimerNumbers(timerNumbers, 0);
    this.cachedTimerNumbers = timerNumbers;
    return timerNumbers;
  }

  /**
   * Assign a number to each time event such that time events active at
   * different times may share the same number.
   * 
   * @return maximal timer number used in this timer tree
   */
  private int getTimerNumbers(Map<UEvent, Integer> timerNumbers, int n) {
    for (UEvent timeEvent : this.getLabel()) {
      if (timerNumbers.get(timeEvent) == null)
        timerNumbers.put(timeEvent, n++);
    }

    if (this.isLeaf())
      return n;

    if (this.isOr()) {
      int max = n;
      for (UTimerTree subTimerTree : this.getSubTrees()) {
        int k = subTimerTree.getTimerNumbers(timerNumbers, n);
        if (k > max)
          max = k;
      }
      return max;
    }

    if (this.isAnd()) {
      for (UTimerTree subTimerTree : this.getSubTrees()) {
        n = subTimerTree.getTimerNumbers(timerNumbers, n);
      }
      return n;
    }

    return 0;
  }

  /**
   * Determine the number of a timer for a time event.
   * 
   * @param timeEvent a (UML time) event
   * @return the number of the timer for <CODE>timeEvent</CODE>, or -1
   * if <CODE>timeEvent</CODE> has no number assigned in this timer tree
   * @pre timeEvent.isTime()
   */
  public int getTimerNumber(UEvent timeEvent) {
    Integer timerNumber = this.getTimerNumbers().get(timeEvent);
    if (timerNumber != null)
      return timerNumber.intValue();
    return -1;
  }
  
  /**
   * @return time event numbers used in this timer tree
   */
  public Set<Integer> getTimerNumbersSet() {
    return new HashSet<>(this.getTimerNumbers().values());
  }

  /**
   * Determine the number of simultaneously active timers
   * 
   * -AK This should be equal to the size of the sets with maximal
   * cardinality in timerSets
   * 
   * @return number of maximally simultaneously active timers
   */
  public int getMaxTimersNumber() {
    Collection<Integer> timerNumbers = this.getTimerNumbers().values();
    if (timerNumbers.isEmpty())
      return 0;
    @Nullable Integer max = Collections.max(this.getTimerNumbers().values());
    return max+1;
  }
}
