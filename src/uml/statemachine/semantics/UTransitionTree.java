package uml.statemachine.semantics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAction;
import uml.UContext;
import uml.UExpression;
import uml.statemachine.UEvent;
import uml.statemachine.UPseudoState;
import uml.statemachine.UTransition;
import uml.statemachine.UVertex;
import util.Formatter;


/**
 * Contains data of a (UML) transition tree.
 *
 * Transition trees combine chains of transitions through junction
 * (pseudo-)states and branches through fork (pseudo-)state or into
 * (concurrent) composite states by default entries (the transitions
 * from the contained initial (pseudo-)state).
 *
 * @see uml.statemachine.semantics.UCompoundTransition
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UTransitionTree {
  private UTransition transition;
  private List<UTransitionTree> branches = new ArrayList<>();

  /**
   * Create a transition tree consisting only of a transition.
   *
   * @param transition a (UML) transition
   */
  public UTransitionTree(UTransition transition) {
    this.transition = transition;
  }

  /**
   * Create a transition tree consisting of a transition and a single branch.
   *
   * @param transition a (UML) transition
   * @param branch a transition tree
   */
  public UTransitionTree(UTransition transition, UTransitionTree branch) {
    this.transition = transition;
    this.branches.add(branch);
  }

  /**
   * Create a transition tree consisting of a transition and a
   * list of branches.
   *
   * @param transition a (UML) transition
   * @param branches a list of transition trees
   */
  public UTransitionTree(UTransition transition, List<UTransitionTree> branches) {
    this.transition = transition;
    this.branches = branches;
  }

  /**
   * A transition tree to a join (pseudo-)state can only consist of a
   * transition, since every transition targetting a join
   * (pseudo-)state must start in a proper state (see UML
   * specification 1.4, pp. 2-160, constraint [4]: ``A join segment
   * should always originate from a state.'').
   *
   * @return whether transition tree targets a join (pseudo-)state
   */
  public boolean hasJoinTarget() {
    return (this.branches.size() == 0 && this.transition.getTarget().getKind() == UPseudoState.Kind.JOIN);
  }

  /**
   * @return transition tree's source vertex
   */
  public UVertex getSource() {
    return this.transition.getSource();
  }

  /**
   * @return transition tree's target vertices
   */
  public Set<UVertex> getTargets() {
    Set<UVertex> targets = new HashSet<>();
    if (this.branches.isEmpty()) {
      targets.add(this.transition.getTarget());
      return targets;
    }
    for (UTransitionTree branch : this.branches)
      targets.addAll(branch.getTargets());
    return targets;
  }

  /**
   * @return transition tree's trigger
   */
  UEvent getTrigger() {
    return this.transition.getTrigger();
  }

  /**
   * @return transition tree's trigger context
   */
  UContext getTriggerContext() {
    return this.transition.getTriggerContext();
  }

  /**
   * @return transition tree's compound guard
   */
  UExpression getGuard() {
    UExpression guard = this.transition.getGuard();
    for (UTransitionTree branch : this.branches)
      guard = UExpression.and(guard, branch.getGuard());
    return guard;
  }

  /**
   * @return transition tree's trigger context
   */
  UContext getGuardContext() {
    return this.transition.getGuardContext();
  }

  /**
   * @return transition tree's compound effect
   */
  UAction getEffect() {
    UAction effect = this.transition.getEffect();
    UAction branchesEffect = UAction.skip();
    for (UTransitionTree branch : this.branches) {
      branchesEffect = UAction.par(branchesEffect, branch.getEffect());
    }
    return UAction.seq(effect, branchesEffect);
  }

  /**
   * @return transition tree's effect context
   */
  UContext getEffectContext() {
    return this.transition.getEffectContext();
  }

  /**
   * @return transition tree's hash code
   */
  public int hashCode() {
    return Objects.hash(this.transition,
                        this.branches);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UTransitionTree other = (UTransitionTree)object;
      return Objects.equals(this.transition, other.transition) &&
             Objects.equals(this.branches, other.branches);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("TransitionTree [transition = ");
    resultBuilder.append(this.transition);
    resultBuilder.append(", branches = ");
    resultBuilder.append(Formatter.list(this.branches));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
