package uml.statemachine.semantics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.statemachine.UEvent;
import uml.statemachine.UPseudoState;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import util.Formatter;

import static java.util.stream.Collectors.toSet;


/**
 * State machine vertex forest.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 *
 */
@NonNullByDefault
public class UVertexForest {
  private List<UVertexTree> trees;

  UVertexForest() {
    this.trees = new ArrayList<>();
  }

  UVertexForest(UVertexTree tree) {
    this();
    this.trees.add(tree);
  }

  public UVertexForest(List<UVertexTree> trees) {
    this();
    this.trees.addAll(trees);
  }

  /**
   * @return whether this vertex forest is empty
   */
  public boolean isEmpty() {
    return this.trees.isEmpty();
  }

  /**
   * @return set of vertices contained in this vertex forest
   */
  Set<UVertex> getVertices() {
    return this.trees.stream().flatMap(tree -> tree.getVertices().stream()).collect(toSet());
  }

  /**
   * @return whether this vertex forest contains a given vertex
   */
  boolean contains(UVertex vertex) {
    return this.trees.stream().anyMatch(tree -> tree.contains(vertex));
  }

  /**
   * @return whether this vertex forest affects a given region
   */
  boolean affects(URegion region) {
    return this.trees.stream().anyMatch(tree -> tree.affects(region));
  }

  /**
   * @return whether this vertex forest contains a pseudo-state
   */
  public boolean isPseudo() {
    return this.trees.stream().anyMatch(tree -> tree.isPseudo());
  }

  /**
   * @return whether this vertex forest is completed at the given states
   */
  public boolean isCompleted(Set<UState> states) {
    return this.trees.stream().allMatch(tree -> tree.isCompleted(states));
  }

  /**
   * @return whether this vertex forest is timed out for the given time event 
   */
  public boolean isTimedOut(UEvent timeEvent) {
    return this.trees.stream().anyMatch(tree -> tree.isTimedOut(timeEvent));
  }

  /**
   * @return whether this vertex forest forms a configuration
   */
  public boolean isConfiguration() {
    if (this.trees.isEmpty())
      return false;
    UStateMachine stateMachine = this.trees.get(0).getTop().getStateMachine();
    return stateMachine.getRegions().stream().map(region ->
               this.trees.stream().filter(tree -> tree.getTop().getContainer().equals(region)).collect(toSet())).allMatch(trees ->
                   trees.size() == 1 && trees.iterator().next().isConfiguration());
  }

  /**
   * @return whether this vertex forest forms an initial configuration
   */
  public boolean isInitial() {
    if (this.trees.isEmpty())
      return false;
    UStateMachine stateMachine = this.trees.get(0).getTop().getStateMachine();
    return stateMachine.getRegions().stream().allMatch(region ->
             this.trees.stream().filter(tree -> tree.getTop().getContainer().equals(region)).count() == 1) &&
           this.trees.stream().allMatch(tree -> tree.getTop().getKind() == UPseudoState.Kind.INITIAL);
  }

  /**
   * Determine whether this vertex forest forms a configuration for a given state.
   *
   * @param state a state
   * @return whether this vertex forest forms a configuration of {@code state}
   */
  boolean isConfiguration(UState state) {
    return state.getRegions().stream().map(region ->
               this.trees.stream().filter(tree -> tree.getTop().getContainer().equals(region)).collect(toSet())).allMatch(trees ->
                   trees.size() == 1 && trees.iterator().next().isConfiguration());
  }

  /**
   * @return this vertex forest's name
   */
  public String getName() {
    return Formatter.separated(this.trees, tree -> tree.getName(), "_");
  }

  /**
   * @return this vertex forest's vertex tree below (and including) the
   * given state or {@code null} if this vertex forest does not
   * contain the given state
   */
  public @Nullable UVertexTree getVertexTree(UState state) {
    return this.trees.stream().map(tree -> tree.getVertexTree(state)).filter(tree -> tree != null).findAny().orElse(null);
  }

  /**
   * Determine the vertex forest resulting from this vertex forest when
   * substituting another vertex forest.
   *
   * All annotations above the given vertex forest in this vertex forest are
   * kept.  The annotations of the substituted vertex forest are not
   * altered.
   *
   * @param other another vertex forest
   * @return the vertex forest resulting from substituting {@code other} in this vertex forest
   */
  public UVertexForest getSubstituted(UVertexForest other) {
    List<UVertexTree> substitutedTrees = new ArrayList<>();
    for (UVertexTree tree : this.trees) {
      UVertexTree substitutedTree = tree;
      for (int i = 0; i < other.trees.size(); i++) {
        if (tree.affects(other.trees.get(i).getTop().getContainer())) {
          substitutedTree = tree.getSubstituted(other.trees.get(i));
          break;
        }
      }
      substitutedTrees.add(substitutedTree);
    }

    return new UVertexForest(substitutedTrees);
  }

  /**
   * Determine the vertex forest resulting from this vertex forest when
   * substituting a vertex tree.
   *
   * All annotations above the given vertex tree in this vertex forest are
   * kept.  The annotations of the substituted vertex tree are not
   * altered.
   *
   * @param vertexTree a vertex tree
   * @return the vertex forest that results from substituting {@code vertexTree}
   *         in this vertex forest
   */
  public UVertexForest getSubstituted(UVertexTree vertexTree) {
    List<UVertexTree> substitutedTrees = new ArrayList<>();
    for (UVertexTree tree : this.trees) {
      UVertexTree substitutedTree = tree;
      if (tree.affects(vertexTree.getTop().getContainer()))
        substitutedTree = tree.getSubstituted(vertexTree);
      substitutedTrees.add(substitutedTree);
    }

    return new UVertexForest(substitutedTrees);
  }

  /**
   * @return set of compound transitions outgoing from this vertex forest
   * @see UVertexTree#getOutgoingCompounds()
   */
  Set<UCompoundTransition> getOutgoingCompounds() {
    Set<UCompoundTransition> vertexOutgoingCompounds = new HashSet<>();
    for (UVertex vertex : this.getVertices())
      vertexOutgoingCompounds.addAll(vertex.getOutgoingCompounds());

    Set<UCompoundTransition> outgoingCompounds = new HashSet<>();
    for (UCompoundTransition vertexOutgoingCompound : vertexOutgoingCompounds) {
      UEvent vertexOutgoingCompoundTrigger = vertexOutgoingCompound.getTrigger();
      if (vertexOutgoingCompoundTrigger.isTime()) {
	if (this.isTimedOut(vertexOutgoingCompoundTrigger))
	  outgoingCompounds.add(vertexOutgoingCompound);
      }
      else {
	if (vertexOutgoingCompoundTrigger.isCompletion()) {
	  if (this.isCompleted(vertexOutgoingCompoundTrigger.getStates()))
	    outgoingCompounds.add(vertexOutgoingCompound);
	}
	else
	  outgoingCompounds.add(vertexOutgoingCompound);
      }
    }
    return outgoingCompounds;
  }

  /**
   * @return completion events that are open in this vertex forest
   * @see UVertexTree#getCompletables()
   */
  Set<UEvent> getCompletables() {
    return this.trees.stream().flatMap(tree -> tree.getCompletables().stream()).collect(toSet());
  }

  /**
   * @return time events that are open in this vertex forest
   * @see UVertexTree#getTimeOutables()
   */
  Set<UEvent> getTimeOutables() {
    return this.trees.stream().flatMap(tree -> tree.getTimeOutables().stream()).collect(toSet());
  }

  /**
   * @return deferred events that are open in this vertex forest
   * @see UVertexTree#getDeferrables()
   */
  Set<UEvent> getDeferrables() {
    return this.trees.stream().flatMap(tree -> tree.getDeferrables().stream()).collect(toSet());
  }

  /**
   * @return whether this state forest contains another state forest
   * disregarding timed-out annotations
   */
  boolean matchesTimeless(@Nullable UVertexForest other) {
    if (other == null)
      return false;

    Iterator<UVertexTree> thisTreesIt = this.trees.iterator();
    Iterator<UVertexTree> otherTreesIt = other.trees.iterator();
    while (thisTreesIt.hasNext() && otherTreesIt.hasNext()) {
      UVertexTree thisSon = thisTreesIt.next();
      UVertexTree otherSon = otherTreesIt.next();
      if (!thisSon.matchesTimeless(otherSon))
	return false;
    }
    return (!thisTreesIt.hasNext() && !otherTreesIt.hasNext());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.trees);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UVertexForest other = (UVertexForest)object;
      return Objects.equals(this.trees, other.trees);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(Formatter.list(this.trees));
    return resultBuilder.toString();
  }
}
