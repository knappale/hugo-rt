package uml.statemachine.semantics;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.statemachine.UEvent;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UTransition;
import uml.statemachine.UVertex;
import util.Formatter;

import static java.util.stream.Collectors.toSet;


/**
 * State machine vertex tree.
 *
 * Any vertex tree consists of a top vertex.
 * 
 * If the top vertex is a state proper, some son vertex trees
 * and annotations for the top state can be present.  The vertex
 * tree can be marked "completed" meaning that a completion
 * event for the top state has been handled.  The vertex tree
 * can also be marked "timed-out" for a set of time events, meaning
 * that this set of time events has been handled for the top state,
 * i.e., corresponding timers have been started.
 *
 * This is meant to be a pure datatype.  No changes to the underlying
 * vertices or events are recognised.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UVertexTree {
  private UVertex vertex;
  private boolean isCompleted = false;
  private Set<UEvent> timedOuts = new HashSet<>();
  private UVertexForest forest;

  private UVertexTree(UVertex vertex, boolean isCompleted, Set<UEvent> timedOuts, UVertexForest forest) {
    this.vertex = vertex;
    this.isCompleted = isCompleted;
    this.timedOuts.addAll(timedOuts);
    this.forest = forest;
  }

  /**
   * Create a leaf vertex tree from a single vertex.
   *
   * @param vertex a vertex
   * @return a simple vertex tree
   */
  public static UVertexTree simple(UVertex vertex) {
    return new UVertexTree(vertex, false, new HashSet<>(), new UVertexForest());
  }

  /**
   * Create a hierarchical vertex tree from a state and a vertex forest.
   *
   * @param state a state
   * @param forest a vertex forest
   * @return a hierarchical vertex tree with the top state put above the vertex forest
   */
  public static UVertexTree hierarchical(UState state, UVertexForest forest) {
    return new UVertexTree(state, false, new HashSet<>(), forest);
  }

  /**
   * Create a hierarchical vertex tree from a state and a single vertex tree
   *
   * @param state a state
   * @param tree a vertex tree
   * @return a hierarchical vertex tree with the top state put above the vertex tree
   */
  public static UVertexTree hierarchical(UState state, UVertexTree tree) {
    return hierarchical(state, new UVertexForest(tree));
  }

  /**
   * Create a hierarchical vertex tree from a state and a list of vertex trees.
   *
   * @param state a state
   * @param trees a list of vertex trees
   * @return a hierarchical vertex tree with the top state put above the vertex trees
   */
  public static UVertexTree hierarchical(UState state, List<UVertexTree> trees) {
    return hierarchical(state, new UVertexForest(trees));
  }

  /**
   * Create a vertex tree with the underlying vertex tree marked as
   * completed at the top.
   *
   * @param tree a vertex tree
   * @return a vertex tree with the underlying vertex tree marked as completed at the top
   */
  public static UVertexTree completed(UVertexTree tree) {
    return new UVertexTree(tree.vertex, true, tree.timedOuts, tree.forest);
  }

  /**
   * Create a vertex tree with the underlying vertex tree marked as
   * timed out at the top for the set of given time events.
   *
   * @param tree a vertex tree
   * @return a vertex tree with the underlying vertex tree marked as
   * timed out at the top for the given set of time events
   */
  public static UVertexTree timedOut(Set<UEvent> timeEvents, UVertexTree tree) {
    Set<UEvent> topTimedOuts = new HashSet<>(tree.timedOuts);
    topTimedOuts.addAll(timeEvents);
    return new UVertexTree(tree.vertex, tree.isCompleted, topTimedOuts, tree.forest);
  }

  /**
   * @return this vertex tree's top vertex
   */
  UVertex getTop() {
    return this.vertex;
  }

  /**
   * @return set of vertices contained in this vertex tree
   */
  Set<UVertex> getVertices() {
    Set<UVertex> vertices = new HashSet<>();
    vertices.add(this.vertex);
    vertices.addAll(this.forest.getVertices());
    return vertices;
  }

  /**
   * @return whether this vertex tree contains the given vertex
   */
  public boolean contains(UVertex vertex) {
    return (this.vertex.equals(vertex) || this.forest.contains(vertex));
  }

  /**
   * @return whether this vertex tree affects a given region
   */
  boolean affects(URegion region) {
    if (this.vertex.getContainer().equals(region))
      return true;
    return this.forest.affects(region);
  }

  /**
   * @return whether this vertex tree is completed at its top
   */
  public boolean isCompleted() {
    return this.isCompleted;
  }

  /**
   * @return whether this vertex tree is completed at the given states
   */
  public boolean isCompleted(Set<UState> states) {
    return (!states.contains(this.vertex) || this.isCompleted) &&
           this.forest.isCompleted(states);
  }

  /**
   * @return set of time events for which this vertex tree is timed out at the top state
   */
  public Set<UEvent> getTopTimedOuts() {
    return this.timedOuts;
  }

  /**
   * @return whether this vertex tree is timed out for the given time event at some contained state
   */
  public boolean isTimedOut(UEvent timeEvent) {
    if (this.vertex.equals(timeEvent.getState()))
      return this.timedOuts.contains(timeEvent);
    return this.forest.isTimedOut(timeEvent);
  }

  /**
   * @return whether this vertex tree contains a pseudo-state
   */
  public boolean isPseudo() {
    return (this.vertex.getKind() != null) || this.forest.isPseudo();
  }

  /**
   * @return whether this vertex tree forms a configuration for its top vertex
   */
  public boolean isConfiguration() {
    if (this.vertex instanceof UState)
      return this.forest.isConfiguration((UState)this.vertex);
    return true;
  }

  /**
   * @return this vertex tree's name
   */
  public String getName() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.vertex.getName());
    if (this.isCompleted)
      resultBuilder.append("_c");
    if (!this.timedOuts.isEmpty()) {
      resultBuilder.append("_");
      for (UTransition topOutgoingTimeTransition : this.vertex.getOutgoings()) {
        if (this.timedOuts.contains(topOutgoingTimeTransition.getTrigger()))
          resultBuilder.append("t");
        else
          resultBuilder.append("r");
      }
    }
    if (!this.forest.isEmpty()) {
      resultBuilder.append("_");
      resultBuilder.append(this.forest.getName());
    }
    return resultBuilder.toString();
  }

  /**
   * Determine vertex tree's sub-vertex tree below (and including) the
   * given state or {@code null} if this vertex tree does not
   * contain the given state.
   * 
   * @param state a state
   * @return this vertex tree's sub-vertex tree below {@code state}
   */
  public @Nullable UVertexTree getVertexTree(UState state) {
    if (this.vertex.equals(state))
      return this;
    return this.forest.getVertexTree(state);
  }

  /**
   * Determine the vertex tree resulting from this vertex tree when
   * substituting another vertex tree.
   *
   * All annotations above the given vertex tree in this vertex tree are
   * kept.  The annotations of the substituted vertex tree are not
   * altered.
   *
   * @param other another vertex tree
   * @return the vertex tree resulting from substituting {@code other} in this vertex tree
   */
  public UVertexTree getSubstituted(UVertexTree other) {
    if (this.vertex.getContainer().equals(other.vertex.getContainer()))
      return other;

    UVertexTree substituted = (this.vertex instanceof UState) ? UVertexTree.hierarchical((UState)this.vertex, this.forest.getSubstituted(other))
                                                              : UVertexTree.simple(this.vertex);
    if (this.isCompleted())
      substituted = UVertexTree.completed(substituted);
    if (!this.getTopTimedOuts().isEmpty())
      substituted = UVertexTree.timedOut(this.getTopTimedOuts(), substituted);
    return substituted;
  }

  /**
   * Determine the set of compound transitions outgoing from this
   * vertex tree.
   *
   * The set of outgoing compound transitions depends on the
   * annotations for this vertex tree: A compound transition with a
   * time event trigger leaving some state is only included if the
   * vertex tree is marked as timed out for this state.  A compound
   * transition with a completion event trigger leaving some state is
   * only included if the vertex tree is marked as completed for this
   * state.
   *
   * @return set of outgoing compound transitions
   */
  Set<UCompoundTransition> getOutgoingCompounds() {
    Set<UCompoundTransition> vertexOutgoingCompounds = new HashSet<>();
    for (UVertex vertex : this.getVertices())
      vertexOutgoingCompounds.addAll(vertex.getOutgoingCompounds());

    Set<UCompoundTransition> outgoingCompounds = new HashSet<>();
    for (UCompoundTransition vertexOutgoingCompound : vertexOutgoingCompounds) {
      UEvent vertexOutgoingCompoundTrigger = vertexOutgoingCompound.getTrigger();
      if (vertexOutgoingCompoundTrigger.isTime()) {
	if (this.isTimedOut(vertexOutgoingCompoundTrigger))
	  outgoingCompounds.add(vertexOutgoingCompound);
      }
      else {
	if (vertexOutgoingCompoundTrigger.isCompletion()) {
	  if (this.isCompleted(vertexOutgoingCompoundTrigger.getStates()))
	    outgoingCompounds.add(vertexOutgoingCompound);
	}
	else
	  outgoingCompounds.add(vertexOutgoingCompound);
      }
    }
    return outgoingCompounds;
  }

  /**
   * Determine the set of completion events that are open in this
   * vertex tree.
   *
   * A completion event is called open in a vertex tree, if the state
   * corresponding to the completion event's state is not completed,
   * but shows an outgoing completion transition and the vertex tree
   * below this vertex tree is final.
   *
   * @return completion events that are open in this vertex tree
   */
  public Set<UEvent> getCompletables() {
    Set<UEvent> completables = new HashSet<>();
    if (!this.isCompleted() && this.vertex.isCompletable() &&
	UVertexTree.completed(this).matchesTimeless(this.vertex.getFinalVertexTree())) {
      for (UTransition topOutgoingCompletion : this.vertex.getOutgoings().stream().filter(outgoing -> outgoing.getTrigger().isCompletion()).collect(toSet())) {
        if (topOutgoingCompletion.getTrigger().getStates().contains(this.vertex))
	  completables.add(UEvent.completion((UState)this.vertex));
      }
    }
    completables.addAll(this.forest.getCompletables());
    return completables;
  }

  /**
   * @return whether this vertex tree contains another vertex tree
   *         disregarding timed-out annotations
   */
  boolean matchesTimeless(@Nullable UVertexTree other) {
    if (other == null)
      return false;

    if (!this.vertex.equals(other.vertex) ||
	this.isCompleted != other.isCompleted)
      return false;

    return this.forest.matchesTimeless(other.forest);
  }

  /**
   * Determine the set of time events that are open in this vertex tree.
   *
   * A time event is called open in a vertex tree if the time event
   * can be handled in this vertex tree, but no corresponding timer has
   * been started.
   *
   * @return time events that are open in this vertex tree
   */
  Set<UEvent> getTimeOutables() {
    Set<UEvent> timeOutables = new HashSet<>();
    for (UTransition topOutgoingTime : this.vertex.getOutgoings().stream().filter(outgoing -> outgoing.getTrigger().isTime()).collect(toSet())) {
      if (!this.getTopTimedOuts().contains(topOutgoingTime.getTrigger()))
	timeOutables.add(topOutgoingTime.getTrigger());
    }
    timeOutables.addAll(this.forest.getTimeOutables());
    return timeOutables;
  }

  /**
   * Determine the set of deferred events that are open in this vertex tree.
   *
   * A deferred event is called open in a state tree if the vertex tree
   * contains a state showing this deferred event.
   *
   * @return deferred events that are open in this vertex tree
   */
  Set<UEvent> getDeferrables() {
    Set<UEvent> deferrables = new HashSet<>();
    if (this.vertex instanceof UState)
      deferrables.addAll(((UState)this.vertex).getDeferrableEvents());
    deferrables.addAll(this.forest.getDeferrables());
    return deferrables;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.vertex);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UVertexTree other = (UVertexTree)object;
      return (Objects.equals(this.vertex, other.vertex) &&
  	      this.isCompleted == other.isCompleted &&
  	      Objects.equals(this.timedOuts, other.timedOuts) &&
	      Objects.equals(this.forest, other.forest));
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.vertex.getName());
    if (this.isCompleted)
      resultBuilder.append("<completed>");
    if (!this.timedOuts.isEmpty())
      resultBuilder.append(Formatter.set(this.timedOuts, timedOut -> timedOut.getName()));
    if (!this.forest.isEmpty())
      resultBuilder.append(this.forest);
    return resultBuilder.toString();
  }
}
