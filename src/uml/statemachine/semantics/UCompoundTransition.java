package uml.statemachine.semantics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UAction;
import uml.UContext;
import uml.UExpression;
import uml.statemachine.UEvent;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UTransition;
import uml.statemachine.UVertex;
import util.Formatter;

import static java.util.stream.Collectors.toSet;
import static util.Objects.requireNonNull;
import static util.Objects.toType;


/**
 * UML compound transition
 *
 * @author <A HREF="mailto:majnu@gmx.net">Timm Schäfer</A>
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UCompoundTransition {
  private List<UTransition> joinTransitions = new ArrayList<>();
  private UTransitionTree transitionTree;
  private UExpression priorityGuard = UExpression.trueConst();

  /**
   * Create a compound transition consisting of an optional initial
   * join part, and a transition tree.
   *
   * The transition tree either starts in a join (pseudo-)state such
   * that there must be an initial join part; or it starts in a
   * simple, composite, initial, choice, or history (pseudo-)state.
   * The transition tree targets simple, composite, choice, or history
   * (pseudo-)states.  If there is an initial join part, all source
   * states are simple or composite (see UML specification 1.4,
   * pp. 2-159f., constraint [3]: ``A fork segment should always
   * target a state.'', and constraint [4]: ``A join segment should
   * always originate from a state.'').
   *
   * @param joinTransitions list of transitions incoming into a join (pseudo-)state
   * @param transitionTree transition tree through junction and fork (pseudo-)states
   */
  public UCompoundTransition(List<UTransition> joinTransitions, UTransitionTree transitionTree) {
    this.joinTransitions = joinTransitions;
    this.transitionTree = transitionTree;
  }

  /**
   * Create a new compound transition with an additional priority guard.
   *
   * Priority guards record under which additional condition a
   * compound transition is enabled.
   *
   * @param priorityGuard new priority guard
   * @param compoundTransition compound transition
   */
  public UCompoundTransition(UExpression priorityGuard, UCompoundTransition compoundTransition) {
    this.joinTransitions = compoundTransition.joinTransitions;
    this.transitionTree = compoundTransition.transitionTree;
    this.priorityGuard = UExpression.and(compoundTransition.priorityGuard, priorityGuard);
  }

  /**
   * Determine whether there is an initial join part.
   *
   * @return whether compound transition shows an initial join part
   */
  public boolean isJoin() {
    return (this.joinTransitions.size() > 0);
  }

  /**
   * Determine compound transition's explicit source states.
   *
   * There may be several source states if there is an initial join
   * part.  If there is no initial join part, the source is the source
   * of the transition tree.
   *
   * @return compound transition's source states
   */
  public Set<UVertex> getSources() {
    Set<UVertex> sources = new HashSet<>();
    if (!this.joinTransitions.isEmpty()) {
      for (UTransition joinTransition : this.joinTransitions)
        sources.add(joinTransition.getSource());
      return sources;
    }
    sources.add(this.transitionTree.getSource());
    return sources;
  }

  /**
   * Determine compound transition's final source states.
   *
   * If the compound transition is a completion transition, not only all
   * source states but also their final states are included in the
   * final source states.
   *
   * @return compound transition's final source states
   */
  public Set<UVertex> getFinalSources() {
    Set<UVertex> sources = this.getSources();
    if (!this.getTrigger().isCompletion())
      return sources;

    Set<UVertex> finalSources = new HashSet<>();
    finalSources.addAll(sources);
    for (UVertex source : sources) {
      UVertexTree finalStateTree = source.getFinalVertexTree();
      if (finalStateTree != null)
        finalSources.addAll(finalStateTree.getVertices());
    }
    return finalSources;
  }

  /**
   * Determine compound transition's explicit target states.
   *
   * The target states are the target states of the transition tree,
   * which may have several targets if it contains a fork.
   *
   * @return compound transition's target states
   */
  public Set<UVertex> getTargets() {
    return this.transitionTree.getTargets();
  }

  /**
   * Determine compound transition's compound trigger.
   *
   * The compound trigger is either a completion event for all source
   * states, if the compound transition has a join part; or the
   * trigger of the transition tree (UML specification 1.4,
   * p. 2-159f., constraint [1]: ``A fork segment should not have
   * guards or triggers.'', constraint [2]: ``A join segment should
   * not have guards or triggers.'', and constraint [5]: ``Transitions
   * outgoing pseudostates may not have a trigger.'')
   *
   * @return compound transition's compound trigger (if there is an
   * initial join part, an artificial completion event for several
   * states is returned)
   */
  public UEvent getTrigger() {
    if (!this.joinTransitions.isEmpty())
      return UEvent.completion(this.joinTransitions.stream().map(UTransition::getSource).flatMap(toType(UState.class)).collect(toSet()));
    return this.transitionTree.getTrigger();
  }

  /**
   * @return compound transition's trigger context
   */
  public UContext getTriggerContext() {
    return this.transitionTree.getTriggerContext();
  }

  /**
   * Determine compound transition's compound guard.
   *
   * The compound guard consists only of the guard of the transition
   * tree, as transitions leading to a join (pseudo-)state and
   * transitions originating in a fork (pseudo-)state may not show
   * guards (UML specification 1.4, p. 2-159, constraint [1]: ``A fork
   * segment should not have guards or triggers.'', and constraint
   * [2]: ``A join segment should not have guards or triggers.'').
   * However, since some compound transitions may take priority over
   * this condition the priority guard has to be taken into account
   * conjunctively.
   *
   * @return compound transition's compound guard
   */
  public UExpression getGuard() {
    return UExpression.and(this.priorityGuard, this.transitionTree.getGuard());
  }

  /**
   * @return compound transition's guard context
   */
  public UContext getGuardContext() {
    return this.transitionTree.getGuardContext();
  }

  /**
   * Determine compound transition's compound effect.
   *
   * The compound effect consists of the sequential composition of the
   * following two parts: the effects of the initial join part in
   * parallel, and the (compound effect) of the transition tree.
   *
   * @return compound transition's compound effect
   */
  public UAction getEffect() {
    UAction joinEffect = UAction.skip();
    for (UTransition joinTransition : this.joinTransitions)
      joinEffect = UAction.par(joinEffect, joinTransition.getEffect());
    UAction treeEffect = this.transitionTree.getEffect();
    return UAction.seq(joinEffect, treeEffect);
  }

  /**
   * @return compound transition's effect context
   */
  public UContext getEffectContext() {
    return this.transitionTree.getEffectContext();
  }

  /**
   * Determine compound transition's main source vertex.
   *
   * "The main source is a direct substate of the Region that contains the source States"
   * (UML 2.5 specification, p. 316).
   *
   * @return compound transition's main source vertex
   */
  public UVertex getMainSourceVertex() {
    UVertex sourceLca = UStateMachine.leastReflexiveCommonAncestorVertex(this.getSources());
    UVertex targetLca = UStateMachine.leastReflexiveCommonAncestorVertex(this.getTargets());
    URegion lca = UStateMachine.leastCommonAncestorRegion(sourceLca, targetLca);

    UVertex mainSourceVertex = sourceLca;
    while (!lca.equals(mainSourceVertex.getContainer()))
      mainSourceVertex = requireNonNull(mainSourceVertex.getContainer().getContainer());
    return mainSourceVertex;
  }

  /**
   * Determine compound transition's main target vertex.
   *
   * "[...] the main target is the substate of the Region that contains the target States"
   * (UML 2.5 specification, p. 316).
   *
   * @return compound transition's main target vertex
   */
  public UVertex getMainTargetVertex() {
    UVertex sourceLca = UStateMachine.leastReflexiveCommonAncestorVertex(this.getSources());
    UVertex targetLca = UStateMachine.leastReflexiveCommonAncestorVertex(this.getTargets());
    URegion lca = UStateMachine.leastCommonAncestorRegion(sourceLca, targetLca);

    UVertex mainTargetVertex = targetLca;
    while (!lca.equals(mainTargetVertex.getContainer()))
      mainTargetVertex = requireNonNull(mainTargetVertex.getContainer().getContainer());
    return mainTargetVertex;
  }

  /**
   * Determine the set of vertices that are entered by this compound
   * transition.
   *
   * Every composite state has to be entered properly: For a
   * non-concurrent composite its initial pseudo-state is entered; for
   * a concurrent composite state the initial pseudo-states of all those
   * orthogonal regions that are not targeted explicitly are entered.
   * 
   * @return compound transition's entered vertices
   */
  public Set<UVertex> getEnteredVertices() {
    Set<UVertex> enteredStates = new HashSet<>();
    UVertex mainTarget = this.getMainTargetVertex();
    enteredStates.add(mainTarget);
    Set<UVertex> targets = this.getTargets();
    for (UState target : targets.stream().flatMap(toType(requireNonNull(UState.class))).collect(toSet())) {
      // Properly enter explicitly targeted composite states
      for (URegion region : target.getRegions()) {
        if (region.chooseVertex(targets) == null)
          enteredStates.add(requireNonNull(region.getInitial()));
      }
      // Properly enter explicit target and container states
      while (!target.equals(mainTarget)) {
        enteredStates.add(target);
        target = requireNonNull(target.getContainer().getContainer());
        // Properly enter neighbouring regions of a concurrent composite state
        if (target.getRegions().size() > 1) {
          for (URegion region : target.getRegions()) {
            if (region.chooseVertex(targets) == null)
              enteredStates.add(requireNonNull(region.getInitial()));
          }
        }
      }
    }
    return enteredStates;
  }

  /**
   * Determine all regions which are not properly entered by
   * this compound transition, since they do not show default entries.
   *
   * @return all improperly entered composite states
   */
  public Set<URegion> getImproperlyEnteredRegions() {
    Set<URegion> improperlyEnteredRegions = new HashSet<>();
    UVertex mainTarget = this.getMainTargetVertex();
    Set<UVertex> targets = this.getTargets();
    for (UState target : targets.stream().flatMap(toType(requireNonNull(UState.class))).collect(toSet())) {
      for (URegion region : target.getRegions()) {
        if (region.chooseVertex(targets) == null && region.getInitial() == null)
          improperlyEnteredRegions.add(region);
      }
      while (!target.equals(mainTarget)) {
        target = requireNonNull(target.getContainer().getContainer());
        // Check neighbouring regions of a concurrent composite state
        if (target.getRegions().size() > 1) {
          for (URegion region : target.getRegions()) {
            if (region.chooseVertex(targets) == null && region.getInitial() == null) {
              improperlyEnteredRegions.add(region);
            }
          }
        }
      }
    }
    return improperlyEnteredRegions;
  }

  /**
   * Determine the set of states that are exited by this compound
   * transition.
   *
   * @return compound transition's exited states
   */
  public Set<UVertex> getExitedVertices() {
    Set<UVertex> exitedVertices = new HashSet<>();
    UVertex mainSource = this.getMainSourceVertex();
    exitedVertices.add(mainSource);
    for (UVertex source : this.getSources()) {
      while (!source.equals(mainSource)) {
        exitedVertices.add(source);
        source = source.getContainer().getContainer();
      }
    }
    return exitedVertices;
  }

  /**
   * Determine whether this compound transition is in conflict with
   * another compound transition.
   *
   * "Two transitions are said to conflict if they both exit the same
   * state, or, more precisely, that the intersection of the set of
   * states they exit is non-empty." (UML 1.4, p. 2-168).
   *
   * -AK Since the main source state is the top-most state left by a
   * compound transition it should be enough to check whether the main
   * source states of this and the other transition are ancestors of
   * each other.
   *
   * @param other another compound transition
   * @return whether this compound transition is in conflict with {@code other}
   */
  public boolean isInConflict(UCompoundTransition other) {
    return (this.getMainSourceVertex().isAncestor(other.getMainSourceVertex()) ||
            other.getMainSourceVertex().isAncestor(this.getMainSourceVertex()));
  }

  /**
   * Determine whether this compound transition is in conflict with
   * another compound transition.
   * 
   * @see uml.statemachine.semantics.UCompoundTransition.isInConflict(UCompoundTransition)
   *
   * @param others a set of compound transitions
   * @return whether this compound transition is in conflict with
   * any compound transition in {@code others}
   */
  public boolean isInConflict(Set<UCompoundTransition> others) {
    return others.stream().anyMatch(other -> this.isInConflict(other));
  }

  /**
   * Determine whether this compound transition is conflict free.
   * 
   * A compound transition is called conflict free, if it cannot be part of a maximally
   * conflict-free prioritised set of compound transitions of cardinality greater than one.
   * 
   * @return whether this compound transition is conflict free
   */
  public boolean isConflictFree() {
    return this.getMainSourceVertex().getStateMachine().getCompounds().stream().
        noneMatch(other -> !other.equals(this) &&
                           other.getTrigger().equals(this.getTrigger()) &&
                           other.isInConflict(this));
  }

  /**
   * Determine whether this compound transition takes priority over
   * another compound transition.
   *
   * "The priority of a transition is defined based on its source
   * state. The priority of joined transitions is based on the
   * priority of the transition with the most transitively nested
   * source state.  In general, if t1 is a transition whose source
   * state is s1, and t2 has source s2, then:
   * <UL>
   * <LI>If s1 is a direct or transitively nested substate of s2, then
   * t1 has higher priority than t2.
   * <LI>If s1 and s2 are not in the same state configuration, then
   * there is no priority difference between t1 and t2." (UML 1.4,
   * p. 2-169).
   * </UL>
   */
  public boolean hasPriority(UCompoundTransition other) {
    int thisMaxSourceLevel = -1;
    UVertex thisMaxSource = null;
    for (UVertex source : this.getSources()) {
      int sourceLevel = source.getLevel();
      if (sourceLevel > thisMaxSourceLevel) {
        thisMaxSource = source;
        thisMaxSourceLevel = sourceLevel;
      }
    }

    int otherMaxSourceLevel = -1;
    UVertex otherMaxSourceState = null;
    for (UVertex source : other.getSources()) {
      int sourceStateLevel = source.getLevel();
      if (sourceStateLevel > otherMaxSourceLevel) {
        otherMaxSourceState = source;
        otherMaxSourceLevel = sourceStateLevel;
      }
    }

    if (thisMaxSource == null || otherMaxSourceState == null)
      return false;
    return (!otherMaxSourceState.equals(thisMaxSource)) && (otherMaxSourceState.isAncestor(thisMaxSource));
  }

  /**
   * Determine compound transition's (artificial) name.
   *
   * The artificial name consists of all source state names (in
   * arbitrary order) and all target state names (in arbitrary order).
   *
   * @return compound transition's name
   */
  public String getName() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(Formatter.separated(this.getSources(), uState -> uState.getName(), ""));
    resultBuilder.append("2");
    resultBuilder.append(Formatter.separated(this.getTargets(), uState -> uState.getName(), ""));
    return resultBuilder.toString();
  }

  /**
   * Determine compound transition's full (artificial) name.
   *
   * The artificial name consists of all full source state names (in
   * arbitrary order) and all full target state names (in arbitrary order).
   *
   * @return compound transition's full name
   */
  public String getFullName() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(Formatter.separated(this.getSources(), uState -> uState.getIdentifier().toString(), "_"));
    resultBuilder.append("_2_");
    resultBuilder.append(Formatter.separated(this.getTargets(), uState -> uState.getIdentifier().toString(), "_"));
    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.joinTransitions, this.transitionTree);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UCompoundTransition other = (UCompoundTransition)object;
      return (Objects.equals(this.joinTransitions, other.joinTransitions) &&
              Objects.equals(this.transitionTree, other.transitionTree));
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("CompoundTransition [mainSourceState = ");
    resultBuilder.append(this.getMainSourceVertex().getIdentifier());
    resultBuilder.append(", sourceStates = ");
    resultBuilder.append(Formatter.set(this.getSources(), state -> state.getIdentifier().toString()));
    resultBuilder.append(", mainTargetState = ");
    resultBuilder.append(this.getMainTargetVertex().getIdentifier());
    resultBuilder.append(", targetStates = ");
    resultBuilder.append(Formatter.set(this.getTargets(), state -> state.getIdentifier().toString()));
    resultBuilder.append(", trigger = ");
    resultBuilder.append(this.getTrigger().getName());
    resultBuilder.append(", guard = ");
    resultBuilder.append(this.getGuard());
    resultBuilder.append(", effect = ");
    resultBuilder.append(this.getEffect());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
