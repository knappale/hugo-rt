package uml.statemachine.semantics;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import uml.UAction;
import uml.UContext;
import uml.UExpression;
import uml.statemachine.UEvent;
import uml.statemachine.UState;
import uml.statemachine.UVertex;

import static java.util.stream.Collectors.toList;
import static util.Objects.toType;
import static util.Objects.requireNonNull;


/**
 * UML maximal conflict free set of compound transitions
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UConflictFreeSet {
  private Set<UCompoundTransition> compounds = new HashSet<>();
  private UExpression conflictGuard = UExpression.trueConst();

  /**
   * Create a maximal conflict free set of compound transitions from a
   * source to a target configuration.
   *
   * The enabledness of a (maximal) conflict free set may depend on an
   * additional guard, making the conflict free set really maximal, as
   * other non-conflicting transitions are currently not enabled.
   * 
   * @param compounds maximal conflict free set of compound transitions
   * @param conflictGuard guard for this maximal conflict free set of
   * compound transitions to be enabled
   *
   * @pre compounds.size() > 0 &&
   *      \forall compound1, compound2 \in compounds .
   *        compound1.getTrigger().equals(compound2.getTrigger())
   */
  public UConflictFreeSet(Set<UCompoundTransition> compounds, UExpression conflictGuard) {
    this.compounds.addAll(compounds);
    this.conflictGuard = conflictGuard;
  }

  /**
   * @return maximal conflict free set's source configuration
   */
  public UConfiguration getTarget(UConfiguration source) {
    return source.getCompoundTarget(this.compounds);
  }

  /**
   * @return vertices entered by this maximal conflict free set
   */
  public Set<UVertex> getEnteredVertices() {
    Set<UVertex> enteredVertices = new HashSet<>();
    for (UCompoundTransition compound : this.compounds) {
      enteredVertices.addAll(compound.getEnteredVertices());
    }
    return enteredVertices;
  }

  /**
   * @return vertices exited by this maximal conflict free set
   */
  public Set<UVertex> getExitedVertices() {
    Set<UVertex> exitedVertices = new HashSet<>();
    for (UCompoundTransition compound : this.compounds) {
      exitedVertices.addAll(compound.getExitedVertices());
    }
    return exitedVertices;
  }

  /**
   * @return maximal conflict free set's triggering event
   */
  public UEvent getTrigger() {
    return this.compounds.iterator().next().getTrigger();
  }

  /**
   * @return maximal conflict free set's trigger context
   */
  public UContext getTriggerContext() {
    return this.compounds.iterator().next().getTriggerContext();
  }

  /**
   * @return maximal conflict free set's compound guard
   */
  public UExpression getGuard() {
    UExpression guard = this.conflictGuard;
    for (UCompoundTransition compound : this.compounds) {
      guard = UExpression.and(guard, compound.getGuard());
    }
    return guard;
  }

  /**
   * @return maximal conflict free set's guard context
   */
  public UContext getGuardContext() {
    return this.compounds.iterator().next().getGuardContext();
  }

  /**
   * @return maximal conflict free set's compound effect
   */
  public UAction getEffect() {
    UAction effect = UAction.skip();
    for (UCompoundTransition compound : this.compounds) {
      effect = UAction.par(effect, compound.getEffect());
    }
    return effect;
  }

  /**
   * @return maximal conflict free set's effect context
   */
  public UContext getEffectContext() {
    return this.compounds.iterator().next().getEffectContext();
  }

  /**
   * @return maximal conflict free set's entry action
   */
  public UAction getEntryAction() {
    List<UState> enteredStates = new ArrayList<>(this.getEnteredVertices().stream().flatMap(toType(requireNonNull(UState.class))).collect(toList()));
    enteredStates = util.Lists.topologicalSort(enteredStates,
					      new Comparator<UState>() {
						public int compare(UState s1, UState s2) {
						  if (s1 == null || s2 == null)
						    return 0;
						  if (s1.equals(s2))
						    return 0;
                                                  if (s1.isAncestor(s2))
                                                    return 1;
                                                  if (s2.isAncestor(s1))
                                                    return -1;
                                                  return 0;
						}
                                              });
    List<UAction> parEntries = new ArrayList<>();
    {
      UAction parEntry = UAction.skip();
      for (int i = 0; i < enteredStates.size(); i++) {
	UState enteredState = enteredStates.get(i);
	if (enteredState.hasEntryAction()) {
	  parEntry = UAction.par(parEntry, enteredState.getEntryAction());
	}
	if (i+1 < enteredStates.size()) {
	  UState nextEnteredState = enteredStates.get(i+1);
	  if (enteredState.isAncestor(nextEnteredState)) {
	    parEntries.add(parEntry);
	    parEntry = UAction.skip();
	  }
	}
      }
      parEntries.add(parEntry);
    }
      
    UAction entry = UAction.skip();
    for (UAction parEntry : parEntries) {
      entry = UAction.seq(entry, parEntry);
    }
    return entry;
  }

  /**
   * @return maximal conflict free set's entry action context
   */
  public UContext getEntryContext() {
    return this.compounds.iterator().next().getTriggerContext();
  }

  /**
   * @return maximal conflict free set's exit action
   */
  public UAction getExitAction() {
    List<UState> exitedStates = new ArrayList<>(this.getExitedVertices().stream().flatMap(toType(requireNonNull(UState.class))).collect(toList()));
    exitedStates = util.Lists.topologicalSort(exitedStates,
					     new Comparator<UState>() {
					       public int compare(UState s1, UState s2) {
					         if (s1 == null || s2 == null)
					           return 0;
					         if (s1.equals(s2))
					           return 0;
                                                 if (s1.isAncestor(s2))
                                                   return -1;
                                                 if (s2.isAncestor(s1))
                                                   return 1;
                                                 return 0;
                                                 }
                                             });
    List<UAction> parExits = new ArrayList<>();
    {
      UAction parExit = UAction.skip();
      for (int i = 0; i < exitedStates.size(); i++) {
	UState exitedState = exitedStates.get(i);
	if (exitedState.hasExitAction())
	  parExit = UAction.par(parExit, exitedState.getExitAction());
	if (i+1 < exitedStates.size()) {
	  UState nextExitedState = exitedStates.get(i+1);
	  if (nextExitedState.isAncestor(exitedState)) {
	    parExits.add(parExit);
	    parExit = UAction.skip();
	  }
	}
      }
      parExits.add(parExit);
    }
      
    UAction exit = UAction.skip();
    for (UAction parExit : parExits) {
      exit = UAction.seq(exit, parExit);
    }
    return exit;
  }

  /**
   * @return maximal conflict free set's exit action context
   */
  public UContext getExitContext() {
    return this.compounds.iterator().next().getTriggerContext();
  }

  /**
   * @return maximal conflict free set's set of compound transitions
   */
  public Set<UCompoundTransition> getCompounds() {
    return this.compounds;
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.getGuard().toString());
    resultBuilder.append("\n");
    for (UCompoundTransition compound : this.getCompounds()) {
      resultBuilder.append(compound);
      resultBuilder.append("\n");
    }
    return resultBuilder.toString();
  }
}
