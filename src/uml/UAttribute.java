package uml;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static util.Objects.requireNonNull;


/**
 * Contains data of an attribute.
 * 
 * An attribute may either be a class attribute (static) or an instance
 * attribute. Moreover it may be a constant (final) or variable.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UAttribute extends UStructural {
  private UClass owner;
  private UClassifier classifier;
  private UExpression upper = UExpression.intConst(1);
  private boolean isStatic = false;
  private boolean isFinal = false;
  private List<UExpression> initialValues = new ArrayList<>();

  /**
   * Creates a new attribute.
   * 
   * @param name attribute's name
   * @param classifier attribute's classifier
   * @param owner attribute's owning class
   */
  public UAttribute(String name, UClassifier classifier, UClass owner) {
    super(name);
    this.classifier = classifier;
    this.owner = owner;
    this.owner.addAttribute(this);
  }

  @Override
  @NonNullByDefault({})
  public <T> T cases(@NonNull Function<@NonNull UAttribute, T> attributeFun,
                     @NonNull Function<@NonNull UParameter, T> parameterFun,
                     @NonNull Function<@NonNull UObject, T> objectFun) {
    return attributeFun.apply(this);
  }

  /**
   * @return attribute's owning class
   */
  public UClass getOwner() {
    return this.owner;
  }

  /**
   * @return attribute's classifier
   */
  public UClassifier getClassifier() {
    return this.classifier;
  }

  /**
   * Add an initial value to this attribute.
   * 
   * @param initialValue initial value
   */
  public void addInitialValue(UExpression initialValue) {
    this.initialValues.add(initialValue);
  }

  /**
   * @return whether attribute has proper initial values
   */
  public boolean hasInitialValues() {
    return this.initialValues.size() > 0;
  }

  /**
   * Add default initial values to this attribute.
   *
   * Only if no initial values are already provided, new initial
   * values are added.  For an array with an upper bound for which
   * the value cannot be determined statically, nothing will be added.
   *
   * @return list of initial values added
   */
  public List<UExpression> addDefaultInitialValues() {
    List<UExpression> addedDefaults = new ArrayList<>();
    if (!this.initialValues.isEmpty())
      return addedDefaults;
    int upper = this.upper.getIntegerValue(this.getUpperContext());
    UExpression defaultValue = this.classifier.getInitialValue();
    for (int i = 0; i < upper; i++) {
      addedDefaults.add(defaultValue);
      this.addInitialValue(defaultValue);
    }
    return addedDefaults;
  }

  /**
   * @return attribute's initial values
   */
  public List<UExpression> getInitialValues() {
    return this.initialValues;
  }

  /**
   * @return attribute's n-th initial value or {@code null} if there is no such initial value
   */
  public @Nullable UExpression getInitialValue(int n) {
    if (0 <= n && n < this.initialValues.size())
      return this.initialValues.get(n);
    else {
      if (this.isArray())
        return getType().getUnderlyingType().getInitialValue();
      else
        return getType().getInitialValue();
    }
  }

 /**
   * @return attribute's initial values context
   */
  public UContext getInitialValuesContext() {
    if (this.isStatic())
      return UContext.modelConstant(this.getOwner().getModel());
    else
      return UContext.classConstant(this.getOwner());
  }

  /**
   * Set attribute's upper multiplicity
   * 
   * @param upper upper multiplicity expression
   */
  public void setUpper(UExpression upper) {
    this.upper = upper;
  }

  /**
   * @return attribute's upper multiplicity
   */
  public UExpression getUpper() {
    return this.upper;
  }

  /**
   * @return attribute's upper multiplicity context
   */
  public UContext getUpperContext() {
    return UContext.classConstant(this.getOwner());
  }

  /**
   * @return whether attribute is an array
   */
  public boolean isArray() {
    return this.getSize() != 1;
  }

  /**
   * @return attribute's size, i.e. upper bound
   */
  public int getSize() {
    return this.upper.getIntegerValue(this.getUpperContext());
  }

  /**
   * Set attribute's final status.
   * 
   * @param isFinal whether the attribute is final
   */
  public void setFinal(boolean isFinal) {
    this.isFinal = isFinal;
  }

  /**
   * @return whether this attribute is final
   */
  public boolean isFinal() {
    return this.isFinal;
  }

  /**
   * Set attribute's static status.
   * 
   * @param isStatic whether the attribute is static
   */
  public void setStatic(boolean isStatic) {
    this.isStatic = isStatic;
  }

  /**
   * @return whether this attribute is static
   */
  @Override
  public boolean isStatic() {
    return this.isStatic;
  }

  /**
   * @return whether this attribute is a constant (final)
   */
  @Override
  public boolean isConstant() {
    return this.isFinal();
  }

  /**
   * @return attribute's type
   */
  public UType getType() {
    if (this.isArray())
      return UType.array(this.getClassifier(), upper.getIntegerValue(getUpperContext()));
    else
      return UType.simple(this.getClassifier());
  }

  /**
   * @return attribute's dependencies
   */
  public Set<UStatual> getDependencies() {
    Set<UStatual> dependencies = new HashSet<UStatual>();
    dependencies.addAll(this.upper.getDependencies(this.getUpperContext()));
    for (UExpression initialValue : this.getInitialValues()) {
      dependencies.addAll(initialValue.getDependencies(this.getInitialValuesContext()));
    }
    return dependencies;
  }

  /**
   * @param prefix prefix string
   * @return slot's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder result = new StringBuilder();

    result.append(prefix);
    if (this.isStatic())
      result.append("static ");
    if (this.isFinal())
      result.append("const ");
    else
      result.append("attr ");
    result.append(this.getName());
    result.append(" : ");
    result.append(this.getClassifier().getName());
    if (this.getSize() > 1) {
      result.append("[");
      result.append(getUpper().toString());
      result.append("]");
    }
    if (this.hasInitialValues()) {
      result.append(" = ");
      if (this.getInitialValues().size() > 1) {
        result.append("{ ");
        String separator = "";
        for (UExpression initialValue : this.getInitialValues()) {
          result.append(separator);
          result.append(initialValue.toString());
          separator = ", ";
        }
        result.append(" }");
      }
      else {
        result.append(requireNonNull(this.getInitialValue(0)).toString());
      }
    }
    result.append(";");

    return requireNonNull(result.toString());
  }

  /**
   * @return attribute's representation in UTE format
   */
  public String declaration() {
    return this.declaration("");
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Attribute [name = ");
    resultBuilder.append(getName());
    resultBuilder.append(", classifier = ");
    resultBuilder.append(getClassifier().getName());
    resultBuilder.append(", upper = ");
    resultBuilder.append(getUpper());
    resultBuilder.append(", final = ");
    resultBuilder.append(isFinal());
    resultBuilder.append(", static = ");
    resultBuilder.append(isStatic());
    if (hasInitialValues()) {
      resultBuilder.append(", initialValues = ");
      resultBuilder.append(getInitialValues().toString());
    }
    resultBuilder.append("]");
    return requireNonNull(resultBuilder.toString());
  }
}
