package uml;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Contains data of a UML signal.
 * 
 * In contrast to UML, a signal is not a classifier.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class USignal extends UElement {
  private UModel model;
  private List<UReception> receptions = new ArrayList<>();

  /**
   * Creates a signal.
   *
   * @param name signal's name
   * @param model signal's owning model
   */
  public USignal(String name, UModel model) {
    super(name);
    this.model = model;
  }

  /**
   * @return classifier's model
   */
  public UModel getModel() {
    return this.model;
  }

  /**
   * Add a reception to this signal.
   *
   * @param reception a reception
   */
  public void addReception(UReception reception) {
    this.receptions.add(reception);
  }

  /**
   * @param prefix prefix string
   * @return signal's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";

    resultBuilder.append(prefix);
    resultBuilder.append("signal ");
    resultBuilder.append(this.getName());
    if (this.receptions.isEmpty()) {
      resultBuilder.append(";");
      return resultBuilder.toString();
    }

    var reception = this.receptions.get(0);
    resultBuilder.append(" {\n");
    for (var parameter : reception.getParameters())
      resultBuilder.append(parameter.declaration(nextPrefix));
    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  /**
   * @return class's representation in UTE format
   */
  public String declaration() {
    return this.declaration("");
  }

  /**
   * @return signal's string representation
   */
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Signal [name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
