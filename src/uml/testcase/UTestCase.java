package uml.testcase;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UCollaboration;
import uml.UContext;
import uml.UElement;
import uml.UType;
import util.Formatter;


/**
 * A test case.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UTestCase extends UElement {
  private UCollaboration uCollaboration;
  private Set<UObservable> observables = new HashSet<>();
  private List<UObservable> testSequence = new LinkedList<>();

  private control.@Nullable Properties properties = null;

  /**
   * Create a test case in a collaboration.
   *
   * @param name test case's name
   * @param collaboration enclosing (UML) collaboration
   */
  public UTestCase(String name, UCollaboration uCollaboration) {
    super(name);
    this.uCollaboration = uCollaboration;
    this.uCollaboration.addTestCase(this);
  }

  /**
   * @return test case's collaboration
   */
  public UCollaboration getCollaboration() {
    return this.uCollaboration;
  }

  /**
   * @return test case's observables
   */
  public Set<UObservable> getObservables() {
    return this.observables;
  }

  /**
   * Add an observable (message occurrence specification) to the set of observables.
   * 
   * @param observable a message occurrence specification
   */
  public void addObservable(UObservable observable) {
    this.observables.add(observable);
  }

  /**
   * @return test case's test sequence
   */
  public List<UObservable> getTestSequence() {
    return this.testSequence;
  }

  /**
   * Add a message occurrence specification to the test sequence.
   * 
   * @param occurrence a message occurrence specification
   */
  public void addOccurrence(UObservable occurrence) {
    this.testSequence.add(occurrence);
  }

 /**
   * Determine test case's properties.
   *
   * @return test case's properties
   */
  public control.Properties getProperties() {
    control.Properties properties = this.properties;
    if (properties != null)
      return properties;
    properties = getCollaboration().getProperties().getCopy();
    this.properties = properties;
    return properties;
  }

  /**
   * Determine test case's type.
   * 
   * @return test case's type
   */
  public UType getType(UContext context) {
    for (UObservable observable : this.observables) {
      uml.UType observableType = observable.getType(context);
      if (observableType.isError())
        return observableType;
    }

    for (UObservable occurrence : this.testSequence) {
      uml.UType occurrenceType = occurrence.getType(context);
      if (occurrenceType.isError())
        return occurrenceType;
    }

    return uml.UType.meta("Test case");
  }

  /**
   * UTE declaration of this test case starting with {@code prefix}.
   * 
   * @param prefix prefix string
   * @return test case's representation in UTE format with prefix
   */
  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";
    String next2Prefix = nextPrefix + "  ";
    String separator = "";
    boolean pending = false;

    resultBuilder.append(prefix);
    resultBuilder.append("test case " + this.getName() + " {\n");

    if (!this.getProperties().equals(getCollaboration().getProperties())) {
      resultBuilder.append(this.getProperties().declarationOfDiffering(getCollaboration().getProperties(), nextPrefix));
      pending = true;
    }

    if (pending)
      resultBuilder.append("\n\n");
    resultBuilder.append(nextPrefix);
    resultBuilder.append("observables {");
    separator = next2Prefix;
    for (UObservable observable : this.getObservables()) {
      resultBuilder.append(separator);
      resultBuilder.append(observable.declaration());
      separator = ",\n" + next2Prefix;
    }
    resultBuilder.append("\n");
    resultBuilder.append(nextPrefix);
    resultBuilder.append("}");
    pending = true;

    if (pending)
      resultBuilder.append("\n\n");
    resultBuilder.append(nextPrefix);
    resultBuilder.append("sequence {");
    separator = next2Prefix;
    for (UObservable occurrence : this.getTestSequence()) {
      resultBuilder.append(separator);
      resultBuilder.append(occurrence.declaration());
      separator = ",\n" + next2Prefix;
    }
    resultBuilder.append("\n");
    resultBuilder.append(nextPrefix);
    resultBuilder.append("}");

    resultBuilder.append(prefix);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("Test case [name = ");
    resultBuilder.append(this.getName());
    resultBuilder.append(", observables = ");
    resultBuilder.append(Formatter.set(observables, observable -> observable.getName()));
    resultBuilder.append("]]");
    
    return resultBuilder.toString();
  }
}
