package uml.testcase;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import uml.UBehavioural;
import uml.UContext;
import uml.UDescriptor;
import uml.UElement;
import uml.UExpression;
import uml.UObject;
import uml.UType;
import util.Formatter;
import util.Identifier;

import static util.Formatter.quoted;
import static util.Objects.requireNonNull;


/**
 * Occurrence specifications in a test case denote the sending or receiving
 * of messages between objects or the sending of a message from the user
 * to an object.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class UObservable extends UElement {
  enum Kind {
    OCC,
    DISP;
  }

  private final Kind kind;
  private final @Nullable UObject sender;
  private final Identifier behaviouralId;
  private final List<UExpression> arguments = new ArrayList<>();
  private final UObject receiver;

  public interface InContextVisitor<T> {
    public UContext getContext();

    public T onTesterOcc(UBehavioural behavioural, List<UExpression> arguments, UObject receiver);
    public T onTesterDisp(UBehavioural behavioural, List<UExpression> arguments, UObject receiver);
    public T onOcc(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver);
    public T onDisp(UObject sender, UBehavioural behavioural, List<UExpression> arguments, UObject receiver);
  }

  private UObservable(Kind kind, @Nullable UObject sender, Identifier behaviouralId, List<UExpression> arguments, UObject receiver) {
    super("");
    this.kind = kind;
    this.sender = sender;
    this.behaviouralId = behaviouralId;
    this.arguments.addAll(arguments);
    this.receiver = receiver;
  }

  /**
   * Create an observable representing the receiving of a message with
   * some arguments from a sender by a receiver.
   *
   * @param sender a sending object
   * @param behaviouralId a message identifier
   * @param arguments a list of arguments
   * @param receiver a receiving object
   * @return an occurrence specification
   */
  public static UObservable occ(@Nullable UObject sender, Identifier behaviouralId, List<UExpression> arguments, UObject receiver) {
    return new UObservable(Kind.OCC, sender, behaviouralId, arguments, receiver);
  }

  /**
   * Create an observable representing the sending of a message with
   * some arguments from a sender to a receiver.
   *
   * @param sender a sending object
   * @param behaviouralId a message identifier
   * @param arguments a list of arguments
   * @param receiver a receiving object
   * @return an occurrence specification
   */
  public static UObservable disp(@Nullable UObject sender, Identifier behaviouralId, List<UExpression> arguments, UObject receiver) {
    return new UObservable(Kind.DISP, sender, behaviouralId, arguments, receiver);
  }

  /**
   * Determine occurrence specification's type.
   * 
   * @return occurrence specification's type
   */
  public UType getType(UContext context) {
    // Check whether all arguments have types
    List<UType> argumentTypes = new ArrayList<>();
    for (UExpression argument : this.arguments) {
      UType argumentType = argument.getType(context);
      if (argumentType.isError())
        return UType.error("In observable " + quoted(this.toString()) + ": " + argumentType.getErrorMessage());
      argumentTypes.add(argumentType);
    }

    // Check whether the callee class shows a behavioural feature
    // with appropriate name and parameter types
    UBehavioural behavioural = context.getBehavioural(this.receiver.getC1ass(), this.behaviouralId, argumentTypes, context.getVoidType());
    if (behavioural == null)
      return uml.UType.error("no matching behavioural element found for " + quoted(UDescriptor.behavioural(receiver.getC1ass(), this.behaviouralId, argumentTypes)) + " in context " + quoted(context.description()));
    UType invocationType = behavioural.getType();
    if (invocationType.isError())
      return invocationType;

    // Ignoring return type of invocation
    return context.getVoidType();
  }
  
  /**
   * Accept an occurrence specification in context visitor.
   *
   * @param visitor a visitor
   * @return visitor's result
   */
  public <T> T accept(InContextVisitor<T> visitor) {
    List<UType> argumentTypes = new ArrayList<>();
    for (UExpression argument : this.arguments) {
      UType argumentType = argument.getType(visitor.getContext());
      argumentTypes.add(argumentType);
    }
    UObject sender = this.sender;
    UBehavioural behavioural = requireNonNull(visitor.getContext().getBehavioural(this.receiver.getC1ass(), this.behaviouralId, argumentTypes, visitor.getContext().getVoidType()));
    switch (this.kind) {
      case OCC:
        if (sender == null)
          return visitor.onTesterOcc(behavioural, this.arguments, this.receiver);
        else
          return visitor.onOcc(sender, behavioural, this.arguments, this.receiver);
      case DISP:
        if (sender == null)
          return visitor.onTesterDisp(behavioural, this.arguments, this.receiver);
        else
          return visitor.onDisp(sender, behavioural, this.arguments, this.receiver);
      default:
        throw new IllegalStateException("Unknown observable kind.");
    }
  }

  /**
   * Determine occurrence specification's string representation in UTE format.
   *
   * @return occurrence specification's string representation in UTE format
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case OCC:
        resultBuilder.append("occ(");
        break;
      case DISP:
        resultBuilder.append("disp(");
        break;
    }
    resultBuilder.append(this.sender != null ? this.sender.getName() : "tester");
    resultBuilder.append(", ");
    resultBuilder.append(this.behaviouralId.toString());
    resultBuilder.append("(");
    resultBuilder.append(Formatter.separated(this.arguments, ", "));
    resultBuilder.append("), ");
    resultBuilder.append(this.receiver.getName());
    resultBuilder.append(")");

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.sender,
                        this.receiver,
                        this.behaviouralId,
                        this.arguments);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      UObservable other = (UObservable)object;
      return this.kind == other.kind &&
             Objects.equals(this.sender, other.sender) &&
             Objects.equals(this.receiver, other.receiver) &&
             Objects.equals(this.behaviouralId, other.behaviouralId) &&
             Objects.equals(this.arguments, other.arguments);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.declaration();
  }
}
