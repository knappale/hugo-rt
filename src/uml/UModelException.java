package uml;


/**
 * @author <A HREF="mailto:majnu@gmx.net">Timm Schäfer</A>
 */
@SuppressWarnings("serial")
public class UModelException extends Exception {
  public UModelException() {
    super();
  }

  public UModelException(String message) {
    super(message);
  }
}
