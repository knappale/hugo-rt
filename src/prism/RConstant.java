package prism;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class RConstant {
  private RDataType type;
  private String name;
  private RExpression expression;
  
  public RConstant(RDataType type, String name, RExpression expression) {
    this.type = type;
    this.name = name;
    this.expression = expression;
  }

  public String expression() {
    return this.name;
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("const ");
    resultBuilder.append(type.declaration());
    resultBuilder.append(" ");
    resultBuilder.append(this.name);
    resultBuilder.append(" = ");
    resultBuilder.append(this.expression.expression());

    return resultBuilder.toString();
  }
}
