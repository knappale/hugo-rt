package prism;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class RDataType {
  enum Kind {
    BOOLEAN,
    INTEGER,
    DOUBLE;

    public String toString() {
      switch (this) {
        case BOOLEAN: return "bool";
        case INTEGER: return "int";
        case DOUBLE: return "double";
        default: return "[unknown]";
      }
    }
  }

  private Kind kind;

  private RDataType(Kind kind) {
    this.kind = kind;
  }

  public static RDataType boolType() {
    return new RDataType(Kind.BOOLEAN);
  }

  public static RDataType intType() {
    return new RDataType(Kind.INTEGER);
  }

  public static RDataType doubleType() {
    return new RDataType(Kind.DOUBLE);
  }

  public String declaration() {
    return this.kind.toString();
  }
}
