package prism;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class RSystem {
  enum Kind {
    MDP,
    CTMC,
    DTMC;

    public String toString() {
      switch (this) {
        case MDP: return "mdp";
        case CTMC: return "ctmc";
        case DTMC: return "dtmc";
      }
      throw new IllegalStateException("Unknown PRISM system kind.");
    }
  }

  private Kind kind;
  private List<RConstant> constants = new ArrayList<>();
  private List<RVariable> variables = new ArrayList<>();
  private List<RModule> modules = new ArrayList<>();
  private List<RLabel> labels = new ArrayList<>();

  private RSystem(Kind kind) {
    this.kind = kind;
  }

  public static RSystem mdp() {
    return new RSystem(Kind.MDP);
  }

  public static RSystem ctmc() {
    return new RSystem(Kind.CTMC);
  }

  public static RSystem dtmc() {
    return new RSystem(Kind.DTMC);
  }

  public void addConstant(RConstant constant) {
    this.constants.add(constant);
  }

  public void addVariable(RVariable variable) {
    this.variables.add(variable);
  }

  public void addModule(RModule module) {
    this.modules.add(module);
  }

  public void addLabel(RLabel label) {
    this.labels.add(label);
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    String itemSep = "";
    String groupSep = "";

    resultBuilder.append(this.kind);
    resultBuilder.append("\n\n");

    itemSep = groupSep;
    groupSep = "";
    for (RConstant constant : this.constants) {
      resultBuilder.append(itemSep);
      resultBuilder.append(constant.declaration());
      resultBuilder.append("\n");
      itemSep = "";
      groupSep = "\n";
    }

    itemSep = groupSep;
    groupSep = "";
    for (RVariable variable : this.variables) {
      resultBuilder.append(itemSep);
      resultBuilder.append("global ");
      resultBuilder.append(variable.declaration());
      resultBuilder.append("\n");
      itemSep = "";
      groupSep = "\n";
    }

    itemSep = groupSep;
    groupSep = "";
    for (RModule module : this.modules) {
      resultBuilder.append(itemSep);
      resultBuilder.append(module.declaration());
      resultBuilder.append("\n");
      itemSep = "\n";
      groupSep = "\n";
    }

    itemSep = groupSep;
    groupSep = "";
    for (RLabel label : this.labels) {
      resultBuilder.append(itemSep);
      resultBuilder.append(label.declaration());
      resultBuilder.append("\n");
      itemSep = "";
      groupSep = "\n";
    }
 
    return resultBuilder.toString();
  }
}
