package prism;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class RLabel {
  private String name;
  private RExpression expression;

  public RLabel(String name, RExpression expression) {
    this.name = name;
    this.expression = expression;
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("label \"");
    resultBuilder.append(this.name);
    resultBuilder.append("\" = ");
    resultBuilder.append(this.expression.expression());
    resultBuilder.append(";");
    return resultBuilder.toString();
  }
}
