package prism;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class RCommand {
  private String label;
  private RExpression guard;
  private List<RBranch> branches = new ArrayList<>();

  public RCommand(String label, RExpression guard, List<RBranch> branches) {
    this.label = label;
    this.guard = guard;
    this.branches.addAll(branches);
  }

  public RCommand(String label, RExpression guard, @NonNull RBranch... branches) {
    this.label = label;
    this.guard = guard;
    this.branches.addAll(Arrays.asList(branches));
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append("[");
    resultBuilder.append(this.label);
    resultBuilder.append("] ");
    resultBuilder.append(this.guard.expression());
    resultBuilder.append(" -> ");
    if (this.branches.isEmpty())
      resultBuilder.append("true");
    else
      resultBuilder.append(Formatter.separated(this.branches, branch -> branch.declaration(), " + "));
    resultBuilder.append(";");

    return resultBuilder.toString();
  }
}
