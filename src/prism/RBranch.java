package prism;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;

/**
 * PRISM updates branch of a command.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de>Alexander Knapp</A>
 * @since 2016-04-16
 */
@NonNullByDefault
public class RBranch {
  private double rate;
  private List<RUpdate> updates = new ArrayList<>();

  public RBranch(Double rate, List<RUpdate> updates) {
    this.rate = rate;
    this.updates.addAll(updates);
  }

  public RBranch(List<RUpdate> updates) {
    this(1.0, updates);
  }

  public RBranch(Double rate, @NonNull RUpdate... updates) {
    this(rate, Arrays.asList(updates));
  }

  public RBranch(@NonNull RUpdate... updates) {
    this(1.0, updates);
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.rate);
    resultBuilder.append(":");
    resultBuilder.append(Formatter.separated(this.updates, update -> Formatter.parenthesised(update.declaration()), " & "));

    return resultBuilder.toString();
  }

}
