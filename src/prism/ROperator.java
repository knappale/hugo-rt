package prism;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * PRISM operators
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public enum ROperator {
  ADD,
  SUB,
  MULT,
  DIV,
  MOD,
  LT,
  LEQ,
  EQ,
  NEQ,
  GEQ,
  GT,
  TRUE,
  FALSE,
  NEG,
  AND,
  OR;

  public String getName() {
    switch (this) {
      case ADD: return "+";
      case SUB: return "-";
      case MULT: return "*";
      case DIV: return "/";
      case MOD: return "mod";
      case LT: return " < ";
      case LEQ: return " <= ";
      case EQ: return " = ";
      case NEQ: return " != ";
      case GEQ: return " >= ";
      case GT: return " > ";
      case TRUE: return "true";
      case FALSE: return "false";
      case NEG: return "!";
      case AND: return " & ";
      case OR: return " | ";
      //$CASES-OMITTED$
      default: return "[unknown]";
    }
  }

  public static boolean precedes(ROperator op1, ROperator op2) {
    return (op1.getPrecedence() > op2.getPrecedence()) ||
           (op1 == op2 && op1.isAssociative());
  }

  public boolean precedes(ROperator op) {
    return precedes(this, op);
  }

  private int getPrecedence() {
    switch (this) {
      case TRUE:
      case FALSE: return 24;
      case NEG: return 22;
      case MULT:
      case MOD:
      case DIV: return 20;
      case SUB: return 19;
      case ADD: return 18;
      case LT:
      case LEQ:
      case GEQ:
      case GT: return 16;
      case EQ:
      case NEQ: return 14;
      case AND:
      case OR: return 10;
      default: return 0;
    }
  }

  private boolean isAssociative() {
    switch (this) {
      case MULT:
      case ADD:
      case AND:
      case OR:
        return true;
      //$CASES-OMITTED$
      default:
        return false;
    }
  }
}
