package prism;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class RVariable {
  static enum Kind {
    BOOL,
    INT;
  }

  private Kind kind;
  private String name;
  private @Nullable RExpression lowerBound;
  private @Nullable RExpression upperBound;
  private @Nullable RExpression initial;

  /**
   * Create a PRISM variable.
   *
   * @param kind variable's kind
   * @param name variable's name
   * @param lowerBound variable's lower range bound expression
   * @param upperBound variable's upper range bound expression
   * @param initial variable's initialisation expression
   */
  private RVariable(Kind kind, String name, @Nullable RExpression lowerBound, @Nullable RExpression upperBound, @Nullable RExpression initial) {
    this.kind = kind;
    this.name = name;
    this.lowerBound = lowerBound;
    this.upperBound = upperBound;
    this.initial = initial;
  }

  public static RVariable bool(String name) {
    return new RVariable(Kind.BOOL, name, null, null, null);
  }

  public static RVariable bool(String name, RExpression initial) {
    return new RVariable(Kind.BOOL, name, null, null, initial);
  }

  public static RVariable integer(String name, RExpression lowerBound, RExpression upperBound) {
    return new RVariable(Kind.INT, name, lowerBound, upperBound, null);
  }

  public static RVariable integer(String name, RExpression lowerBound, RExpression upperBound, RExpression initial) {
    return new RVariable(Kind.INT, name, lowerBound, upperBound, initial);
  }

  String getTypeString() {
    switch (this.kind) {
      case BOOL:
        return "bool";
      case INT: {
        RExpression lowerBound = this.lowerBound;
        RExpression upperBound = this.upperBound;
        if (lowerBound == null || upperBound == null)
          return "int";
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.append("[");
        resultBuilder.append(lowerBound.expression());
        resultBuilder.append("..");
        resultBuilder.append(upperBound.expression());
        resultBuilder.append("]");
        return resultBuilder.toString();
      }
    }

    throw new IllegalStateException("Unknown PRISM variable type");
  }

  public String expression() {
    return this.name;
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(this.name);
    resultBuilder.append(" : ");
    resultBuilder.append(this.getTypeString());
    RExpression initial = this.initial;
    if (initial != null) {
      resultBuilder.append(" init ");
      resultBuilder.append(initial.expression());
    }
    resultBuilder.append(";");

    return resultBuilder.toString();
  }
}
