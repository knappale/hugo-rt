package prism;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class RClock {
  private String name;

  public RClock(String name) {
    this.name = name;
  }

  public String expression() {
    return this.name;
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("clock ");
    resultBuilder.append(this.name);
    resultBuilder.append(";");
    return resultBuilder.toString();
  }
}
