package prism;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;

import static util.Objects.requireNonNull;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class RExpression {
  static enum Kind {
    BOOLEAN,
    INTEGER,
    ACCESS,
    CONDITIONAL,
    UNARY,
    BINARY;
  }

  private boolean hasPrecedence(RExpression other) {
    if ((this.kind == Kind.UNARY || this.kind == Kind.BINARY) && (other.kind == Kind.UNARY || other.kind == Kind.BINARY))
      return ROperator.precedes(requireNonNull(this.op), requireNonNull(other.op));
    return true;
  }

  private static final RExpression TRUEEXPRESSION;
  private static final RExpression FALSEEXPRESSION;
  static {
    TRUEEXPRESSION = new RExpression(Kind.BOOLEAN);
    TRUEEXPRESSION.op = ROperator.TRUE;
    FALSEEXPRESSION = new RExpression(Kind.BOOLEAN);
    FALSEEXPRESSION.op = ROperator.FALSE;
  }

  private Kind kind;
  private int num;
  private @Nullable RVariable variable;
  private @Nullable RClock clock;
  private @Nullable RConstant constant;
  private @Nullable RExpression guard;
  private @Nullable RExpression lexpr;
  private @Nullable ROperator op;
  private @Nullable RExpression rexpr;

  protected RExpression(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create an expression representing a boolean.
   */
  public static RExpression boolConst(boolean bool) {
    return (bool ? TRUEEXPRESSION : FALSEEXPRESSION);
  }

  /**
   * Create an expression representing <CODE>true</CODE>.
   */
  public static RExpression trueConst() {
    return TRUEEXPRESSION;
  }

  /**
   * Create an expression representing <CODE>false</CODE>.
   */
  public static RExpression falseConst() {
    return FALSEEXPRESSION;
  }

  /**
   * Create an expression representing an integer.
   */
  public static RExpression intConst(int num) {
    RExpression e = new RExpression(Kind.INTEGER);
    e.num = num;
    return e;
  }

  /**
   * Create an expression representing a variable access.
   */
  public static RExpression access(RVariable variable) {
    RExpression e = new RExpression(Kind.ACCESS);
    e.variable = variable;
    return e;
  }

  /**
   * Create an expression representing a clock access.
   */
  public static RExpression access(RClock clock) {
    RExpression e = new RExpression(Kind.ACCESS);
    e.clock = clock;
    return e;
  }

  /**
   * Create an expression representing a constant access.
   */
  public static RExpression access(RConstant constant) {
    RExpression e = new RExpression(Kind.ACCESS);
    e.constant = constant;
    return e;
  }

  /**
   * Create an expression representing a negation.
   */
  public static RExpression neg(RExpression expr) {
    if (expr.equals(trueConst()))
      return falseConst();
    if (expr.equals(falseConst()))
      return trueConst();
    if (expr.kind == Kind.UNARY && expr.op == ROperator.NEG)
      return requireNonNull(expr.lexpr);
    if (expr.kind == Kind.BINARY) {
      switch (requireNonNull(expr.op)) {
        case LT:
          return RExpression.relational(requireNonNull(expr.lexpr), ROperator.GEQ, requireNonNull(expr.rexpr));
        case LEQ:
          return RExpression.relational(requireNonNull(expr.lexpr), ROperator.GT, requireNonNull(expr.rexpr));
        case EQ:
          return RExpression.relational(requireNonNull(expr.lexpr), ROperator.NEQ, requireNonNull(expr.rexpr));
        case NEQ:
          return RExpression.relational(requireNonNull(expr.lexpr), ROperator.EQ, requireNonNull(expr.rexpr));
        case GEQ:
          return RExpression.relational(requireNonNull(expr.lexpr), ROperator.LT, requireNonNull(expr.rexpr));
        case GT:
          return RExpression.relational(requireNonNull(expr.lexpr), ROperator.LEQ, requireNonNull(expr.rexpr));
        //$CASES-OMITTED$
        default:
      }
    }
    RExpression e = new RExpression(Kind.UNARY);
    e.op = ROperator.NEG;
    e.lexpr = expr;
    return e;
  }

  /**
   * Create an expression representing a conjunction.
   */
  public static RExpression and(RExpression lexpr, RExpression rexpr) {
    if (lexpr.equals(falseConst()))
      return falseConst();
    if (rexpr.equals(falseConst()))
      return falseConst();
    if (lexpr.equals(trueConst()))
      return rexpr;
    if (rexpr.equals(trueConst()))
      return lexpr;
    RExpression e = new RExpression(Kind.BINARY);
    e.lexpr = lexpr;
    e.op = ROperator.AND;
    e.rexpr = rexpr;
    return e;
  }

  /**
   * Create an expression representing a disjunction.
   */
  public static RExpression or(RExpression lexpr, RExpression rexpr) {
    if (lexpr.equals(trueConst()))
      return trueConst();
    if (rexpr.equals(trueConst()))
      return trueConst();
    if (lexpr.equals(falseConst()))
      return rexpr;
    if (rexpr.equals(falseConst()))
      return lexpr;
    RExpression e = new RExpression(Kind.BINARY);
    e.lexpr = lexpr;
    e.op = ROperator.OR;
    e.rexpr = rexpr;
    return e;
  }

  /**
   * Create an expression representing a conditional.
   */
  public static RExpression conditional(RExpression guard, RExpression lexpr, RExpression rexpr) {
    RExpression e = new RExpression(Kind.CONDITIONAL);
    e.guard = guard;
    e.lexpr = lexpr;
    e.rexpr = rexpr;
    return e;
  }

  /**
   * Create an expression representing a unary minus.
   */
  public static RExpression minus(RExpression expr) {
    return arithmetical(RExpression.intConst(0), ROperator.SUB, expr);
  }

  /**
   * Create an expression representing an arithmetical expression.
   */
  public static RExpression arithmetical(RExpression lexpr, ROperator op, RExpression rexpr) {
    RExpression e = new RExpression(Kind.BINARY);
    e.lexpr = lexpr;
    e.op = op;
    e.rexpr = rexpr;
    return e;
  }

  /**
   * Create an expression representing a relational expression.
   */
  public static RExpression relational(RExpression lexpr, ROperator op, RExpression rexpr) {
    RExpression e = new RExpression(Kind.BINARY);
    e.lexpr = lexpr;
    e.op = op;
    e.rexpr = rexpr;
    return e;
  }

  /**
   * Create an expression representing a less-than.
   */
  public static RExpression lt(RExpression lexpr, RExpression rexpr) {
    return relational(lexpr, ROperator.LT, rexpr);
  }

  /**
   * Create an expression representing a less-than-or-equal
   */
  public static RExpression leq(RExpression lexpr, RExpression rexpr) {
    return relational(lexpr, ROperator.LEQ, rexpr);
  }

  /**
   * Create an expression representing an equation.
   */
  public static RExpression eq(RExpression lexpr, RExpression rexpr) {
    return relational(lexpr, ROperator.EQ, rexpr);
  }

  /**
   * Create an expression representing an inequation.
   */
  public static RExpression neq(RExpression lexpr, RExpression rexpr) {
    return relational(lexpr, ROperator.NEQ, rexpr);
  }

  /**
   * Create an expression representing a greater-than-or equal.
   */
  public static RExpression geq(RExpression lexpr, RExpression rexpr) {
    return relational(lexpr, ROperator.GEQ, rexpr);
  }

  /**
   * Create an expression representing a greater-than.
   */
  public static RExpression gt(RExpression lexpr, RExpression rexpr) {
    return relational(lexpr, ROperator.GT, rexpr);
  }

  /**
   * @return expression representation in PRISM-format
   */
  public String expression() {
    StringBuilder resultBuilder = new StringBuilder();

    switch (this.kind) {
      case BOOLEAN: {
        resultBuilder.append(requireNonNull(this.op).getName());
        return resultBuilder.toString();
      }

      case INTEGER: {
        resultBuilder.append(this.num);
        return resultBuilder.toString();
      }

      case ACCESS: {
        RVariable variable = this.variable;
        if (variable != null) {
          resultBuilder.append(variable.expression());
          return resultBuilder.toString();
        }
        RClock clock = this.clock;
        if (clock != null) {
          resultBuilder.append(clock.expression());
          return resultBuilder.toString();
        }
        RConstant constant = this.constant;
        if (constant != null) {
          resultBuilder.append(constant.expression());
          return resultBuilder.toString();
        }
        return resultBuilder.toString();
      }

      case CONDITIONAL: {
        resultBuilder.append("(");
        resultBuilder.append(requireNonNull(this.guard).expression());
        resultBuilder.append(" ? ");
        resultBuilder.append(requireNonNull(this.lexpr).expression());
        resultBuilder.append(" : ");
        resultBuilder.append(requireNonNull(this.rexpr).expression());
        resultBuilder.append(")");
        return resultBuilder.toString();
      }

      case UNARY: {
        resultBuilder.append(requireNonNull(this.op).getName());
        resultBuilder.append(Formatter.parenthesised(requireNonNull(this.lexpr), e -> e.hasPrecedence(this), e -> e.expression()));
        return resultBuilder.toString();
      }

      case BINARY: {
        RExpression lexpr = requireNonNull(this.lexpr);
        ROperator op = requireNonNull(this.op);
        RExpression rexpr = requireNonNull(this.rexpr);
        if (op == ROperator.MOD) {
          resultBuilder.append(op.getName());
          resultBuilder.append("(");
          resultBuilder.append(lexpr.expression());
          resultBuilder.append(", ");
          resultBuilder.append(rexpr.expression());
          resultBuilder.append(")");
        }
        else {
          resultBuilder.append(Formatter.parenthesised(lexpr, e -> e.hasPrecedence(this), e -> e.expression()));
          resultBuilder.append(op.getName());
          resultBuilder.append(Formatter.parenthesised(rexpr, e -> e.hasPrecedence(this), e -> e.expression()));
        }
        return resultBuilder.toString();
      }
    }

    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.variable,
                        this.clock,
                        this.constant,
                        this.guard,
                        this.lexpr,
                        this.rexpr);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      RExpression other = (RExpression)object;
      return this.kind == other.kind &&
             this.num == other.num &&
             Objects.equals(this.variable, other.variable) &&
             Objects.equals(this.clock, other.clock) &&
             Objects.equals(this.constant, other.constant) &&
             Objects.equals(this.guard, other.guard) &&
             Objects.equals(this.lexpr, other.lexpr) &&
             this.op == other.op &&
             Objects.equals(this.rexpr, other.rexpr);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return expression();
  }
}
