package prism;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class RModule {
  private String name;
  private List<RVariable> variables = new ArrayList<>();
  private List<RClock> clocks = new ArrayList<>();
  private List<RCommand> commands = new ArrayList<>();

  public RModule(String name) {
    this.name = name;
  }

  public void addVariable(RVariable variable) {
    this.variables.add(variable);
  }

  public void addVariables(@NonNull RVariable... variables) {
    this.variables.addAll(Arrays.asList(variables));
  }

  public void addClock(RClock clock) {
    this.clocks.add(clock);
  }

  public void addCommand(RCommand command) {
    this.commands.add(command);
  }

  public void addCommands(@NonNull RCommand... commands) {
    this.commands.addAll(Arrays.asList(commands));
  }

  public String declaration(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextPrefix = prefix + "  ";
    boolean pending = false;

    resultBuilder.append("module ");
    resultBuilder.append(this.name);
    resultBuilder.append("\n");

    if (!this.variables.isEmpty()) {
      for (RVariable variable : this.variables) {
        resultBuilder.append(nextPrefix);
        resultBuilder.append(variable.declaration());
        resultBuilder.append("\n");
      }
      pending = true;
    }

    if (!this.clocks.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      for (RClock clock : this.clocks) {
        resultBuilder.append(nextPrefix);
        resultBuilder.append(clock.declaration());
        resultBuilder.append("\n");
      }
      pending = true;
    }

    if (!this.commands.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      for (RCommand command : this.commands) {
        resultBuilder.append(nextPrefix);
        resultBuilder.append(command.declaration());
        resultBuilder.append("\n");
      }
      pending = true;
    }

    resultBuilder.append(prefix);
    resultBuilder.append("endmodule");

    return resultBuilder.toString();
  }

  public String declaration() {
    return this.declaration("");
  }
}
