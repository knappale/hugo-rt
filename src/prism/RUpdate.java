package prism;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static util.Objects.requireNonNull;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class RUpdate {
  private @Nullable RVariable variable;
  private @Nullable RClock clock;
  private RExpression expression;

  private RUpdate(RVariable variable, RExpression expression) {
    this.variable = variable;
    this.expression = expression;
  }

  private RUpdate(RClock clock, RExpression expression) {
    this.clock = clock;
    this.expression = expression;
  }

  public static RUpdate assign(RVariable variable, RExpression expression) {
    return new RUpdate(variable, expression);
  }

  public static RUpdate inc(RVariable variable) {
    return new RUpdate(variable, RExpression.arithmetical(RExpression.access(variable), ROperator.ADD, RExpression.intConst(1)));
  }

  public static RUpdate dec(RVariable variable) {
    return new RUpdate(variable, RExpression.arithmetical(RExpression.access(variable), ROperator.SUB, RExpression.intConst(1)));
  }

  public static RUpdate reset(RClock clock) {
    return new RUpdate(clock, RExpression.intConst(0));
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    RVariable variable = this.variable;
    if (variable != null)
      resultBuilder.append(variable.expression());
    else
      resultBuilder.append(requireNonNull(this.clock).expression());
    resultBuilder.append("' = ");
    resultBuilder.append(this.expression.expression());

    return resultBuilder.toString();
  }
}
