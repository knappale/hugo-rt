package cpp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import static java.util.stream.Collectors.toList;


@NonNullByDefault
public class CClass {
  private String name;
  private List<CAttribute> attributes = new ArrayList<>();
  private CMethod constructor;
  private List<CMethod> methods = new ArrayList<>();
  private List<CEnum> enumerations = new ArrayList<>();
  private List<String> headers = new ArrayList<>();

  public CClass(String name) {
    this.name = name;
    this.constructor = new CMethod(name, "", new ArrayList<>(), CKeywords.PUBLIC);
  }

  /**
   * @return class' name
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return class' constructor
   */
  public CMethod getConstructor() {
    return this.constructor;
  }

  /**
   * Add an attribute to this class.
   *
   * @param attribute an attribute
   */
  public void addAttribute(CAttribute attribute) {
    this.attributes.add(attribute);
    String initialValueString = attribute.getInitialValue();
    if (initialValueString != null && !(attribute.isStatic() && attribute.isConst()))
      this.constructor.addBlockStatement(new CCode(attribute.getName() + " = " + initialValueString + ";"));
  }

  /**
   * Add a collection of attributes.
   * 
   * @param attributes collection of attributes to add
   */
  public void addAttributes(Collection<CAttribute> attributes) {
    for (CAttribute attribute : attributes)
      this.addAttribute(attribute);
  }

  /**
   * @return class' attributes
   */
  public List<CAttribute> getAttributes() {
    return this.attributes;
  }
  
  /**
   * Add an enumeration to this class.
   *
   * @param enumeration an enumeration
   */
  public void addEnumerationType(CEnum enumeration){
    this.enumerations.add(enumeration);
  }
  
  /**
   * @return class' enumerations
   */
  public List<CEnum> getEnumerations() {
    return this.enumerations;
  }

  /**
   * Add a method to this class.
   *
   * @param method a method
   */
  public void addMethod(CMethod method) {
    this.methods.add(method);
  }

  /**
   * Add a collection of methods to this class.
   *
   * @param methods a collection of methods
   */
  public void addMethods(Collection<CMethod> methods) {
    this.methods.addAll(methods);
  }

  /**
   * @return class' methods
   */
  protected List<CMethod> getMethods() {
    return this.methods;
  }

  /**
   * Add a header to this class.
   *
   * @param header a header
   */
  public void addHeader(String header) {
    this.headers.add(header);
  }

  /**
   * @return the headers to include for this class
   */
  public List<String> getHeaders() {
    return this.headers;
  }

  public String getDeclarationFileName() {
    return this.getName().replace('.', '_') + ".hh";
  }

  public String getImplementationFileName() {
    return this.getName().replace('.', '_') + ".cpp";
  }

  public String getForwardDeclaration() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(CKeywords.CLASS);
    resultBuilder.append(" ");
    resultBuilder.append(this.getName());
    resultBuilder.append(";");
    return resultBuilder.toString();
  }

  public String getDeclaration() {
    StringBuilder resultBuilder = new StringBuilder();
    String indent = "  ";
    String nextIndent = indent + "  ";

    resultBuilder.append("class ");
    resultBuilder.append(this.name);
    resultBuilder.append(" {\n");

    StringBuilder publicBlock = new StringBuilder();
    StringBuilder protectedBlock = new StringBuilder();
    StringBuilder defaultBlock = new StringBuilder();
    for (CEnum enumeration : this.enumerations) {
      StringBuilder block = enumeration.getVisibility().equals(CKeywords.PUBLIC) ? publicBlock :
                            enumeration.getVisibility().equals(CKeywords.PROTECTED) ? protectedBlock :
                                defaultBlock;
      block.append(nextIndent);
      block.append(enumeration.declaration(nextIndent));
      block.append("\n");
    }
    for (CAttribute attribute : this.getAttributes()) {
      StringBuilder block = attribute.getVisibility().equals(CKeywords.PUBLIC) ? publicBlock :
                            attribute.getVisibility().equals(CKeywords.PROTECTED) ? protectedBlock :
                                defaultBlock;
      block.append(nextIndent);
      block.append(attribute.declaration());
      block.append("\n");
    }
    {
      StringBuilder block = this.constructor.getVisibility().equals(CKeywords.PUBLIC) ? publicBlock :
                            this.constructor.getVisibility().equals(CKeywords.PROTECTED) ? protectedBlock :
                                defaultBlock;
      block.append(nextIndent);
      block.append(this.constructor.declaration());
      block.append("\n");
    }
    for (CMethod method : this.getMethods()) {
      StringBuilder block = method.getVisibility().equals(CKeywords.PUBLIC) ? publicBlock :
                            method.getVisibility().equals(CKeywords.PROTECTED) ? protectedBlock :
                                defaultBlock;
      block.append(nextIndent);
      block.append(method.declaration());
      block.append("\n");
    }
    resultBuilder.append(defaultBlock);
    if (!protectedBlock.toString().equals("")) {
      resultBuilder.append(indent + CKeywords.PROTECTED + ":\n");
      resultBuilder.append(protectedBlock);
    }
    if (!publicBlock.toString().equals("")) {
      resultBuilder.append(indent + CKeywords.PUBLIC + ":\n");
      resultBuilder.append(publicBlock);
    }

    resultBuilder.append("};");

    return resultBuilder.toString();
  }

  public String getDefinitions() {
    StringBuilder resultBuilder = new StringBuilder();
    String sep = "";

    for (CAttribute attribute : this.getAttributes().stream().filter(a -> a.isStatic() && !a.isConst()).collect(toList())) {
      resultBuilder.append(sep);
      resultBuilder.append(attribute.getInitialisation(this.name));
      resultBuilder.append("\n");
      sep = "\n";
    }
      
    {
      resultBuilder.append(sep);
      resultBuilder.append(this.constructor.statement("", this.name));
      resultBuilder.append("\n");
      sep = "\n";
    }

    for (CMethod method : this.getMethods()) {
      resultBuilder.append(sep);
      resultBuilder.append(method.statement("", this.name));
      resultBuilder.append("\n");
      sep = "\n";
    }

    return resultBuilder.toString();
  }

  public String toString() {
    return "Class [" + "Attributes = " + attributes + "]";
  }
}
