package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a single statement in unstructured Java code.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class CCode extends CStatement {
  private String code = "";

  public CCode(String code) {
    this.code = code.trim();
  }

  @Override
  public boolean isEmpty() {
    return super.isEmpty() &&
           this.code.equals("");
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();

    if (!onNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append(this.code);

    return resultBuilder.toString();
  }
}
