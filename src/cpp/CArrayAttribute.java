package cpp;

import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class CArrayAttribute extends CAttribute {
  private int size;

  public CArrayAttribute(String name, String type, List<String> initialValues, String modifiers, int size) {
    super(name, type, modifiers);
    this.getInitialValues().addAll(initialValues);
    this.size = size;
  }  

  public CArrayAttribute(String name, String type, String modifiers,int size) {
    super(name, type, modifiers);
    this.size = size;
  }

  public String declaration() {
    if (this.isStatic())
      return this.getType() + " " + this.getName() + "[" + this.size + "] = " + this.getInitialValues();
    return this.getType() + " " + this.getName() + "[" + this.size + "]";
  }
}
