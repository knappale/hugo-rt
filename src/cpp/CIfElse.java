package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * C++ if-else-statement
 */
@NonNullByDefault
public class CIfElse extends CStatement {
  private String condition;
  private CStatement thenStatement;
  private @Nullable CStatement elseStatement;
  
  /**
   * Create a new conditional from a condition, a then statement, and
   * an optional else statement.
   * 
   * @param condition boolean expression
   * @param thenStatement statement executed if {@code condition} evaluates to {@code true}
   * @param elseStatement statement executed if {@code condition} evaluates to {@code false}
   */
  public CIfElse(String condition, CStatement thenStatement, @Nullable CStatement elseStatement) {
    this.condition = condition;
    this.thenStatement = thenStatement;
    if (elseStatement != null && !elseStatement.isEmpty())
      this.elseStatement = elseStatement;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();

    if (!onNewLine)
      resultBuilder.append("\n");

    resultBuilder.append(indent);
    resultBuilder.append("if (");
    resultBuilder.append(this.condition);
    resultBuilder.append(") ");
    resultBuilder.append(this.thenStatement.statement(indent + "  ", false));

    CStatement elseStatement = this.elseStatement;
    if (elseStatement != null && !elseStatement.isEmpty()) {
      resultBuilder.append("\n");
      resultBuilder.append(indent);
      resultBuilder.append("else ");
      resultBuilder.append(elseStatement.statement(indent + "  ", false));
    }

    return resultBuilder.toString();
  }
}
