package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class CBranch extends CStatement {
  private String guard = "";
  private CStatement statement = new CBlock();

  public CBranch() {
  }

  public CBranch(String guard) {
    this.guard = guard.trim();
  }

  public CBranch(String guard, CBlockStatement blockStatement) {
    this.guard = guard.trim();
    this.statement.asBlock().addBlockStatement(blockStatement);
  }

  @Override
  public CBranch asBranch() {
    return this;
  }

  @Override
  public boolean isEmpty() {
    return super.isEmpty() &&
           "".equals(this.guard) &&
           this.statement.isEmpty();
  }

  public boolean hasEmptyStatement() {
    return this.statement.isEmpty();
  }

  public CBranch addBlockStatement(CBlockStatement blockStatement) {
    this.statement = this.statement.asBlock().addBlockStatement(blockStatement);
    return this;
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    if ("".equals(this.guard))
      return this.statement.statement(indent, onNewLine);

    StringBuilder resultBuilder = new StringBuilder();
    String nextIndent = indent + "  ";

    if (!onNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("if (");
    resultBuilder.append(this.guard);
    resultBuilder.append(") ");
    resultBuilder.append(this.statement.statement(nextIndent, false));

    return resultBuilder.toString();
  }
}
