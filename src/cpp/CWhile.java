package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a while loop.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba </A>
 */
@NonNullByDefault
public class CWhile extends CStatement {
  private String condition;
  private CStatement statement;

  /**
   * Create a new while loop.
   */
  public CWhile(String condition, CStatement statement) {
    this.condition = condition.trim();
    this.statement = statement;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();

    if (!onNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("while (");
    resultBuilder.append(this.condition);
    resultBuilder.append(") ");
    resultBuilder.append(this.statement.statement(indent + "  ", false));

    return resultBuilder.toString();
  }
}
