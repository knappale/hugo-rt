package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a case in a switch.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class CCase {
  private String label;
  private CBlockStatement blockStatement;
  
  public CCase(String label, CBlockStatement blockStatement) {
    this.label = label;
    this.blockStatement = blockStatement;
  }

  boolean isEmpty() {
    return this.blockStatement.isEmpty();
  }

  /**
   * Determine the C++ code of this case.
   * 
   * The general contract is that overriding methods produce a string without
   * a trailing new line ("\n"), i.e., the string when output ends in the middle
   * of a line.
   * 
   * @param indent indentation string
   * @param onNewLine whether the statement occurs in a context on a new (empty) line
   * @param emptyLineAbove whether the statement occurs in a context with at least one empty line above
   * @return C++ code of this block statement
   */
  public String statement(String indent, boolean onNewLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (!onNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("case ");
    resultBuilder.append(this.label);
    resultBuilder.append(": ");
    resultBuilder.append(this.blockStatement.statement(indent + "  ", false));

    return resultBuilder.toString();
  }

  /**
   * Determine the C++ code of this case.
   * 
   * The general contract is that overriding methods produce a string without
   * a trailing new line ("\n"), i.e., the string when output ends in the middle
   * of a line.
   * 
   * @param indent indentation string
   * @param onNewLine whether the statement occurs in a context on a new (empty) line
   * @return C++ code of this block statement
   */
  public String statement(String indent, boolean onNewLine) {
    return this.statement(indent, onNewLine, false);
  }

  /**
   * @return C++ code of this case
   */
  public String statement() {
    return this.statement("", true);
  }
}
