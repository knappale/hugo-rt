package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a local variable declaration.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class CVariableDeclaration extends CBlockStatement {
  private String type = "";
  private String name = "";
  private String value = "";
  private int length;

  /**
   * Create a new local variable declaration.
   * 
   * @param name name of the variable
   * @param type type of the variable
   */
  public CVariableDeclaration(String name, String type) {
    this.name = name.trim();
    this.type = type.trim();
    this.value = "";
  }

  /**
   * Create a new local variable declaration including an initial value
   * expression.
   * 
   * @param name name of the variable
   * @param type type of the variable
   * @param value variable initial value
   */
  public CVariableDeclaration(String name, String type, String value) {
    this.name = name.trim();
    this.type = type.trim();
    this.value = value.trim();
  }

  /**
   * Create a new local array variable declaration including an initial value
   * expression.
   * 
   * @param name name of the variable
   * @param type type of the variable
   * @param value initial value of the variable
   * @param length length of the variable
   */
  public CVariableDeclaration(String name, String type, String value, int length) {
    this.name = name.trim();
    this.type = type.trim();
    this.value = value.trim();
    this.length = length;
  }

  /**
   * @return variable declaration's name
   */
  public String getName() {
    return this.name;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  /**
   * Transform this local variable declaration into an attribute.
   *
   * @return an attribute representing this variable declaration
   */
  public CAttribute getAttribute() {
    return new CAttribute(this.name, this.type, this.value, CKeywords.PRIVATE);
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();

    if (!onNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append(this.type);
    resultBuilder.append(" ");
    resultBuilder.append(this.name);
    if (this.length > 1)
      resultBuilder.append(" ["+length+"]");
    if (!this.value.equals("")) {
      resultBuilder.append(" = ");
      resultBuilder.append(this.value);
    }
    resultBuilder.append(";");

    return resultBuilder.toString();
  }
}
