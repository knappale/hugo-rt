package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class CArrayParameter extends CParameter {
  private int size;

  public CArrayParameter(String name, String type, int size) {
    super(name, type);
    this.size = size;
  }
  
  public String declaration() {
    return this.getType() + " " + this.getName() + "[" + this.size + "]";
  }
}
