package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class CBreak extends CStatement {
  public CBreak() {
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();

    if (!onNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("break;");

    return resultBuilder.toString();
  }
}
