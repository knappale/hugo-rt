package cpp;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


@NonNullByDefault
public abstract class CBlockStatement {
  private List<String> comments = new ArrayList<>();
  private List<String> labels = new ArrayList<>();

  public CBlockStatement addComment(String comment) {
    this.comments.add(comment);
    return this;
  }

  public CBlockStatement addLabel(String label) {
    this.labels.add(label);
    return this;
  }

  /**
   * @return a statement from this block statement
   */
  public CStatement asStatement() {
    return new CBlock(this);
  }

  /**
   * @return this block statement as a branch
   */
  public CBranch asBranch() {
    return new CBranch().addBlockStatement(this);
  }

  /**
   * @return whether this block statement is empty
   */
  public boolean isEmpty() {
    return this.comments.isEmpty() && this.labels.isEmpty();
  }

  /**
   * Determine the C++ code of this block statement.
   * 
   * The general contract is that overriding methods produce a string without
   * a trailing new line ("\n"), i.e., the string when output ends in the middle
   * of a line.
   * 
   * @param indent indentation string
   * @param onNewLine whether the statement occurs in a context on a new (empty) line
   * @return C++ code of this block statement
   */
  public abstract String statement(String indent, boolean onNewLine);

  /**
   * Determine the C++ code of this block statement.
   * 
   * The general contract is that overriding methods produce a string without
   * a trailing new line ("\n"), i.e., the string when output ends in the middle
   * of a line.
   * 
   * @param indent indentation string
   * @param onNewLine whether the statement occurs in a context on a new (empty) line
   * @param emptyLineAbove whether the statement occurs in a context with at least one empty line above
   * @return C++ code of this block statement
   */
  public String blockStatement(String indent, boolean onNewLine, boolean emptyLineAbove) {
    if (this.comments.isEmpty() && this.labels.isEmpty())
      return this.statement(indent, onNewLine);

    StringBuilder resultBuilder = new StringBuilder();
    if (!onNewLine)
      resultBuilder.append("\n");
    if (!emptyLineAbove)
      resultBuilder.append("\n");
    if (!this.comments.isEmpty()) {
      resultBuilder.append(Formatter.separated(this.comments, comment -> indent + "// " + comment, "\n"));
      resultBuilder.append("\n");
    }
    if (!this.labels.isEmpty()) {
      resultBuilder.append(Formatter.separated(this.labels, label -> indent + label + ":", "\n"));
      resultBuilder.append("\n");
    }
    resultBuilder.append(this.statement(indent, true));
    return resultBuilder.toString();
  }

  /**
   * @return C++ code of this block statement
   */
  public String statement() {
    return this.statement("", true);
  }
}
