package cpp;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import static java.util.stream.Collectors.toList;


@NonNullByDefault
public class CChoice extends CBlockStatement {
  private List<CBranch> branches = new ArrayList<>();

  @Override
  public boolean isEmpty() {
    return super.isEmpty() &&
           this.branches.stream().allMatch(CBranch::isEmpty);
  }

  /**
   * Add branch to this choice.
   *
   * @param branch branch, i.e., a conditional statement
   */
  public void addBranch(CBranch branch) {
    this.branches.add(branch);
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();

    String sep = "";
    String nextIndent = indent;
    boolean nextOnNewLine = onNewLine;
    for (CBranch branch : this.branches.stream().filter(b -> !b.isEmpty()).collect(toList())) {
      resultBuilder.append(sep);
      resultBuilder.append(branch.statement(nextIndent, nextOnNewLine));
      sep = "\n" + nextIndent + CKeywords.ELSE + " ";
      nextIndent = nextIndent + "  ";
      nextOnNewLine = false;
    }

    return resultBuilder.toString();
  }
}
