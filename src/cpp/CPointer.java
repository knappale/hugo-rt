package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class CPointer extends CAttribute {
  public CPointer(String name, String type, String modifiers) {
    super(name, type, modifiers);
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.getType());
    resultBuilder.append("* ");
    resultBuilder.append(this.getName());
    resultBuilder.append(" = ");
    resultBuilder.append(CKeywords.NULLPTR);
    resultBuilder.append(";");
    return resultBuilder.toString();
  }
}
