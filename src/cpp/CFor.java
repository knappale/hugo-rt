package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class CFor extends CStatement {
  private String initialisation;
  private String guard;
  private String increment;
  private CStatement statement = new CBlock();

  public CFor(String init, String guard, String increment) {
    this.initialisation = init;
    this.guard = guard;
    this.increment = increment;
  }

  public CFor addBlockStatement(CBlockStatement blockStatement) {
    this.statement = this.statement.asBlock().addBlockStatement(blockStatement);
    return this;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextIndent = indent + "  ";

    if (!onNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append(CKeywords.FOR);
    resultBuilder.append(" (");
    resultBuilder.append(initialisation);
    resultBuilder.append("; ");
    resultBuilder.append(guard);
    resultBuilder.append("; ");
    resultBuilder.append(increment);
    resultBuilder.append(") ");
    resultBuilder.append(this.statement.statement(nextIndent, false));

    return resultBuilder.toString();
  }

}
