package cpp;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;


@NonNullByDefault
public class CAttribute {
  private String name;
  private String type;
  private List<String> initialValues = new ArrayList<>();
  private String modifiers;

  public CAttribute(String name, String type, String modifiers) {
    this.name = name.trim();
    this.type = type.trim();
    this.modifiers = modifiers.trim();
  }

  public CAttribute(String name, String type, String initialValue, String modifiers) {
    this.name = name.trim();
    this.type = type.trim();
    initialValue = initialValue.trim();
    if (!initialValue.equals(""))
      this.initialValues.add(initialValue);
    this.modifiers = modifiers;
  }

  public String getName() {
    return this.name;
  }

  public String getType() {
    return this.type;
  }

  public @Nullable String getInitialValue() {
    if (this.initialValues.size() > 0)
      return this.initialValues.get(0);
    else
      return null;
  }

  public String getVisibility() {
    if (this.modifiers.contains(CKeywords.PUBLIC))
      return CKeywords.PUBLIC;
    if (this.modifiers.contains(CKeywords.PROTECTED))
      return CKeywords.PROTECTED;
    return CKeywords.PRIVATE;
  }

  public boolean isStatic() {
    return this.modifiers.contains(CKeywords.STATIC);
  }

  public boolean isConst() {
    return this.modifiers.contains(CKeywords.CONST);
  }

  public String getInitialisation(String className) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.type);
    resultBuilder.append(" ");
    if (!"".equals(className)) {
      resultBuilder.append(className);
      resultBuilder.append("::");
    }
    resultBuilder.append(this.name);
    if (!this.initialValues.isEmpty()) {
      resultBuilder.append(" = ");
      resultBuilder.append(Formatter.setOrSingleton(this.initialValues));
    }
    resultBuilder.append(";");
    return resultBuilder.toString();
  }
    
  public List<String> getInitialValues() {
    return this.initialValues; 
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    if (this.isStatic()) {
      resultBuilder.append(CKeywords.STATIC);
      resultBuilder.append(" ");
    }
    if (this.isConst()) {
      resultBuilder.append(CKeywords.CONST);
      resultBuilder.append(" ");
    }
    resultBuilder.append(this.getType());
    resultBuilder.append(" ");
    resultBuilder.append(this.getName());
    if (this.isStatic() && this.isConst()) {
      resultBuilder.append(" = ");
      resultBuilder.append(Formatter.setOrSingleton(this.initialValues));
    }
    resultBuilder.append(";");
    return resultBuilder.toString();
  }
}
