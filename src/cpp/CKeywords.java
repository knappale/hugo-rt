package cpp;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;


/**
 * Java code keywords.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class CKeywords {
  // Reserved keywords
  public static final @NonNull String ASM = "asm";
  public static final @NonNull String AUTO = "auto";
  public static final @NonNull String BOOL = "bool";
  public static final @NonNull String BREAK = "break";
  public static final @NonNull String CASE = "case";
  public static final @NonNull String CATCH = "catch";
  public static final @NonNull String CHAR = "char";
  public static final @NonNull String CLASS = "class";
  public static final @NonNull String CONST = "const";
  public static final @NonNull String CONST_CAST = "const_cast";
  public static final @NonNull String CONTINUE = "continue";
  public static final @NonNull String DEFAULT = "default";
  public static final @NonNull String DELETE = "delete";
  public static final @NonNull String DO = "do";
  public static final @NonNull String DOUBLE = "double";
  public static final @NonNull String DYNAMIC_CAST = "dynamic_cast";
  public static final @NonNull String ELSE = "else";
  public static final @NonNull String ENUM = "enum";
  public static final @NonNull String EXPLICIT = "explicit";
  public static final @NonNull String EXTERN = "extern";
  public static final @NonNull String FALSE = "false";
  public static final @NonNull String FLOAT = "float";
  public static final @NonNull String FOR = "for";
  public static final @NonNull String FRIEND = "friend";
  public static final @NonNull String GOTO = "goto";
  public static final @NonNull String IF = "if";
  public static final @NonNull String INLINE = "inline";
  public static final @NonNull String INT = "int";
  public static final @NonNull String LONG = "long";
  public static final @NonNull String MUTABLE = "mutable";
  public static final @NonNull String NAMESPACE = "namespace";
  public static final @NonNull String NEW = "new";
  public static final @NonNull String NULLPTR = "nullptr";
  public static final @NonNull String OPERATOR = "operator";
  public static final @NonNull String PRIVATE = "private";
  public static final @NonNull String PROTECTED = "protected";
  public static final @NonNull String PUBLIC = "public";
  public static final @NonNull String REGISTER = "register";
  public static final @NonNull String REINTERPRET_CAST = "reinterpret_cast";
  public static final @NonNull String RETURN = "return";
  public static final @NonNull String SHORT = "short";
  public static final @NonNull String SIGNED = "signed";
  public static final @NonNull String SIZEOF = "sizeof";
  public static final @NonNull String STATIC = "static";
  public static final @NonNull String STATIC_CAST = "static_cast";
  public static final @NonNull String STRUCT = "struct";
  public static final @NonNull String SWITCH = "switch";
  public static final @NonNull String TEMPLATE = "template";
  public static final @NonNull String THIS = "this";
  public static final @NonNull String THROW = "throw";
  public static final @NonNull String TRUE = "true";
  public static final @NonNull String TRY = "try";
  public static final @NonNull String TYPEDEF = "typedef";
  public static final @NonNull String TYPEID = "typeid";
  public static final @NonNull String TYPENAME = "typename";
  public static final @NonNull String UNION = "union";
  public static final @NonNull String UNSIGNED = "unsigned";
  public static final @NonNull String USING = "using";
  public static final @NonNull String VIRTUAL = "virtual";
  public static final @NonNull String VOID = "void";
  public static final @NonNull String VOLATILE = "volatile";
  public static final @NonNull String WCHAR_T = "wchar_t";
  public static final @NonNull String STRING = "string";
  public static final @NonNull String WHILE ="while";

  public static final @NonNull String EVENTQUEUE = "eq";
  public static final @NonNull String JUMPVARIABLE = "jumplabel";
  
  private static @NonNull String @NonNull[] keywords = {
    ASM, AUTO, BOOL, BREAK, CASE, CATCH, CHAR, CLASS, CONST, CONST_CAST, CONTINUE, 
    DEFAULT, DELETE, DO, DOUBLE, DYNAMIC_CAST, ELSE, ENUM, EXPLICIT, EXTERN, 
    FALSE, FLOAT, FOR, FRIEND, GOTO, IF, INLINE, INT, LONG, MUTABLE, NAMESPACE, 
    NEW, NULLPTR, OPERATOR, PRIVATE, PROTECTED, PUBLIC, REGISTER, REINTERPRET_CAST, 
    RETURN, SHORT, SIGNED, SIZEOF, STATIC, STATIC_CAST, STRUCT, SWITCH, TEMPLATE,
    THIS, THROW, TRUE, TRY, TYPEDEF, TYPEID, TYPENAME, UNION, UNSIGNED, USING, 
    VIRTUAL, VOID, VOLATILE, WCHAR_T, STRING, WHILE,
    EVENTQUEUE, JUMPVARIABLE };

  public static boolean isKeyword(String word) {
    for (int i = 0; i < keywords.length; i++) {
      if (keywords[i].equals(word))
        return true;
    }
    return false;
  }

  public static @NonNull Set<@NonNull String> getKeywords() {
    Set<@NonNull String> result = new HashSet<>();
    for (int i = 0; i < keywords.length; i++)
      result.add(keywords[i]);
    return result;
  }
}
