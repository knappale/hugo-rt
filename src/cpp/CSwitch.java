package cpp;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Representation of a switch.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class CSwitch extends CStatement {
  private String expression;
  private List<CCase> cases = new ArrayList<>();
  private @Nullable CBlockStatement defaultBlockStatement;

  /**
   * Create a new switch.
   */
  public CSwitch(String expression, List<CCase> cases, @Nullable CBlockStatement defaultBlockStatement) {
    this.expression = expression;
    this.cases.addAll(cases);
    this.defaultBlockStatement = defaultBlockStatement;   
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();

    if (!onNewLine)
      resultBuilder.append("\n");

    resultBuilder.append(indent);
    resultBuilder.append("switch (");
    resultBuilder.append(expression);
    resultBuilder.append(") {\n");
    indent = indent + "  ";

    String separator = "";
    for (CCase aCase : this.cases) {
      resultBuilder.append(separator);
      resultBuilder.append(aCase.statement(indent, true));
      separator = "\n";
    }

    CBlockStatement defaultBlockStatement = this.defaultBlockStatement;
    if (defaultBlockStatement != null) {
      resultBuilder.append(separator);
      resultBuilder.append("default: ");
      resultBuilder.append(defaultBlockStatement.statement(indent, false));
    }

    indent = indent.substring(0, indent.length()-2);
    resultBuilder.append("\n");
    resultBuilder.append(indent);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
