package cpp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import static java.util.stream.Collectors.toList;


@NonNullByDefault
public class CSystem {
  private Set<CClass> classes = new HashSet<>();
  private List<CVariableDeclaration> variables = new ArrayList<>();
  private List<CMethod> methods = new ArrayList<>();
  
  public CSystem() {
  }
  
  public void addClass(CClass cclass) {
    this.classes.add(cclass);
  }
  
  public Set<CClass> getCClasses() {
    return this.classes;
  }
  
  public List<CVariableDeclaration> getVariables() {
    return this.variables;
  }
  
  public void addVariable(CVariableDeclaration variable) {
    this.variables.add(variable);
  }

  public List<CMethod> getMethods() {
    return this.methods;
  }
  
  public void addMethod(CMethod m) {
    this.methods.add(m);
  }

  public String getHeaderFile(CClass cClass, String includePath) {
    StringBuilder resultBuilder = new StringBuilder();
    String declaredMacroName = cClass.getDeclarationFileName().toUpperCase().replace('.', '_');
    List<CClass> otherClasses = this.getCClasses().stream().filter(cc -> !cClass.getName().equals(cc.getName())).collect(toList());

    resultBuilder.append("#ifndef ");
    resultBuilder.append(declaredMacroName);
    resultBuilder.append("\n");
    resultBuilder.append("#define ");
    resultBuilder.append(declaredMacroName);
    resultBuilder.append("\n\n");

    if (!cClass.getHeaders().isEmpty()) {
      for (String header : cClass.getHeaders()) {
        resultBuilder.append("#include \"");
        resultBuilder.append(includePath);
        resultBuilder.append(header);
        resultBuilder.append(".hh\"");
        resultBuilder.append("\n");
      }
      resultBuilder.append("\n");
    }

    // Inclusions
    if (!otherClasses.isEmpty()) {
      for (CClass otherClass : otherClasses) {
        resultBuilder.append("#include \"");
        resultBuilder.append(includePath);
        resultBuilder.append(otherClass.getDeclarationFileName());
        resultBuilder.append("\"");
        resultBuilder.append("\n");
      }
      resultBuilder.append("\n");
    }

    // Forward declarations
    if (!otherClasses.isEmpty()) {
      for (CClass otherClass : otherClasses) {
        resultBuilder.append(otherClass.getForwardDeclaration());
        resultBuilder.append("\n");
      }
      resultBuilder.append("\n");
    }

    // Declaration proper
    resultBuilder.append(cClass.getDeclaration());
    resultBuilder.append("\n");

    resultBuilder.append("\n#endif /*");
    resultBuilder.append(declaredMacroName);
    resultBuilder.append("*/\n");
    return resultBuilder.toString();
  }

  public String getSystemFile(String includePath) {
    var resultBuilder = new StringBuilder();
    for (var cClass : this.getCClasses()) {
      resultBuilder.append("#include \"" + includePath);
      resultBuilder.append(cClass.getDeclarationFileName());
      resultBuilder.append("\"");
      resultBuilder.append("\n");
    }

    if (!this.getVariables().isEmpty() || !this.getMethods().isEmpty())
      resultBuilder.append("\n\n");

    for (var cVariable : this.getVariables()) {
      resultBuilder.append(cVariable.statement());
      resultBuilder.append("\n");
    }

    if (!this.getVariables().isEmpty())
      resultBuilder.append("\n");

    var sep = "";
    for (var cMethod : this.getMethods()) {
      resultBuilder.append(sep);
      resultBuilder.append(cMethod.statement("", ""));
      sep = "\n\n";
    }

    if (!this.getMethods().isEmpty())
      resultBuilder.append("\n");

    return resultBuilder.toString();
  }
}
