package cpp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class CBlock extends CStatement {
  private List<CVariableDeclaration> variableDeclarations = new ArrayList<>();
  private List<CBlockStatement> blockStatements = new ArrayList<>();
  
  public CBlock(@NonNull CBlockStatement... blockStatements) {
    this.blockStatements = new ArrayList<>(Arrays.asList(blockStatements));
  }

  public static CBlock merge(CBlock block1, CBlock block2) {
    CBlock result = new CBlock();
    result.variableDeclarations.addAll(block1.variableDeclarations);
    result.blockStatements.addAll(block1.blockStatements);
    result.variableDeclarations.addAll(block2.variableDeclarations);
    result.blockStatements.addAll(block2.blockStatements);
    return result;
  }

  public void addVariables(Collection<CVariableDeclaration> declarations) {
    this.variableDeclarations.addAll(declarations);
  }

  public void addVariable(CVariableDeclaration var) {
    this.variableDeclarations.add(var);
  }
  
  public CBlock addBlockStatement(CBlockStatement blockStatement) {
    this.blockStatements.add(blockStatement);
    return this;
  }
  
  @Override
  public boolean isEmpty() {
    return super.isEmpty() &&
           this.variableDeclarations.stream().allMatch(CVariableDeclaration::isEmpty) &&
           this.blockStatements.stream().allMatch(CBlockStatement::isEmpty);
  }

  @Override
  public CBlock asBlock() {
    return this;
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();
    String indent_1 = (indent.length() >= 2 ? indent.substring(0, indent.length()-2) : "");
    String indent1 = indent + "  ";
    
    if (onNewLine)
      resultBuilder.append(indent);
    else {
      indent1 = indent;
      indent = indent_1;
    }
    
    boolean nextEmptyLineAbove = false;
    resultBuilder.append("{\n");
    if (!this.variableDeclarations.isEmpty()) {
      for (CVariableDeclaration variableDeclaration : this.variableDeclarations) {
        resultBuilder.append(variableDeclaration.statement(indent1, true));
        resultBuilder.append("\n");
      }
      resultBuilder.append("\n");
      nextEmptyLineAbove = true;
    }
    boolean nextOnNewLine = true;
    for (CBlockStatement blockStatement : this.blockStatements) {
      resultBuilder.append(blockStatement.blockStatement(indent1, nextOnNewLine, nextEmptyLineAbove));
      nextOnNewLine = false;
      nextEmptyLineAbove = false;
    }
    if (!nextOnNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
