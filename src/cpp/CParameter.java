package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class CParameter {
  private String name;
  private String type;

  public CParameter(String name, String type) {
    this.name = name.trim();
    this.type = type.trim();
  }

  public String getName() {
    return this.name;
  }

  public String getType() {
    return this.type;
  }
  
  public String declaration() {
    return this.type + " " + this.name;
  }
}
