package cpp;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public abstract class CStatement extends CBlockStatement {
  public CBlock asBlock() {
    return new CBlock(this);
  }

  @Override
  public CStatement asStatement() {
    return this;
  }
}
