package cpp;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static util.Objects.assertNonNull;

import org.eclipse.jdt.annotation.NonNullByDefault;


@NonNullByDefault
public class CEnum  {
  private String name;
  private String visibility;
  private Map<String, Integer> values = new HashMap<>();
  
  public CEnum(String name, String visibility){
    this.name = name;
    this.visibility = visibility;
  }
 
  public CEnum(String name, String visibility, Collection<String> literals){
    this(name, visibility);
    for (String literal : literals)
      this.addLiteral(literal);
  }
 
  public CEnum(String name, String visibility, Map<String, Integer> values) {
    this(name, visibility);
    this.values = values;
  }

  public void addLiteral(String literal, int value) {
    this.values.put(literal, value);
  }

  public void addLiteral(String literal) {
    this.values.put(literal, 0);
  }

  public Set<String> getLiterals() {
    return this.values.keySet();
  }

  public String getName() {
    return this.name;
  }

  public String getVisibility() {
    return this.visibility;
  }

  public String declaration(String indent) {
    String nextIndent = indent + "  ";
    StringBuilder resultBuilder = new StringBuilder(CKeywords.ENUM);
    resultBuilder.append(" ");
    resultBuilder.append(this.name);
    resultBuilder.append(" {\n");
    int nextNumber = assertNonNull(this.values.values().stream().max(Integer::compare).orElse(0)) + 1;
    String sep = nextIndent;
    for (String literal : this.values.keySet()) {
      resultBuilder.append(sep);
      resultBuilder.append(literal);
      resultBuilder.append(" = ");
      Integer value = this.values.get(literal);
      if (value != 0)
        resultBuilder.append(value);
      else
        resultBuilder.append(nextNumber++);
      sep = ",\n" + nextIndent;
    }
    resultBuilder.append("\n");
    resultBuilder.append(indent);
    return resultBuilder.append("};").toString();
  }
}
