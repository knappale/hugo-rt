package cpp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import static java.util.stream.Collectors.toList;


@NonNullByDefault
public class CSequence extends CBlockStatement {
  private List<CBlockStatement> blockStatements = new ArrayList<>();

  public CSequence(@NonNull CBlockStatement... blockStatements) {
    this.blockStatements = new ArrayList<>(Arrays.asList(blockStatements));
  }

  public void addBlockStatement(CBlockStatement blockStatement) {
    this.blockStatements.add(blockStatement);
  }

  public List<CStatement> getStatements() {
    return this.blockStatements.stream().map(blockStatement -> blockStatement.asStatement()).collect(toList());
  }

  @Override
  public boolean isEmpty() {
    return super.isEmpty() &&
           this.blockStatements.stream().allMatch(CBlockStatement::isEmpty);
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();
    
    for (CBlockStatement blockStatement : this.blockStatements.stream().filter(b -> !b.isEmpty()).collect(toList())) {
      resultBuilder.append(blockStatement.blockStatement(indent, onNewLine, false));
      onNewLine = false;
    }

    return resultBuilder.toString();
  }
}
