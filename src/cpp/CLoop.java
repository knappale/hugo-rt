package cpp;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import static java.util.stream.Collectors.toList;


@NonNullByDefault
public class CLoop extends CBlockStatement {
  private List<CBranch> branches = new ArrayList<>();

  @Override
  public boolean isEmpty() {
    return super.isEmpty() &&
           this.branches.stream().allMatch(CBranch::isEmpty);
  }

  /**
   * Add branch to this loop.
   *
   * @param branch branch, i.e., a conditional statement
   */
  public void addBranch(CBranch branch) {
    this.branches.add(branch);
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextIndent = indent + "  ";

    if (!onNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append(CKeywords.WHILE);
    resultBuilder.append(" (");
    resultBuilder.append(CKeywords.TRUE);
    resultBuilder.append(") ");
    List<CBranch> branches = this.branches.stream().filter(b -> !b.isEmpty()).collect(toList());
    if (branches.size() == 1)
      resultBuilder.append(branches.iterator().next().statement(nextIndent, false));
    else {
      String sep = "{";
      for (CBranch branch : branches) {
        resultBuilder.append(sep);
        resultBuilder.append(branch.statement(nextIndent, false));
        sep = "\n" + nextIndent + CKeywords.ELSE + " ";
        nextIndent = nextIndent + "  ";
      }
      resultBuilder.append("\n");
      resultBuilder.append(indent);
      resultBuilder.append("}");
    }

    return resultBuilder.toString();
  }
}
