package cpp;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * C++ if-statement
 */
@NonNullByDefault
public class CIfs extends CStatement {
  private List<CBranch> branches = new ArrayList<>();
  private @Nullable CStatement elseStatement;
  
  /**
   * Create a new (iterated) conditional with an optional else branch.
   * 
   * @param branches a list of branches
   * @param elseBranch an else branch
   */
  public CIfs(List<CBranch> branches, @Nullable CBranch elseBranch) {
    this.branches.addAll(branches);
    if (elseBranch != null && !elseBranch.isEmpty())
      this.elseStatement = elseBranch;
  }

  /**
   * Create a new conditional from a condition, a then statement, and
   * an optional else statement.
   * 
   * @param condition boolean expression
   * @param thenBlockStatement statement executed if {@code condition} evaluates to {@code true}
   * @param elseStatement statement executed if {@code condition} evaluates to {@code false}
   */
  public CIfs(String condition, CBlockStatement thenBlockStatement, @Nullable CStatement elseStatement) {
    this.branches.add(new CBranch(condition, thenBlockStatement));
    if (elseStatement != null && !elseStatement.isEmpty())
      this.elseStatement = elseStatement;
  }

  /**
   * Create a new conditional from a then branch and an optional else statement.
   *
   * @param thenBranch then branch, containing the if-condition
   * @param elseStatement statement executed if condition of {@code thenBranch} evaluates to {@code false}
   */
  public CIfs(CBranch thenBranch, @Nullable CStatement elseStatement) {
    this.branches.add(thenBranch);
    if (elseStatement != null && !elseStatement.isEmpty())
      this.elseStatement = elseStatement;
  }

  @Override
  public boolean isEmpty() {
    CStatement elseStatement = this.elseStatement;
    return this.branches.stream().allMatch(CBranch::isEmpty) && (elseStatement == null || elseStatement.isEmpty());
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();

    if (!onNewLine)
      resultBuilder.append("\n");

    String separator = "";
    for (CBranch branch : this.branches) {
      resultBuilder.append(separator);
      resultBuilder.append(branch.statement(indent, true));
      separator = " else \n";
    }
    CStatement elseStatement = this.elseStatement;
    if (elseStatement != null && !elseStatement.isEmpty()) {
      resultBuilder.append("\n");
      resultBuilder.append(indent);
      resultBuilder.append("else ");
      resultBuilder.append(elseStatement.statement(indent + "  ", false));
    }

    return resultBuilder.toString();
  }
}
