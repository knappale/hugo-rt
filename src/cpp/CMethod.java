package cpp;

import java.util.Collection;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * C++ method
 */
@NonNullByDefault
public class CMethod {
  private String name;
  private String type;
  private List<CParameter> parameters;
  private String modifiers;
  private CBlock body;
 
  public CMethod(String name, String type, List<CParameter> parameters, String modifiers) {
    this.name = name.trim();
    this.type = type.trim();
    this.parameters = parameters;
    this.modifiers = modifiers.trim();
    this.body = new CBlock();
  }

  public String getName() {
    return this.name;
  }

  public List<CParameter> getParameters() {
    return this.parameters;
  }

  public CBlock getBody() {
    return this.body;
  }

  public CMethod addVariables(Collection<CVariableDeclaration> variableDeclarations) {
    this.body.addVariables(variableDeclarations);
    return this;
  }

  public CMethod addVariable(CVariableDeclaration variableDeclaration) {
    this.body.addVariable(variableDeclaration);
    return this;
  }
  
  public CMethod addBlockStatement(CBlockStatement blockStatement) {
    this.body.addBlockStatement(blockStatement);
    return this;
  }

  public String getVisibility() {
    if (this.modifiers.contains(CKeywords.PUBLIC))
      return CKeywords.PUBLIC;
    if (this.modifiers.contains(CKeywords.PROTECTED))
      return CKeywords.PROTECTED;
    return CKeywords.PRIVATE;
  }

  public boolean isStatic() {
    return this.modifiers.contains(CKeywords.STATIC);
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    if (this.isStatic()) {
      resultBuilder.append(CKeywords.STATIC);
      resultBuilder.append(" ");
    }
    if (!"".equals(this.type)) {
      resultBuilder.append(this.type);
      resultBuilder.append(" ");
    }
    resultBuilder.append(this.name);
    resultBuilder.append("(");
    resultBuilder.append(Formatter.separated(this.parameters, parameter -> parameter.declaration(), ", "));
    return resultBuilder.append(");").toString();
  }

  public String statement(String indent, String className) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextIndent = indent + "  ";

    if (!"".equals(this.type)) {
      resultBuilder.append(this.type);
      resultBuilder.append(" ");
    }
    if (!className.equals("")) {
      resultBuilder.append(className);
      resultBuilder.append("::");
    }
    resultBuilder.append(this.name);
    resultBuilder.append("(");
    resultBuilder.append(Formatter.separated(this.parameters, parameter -> parameter.declaration(), ", "));
    resultBuilder.append(") ");
    resultBuilder.append(this.body.statement(nextIndent, false));

    return resultBuilder.toString();
  }
}
