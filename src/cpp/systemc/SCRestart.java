package cpp.systemc;

import cpp.CStatement;


public class SCRestart extends CStatement {
  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();

    if (!onNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("// Restart");

    return resultBuilder.toString();
  }
}
