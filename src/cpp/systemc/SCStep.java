package cpp.systemc;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import cpp.CBlockStatement;
import cpp.CKeywords;
import cpp.CStatement;
import util.Formatter;


@NonNullByDefault
public class SCStep extends CBlockStatement {
  private List<String> jumps = new ArrayList<>();
  private String target = "";
  private String guard = "";
  private List<CStatement> statements = new ArrayList<>();
  
  public SCStep() {
  }
  
  public void addStatement(CStatement s) {
    this.statements.add(s);
  }

  public List<CStatement> getStatements() {
    return this.statements;
  }

  public void setGuard(String guard) {
    this.guard = guard;
  }

  public String getTarget() {
    return this.target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public void addJump(String label) {
    this.jumps.add(label);
  }

  public List<String> getJumps() {
    return this.jumps;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public String statement(String indent, boolean onNewLine) {
    StringBuilder resultBuilder = new StringBuilder();
    String nextIndent = indent + "  ";

    if (!onNewLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("if ((");
    resultBuilder.append(Formatter.separated(this.jumps, jump -> CKeywords.JUMPVARIABLE + " == " + jump, " || "));
    resultBuilder.append(")");
    if (!"".equals(this.guard)) {
      resultBuilder.append(" && ");
      resultBuilder.append(this.guard);
    }
    resultBuilder.append(") {\n");
    for (CStatement statement : this.statements) {
      resultBuilder.append(statement.statement(nextIndent, true));
      resultBuilder.append("\n");
    }
    resultBuilder.append(nextIndent);
    resultBuilder.append(CKeywords.JUMPVARIABLE);
    resultBuilder.append(" = ");
    if ("".equals(this.target))
      resultBuilder.append("-1");
    else
      resultBuilder.append(this.target);
    resultBuilder.append(indent);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
