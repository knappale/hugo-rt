package cpp.systemc;

import java.util.ArrayList;

import org.eclipse.jdt.annotation.NonNullByDefault;

import cpp.CBlock;
import cpp.CBlockStatement;
import cpp.CMethod;


@NonNullByDefault
public class SCConstructor extends CMethod {
  private CBlock init;

  public SCConstructor(String name) {
    super(name, "", new ArrayList<>(), "");
    this.init = new CBlock();
  }
  
  public void addInitBlockStatement(CBlockStatement s) {
    this.init.addBlockStatement(s);
  }
  
  public CBlock getInitBlock() {
    return this.init;
  }
  
  public String declaration(String indent) {
    StringBuilder result = new StringBuilder();
    result.append(indent);
    result.append("SC_CTOR(");
    result.append(this.getName());
    result.append(")");
    result.append(CBlock.merge(this.init, this.getBody()).statement(indent, false));
    return result.toString();
  }
}
