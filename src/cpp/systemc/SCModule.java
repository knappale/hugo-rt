package cpp.systemc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNullByDefault;

import cpp.CAttribute;
import cpp.CClass;
import cpp.CEnum;
import cpp.CMethod;


@NonNullByDefault
public class SCModule extends CClass {
  private List<SCConstructor> constructors;
  private List<CMethod> globalMethods = new ArrayList<>();
  private Map<String, Integer> preprocessorDefinitions = new HashMap<>();

  public SCModule(String name) {
    super(name);
    constructors = new ArrayList<>();
  }

  public List<SCConstructor> getConstructors() {
    return this.constructors;
  }

  public void addConstructor(SCConstructor constr) {
    this.constructors.add(constr);
  }

  /**
   * Add a method as global to this class.
   *
   * @param method a method
   */
  public void addGlobalMethod(CMethod method) {
    this.globalMethods.add(method);
  }
  
  public List<CMethod> getGlobalMethods() {
    return this.globalMethods;
  }

  /**
   * Add a preprocessor definition to this module.
   *
   * @param name the name of the preprocessor definition
   * @param value the integer value of the preprocessor definition
   */
  public void addPreprocessorDefinition (String name, int value) {
    this.preprocessorDefinitions.put(name,value);
  }
  
  /**
   * @return the preprocessor definitions for this module
   */
  public Map<String, Integer> getPreprocessorDefinitions() {
    return this.preprocessorDefinitions;
  }

  public String getHeaderFile() {
    StringBuilder resultBuilder = new StringBuilder();

    String indent="";
    for (String key : this.getPreprocessorDefinitions().keySet())
      resultBuilder.append("#define " + key + " " + getPreprocessorDefinitions().get(key) + "\n");

    resultBuilder.append(indent+"SC_MODULE("+getName()+"){\n");
    indent = "  ";
    for (CEnum e : this.getEnumerations())
      resultBuilder.append(indent + e.declaration(indent) + "\n");
    for (CAttribute a : this.getAttributes())
      resultBuilder.append(indent + a.declaration() + "\n");
    for (CMethod m : this.getMethods())
      resultBuilder.append(indent + m.declaration() + "\n");
    resultBuilder.append(getConstructors().get(0).declaration(indent));
    indent = "";
    resultBuilder.append(indent + "};\n");

    return resultBuilder.toString();
  }
  
  public String getDefinitions() {
    StringBuilder resultBuilder = new StringBuilder();

    String indent = "";
    for (CAttribute a : getAttributes())
      if (a.isStatic())
        resultBuilder.append(a.getInitialisation(getName()) + "\n\n");
    for (CMethod m : getMethods())
      resultBuilder.append(indent + m.statement(indent, this.getName()) + "\n");
    for (CMethod m : getGlobalMethods())
      resultBuilder.append(indent + m.statement(indent, ""));

    return resultBuilder.toString();
  }

  public String toString() {
    return this.getHeaderFile() + "\n" + this.getDefinitions();
  }
}
