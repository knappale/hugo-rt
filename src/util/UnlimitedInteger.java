package util;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Unlimited integer numbers (including negative and positive infinity).
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @since 2016-06-03
 */
@NonNullByDefault
public class UnlimitedInteger {
  private boolean negInfty;
  private int integer;
  private boolean posInfty;

  private static UnlimitedInteger NEGINFTY = new UnlimitedInteger(true, 0, false);
  private static UnlimitedInteger POSINFTY = new UnlimitedInteger(false, 0, true);

  /**
   * Create a new unlimited integer.
   *
   * @param negInfty whether the new unlimited integer is the negative infinity
   * @param integer the integer number underlying the new unlimited integer number
   * @param posInfty whether the new unlimited integer is the positive infinity
   */
  private UnlimitedInteger(boolean negInfty, int integer, boolean posInfty) {
    this.negInfty = negInfty;
    this.integer = integer;
    this.posInfty = posInfty;
  }

  /**
   * @return an unlimited integer number representing negative infinity
   */
  public static UnlimitedInteger negInfty() {
    return NEGINFTY;
  }

  /**
   * @param integer an integer number
   * @return an unlimited natural number representing {@code integer}
   */
  public static UnlimitedInteger integer(int integer) {
    return new UnlimitedInteger(false, integer, false);
  }

  /**
   * @return an unlimited integer number representing positive infinity
   */
  public static UnlimitedInteger posInfty() {
    return POSINFTY;
  }

  /**
   * @return whether this unlimited integer number represents infinity
   */
  public boolean isInfty() {
    return this.negInfty || this.posInfty;
  }

  /**
   * @return whether this unlimited integer number represents negative infinity
   */
  public boolean isNegInfty() {
    return this.negInfty;
  }

  /**
   * @return whether this unlimited integer number represents positive infinity
   */
  public boolean isPosInfty() {
    return this.negInfty;
  }

  /**
   * @return the unlimited integer number's underlying integer number
   */
  public int getInteger() {
    return this.integer;
  }

  /**
   * Determine whether this unlimited integer is less than than another unlimited integer.
   *
   * @param other another unlimited integer
   * @return whether this unlimited integer is less than {@code other}
   */
  public boolean lt(UnlimitedInteger other) {
    if (this.equals(UnlimitedInteger.posInfty()))
      return false;
    if (this.equals(UnlimitedInteger.negInfty()))
      return !other.equals(UnlimitedInteger.negInfty());
    if (other.equals(UnlimitedInteger.posInfty()))
      return true;
    if (other.equals(UnlimitedInteger.negInfty()))
      return false;
    return this.integer < other.integer;
  }

  /**
   * Determine whether this unlimited integer is less or equal than than another unlimited integer.
   *
   * @param other another unlimited integer
   * @return whether this unlimited integer is less or equal than {@code other}
   */
  public boolean leq(UnlimitedInteger other) {
    return this.equals(other) || this.lt(other);
  }

  @Override
  public int hashCode() {
    return this.integer;
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof UnlimitedInteger other)
      return this.negInfty == other.negInfty &&
             this.integer == other.integer &&
             this.posInfty == other.posInfty;
    return false;
  }

  @Override
  public String toString() {
    if (this.negInfty)
      return "-infty";
    if (this.posInfty)
      return "infty";
    return Integer.toString(this.integer);
  }
}
