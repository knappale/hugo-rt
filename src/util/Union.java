package util;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.Objects;

import org.eclipse.jdt.annotation.Nullable;

public class Union<T, U> {
  private @Nullable T left;
  private @Nullable U right;

  public class Cases<X> {
    private Function<T, X> leftFun = null;
    private Function<U, X> rightFun = null;
    private Supplier<X> otherwiseFun = null;

    public Cases<X> left(Function<T, X> leftFun) {
      this.leftFun = leftFun;
      return this;
    }
    
    public Cases<X> right(Function<U, X> rightFun) {
      this.rightFun = rightFun;
      return this;
    }
    
    public Cases<X> otherwise(Supplier<X> otherwiseFun) {
      this.otherwiseFun = otherwiseFun;
      return this;
    }

    public X apply() {
      if (left != null)
        return this.leftFun.apply(left);
      if (right != null)
        return this.rightFun.apply(right);

      assert (this.otherwiseFun != null) : "No default for case distinction on union type";
      return this.otherwiseFun.get();
    }
  }


  private Union() {
  }

  public static <T, U> Union<T, U> left(T left) {
    Union<T, U> union = new Union<>();
    union.left = left;
    return union;
  }

  public static <T, U> Union<T, U>  right(U right) {
    Union<T, U>  union = new Union<>();
    union.right = right;
    return union;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.left, this.right);
  }

  @Override
  public boolean equals(Object object) {
    if (object instanceof Union<?, ?> other)
      return Objects.equals(this.left, other.left) &&
             Objects.equals(this.right, other.right);
    return false;
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("<");
    if (this.left != null)
      resultBuilder.append(this.left);
    else
      resultBuilder.append(this.right);
    resultBuilder.append(">");
    return resultBuilder.toString();
  }
}
