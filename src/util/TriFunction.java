package util;

/**
 * Function with three arguments type.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @since 2016-04-03
 */
@FunctionalInterface
public interface TriFunction<T, U, S, R> {
  /**
   * Applies this function to the given arguments.
   *
   * @param t the first function argument
   * @param u the second function argument
   * @param s the third function argument
   * @return the function result
   */
  R apply(T t, U u, S s);
}
