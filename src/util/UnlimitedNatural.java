package util;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Unlimited natural numbers (including infinity).
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @since 2016-04-03
 */
@NonNullByDefault
public class UnlimitedNatural {
  private boolean infty;
  private int natural;

  private static UnlimitedNatural INFTY = new UnlimitedNatural(true, 0);

  /**
   * Create a new unlimited natural.
   *
   * @param infty whether the new unlimited natural is infinity
   * @param natural the natural number underlying the new unlimited natural number
   */
  private UnlimitedNatural(boolean infty, int natural) {
    this.infty = infty;
    this.natural = natural;
  }

  /**
   * @return an unlimited natural number representing infinity
   */
  public static UnlimitedNatural infty() {
    return INFTY;
  }

  /**
   * @param natural a natural number
   * @return an unlimited natural number representing {@code natural}
   * @pre natural >= 0
   */
  public static UnlimitedNatural nat(int natural) {
    assert (natural >= 0) : "Cannot create an unlimited natural from " + natural;
    return new UnlimitedNatural(false, natural);
  }

  /**
   * @return whether this unlimited natural number represents infinity
   */
  public boolean isInfty() {
    return this.infty;
  }

  /**
   * @return the unlimited natural number's underlying natural number
   */
  public int getNatural() {
    return this.natural;
  }

  @Override
  public int hashCode() {
    return this.natural;
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof UnlimitedNatural other)
      return this.infty == other.infty &&
             this.natural == other.natural;
    return false;
  }

  @Override
  public String toString() {
    if (this.infty)
      return "infty";
    return Integer.toString(this.natural);
  }
}
