package util;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Utilities for objects.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public final class Objects {
  public static @NonNull <T> T assertNonNull(@Nullable T t) {
    assert (t != null) : "@NonNull invariant violated";
    return t;
  }

  public static <T> @NonNull T requireNonNull(@Nullable T t) {
    assert (t != null) : "@NonNull precondition violated";
    return t;
  }

  public static <T, U> Function<T, Stream<U>> toType(Class<U> clazz) {
    if (clazz == null)
      return t -> Stream.empty();
    return t -> clazz.isInstance(t) ? Stream.of(clazz.cast(t)) : Stream.empty();
  }

  public static <T> Stream<@NonNull T> ignoreNull(T t) {
    return t != null ? Stream.of(t) : Stream.empty();
  }

  public static <@NonNull T extends U, U> @NonNull Set<@NonNull T> subSetOf(@NonNull Set<U> set, Class<T> clazz) {
    Set<@NonNull T> result = new HashSet<>();
    for (U e : set) {
      if (e == null)
        continue;
      if (clazz.isInstance(e))
        result.add(clazz.cast(e));
    }
    return result;
  }
}
