package util;

import org.eclipse.jdt.annotation.NonNull;


public class Strings {
  public static @NonNull String capitalise(String string) {
    if (string == null)
      return "";
    return string.substring(0, 1).toUpperCase() + string.substring(1);
  }
}
