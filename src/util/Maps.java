package util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiFunction;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Auxiliary set functions
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class Maps {
  /**
   * Update a set-valued map by adding an element to the set registered for a key.
   * 
   * @param map a map
   * @param key a key
   * @param element element to be added
   * @return the updated map
   */
  public static <T, U> Map<T, Set<U>> update(Map<T, Set<U>> map, T key, U element) {
    Set<U> elements = map.get(key);
    if (elements == null)
      elements = new HashSet<>();
    elements.add(element);
    map.put(key, elements);
    return map;
  }

  /**
   * Compute the combination of two maps by a function.
   *
   * @param map1 a map
   * @param map2 a map
   * @return {@code map1} combine {@code map2}
   */
  public static <T, U> Map<T, U> combine(Map<T, U> map1, Map<T, U> map2, BiFunction<U, U, U> combine) {
    Map<T, U> combined = new HashMap<>(map1);
    for (Entry<T, U> entry : map2.entrySet()) {
      T key = entry.getKey();
      @Nullable U value1 = map1.get(key);
      U value2 = entry.getValue();
      if (value1 == null)
        combined.put(key, value2);
      else
        combined.put(key, combine.apply(value1, value2));
    }
    return combined;
  }
}
