package util;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import java.util.Objects;


/**
 * Extended java.util.BitSet's.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class BitSet {
  private java.util.BitSet bitSet;
  private int size;

  public BitSet() {
    this.bitSet = new java.util.BitSet();
    this.size = bitSet.size();
  }

  public BitSet(BitSet bitSet) {
    this(bitSet.size, bitSet.value());
  }

  public BitSet(int size) {
    this.bitSet = new java.util.BitSet(size);
    this.size = size;
  }

  public BitSet(int size, int n) {
    this.bitSet = new java.util.BitSet(size);
    this.size = size;
    for (int i = 0; n > 0; n/=2, i++)
      if (n%2 == 1)
	bitSet.set(i);
  }

  public boolean get(int i) {
    return bitSet.get(i);
  }

  public void set(int i) {
    bitSet.set(i);
  }

  public void clear(int i) {
    bitSet.clear(i);
  }

  public int leastUnused() {
    for (int i = 0; i < size; i++)
      if (!bitSet.get(i))
	return i;
    return size;
  }

  public int value() {
    int result = 0;

    for (int i = size-1; i >= 0; i--)
      if (bitSet.get(i))
	result += (1 << i);

    return result;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.bitSet);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof BitSet other)
      return this.size == other.size &&
             Objects.equals(this.bitSet, other.bitSet);
    return false;
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    for (int i = 0; i < size; i++)
      resultBuilder.append(bitSet.get(i) ? "1" : "0");
    return resultBuilder.toString();
  }
}
