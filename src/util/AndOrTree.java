package util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * And-Or-trees.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class AndOrTree<@NonNull T> {
  enum Kind {
    LEAF,
    AND,
    OR;
  }

  public interface Visitor<@NonNull T, U> {
    public U onLeaf(T contents);
    public U onAnd(T contents, List<AndOrTree<@NonNull T>> subTrees);
    public U onOr(T contents, List<AndOrTree<@NonNull T>> subTrees);
  }

  private Kind kind;
  private T label;
  private List<AndOrTree<T>> subTrees = new ArrayList<>();

  protected AndOrTree(Kind kind, T label) {
    this.kind = kind;
    this.label = label;
  }

  public static <@NonNull T> AndOrTree<T> leaf(T label) {
    return new AndOrTree<T>(Kind.LEAF, label);
  }

  public static <@NonNull T> AndOrTree<T> and(T label, List<AndOrTree<T>> subTrees) {
    AndOrTree<T> t = new AndOrTree<T>(Kind.AND, label);
    t.subTrees.addAll(subTrees);
    return t;
  }

  public static <@NonNull T> AndOrTree<T> or(T label, List<AndOrTree<T>> subTrees) {
    AndOrTree<T> t = new AndOrTree<T>(Kind.OR, label);
    t.subTrees.addAll(subTrees);
    return t;
  }

  public boolean isLeaf() {
    return (this.kind == Kind.LEAF);
  }

  public boolean isAnd() {
    return (this.kind == Kind.AND);
  }

  public boolean isOr() {
    return (this.kind == Kind.OR);
  }

  public T getLabel() {
    return this.label;
  }

  public List<AndOrTree<T>> getSubTrees() {
    return this.subTrees;
  }

  public <U> U accept(Visitor<T, U> visitor) {
    switch (this.kind) {
      case LEAF:
        return visitor.onLeaf(this.label);
      case AND:
        return visitor.onAnd(this.label, this.subTrees);
      case OR:
        return visitor.onOr(this.label, this.subTrees);
    }
    throw new IllegalStateException("Unknown state tree kind.");
  }

  private String toString(String prefix) {
    StringBuilder resultBuilder = new StringBuilder();

    resultBuilder.append(prefix);
    switch (this.kind) {
      case AND:
        resultBuilder.append("AND ");
        break;
      case OR:
        resultBuilder.append("OR ");
        break;
      case LEAF:
        resultBuilder.append("LEAF ");
        break;
    }
    resultBuilder.append("(label = ");
    resultBuilder.append(this.label);
    resultBuilder.append(")\n");
    for (AndOrTree<T> subTree : this.subTrees)
      resultBuilder.append(subTree.toString(prefix + "  "));

    return resultBuilder.toString();
  }

  @Override
  public String toString() {
    return this.toString("");
  }
}
