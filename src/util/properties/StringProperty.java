package util.properties;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class StringProperty extends Property<String> {
  private static ValueHandler<String> handler = new ValueHandler<>() {
    public String getFormat() {
      return "<string>";
    }

    public String getDefault() {
      return "";
    }

    public boolean isOptional() {
      return false;
    }

    public String parse(@Nullable String valueString) throws PropertyException {
      if (valueString == null)
        throw new PropertyException(this.getFormat() + " expected, but got null");
      if (valueString.trim().isEmpty() && !this.isOptional())
        throw new PropertyException("The argument must not be empty");
      return valueString;
    }
  };

  public StringProperty(String name, String description, String option, String value) {
    super(name, description, option, handler, value);
  }
}
