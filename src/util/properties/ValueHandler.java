package util.properties;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
interface ValueHandler<T> {
  public String getFormat();
  public T getDefault();
  public boolean isOptional();
  public T parse(@Nullable String valueString) throws PropertyException;
}
