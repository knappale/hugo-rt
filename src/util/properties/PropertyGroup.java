package util.properties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import static util.Objects.requireNonNull;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class PropertyGroup extends KeyStringHandler {
  private List<Property<@NonNull ?>> properties = new ArrayList<>();

  public PropertyGroup(String name, String description, String option, List<@NonNull ? extends Property<@NonNull ?>> properties) {
    super(name, description, option);
    for (Property<@NonNull ?> property : properties)
      this.properties.add(property);
  }

  public PropertyGroup(String name, String description, String option, @NonNull Property<@NonNull ?> ... properties) {
    this(name, description, option, Arrays.asList(properties));
  }

  private String errorString(String text) {
    return this.name + " property group " + text + ", keeping current values " + this.valuesString();
  }

  public boolean parseByOption(String argument) throws PropertyException {
    if (!matchesByOption(argument))
      return false;

    String valueString = this.getValueString(argument);
    if (valueString == null || valueString.equals(""))
      throw new PropertyException(errorString("does not show at least one value"));

    boolean finished = false;
    for (Property<@NonNull ?> property : this.properties) {
      String subValueString = null;

      int commaPos = valueString.indexOf(",");
      if (commaPos >= 0) {
        subValueString = valueString.substring(0, commaPos).trim();
        valueString = valueString.substring(commaPos+1).trim();
      }
      else {
        subValueString = valueString.trim();
        valueString = "";
        finished = true;
      }

      if (!subValueString.equals(""))
        property.parseByName(property.getName() + "=" + subValueString);

      if (finished)
        break;
    }
    if (!finished)
      throw new PropertyException(errorString("has too many values"));
    return true;
  }
  
  public abstract void check(List<Object> values) throws PropertyException;

  void check() throws PropertyException {
    for (Property<@NonNull ?> property : this.properties)
      property.check();

    List<Object> uncommittedValues = new ArrayList<Object>();
    for (Property<@NonNull ?> property : this.properties)
      uncommittedValues.add(property.getValueToCommit());
    this.check(uncommittedValues);
  }

  PropertyGroup getCopy(Map<Property<@NonNull ?>, Property<@NonNull ?>> copies) {
    for (Property<@NonNull ?> property : this.properties)
      property.provideCopy(copies);
    List<Property<@NonNull ?>> copiedProperties = new ArrayList<>();
    for (Property<@NonNull ?> property : this.properties)
      copiedProperties.add(requireNonNull(copies.get(property)));
    
    return new PropertyGroup(this.name, this.description, this.option, copiedProperties) {
                 public void check(List<Object> values) throws PropertyException {
                   this.check(values);
                 }
               };
  }

  public void commit() throws PropertyException {
    try {
      this.check();
    }
    catch (PropertyException pe) {
      throw new PropertyException("Values " + this.valuesToCommitString() + " failed check for " + this.getName() + " property group. Reason: " + pe.getMessage() + ". Keeping current values " + this.valuesString());
    }

    for (Property<@NonNull ?> property : this.properties)
      property.commit();
  }

  public void abort() {
    for (Property<@NonNull ?> property : this.properties)
      property.abort();
  }

  String valuesString() {
    return Formatter.separated(this.properties, property -> property.valueString(), ", ");
  }

  String valuesToCommitString() {
    return Formatter.separated(this.properties, property -> property.valueToCommitString(), ", ");
  }

  String getFormat() {
    StringBuilder resultBuilder = new StringBuilder();

    if (this.properties.size() == 0)
      return resultBuilder.toString();

    if (this.properties.size() == 1) {
      resultBuilder.append(this.properties.get(0).getFormat());
      return resultBuilder.toString();
    }

    if (this.properties.size() >= 2) {
      resultBuilder.append("(");
 
      resultBuilder.append(this.properties.get(0).getFormat());
      resultBuilder.append(" | [");
      resultBuilder.append(this.properties.get(0).getFormat());
      resultBuilder.append("]");

      String prefix = ",[";
      int count = 0;
      for (Property<@NonNull ?> property : this.properties.subList(1, this.properties.size()-1)) {
        resultBuilder.append(prefix);
        resultBuilder.append("[");
        resultBuilder.append(property.getFormat());
        resultBuilder.append("]");
        count++;
      }

      resultBuilder.append(prefix);
      resultBuilder.append(properties.get(this.properties.size()-1).getFormat());
      resultBuilder.append("]");
      
      for (int i = 0; i < count; i++)
        resultBuilder.append("]");

      resultBuilder.append(")");
    }

    return resultBuilder.toString();
  }

  public String descriptionString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.description);
    resultBuilder.append(" (currently = ");
    resultBuilder.append(this.valuesString());
    resultBuilder.append(")");
    return resultBuilder.toString();
  }

  public String optionString(String indent) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(indent);
    if (!this.option.equals("")) {
      resultBuilder.append("-");
      resultBuilder.append(this.option);
      resultBuilder.append(", ");
    }
    resultBuilder.append("--");
    resultBuilder.append(this.name);
    resultBuilder.append("=");
    resultBuilder.append(this.getFormat());
    resultBuilder.append("\n");
    resultBuilder.append(indent);
    resultBuilder.append("   ");
    resultBuilder.append(this.descriptionString());
    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.properties);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof PropertyGroup other)
      return Objects.equals(this.properties, other.properties);
    return false;
  }
}
