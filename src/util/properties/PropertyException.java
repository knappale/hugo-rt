package util.properties;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
@SuppressWarnings("serial")
public class PropertyException extends Exception {
  public PropertyException() {
    super();
  }

  public PropertyException(String message) {
    super(message);
  }
}
