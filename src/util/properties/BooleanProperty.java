package util.properties;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class BooleanProperty extends Property<Boolean> {
  private static ValueHandler<Boolean> handler = new ValueHandler<>() {
    public String getFormat() {
      return "(false | true)";
    }

    public Boolean getDefault() {
      return true;
    }

    public boolean isOptional() {
      return true;
    }

    public Boolean parse(@Nullable String valueString) throws PropertyException {
      if ("true".equals(valueString))
        return true;
      if ("false".equals(valueString))
        return false;
      throw new PropertyException(this.getFormat() + " expected, but got " + Formatter.quoted(valueString));
    }
  };

  public BooleanProperty(String name, String description, String option, Boolean value) {
    super(name, description, option, handler, value);
  }

  public void check(Boolean value) throws PropertyException {
  }
}
