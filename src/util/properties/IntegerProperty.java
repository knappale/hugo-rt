package util.properties;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class IntegerProperty extends Property<Integer> {
  private static ValueHandler<Integer> handler = new ValueHandler<>() {
    public String getFormat() {
      return "<int>";
    }

    public Integer getDefault() {
      return 1;
    }

    public boolean isOptional() {
      return true;
    }

    public Integer parse(@Nullable String valueString) throws PropertyException {
      try {
        return Integer.parseInt(valueString);
      }
      catch (NumberFormatException nfe) {
        throw new PropertyException(this.getFormat() + " expected, but got " + Formatter.quoted(valueString));
      }
    }
  };

  public IntegerProperty(String name, String description, String option, Integer value) {
    super(name, description, option, handler, value);
  }
}
