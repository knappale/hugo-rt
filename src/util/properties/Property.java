package util.properties;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static util.Objects.requireNonNull;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class Property<@NonNull T> extends KeyStringHandler {
  private ValueHandler<@NonNull T> handler;

  private T value;
  private Optional<@NonNull T> uncommittedValue = Optional.empty();

  Property(String name, String description, String option, ValueHandler<@NonNull T> handler, T value) {
    super(name, description, option);
    this.handler = handler;
    this.value = value;
  }

  public T getValue() {
    return this.value;
  }

  String getFormat() {
    return this.handler.getFormat();
  }

  boolean isOptional() {
    return this.handler.isOptional();
  }

  String valueString() {
    return requireNonNull(this.value.toString());
  }

  public boolean parseByName(String argument) throws PropertyException {
    if (!this.matchesByName(argument))
      return false;

    String valueString = this.getValueString(argument);
    try {
      this.uncommittedValue = Optional.of(this.handler.parse(valueString));
    }
    catch (PropertyException pe) {
      throw new PropertyException("Cannot parse property " + this.getName() + ": " + pe.getMessage());
    }
    return true;
  }

  public boolean parseByOption(String argument) throws PropertyException {
    if (!this.matchesByOption(argument))
      return false;

    String valueString = this.getValueString(argument);
    if (valueString == null) {
      this.uncommittedValue = Optional.of(this.handler.getDefault());
    }
    else {
      try {
        this.uncommittedValue = Optional.of(this.handler.parse(valueString));
      }
      catch (PropertyException pe) {
        throw new PropertyException("Cannot parse property " + this.getName() + ": " + pe.getMessage());
      }
    }
    return true;
  }

  public abstract void check(T value) throws PropertyException;

  void check() throws PropertyException {
    if (this.uncommittedValue.isPresent())
      this.check(this.uncommittedValue.get());
  }

  T getValueToCommit() {
    if (this.uncommittedValue.isPresent())
      return this.uncommittedValue.get();
    return this.value;
  }

  String valueToCommitString() {
    if (this.uncommittedValue.isPresent())
      return requireNonNull(this.uncommittedValue.get().toString());
    return requireNonNull(this.value.toString());
  }

  public void commit() throws PropertyException {
    if (!this.uncommittedValue.isPresent())
      return;

    try {
      this.check();
    }
    catch (PropertyException pe) {
      throw new PropertyException("Value " + this.valueToCommitString() + " failed check for " + this.getName() + " property. Reason: " + pe.getMessage() + ". Keeping current value " + this.valueString());
    }

    this.value = this.uncommittedValue.get();
    this.uncommittedValue = Optional.empty();
  }

  public void abort() {
    this.uncommittedValue = Optional.empty();
  }

  public void provideCopy(Map<Property<@NonNull ?>, Property<@NonNull ?>> copies) {
    if (copies.get(this) != null)
      return;

    copies.put(this, new Property<T>(this.name, this.description, this.option, this.handler, this.value) {
                       public void check(T value) throws PropertyException {
                         this.check(value);
                       }
                     });
  }

  public String descriptionString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.description);
    resultBuilder.append(" (currently = ");
    resultBuilder.append(this.valueString());
    resultBuilder.append(")");
    return resultBuilder.toString();
  }

  public String optionString(String indent) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(indent);
    if (!this.option.equals("")) {
      resultBuilder.append("-");
      resultBuilder.append(this.option);
      resultBuilder.append(", ");
    }
    resultBuilder.append("--");
    resultBuilder.append(this.name);
    if (this.isOptional()) {
      resultBuilder.append("[=");
      resultBuilder.append(this.getFormat());
      resultBuilder.append("] (no value means ");
      resultBuilder.append(this.handler.getDefault().toString());
      resultBuilder.append(")\n");
    }
    else {
      resultBuilder.append("=");
      resultBuilder.append(this.getFormat());
      resultBuilder.append("\n");
    }
    resultBuilder.append(indent);
    resultBuilder.append("   ");
    resultBuilder.append(this.descriptionString());
    return resultBuilder.toString();
  }

  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(this.name);
    resultBuilder.append(" = ");
    resultBuilder.append(this.valueString());
    return resultBuilder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.value);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof Property<?> other) 
      return super.equals(other) &&
    		 Objects.equals(this.value, other.value) &&
             Objects.equals(this.uncommittedValue, other.uncommittedValue);
    return false;
  }
}
