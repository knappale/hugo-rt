package util.properties;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static util.Objects.requireNonNull;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class KeyStringHandler {
  String name = "";
  String description = "";
  String option = "";

  KeyStringHandler(String name, String description, String option) {
    this.name = name;
    this.description = description;
    this.option = option;
  }

  public String getName() {
    return this.name;
  }

  String getKeyString(String argument) {
    String keyString = argument.trim();

    // Only consider first word
    int firstSpacePos = keyString.indexOf(" ");
    if (firstSpacePos >= 0)
      keyString = keyString.substring(0, firstSpacePos);

    // If first word contains a '=' discard everything after and including '='
    int firstEqualsPos = keyString.indexOf("=");
    if (firstEqualsPos >= 0)
      keyString = keyString.substring(0, firstEqualsPos);

    return requireNonNull(keyString);
  }

  @Nullable String getValueString(String argument) {
    String valueString = argument.trim();

    // If argument contains a '=' discard everything before and including '='
    int firstEqualsPos = valueString.indexOf("=");
    if (firstEqualsPos >= 0)
      return valueString.substring(firstEqualsPos+1).trim();

    // Otherwise there is no value string
    return null;
  }

  public boolean matchesByName(String argument) {
    String keyString = this.getKeyString(argument);
    return (keyString.toLowerCase().equals(this.name.toLowerCase()));
  }

  public boolean matchesByOption(String argument) {
    String keyString = this.getKeyString(argument);
    return ((!this.option.equals("") && keyString.equals("-" + this.option)) ||
            keyString.toLowerCase().equals("--" + this.name.toLowerCase()));
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof KeyStringHandler other)
      return Objects.equals(this.name, other.name) && 
             Objects.equals(this.description, other.description) && 
             Objects.equals(this.option, other.option);
    return false;
  }
}
