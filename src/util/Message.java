package util;

import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNull;

import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;


/**
 * Simple messages
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
public class Message {
  public static final int DEBUG = -4;
  public static final int FATALERROR = -3;
  public static final int ERROR = -2;
  public static final int WARNING = -1;
  public static final int USAGE = 0;
  public static final int LOG = 1;
  public static final int INFO = 2;

  public static final int MINVERBOSITY = WARNING + 1;
  public static final int MAXVERBOSITY = INFO;
  public static int verbosity = 1;
  public static boolean debug = false;
  private static Logger logger = null;

  public static void fatalError(String msg) {
    message(FATALERROR, msg);
  }

  public static void error(String msg) {
    message(ERROR, msg);
  }

  public static void warning(String msg) {
    message(WARNING, msg);
  }

  public static void usage(String msg) {
    message(USAGE, msg);
  }

  public static void log(String msg) {
    message(LOG, msg);
  }

  public static void info(String msg) {
    message(INFO, msg);
  }

  public static void message(int category, String msg) {
    if (category <= verbosity) {
      switch (category) {
        case FATALERROR :
          System.err.println("[FATAL ERROR] " + msg);
          break;
        case ERROR :
          System.err.println("[ERROR] " + msg);
          break;
        case WARNING :
          System.err.println("[WARNING] " + msg);
          break;
        case USAGE :
          System.out.println(msg);
          break;
        case LOG :
          System.out.println(msg);
          break;
        case INFO :
          System.out.println("[info] " + msg);
          break;
        case DEBUG :
          System.out.println("[DEBUG] " + msg);
          break;
        default :
          System.err.println("[UNKNOWN] " + msg);
      }
    }
  }
  
  private static class DebugFormatter extends Formatter {
    public synchronized String format(LogRecord re) {
      StringBuffer result = new StringBuffer();
      result.append("[DEBUG] ");
      result.append(re.getSourceClassName());
      result.append("::");
      result.append(re.getSourceMethodName());
      result.append("(");
      if (re.getParameters() != null) {
        for (int i = 0; i < re.getParameters().length; i++) {
          result.append(re.getParameters()[i]);
          if (i < re.getParameters().length - 1)
            result.append(", ");
        }
      }
      result.append(")");
      result.append("\n");
      result.append(formatMessage(re));
      result.append("\n");
      return result.toString();
    }
  }

  public static @NonNull Logger debug() {
    Logger logger = Message.logger;
    if (logger != null)
      return logger;
    logger = Logger.getLogger("util.Debug");
    if (Message.debug)
      logger.setLevel(Level.FINEST);
    else
      logger.setLevel(Level.WARNING);
    logger.setUseParentHandlers(false);
    ConsoleHandler handler = new ConsoleHandler();
    handler.setLevel(Level.FINEST);
    handler.setFormatter(new DebugFormatter());
    logger.addHandler(handler);
    Message.logger = logger;
    return logger;
  }
}
