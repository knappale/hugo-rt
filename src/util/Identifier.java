package util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Identifiers as dot-separated names.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class Identifier {
  private List<String> identifiers = new ArrayList<>();

  private Identifier() {
  }

  /**
   * Create an empty identifier.
   *
   * @return empty identifier
   */
  public static Identifier empty() {
    return new Identifier();
  }

  /**
   * Create an un-qualified identifier.
   *
   * @param identifier identifier string
   * @return identifier
   */
  public static Identifier id(String identifier) {
    Identifier i = new Identifier();
    i.identifiers.add(identifier);
    return i;
  }

  /**
   * Create a qualified identifier.
   *
   * @param qualifier qualifier identifier
   * @param identifier identifier string
   * @return identifier
   */
  public static Identifier id(Identifier qualifier, String identifier) {
    Identifier i = new Identifier();
    i.identifiers.addAll(qualifier.identifiers);
    i.identifiers.add(identifier);
    return i;
  }

  /**
   * Create a qualified identifier.
   *
   * @param qualifier qualifier identifier
   * @param identifier identifier
   * @return identifier
   */
  public static Identifier id(Identifier qualifier, Identifier identifier) {
    Identifier i = new Identifier();
    i.identifiers.addAll(qualifier.identifiers);
    i.identifiers.addAll(identifier.identifiers);
    return i;
  }

  /**
   * Determine whether this identifier matches another identifier.
   *
   * @param other an identifier
   * @return whether this identifier matches the other identifier
   */
  public boolean matches(@Nullable Identifier other) {
    if (other == null)
      return false;

    if (this.identifiers.size() != other.identifiers.size())
      return false;
    for (int i = 0; i < this.identifiers.size(); i++)
      if (!this.identifiers.get(i).equals(other.identifiers.get(i)))
	return false;
    return true;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.identifiers);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof Identifier other)
      return Objects.equals(this.identifiers, other.identifiers);
    return false;
  }

  @Override
  public String toString() {
    return Formatter.separated(this.identifiers, ".");
  }
}
