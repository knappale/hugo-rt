package util;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Pairs.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class Pair<A, B> {
  private A first;
  private B second;

  /**
   * Create a new pair.
   *
   * @param first the first component
   * @param second the second component
   */
  public Pair(A first, B second) {
    this.first = first;
    this.second = second;
  }

  /**
   * @return this pair's first component
   */
  public A getFirst() {
    return this.first;
  }

  /**
   * @return this pair's second component
   */
  public B getSecond() {
    return this.second;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.first, this.second);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof Pair<?, ?> other)
      return Objects.equals(this.first, other.first) &&
             Objects.equals(this.second, other.second);
    return false;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("<");
    builder.append(this.first);
    builder.append(", ");
    builder.append(this.second);
    builder.append(">");
    return builder.toString();
  }
}
