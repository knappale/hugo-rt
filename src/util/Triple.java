package util;

import java.util.Objects;


/**
 * Triples.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class Triple<A, B, C> {
  private A first;
  private B second;
  private C third;

  public Triple(A first, B second, C third) {
    this.first = first;
    this.second = second;
    this.third = third;
  }

  public A getFirst() {
    return this.first;
  }

  public B getSecond() {
    return this.second;
  }

  public C getThird() {
    return this.third;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.first, this.second, this.third);
  }

  @Override
  public boolean equals(Object object) {
    if (object instanceof Triple<?, ?, ?> other)
      return Objects.equals(this.first, other.first) &&
             Objects.equals(this.second, other.second) &&
             Objects.equals(this.third, other.third);
    return false;
  }

  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("<");
    resultBuilder.append(this.first);
    resultBuilder.append(", ");
    resultBuilder.append(this.second);
    resultBuilder.append(", ");
    resultBuilder.append(this.third);
    resultBuilder.append(">");
    return resultBuilder.toString();
  }
}
