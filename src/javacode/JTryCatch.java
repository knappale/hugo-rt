package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a try-catch-block.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba </A>
 */
@NonNullByDefault
public class JTryCatch extends JStatement {
  private JBlock tryBlock;
  private String exceptionDeclaration;
  private JBlock catchBlock;

  /**
   * Create a new try-catch-block.
   */
  public JTryCatch(JBlock tryBlock, String exceptionDeclaration, JBlock catchBlock) {
    this.tryBlock = tryBlock;
    this.exceptionDeclaration = exceptionDeclaration;
    this.catchBlock = catchBlock;
  }

  /**
   * Create a new try-catch-block with empty catch block.
   */
  public JTryCatch(JBlock tryBlock, String exceptionDeclaration) {
    this(tryBlock, exceptionDeclaration, new JBlock());
  }

  @Override
  boolean isEmpty() {
    return tryBlock.isEmpty();
  }

  @Override
  boolean isUnconditionalJump() {
    return false;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("try ");
    resultBuilder.append(this.tryBlock.statement(indent + "  ", true, false));
    resultBuilder.append("\n");
    resultBuilder.append(indent);
    resultBuilder.append("catch (");
    resultBuilder.append(this.exceptionDeclaration);
    resultBuilder.append(") ");
    resultBuilder.append(this.catchBlock.statement(indent + "  ", true, false));

    return resultBuilder.toString();
  }
}
