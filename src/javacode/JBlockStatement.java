package javacode;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Java statement that is part of a block.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class JBlockStatement {
  private List<String> comments = new ArrayList<>();

  public void addComment(String comment) {
    comments.add(comment);
  }

  public List<String> getComments() {
    return this.comments;
  }

  /**
   * @return a statement from this block statement
   */
  public JStatement getStatement() {
    return new JBlock(this);
  }

  /**
   * @return whether this block statement is an unconditional jump
   */
  abstract boolean isUnconditionalJump();
  
  /**
   * @return whether this block statement is empty
   */
  abstract boolean isEmpty();

  /**
   * @return this block statement as a cycle
   */
  public JCycle asJCycle() {
    return JCycle.blockStatement(this);
  }

  /**
   * Determine the Java code of this block statement.
   * 
   * The general contract is that overriding methods produce a string without
   * a trailing new line ("\n"), i.e., the string when output ends in the middle
   * of a line.
   * 
   * @param indent indentation string
   * @param inLine whether the statement occurs in a context on a non-empty line
   * @param emptyLineAbove whether the statement occurs in a context with at least
   *                       one empty line above
   * @return Java code of this statement
   */
  public abstract String statement(String indent, boolean inLine, boolean emptyLineAbove);

  /**
   * @return Java code of this statement
   */
  public String statement() {
    return this.statement("", false, true);
  }
}
