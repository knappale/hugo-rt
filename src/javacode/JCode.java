package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a single statement in unstructured Java code.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class JCode extends JStatement {
  private String code = "";

  /**
   * Create a new code fragment.
   * 
   * @param code a string containing a Java statement
   */
  public JCode(String code) {
    this.code = code.trim();
  }

  @Override
  boolean isEmpty() {
    return this.code.equals("");
  }

  @Override
  boolean isUnconditionalJump() {
    return false;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");

    resultBuilder.append(indent);
    resultBuilder.append(this.code);

    return resultBuilder.toString();
  }
}
