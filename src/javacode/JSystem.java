package javacode;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a collection of classes.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class JSystem {
  private Set<JClass> classes = new HashSet<>();

  public JSystem() {
  }

  /**
   * Add a class to this Java code.
   * 
   * @param jClass a Java class
   */
  public void addClass(JClass jClass) {
    classes.add(jClass);
  }

  /**
   * @return classes in this Java code
   */
  public Collection<JClass> getJClasses() {
    return this.classes;
  }
}
