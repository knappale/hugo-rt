package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a case in a switch.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class JCase {
  private String label;
  private JBlockStatement blockStatement;
  
  public JCase(String label, JBlockStatement blockStatement) {
    this.label = label;
    this.blockStatement = blockStatement;
  }

  boolean isEmpty() {
    return this.blockStatement.isEmpty();
  }

  boolean isUnconditionalJump() {
    return this.blockStatement.isUnconditionalJump();
  }

  /**
   * @return whether this case is terminated by an unconditional jump
   */
  boolean hasUnconditionalJump() {
    return this.blockStatement.isUnconditionalJump();
  }

  /**
   * Determine the Java code of this case.
   * 
   * The general contract is that overriding methods produce a string without
   * a trailing new line ("\n"), i.e., the string when output ends in the middle
   * of a line.
   * 
   * @param indent indentation string
   * @param inLine whether the statement occurs in a context on a non-empty line
   * @param emptyLineAbove whether the statement occurs in a context with at least
   *                       one empty line above
   * @return Java code of this case
   */
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("case ");
    resultBuilder.append(this.label);
    resultBuilder.append(": ");
    resultBuilder.append(this.blockStatement.statement(indent + "  ", true, false));

    return resultBuilder.toString();
  }

  /**
   * @return Java code of this case
   */
  public String statement() {
    return this.statement("", false, true);
  }
}
