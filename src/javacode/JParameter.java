package javacode;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Representation of a parameter of a method.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class JParameter {
  /** @invariant !type.equals("") && !name.equals("") */
  /** @invariant type.equals(type.trim()) && name.equals(name.trim()) */
  private String type;
  private String name;

  /**
   * Create a new parameter.
   * 
   * @param name name of the parameter
   * @param type type of the parameter
   */
  public JParameter(String name, String type) {
    this.name = name;
    this.type = type;
  }

  /**
   * @return parameter's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return Java code representation
   */
  public String declaration() {
    return this.type + " " + this.name;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name, this.type);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      JParameter other = (JParameter)object;
      return Objects.equals(this.name, other.name) &&
             Objects.equals(this.type, other.type);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.name + " : " + this.type;
  }
}
