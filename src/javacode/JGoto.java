package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a goto-statement in a cycle environment.
 * 
 * A goto statement continues the block where the target's label is in the
 * cycle's <CODE>switch</CODE> statement.  A goto can jump into any
 * surrounding block or any other block.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class JGoto extends JBlockStatement {
  private String target;
  private String cycleLabel;

  /**
   * Create a goto statement to a target in some block.
   * 
   * @param target target to jump to
   */
  public JGoto(String target) {
    this.target = target;
    this.cycleLabel = "";
  }

  void setCycleLabel(String cycleLabel) {
    this.cycleLabel = cycleLabel;
  }

  boolean matches(JTarget target) {
    return target.matches(this.target);
  }

  boolean matches(String label) {
    return this.target.equals(label);
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  boolean isUnconditionalJump() {
    return true;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append(JKeywords.GOTOVAR + " = " + this.target + ";\n");
    resultBuilder.append(indent);
    resultBuilder.append("continue " + this.cycleLabel + ";");

    return resultBuilder.toString();
  }
}
