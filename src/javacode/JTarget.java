package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class JTarget extends JCycle {
  private JCycle cycle;

  public JTarget(String label, JCycle cycle) {
    super(label);
    this.cycle = cycle;
  }

  public JTarget getExtended(JBlockStatement blockStatement) {
    return new JTarget(this.label, JCycle.sequential(this.cycle, JCycle.blockStatement(blockStatement)));
  }

  boolean matches(JGoto jGoto) {
    return jGoto.matches(this.label);
  }

  boolean matches(String target) {
    return this.label.equals(target);
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  boolean isUnconditionalJump() {
    return this.cycle.isUnconditionalJump();
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();
    String indent1 = indent + "  ";

    if (inLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);
    
    resultBuilder.append("case ");
    resultBuilder.append(this.label);
    resultBuilder.append(":");
    resultBuilder.append(this.cycle.statement(indent1, true, false));

    return resultBuilder.toString();
  }
}
