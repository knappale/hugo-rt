package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * Representation of {@code return}.
 */
@NonNullByDefault
public class JReturn extends JStatement {
  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  boolean isUnconditionalJump() {
    return true;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("return;");
    return resultBuilder.toString();
  }
}