package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of class attributes.
 * 
 * Attributes may only be used in classes, not in blocks.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba </A>
 */
@NonNullByDefault
public class JAttribute {
  private String type;
  private String name;
  private String value;
  private String modifiers = "";

  /**
   * Create an attribute.
   * 
   * @param name name of the attribute
   * @param type type of the attribute
   * @param value initial value of the attribute
   * @param modifiers white-space separated list of modifiers, visibility placed first
   */
  public JAttribute(String name, String type, String value, String modifiers) {
    this.name = name.trim();
    this.type = type.trim();
    this.value = value.trim();
    this.modifiers = modifiers.trim();
  }

  /**
   * @return attribute's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return Java code for declaration of this attribute
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();

    if (!this.modifiers.equals(""))
      resultBuilder.append(this.modifiers + " ");
    resultBuilder.append(this.type + " ");
    resultBuilder.append(this.name);
    if (!this.value.equals(""))
      resultBuilder.append(" = " + this.value);
    resultBuilder.append(";");

    return resultBuilder.toString();
  }
}
