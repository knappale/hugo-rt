package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class JBreak extends JStatement {
  private @Nullable JLabel label;

  public JBreak() {
  }

  public JBreak(JLabel label) {
    this.label = label;
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  boolean isUnconditionalJump() {
    return true;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("break");
    JLabel label = this.label;
    if (label != null) {
      resultBuilder.append(" ");
      resultBuilder.append(label);
    }
    resultBuilder.append(";");

    return resultBuilder.toString();
  }
}
