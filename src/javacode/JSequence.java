package javacode;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class JSequence extends JBlockStatement {
  private List<JBlockStatement> blockStatements = new ArrayList<>();
  
  public JSequence() {
  }

  public JSequence(@NonNull JBlockStatement... blockStatements) {
    for (JBlockStatement blockStatement : blockStatements)
      this.blockStatements.add(blockStatement);
  }

  public void addBlockStatement(JBlockStatement blockStatement) {
    if (!blockStatement.isEmpty())
      this.blockStatements.add(blockStatement);
  }

  @Override
  public boolean isUnconditionalJump() {
    return this.blockStatements.stream().anyMatch(JBlockStatement::isUnconditionalJump);
  }

  @Override
  public boolean isEmpty() {
    return this.blockStatements.stream().allMatch(JBlockStatement::isEmpty);
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();
    
    for (JBlockStatement blockStatement : this.blockStatements) {
      resultBuilder.append(blockStatement.statement(indent, inLine, emptyLineAbove));
      inLine = true;
      emptyLineAbove = false;
    }

    return resultBuilder.toString();
  }
}
