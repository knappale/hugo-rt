package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of Java statements.
 *    
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public abstract class JStatement extends JBlockStatement {
  @Override
  public JStatement getStatement() {
    return this;
  }
}
