package javacode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a block.
 * 
 * A block can contain several statements and is a statement itself. In fact, a
 * block is a linked list of statements. As a special non-Java feature, a block
 * can provide a switch structure which makes it possible that gotos can be
 * executed within a block. It is also possible to break the block and jump to a
 * label of the surrounding block. A block can also define local variables,
 * which are only valid within the block.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class JBlock extends JStatement {
  private List<JVariableDeclaration> variableDeclarations = new ArrayList<>();
  private List<JBlockStatement> blockStatements = new ArrayList<>();

  /**
   * Create a block.
   */
  public JBlock() {
  }

  /**
   * Create a block for a block statement.
   */
  public JBlock(JBlockStatement blockStatement) {
    this.blockStatements.add(blockStatement);
  }
  
  /**
   * Create a block for a list of block statements.
   */
  public JBlock(List<JBlockStatement> blockStatements) {
    this.blockStatements.addAll(blockStatements);
  }

  /**
   * Add a local variables to this block.
   * 
   * @param variable a variable to be added
   */
  public void addVariable(JVariableDeclaration variableDeclaration) {
    this.variableDeclarations.add(variableDeclaration);
  }

  /**
   * Add local variables to this block.
   * 
   * @param variables collection of variables to be added
   */
  public void addVariables(Collection<JVariableDeclaration> variables) {
    this.variableDeclarations.addAll(variables);
  }

  /**
   * Add a statement to this block.
   * 
   * @param statement statement to be added
   */
  public void addBlockStatement(JBlockStatement blockStatement) {
    this.blockStatements.add(blockStatement);
  }

  @Override
  boolean isEmpty() {
    return this.variableDeclarations.stream().allMatch(JVariableDeclaration::isEmpty) &&
           this.blockStatements.stream().allMatch(JBlockStatement::isEmpty);
  }

  @Override
  boolean isUnconditionalJump() {
    for (JBlockStatement blockStatement : blockStatements) {
      if (blockStatement.isUnconditionalJump())
        return true;
    }    
    return false;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();
    String indent_1 = (indent.length() >= 2 ? indent.substring(0, indent.length()-2) : "");
    String indent1 = indent + "  ";
    
    if (!inLine)
      resultBuilder.append(indent);
    else {
      indent1 = indent;
      indent = indent_1;
    }

    resultBuilder.append("{\n");
    if (!this.variableDeclarations.isEmpty()) {
      for (JVariableDeclaration variableDeclaration : this.variableDeclarations) {
        resultBuilder.append(indent1);
        resultBuilder.append(variableDeclaration.statement());
        resultBuilder.append("\n");
      }
      resultBuilder.append("\n");
    }
    for (JBlockStatement blockStatement : this.blockStatements) {
      resultBuilder.append(blockStatement.statement(indent1, false, false));
      resultBuilder.append("\n");
    }
    resultBuilder.append(indent);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
