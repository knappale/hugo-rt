package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * Representation of a local variable declaration.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class JVariableDeclaration extends JBlockStatement {
  private boolean isFinal = false;
  private String type = "";
  private String name = "";
  private String value = "";

  /**
   * Create a new local variable declaration.
   * 
   * @param name name of the variable
   * @param type type of the variable
   */
  public JVariableDeclaration(String name, String type) {
    this.name = name.trim();
    this.type = type.trim();
    this.value = "";
    this.isFinal = false;
  }

  /**
   * Create a new local variable declaration including an initial value expression.
   * 
   * @param name name of the variable
   * @param type type of the variable
   * @param value variable initial value
   */
  public JVariableDeclaration(String name, String type, String value) {
    this.name = name.trim();
    this.type = type.trim();
    this.value = value.trim();
    this.isFinal = false;
  }

  /**
   * Create a new local variable declaration including an initial value expression
   * and whether the variable is to be final.
   * 
   * @param name name of the variable
   * @param type type of the variable
   * @param value initial value of the variable
   * @param isFinal whether the variable is to be final
   */
  public JVariableDeclaration(String name, String type, String value, boolean isFinal) {
    this.name = name.trim();
    this.type = type.trim();
    this.value = value.trim();
    this.isFinal = isFinal;
  }

  /**
   * @return variable's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Transform this local variable declaration into a class attribute.
   *
   * @return a class attribute representing this variable declaration
   */
  public JAttribute getClassAttribute() {
    return new JAttribute(this.name, this.type, this.value, JKeywords.PRIVATE + " " + JKeywords.STATIC + (this.isFinal ? " " + JKeywords.FINAL : ""));
  }

  /**
   * Transform this local variable declaration into an (instance) attribute.
   *
   * @return an (instance) attribute representing this variable declaration
   */
  public JAttribute getAttribute() {
    return new JAttribute(this.name, this.type, this.value, JKeywords.PRIVATE + " " + (this.isFinal ? " " + JKeywords.FINAL : ""));
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  boolean isUnconditionalJump() {
    return false;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (!inLine)
      resultBuilder.append(indent);

    if (this.isFinal) {
      resultBuilder.append(JKeywords.FINAL);
      resultBuilder.append(" ");
    }
    resultBuilder.append(this.type);
    resultBuilder.append(" ");
    resultBuilder.append(this.name);
    if (!this.value.equals("")) {
      resultBuilder.append(" = ");
      resultBuilder.append(this.value);
    }
    resultBuilder.append(";");

    return resultBuilder.toString();
  }
}
