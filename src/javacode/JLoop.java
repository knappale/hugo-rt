package javacode;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Representation of a loop.
 * 
 * This is represented by a while loop with {@code true} as condition.  The code
 * has the following form:
 *
 * <PRE>
 *   label:
 *   while (true) {
 *     if (branch1.condition) {
 *       branch1.statement
 *       continue label;
 *     }
 *     if (branch2.condition2) {
 *       branch2.statement
 *       continue label;
 *     }
 *     ...
 *     elseBranch.statement
 *     continue label;
 *   }
 * </PRE>
 *
 * We expect a loop to be exitable if there is at least one branch in it which
 * can be exited (i.e., is not extended by a {@code continue}).
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class JLoop extends JBlockStatement {
  private JLabel label;
  private List<JBranch> branches = new ArrayList<>();
  private JBranch elseBranch;
  private boolean isUnconditionalJump = false;

  /**
   * Create a new loop.
   */
  public JLoop(JLabel label, List<JBranch> branches, @Nullable JBranch elseBranch) {
    this.label = label;
    for (JBranch branch : branches)
      this.addBranch(branch);
    if (elseBranch == null) {
      elseBranch = JBranch.unguarded(JCycle.blockStatement(new JContinue(this.label)));
    }
    else {
      if (!elseBranch.hasUnconditionalJump())
        elseBranch = elseBranch.getExtended(new JContinue(this.label));
      else
        this.isUnconditionalJump = elseBranch.isUnconditionalJump();
    }
    this.elseBranch = elseBranch;
  }

  /**
   * Add branch to this loop.
   *
   * @param branch branch, i.e., a conditional statement
   */
  public void addBranch(JBranch branch) {
    if (!branch.hasUnconditionalJump())
      branch = branch.getExtended(new JContinue(this.label));
    else
      this.isUnconditionalJump = branch.isUnconditionalJump();
    this.branches.add(branch);
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  boolean isUnconditionalJump() {
    return this.isUnconditionalJump;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();
    String indent1 = indent + "  ";

    if (inLine)
      resultBuilder.append("\n");

    resultBuilder.append(indent);
    resultBuilder.append(this.label + ":\n");
    resultBuilder.append(indent);
    resultBuilder.append("while (true) {\n");
    for (JBranch branch : branches) {
      resultBuilder.append(branch.statement(indent1, false, false));
      resultBuilder.append("\n");
    }
    resultBuilder.append(this.elseBranch.statement(indent1, false, false));
    resultBuilder.append("\n");
    resultBuilder.append(indent);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
