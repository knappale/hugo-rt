package javacode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Java code keywords.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class JKeywords {
  // Reserved keywords
  public static final String VOID = "void";
  public static final String BOOLEAN = "boolean";
  public static final String CHAR = "char";
  public static final String BYTE = "byte";
  public static final String SHORT = "short";
  public static final String INT = "int";
  public static final String LONG = "long";
  public static final String FLOAT = "float";
  public static final String DOUBLE = "double";
  public static final String TRUE = "true";
  public static final String FALSE = "false";
  public static final String STRICTFP = "strictfp";
  public static final String IF = "if";
  public static final String ELSE = "else";
  public static final String FOR = "for";
  public static final String WHILE = "while";
  public static final String DO = "do";
  public static final String BREAK = "break";
  public static final String CONTINUE = "continue";
  public static final String SWITCH = "switch";
  public static final String CASE = "case";
  public static final String DEFAULT = "default";
  public static final String RETURN = "return";
  public static final String PUBLIC = "public";
  public static final String PROTECTED = "protected";
  public static final String PRIVATE = "private";
  public static final String STATIC = "static";
  public static final String FINAL = "final";
  public static final String CONST = "const";
  public static final String CLASS = "class";
  public static final String INTERFACE = "interface";
  public static final String EXTENDS = "extends";
  public static final String IMPLEMENTS = "implements";
  public static final String THIS = "this";
  public static final String NEW = "new";
  public static final String GOTO = "goto";
  public static final String INTEGERCLASS = "Integer";
  public static final String BOOLEANCLASS = "Boolean";
  public static final String THREADCLASS = "Thread";
  public static final String RUNNABLEINTERFACE = "Runnable";
  public static final String EVENTCLASS = "Event";
  public static final String EVENTQUEUECLASS = "EventQueue";
  public static final String GOTOVAR = "gotoVar";

  private static final @NonNull String[] keywords = {
    VOID, BOOLEAN, CHAR, BYTE, SHORT, INT, LONG, TRUE, FALSE, STRICTFP,
    IF, ELSE, FOR, WHILE, DO, BREAK, CONTINUE, SWITCH, CASE, DEFAULT, RETURN,
    PUBLIC, PROTECTED, PRIVATE, STATIC, FINAL, CONST, CLASS, INTERFACE, EXTENDS, IMPLEMENTS,
    THIS, NEW, GOTO, INTEGERCLASS, BOOLEANCLASS, THREADCLASS, RUNNABLEINTERFACE,
    EVENTCLASS, EVENTQUEUECLASS, GOTOVAR
  };

  public static boolean isKeyword(String word) {
    for (int i = 0; i < keywords.length; i++) {
      if (keywords[i].equals(word))
        return true;
    }
    return false;
  }

  public static Set<String> getKeywords() {
    return new HashSet<>(Arrays.asList(keywords));
  }

  /**
   * Determine the smallest integer type that can keep track of a
   * number of values (positively).
   *
   * @param numberOfValues number of values the numeric type has to hold
   * @return the minimal integer type for keeping track of {@link numberOfValues}
   */
  public static String getMinimalType(int numberOfValues) {
    if (numberOfValues > Short.MAX_VALUE)
      return JKeywords.INT;
    if (numberOfValues > Byte.MAX_VALUE)
      return JKeywords.SHORT;
    return JKeywords.BYTE;
  }
}
