package javacode;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Representation of a label.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class JLabel {
  private String name;

  public JLabel(String name) {
    this.name = name;
  }

  public JLabel() {
    this("");
  }

  public String getName() {
    return this.name;
  }

  public boolean isEmpty() {
    return "".equals(this.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      JLabel other = (JLabel)object;
      return Objects.equals(this.name, other.name);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.name;
  }
}
