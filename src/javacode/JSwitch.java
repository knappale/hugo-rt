package javacode;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Representation of a switch.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class JSwitch extends JStatement {
  private String expression;
  private List<JCase> cases = new ArrayList<>();
  private @Nullable JBlockStatement defaultBlockStatement;

  /**
   * Create a new switch.
   */
  public JSwitch(String expression, List<JCase> cases, @Nullable JBlockStatement defaultBlockStatement) {
    this.expression = expression;
    this.cases.addAll(cases);
    this.defaultBlockStatement = defaultBlockStatement;   
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  boolean isUnconditionalJump() {
    JBlockStatement defaultStatement = this.defaultBlockStatement;
    return this.cases.stream().allMatch(JCase::isUnconditionalJump) && (defaultStatement == null || defaultStatement.isUnconditionalJump());
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");

    resultBuilder.append(indent);
    resultBuilder.append("switch (");
    resultBuilder.append(expression);
    resultBuilder.append(") {\n");
    indent = indent + "  ";

    String separator = "";
    for (JCase aCase : this.cases) {
      resultBuilder.append(separator);
      resultBuilder.append(aCase.statement(indent, false, false));
      separator = "\n";
    }

    JBlockStatement defaultBlockStatement = this.defaultBlockStatement;
    if (defaultBlockStatement != null) {
      resultBuilder.append(separator);
      resultBuilder.append("default: ");
      resultBuilder.append(defaultBlockStatement.statement(indent, true, false));
    }

    indent = indent.substring(0, indent.length()-2);
    resultBuilder.append("\n");
    resultBuilder.append(indent);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
