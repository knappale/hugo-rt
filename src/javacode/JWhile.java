package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a while loop.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba </A>
 */
@NonNullByDefault
public class JWhile extends JStatement {
  private String condition;
  private JStatement statement;

  /**
   * Create a new while loop.
   */
  public JWhile(String condition, JStatement statement) {
    this.condition = condition.trim();
    this.statement = statement;
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  boolean isUnconditionalJump() {
    return false;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("while (");
    resultBuilder.append(this.condition);
    resultBuilder.append(") ");
    resultBuilder.append(statement.statement(indent + "  ", true, false));

    return resultBuilder.toString();
  }
}
