package javacode;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Representation of a choice.
 * 
 * This is represented by a while loop with <CODE>true</CODE> as condition and
 * a <CODE>break</CODE> as its final statement.  The code has the following form
 * 
 * <PRE>
 *   label:
 *   while (true) {
 *     if (branch1.condition) {
 *       branch1.statement
 *       break label;
 *     }
 *     if (branch2.condition2) {
 *       branch2.statement
 *       break label;
 *     }
 *     ...
 *     elseBranch.statement
 *     break label;
 *   }
 * </PRE>
 *
 * We expect a choice to be exitable if there is at least one statement in it
 * which can be exited (i.e., is not extended by a <CODE>break</CODE>).
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class JChoice extends JBlockStatement {
  private JLabel label;
  private List<JBranch> branches = new ArrayList<>();
  private JBranch elseBranch;
  private boolean isUnconditionalJump = true;

  /**
   * Create a new choice.
   */
  public JChoice(JLabel label, List<JBranch> branches, @Nullable JBranch elseBranch) {
    this.label = label;
    for (JBranch branch : branches)
      this.addBranch(branch);
    if (elseBranch == null) {
      elseBranch = JBranch.unguarded(JCycle.blockStatement(new JBreak(this.label)));
      this.isUnconditionalJump = false;
    }
    else {
      if (!elseBranch.hasUnconditionalJump()) {
        elseBranch = elseBranch.getExtended(new JBreak(this.label));
        this.isUnconditionalJump = false;
      }
    }
    this.elseBranch = elseBranch;   
  }

  /**
   * Add branch to this choice.
   *
   * @param branch branch, i.e., a conditional statement
   */
  private void addBranch(JBranch branch) {
    if (!branch.hasUnconditionalJump()) {
      branch = branch.getExtended(new JBreak(this.label));
      this.isUnconditionalJump = false;
    }
    this.branches.add(branch);
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  boolean isUnconditionalJump() {
    return this.isUnconditionalJump;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");

    resultBuilder.append(indent);
    resultBuilder.append(this.label + ":\n");
    resultBuilder.append(indent);
    resultBuilder.append("while (true) {\n");
    indent = indent + "  ";

    String separator = "";
    for (JBranch branch : this.branches) {
      resultBuilder.append(separator);
      resultBuilder.append(branch.statement(indent, false, false));
      separator = "\n";
    }
    resultBuilder.append(separator);
    resultBuilder.append(this.elseBranch.statement(indent, false, false));

    indent = indent.substring(0, indent.length()-2);
    resultBuilder.append("\n");
    resultBuilder.append(indent);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
