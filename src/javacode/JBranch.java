package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Representation of a branch.  The code has the following form:
 *
 * <PRE> 
 *   if (branch.condition) {
 *     branch.statement
 *   }
 * </PRE>
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class JBranch extends JCycle {
  private String guard;
  private JCycle cycle;
  
  private JBranch(JCycle cycle) {
    super("");
    this.guard = "";
    this.cycle = cycle;
  }

  public static JBranch unguarded(JCycle cycle) {
    JBranch b = new JBranch(cycle);
    return b;
  }

  public static JBranch unguarded(JBlockStatement blockStatement) {
    return unguarded(blockStatement.asJCycle());
  }

  public static JBranch guarded(String guard, JCycle cycle) {
    JBranch b = new JBranch(cycle);
    b.guard = guard;
    return b;
  }

  public static JBranch guarded(String guard, JBlockStatement blockStatement) {
    return guarded(guard, blockStatement.asJCycle());
  }

  @Override
  public JBranch asBranch() {
    return this;
  }

  public JBranch getExtended(JBlockStatement blockStatement) {
    if ("".equals(this.guard))
      return JBranch.unguarded(JCycle.sequential(this.cycle, JCycle.blockStatement(blockStatement)));
    else
      return JBranch.guarded(this.guard, JCycle.sequential(this.cycle, JCycle.blockStatement(blockStatement)));
  }

  @Override
  boolean isEmpty() {
    return "".equals(this.guard) && this.cycle.isEmpty();
  }

  public boolean hasEmptyCycle() {
    return this.cycle.isEmpty();
  }

  @Override
  boolean isUnconditionalJump() {
    if ("".equals(this.guard) || JKeywords.TRUE.equals(this.guard))
      return this.cycle.isUnconditionalJump();
    return false;
  }

  /**
   * @return whether this branch is terminated by an unconditional jump
   */
  boolean hasUnconditionalJump() {
    return this.cycle.isUnconditionalJump();
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if ("".equals(this.guard)) {
      resultBuilder.append(this.cycle.statement(indent, inLine, emptyLineAbove));
      return resultBuilder.toString();
    }

    if (inLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append("if (");
    resultBuilder.append(this.guard);
    resultBuilder.append(") {\n");
    resultBuilder.append(this.cycle.statement(indent + "  ", false, false));
    resultBuilder.append("\n");
    resultBuilder.append(indent);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
