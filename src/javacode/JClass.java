package javacode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * Represents a Java class.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class JClass {
  private String packageDeclaration = "";
  private Set<String> importedPackages = new HashSet<>();
  private String modifiers = "";
  private String name = "";
  private String extendedClass = "";
  public List<String> implementedInterfaces = new ArrayList<>();
  private List<JAttribute> attributes = new ArrayList<>();
  private List<JMethod> methods = new ArrayList<>();

  /**
   * Constructor with name and modifiers.
   * 
   * A visibility modifier has to be put in first place of modifiers.
   * 
   * @param name name of the class
   * @param modifiers white space seperated string of modifiers
   */
  public JClass(String packageDeclaration, String name, String modifiers) {
    this.packageDeclaration = packageDeclaration.trim();
    String trimmedName = name.trim();
    assert (trimmedName != null);
    this.name = trimmedName;
    this.modifiers = modifiers.trim();
  }

  /**
   * @return class's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return class's qualified name
   */
  public String getQualifiedName() {
    if (this.packageDeclaration.equals(""))
      return this.name;
    return this.packageDeclaration + "." + this.name;
  }

  /**
   * Add a package import.
   * 
   * @param importedPackage name of package to be imported
   */
  public void addImportingPackage(String importedPackage) {
    this.importedPackages.add(importedPackage.trim());
  }

  /**
   * Set the class which this class inherits from.
   * 
   * @param className name of class to extend
   */
  public void setExtendingClass(String className) {
    this.extendedClass = className.trim();
  }

  /**
   * Add the name of an interface which this class will implement.
   * 
   * @param interfaceName name of interface to implement
   */
  public void addImplementedInterface(String interfaceName) {
    this.implementedInterfaces.add(interfaceName.trim());
  }

  /**
   * Add an attribute.
   * 
   * @param attribute attribute to add
   */
  public void addAttribute(JAttribute attribute) {
    this.attributes.add(attribute);
  }

  /**
   * Add a collection of attributes.
   * 
   * @param attributes collection of attributes to add
   */
  public void addAttributes(Collection<JAttribute> attributes) {
    this.attributes.addAll(attributes);
  }

  /**
   * @return attributes of this class
   */
  public Collection<JAttribute> getAttributes() {
    return this.attributes;
  }

  /**
   * Add a constructor.
   * 
   * @param modifiers white-space separated list of modifiers, visibility in first place
   * @param parameters list of parameters
   * @param block constructor body
   */
  public void addConstructor(String modifiers, List<JParameter> parameters, JBlock block) {
    this.addMethod(new JMethod(this.name, modifiers, "", parameters, block));
  }

  /**
   * Add a method.
   * 
   * @param method method to add
   */
  public void addMethod(JMethod method) {
    methods.add(method);
  }

  /**
   * Add a collection of methods.
   * 
   * @param methods collection of methods to add
   */
  public void addMethods(Collection<JMethod> methods) {
    this.methods.addAll(methods);
  }

  /**
   * @return methods of this class
   */
  public Collection<JMethod> getMethods() {
    return methods;
  }
  
  /**
   * Determine Java code representation of this class.
   * 
   * @param indent the indent which will be put in front of every line
   */
  public String declaration() {
    StringBuilder resultBuilder = new StringBuilder();
    String indent = "  ";

    if (!this.packageDeclaration.equals("")) {
      resultBuilder.append("package ");
      resultBuilder.append(this.packageDeclaration);
      resultBuilder.append(";\n");
      resultBuilder.append("\n");
    }

    for (String importedPackage : this.importedPackages) {
      resultBuilder.append("import ");
      resultBuilder.append(importedPackage);
      resultBuilder.append(";\n");
    }
    if (!this.importedPackages.isEmpty())
      resultBuilder.append("\n");
    resultBuilder.append("\n");

    if (!this.modifiers.equals("")) {
      resultBuilder.append(this.modifiers);
      resultBuilder.append(" ");
    }
    resultBuilder.append("class ");
    resultBuilder.append(this.name);
    resultBuilder.append(" ");
    if (!this.extendedClass.equals("")) {
      resultBuilder.append("extends ");
      resultBuilder.append(this.extendedClass);
      resultBuilder.append(" ");
    }
    if (!this.implementedInterfaces.isEmpty()) {
      resultBuilder.append("implements ");
      resultBuilder.append(Formatter.separated(this.implementedInterfaces, ", "));
      resultBuilder.append(" ");
    }
    resultBuilder.append("{\n");

    boolean pending = false;
    if (!this.attributes.isEmpty()) {
      for (JAttribute attribute : this.attributes) {
        resultBuilder.append(indent);
        resultBuilder.append(attribute.declaration());
        resultBuilder.append("\n");
      }
      pending = true;
    }

    if (!this.methods.isEmpty()) {
      if (pending)
        resultBuilder.append("\n");
      String sep = "";
      for (JMethod method : this.methods) {
        resultBuilder.append(sep);
        resultBuilder.append(method.declaration(indent));
        resultBuilder.append("\n");
        sep = "\n";
      }
    }

    resultBuilder.append("}\n");
    
    return resultBuilder.toString();
  }
}
