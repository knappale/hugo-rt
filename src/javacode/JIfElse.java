package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Java conditional
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class JIfElse extends JStatement {
  private String condition;
  private JStatement thenStatement;
  private @Nullable JStatement elseStatement = null;

  /**
   * Create a new conditional from a condition, a then statement, and
   * an optional else statement.
   * 
   * @param condition boolean expression
   * @param thenStatement statement executed if {@code condition} evaluates to {@code true}
   * @param elseStatement statement executed if {@code condition} evaluates to {@code false}
   */
  public JIfElse(String condition, JStatement thenStatement, @Nullable JStatement elseStatement) {
    this.condition = condition;
    this.thenStatement = thenStatement;
    if (elseStatement != null && !elseStatement.isEmpty())
      this.elseStatement = elseStatement;
  }

  @Override
  boolean isEmpty() {
    return false;
  }

  @Override
  boolean isUnconditionalJump() {
    return false;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");

    resultBuilder.append(indent);
    resultBuilder.append("if (");
    resultBuilder.append(this.condition);
    resultBuilder.append(") ");
    resultBuilder.append(this.thenStatement.statement(indent + "  ", true, false));

    JStatement elseStatement = this.elseStatement;
    if (elseStatement != null && !elseStatement.isEmpty()) {
      resultBuilder.append("\n");
      resultBuilder.append(indent);
      resultBuilder.append("else ");
      resultBuilder.append(elseStatement.statement(indent + "  ", true, false));
    }

    return resultBuilder.toString();
  }
}
