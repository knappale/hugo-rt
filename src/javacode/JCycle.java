package javacode;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * A cycle is a block statement encapsulating a head, which is a block statement,
 * and a list of targets, which again are cycles.
 * 
 * A cycle containing only a head but no targets is rendered as a simple block statement,
 * whereas a cycle containing also block statements is rendered as a while statement
 * containing a switch statement over the goto-variable with the targets as cases.
 * 
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class JCycle extends JBlockStatement {
  protected String label;
  // @invariant cases.isEmpty() implies head != null
  private @Nullable JBlockStatement head = null;
  private List<JTarget> cases = new ArrayList<>();

  /**
   * Create a new labelled cycle.
   * 
   * @param label a label name
   */
  protected JCycle(String label) {
    this.label = label;
  }

  /**
   * Create a new cycle with a block statement as its head.
   * 
   * @param blockStatement a block statement
   * @return a cycle with {@code blockStatement} as its head
   */
  public static JCycle blockStatement(JBlockStatement blockStatement) {
    JCycle c = new JCycle("");
    c.head = blockStatement;
    return c;
  }

  /**
   * Create a new cycle with a target.
   * 
   * @param target a target cycle
   * @return a cycle with {@code target} as its only target
   */
  public static JCycle target(JTarget target) {
    JCycle c = new JCycle("");
    c.head = null;
    c.cases.add(target);
    return c;
  }

  /**
   * Combine two cycles sequentially.
   * 
   * The algorithm is as follows: If the left cycle contains no targets, the
   * combined cycle contains as its head the head of the left cycle with the
   * head of the right cycle sequentially composed and all targets of the right
   * cycle. If the left cycle contains targets, the combined cycle contains as
   * head the head of the left cycle and as targets of the left and the right
   * cycle with the head of the right cycle sequentially composed to the last
   * target cycle of the left cycle.
   * 
   * @param left a cycle
   * @param right a cycle
   * @return a new cycle obtained from {@code left} and {@code right}
   * by the algorithm above
   */
  public static JCycle sequential(JCycle left, JCycle right) {
    JCycle seq = new JCycle(left.label.isEmpty() ? right.label : left.label);

    if (left.cases.isEmpty()) {
      if (left.head != null) {
        if (right.head != null) {
          JSequence head = new JSequence();
          head.addBlockStatement(requireNonNull(left.head));
          head.addBlockStatement(requireNonNull(right.head));
          seq.head = head;
          seq.cases.addAll(right.cases);
          return seq;
        }
        seq.head = left.head;
        seq.cases.addAll(right.cases);
        return seq;
      }
      seq.head = right.head;
      seq.cases.addAll(right.cases);
      return seq;
    }
    else {
      seq.head = left.head;
      if (right.head != null) {
        seq.cases.addAll(left.cases.subList(0, left.cases.size()-1));
        seq.cases.add(left.cases.get(left.cases.size()-1).getExtended(requireNonNull(right.head)));
      }
      else {
        seq.cases.addAll(left.cases);
      }
      seq.cases.addAll(right.cases);
      return seq;
    }
  }

  /**
   * @return a branch out of this cycle
   */
  public JBranch asBranch() {
    return JBranch.unguarded(this);
  }

  /**
   * @return this cycle
   */
  @Override
  public JCycle asJCycle() {
    return this;
  }

  /**
   * Back-patch a set of goto statements in this cycle.
   * 
   * This cycle and all cycles inside this cycle will also be labelled, such that
   * the goto statements will jump to the right blocks.
   * 
   * @param gotoStatements a set of goto statements
   * @return whether the cycle has been initialised by the back-patching process,
   * i.e., has got a cycle label
   */
  public boolean backpatch(String cycleLabel, Set<JGoto> gotoStatements) {
    boolean initialised = false;
    for (JCycle target : cases) {
      for (JGoto jGoto : gotoStatements) {
        if (jGoto.matches(target.label)) {
          if (this.label.isEmpty()) {
            this.label = cycleLabel;
            initialised = true;
          }
          jGoto.setCycleLabel(this.label);
        }
      }
    }
    return initialised;
  }

  @Override
  boolean isEmpty() {
    JBlockStatement head = this.head;
    return this.cases.isEmpty() && (head == null || head.isEmpty());
  }

  @Override
  boolean isUnconditionalJump() {
    if (this.cases.isEmpty())
      return requireNonNull(this.head).isUnconditionalJump();
    else
      return true;
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();
    
    if (this.cases.isEmpty()) {
      resultBuilder.append(requireNonNull(this.head).statement(indent, inLine, emptyLineAbove));
      return resultBuilder.toString();
    }

    String indent1 = indent + "  ";
    String indent2 = indent1 + "  ";
    String indent3 = indent2 + "  ";

    if (inLine)
      resultBuilder.append("\n");
    resultBuilder.append(indent);

    resultBuilder.append(JKeywords.GOTOVAR + " = 0;\n");
    if (!this.label.equals("")) {
      resultBuilder.append(indent);
      resultBuilder.append(this.label + ":\n");
    }
    resultBuilder.append(indent);
    resultBuilder.append("while (true) {\n");
    resultBuilder.append(indent1);
    resultBuilder.append("switch (" + JKeywords.GOTOVAR + ") {\n");
    if (this.head != null) {
      resultBuilder.append(indent2);
      resultBuilder.append("case 0:");
      resultBuilder.append(requireNonNull(this.head).statement(indent3, true, false));
      resultBuilder.append("\n");
    }
    for (JTarget target : this.cases) {
      resultBuilder.append(target.statement(indent2, false, false));
      resultBuilder.append("\n");
    }
    resultBuilder.append(indent1);
    resultBuilder.append("}\n");
    resultBuilder.append(indent1);
    if (!this.label.equals(""))
      resultBuilder.append("break " + this.label + ";\n");
    else
      resultBuilder.append("break;\n");
    resultBuilder.append(indent);
    resultBuilder.append("}");

    return resultBuilder.toString();
  }
}
