package javacode;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import util.Formatter;


/**
 * Representation of a Java method.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class JMethod {
  private String type;
  private String modifiers;
  private String name;
  private List<JParameter> parameters = new ArrayList<>();
  private JBlock body;

  /**
   * Create a method with a method body.
   * 
   * @param name name of method
   * @param modifiers white-space separated list of modifiers, visibility in first place
   * @param ret return type of the method
   * @param parameters set of parameters
   * @param block method body
   */
  public JMethod(String name, String modifiers, String type, List<JParameter> parameters, JBlock block) {
    this.name = name.trim();
    this.modifiers = modifiers.trim();
    this.type = type.trim();
    this.parameters = parameters;
    this.body = block;
  }

  /**
   * Create a method with an empty method body.
   * 
   * @param name name of method
   * @param modifiers white-space seperated list of modifiers, visibility in first place
   * @param type return type of the method
   * @param parameters list of parameters
   */
  public JMethod(String name, String modifiers, String type, List<JParameter> parameters) {
    this(name, modifiers, type, parameters, new JBlock());
  }

  /**
   * @return method's body
   */
  public JBlock getBody() {
    return this.body;
  }

  /**
   * @return method's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return method's parameters
   */
  public List<JParameter> getParameters() {
    return this.parameters;
  }

  /**
   * @return method's return type
   */
  public String getType() {
    return this.type;
  }

  /**
   * @return method's modifiers
   */
  public String getModifiers() {
    return this.modifiers;
  }

  /**
   * Determine Java code of this method.
   * 
   * @param indent indent
   * @return Java code of this method, indented by {@code indent}
   */
  public String declaration(String indent) {
    StringBuilder resultBuilder = new StringBuilder();
    String indent1 = indent + "  ";
    
    resultBuilder.append(indent);
    if (!this.modifiers.equals("")) {
      resultBuilder.append(this.modifiers);
      resultBuilder.append(" ");
    }
    if (!this.type.equals("")) {
      resultBuilder.append(this.type);
      resultBuilder.append(" ");
    }
    resultBuilder.append(this.name);
    resultBuilder.append("(");
    resultBuilder.append(Formatter.separated(this.parameters, parameter -> parameter.declaration(), ", "));
    resultBuilder.append(") ");
    resultBuilder.append(body.statement(indent1, true, false));
    return resultBuilder.toString();
  }
}
