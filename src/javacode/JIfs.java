package javacode;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Representation of a possibly iterated conditional statement. The code
 * has the following form:
 *
 * <PRE> 
 *   if (branch1.condition) {
 *     branch1.statement
 *   } else
 *   if (branch2.condition) {
 *     branch2.statement
 *   } else
 *   ...
 *   elseStatement
 * </PRE>
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 */
@NonNullByDefault
public class JIfs extends JStatement {
  private List<JBranch> branches = new ArrayList<>();
  private @Nullable JStatement elseStatement = null;

  /**
   * Create a new (iterated) conditional with an optional else branch.
   * 
   * @param branches a list of branches
   * @param elseBranch an else branch
   */
  public JIfs(List<JBranch> branches, @Nullable JBranch elseBranch) {
    this.branches.addAll(branches);
    if (elseBranch != null && !elseBranch.hasEmptyCycle())
      this.elseStatement = elseBranch.getStatement();
  }

  /**
   * Create a new conditional from a condition, a then statement, and
   * an optional else statement.
   * 
   * @param condition boolean expression
   * @param thenBlockStatement statement executed if {@code condition} evaluates to {@code true}
   * @param elseStatement statement executed if {@code condition} evaluates to {@code false}
   */
  public JIfs(String condition, JBlockStatement thenBlockStatement, @Nullable JStatement elseStatement) {
    this.branches.add(JBranch.guarded(condition, thenBlockStatement.asJCycle()));
    if (elseStatement != null && !elseStatement.isEmpty())
      this.elseStatement = elseStatement;
  }

  /**
   * Create a new conditional from a then branch and an optional else statement.
   *
   * @param thenBranch then branch, containing the if-condition
   * @param elseStatement statement executed if condition of {@code thenBranch} evaluates to {@code false}
   */
  public JIfs(JBranch thenBranch, @Nullable JStatement elseStatement) {
    this.branches.add(thenBranch);
    if (elseStatement != null && !elseStatement.isEmpty())
      this.elseStatement = elseStatement;
  }

  @Override
  boolean isEmpty() {
    JStatement elseStatement = this.elseStatement;
    return this.branches.stream().allMatch(JBranch::isEmpty) && (elseStatement == null || elseStatement.isEmpty());
  }

  @Override
  boolean isUnconditionalJump() {
    JStatement elseStatement = this.elseStatement;
    if (elseStatement == null)
      return false;
    return this.branches.stream().allMatch(b -> b.isUnconditionalJump()) && elseStatement.isUnconditionalJump();
  }

  @Override
  public String statement(String indent, boolean inLine, boolean emptyLineAbove) {
    StringBuilder resultBuilder = new StringBuilder();

    if (inLine)
      resultBuilder.append("\n");

    String separator = "";
    for (JBranch branch : this.branches) {
      resultBuilder.append(separator);
      resultBuilder.append(branch.statement(indent, false, false));
      separator = " else \n";
    }
    JStatement elseStatement = this.elseStatement;
    if (elseStatement != null && !elseStatement.isEmpty()) {
      resultBuilder.append("\n");
      resultBuilder.append(indent);
      resultBuilder.append("else ");
      resultBuilder.append(elseStatement.statement(indent + "  ", true, false));
    }

    return resultBuilder.toString();
  }
}
