package runtime;


/**
 * An event is a signal, call, time, or completion event that a Hugo/RT 
 * Java class can receive via its event queue.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class Event {
  public static class Call extends Event {
    private boolean isActive = false;

    Call(int id, Object... args) {
      super(id, args);
      this.isActive = true;
    }

    void waitForAcknowledgement() {
      while (this.isActive) {
        synchronized (this) {
          try {
            this.wait();
          }
          catch (InterruptedException e) {
          }
        }
      }
    }

    void acknowledge() {
      synchronized (this) {
        this.isActive = false;
        this.notify();
      }
    }
  }

  int id;
  Object[] args = null;

  Event(int id, Object... args) {
    this.id = id;
    this.args = args;
  }

  /**
   * @return this event's id
   */
  public int getId() {
    return this.id;
  }

  /**
   * Determine this event's argument for a given number.
   *
   * @param i argument number
   * @return this event's {@link i}'s argument or {@code null} if there
   *         is no {@link i}'s argument
   */
  public Object getArgument(int i) {
    if (i < 0 || this.args == null || i >= this.args.length)
      return null;
    return this.args[i];
  }

  /**
   * @return all arguments of this event
   */
  public Object[] getArguments() {
    return this.args;
  }

  @Override
  public int hashCode() {
    return this.id + (this.args == null ? 0 : this.args.hashCode());
  }

  @Override
  public boolean equals(Object object) {
    if (object == null)
      return false;
    try {
      Event other = (Event)object;
      if (this.id != other.id)
        return false;
      if ((this.args == null && other.args != null) ||
          (this.args != null && other.args == null))
        return false;
      if (this.args == null && other.args == null)
        return true;
      if (this.args.length != other.args.length)
        return false;
      for (int i = 0; i < this.args.length; i++)
        if (!this.args[i].equals(other.args[i]))
          return false;
      return true;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }
        
  @Override
  public String toString() {
    StringBuilder resultBuilder = new StringBuilder("Event ");
    resultBuilder.append(this.id);
    resultBuilder.append("(");
    String separator = "";
    for (Object arg : this.args) {
      resultBuilder.append(separator);
      resultBuilder.append(arg);
      separator = ", ";
    }
    resultBuilder.append(")");
    return resultBuilder.toString();
  }
}
