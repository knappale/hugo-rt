package runtime;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.eclipse.jdt.annotation.NonNull;


/**
 * The event queue manages all events which are received
 * from other classes by reception or call. It also manages events that
 * originate from the referring state machine itself.
 * 
 * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de>Max Raba</A>
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class EventQueue {
  public static boolean DEBUG = true;
  public final void debug(String msg) {
    if (DEBUG) {
      System.err.print("[DEBUG] ");
      System.err.print(this.toString());
      System.err.print(" ");
      System.err.println(msg);
    }
  }

  /**
   * Queue implementation.
   * 
   * This class deliberately contains no synchronisation: All access must be guarded
   * through synchronisation on the level of the class {@code EventQueue}.
   */
  private class Queue<T> {
    @NonNull Stack<T> front;
    @NonNull Stack<T> rear;

    /**
     * Create a new queue.
     */
    public Queue() {
      this.rear = new Stack<>();
      this.front = new Stack<>();
    }

    /**
     * Put an object into the queue.
     * 
     * @param object an object
     */
    public void insert(T object) {
      this.rear.push(object);
    }

    /**
     * Add an object as first element to the queue.
     * 
     * @param object an object
     */
    public void insertFront(T object) {
      this.front.push(object);
    }

    /**
     * Get an object from the queue.
     * 
     * @return an object, which has been dequeued, or {@code null} if queue is empty
     */
    public T get() {
      if (this.front.isEmpty()) {
        this.front.addAll(this.rear);
        this.rear.clear();
      }
      try {
        return this.front.pop();
      }
      catch (EmptyStackException ese) {
        throw new EmptyQueueException();
      }
    }

    /**
     * Remove all occurrences of an object from the queue.
     * 
     * @param object an object
     */
    public void removeAll(T object) {
      while (this.front.contains(object))
        this.front.remove(object);
      while (this.rear.contains(object))
        this.front.remove(object);
    }

    /**
     * @return whether this queue is empty
     */
    public boolean isEmpty() {
      return this.rear.isEmpty() && this.front.isEmpty();
    }

    @Override
    public String toString() {
      if (this.front.isEmpty()) {
        this.front.addAll(this.rear);
        this.rear.clear();
      }
      return this.front.toString();
    }
  }

  /**
   * Empty queue exception.
   */
  @SuppressWarnings("serial")
  private class EmptyQueueException extends EmptyStackException {
    public EmptyQueueException() {
      super();
    }
  }

  /**
   * Timer for a time event.
   * 
   * When its thread has been started, a timer will place a time event into its
   * event queue after a certain time.
   * 
   * @author <A HREF="mailto:Maximilian.Raba@ifi.lmu.de">Max Raba</A>
   */
  static class Timer extends Thread {
    private int id;
    private int time;
    private EventQueue queue;
    private boolean isActive = true;

    /**
     * Create a new event timer.
     * 
     * @param id time event id to place into queue
     * @param time (earliest) moment in time to raise the event
     * @param queue event queue where the time event will be queued
     */
    public Timer(int id, int time, EventQueue queue) {
      this.id = id;
      this.time = time;
      this.queue = queue;
    }

    /**
     * Start the timer and eventually place the time event into the event queue.
     */
    public void run() {
      if (this.time > 0) {
        synchronized (this) {
          try {
            this.wait(this.time);
          }
          catch (InterruptedException e) {
          }
        }
      }
      if (this.isActive)
        this.queue.insertTime(this.id);
    }

    /**
     * Cancel the timer.
     */
    public void cancel() {
      this.isActive = false;
      synchronized (this) {
        this.notify();
      }
    }
  }

  private Event currentEvent = null;
  private EventQueue sender = null;
  private Queue<Event> externalEventQueue = new Queue<>();
  private Queue<Integer> internalEventIdQueue = new Queue<>();
  private Queue<Event> deferredEventQueue = new Queue<>();
  private Set<Integer> waitStateEventIds = new HashSet<>();
  private boolean chosen = false;
  /**
   * When waiting for an acknowledgement for {@code id}, {@code waitForAcknowledgement} is
   * set to plus {@code id} (strictly positive); when an acknowledgement for {@code id} arrives,
   * {@code waitForAcknowledgement} is set to minus {@code id} (strictly negative)
   */
  private int waitForAcknowledgement = 0;
  private Set<Integer> completed = new HashSet<>();
  private Set<Integer> timedOut = new HashSet<>();
  private Map<Integer, Timer> timers = new HashMap<>();

  /**
   * Create a new event queue.
   */
  public EventQueue() {
  }

  /**
   * Create a new event queue with a set of wait state event ids.
   *
   * @param waitStateEventIds a set of wait state event ids
   */
  public EventQueue(Set<Integer> waitStateEventIds) {
    this.waitStateEventIds.addAll(waitStateEventIds);
  }

  /**
   * Insert a signal event into the event queue.
   * 
   * As an external event, the signal event is inserted at the rear
   * of the external event queue.
   * 
   * @param id the signal event id
   * @param args the signal event arguments
   */
  public void insertSignal(int id, Object... args) {
    Event event = new Event(id, args);

    synchronized (this) {
      debug("inserting signal " + event + " ...");
      this.externalEventQueue.insert(event);
      this.notify();
    }
  }

  /**
   * Insert a call event into the event queue.
   * 
   * As an external event, the call event is inserted at the rear
   * of the external event queue.
   * 
   * @param sender the event queue of the calling object
   * @param id the call event id
   * @param args the call event arguments
   */
  public void insertCall(EventQueue sender, int id, Object... objs) {
    // Since we do not wait for an acknowledgement we use a simple Event, not a Event.Call
    Event event = new Event(id, objs);

    synchronized (this) {
      debug("inserting call " + event + " ...");
      this.sender = sender;
      this.externalEventQueue.insert(event);
      this.notify();
    }
  }

  /**
   * Insert a call event into the event queue and wait
   * for its acknowledgement.
   * 
   * As an external event, the call event is inserted at the rear
   * of the external event queue.
   * 
   * @param id the call event id
   * @param args the call event arguments
   */
  public void insertCall(int id, Object... objs) {
    Event.Call callEvent = new Event.Call(id, objs);

    synchronized (this) {
      debug("inserting call " + callEvent + " ...");
      this.externalEventQueue.insert(callEvent);
      this.notify();
    }

    debug("waiting for acknowledgement for call " + callEvent + " ...");
    callEvent.waitForAcknowledgement();
  }

  /**
   * Insert a completion event into the event queue.
   * 
   * As an internal event, the completion event is inserted
   * at the front of the internal event queue.
   * 
   * @param id the completion event id
   */
  public void insertCompletion(int id) {
    synchronized (this) {
      debug("inserting completion " + id + " ...");
      this.internalEventIdQueue.insertFront(id);
      this.notify();
    }
  }

  /**
   * Insert a time event into the event queue.
   * 
   * As an internal event, the time event is inserted
   * at the front of the internal event queue.
   * 
   * @param id the time event id
   */
  private void insertTime(int id) {
    synchronized (this) {
      debug("inserting time " + id + " ...");
      this.internalEventIdQueue.insertFront(id);
      this.timers.remove(id);
      this.timedOut.add(id);
      this.notify();
    }
  }

  /**
   * Fetch the next event from the event queue.
   * 
   * Acknowledgements come first.  Then internal events are prioritised.
   * Deferred events will only be fetched if the state machine has executed
   * {@code chosen} since the last deferring of an event.
   *
   * @return the fetched event
   */
  public synchronized Event fetch() {
    if (this.waitForAcknowledgement != 0) {
      // Wait for an acknowledgement
      debug("waiting for acknowledgement of " + this.waitForAcknowledgement + " ...");
      while (this.waitForAcknowledgement > 0) {
        try {
          this.wait();
        }
        catch (InterruptedException e) {
        }
      }
      Event ack = new Event(-this.waitForAcknowledgement, this.currentEvent.getArguments());
      this.waitForAcknowledgement = 0;
      debug("acknowledgment " + ack);
      return ack;
    }

    debug("fetching ...");
    this.currentEvent = null;

    // For wait states a completion event may be generated
    int generatedCompletionEventId = 0;
    for (int waitStateEventId : this.waitStateEventIds) {
      if (this.completed.contains(waitStateEventId))
        generatedCompletionEventId = waitStateEventId;
    }

    // Wait for an event
    while (this.internalEventIdQueue.isEmpty() &&
           (!this.chosen || this.deferredEventQueue.isEmpty()) &&
           this.externalEventQueue.isEmpty() &&
           generatedCompletionEventId == 0) {
      try {
        this.wait();
      }
      catch (InterruptedException e) {
      }
    }
    
    // Choose an event
    if (!this.internalEventIdQueue.isEmpty()) {
      this.currentEvent = new Event(this.internalEventIdQueue.get());
    }
    else {
      if (this.chosen && !this.deferredEventQueue.isEmpty()) {
        this.currentEvent = this.deferredEventQueue.get();
      }
      else {
        if (!this.externalEventQueue.isEmpty()) {
          this.currentEvent = this.externalEventQueue.get();
        }
        else {
          this.currentEvent = new Event(generatedCompletionEventId);
        }
      }
    }

    debug("fetched " + this.currentEvent);
    return this.currentEvent;
  }

  /**
   * Defer the current event, i.e., put it into a separate deferred events queue
   * and wait for another event to be chose before considering this deferred event
   * again.
   */
  public synchronized void defer() {
    debug("deferring " + this.currentEvent + " ...");
    this.chosen = false;
    this.deferredEventQueue.insert(this.currentEvent);
  }

  /**
   * Record that some event has been chosen, i.e., deferred events can be
   * reconsidered.
   */
  public void chosen() {
    this.chosen = true;
    debug("chosen for " + this.currentEvent);
  }

  /**
   * Acknowledge the last call sent out.
   */
  public synchronized void acknowledge() {
    if (this.waitForAcknowledgement == 0) {
      if (this.currentEvent == null)
        return;
      try {
        Event.Call callEvent = (Event.Call)this.currentEvent;
        debug("acknowledging " + callEvent + " ...");
        callEvent.acknowledge();
      }
      catch (ClassCastException cce) {
      }
    }
    else {
      debug("acknowledging " + this.waitForAcknowledgement + " ...");
      this.waitForAcknowledgement = -this.waitForAcknowledgement;
      this.notify();
    }
  }

  /**
   * Set this event queue to wait for an acknowledgement of event {@code id}.
   *
   * @param id an event id
   */
  public synchronized void waitForAcknowledgement(int id) {
    this.waitForAcknowledgement = id;
  }

  /**
   * Send out an acknowledgement to the event queue of the current call
   */
  public synchronized void sendAcknowledgement() {
    if (this.sender != null)
      this.sender.acknowledge();
  }

  public synchronized void complete(int id) {
    debug("completing " + id);
    this.completed.add(id);
    this.insertCompletion(id);
  }

  public synchronized void uncomplete(int id) {
    this.completed.remove(id);
    this.internalEventIdQueue.removeAll(id);
  }

  public synchronized boolean isCompleted(int id) {
    return this.completed.contains(id);
  }

  public synchronized void startTimer(int id, int timeLow, @SuppressWarnings("unused") int timeHigh) {
    this.timedOut.remove(id);
    Timer timer = new Timer(id, timeLow, this);
    this.timers.put(id, timer);
    timer.start();
  }

  public synchronized void stopTimer(int id) {
    this.timedOut.remove(id);
    Timer timer = this.timers.get(id);
    if (timer != null) {
      timer.cancel();
      this.timers.remove(id);
    }
  }

  public synchronized boolean isTimedOut(int id) {
    return this.timedOut.contains(id);
  }

  @Override
  public String toString () {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("Queue [currentEvent = [");
    resultBuilder.append(this.currentEvent);
    resultBuilder.append("], hasSender = ");
    resultBuilder.append(this.sender != null);
    resultBuilder.append("], waitForAcknowledgement = ");
    resultBuilder.append(this.waitForAcknowledgement);
    resultBuilder.append("], externalEvents = ");
    resultBuilder.append(this.externalEventQueue);
    resultBuilder.append("], deferredEvents = [");
    resultBuilder.append(this.deferredEventQueue);
    resultBuilder.append(", internalEvents = [");
    resultBuilder.append(this.internalEventIdQueue);
    resultBuilder.append("], chosen = ");
    resultBuilder.append(this.chosen);
    resultBuilder.append("], timers = [");
    resultBuilder.append(this.timers);
    resultBuilder.append("], completed = [");
    resultBuilder.append(this.completed);
    resultBuilder.append("], timedOut = [");
    resultBuilder.append(this.timedOut);
    resultBuilder.append("]");
    return resultBuilder.toString();
  }
}
