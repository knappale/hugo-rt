#ifndef CODEGENERATOR_QUEUE_EVENT_HH
#define CODEGENERATOR_QUEUE_EVENT_HH

#include "event.hh"


namespace runtime {

class queue_event_ {
public:
  int length;
  event **mem;
  int head;
  int tail;

  explicit queue_event_(int length);
  ~queue_event_();

  void enq(event* e);
  event* deq();
  bool ismt();
};

}

#endif /*CODEGENERATOR_QUEUE_EVENT_HH*/
