#include "../include/queue_event_.hh"


namespace runtime {

queue_event_::queue_event_(int length) {
  this->length = length;
  this->mem = new event*[length];
  for (int i = 0; i < length; i++)
    this->mem[i] = nullptr;
  this->head = 0;
  this->tail = 0;
}

queue_event_::~queue_event_() {
  delete[] this->mem;
}

void queue_event_::enq(event* e) {
  if (!((this->head+1 == this->tail) || (this->head == this->length-1 && this->tail == 0))) {
    this->mem[this->head] = e;
    this->head++;
    if (this->head == this->length)
      this->head = 0;
  }
}

event* queue_event_::deq() {
  event* e = this->mem[this->tail];
  this->tail++;
  if (this->tail == this->length)
    this->tail = 0;
  return e;
}

bool queue_event_::ismt() {
  return head == tail;
}

}
