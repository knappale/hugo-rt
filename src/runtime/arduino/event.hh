#ifndef CODEGENERATOR_EVENT_HH
#define CODEGENERATOR_EVENT_HH

#define MT -1

namespace runtime {

class event {
  int id;
  void** args;
  int nargs;

public:
  event(int maxnargs);
  ~event();

  bool match(int other_id);
  int getid();
  event* setid(int id);
  void *getarg(int i);
  void invalidate();

  friend class mem_event_;
  friend class stack_event_;
};

}

#endif // CODEGENERATOR_EVENT_HH
