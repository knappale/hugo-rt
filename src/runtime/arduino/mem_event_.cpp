/**
 * Memory pool for events.
 *
 * @author Alexander Knapp
 * @version 1.0 2021-02-11
 */

#include "../include/mem_event_.hh"


namespace runtime {

mem_event_::mem_event_(int size, int maxnargs) {
  this->size = size;
  this->mem = new event*[size];
  for (int i = 0; i < size; i++)
    this->mem[i] = new event(maxnargs);
}

mem_event_::~mem_event_() {
  for (int i = 0; i < size; i++)
    delete this->mem[i];
  delete[] this->mem;
}

event* mem_event_::get(int id, int nargs, void** args) {
  for (int i = 0; i < this->size; i++) {
    if (this->mem[i]->id == MT) {
      this->mem[i]->id = id;
      this->mem[i]->nargs = nargs;
      for (int j = 0; j < nargs; j++)
        this->mem[i]->args[j] = args[j];
      return this->mem[i];
    }
  }
  return nullptr;
}

event* mem_event_::get(int id) {
  return this->get(id, 0, nullptr);
}

event* mem_event_::get(event *ev) {
  return this->get(ev->id, ev->nargs, ev->args);
}

}
