/**
  * Event pool
  *
  * @author Alexander Knapp
  * @author Steven Mueller
  * @version 1.0 2021-02-11
  */

#ifndef CODEGENERATOR_EVENTPOOL_HH
#define CODEGENERATOR_EVENTPOOL_HH

#include "Arduino.h"
#include "../include/event.hh"
#include "../include/mem_event_.hh"
#include "../include/queue_event_.hh"
#include "../include/stack_event_.hh"
#include "../include/timer.hh"


namespace runtime {

class eventpool {
private:
  mem_event_* extmem;
  mem_event_* intmem;

  event* current;
  eventpool* sender;
  int toack;
  bool ischosen;
  queue_event_* external;
  queue_event_* deferred;
  stack_event_* completions;
  stack_event_* times;
  int completed_size;
  int* completed;
  int timers_size;
  timer* timers;

public:
  eventpool(int maxnargs, int external_length, int deferred_length, int internal_length, int completed_size, int timers_size);
  ~eventpool();

  void insertsignal(int id, int nargs, void **args);
  void insertcall(eventpool* sender, int id, int nargs, void **args);
  void insertcompletion(int id);

  event* poll();
  event* fetch();

  void handled();
  void defer();
  void chosen();

  void acknowledge();
  void waitforacknowledgement(int id);
  void sendacknowledgement();

  void complete(int id);
  void uncomplete(int id);
  bool iscompleted(int id);

  void starttimer(int id, unsigned long low, unsigned long high);
  void stoptimer(int evt);
  bool istimedout(int id);

  void log(char *msg);
};

}
#endif /*CODEGENERATOR_EVENTPOOL_HH*/
