#ifndef CODEGENERATOR_MEM_EVENT_HH
#define CODEGENERATOR_MEM_EVENT_HH

#include "../include/event.hh"


namespace runtime {

class mem_event_ {
protected:
  int size;
  event **mem;

public:
  mem_event_(int size, int maxnargs);
  ~mem_event_();

  event* get(int id);
  event* get(int id, int nargs, void** args);
  event* get(event *ev);
};

}

#endif /*CODEGENERATOR_MEM_EVENT_HH*/
