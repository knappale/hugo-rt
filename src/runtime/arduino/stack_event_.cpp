#include "../include/stack_event_.hh"

namespace runtime {

stack_event_::stack_event_(int length){
  this->length = length;
  this->mem = new event*[length];
  for (int i = 0; i < length; i++)
    this->mem[i] = nullptr;
  this->top = length;
}

stack_event_::~stack_event_() {
  delete[] this->mem;
}

void stack_event_::push(event* e) {
  if (!this->top == 0) {
    this->top--;
    this->mem[this->top] = e;
  }
}

event* stack_event_::pop() {
  event *e = nullptr;
  if (this->top < this->length) {
    e = this->mem[this->top];
	this->top++;
  }
  return e;
}

void stack_event_::rem(event* e) {
  int gap = 0;
  for (int i = this->length-1; i > 0; i--) {
    if (this->mem[i]->id == e->id) {
      gap++;
      delete this->mem[i];
    }
    if (gap > 0)
      this->mem[i] = (i-gap < this->top) ? nullptr : this->mem[i-gap];
  }
  this->top -= gap;
  delete e;
}

bool stack_event_::ismt() {
  return this->top == this->length;
}

}
