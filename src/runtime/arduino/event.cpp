#include "../include/event.hh"


namespace runtime {

event::event(int maxnargs) {
  this->id = MT;
  this->nargs = 0;
  this->args = nullptr;
  if (maxnargs > 0) {
    this->args = new void*[maxnargs];
    for (int i = 0; i < maxnargs; i++)
      this->args[i] = nullptr;
  }
}

event::~event() {
  delete[] this->args;
}

bool event::match(int other_id) {
  return this->id == other_id;
}

int event::getid() {
  return this->id;
}

event* event::setid(int id) {
  this->id = id;
  return this;
}

void* event::getarg(int i) {
  return this->args[i];
}

void event::invalidate() {
  this->id = MT;
}

}
