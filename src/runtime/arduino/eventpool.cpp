/**
  * Event pool implementation
  *
  * @author Alexander Knapp
  * @author Steven Mueller
  * @version 1.0 2021-02-11
  */

#include "../include/eventpool.hh"


namespace runtime {

eventpool::eventpool(int maxnargs, int external_length, int deferred_length, int internal_length, int completed_size, int timers_size) {
  this->extmem = new mem_event_(external_length + deferred_length, maxnargs);
  this->intmem = new mem_event_(completed_size + timers_size, 0);
  this->external = new queue_event_(external_length);
  this->deferred = new queue_event_(deferred_length);
  this->completions = new stack_event_(min(internal_length, completed_size));
  this->completed_size = completed_size;
  this->completed = new int[completed_size];
  for (int i = 0; i < completed_size; i++)
    this->completed[i] = MT;
  this->times = new stack_event_(min(internal_length, timers_size));
  this->timers_size = timers_size;
  this->timers = new timer[timers_size];
  for (int i = 0; i < timers_size; i++)
    this->timers[i] = { MT, 0L, 0L };

  this->current = nullptr;
  this->sender = nullptr;
  this->toack = 0;
  this->ischosen = false;
}

eventpool::~eventpool() {
  delete this->extmem;
  delete this->intmem;
  delete this->external;
  delete this->deferred;
  delete this->completions;
  delete[] this->completed;
  delete this->times;
  delete[] this->timers;
}

void eventpool::insertsignal(int id, int nargs, void **args) {
  noInterrupts();
  event* ev = this->extmem->get(id, nargs, args);
  if (ev != nullptr)
    this->external->enq(ev);
  interrupts();
}

void eventpool::insertcall(eventpool* sender, int id, int nargs, void **args) {
  noInterrupts();
  event* ev = this->extmem->get(id, nargs, args);
  if (ev != nullptr) {
    this->sender = sender;
    this->external->enq(ev);
  }
  interrupts();
}

void eventpool::insertcompletion(int id) {
  event* ev = this->intmem->get(id);
  if (ev != nullptr)
    this->completions->push(ev);
}

event* eventpool::poll() {
  // If an acknowledgement for the last call arrived
  if (this->toack < 0) {
    // Produce an acknowledgement event with the same parameters as the current event
    this->current->setid(-this->toack);
    this->toack = 0;
    return this->current;
  }
  // If we are still waiting for an acknowledgement
  if (this->toack > 0)
    return nullptr;

  // Check for timers
  for (int i = 0; i < this->timers_size ; i++) {
    if (this->timers[i].id != MT && ((millis() - this->timers[i].st) > this->timers[i].tm)) {
      event* ev = this->intmem->get(this->timers[i].id);
      if (ev != nullptr)
        this->times->push(ev);
      this->stoptimer(this->timers[i].id);
    }
  }

  // If there is a current event
  if (this->current != nullptr)
    return this->current;

  // Choose a new event if possible
  if (!this->completions->ismt()) {
    this->current = this->completions->pop();
  }
  else {
    if (!this->times->ismt()) {
      this->current = this->times->pop();
    }
    else {
      if (!this->deferred->ismt() && this->ischosen) {
        this->current = this->deferred->deq();
      }
      else {
        if (!this->external->ismt()) {
          noInterrupts();
          this->current = this->external->deq();
          interrupts();
        }
      }
    }
  }
  return this->current;
}

event* eventpool::fetch() {
  while (this->current == nullptr)
    this->poll();

  return this->current;
}

void eventpool::handled() {
  // Invalidate the current event
  if (this->current != nullptr) {
    noInterrupts();
    this->current->invalidate();
    this->current = nullptr;
    interrupts();
  }
}

void eventpool::defer() {
  this->ischosen = false;
  if (this->current != nullptr) {
    noInterrupts();
    event* ev = this->extmem->get(this->current);
    interrupts();
    if (ev != nullptr)
      this->deferred->enq(ev);
  }
}

void eventpool::chosen() {
  this->ischosen = true;
}

void eventpool::acknowledge() {
  this->toack = -this->toack;
}

void eventpool::waitforacknowledgement(int id) {
  this->toack = id;
}

void eventpool::sendacknowledgement() {
  if (this->sender != nullptr)
    this->sender->acknowledge();
}

void eventpool::complete(int id) {
  this->insertcompletion(id);
  for (int i = 0; i < this->completed_size; i++)
    if (this->completed[i] == MT) {
      this->completed[i] = id;
      break;
    }
}

void eventpool::uncomplete(int id) {
  if (this->current != nullptr)
    this->completions->rem(this->current);
  for (int i = 0; i < this->completed_size; i++)
    if (this->completed[i] == id)
      this->completed[i] = MT;
}

bool eventpool::iscompleted(int id) {
  for (int i = 0; i < this->completed_size; i++)
    if (this->completed[i] == id)
      return true;
  return false;
}

void eventpool::starttimer(int id, unsigned long low, unsigned long high) {
  timer tmr = { id, low, millis() };
  for (int i = 0; i < this->timers_size; i++)
    if (this->timers[i].id == MT) {
      this->timers[i] = tmr;
      break;
    }
}

void eventpool::stoptimer(int id) {
  for (int i = 0; i < this->timers_size; i++)
    if (this->timers[i].id == id)
      this->timers[i] = { MT, 0L, 0L };
}

bool eventpool::istimedout(int id) {
  for (int i = 0; i < this->timers_size; i++)
    if (this->timers[i].id == id)
      return false;
  return true;
}

void eventpool::log(char* msg) {
  // Ignore the message...
}

}
