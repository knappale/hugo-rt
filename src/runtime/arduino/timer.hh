#ifndef CODEGENERATOR_TIMER_HH
#define CODEGENERATOR_TIMER_HH


namespace runtime {

typedef struct Timer {
  int id;
  unsigned long tm; // duration
  unsigned long st; // when started
} timer;

}

#endif /*CODEGENERATOR_TIMER_HH*/
