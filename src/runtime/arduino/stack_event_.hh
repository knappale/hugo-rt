#ifndef CODEGENERATOR_STACK_EVENT_HH
#define CODEGENERATOR_STACK_EVENT_HH
#include "event.hh"

namespace runtime {

class stack_event_ {
public:
  int length;
  event **mem;
  int top;

  explicit stack_event_(int length);
  ~stack_event_();

  void push(event* e);
  event* pop();
  void rem(event* e);
  bool ismt();
};

}

#endif /*CODEGENERATOR_STACK_EVENT_HH*/
