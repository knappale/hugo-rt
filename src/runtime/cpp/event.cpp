#include "event.hh"

namespace runtime {

using namespace std;

event::event(int id, int nargs, void **args) {
  this->id = id;
  for (int i = 0; i < nargs; i++)
    this->args.push_back(args[i]);
}

bool event::match(int other_id) {
  return this->id == other_id;
}

int event::getid() {
  return this->id;
}

void *event::getarg(int i) {
  return (this->args)[i];
}

event *event::copy(int id) {
  return new event(id, this->args.size(), this->args.data());
}

ostream& event::print(ostream& out) const {
  out << "event(" << this->id;
  for (auto a : this->args)
    out << ", " << a;
  out << ")";
  return out;
}

callevent::callevent(int id, int nargs, void **args) : event(id, nargs, args) {
  isactive = true;
}

callevent *callevent::copy(int id) {
  return new callevent(id, this->args.size(), this->args.data());
}

void callevent::acknowledge() {
  unique_lock<mutex> lck(mtx);
  isactive = false;
  cond.notify_all();
}

void callevent::waitforacknowledge() {
  unique_lock<mutex> lck(mtx);
  while (isactive)
    cond.wait(lck);
}

ostream& callevent::print(ostream& out) const {
  out << "callevent(" << this->id;
  for (auto a : this->args)
    out << ", " << a;
  out << ")";
  return out;
}

}
