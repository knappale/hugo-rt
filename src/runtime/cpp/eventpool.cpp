#include "eventpool.hh"
#include "timer.hh"

namespace runtime {

using namespace std;

eventpool::eventpool(int *waitIds) {
  mtx.lock();
  while (waitIds != nullptr)
    waitstates.insert(*waitIds++);
  current = nullptr;
  ischosen = false;
  sender = nullptr;
  toack = 0;
  mtx.unlock();
}

eventpool::~eventpool() {
  if (current != nullptr)
    delete current;
}

void eventpool::insertsignal(int id, int nargs, void **args) {
  event *e = new event(id, nargs, args);

  mtx.lock();
  external.push_back(e);
  cond.notify_all();
  log("Signal inserted");
  mtx.unlock();
}

void eventpool::insertcall(eventpool *sender, int id, int nargs, void **args) {
  event *e = new event(id, nargs, args);

  mtx.lock();
  this->sender = sender;
  external.push_back(e);
  cond.notify_all();
  log("Signal inserted");
  mtx.unlock();
}

void eventpool::insertcall(int id, int nargs, void **args) {
  callevent *e = new callevent(id, nargs, args);

  mtx.lock();
  external.push_back(e);
  cond.notify_all();
  log("Call inserted");
  mtx.unlock();

  e->waitforacknowledge();
}

void eventpool::insertcompletion(int id) {
  mtx.lock();
  internal.push_front(id);
  completed.insert(id);
  cond.notify_all();
  log("Completion inserted");
  mtx.unlock();
}

void eventpool::inserttime(int id) {
  mtx.lock();
  internal.push_front(id);
  timedout.insert(id);
  cond.notify_all();
  log("Time out inserted");
  mtx.unlock();
}

event *eventpool::fetch() {
  unique_lock<mutex> lck(mtx);

  if (toack != 0) {
    while (toack > 0)
      cond.wait(lck);
    event *ack = current->copy(-toack);
    toack = 0;
    log("Acknowledged");
    return ack;
  }

  if (current != nullptr) {
    delete current;
    current = nullptr;
  }

  // For wait states a completion event may be generated
  int gen_wait_id = 0;
  for (int wait_id : waitstates) {
    if (completed.find(wait_id) != completed.end())
      gen_wait_id = wait_id;
  }

  // Wait for an event
  while (internal.empty() &&
         (deferred.empty() || !ischosen) &&
         external.empty() &&
         gen_wait_id == 0) {
    cond.wait(lck);
  }

  // Choose an event
  if (!internal.empty()) {
    current = new event(internal.front());
    internal.pop_front();
  }
  else {
    if (!deferred.empty() && ischosen) {
      current = deferred.front();
      deferred.pop_front();
    }
    else {
      if (!external.empty()) {
        current = external.front();
        external.pop_front();
      }
      else {
        current = new event(gen_wait_id);
      }
    }
  }

  log("Fetched");
  return current;
}

void eventpool::defer() {
  mtx.lock();
  ischosen = false;
  deferred.push_back(current);
  mtx.unlock();
}

void eventpool::chosen() {
  ischosen = true;
}

void eventpool::acknowledge() {
  mtx.lock();
  if (toack == 0) {
    if (current != nullptr)
      current->acknowledge();
  }
  else {
    toack = -toack;
    cond.notify_all();
  }
  mtx.unlock();
  log("Acknowledged");
}

void eventpool::waitforacknowledgement(int id) {
  mtx.lock();
  toack = id;
  mtx.unlock();
}

void eventpool::sendacknowledgement() {
  mtx.lock();
  if (sender != nullptr)
    sender->acknowledge();
  mtx.unlock();
}

void eventpool::complete(int id) {
  insertcompletion(id);
}

void eventpool::uncomplete(int id) {
  mtx.lock();
  for (list<int>::iterator itr = internal.begin(); itr != internal.end(); itr++) {
    if (*itr == id) {
      internal.erase(itr);
      break;
    }
  }
  completed.erase(id);
  log("Uncompleted");
  mtx.unlock();
}

bool eventpool::iscompleted(int id) {
  mtx.lock();
  bool b = (completed.find(id) != completed.end());
  mtx.unlock();
  return b;
}

void eventpool::starttimer(int id, int low, int high) {
  timer *t = new timer(low, [=]() { this->inserttime(id); });
  mtx.lock();
  timedout.erase(id);
  timers.insert(pair<int, timer*>(id, t));
  t->start();
  log("Timer started");
  mtx.unlock();
}

void eventpool::stoptimer(int id) {
  mtx.lock();
  timedout.erase(id);
  timer *t = timers.at(id);
  if (t != nullptr) {
    t->stop();
    timers.erase(id);
    delete t;
  }
  log("Timer stopped");
  mtx.unlock();
}

bool eventpool::istimedout(int id) {
  mtx.lock();
  bool b = (timedout.find(id) != timedout.end());
  mtx.unlock();
  return b;
}

ostream& eventpool::print(ostream& out) const {
  string sep;

  out << "eventpool(current = ";
  if (current == nullptr)
    cout << "none";
  else
    cout << *current;
  cout << ", internal = ["; sep = "";
  for (auto i : internal) {
    out << sep << i;
    sep = ", ";
  }
  out << "], deferred = ["; sep = "";
  for (auto e : deferred) {
    out << sep << *e;
    sep = ", ";
  }
  out << "], external = ["; sep = "";
  for (auto e : external) {
    out << sep << *e;
    sep = ", ";
  }
  out << "], completed = ["; sep = "";
  for (auto i : completed) {
    out << sep << i;
    sep = ", ";
  }
  out << "], timers = ["; sep = "";
  for (auto p : timers) {
    out << sep << p.first;
    sep = ", ";
  }
  out << "])";
  return out;
}

void eventpool::log(string msg) const {
  cout << msg << ": " << *this << endl;
}

}
