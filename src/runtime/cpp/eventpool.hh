#ifndef EVENTPOOL_HH
#define EVENTPOOL_HH

#include <iostream>
#include <algorithm>
#include <iterator>
#include <list>
#include <set>
#include <map>
#include <mutex>
#include <condition_variable>
#include "event.hh"
#include "timer.hh"


namespace runtime {

using namespace std;

class eventpool {
  set<int> waitstates;

  list<int> internal;
  list<event *> external;
  list<event *> deferred;
  set<int> completed;
  set<int> timedout;
  map<int, timer *> timers;

  event *current;
  bool ischosen;
  eventpool *sender;
  int toack;

  mutex mtx;
  condition_variable cond;

public:
  eventpool() : eventpool(nullptr) {
  }
  eventpool(int *waitIds);
  ~eventpool();

  void insertsignal(int id, int nargs, void **args);
  void insertcall(eventpool *sender, int id, int nargs, void **args);
  void insertcall(int id, int nargs, void **args);
  void insertcompletion(int id);
  void inserttime(int id);

  event *fetch();

  void defer();
  void chosen();
  void acknowledge();
  void waitforacknowledgement(int id);
  void sendacknowledgement();

  void complete(int id);
  void uncomplete(int id);
  bool iscompleted(int id);

  void starttimer(int id, int low, int high);
  void stoptimer(int id);
  bool istimedout(int id);

  ostream& print(ostream& out) const;
  void log(string msg) const;

  friend ostream& operator<<(ostream& out, const eventpool& evtq) {
    return evtq.print(out);
  }
};

}

#endif /*EVENTPOOL_HH*/
