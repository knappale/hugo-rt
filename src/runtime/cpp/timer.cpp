#include "timer.hh"

namespace runtime {

void timer::start() {
  this->clear = false;
  std::thread t([=]() {
      if (this->clear) return;
      std::this_thread::sleep_for(std::chrono::milliseconds(this->delay));
      if (this->clear) return;
      this->fun();
  });
  t.detach();
}

void timer::stop() {
  this->clear = true;
}

}


