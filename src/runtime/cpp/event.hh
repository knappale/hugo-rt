#ifndef EVENT_HH_
#define EVENT_HH_

#include <iostream>
#include <vector>
#include <mutex>
#include <condition_variable>


namespace runtime {

using namespace std;

class event {
protected:
  int id;
  vector<void *> args;

public:
  event(int id) : event(id, 0, nullptr) {
  }
  event(int id, int nargs, void **args);
  virtual ~event() {
  }

  bool match(int other_id);
  int getid();
  void *getarg(int i);
  virtual event *copy(int id);

  virtual void acknowledge() {
  }
  virtual void waitforacknowledge() {
  }

  bool operator==(const event& other) {
    return this->id == other.id && this->args == other.args;
  }

  virtual ostream& print(ostream& out) const;

  friend ostream& operator<<(ostream& s, const event& e) {
    return e.print(s);
  }
};

class callevent : public event {
private:
  bool isactive;
  mutex mtx;
  condition_variable cond;

public:
  callevent(int id) : callevent(id, 0, nullptr) {
  }
  callevent(int id, int nargs, void **args);
  ~callevent() {
  }

  virtual callevent *copy(int id);
  virtual void acknowledge() override;
  virtual void waitforacknowledge() override;

  virtual ostream& print(ostream& out) const override;
};

}

#endif /* EVENT_HH_ */
