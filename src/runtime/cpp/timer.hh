#ifndef TIMER_HH_
#define TIMER_HH_

#include <thread>
#include <chrono>
#include <functional>


namespace runtime {

class timer {
private:
  int delay;
  std::function<void ()> fun;
  bool clear = false;

public:
  timer(int delay, std::function<void ()> fun) : delay(delay), fun(fun) {
  }
  ~timer() {
  }

  void start();
  void stop();
};

}

#endif /* TIMER_HH_ */
