package promela;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Promela data type
 *
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public final class PDataType {
  private String kind;
  private @Nullable String name;
  private List<PConstant> constants = new LinkedList<>();
  private List<PField> fields = new LinkedList<>();

  private PDataType(String kind) {
    this.kind = kind;
  }

  private static final PDataType PID = new PDataType(PKeywords.PID);
  private static final PDataType BOOL = new PDataType(PKeywords.BOOL);
  private static final PDataType BIT = new PDataType(PKeywords.BIT);
  private static final PDataType BYTE = new PDataType(PKeywords.BYTE);
  private static final PDataType SHORT = new PDataType(PKeywords.SHORT);
  private static final PDataType INT = new PDataType(PKeywords.INT);
  private static final PDataType MTYPE = new PDataType(PKeywords.MTYPE);

  /**
   * @return the pid data type
   */
  public static PDataType pidType() {
    return PID;
  }

  /**
   * @return the boolean data type
   */
  public static PDataType boolType() {
    return BOOL;
  }

  /**
   * @return the bit data type
   */
  public static PDataType bitType() {
    return BIT;
  }

  /**
   * @return the byte data type
   */
  public static PDataType byteType() {
    return BYTE;
  }

  /**
   * @return the short data type
   */
  public static PDataType shortType() {
    return SHORT;
  }

  /**
   * @return the int data type
   */
  public static PDataType intType() {
    return INT;
  }

  /**
   * Create a data type that is minimal for coping with a given number
   * of entities.
   *
   * @param size number of entities
   * @return minimal datatype for size number of entities
   */
  public static PDataType minimal(int size) {
    if (size <= 1)
      return PDataType.bitType();
    if (size <= 127)
      return PDataType.byteType();
    if (size <= 32767)
      return PDataType.shortType();
    return PDataType.intType();
  }

  /**
   * Initialise the mtype data type.
   *
   * There is only a single mtype per Promela specification.
   *
   * @param constants enumeration constants
   * @return the initialised mtype
   */
  public static PDataType mtype(Collection<PConstant> constants) {
    var d = MTYPE;
    d.constants.addAll(constants);
    return d;
  }

  /**
   * Create a user-defined (i.e. Promela structure) data type.
   *
   * @param name data type's name
   * @return user-defined type named {@code name}
   */
  public static PDataType typedef(String name, List<PField> fields) {
    var d = new PDataType(PKeywords.TYPEDEF);
    d.name = name;
    d.fields.addAll(fields);
    return d;
  }

  /**
   * @return data type's name
   */
  public String getName() {
    var name = this.name;
    if (name != null)
      return name;
    return this.kind;
  }

  /**
   * @return maximal value of this datatype
   */
  public int maxValue() {
    switch (this.kind) {
      case PKeywords.BIT: return 1;
      case PKeywords.BYTE: return 127;
      case PKeywords.SHORT: return 32767;
      case PKeywords.INT: return 2147483647;
      default: return -1;
    }
  }

  /**
   * Build an mtype or typedef declaration (i.e. Promela structure declaration),
   * every inner line prefixed by {@code prefix}.
   *
   * @param prefix line indentation
   * @return data type declaration, inner lines prefixed by {@code prefix}
   */
  public String declaration(String prefix) {
    var builder = new StringBuilder();
    switch (this.kind) {
      case PKeywords.MTYPE: {
        builder.append(PKeywords.MTYPE);
        builder.append(" = {");
        String sep = "";
        for (var it = this.constants.iterator(); it.hasNext(); sep = ", ") {
          builder.append(sep);
          builder.append(it.next().getName());
        }
        builder.append("}");
        break;
      }
      case PKeywords.TYPEDEF: {
        builder.append(PKeywords.TYPEDEF);
        builder.append(" ");
        builder.append(this.name);
        builder.append(" {");
        String sep = "";
        for (var it = this.fields.iterator(); it.hasNext(); sep = ";") {
          builder.append(sep);
          builder.append("\n" + prefix + "    " + it.next().declaration());
        }
        builder.append("\n" + prefix + "}");
        break;
      }
      default:
    }
    return builder.toString();
  }

  /**
   * Return an mtype-declaration
   * or typedef-declaration (i.e. Promela structure declaration)
   *
   * @return data type declaration
   */
  public String declaration() {
    return declaration("");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.kind, this.name);
  }

  @Override
  public boolean equals(java.lang.@Nullable Object object) {
    if (object instanceof PDataType other)
      return Objects.equals(this.kind, other.kind) &&
             Objects.equals(this.name, other.name);
    return false;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append(" ");
    builder.append(this.declaration());
    return builder.toString();
  }
}
