package promela;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import util.Formatter;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class PInline {
  private String name;
  private List<PVariable> parameters = new ArrayList<PVariable>();
  private PStatement statement;

  public PInline(String name, List<PVariable> arguments, PStatement statement) {
    this.name = name;
    this.parameters.addAll(arguments);
    this.statement = statement;
  }

  /**
   * @return this inline's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Determine inline declaration in Promela format.
   *
   * @param prefix line indentation
   * @return inline declaration
   */
  public String declaration(String prefix) {
    var builder = new StringBuilder();

    builder.append(PKeywords.INLINE);
    builder.append(" ");
    builder.append(this.name);
    builder.append("(");
    builder.append(Formatter.separated(this.parameters, p -> p.getName(), ", "));
    builder.append(") {\n");
    String nextPrefix = prefix + "  ";
    builder.append(nextPrefix);
    builder.append(this.statement.code(nextPrefix));
    builder.append("\n");
    builder.append(prefix);
    builder.append("}");

    return builder.toString();
  }

  /**
   * @return inline's declaration in Promela format
   */
  public String declaration() {
    return this.declaration("");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name, this.parameters, this.statement);
  }

  @Override
  public boolean equals(java.lang.@Nullable Object object) {
    if (object instanceof PInline other)
      return Objects.equals(this.name, other.name) &&
             Objects.equals(this.parameters, other.parameters) &&
             Objects.equals(this.statement, other.statement);
    return false;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append(" ");
    builder.append(this.declaration());
    return builder.toString();
  }
}
