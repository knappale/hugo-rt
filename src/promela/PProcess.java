package promela;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;


/**
 * Promela process
 *
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public final class PProcess {
  enum Kind {
    PROCTYPE,
    INIT,
    NEVER;
  }

  private Kind kind;
  private String name;
  private List<PParameter> parameters = new ArrayList<PParameter>();
  private List<PChannel> channels = new ArrayList<PChannel>();
  private List<PVariable> variables = new ArrayList<PVariable>();
  private @Nullable PStatement statement = null;

  private PProcess(Kind kind, String name) {
    this.kind = kind;
    this.name = name;
  }

  /**
   * Create a user-defined process
   *
   * @param name process name
   * @param channels channel declarations
   * @param variables variable declarations
   * @param statement any statement
   */
  public static PProcess proctype(String name) {
    return new PProcess(Kind.PROCTYPE, name);
  }

  /**
   * Create a simple user-defined process
   *
   * @param name process name
   * @param statement a statement
   */
  public static PProcess proctype(String name, PStatement statement) {
    PProcess p = new PProcess(Kind.PROCTYPE, name);
    p.statement = statement;
    return p;
  }

  /**
   * Create a user-defined process
   *
   * @param name process name
   * @param channels channel declarations
   * @param variables variable declarations
   * @param statement any statement
   */
  public static PProcess proctype(String name, Collection<PChannel> channels, Collection<PVariable> variables, PStatement statement) {
    PProcess p = new PProcess(Kind.PROCTYPE, name);
    p.channels.addAll(channels);
    p.variables.addAll(variables);
    p.statement = statement;
    return p;
  }

  /**
   * Create an initial process
   *
   * @param statement any statement
   */
  public static PProcess init(PStatement statement) {
    PProcess p = new PProcess(Kind.INIT, PKeywords.INIT);
    p.statement = statement;
    return p;
  }

  /**
   * Create a never-claim
   *
   * @param statement any statement
   */
  public static PProcess never(PStatement statement) {
    PProcess p = new PProcess(Kind.NEVER, PKeywords.NEVER);
    p.statement = statement;
    return p;
  }

  /**
   * @return process' name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Set the process' statement.
   * 
   * @param statement a Promela statement
   */
  public PProcess setStatement(PStatement statement) {
    this.statement = statement;
    return this;
  }

  /**
   * Add a variable declaration.
   * 
   * @param variable a variable declaration
   */
  public PProcess addVarDecl(PVariable variable) {
    this.variables.add(variable);
    return this;
  }

  /**
   * Add variable declarations.
   * 
   * @param variables variable declarations
   */
  public PProcess addVarDecls(Collection<PVariable> variables) {
    for (var variable : variables)
      this.addVarDecl(variable);
    return this;
  }

  /**
   * @return process' variables
   */
  public List<PVariable> getVarDecls() {
    return this.variables;
  }

  /**
   * Add a channel declaration.
   * 
   * @param channel a channel declaration
   */
  public PProcess addChanDecl(PChannel channel) {
    this.channels.add(channel);
    return this;
  }

  /**
   * Add channel declarations.
   * 
   * @param channels channel declarations
   */
  public PProcess addChanDecls(Collection<PChannel> channels) {
    for (var channel : channels)
      this.addChanDecl(channel);
    return this;
  }

  /**
   * @return process' channels
   */
  public List<PChannel> getChanDecls() {
    return this.channels;
  }

  /**
   * Add a parameter which represents variable declaration.
   * 
   * @param variable a parameter variable
   */
  public PProcess addParam(PVariable variable) {
    this.parameters.add(PParameter.variable(variable));
    return this;
  }

  /**
   * Add a parameter which represents channel declaration.
   * 
   * @param channel a parameter channel
   */
  public PProcess addParam(PChannel channel) {
    this.parameters.add(PParameter.channel(channel));
    return this;
  }
  
  /**
   * @return process' parameters
   */
  public List<PParameter> getParameters() {
    return this.parameters;
  }

  /**
   * Return a process declaration
   *
   * @param prefix line indentation
   * @return process declaration, inner lines prepended by <CODE>prefix</CODE>,
   */
  public String declaration(String prefix) {
    var builder = new StringBuilder();

    switch (this.kind) {
      case PROCTYPE: {
        builder.append(PKeywords.PROCTYPE + " " + getName() + "(");
        if (!this.parameters.isEmpty())
          builder.append(Formatter.separated(this.getParameters(), p -> p.declaration(), "; "));
        builder.append(") {");
        builder.append("\n" + prefix + "  ");
        if (!this.channels.isEmpty()) {
          var indent = "";
          for (PChannel pChannel : this.getChanDecls()) {
            builder.append(indent);
            builder.append(pChannel.declaration());
            builder.append(";\n");
            indent = prefix + "  "; 
          }
          builder.append("\n" + prefix + "  ");
        }
        if (!this.variables.isEmpty()) {
          var indent = "";
          for (PVariable pVariable : this.getVarDecls()) {
            builder.append(indent);
            builder.append(pVariable.declaration());
            builder.append(";\n");
            indent = prefix + "  "; 
          }
          builder.append("\n" + prefix + "  ");
        }
        var statement = this.statement;
        if (statement != null) {
          builder.append(statement.code(prefix + "  "));
          builder.append("\n");
        }
        builder.append(prefix + "}");
        return builder.toString();
      }

      case INIT: {
        builder.append(PKeywords.INIT + " {\n");
        var statement = this.statement;
        if (statement != null) {
          builder.append(prefix + "  ");
          builder.append(statement.code(prefix + "  "));
          builder.append("\n");
        }
        builder.append(prefix + "}");
        return builder.toString();
      }

      case NEVER: {
        builder.append(PKeywords.NEVER + " {\n");
        var statement = this.statement;
        if (statement != null) {
          builder.append(prefix + "  ");
          builder.append(statement.code(prefix + "  "));
          builder.append("\n");
        }
        builder.append(prefix + "}");
        return builder.toString();
      }
    }

    return builder.toString();
  }
  
  /**
   * Return a process declaration
   *
   * @return process declaration
   */
  public String declaration() {
    return declaration("");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof PProcess other)
      return this.kind == other.kind &&
             Objects.equals(this.name, other.name);
    return false;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append(" ");
    builder.append(this.declaration());
    return builder.toString();
  }
}
