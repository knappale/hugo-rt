package promela;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

/**
 * Promela variable
 *
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public class PVariable extends PField {
  /**
   * Pre-defined special variable '_'
   */
  public static class Underscore extends PVariable {
    private Underscore() {
      super(null, PKeywords.USCORE, null);
    }
  }

  /**
   * Pre-defined special variable 'timeout'
   */
  public static class Timeout extends PVariable {
    private Timeout() {
      super(null, PKeywords.TIMEOUT, null);
    }
  }

  private static final Underscore UNDERSCORE = new Underscore();
  private static final Timeout TIMEOUT = new Timeout();

  private PVariable(@Nullable PDataType type, String name, @Nullable PConstant size) {
    super(new PField.Type() {
            public String code() {
              return type == null ? "" : type.getName();
            }
          }, name, size);
  }

  /**
   * Create a simple (i.e. non-array) variable
   */
  public static PVariable simple(String name, PDataType type) {
    return new PVariable(type, name, null);
  }

  /**
   * Create an array of variables
   */
  public static PVariable array(String name, PDataType type, PConstant size) {
    return new PVariable(type, name, size);
  }

  /**
   * @return the pre-declared hidden variable {@code _}
   */
  public static Underscore uscore() {
    return UNDERSCORE;
  }

  /**
   * @return the pre-declared, global, read-only variable {@code timeout}
   */
  public static Timeout timeout() {
    return TIMEOUT;
  }

  /**
   * Initialise this variable
   *
   * @param expression initialiser
   * @return this initialised variable
   */
  public PVariable initialise(PExpression expression) {
    super.initialise(new PField.Initialiser() {
                       public String code() {
                         return expression.code();
                       }
                     });
    return this;
  }
}
