package promela;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static util.Objects.requireNonNull;


/**
 * Promela expression
 *
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public final class PExpression {
  enum Kind {
    CONST,
    VAR,
    DOT,
    CHAN,
    BRACKET,
    POLL_NO_SIDE_EFFECT,
    RANDOM_POLL_NO_SIDE_EFFECT,
    INCR,
    DECR,
    RUN,
    NULLARY,
    UNARY,
    BINARY,
    FUNCTION,
    CONDITIONAL,
    MACRO,
    REMOTEREF,
    UNCHECKED;
  }
 
  enum PFunction {
    LEN,
    EMPTY,
    NEMPTY,
    NFULL,
    FULL,
    EVAL,
    ENABLED,
    PC_VALUE;

    /**
      * @return the name of the function
      */
    String getName() {
      switch (this) {
        case LEN:
          return PKeywords.LEN;
        case EMPTY:
          return PKeywords.EMPTY;
        case NEMPTY:
          return PKeywords.NEMPTY;
        case NFULL:
          return PKeywords.NFULL;
        case FULL:
          return PKeywords.FULL;
        case EVAL:
          return PKeywords.EVAL;
        case ENABLED:
          return PKeywords.ENABLED;
        case PC_VALUE:
          return PKeywords.PC_VALUE;
        default:
          return "[unknown]";
      }
    }
  }
 
  /**
   * @return whether this expression precedes another expression
   */
  private boolean precedes(PExpression other) {
    if ((this.kind == Kind.NULLARY || this.kind == Kind.UNARY || this.kind == Kind.BINARY) &&
        (other.kind == Kind.NULLARY || other.kind == Kind.UNARY || other.kind == Kind.BINARY))
      return requireNonNull(this.operator).precedes(requireNonNull(other.operator));
    return !(this.kind == Kind.POLL_NO_SIDE_EFFECT ||
             this.kind == Kind.RANDOM_POLL_NO_SIDE_EFFECT ||
             this.kind == Kind.RUN ||
             this.kind == Kind.MACRO ||
             this.kind == Kind.UNCHECKED);
  }

  /**
   * @return whether this expression is precedence preserved
   * @see PMacro#codeValue(String)
   */
  boolean isPrecedencePreserved() {
    if (this.kind == Kind.NULLARY || this.kind == Kind.UNARY || this.kind == Kind.BINARY)
      return (requireNonNull(this.operator).getPriority() >= POperator.MAXPRIORITY);
    return true;
  }

  private Kind kind;
  private @Nullable POperator operator;
  private @Nullable PFunction function;
  private @Nullable PConstant constant = null;
  private @Nullable PVariable variable = null;
  private @Nullable PChannel channel = null;
  private @Nullable PMacro macro = null;
  private @Nullable PExpression leftExpression = null;
  private @Nullable PExpression innerExpression = null;
  private @Nullable PExpression rightExpression = null;
  private @Nullable List<PExpression> args = null;
  private @Nullable String processType = null;
  private @Nullable String uncheckedString = null;

  private PExpression(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create an expression which represents the boolean constant 'true'
   */
  public static PExpression trueConst() {
    PExpression e = new PExpression(Kind.NULLARY);
    e.operator = POperator.TRUE;
    return e;
  }

  /**
   * Create an expression which represents the boolean constant 'false'
   */
  public static PExpression falseConst() {
    PExpression e = new PExpression(Kind.NULLARY);
    e.operator = POperator.FALSE;
    return e;
  }

  /**
   * Create an expression which represents an integer constant.
   *
   * @param integerConstant an integer constant
   * @return a Promela expression representing <CODE>integerConstant</CODE>
   */
  public static PExpression numeric(int integerConstant) {
    return PExpression.constant(PConstant.numeric(integerConstant));
  }

  /**
   * Create an expression which represents access to constant
   */
  public static PExpression constant(PConstant constant) {
    PExpression e = new PExpression(Kind.CONST);
    e.constant = constant;
    return e;
  }

  /**
   * Create an expression which represents access to variable
   */
  public static PExpression variable(PVariable variable) {
    PExpression e = new PExpression(Kind.VAR);
    e.variable = variable;
    return e;
  }

  /**
   * Create an expression which represents access to channel
   */
  public static PExpression channel(PChannel channel) {
    PExpression e = new PExpression(Kind.CHAN);
    e.channel = channel;
    return e;
  }

  /**
   * Create an expression which represents access to an array.
   *
   * @param varRefExpr any expression (access to variable, access to channel, or access to structure)
   * @param offsetExpr any expression (which evaluates to an offset)
   */
  public static PExpression bracket(PExpression varRefExpr, PExpression offsetExpr) {
    PExpression e = new PExpression(Kind.BRACKET);
    e.leftExpression = varRefExpr;
    e.innerExpression = offsetExpr;
    return e;
  }

  /**
   * Create an expression which represents access to an array.
   *
   * @param variable a variable
   * @param offsetExpr any expression (which evaluates to an offset)
   */
  public static PExpression bracket(PVariable variable, PExpression offsetExpr) {
    return bracket(PExpression.variable(variable), offsetExpr);
  }

  /**
   * Create an expression which represents access to structure
   *
   * @param varRefExpr expression (access to variable, access to array, or access to structure)
   * @param field expression which represents access to variable or access to channel
   */
  public static PExpression dot(PExpression varRefExpr, PExpression field) {
    PExpression e = new PExpression(Kind.DOT);
    e.leftExpression = varRefExpr;
    e.innerExpression = field;
    return e;
  }

  /**
   * Create an expression which represents (postfix) increment
   *
   * @param innerExpression any expression (access to variable, access to array, or access to structure)
   */
  public static PExpression increment(PExpression innerExpression) {
    PExpression e = new PExpression(Kind.INCR);
    e.innerExpression = innerExpression;
    return e;
  }

  /**
   * Create an expression which represents (postfix) decrement
   *
   * @param innerExpression any expression (access to variable, access to array, or access to structure)
   */
  public static PExpression decrement(PExpression innerExpression) {
    PExpression e = new PExpression(Kind.DECR);
    e.innerExpression = innerExpression;
    return e;
  }

  /**
   * Create an expression which represents unary minus
   */
  public static PExpression unaryMinus(PExpression innerExpression) {
    PExpression e = new PExpression(Kind.UNARY);
    e.operator = POperator.UMIN;
    e.innerExpression = innerExpression;
    return e;
  }

  /**
   * Create an expression which represents bitwise complement
   */
  public static PExpression bitComplement(PExpression innerExpression) {
    PExpression e = new PExpression(Kind.UNARY);
    e.operator = POperator.BNOT;
    e.innerExpression = innerExpression;
    return e;
  }

  /**
   * Create an expression which represents logical complement
   */
  public static PExpression not(PExpression innerExpression) {
    PExpression e = new PExpression(Kind.UNARY);
    e.operator = POperator.NOT;
    e.innerExpression = innerExpression;
    return e;
  }

  /**
   * Create an expression which represents binary junctional expression.
   *
   * Operators: {@link #MULT MULT}, {@link #DIV DIV}, {@link #MOD MOD}, {@link #PLUS PLUS},
   * {@link #MINUS MINUS}, {@link #SHL SHL}, {@link #SSHR SSHR}, {@link #LT LT}, {@link #LE LE},
   * {@link #GT GT}, {@link #GE GE}, {@link #EQ EQ}, {@link #NEQ NEQ}, {@link #AND AND},
   * {@link #OR OR}, {@link #XOR XOR}, {@link #CAND CAND}, {@link #COR COR}
   */
  public static PExpression binary(PExpression leftExpression, POperator op, PExpression rightExpression) {
    if (op == POperator.CAND) {
      if (trueConst().equals(leftExpression))
        return rightExpression;
      if (trueConst().equals(rightExpression))
        return leftExpression;
      if (falseConst().equals(leftExpression))
        return falseConst();
    }
    if (op == POperator.COR) {
      if (falseConst().equals(leftExpression))
        return rightExpression;
      if (falseConst().equals(rightExpression))
        return leftExpression;
      if (trueConst().equals(leftExpression))
        return trueConst();
    }
    PExpression e = new PExpression(Kind.BINARY);
    e.operator = op;
    e.leftExpression = leftExpression;
    e.rightExpression = rightExpression;
    return e;
  }

  /**
   * Create an equality test
   * 
   * @param leftExpression left expression
   * @param rightExpression right expression
   * @return test for equality of left and right expression
   */
  public static PExpression eq(PExpression leftExpression, PExpression rightExpression) {
    return binary(leftExpression, POperator.EQ, rightExpression);
  }

  /**
   * Create a conjunction
   * 
   * @param leftExpression left expression
   * @param rightExpression right expression
   * @return conjunction of left and right expression
   */
  public static PExpression and(PExpression leftExpression, PExpression rightExpression) {
    return binary(leftExpression, POperator.CAND, rightExpression);
  }

  /**
   * Create a disjunction
   * 
   * @param leftExpression left expression
   * @param rightExpression right expression
   * @return disjunction of left and right expression
   */
  public static PExpression or(PExpression leftExpression, PExpression rightExpression) {
    return binary(leftExpression, POperator.COR, rightExpression);
  }

  /**
   * Create an expression which represents run-operation.
   *
   * @param pname name of a Promela process
   *
   * @see PExpression#addRunArg(PExpression)
   */
  public static PExpression run(String pname) {
    PExpression e = new PExpression(Kind.RUN);
    e.processType = pname;
    e.args= new ArrayList<>();
    return e;
  }

  /**
   * Create an expression which represents a remote reference.
   *
   * @param pname name of a Promela process
   * @param pid process identifier
   * @param ref referenced expression
   */
  public static PExpression remoteRef(String pname, PExpression pid, PExpression ref) {
    PExpression e = new PExpression(Kind.REMOTEREF);
    e.processType = pname;
    e.leftExpression = pid;
    e.rightExpression = ref;
    return e;
  }

  /**
   * Create an expression which represents a function call
   */
  private static PExpression fun(PFunction fun, PExpression innerExpression) {
    PExpression e = new PExpression(Kind.FUNCTION);
    e.function = fun;
    e.innerExpression = innerExpression;
    return e;
  }

  /**
   * Create an expression which represents a len-function call
   */
  public static PExpression len(PExpression innerExpression) {
    return fun(PFunction.LEN, innerExpression);
  }

  /**
   * Create an expression which represents an empty-function call
   */
  public static PExpression empty(PExpression innerExpression) {
    return fun(PFunction.EMPTY, innerExpression);
  }

  /**
   * Create an expression which represents an nempty-function call
   */
  public static PExpression nempty(PExpression innerExpression) {
    return fun(PFunction.NEMPTY, innerExpression);
  }

  /**
   * Create an expression which represents an nfull-function call
   */
  public static PExpression nfull(PExpression innerExpression) {
    return fun(PFunction.NFULL, innerExpression);
  }

  /**
   * Create an expression which represents a full-function call
   */
  public static PExpression full(PExpression innerExpression) {
    return fun(PFunction.FULL, innerExpression);
  }

  /**
   * Create an expression which represents an eval-function call
   */
  public static PExpression eval(PExpression innerExpression) {
    return fun(PFunction.EVAL, innerExpression);
  }

  /**
   * Create an expression which represents an enabled-function call
   */
  public static PExpression enabled(PExpression innerExpression) {
    return fun(PFunction.ENABLED, innerExpression);
  }

  /**
   * Create an expression which represents a pc_value-function call
   */
  public static PExpression pc_value(PExpression innerExpression) {
    return fun(PFunction.PC_VALUE, innerExpression);
  }

  /**
   * Create an expression which represents a conditional
   */
  public static PExpression conditional(PExpression leftExpression, PExpression innerExpression, PExpression rightExpression) {
    PExpression e = new PExpression(Kind.CONDITIONAL);
    e.leftExpression = leftExpression;
    e.innerExpression = innerExpression;
    e.rightExpression = rightExpression;
    return e;
  }

  /**
   * Create an expression which represents poll-access to channel without side-effect
   *
   * @param varRefExpr any expression (access to channel, access to array, or access to structure)
   *
   * @see PExpression#addPollNoSideEffectArg(PExpression)
   */
  public static PExpression pollNoSideEffect(PExpression varRefExpr) {
    PExpression e = new PExpression(Kind.POLL_NO_SIDE_EFFECT);
    e.args = new ArrayList<>();
    e.args.add(varRefExpr);
    return e;
  }

  /**
   * Create an expression which represents random poll-access to
   * channel without side-effect.
   *
   * @param varRefExpr any expression (access to channel, access to
   * array, or access to structure)
   *
   * @see PExpression#addPollNoSideEffectArg(PExpression)
   */
  public static PExpression randomPollNoSideEffect(PExpression varRefExpr) {
    PExpression e = new PExpression(Kind.RANDOM_POLL_NO_SIDE_EFFECT);
    e.args = new ArrayList<>();
    e.args.add(varRefExpr);
    return e;
  }

  /**
   * Create a (basic) expression which represents macro
   */
  public static PExpression macro(PMacro macro) {
    PExpression s = new PExpression(Kind.MACRO);
    s.macro = macro;
    return s;
  }

  /**
   * Add a run-argument
   *
   * @param parameter expression which represents actual parameter of a process
   */
  public PExpression addRunArg(PExpression parameter) {
    if (this.kind == Kind.RUN && this.args != null) {
      this.args.add(parameter);
    }
    return this;
  }

  /**
   * Add a poll-no-side-effect-argument
   *
   * @param recvArg expression which represents a receive argument
   */
  public PExpression addPollNoSideEffectArg(PExpression recvArg) {
    if ((this.kind == Kind.POLL_NO_SIDE_EFFECT || this.kind == Kind.RANDOM_POLL_NO_SIDE_EFFECT) &&
        (this.args != null)) {
      this.args.add(recvArg);
    }
    return this;
  }
  
  /**
   * Create an Expression object consisting of the expression in <code>exp</code>.
   * When code is generated the parameter will be inserted without any checks,
   * thus make sure the code entered is valid PROMELA code.
   * 
   * @param exp a complete (and valid) PROMELA expression
   * @return expression created from a string
   */
  public static PExpression unchecked(String exp) {
    PExpression e = new PExpression(Kind.UNCHECKED);
    e.uncheckedString = exp;
    return e;
  }

  /**
   * @return Promela code which represents this expression
   */
  public String code() {
    var builder = new StringBuilder();
    switch (this.kind) {
      case NULLARY: {
        builder.append(requireNonNull(this.operator).getName());
        return builder.toString();
      }
      case UNARY: {
        PExpression innerExpression = requireNonNull(this.innerExpression);
        builder.append(requireNonNull(this.operator).getName());
        if (innerExpression.precedes(this))
          builder.append(innerExpression.code());
        else {
          builder.append("(");
          builder.append(innerExpression.code());
          builder.append(")");
        }
        return builder.toString();
      }
      case BINARY: {
        PExpression leftExpression = requireNonNull(this.leftExpression);
        PExpression rightExpression = requireNonNull(this.rightExpression);
        if (leftExpression.precedes(this))
          builder.append(leftExpression.code());
        else {
          builder.append("(");
          builder.append(leftExpression.code());
          builder.append(")");
        }
        builder.append(requireNonNull(this.operator).getName());
        if (rightExpression.precedes(this))
          builder.append(rightExpression.code());
        else {
          builder.append("(");
          builder.append(rightExpression.code());
          builder.append(")");
        }
        return builder.toString();
      }
      case CONST: {
        builder.append(requireNonNull(this.constant).getName());
        return builder.toString();
      }
      case VAR: {
        builder.append(requireNonNull(this.variable).getName());
        return builder.toString();
      }
      case CHAN: {
        builder.append(requireNonNull(this.channel).getName());
        return builder.toString();
      }
      case BRACKET: {
        builder.append(requireNonNull(this.leftExpression).code());
        builder.append("[");
        builder.append(requireNonNull(this.innerExpression).code());
        builder.append("]");
        return builder.toString();
      }
      case DOT: {
        builder.append(requireNonNull(this.leftExpression).code());
        builder.append(".");
        builder.append(requireNonNull(this.innerExpression).code());
        return builder.toString();
      }
      case FUNCTION: {
        builder.append(requireNonNull(this.function).getName());
        builder.append("(");
        builder.append(requireNonNull(this.innerExpression).code());
        builder.append(")");
        return builder.toString();
      }
      case POLL_NO_SIDE_EFFECT: {
        var it = requireNonNull(this.args).iterator();
        builder.append(it.next().code()); // polled channel
        builder.append("?[");
        for (var sep = ""; it.hasNext(); sep = ",") {
          builder.append(sep);
          builder.append(it.next().code()); // receive argument
        }
        builder.append("]");
        return builder.toString();
      }
      case RANDOM_POLL_NO_SIDE_EFFECT: {
        var it = requireNonNull(this.args).iterator();
        builder.append(it.next().code()); // polled channel
        builder.append("??[");
        for (var sep = ""; it.hasNext(); sep = ",") {
          builder.append(sep);
          builder.append(it.next().code()); // receive argument
        }
        builder.append("]");
        return builder.toString();
      }
      case REMOTEREF: {
        builder.append(requireNonNull(this.processType));
        builder.append("[");
        builder.append(requireNonNull(this.leftExpression).code());
        builder.append("]:");
        builder.append(requireNonNull(this.rightExpression).code());
        return builder.toString();
      }
      case CONDITIONAL: {
        builder.append("(");
        builder.append(requireNonNull(this.leftExpression).code());
        builder.append(" -> ");
        builder.append(requireNonNull(this.innerExpression).code());
        builder.append(" : ");
        builder.append(requireNonNull(this.rightExpression).code());
        builder.append(")");
        return builder.toString();
      }
      case INCR: {
        builder.append(requireNonNull(this.innerExpression).code());
        builder.append("++");
        return builder.toString();
      }
      case DECR: {
        builder.append(requireNonNull(this.innerExpression).code());
        builder.append("--");
        return builder.toString();
      }
      case RUN: {
        builder.append(PKeywords.RUN);
        builder.append(" ");
        builder.append(requireNonNull(this.processType));
        builder.append("(");
        var it = requireNonNull(this.args).iterator();
        for (var sep = ""; it.hasNext(); sep = ", ") {
          builder.append(sep);
          builder.append(it.next().code());
        }
        builder.append(")");
        return builder.toString();
      }
      case MACRO: {
        builder.append(requireNonNull(this.macro).getName());
        return builder.toString();
      }
      case UNCHECKED: {
        builder.append(requireNonNull(this.uncheckedString));
        return builder.toString();
      }
      default:
        return builder.toString();
    }
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(this.constant,
                        this.variable,
                        this.channel,
                        this.leftExpression,
                        this.innerExpression,
                        this.rightExpression,
                        this.args,
                        this.macro,
                        this.uncheckedString);
  }

  @Override
  public boolean equals(java.lang.@Nullable Object object) {
    if (object instanceof PExpression other)
      return this.kind == other.kind &&
             this.operator == other.operator &&
             this.function == other.function &&
             Objects.equals(this.constant, other.constant) &&
             Objects.equals(this.variable, other.variable) &&
             Objects.equals(this.channel, other.channel) &&
             Objects.equals(this.leftExpression, other.leftExpression) &&
             Objects.equals(this.innerExpression, other.innerExpression) &&
             Objects.equals(this.rightExpression, other.rightExpression) &&
             Objects.equals(this.args, other.args) &&
             Objects.equals(this.macro, other.macro) &&
             Objects.equals(this.uncheckedString, other.uncheckedString);
    return false;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append(" ");
    builder.append(this.code());
    return builder.toString();
  }
}
