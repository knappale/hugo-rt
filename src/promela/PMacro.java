package promela;

import static util.Objects.requireNonNull;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Promela macro definition
 *
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public final class PMacro {
  private static final String DEFINE = "#define";

  enum Kind {
    CONST,
    EXPR,
    STMNT;
  }

  private Kind kind;
  private String name;
  private @Nullable PConstant constant = null;
  private @Nullable PExpression expression = null;
  private @Nullable PStatement statement = null;
  
  private PMacro(Kind kind, String name) {
    this.kind = kind;
    this.name = name;
  }

  /**
   * Create a macro which represents constant
   *
   * @param name macro's name
   * @param constant macro's value
   */
  public static PMacro constant(String name, PConstant constant) {
    PMacro m = new PMacro(Kind.CONST, name);
    m.constant = constant;
    return m;
  }

  /**
   * Create a macro which represents expression
   *
   * @param name macro's name
   * @param expression macro's value
   */
  public static PMacro expression(String name, PExpression expression) {
    PMacro m = new PMacro(Kind.EXPR, name);
    m.expression = expression;
    return m;
  }

  /**
   * Create a macro which represents statement
   *
   * @param name macro's name
   * @param statement macro's value
   */
  public static PMacro statement(String name, PStatement statement) {
    PMacro m = new PMacro(Kind.STMNT, name);
    m.statement = statement;
    return m;
  }

  /**
   * @return macro's value
   */
  public int getValue() {
    if (this.kind == Kind.CONST)
      return requireNonNull(this.constant).getValue();
    return 0;
  }

  /**
   * @return macro's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return string representation of macro's value
   */
  private String codeValue(String prefix) {
    String result = "";

    switch (this.kind) {
      case CONST:
        result = requireNonNull(this.constant).getName();
        break;
      case EXPR:
        PExpression expression = requireNonNull(this.expression);
        if (expression.isPrecedencePreserved())
          result = expression.code();
        else
          result = "(" + expression.code() + ")";
        break;
      case STMNT:
        result = requireNonNull(this.statement).code(prefix);
        break;
    }

    return result;
  }

  private String insertBackSlashBeforeNewLine(String string) {
    StringBuilder resultBuilder = new StringBuilder();
    char[] inputChars = string.toCharArray();
    for (int i = 0; i < inputChars.length; i++) {
      if (inputChars[i] == '\n') {
        resultBuilder.append(" \\");
      }
      resultBuilder.append(inputChars[i]);
    }
    return resultBuilder.toString();
  }

  /**
   * Return Promela code which represents this macro
   *
   * @param prefix line indentation
   * @return Promela code which represents this macro, inner lines
   * prepended by <CODE>prefix</CODE>
   */
  public String code(String prefix) {
    var builder = new StringBuilder();
    builder.append(DEFINE + " " + getName() + " ");
    String codedValue = insertBackSlashBeforeNewLine(codeValue(prefix + "    "));
    if (codedValue.indexOf('\n') >= 0) {
      builder.append("\\\n" + prefix + "    ");
    }
    builder.append(codedValue);
    return builder.toString();
  }

  /**
   * Return Promela code which represents this macro
   *
   * @return Promela code which represents this macro
   */
  public String code() {
    return code("");
  }

  @Override
  public int hashCode() {
    return this.name.hashCode();
  }

  @Override
  public boolean equals(java.lang.@Nullable Object object) {
    if (object instanceof PMacro other)
      return this.name.equals(other.name);
    return false;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append(" ");
    builder.append(this.code());
    return builder.toString();
  }
}
