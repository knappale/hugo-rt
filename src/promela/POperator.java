package promela;

import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public enum POperator {
  TRUE,
  FALSE,
  UMIN,
  BNOT,
  NOT,
  MULT,
  DIV,
  MOD,
  PLUS,
  MINUS,
  SHL,
  SSHR, // signed right shift
  LT,
  LE,
  GT,
  GE,
  EQ,
  NEQ,
  AND,
  OR,
  XOR,
  CAND,
  COR;

  String getName() {
    switch (this) {
      case FALSE:
        return PKeywords.FALSE;
      case TRUE:
        return PKeywords.TRUE;
      case UMIN:
        return "-";
      case BNOT:
        return "~";
      case NOT:
        return "!";
      case MULT:
        return "*";
      case DIV:
        return "/";
      case MOD:
        return "%";
      case PLUS:
        return "+";
      case MINUS:
        return "-";
      case SHL:
        return " << ";
      case SSHR:
        return " >> ";
      case LT:
        return " < ";
      case LE:
        return " <= ";
      case GT:
        return " > ";
      case GE:
        return " >= ";
      case EQ:
        return " == ";
      case NEQ:
        return " != ";
      case AND:
        return "&";
      case XOR:
        return "^";
      case OR:
        return "|";
      case CAND:
        return " && ";
      case COR:
        return " || ";
      default:
        return "[unknown]";
    }
  }

  public boolean precedes(POperator other) {
    return (this.getPriority() > other.getPriority()) ||
           (this == other && other.isAssociative());
  }

  static final int MAXPRIORITY = 14;

  int getPriority() {
    switch (this) {
      case TRUE:
      case FALSE:
        return 14;
      case UMIN:
      case BNOT:
      case NOT:
        return 13;
      case MULT:
      case DIV:
      case MOD:
        return 12;
      case PLUS:
      case MINUS:
        return 11;
      case SHL:
      case SSHR:
        return 10;
      case LT:
      case LE:
      case GT:
      case GE:
        return 9;
      case EQ:
      case NEQ:
        return 8;
      case AND:
      case XOR:
      case OR:
        return 7;
      case CAND:
      case COR:
        return 4;
      default:
        return 0;
    }
  }

  public boolean isAssociative() {
    switch (this) {
      case MULT:
      case PLUS:
      case AND:
      case OR:
      case XOR:
      case CAND:
      case COR:
        return true;
      case UMIN:
      case BNOT:
      case NOT:
      case DIV:
      case EQ:
      case FALSE:
      case GE:
      case GT:
      case LE:
      case LT:
      case MINUS:
      case MOD:
      case NEQ:
      case SHL:
      case SSHR:
      case TRUE:
      default:
        return false;
    }
  }
}
