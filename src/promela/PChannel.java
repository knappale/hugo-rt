package promela;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Promela channel
 *
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public final class PChannel extends PField {
  private PChannel(String name, @Nullable PConstant size) {
    super(new PField.Type() {
            public String code() {
              return PKeywords.CHAN;
            }
          }, name, size);
  }

  /**
   * Create a simple (i.e. non-array) channel
   */
  public static PChannel simple(String name) {
    return new PChannel(name, null);
  }

  /**
   * Create an array of channels
   */
  public static PChannel array(String name, PConstant size) {
    return new PChannel(name, size);
  }

  /**
   * Initialise this channel.
   *
   * @param expression initialiser
   * @return this initialised channel
   */
  public PChannel initialise(PConstant bufferSize, PMessageType messageType) {
    super.initialise(new PField.Initialiser() {
                       public String code() {
                         var builder = new StringBuilder();
                         builder.append("[");
                         builder.append(bufferSize.getName());
                         builder.append("] ");
                         builder.append(PKeywords.OF);
                         builder.append(" ");
                         builder.append(messageType.declaration());
                         return builder.toString();
                       }
                     });
    return this;
  }
}
