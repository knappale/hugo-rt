package promela;

import static util.Objects.requireNonNull;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class PParameter {
  private static class Variable extends PParameter {
    private PVariable variable;

    private Variable(PVariable variable) {
      this.variable = variable;
    }

    @Override
    public String getName() {
      return this.variable.getName();
    }

    @Override
    public boolean isVariable() {
      return true;
    }

    @Override
    public @NonNull PVariable getVariable() {
      return this.variable;
    }

    @Override
    public boolean isChannel() {
      return false;
    }

    @Override
    public String declaration() {
      return requireNonNull(this.getVariable()).parameter().toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.variable);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Variable other = (Variable)object;
        return Objects.equals(this.variable, other.variable);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("Parameter [variable = ");
      builder.append(this.getVariable());
      builder.append("]");
      return builder.toString();
    }
  }

  private static class Channel extends PParameter {
    private PChannel channel;

    private Channel(PChannel channel) {
      this.channel = channel;
    }

    @Override
    public String getName() {
      return this.channel.getName();
    }

    @Override
    public boolean isVariable() {
      return false;
    }

    @Override
    public boolean isChannel() {
      return true;
    }

    @Override
    public @NonNull PChannel getChannel() {
      return this.channel;
    }

    @Override
    public String declaration() {
      return requireNonNull(this.getChannel()).parameter().toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.channel);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Channel other = (Channel)object;
        return Objects.equals(this.channel, other.channel);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("Parameter [channel = ");
      builder.append(this.getChannel());
      builder.append("]");
      return builder.toString();
    }
  }

  private PParameter() {
  }

  /**
   * Create a parameter which represents channel declaration.
   *
   * @param channel a channel declaration
   */
  public static PParameter channel(PChannel channel) {
    return new Channel(channel);
  }

  /**
   * Create a parameter which represents variable declaration.
   *
   * @param variable a variable declaration
   */
  public static PParameter variable(PVariable variable) {
    return new Variable(variable);
  }

  /**
   * @return parameter's name
   */
  public abstract String getName();

  /**
   * @return whether this parameter is a variable
   */
  public abstract boolean isVariable();

  /**
   * @return parameter's variable
   */
  public @Nullable PVariable getVariable() {
    return null;
  }

  /**
   * @return whether this parameter is a channel
   */
  public abstract boolean isChannel();

  /**
   * @return parameter's channel
   */
  public @Nullable PChannel getChannel() {
    return null;
  }

  /**
   * @return parameter declaration
   */
  public abstract String declaration();
}
