package promela;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 * Promela keywords.
 *
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public abstract class PKeywords {
  public static final String ACTIVE = "active";
  public static final String ASSERT = "assert";
  public static final String ATOMIC = "atomic";
  public static final String BIT = "bit";
  public static final String BOOL = "bool";
  public static final String BREAK = "break";
  public static final String BYTE = "byte";
  public static final String CHAN = "chan";
  public static final String D_STEP = "d_step";
  public static final String DPROCTYPE = "Dproctype";
  public static final String DO = "do";
  public static final String ELSE = "else";
  public static final String EMPTY = "empty";
  public static final String ENABLED = "enabled";
  public static final String EVAL = "eval";
  public static final String FALSE = "false";
  public static final String FI = "fi";
  public static final String FULL = "full";
  public static final String GOTO = "goto";
  public static final String HIDDEN = "hidden";
  public static final String IF = "if";
  public static final String INIT = "init";
  public static final String INLINE = "inline";
  public static final String INT = "int";
  public static final String LEN = "len";
  public static final String MTYPE = "mtype";
  public static final String NEMPTY = "nempty";
  public static final String NEVER = "never";
  public static final String NFULL = "nfull";
  public static final String OD = "od";
  public static final String OF = "of";
  public static final String PC_VALUE = "pc_value";
  public static final String PID = "pid";
  public static final String PRINTF = "printf";
  public static final String PRIORITY = "priority";
  public static final String PROCTYPE = "proctype";
  public static final String PROVIDED = "provided";
  public static final String RUN = "run";
  public static final String SHORT = "short";
  public static final String SKIP = "skip";
  public static final String TIMEOUT = "timeout";
  public static final String TRUE = "true";
  public static final String TYPEDEF = "typedef";
  public static final String UNLESS = "unless";
  public static final String UNSIGNED = "unsigned";
  public static final String USCORE = "_";
  public static final String XR = "xr";
  public static final String XS = "xs";

  private static final @NonNull String[] keywords = {
    ACTIVE, ASSERT, ATOMIC, BIT, BOOL, BREAK, BYTE, CHAN, D_STEP, DPROCTYPE,
    DO, ELSE, EMPTY, ENABLED, EVAL, FALSE, FI, FULL, GOTO, HIDDEN, IF, INIT,
    INLINE, INT, LEN, MTYPE, NEMPTY, NEVER, NFULL, OD, OF, PC_VALUE, PID, PRINTF,
    PRIORITY, PROCTYPE, PROVIDED, RUN, SHORT, SKIP, TIMEOUT, TRUE, TYPEDEF,
    UNLESS, UNSIGNED, USCORE, XR, XS };

  public static boolean isKeyword(String word) {
    for (int i = 0; i < keywords.length; i++) {
      if (keywords[i].equals(word))
        return true;
    }
    return false;
  }

  public static Set<String> getKeywords() {
    return new HashSet<>(Arrays.asList(keywords));
  }
}
