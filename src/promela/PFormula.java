package promela;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static util.Objects.requireNonNull;


/**
 * Promela linear temporal logic formula
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
@NonNullByDefault
public class PFormula {
  enum Kind {
    TERM,
    UNARY,
    BINARY;
  }

  enum Operator {
    ALWAYS,
    SOMETIME,
    UNTIL,
    NEG,
    AND,
    OR,
    IMPLY;

    String getOp() {
      switch (this) {
        case ALWAYS: return "[] ";
        case SOMETIME: return "<> ";
        case UNTIL: return " U ";
        case NEG: return "! ";
        case AND: return " && ";
        case OR: return " || ";
        case IMPLY: return " -> ";
        default: return "[unknown]";
      }
    }
  }

  private Kind kind;
  private @Nullable PMacro macro = null;
  private @Nullable PFormula left = null;
  private @Nullable Operator op;
  private @Nullable PFormula right = null;

  private PFormula(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create a formula representing a (simple) term.
   *
   * @param formula a formula
   * @return a formula
   */
  public static PFormula term(PMacro macro) {
    PFormula f = new PFormula(Kind.TERM);
    f.macro = macro;
    return f;
  }

  /**
   * Create a formula representing always.
   *
   * @param formula a formula
   * @return a formula
   */
  public static PFormula always(PFormula formula) {
    PFormula f = new PFormula(Kind.UNARY);
    f.op = Operator.ALWAYS;
    f.left = formula;
    return f;
  }

  /**
   * Create a formula representing sometime.
   *
   * @param formula a formula
   * @return a formula
   */
  public static PFormula sometime(PFormula formula) {
    PFormula f = new PFormula(Kind.UNARY);
    f.op = Operator.SOMETIME;
    f.left = formula;
    return f;
  }

  /**
   * Create a formula representing until.
   *
   * @param left a formula
   * @param right a formula
   * @return a formula
   */
  public static PFormula until(PFormula left, PFormula right) {
    PFormula f = new PFormula(Kind.BINARY);
    f.left = left;
    f.op = Operator.UNTIL;
    f.right = right;
    return f;
  }

  /**
   * Create a formula representing a negation.
   *
   * @param formula a formula
   * @return a formula
   */
  public static PFormula neg(PFormula formula) {
    PFormula f = new PFormula(Kind.UNARY);
    f.op = Operator.NEG;
    f.left = formula;
    return f;
  }

  /**
   * Create a formula representing a conjunction.
   *
   * @param left a formula
   * @param right a formula
   * @return a formula
   */
  public static PFormula and(PFormula left, PFormula right) {
    PFormula f = new PFormula(Kind.BINARY);
    f.left = left;
    f.op = Operator.AND;
    f.right = right;
    return f;
  }

  /**
   * Create a formula representing a disjunction.
   *
   * @param left a formula
   * @param right a formula
   * @return a formula
   */
  public static PFormula or(PFormula left, PFormula right) {
    PFormula f = new PFormula(Kind.BINARY);
    f.left = left;
    f.op = Operator.OR;
    f.right = right;
    return f;
  }

  /**
   * Create a formula representing an implication.
   *
   * @param left a formula
   * @param right a formula
   * @return a formula
   */
  public static PFormula imply(PFormula left, PFormula right) {
    PFormula f = new PFormula(Kind.BINARY);
    f.left = left;
    f.op = Operator.IMPLY;
    f.right = right;
    return f;
  }

  /**
   * @return macros used in this formula
   */
  public Set<PMacro> getMacros() {
    Set<PMacro> macros = new HashSet<PMacro>();

    switch (this.kind) {
      case TERM:
        macros.add(requireNonNull(this.macro));
        return macros;

      case UNARY:
        macros.addAll(requireNonNull(this.left).getMacros());
        return macros;

      case BINARY:
        macros.addAll(requireNonNull(this.left).getMacros());
        macros.addAll(requireNonNull(this.right).getMacros());
        return macros;
    }

    return macros;
  }

  /**
   * @return formula representation in Promela-format, prefixing inner lines with {@code prefix}
   */
  public String code(String prefix) {
    var builder = new StringBuilder();

    builder.append(prefix);
    switch (this.kind) {
      case TERM:
        builder.append(requireNonNull(this.macro).getName());
        return builder.toString();

      case UNARY:
        builder.append(requireNonNull(this.op).getOp());
        builder.append("(");
        builder.append(requireNonNull(this.left).code());
        builder.append(")");
        return builder.toString();        

      case BINARY:
	builder.append("(");
	builder.append(requireNonNull(this.left).code());
	builder.append(")");
        builder.append(requireNonNull(this.op).getOp());
	builder.append("(");
	builder.append(requireNonNull(this.right).code());
	builder.append(")");
        return builder.toString();
    }

    return builder.toString();
  }

  /**
   * @return formula representation in Promela-format
   */
  public String code() {
    return this.code("");
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.macro, this.left, this.right);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object instanceof PFormula other)
      return this.kind == other.kind &&
             Objects.equals(this.macro, other.macro) &&
             Objects.equals(this.left, other.left) &&
             Objects.equals(this.right, other.right) &&
             this.op == other.op;
    return false;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append(" ");
    builder.append(this.code());
    return builder.toString();
  }
}
