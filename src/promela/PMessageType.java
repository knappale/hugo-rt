package promela;


import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Message type of an initialised channel
 * 
 * @author Magdi Ismail
 */
@NonNullByDefault
public final class PMessageType {
  /**
   * Part of a message type
   */
  static class Part {
    private String declarator = "";

    private Part() {
    }

    /**
     * Create a data type part.
     */
    private static Part part(PDataType dataType) {
      Part p = new Part();
      p.declarator = dataType.getName();
      return p;
    }

    /**
     * Create a channel part
     */
    private static Part chanPart() {
      Part p = new Part();
      p.declarator = PKeywords.CHAN;
      return p;
    }

    /**
     * @return part's declarator
     */
    String declaration() {
      return this.declarator;
    }

    @Override
    public int hashCode() {
      return this.declarator.hashCode();
    }

    @Override
    public boolean equals(java.lang.@Nullable Object object) {
      if (object instanceof Part other)
        return (this.declarator.equals(other.declarator));
      return false;
    }

    /**
     * @return string representation of this part
     */
    public String toString() {
      var builder = new StringBuilder();
      builder.append("Part");
      builder.append(" [declarator = ");
      builder.append(this.declarator);
      builder.append("]");
      return builder.toString();
    }
  }

  private List<PMessageType.Part> parts = new LinkedList<>();

  private PMessageType() {
  }

  /**
   * Create a (anonymous) message type
   */
  public static PMessageType create() {
    PMessageType m = new PMessageType();
    return m;
  }

  /**
   * Create a channel part and add it to this message type
   */
  public PMessageType addPartChannel() {
    Part p = Part.chanPart();
    this.parts.add(p);
    return this;
  }

  /**
   * Create a data type part and add it to this message type
   * 
   * @param dataType data type whose <CODE>kind</CODE> serves as declarator
   */
  public PMessageType addPartDataType(PDataType dataType) {
    Part p = Part.part(dataType);
    this.parts.add(p);
    return this;
  }

  /**
   * @return parts
   */
  List<PMessageType.Part> getParts() {
    return this.parts;
  }

  /**
   * @return message type declaration
   */
  public String declaration() {
    var builder = new StringBuilder();
    builder.append("{");
    var sep = " ";
    for (var it = this.parts.iterator(); it.hasNext(); sep = ", ") {
      builder.append(sep);
      builder.append(it.next().declaration());
    }
    builder.append(" }");
    return builder.toString();
  }

  @Override
  public int hashCode() {
    return this.parts.hashCode();
  }

  @Override
  public boolean equals(java.lang.@Nullable Object object) {
    if (object instanceof PMessageType other)
      return (this.parts.equals(other.parts));
    return false;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append(" = ");
    builder.append(this.declaration());
    return builder.toString();
  }
}
