package promela.run;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import promela.PChannel;
import promela.PField;
import promela.PVariable;
import util.Formatter;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class PValuationEntry {
  private static enum Kind {
    SIMPLE,
    ARRAY;
  }
  
  private Kind kind;
  private PField field;
  private int offset;
  private List<List<Integer>> values = new ArrayList<>();
  
  private PValuationEntry(Kind kind, PField field) {
    this.kind = kind;
    this.field = field;
  }
  
  public static PValuationEntry variable(PVariable variable, int value) {
    // Promela stores arrays of size one as a simple variable
    if (variable.isArray())
      return variable(variable, 0, value);
    PValuationEntry e = new PValuationEntry(Kind.SIMPLE, variable);
    List<Integer> valueList = new ArrayList<>();
    valueList.add(value);
    e.values.add(valueList);
    return e;
  }

  public static PValuationEntry variable(PVariable variable, int offset, int value) {
    PValuationEntry e = new PValuationEntry(Kind.ARRAY, variable);
    e.offset = offset;
    List<Integer> valueList = new ArrayList<>();
    valueList.add(value);
    e.values.add(valueList);
    return e;
  }

  public static PValuationEntry channel(PChannel channel, List<List<Integer>> valueLists) {
    PValuationEntry e = new PValuationEntry(Kind.SIMPLE, channel);
    e.values.addAll(valueLists);
    return e;
  }

  public static PValuationEntry channel(PChannel channel, int offset, List<List<Integer>> valueLists) {
    PValuationEntry e = new PValuationEntry(Kind.ARRAY, channel);
    e.offset = offset;
    e.values.addAll(valueLists);
    return e;
  }

  public boolean matches(PField field) {
    return this.kind == Kind.SIMPLE && this.field.equals(field);
  }

  public boolean matches(PField field, int offset) {
    return this.kind == Kind.ARRAY && this.field.equals(field) && this.offset == offset;
  }

  public int getValue() {
    return this.values.get(0).get(0);
  }

  public List<Integer> getValueList(int offset) {
    if (offset < 0 || offset >= this.values.size())
      return new ArrayList<>();
    return this.values.get(offset);
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append("Entry [");
    switch (this.kind) {
      case SIMPLE:
        builder.append("simple = ");
        builder.append(this.field.getName());
        break;
      case ARRAY:
        builder.append("array = ");
        builder.append(this.field.getName());
        builder.append("[");
        builder.append(this.offset);
        builder.append("]");
        break;
      default:
    }
    builder.append(", values = ");
    builder.append(Formatter.list(this.values, valueList -> Formatter.list(valueList)));
    builder.append("]");
    return builder.toString();
  }
}
