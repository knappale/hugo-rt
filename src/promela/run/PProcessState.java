package promela.run;

import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import promela.PProcess;
import promela.PVariable;
import promela.PChannel;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class PProcessState {
  private PProcess process;
  private int number = 0;
  private PValuation valuation = new PValuation();
  
  public PProcessState(PProcess process, int number) {
    this.process = process;
    this.number = number;
  }
  
  public PProcess getProcess() {
    return this.process;
  }

  public int getNumber() {
    return this.number;
  }

  public void addEntry(PValuationEntry entry) {
    this.valuation.addEntry(entry);
  }
  
  public int getValue(PVariable variable) {
    return this.valuation.getValue(variable);
  }

  public int getValue(PVariable variable, int offset) {
    return this.valuation.getValue(variable, offset);
  }

  public List<Integer> getValue(PChannel channel, int offset) {
    return this.valuation.getValue(channel, offset);
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append("ProcessState[process = ");
    builder.append(this.process.getName());
    builder.append(", number = ");
    builder.append(this.number);
    builder.append(", valuation = ");
    builder.append(this.valuation);
    return builder.toString();
  }
}
