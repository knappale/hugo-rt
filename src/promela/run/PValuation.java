package promela.run;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import promela.PChannel;
import promela.PVariable;
import util.Formatter;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class PValuation {
  private List<PValuationEntry> valuation = new ArrayList<>();
  
  public PValuation() {
  }
  
  public void addEntry(PValuationEntry entry) {
    this.valuation.add(entry);
  }
  
  public int getValue(PVariable variable) {
    for (PValuationEntry entry : this.valuation) {
      if (entry.matches(variable))
        return entry.getValue();
    }
    util.Message.debug().info("No value found for variable `" + variable.getName() + "' in " + this + ".");
    return 0;
  }
  
  public int getValue(PVariable variable, int offset) {
    for (PValuationEntry entry : this.valuation) {
      if (entry.matches(variable, offset))
        return entry.getValue();
    }
    util.Message.debug().info("No value found for array variable `" + variable.getName() + "[" + offset + "]' in " + this + ".");
    return 0;
  }

  public List<Integer> getValue(PChannel channel, int offset) {
    for (PValuationEntry entry : this.valuation) {
      if (entry.matches(channel))
        return entry.getValueList(offset);
    }
    util.Message.debug().info("No value found for channel `" + channel.getName() + "[" + offset + "]' in " + this + ".");
    return new ArrayList<>();
  }
  
  public List<Integer> getValue(PChannel channel, int offset1, int offset2) {
    for (PValuationEntry entry : this.valuation) {
      if (entry.matches(channel, offset1))
        return entry.getValueList(offset2);
    }
    util.Message.debug().info("No value found for channel `" + channel.getName() + "[" + offset1 + "][" + offset2 + "]' in " + this + ".");
    return new ArrayList<>();
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append("Valuation [entries = ");
    builder.append(Formatter.list(this.valuation));
    builder.append("]");
    return builder.toString();
  }
}
