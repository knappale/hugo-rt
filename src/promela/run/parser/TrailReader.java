package promela.run.parser;

import java.io.BufferedInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import promela.run.PSystemState;


/**
 * Reads data from a Promela trail-file.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class TrailReader {
  private List<@NonNull PSystemState> trail = null;

  @SuppressWarnings("null")
  public TrailReader(BufferedInputStream trailInputStream, promela.PSystem system) throws TrailException {
    try {
      this.trail = promela.run.parser.PromelaParser.trail(trailInputStream, system);
    }
    catch (Exception e) {
      StringWriter stackTraceWriter = new StringWriter();
      e.printStackTrace(new PrintWriter(stackTraceWriter, true));
      throw new TrailException("Trail parsing failed: " + e.getMessage());
    }
    if (this.trail == null)
      throw new TrailException("No trail found in input");
  }

  public List<@NonNull PSystemState> getTrail() {
    return this.trail;
  }
}
