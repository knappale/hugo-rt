package promela.run.parser;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@SuppressWarnings("serial")
public class TrailException extends Exception {
  public TrailException() {
    super();
  }

  public TrailException(String message) {
    super(message);
  }
}
