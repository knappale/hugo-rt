package promela.run;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import promela.PChannel;
import promela.PProcess;
import promela.PVariable;
import util.Formatter;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
@NonNullByDefault
public class PSystemState {
  private PValuation valuation = new PValuation();
  private List<PProcessState> processStates = new ArrayList<>();
  
  public PSystemState() {
  }

  public void combine(PSystemState other) {
    for (PProcessState otherProcessState : other.processStates) {
      if (this.getProcessState(otherProcessState.getProcess(), otherProcessState.getNumber()) == null)
        this.addProcessState(otherProcessState);
    }
  }

  public void addProcessState(PProcessState processState) {
    this.processStates.add(processState);
  }

  public @Nullable PProcessState getProcessState(PProcess process, int number) {
    for (PProcessState processState : this.processStates) {
      if (processState.getProcess().equals(process) &&
          processState.getNumber() == number)
        return processState;
    }
    return null;
  }

  public void addEntry(PValuationEntry entry) {
    this.valuation.addEntry(entry);
  }

  public int getValue(PVariable variable) {
    return valuation.getValue(variable);
  }

  public int getValue(PVariable variable, int offset) {
    return valuation.getValue(variable, offset);
  }

  public List<Integer> getValue(PChannel channel, int processNumber, int offset) {
    return valuation.getValue(channel, processNumber, offset);
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append("SystemState [valuation = ");
    builder.append(this.valuation);
    builder.append(", processStates = ");
    builder.append(Formatter.separated(this.processStates, ", "));
    builder.append("]");
    return builder.toString();
  }
}
