package promela;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import static java.util.stream.Collectors.toSet;


/**
 * Promela-LTL specification
 *
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public class PSpecification {
  private List<PFormula> formulae = new ArrayList<>();
    
  private PSpecification() {
  }

  /**
   * Create a (anonymous) Promela-LTL specification
   */
  public static PSpecification create() {
    PSpecification s = new PSpecification();
    return s;
  }

  /**
   * Add a formula.
   * 
   * @param formula formula
   * @return this specification
   */
  public PSpecification addFormula(PFormula formula) {
    this.formulae.add(formula);
    return this;
  }
  
  /**
   * Determine Promela-LTL code which represents this specification, all lines
   * prepended by a prefix.
   *
   * @param prefix line indentation
   * @return Promela-LTL code for this specification, all lines prepended by {@code prefix}
   */
  public String code(String prefix) {
    var builder = new StringBuilder();
    boolean pending = false;

    Set<PMacro> macros = this.formulae.stream().flatMap(formula -> formula.getMacros().stream()).collect(toSet());
    if (!macros.isEmpty()) {
      pending = true;
      for (PMacro macro : macros) {
        builder.append(prefix);
        builder.append(macro.code(prefix));
        builder.append("\n");
      }
    }

    if (!this.formulae.isEmpty()) {
      if (pending)
        builder.append("\n");
      pending = true;
      builder.append("#ifdef NOTES\n");
      for (PFormula formula : this.formulae) {
        builder.append(prefix);
        builder.append(formula.code(prefix));
        builder.append("\n");
      }
      builder.append("#endif\n");
    }

    return builder.toString();
  }

  /**
   * @return Promela-LTL code which represents this specification
   */
  public String code() {
    return code("");
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append("");
    builder.append(this.code());
    return builder.toString();
  }
}
