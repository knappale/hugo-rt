package promela;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Promela field of a user-defined Promela data type.
 *
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public abstract class PField {
  public interface Type {
    public String code();
  }

  public interface Initialiser {
    public String code();
  }

  private String name;
  private Type type;
  private @Nullable PConstant size = null;
  private @Nullable Initialiser initialiser = null;

  PField(Type type, String name, @Nullable PConstant size) {
    this.type = type;
    this.name = name;
    this.size = size;
  }

  /**
   * @return field's name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Initialise this field
   *
   * @param initialiser initialiser
   * @return this initialised field
   */
  public PField initialise(Initialiser initialiser) {
    this.initialiser = initialiser;
    return this;
  }

  /**
   * @return whether this field is an array
   */
  public boolean isArray() {
    return this.size != null;
  }

  /**
   * @return field declaration
   */
  public String declaration() {
    var builder = new StringBuilder();
    builder.append(this.parameter());
    var initialiser = this.initialiser;
    if (initialiser != null) {
      builder.append(" = ");
      builder.append(initialiser.code());
    }
    return builder.toString();
  }

  /**
   * @return field parameter declaration
   */
  public String parameter() {
    var builder = new StringBuilder();
    builder.append(this.type.code());
    builder.append(" ");
    builder.append(this.name);
    var size = this.size;
    if (size != null) {
      builder.append("[");
      builder.append(size.getName());
      builder.append("]");
    }
    return builder.toString();
  }

  @Override
  public int hashCode() {
    return this.getName().hashCode();
  }

  @Override
  public boolean equals(java.lang.@Nullable Object object) {
    if (object instanceof PField other)
      return (this.getName().equals(other.getName()));
    return false;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append(" ");
    builder.append(this.declaration());
    return builder.toString();
  }
}
