package promela;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static java.util.Objects.requireNonNull;


/**
 * Promela constant
 * 
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public final class PConstant {
  enum Kind {
    TRUE,
    FALSE,
    NUMERIC,
    SYMBOLIC,
    MACRO;
  }

  private Kind kind;
  private int numeric = 0;
  private @Nullable PMacro macro;
  private @Nullable String name;

  private PConstant(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create a numeric constant
   */
  public static PConstant numeric(int number) {
    PConstant c = new PConstant(Kind.NUMERIC);
    c.numeric = number;
    return c;
  }

  /**
   * Create the boolean constant 'true'
   */
  public static PConstant trueConst() {
    PConstant c = new PConstant(Kind.TRUE);
    return c;
  }

  /**
   * Create the boolean constant 'false'
   */
  public static PConstant falseConst() {
    PConstant c = new PConstant(Kind.FALSE);
    return c;
  }

  /**
   * Create a symbolic constant
   *
   * @param name symbol name which refers to an mtype-declaration
   */
  public static PConstant symbolic(String name) {
    PConstant c = new PConstant(Kind.SYMBOLIC);
    c.name = name;
    return c;
  }

  /**
   * Create a macro constant.
   *
   * @param macro macro definition
   */
  public static PConstant macro(promela.PMacro macro) {
    PConstant c = new PConstant(Kind.MACRO);
    c.macro = macro;
    return c;
  }
  
  /**
   * @return constant's name
   */
  public String getName() {
    switch (this.kind) {
      case FALSE:
        return PKeywords.FALSE;
      case TRUE:
        return PKeywords.TRUE;
      case NUMERIC:
        return Integer.toString(this.numeric);
      case SYMBOLIC:
        return requireNonNull(this.name);
      case MACRO:
        return requireNonNull(this.macro).getName();
    }
    return "";
  }

  public int getValue() {
    switch (this.kind) {
      case FALSE:
        return 0;
      case TRUE:
        return 1;
      case NUMERIC:
        return this.numeric;
      case SYMBOLIC:
        return 0;
      case MACRO: {
        return requireNonNull(this.macro).getValue();
      }
    }
    return 0;
  }

  /**
   * @return constant's dependencies
   */
  Set<PConstant> getDependencies() {
    return new HashSet<>();
  }
 
  @Override
  public int hashCode() {
    return Objects.hash(this.name);
  }

  @Override
  public boolean equals(java.lang.@Nullable Object object) {
    if (object instanceof PConstant other)
      return this.kind == other.kind &&
             this.numeric == other.numeric &&
             Objects.equals(this.macro, other.macro) &&
             Objects.equals(this.name, other.name);
    return false;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append(" ");
    builder.append(this.getName());
    builder.append(" = ");
    builder.append(this.getValue());
    return builder.toString();
  }
}
