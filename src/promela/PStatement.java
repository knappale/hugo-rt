package promela;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import static util.Objects.requireNonNull;

/**
 * Promela statement
 * 
 * @author <A HREF="mailto:ismail@cip.ifi.lmu.de">Magdi Ismail</A>
 */
@NonNullByDefault
public final class PStatement {
  enum Kind {
    SKIP,
    VAR_DECL,
    CHAN_DECL,
    ASSIGN,
    EXP,
    ASSERT,
    ELSE,
    BREAK,
    GOTO,
    SEND,
    SORTEDSEND,
    RECV,
    RANDOMRECV,
    ATOMIC,
    D_STEP,
    SEL,
    REP,
    SEQUENCE,
    XR,
    XS,
    CALL,
    MACRO,
    PRINTF;
  }

  private Kind kind;
  private @Nullable PVariable variable = null;
  private @Nullable PChannel channel = null;
  private @Nullable PMacro macro = null;
  private @Nullable PInline inline = null;
  private String targetLabel = "";
  private @Nullable PExpression leftExpression = null;
  private @Nullable PExpression rightExpression = null;
  private List<String> labels = new ArrayList<>();
  private @Nullable PStatement leftStatement = null;
  private @Nullable PStatement rightStatement = null;
  private List<PExpression> args = new ArrayList<>();
  private List<Branch> branches = new ArrayList<>();
  private @Nullable Branch elseBranch = null;

  private PStatement(Kind kind) {
    this.kind = kind;
  }

  /**
   * Create a (basic) statement which represents skip
   */
  public static PStatement skip() {
    PStatement s = new PStatement(Kind.SKIP);
    return s;
  }

  /**
   * Create a (basic) statement which represents variable-declaration
   */
  public static PStatement varDecl(PVariable variable) {
    PStatement s = new PStatement(Kind.VAR_DECL);
    s.variable = variable;
    return s;
  }

  /**
   * Create a (basic) statement which represents channel-declaration
   */
  public static PStatement chanDecl(PChannel channel) {
    PStatement s = new PStatement(Kind.CHAN_DECL);
    s.channel = channel;
    return s;
  }

  /**
   * Create a (basic) statement which represents assignment
   */
  public static PStatement assignment(PExpression leftExpression, PExpression rightExpression) {
    PStatement s = new PStatement(Kind.ASSIGN);
    s.leftExpression = leftExpression;
    s.rightExpression = rightExpression;
    return s;
  }

  /**
   * Create a (basic) statement which represents expression
   */
  public static PStatement expression(PExpression expression) {
    PStatement s = new PStatement(Kind.EXP);
    s.leftExpression = expression;
    return s;
  }

  /**
   * Create a (basic) statement which represents assertion
   */
  public static PStatement assertion(PExpression expression) {
    PStatement s = new PStatement(Kind.ASSERT);
    s.leftExpression = expression;
    return s;
  }

  /**
   * Create a (basic) statement which represents else
   */
  public static PStatement elseStm() {
    PStatement s = new PStatement(Kind.ELSE);
    return s;
  }

  /**
   * Create a (basic) statement which represents break
   */
  public static PStatement breakStm() {
    PStatement s = new PStatement(Kind.BREAK);
    return s;
  }

  /**
   * Create a statement which represents goto
   * 
   * @param targetLabel target statement's label
   */
  public static PStatement gotoStm(String targetLabel) {
    PStatement s = new PStatement(Kind.GOTO);
    s.targetLabel = targetLabel;
    return s;
  }

  /**
   * Create a statement which represents goto
   * 
   * @param targetStatement target statement
   * @pre !targetStatement.labels.isEmpty()
   */
  public static PStatement gotoStm(PStatement targetStatement) {
    PStatement s = new PStatement(Kind.GOTO);
    s.targetLabel = targetStatement.labels.get(0);
    return s;
  }

  /**
   * Create a (basic) statement which represents send
   * 
   * @param varRefExpr any expression (access to channel, access to array, or
   *            access to structure)
   * 
   * @see PStatement#addSendStmArg(PExpression)
   */
  public static PStatement send(PExpression varRefExpr) {
    PStatement e = new PStatement(Kind.SEND);
    e.args.add(varRefExpr);
    return e;
  }

  /**
   * Create a (basic) statement which represents sorted send.
   * 
   * @param varRefExpr any expression (access to channel, access to array, or
   *            access to structure)
   * 
   * @see PStatement#addSendStmArg(PExpression)
   */
  public static PStatement sortedSend(PExpression varRefExpr) {
    PStatement e = new PStatement(Kind.SORTEDSEND);
    e.args.add(varRefExpr);
    return e;
  }

  /**
   * Create a (basic) statement which represents receive
   * 
   * @param varRefExpr any expression (access to channel, access to array, or
   *            access to structure)
   * 
   * @see PStatement#addRecvStmArg(PExpression)
   */
  public static PStatement receive(PExpression varRefExpr) {
    PStatement e = new PStatement(Kind.RECV);
    e.args.add(varRefExpr);
    return e;
  }

  /**
   * Create a (basic) statement which represents random receive.
   * 
   * @param varRefExpr any expression (access to channel, access to array, or
   *            access to structure)
   * 
   * @see PStatement#addRecvStmArg(PExpression)
   */
  public static PStatement randomReceive(PExpression varRefExpr) {
    PStatement e = new PStatement(Kind.RANDOMRECV);
    e.args.add(varRefExpr);
    return e;
  }

  /**
   * Create a statement which represents atomic.
   * 
   * @param statement any statement
   */
  public static PStatement atomic(PStatement statement) {
    PStatement s = new PStatement(Kind.ATOMIC);
    s.leftStatement = statement;
    return s;
  }

  /**
   * Create a statement which represents d_step.
   * 
   * @param statement any statement
   */
  public static PStatement d_step(PStatement statement) {
    PStatement s = new PStatement(Kind.D_STEP);
    s.leftStatement = statement;
    return s;
  }

  /**
   * Create a statement representing a blank selection.
   * 
   * @see PStatement#isBlankSel()
   * @see PStatement#addBranch(PStatement)
   * @see PStatement#setElseBranch(PStatement)
   */
  public static PStatement selection() {
    PStatement s = new PStatement(Kind.SEL);
    s.branches = new LinkedList<>();
    return s;
  }

  /**
   * Create a statement representing a complete selection.
   * 
   * @see PStatement#isBlankSel()
   * @see PStatement#addBranch(PStatement)
   * @see PStatement#setElseBranch(PStatement)
   */
  public static PStatement selection(Collection<PStatement> statements, @Nullable PStatement elseStatement) {
    if (elseStatement == null && statements != null && statements.size() == 1)
      return statements.iterator().next();

    PStatement s = selection();
    s.addBranches(statements);
    if (elseStatement != null)
      s.setElseBranch(elseStatement);
    return s;
  }

  /**
   * Create a statement representing a complete selection.
   * 
   * @see PStatement#isBlankSel()
   * @see PStatement#addBranch(PStatement)
   * @see PStatement#setElseBranch(PStatement)
   */
  public static PStatement selection(@Nullable PStatement statement, PStatement elseStatement) {
    if (statement == null)
      return selection(new LinkedList<>(), elseStatement);
    var statements = new ArrayList<PStatement>();
    statements.add(statement);
    return selection(statements, elseStatement);
  }

  /**
   * Create a statement representing a complete selection.
   *
   * @param statements a collection of statements
   */
  public static PStatement selection(Collection<PStatement> statements) {
    return selection(statements, null);
  }

  /**
   * Create a statement representing a blank repetition.
   * 
   * @see PStatement#isBlankRep()
   * @see PStatement#addBranch(PStatement)
   * @see PStatement#setElseBranch(PStatement)
   */
  public static PStatement repetition() {
    PStatement s = new PStatement(Kind.REP);
    s.branches = new LinkedList<>();
    return s;
  }

  /**
   * Create a statement representing a repetition.
   * 
   * @param statement a statement to be repeated indefinitely
   * @return repetition
   */
  public static PStatement repetition(PStatement statement) {
    PStatement s = new PStatement(Kind.REP);
    s.branches = new LinkedList<>();
    s.branches.add(Branch.branch(statement));
    return s;
  }

  /**
   * Create a statement which represents sequential composition of statements.
   * 
   * @param leftStatement any statement
   * @param rightStatement any statement
   */
  public static PStatement sequence(@Nullable PStatement leftStatement, @Nullable PStatement rightStatement) {
    if (leftStatement == null && rightStatement == null)
      return PStatement.skip();
    if (leftStatement != null && rightStatement == null)
      return leftStatement;
    if (leftStatement == null && rightStatement != null)
      return rightStatement;
    assert (leftStatement != null && rightStatement != null);

    if (leftStatement.kind == Kind.SKIP) {
      if (!leftStatement.labels.isEmpty())
        rightStatement.labels.addAll(leftStatement.labels);
      return rightStatement;
    }
    if (rightStatement.kind == Kind.SKIP && rightStatement.labels.isEmpty()) {
      return leftStatement;
    }

    PStatement s = new PStatement(Kind.SEQUENCE);
    s.leftStatement = leftStatement;
    s.rightStatement = rightStatement;
    return s;
  }

  /**
   * Create a branch statement, i.e., a sequential composition of statements
   * where the first statement acts as a guard and is given by an expression.
   * 
   * @param guard an expression used as guard
   * @param statement any statement
   */
  public static PStatement branch(PExpression guard, PStatement statement) {
    return sequence(expression(guard), statement);
  }

  /**
   * Create a (basic) statement which represents exclusive receive on channels.
   * 
   * @param varRefExpr any expression (access to channel, access to array, or
   *            access to structure)
   */
  public static PStatement exclusiveRead(PExpression varRefExpr) {
    PStatement s = new PStatement(Kind.XR);
    s.leftExpression = varRefExpr;
    return s;
  }

  /**
   * Create a (basic) statement which represents exclusive send on channels.
   * 
   * @param varRefExpr any expression (access to channel, access to array, or
   *            access to structure)
   */
  public static PStatement exclusiveSend(PExpression varRefExpr) {
    PStatement s = new PStatement(Kind.XS);
    s.leftExpression = varRefExpr;
    return s;
  }

  /**
   * Create a (basic) statement which an inline call.
   *
   * @param inline an inline "macro"
   * @param args a list of argument expressions
   */
  public static PStatement call(PInline inline, List<PExpression> args) {
    PStatement s = new PStatement(Kind.CALL);
    s.inline = inline;
    s.args.addAll(args);
    return s;
  }

  /**
   * Create a (basic) statement which represents macro.
   */
  public static PStatement macro(PMacro macro) {
    PStatement s = new PStatement(Kind.MACRO);
    s.macro = macro;
    return s;
  }

  /**
   * Create a (basic) statement which represents a <CODE>printf</CODE>.
   */
  public static PStatement printf(String format, List<PExpression> args) {
    PStatement s = new PStatement(Kind.PRINTF);
    s.targetLabel = format;
    s.args.addAll(args);
    return s;
  }

  /**
   * Create a labelled statement.
   */
  public static PStatement label(String label, PStatement statement) {
    if (!statement.labels.contains(label))
      statement.labels.add(label);
    return statement;
  }

  /**
   * Add a send argument to send-statement-arguments.
   * 
   * @param sendArg expression which represents a send argument
   */
  public PStatement addSendStmArg(PExpression sendArg) {
    if ((this.kind == Kind.SEND || this.kind == Kind.SORTEDSEND) &&
        (this.args != null && this.args.size() > 0)) {
      this.args.add(sendArg);
    }
    return this;
  }

  /**
   * Add send arguments to send-statement-arguments.
   * 
   * @param sendArgs list of expressions which represent send arguments
   */
  public PStatement addSendStmArgs(List<PExpression> sendArgs) {
    for (PExpression sendArg : sendArgs)
      this.addSendStmArg(sendArg);
    return this;
  }

  /**
   * Add a receive argument to receive-statement-arguments.
   * 
   * @param recvArg expression which represents a receive argument
   */
  public PStatement addRecvStmArg(PExpression recvArg) {
    if ((this.kind == Kind.RECV || this.kind == Kind.RANDOMRECV) &&
        (this.args != null && this.args.size() > 0)) {
      this.args.add(recvArg);
    }
    return this;
  }

  /**
   * Create a branch and add it to this (selection/repetition-) statement.
   * 
   * @param statement any statement
   */
  public PStatement addBranch(PStatement statement) {
    Branch b = Branch.branch(statement);
    this.branches.add(b);
    return this;
  }

  /**
   * Create a branch and add it to this (selection/repetition-) statement.
   * 
   * @param guard any expression
   * @param statement any statement
   */
  public PStatement addBranch(PExpression guard, PStatement statement) {
    Branch b = Branch.branch(PStatement.sequence(PStatement.expression(guard), statement));
    this.branches.add(b);
    return this;
  }

  /**
   * Create branches and add them to this (selection/repetition-) statement.
   * 
   * @param statements collection of statements
   */
  public PStatement addBranches(Collection<PStatement> statements) {
    for (PStatement statement : statements)
      this.addBranch(statement);
    return this;
  }

  /**
   * Create an else-branch and add it to this (selection/repetition-) statement.
   * 
   * @param statement any statement
   */
  public PStatement setElseBranch(PStatement statement) {
    Branch b = Branch.elseBranch(statement);
    this.elseBranch = b;
    return this;
  }

  /**
   * Return Promela code which represents this statement
   * 
   * @param prefix line indentation
   * @return Promela code which represents this statement, inner lines prepended by {@code prefix}
   */
  public String code(String prefix) {
    var builder = new StringBuilder();

    for (String label : this.labels) {
      builder.append(label + ":");
      builder.append("\n" + prefix);
    }

    switch (this.kind) {
      case SKIP: {
        builder.append(PKeywords.SKIP);
        return builder.toString();
      }
      case VAR_DECL: {
        builder.append(requireNonNull(this.variable).declaration());
        return builder.toString();
      }
      case CHAN_DECL: {
        builder.append(requireNonNull(this.channel).declaration());
        return builder.toString();
      }
      case ASSIGN: {
        builder.append(requireNonNull(this.leftExpression).code());
        builder.append(" = ");
        builder.append(requireNonNull(this.rightExpression).code());
        return builder.toString();
      }
      case EXP: {
        builder.append(requireNonNull(this.leftExpression).code());
        return builder.toString();
      }
      case ASSERT: {
        builder.append(PKeywords.ASSERT);
        builder.append("(");
        builder.append(requireNonNull(this.leftExpression).code());
        builder.append(")");
        return builder.toString();
      }
      case ELSE: {
        builder.append(PKeywords.ELSE);
        return builder.toString();
      }
      case BREAK: {
        builder.append(PKeywords.BREAK);
        return builder.toString();
      }
      case GOTO: {
        builder.append(PKeywords.GOTO);
        builder.append(" ");
        builder.append(this.targetLabel);
        return builder.toString();
      }
      case SEND: {
        Iterator<PExpression> it = this.args.iterator();
        builder.append(it.next().code()); // channel
        builder.append("!");
        for (String sep = ""; it.hasNext(); sep = ",") {
          builder.append(sep);
          builder.append(it.next().code()); // send argument
        }
        return builder.toString();
      }
      case SORTEDSEND: {
        Iterator<PExpression> it = this.args.iterator();
        builder.append(it.next().code()); // channel
        builder.append("!!");
        for (String sep = ""; it.hasNext(); sep = ",") {
          builder.append(sep);
          builder.append(it.next().code()); // send argument
        }
        return builder.toString();
      }
      case RECV: {
        Iterator<PExpression> it = this.args.iterator();
        builder.append(it.next().code()); // channel
        builder.append("?");
        for (String sep = ""; it.hasNext(); sep = ",") {
          builder.append(sep);
          builder.append(it.next().code()); // receive argument
        }
        return builder.toString();
      }
      case RANDOMRECV: {
        Iterator<PExpression> it = this.args.iterator();
        builder.append(it.next().code()); // channel
        builder.append("??");
        for (String sep = ""; it.hasNext(); sep = ",") {
          builder.append(sep);
          builder.append(it.next().code()); // receive argument
        }
        return builder.toString();
      }
      case ATOMIC: {
        String nextPrefix = prefix + "  ";
        builder.append(PKeywords.ATOMIC);
        builder.append(" {\n");
        builder.append(nextPrefix);
        builder.append(requireNonNull(this.leftStatement).code(nextPrefix));
        builder.append("\n");
        builder.append(prefix);
        builder.append("}");
        return builder.toString();
      }
      case D_STEP: {
        String nextPrefix = prefix + "  ";
        builder.append(PKeywords.D_STEP);
        builder.append(" {\n");
        builder.append(nextPrefix);
        builder.append(requireNonNull(this.leftStatement).code(nextPrefix));
        builder.append("\n");
        builder.append(prefix);
        builder.append("}");
        return builder.toString();
      }
      case SEL: {
        builder.append(PKeywords.IF);
        if (this.branches.isEmpty() && this.elseBranch == null) { // should not happen
          builder.append("\n");
          builder.append(prefix);
          builder.append(Branch.branch().code()); // default branch
        }
        else {
          for (Branch branch : this.branches) {
            builder.append("\n");
            builder.append(prefix);
            builder.append(branch.code(prefix));
          }
          var elseBranch = this.elseBranch;
          if (elseBranch != null) {
            builder.append("\n");
            builder.append(prefix);
            builder.append(elseBranch.code(prefix));
          }
        }
        builder.append("\n");
        builder.append(prefix);
        builder.append(PKeywords.FI);
        return builder.toString();
      }
      case REP: {
        builder.append(PKeywords.DO);
        if (this.branches.isEmpty() && this.elseBranch == null) { // should not happen
          builder.append("\n");
          builder.append(prefix);
          builder.append(Branch.branch().code()); // default branch
        }
        else {
          for (Branch branch : this.branches) {
            builder.append("\n");
            builder.append(prefix);
            builder.append(branch.code(prefix));
          }
          var elseBranch = this.elseBranch;
          if (elseBranch != null) {
            builder.append("\n");
            builder.append(prefix);
            builder.append(elseBranch.code(prefix));
          }
        }
        builder.append("\n");
        builder.append(prefix);
        builder.append(PKeywords.OD);
        return builder.toString();
      }
      case SEQUENCE: {
        builder.append(requireNonNull(this.leftStatement).code(prefix));
        builder.append(";\n");
        builder.append(prefix);
        builder.append(requireNonNull(this.rightStatement).code(prefix));
        return builder.toString();
      }
      case XR: {
        builder.append(PKeywords.XR);
        builder.append(" ");
        builder.append(requireNonNull(this.leftExpression).code());
        return builder.toString();
      }
      case XS: {
        builder.append(PKeywords.XS);
        builder.append(" ");
        builder.append(requireNonNull(this.leftExpression).code());
        return builder.toString();
      }
      case CALL: {
        builder.append(requireNonNull(this.inline).getName());
        builder.append(Formatter.tuple(this.args));
        return builder.toString();
      }
      case MACRO: {
        builder.append(requireNonNull(this.macro).getName());
        return builder.toString();
      }
      case PRINTF: {
        builder.append(PKeywords.PRINTF);
        builder.append("(\"");
        builder.append(this.targetLabel);
        builder.append("\", ");
        builder.append(Formatter.separated(this.args, e -> e.code(), ", "));
        builder.append(")");
        return builder.toString();
      }
      default:
        return builder.toString();
    }
  }

  /**
   * Return Promela code which represents this statement
   * 
   * @return Promela code which represents this statement
   */
  public String code() {
    return code("");
  }

  @Override
  public int hashCode() {
    return (Objects.hash(this.variable,
                         this.channel,
                         this.leftExpression,
                         this.rightExpression,
                         this.labels,
                         this.targetLabel,
                         this.leftStatement,
                         this.rightStatement,
                         this.args,
                         this.branches,
                         this.elseBranch,
                         this.inline,
                         this.macro));
  }

  @Override
  public boolean equals(java.lang.@Nullable Object object) {
    if (object instanceof PStatement other)
      return this.kind == other.kind &&
             Objects.equals(this.variable, other.variable) &&
             Objects.equals(this.channel, other.channel) &&
             Objects.equals(this.leftExpression, other.leftExpression) &&
             Objects.equals(this.rightExpression, other.rightExpression) &&
             Objects.equals(this.labels, other.labels) &&
             Objects.equals(this.targetLabel, other.targetLabel) &&
             Objects.equals(this.leftStatement, other.leftStatement) &&
             Objects.equals(this.rightStatement, other.rightStatement) &&
             Objects.equals(this.args, other.args) &&
             Objects.equals(this.branches, other.branches) &&
             Objects.equals(this.elseBranch, other.elseBranch) &&
             Objects.equals(this.inline, other.inline) &&
             Objects.equals(this.macro, other.macro);
    return false;
  }

  @Override
  public String toString() {
    var result = new StringBuilder();
    result.append(this.getClass().getName());
    result.append(" ");
    result.append(this.code());
    return result.toString();
  }

  /**
   * Promela branch of a selection- or repetition-statement
   */
  static final class Branch {
    private PStatement statement;

    private Branch(PStatement statement) {
      this.statement = statement;
    }

    /**
     * Create a default branch.
     *
     * Needed only if a blank selection/repetition-statement has to be coded
     * (should not happen)
     * 
     * @see PStatement#isBlankSel()
     * @see PStatement#isBlankRep()
     * @see PStatement#code(String)
     */
    private static Branch branch() {
      return new Branch(PStatement.skip());
    }

    /**
     * Create a branch
     * 
     * @param statement any statement
     */
    private static Branch branch(PStatement statement) {
      return new Branch(statement);
    }

    /**
     * Create an else-branch
     * 
     * @param statement any statement
     */
    private static Branch elseBranch(PStatement statement) {
      return new Branch(PStatement.sequence(PStatement.elseStm(), statement));
    }

    /**
     * Return Promela code which represents this branch's <CODE>statement</CODE>
     * 
     * @param statement any statement
     */
    private static String codeStm(PStatement statement, boolean appendSep, String prefix) {
      StringBuilder result = new StringBuilder();
      if (statement.kind != Kind.SEQUENCE) {
        String sep = (appendSep ? " ->" : "");
        result.append(statement.code(prefix) + sep);
        return result.toString();
      }
      var leftStatement = statement.leftStatement; assert leftStatement != null;
      result.append(codeStm(leftStatement, true, prefix));
      String sep = (appendSep ? ";" : "");
      result.append("\n" + prefix + requireNonNull(statement.rightStatement).code(prefix) + sep);
      return result.toString();
    }

    /**
     * Return Promela code which represents this branch
     * 
     * @param prefix line indentation
     * @return Promela code which represents this branch, inner lines prepended
     *         by <CODE>prefix</CODE>
     */
    private String code(String prefix) {
      StringBuilder result = new StringBuilder();
      result.append(":: ");
      result.append(codeStm(this.statement, false, prefix + "   "));
      return result.toString();
    }

    /**
     * Return Promela code which represents this branch
     * 
     * @return Promela code which represents this branch
     */
    private String code() {
      return code("");
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.statement);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object instanceof Branch other)
        return Objects.equals(this.statement, other.statement);
      return false;
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(this.getClass().getName());
      builder.append(" ");
      builder.append(this.code());
      return builder.toString();
    }
  }
}
