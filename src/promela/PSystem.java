package promela;


import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Promela system
 */
@NonNullByDefault
public final class PSystem {
  private List<PMacro> macros = new ArrayList<>();
  private List<PInline> inlines = new ArrayList<>();
  private List<PDataType> datatypes = new ArrayList<>();
  private List<PVariable> variables = new ArrayList<>();
  private List<PChannel> channels = new ArrayList<>();
  private List<PProcess> processes = new ArrayList<>();
  private @Nullable PSpecification specification;
    
  private PSystem() {
  }

  /**
   * Create a Promela system.
   */
  public static PSystem create() {
    PSystem s = new PSystem();
    return s;
  }

  /**
   * Add an inline definition.
   * 
   * @param inline an inline definition
   * @return this system
   */
  public PSystem addInlineDef(PInline inline) {
    this.inlines.add(inline);
    return this;
  }
  
  /**
   * Add a macro definition.
   * 
   * @param macro macro definition
   * @return this system
   */
  public PSystem addMacroDef(PMacro macro) {
    this.macros.add(macro);
    return this;
  }
  
  /**
   * Add a datatype definition.
   * 
   * @param datatype datatype definition
   * @return this system
   */
  public PSystem addDataType(PDataType datatype) {
    this.datatypes.add(datatype);
    return this;
  }
  
  /**
   * Add a variable declaration.
   * 
   * @param variable variable declaration
   * @return this system
   */
  public PSystem addVarDecl(PVariable variable) {
    this.variables.add(variable);
    return this;
  }
  
  /**
   * @return system's variable declarations
   */
  public List<PVariable> getVarDecls() {
    return this.variables;
  }
  
  /**
   * Add a channel declaration.
   * 
   * @param channel channel declaration
   * @return this system
   */
  public PSystem addChanDecl(PChannel channel) {
    this.channels.add(channel);
    return this;
  }

  /**
   * @return system's channel declarations
   */
  public List<PChannel> getChanDecls() {
    return this.channels;
  }

  /**
   * Add a process definition.
   * 
   * @param process process definition
   * @return this system
   */
  public PSystem addProcess(PProcess process) {
    this.processes.add(process);
    return this;
  }

  /**
   * @return system's process definitions
   */
  public List<PProcess> getProcesses() {
    return this.processes;
  }

  /**
   * Set specification.
   * 
   * @param specification specification
   * @return this system
   */
  public PSystem setSpecification(PSpecification specification) {
    this.specification = specification;
    return this;
  }
  
  /**
   * @return whether this system shows a specification
   */
  public boolean hasSpecification() {
    return (this.specification != null);
  }
  
  /**
   * @return system's specification
   */
  public @Nullable PSpecification getSpecification() {
    return this.specification;
  }
  
  /**
   * Determine Promela code which represents this system.
   *
   * @param prefix line indentation
   * @return Promela code which represents this system, all lines prepended by {@code prefix}
   */
  public String code(String prefix) {
    var builder = new StringBuilder();
    boolean pending = false;

    if (!this.macros.isEmpty()) {
      pending = true;
      for (var it = this.macros.iterator(); it.hasNext();) {
        builder.append(prefix);
        builder.append(it.next().code(prefix));
        builder.append("\n");
      }
    }

    if (!this.datatypes.isEmpty()) {
      if (pending)
        builder.append("\n");
      pending = true;
      for (var it = this.datatypes.iterator(); it.hasNext();) {
        builder.append(prefix);
        builder.append(it.next().declaration(prefix));
        builder.append("\n");
      }
    }
    
    if (!this.channels.isEmpty()) {
      if (pending)
        builder.append("\n");
      pending = true;
      for (var it = this.channels.iterator(); it.hasNext();) {
        builder.append(prefix);
        builder.append(it.next().declaration());
        builder.append(";\n");
      }
    }
    
    if (!this.variables.isEmpty()) {
      if (pending)
        builder.append("\n");
      pending = true;
      for (var it = this.variables.iterator(); it.hasNext();) {
        builder.append(prefix);
        builder.append(it.next().declaration());
        builder.append(";\n");
      }
    }
    
    if (!this.inlines.isEmpty()) {
      if (pending)
        builder.append("\n");
      pending = true;
      for (var it = this.inlines.iterator(); it.hasNext();) {
        builder.append(prefix);
        builder.append(it.next().declaration(prefix));
        builder.append("\n");
        if (it.hasNext())
          builder.append("\n");
      }
    }
    
    if (!this.processes.isEmpty()) {
      if (pending)
        builder.append("\n");
      pending = true;
      for (var it = this.processes.iterator(); it.hasNext();) {
        builder.append(prefix);
        builder.append(it.next().declaration(prefix));
        builder.append("\n");
        if (it.hasNext())
          builder.append("\n");
      }
    }

    return builder.toString();
  }

  /**
   * @return Promela code which represents this system
   */
  public String code() {
    return code("");
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.getClass().getName());
    builder.append(" ");
    builder.append(this.code());
    return builder.toString();
  }
}
