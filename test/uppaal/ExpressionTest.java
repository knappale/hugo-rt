package uppaal;

import static org.junit.Assert.*;

import org.junit.Test;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class ExpressionTest {
  /**
   * Test coding of integer expressions.
   */
  @Test
  public void testCoding() {
    String expected, actual;
    
    expected = "-1";
    actual = TExpression.intConst(-1).expression();
    assertEquals(expected, actual);

    expected = "-1";
    actual = TExpression.minus(TExpression.intConst(1)).expression();
    assertEquals(expected, actual);

    expected = "1";
    actual = TExpression.minus(TExpression.minus(TExpression.intConst(1))).expression();
    assertEquals(expected, actual);

    expected = "!x";
    actual = TExpression.neg(new TVariable("x").access()).expression();
    assertEquals(expected, actual);

    expected = "x";
    actual = TExpression.neg(TExpression.neg(new TVariable("x").access())).expression();
    assertEquals(expected, actual);

    expected = "-(1+2)";
    actual = TExpression.minus(TExpression.arithmetical(TExpression.intConst(1),
                                                                    TOperator.ADD,
                                                                    TExpression.intConst(2))).expression();
    assertEquals(expected, actual);

    expected = "1+2+3";
    actual = TExpression.arithmetical(TExpression.intConst(1),
                                            TOperator.ADD,
                                            TExpression.arithmetical(TExpression.intConst(2),
                                                                           TOperator.ADD,
                                                                           TExpression.intConst(3))).expression();
    assertEquals(expected, actual);
 
    expected = "(1-2)-3";
    actual = TExpression.arithmetical(TExpression.arithmetical(TExpression.intConst(1),
                                                                           TOperator.SUB,
                                                                           TExpression.intConst(2)),
                                            TOperator.SUB,
                                            TExpression.intConst(3)).expression();
    assertEquals(expected, actual);

    expected = "1-(2-3)";
    actual = TExpression.arithmetical(TExpression.intConst(1),
                                            TOperator.SUB,
                                            TExpression.arithmetical(TExpression.intConst(2),
                                                                           TOperator.SUB,
                                                                           TExpression.intConst(3))).expression();
    assertEquals(expected, actual);

    expected = "1-(2+3)";
    actual = TExpression.arithmetical(TExpression.intConst(1),
                                            TOperator.SUB,
                                            TExpression.arithmetical(TExpression.intConst(2),
                                                                           TOperator.ADD,
                                                                           TExpression.intConst(3))).expression();
    assertEquals(expected, actual);
 
    expected = "3*(2+1)";
    actual = TExpression.arithmetical(TExpression.intConst(3),
                                            TOperator.MULT,
                                            TExpression.arithmetical(TExpression.intConst(2),
                                                                           TOperator.ADD,
                                                                           TExpression.intConst(1))).expression();
    assertEquals(expected, actual);

    expected = "1+2*3";
    actual = TExpression.arithmetical(TExpression.intConst(1),
                                            TOperator.ADD,
                                            TExpression.arithmetical(TExpression.intConst(2),
                                                                           TOperator.MULT,
                                                                           TExpression.intConst(3))).expression();
    assertEquals(expected, actual);

    expected = "x && (x || y)";
    actual = TExpression.and(new TVariable("x").access(),
                                   TExpression.or(new TVariable("x").access(),
                                                        new TVariable("y").access())).expression();
    assertEquals(expected, actual);

    expected = "(x && y) || x";
    actual = TExpression.or(TExpression.and(new TVariable("x").access(),
                                                        new TVariable("y").access()),
                                  new TVariable("x").access()).expression();
    assertEquals(expected, actual);
  }

  /*
   * Test equality on integer expressions
   */
  @Test
  public void testEquals() {
    assertFalse(TExpression.intConst(1).equals(TExpression.intConst(2)));
    assertTrue(TExpression.intConst(1).equals(TExpression.intConst(1)));
    assertTrue(TExpression.trueConst() == TExpression.trueConst());
  }
}
