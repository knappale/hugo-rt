package uppaal;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 * @since May 30, 2013
 */
public class TransitionTest {
  TState state1 = null;
  TState state2 = null;
  TTransition transition = null;

  @Before
  public void setUp() throws Exception {
    TState state1 = new TState("S1");
    TState state2 = new TState("S2");
    TTransition transition = new TTransition(state1, state2);

    this.state1 = state1;
    this.state2 = state2;
    this.transition = transition;
  }

  @Test
  public void testDeclaration() {
    String expected = "";
    String actual = "";

    this.transition.addGuard(TExpression.or(TExpression.leq(TExpression.intConst(0), TExpression.intConst(1)),
                                           TExpression.eq(TExpression.intConst(1), TExpression.intConst(2))));

    expected = "  S1 -> S2 {\n" +
               "    guard 0 <= 1 || 1 == 2;\n" +
               "  }";
    actual = this.transition.declaration("  ");
    assertEquals(expected, actual);

    this.transition.addGuard(TExpression.geq(TExpression.intConst(2), TExpression.intConst(3)));

    expected = "  S1 -> S2 {\n" +
               "    guard (0 <= 1 || 1 == 2) && \n" +
               "          2 >= 3;\n" +
               "  }";
    actual = this.transition.declaration("  ");
    assertEquals(expected, actual);
  }
}
