package uppaal;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class StateTest {
  TClock clock1 = null;
  TClock clock2 = null;
  TState state = null;

  @Before
  public void setUp() throws Exception {
    this.clock1 = new TClock("clock1");
    this.clock2 = new TClock("clock2");
    this.state = new TState("S");

  }

  public void testDeclaration() {
    String expected = "";
    String actual = "";

    expected = "S";
    actual = state.declaration();
    assertEquals(expected, actual);

    state.addInvariant(TClockUpperBound.lt(this.clock1.access(), TExpression.intConst(5)));

    expected = "S { clock1 < 5 }";
    actual = state.declaration();
    assertEquals(expected, actual);

    state.addInvariant(TClockUpperBound.leq(this.clock2.access(), TExpression.intConst(2)));

    expected = "S { clock1 < 5 && \n" +
               "    clock2 <= 2 }";
    actual = state.declaration();
    assertEquals(expected, actual);

    state.addInvariant(TClockUpperBound.lt(this.clock2.access(), this.clock1.access(), TExpression.intConst(3)));

    expected = "S { clock1 < 5 && \n" +
               "    clock2 <= 2 && \n" +
               "    clock2-clock1 < 3 }";
    actual = state.declaration();
    assertEquals(expected, actual);
  }

  @Test
  public void testDeclarationXML() {
    String expected = "";
    String actual = "";

    expected = "<location id=\"S\">\n" +
               "<name>S</name>\n" +
               "</location>";
    actual = state.declarationXML();
    assertEquals(expected, actual);

    state.addInvariant(TClockUpperBound.lt(clock1.access(), TExpression.intConst(5)));

    expected = "<location id=\"S\">\n" +
               "<name>S</name>\n" +
               "<label kind=\"invariant\">clock1 &lt; 5</label>\n" +
               "</location>";
    actual = state.declarationXML();
    assertEquals(expected, actual);

    state.addInvariant(TClockUpperBound.leq(clock2.access(), TExpression.intConst(2)));

    expected = "<location id=\"S\">\n" +
               "<name>S</name>\n" +
               "<label kind=\"invariant\">clock1 &lt; 5 &amp;&amp; clock2 &lt;= 2</label>\n" +
               "</location>";
    actual = state.declarationXML();
    assertEquals(expected, actual);

    state.addInvariant(TClockUpperBound.lt(this.clock2.access(), this.clock1.access(), TExpression.intConst(3)));

    expected = "<location id=\"S\">\n" +
               "<name>S</name>\n" +
               "<label kind=\"invariant\">clock1 &lt; 5 &amp;&amp; clock2 &lt;= 2 &amp;&amp; clock2-clock1 &lt; 3</label>\n" +
               "</location>";
    actual = state.declarationXML();
    assertEquals(expected, actual);
  }
}
