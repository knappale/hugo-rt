package uppaal;

import static org.junit.Assert.*;

import org.junit.Test;

import uppaal.query.TFormula;
import uppaal.query.TTerm;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class QueryTest {
  @Test
  public void coding() {
    String expected = "";
    String actual = "";

    expected = "A[] true";
    actual = TQuery.ag(TFormula.trueConst()).declaration();
    assertEquals(expected, actual);

    expected = "A[] not deadlock";
    actual = TQuery.ag(TFormula.neg(TFormula.deadlock())).declaration();
    assertEquals(expected, actual);

    expected = "A[] not deadlock imply true";
    actual = TQuery.ag(TFormula.imply(TFormula.neg(TFormula.deadlock()), TFormula.trueConst())).declaration();
    assertEquals(expected, actual);

    expected = "A[] (not deadlock) && true";
    actual = TQuery.ag(TFormula.junctional(TFormula.neg(TFormula.deadlock()), uppaal.TOperator.AND, TFormula.trueConst())).declaration();
    assertEquals(expected, actual);

    expected = "A[] true && (not deadlock)";
    actual = TQuery.ag(TFormula.junctional(TFormula.trueConst(), uppaal.TOperator.AND, TFormula.neg(TFormula.deadlock()))).declaration();
    assertEquals(expected, actual);

    expected = "E<> not deadlock";
    actual = TQuery.ef(TFormula.and(TFormula.neg(TFormula.deadlock()),
                                         TFormula.or(TFormula.falseConst(),
                                                    TFormula.trueConst()))).declaration();
    assertEquals(expected, actual);

    expected = "E<> not deadlock and (1 == 1 or 2 == 1)";
    actual = TQuery.ef(TFormula.and(TFormula.neg(TFormula.deadlock()),
                                         TFormula.or(TFormula.term(TTerm.eq(TTerm.intConst(1), TTerm.intConst(1))),
                                                    TFormula.term(TTerm.eq(TTerm.intConst(2), TTerm.intConst(1)))))).declaration();
    assertEquals(expected, actual);

    expected = "E<> not deadlock or (1 == 1 and 2 == 1)";
    actual = TQuery.ef(TFormula.or(TFormula.neg(TFormula.deadlock()),
                                        TFormula.and(TFormula.term(TTerm.eq(TTerm.intConst(1), TTerm.intConst(1))),
                                                    TFormula.term(TTerm.eq(TTerm.intConst(2), TTerm.intConst(1)))))).declaration();
    assertEquals(expected, actual);
  }

  /*
   * Test equality on queries 
   */
  @Test
  public void equals() {
    assertFalse(TQuery.ag(TFormula.trueConst()).equals(TQuery.ef(TFormula.trueConst())));
    assertTrue(TQuery.ag(TFormula.deadlock()).equals(TQuery.ag(TFormula.deadlock())));
    assertTrue(TTerm.intConst(1).equals(TTerm.intConst(1)));
    assertTrue(TFormula.trueConst() == TFormula.trueConst());
  }
}
