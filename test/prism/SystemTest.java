package prism;

import static org.junit.Assert.*;

import org.junit.Test;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">Alexander Knapp</A>
 */
public class SystemTest {
  @Test
  public void declaration() {
    RVariable x = RVariable.integer("x", RExpression.intConst(0), RExpression.intConst(2), RExpression.intConst(0));
    RVariable y = RVariable.integer("y", RExpression.intConst(0), RExpression.intConst(2), RExpression.intConst(0));

    RExpression guardx0 = RExpression.eq(RExpression.access(x), RExpression.intConst(0));
    RExpression guardx1y = RExpression.and(RExpression.eq(RExpression.access(x), RExpression.intConst(1)), RExpression.neq(RExpression.access(y), RExpression.intConst(2)));
    RExpression guardx2 = RExpression.eq(RExpression.access(x), RExpression.intConst(2));

    RUpdate x0 = RUpdate.assign(x, RExpression.intConst(0));
    RUpdate x1 = RUpdate.assign(x, RExpression.intConst(1));
    RUpdate x2 = RUpdate.assign(x, RExpression.intConst(2));

    RCommand c11 = new RCommand("", guardx0, new RBranch(0.8, x0), new RBranch(0.2, x1));
    RCommand c12 = new RCommand("", guardx1y, new RBranch(x2));
    RCommand c13 = new RCommand("", guardx2, new RBranch(0.5, x2), new RBranch(0.5, x0));

    RModule m1 = new RModule("M1");
    m1.addVariable(x);
    m1.addCommands(c11, c12, c13);

    RExpression guardy0 = RExpression.eq(RExpression.access(y), RExpression.intConst(0));
    RExpression guardy1x = RExpression.and(RExpression.eq(RExpression.access(y), RExpression.intConst(1)), RExpression.neq(RExpression.access(x), RExpression.intConst(2)));
    RExpression guardy2 = RExpression.eq(RExpression.access(y), RExpression.intConst(2));

    RUpdate y0 = RUpdate.assign(y, RExpression.intConst(0));
    RUpdate y1 = RUpdate.assign(y, RExpression.intConst(1));
    RUpdate y2 = RUpdate.assign(y, RExpression.intConst(2));

    RCommand c21 = new RCommand("", guardy0, new RBranch(0.8, y0), new RBranch(0.2, y1));
    RCommand c22 = new RCommand("", guardy1x, new RBranch(y2));
    RCommand c23 = new RCommand("", guardy2, new RBranch(0.5, y2), new RBranch(0.5, y0));

    RModule m2 = new RModule("M2");
    m2.addVariable(y);
    m2.addCommands(c21, c22, c23);

    RSystem s = RSystem.mdp();
    s.addModule(m1);
    s.addModule(m2);

    String expected = "mdp\n" +
                      "\n" +
                      "module M1\n" +
                      "  x : [0..2] init 0;\n" +
                      "\n" +
                      "  [] x = 0 -> 0.8:(x' = 0) + 0.2:(x' = 1);\n" +
                      "  [] x = 1 & y != 2 -> 1.0:(x' = 2);\n" +
                      "  [] x = 2 -> 0.5:(x' = 2) + 0.5:(x' = 0);\n" +
                      "endmodule\n" +
                      "\n" +
                      "module M2\n" +
                      "  y : [0..2] init 0;\n" +
                      "\n" +
                      "  [] y = 0 -> 0.8:(y' = 0) + 0.2:(y' = 1);\n" +
                      "  [] y = 1 & x != 2 -> 1.0:(y' = 2);\n" +
                      "  [] y = 2 -> 0.5:(y' = 2) + 0.5:(y' = 0);\n" +
                      "endmodule\n";
    assertEquals(expected, s.declaration());
  }
}
