package util.properties;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;

import static util.Objects.requireNonNull;


public class PropertyGroupTest {
  BooleanProperty booleanProperty;
  IntegerProperty integerProperty;

  IntegerProperty integer1Property;
  IntegerProperty integer2Property;
  PropertyGroup propertyGroup;
  PropertyGroup heterogeneousPropertyGroup;
  PropertyGroup bigPropertyGroup;

  @Before
  public void setUp() throws Exception {
    BooleanProperty booleanProperty = new BooleanProperty("bool", "boolean", "b", false);
    IntegerProperty integerProperty = new IntegerProperty("int", "integer", "i", 0) {
                                        public void check(@NonNull Integer value) throws PropertyException {
                                          if (!(value >= 0))
                                            throw new PropertyException("Value must be greater or equal to zero");
                                        }
                                      };
    IntegerProperty integer1Property = new IntegerProperty("int1", "integer1", "i1", -2) {
                                         public void check(@NonNull Integer value) throws PropertyException {
                                          if (!(value <= 0))
                                            throw new PropertyException("Value must be less or equal to zero");
                                        }
                                      };
    IntegerProperty integer2Property = new IntegerProperty("int2", "integer2", "i2", 3) {
                                         public void check(@NonNull Integer value) throws PropertyException {
                                           if (!(value >= 0))
                                             throw new PropertyException("Value must be greater or equal to zero");
                                         }
                                       };
    PropertyGroup propertyGroup = new PropertyGroup("group", "group", "g", integer1Property, integer2Property) {
                                    public void check(List<Object> values) throws PropertyException {
                                      if (!((Integer)values.get(0) < (Integer)values.get(1)))
                                        throw new PropertyException("First value must be less than second value");
                                    }
                                  };
    PropertyGroup heterogeneousPropertyGroup = new PropertyGroup("het", "a heterogenous group", "h", integerProperty, booleanProperty, integer2Property) {
                                                 public void check(List<Object> values) throws PropertyException {
                                                 }
                                               };
    List<@NonNull IntegerProperty> integerProperties = new ArrayList<>();
    for (int i = 0; i < 5; i++)
      integerProperties.add(requireNonNull(integerProperty));
  
    PropertyGroup bigPropertyGroup = new PropertyGroup("big", "a big group", "f", integerProperties) {
                                       public void check(List<Object> values) throws PropertyException {
                                       }
                                     };

    this.booleanProperty = booleanProperty;
    this.integerProperty = integerProperty;
    this.integer1Property = integer1Property;
    this.integer2Property = integer2Property;
    this.propertyGroup = propertyGroup;
    this.heterogeneousPropertyGroup = heterogeneousPropertyGroup;
    this.bigPropertyGroup = bigPropertyGroup;
  }

  @Test
  public void matching() {
    assertTrue(propertyGroup.matchesByOption("-g"));
    assertTrue(propertyGroup.matchesByOption("--group"));
    assertTrue(propertyGroup.matchesByOption("  --group "));
    assertFalse(propertyGroup.matchesByOption("-group"));
    assertFalse(propertyGroup.matchesByOption("group"));

    assertFalse(propertyGroup.matchesByOption("group"));
    assertFalse(propertyGroup.matchesByOption("-group"));
    assertFalse(propertyGroup.matchesByOption("--g"));

    assertTrue(propertyGroup.matchesByName("group"));
    assertTrue(propertyGroup.matchesByName("GroUp"));
    assertFalse(propertyGroup.matchesByName("g"));
    assertFalse(propertyGroup.matchesByName("grp"));
    assertFalse(propertyGroup.matchesByName("-g"));
    assertFalse(propertyGroup.matchesByName("--group"));

    assertTrue(propertyGroup.matchesByName("group=true"));
    assertTrue(propertyGroup.matchesByName("group = true"));
    assertTrue(propertyGroup.matchesByName("group true"));
    assertTrue(propertyGroup.matchesByName("group true  bool"));
    assertTrue(propertyGroup.matchesByName("  group true  bool"));
    assertFalse(propertyGroup.matchesByName("grp=true"));
    assertFalse(propertyGroup.matchesByName(" grp = abc"));

    assertTrue(propertyGroup.matchesByOption("-g=abc"));
    assertTrue(propertyGroup.matchesByOption("--group=abc"));
    assertTrue(propertyGroup.matchesByOption("--grOup=abc"));
    assertFalse(propertyGroup.matchesByOption("-G=abc"));
    assertFalse(propertyGroup.matchesByOption("group=abc"));

    assertTrue(heterogeneousPropertyGroup.matchesByOption("-h=abc"));
    assertTrue(heterogeneousPropertyGroup.matchesByOption("--het=abc"));
    assertFalse(heterogeneousPropertyGroup.matchesByOption("het=abc"));
  }

  @Test
  public void parsing() {
    try {
      assertTrue(propertyGroup.parseByOption("--group=0,0"));
      assertEquals(Integer.valueOf(0), integer1Property.getValueToCommit());
      assertEquals(Integer.valueOf(0), integer2Property.getValueToCommit());
      assertTrue(propertyGroup.parseByOption("--group=-2,17"));
      assertEquals(Integer.valueOf(-2), integer1Property.getValueToCommit());
      assertEquals(Integer.valueOf(17), integer2Property.getValueToCommit());
      assertTrue(propertyGroup.parseByOption(" --group =  -3, 0"));
      assertEquals(Integer.valueOf(-3), integer1Property.getValueToCommit());
      assertEquals(Integer.valueOf(0), integer2Property.getValueToCommit());
      assertTrue(propertyGroup.parseByOption("--group=-11"));
      assertEquals(Integer.valueOf(-11), integer1Property.getValueToCommit());
      assertEquals(Integer.valueOf(0), integer2Property.getValueToCommit());
      assertTrue(propertyGroup.parseByOption("  -g =  31"));
      assertEquals(Integer.valueOf(31), integer1Property.getValueToCommit());
      assertEquals(Integer.valueOf(0), integer2Property.getValueToCommit());
      assertTrue(propertyGroup.parseByOption("  -g =  ,"));
      assertEquals(Integer.valueOf(31), integer1Property.getValueToCommit());
      assertEquals(Integer.valueOf(0), integer2Property.getValueToCommit());
      assertTrue(propertyGroup.parseByOption("  -g =  4,"));
      assertEquals(Integer.valueOf(4), integer1Property.getValueToCommit());
      assertEquals(Integer.valueOf(0), integer2Property.getValueToCommit());
      assertTrue(propertyGroup.parseByOption("  -g =  ,4"));
      assertEquals(Integer.valueOf(4), integer1Property.getValueToCommit());
      assertEquals(Integer.valueOf(4), integer2Property.getValueToCommit());
      assertFalse(propertyGroup.parseByOption("=3,  42"));
    } catch (PropertyException e) {
      fail(e.getMessage());
    }

    try {
      propertyGroup.parseByOption(" --group =  ");
      fail();
    } catch (PropertyException e) {
    }
    try {
      propertyGroup.parseByOption(" --group =  flse");
      fail();
    } catch (PropertyException e) {
    }
    try {
      propertyGroup.parseByOption(" --group =  3,3,3");
      fail();
    } catch (PropertyException e) {
    }
    try {
      propertyGroup.parseByOption(" --group =  3,3 3");
      fail();
    } catch (PropertyException e) {
    }
    try {
      propertyGroup.parseByOption(" --group =  3,,");
      fail();
    } catch (PropertyException e) {
    }
    try {
      propertyGroup.parseByOption(" --group =  3,false ");
      fail();
    } catch (PropertyException e) {
    }

    try {
      assertTrue(heterogeneousPropertyGroup.parseByOption("--het=0,true,0"));
      assertEquals(Integer.valueOf(0), integerProperty.getValueToCommit());
      assertEquals(true, booleanProperty.getValueToCommit());
      assertEquals(Integer.valueOf(0), integer2Property.getValueToCommit());
      assertTrue(heterogeneousPropertyGroup.parseByOption("--het=3, false,17"));
      assertEquals(Integer.valueOf(3), integerProperty.getValueToCommit());
      assertEquals(false, booleanProperty.getValueToCommit());
      assertEquals(Integer.valueOf(17), integer2Property.getValueToCommit());
      assertTrue(heterogeneousPropertyGroup.parseByOption(" --het =  -3, true"));
      assertEquals(Integer.valueOf(-3), integerProperty.getValueToCommit());
      assertEquals(true, booleanProperty.getValueToCommit());
      assertEquals(Integer.valueOf(17), integer2Property.getValueToCommit());
      assertTrue(heterogeneousPropertyGroup.parseByOption("--het=-11"));
      assertEquals(Integer.valueOf(-11), integerProperty.getValueToCommit());
      assertEquals(true, booleanProperty.getValueToCommit());
      assertEquals(Integer.valueOf(17), integer2Property.getValueToCommit());
      assertTrue(heterogeneousPropertyGroup.parseByOption("  -h =  31"));
      assertEquals(Integer.valueOf(31), integerProperty.getValueToCommit());
      assertEquals(true, booleanProperty.getValueToCommit());
      assertEquals(Integer.valueOf(17), integer2Property.getValueToCommit());
      assertTrue(heterogeneousPropertyGroup.parseByOption("  -h =  ,"));
      assertEquals(Integer.valueOf(31), integerProperty.getValueToCommit());
      assertEquals(true, booleanProperty.getValueToCommit());
      assertEquals(Integer.valueOf(17), integer2Property.getValueToCommit());
      assertTrue(heterogeneousPropertyGroup.parseByOption("  -h =  , ,"));
      assertEquals(Integer.valueOf(31), integerProperty.getValueToCommit());
      assertEquals(true, booleanProperty.getValueToCommit());
      assertEquals(Integer.valueOf(17), integer2Property.getValueToCommit());
      assertTrue(heterogeneousPropertyGroup.parseByOption("  -h =  4,,"));
      assertEquals(Integer.valueOf(4), integerProperty.getValueToCommit());
      assertEquals(true, booleanProperty.getValueToCommit());
      assertEquals(Integer.valueOf(17), integer2Property.getValueToCommit());
      assertTrue(heterogeneousPropertyGroup.parseByOption("  -h =  ,false"));
      assertEquals(Integer.valueOf(4), integerProperty.getValueToCommit());
      assertEquals(false, booleanProperty.getValueToCommit());
      assertEquals(Integer.valueOf(17), integer2Property.getValueToCommit());
      assertTrue(heterogeneousPropertyGroup.parseByOption("  -h =  ,,4"));
      assertEquals(Integer.valueOf(4), integerProperty.getValueToCommit());
      assertEquals(false, booleanProperty.getValueToCommit());
      assertEquals(Integer.valueOf(4), integer2Property.getValueToCommit());
      assertFalse(heterogeneousPropertyGroup.parseByOption("=3,  42"));
    } catch (PropertyException e) {
      fail(e.getMessage());
    }

    try {
      heterogeneousPropertyGroup.parseByOption(" --het =  ,,, ");
      fail();
    } catch (PropertyException e) {
    }
  }

  @Test
  public void checking() {
    try {
      propertyGroup.parseByOption("--group=-1,4");
      integer1Property.check();
      integer2Property.check();
      propertyGroup.check();
    } catch (PropertyException e) {
      fail();
    }

    try {
      propertyGroup.parseByOption("--group=0,0");
      integer1Property.check();
      integer2Property.check();
      propertyGroup.check();
      fail();
    } catch (PropertyException e) {
    }
  }

  @Test
  public void committing() {
    try {
      propertyGroup.parseByOption("--group=-1,4");
      propertyGroup.commit();
    } catch (PropertyException e) {
      fail(e.getMessage());
    }
    assertEquals(Integer.valueOf(-1), integer1Property.getValue());
    assertEquals(Integer.valueOf(4), integer2Property.getValue());

    try {
      propertyGroup.parseByOption("--group=0,0");
      propertyGroup.commit();
      fail();
    } catch (PropertyException e) {
      assertEquals(Integer.valueOf(-1), integer1Property.getValue());
      assertEquals(Integer.valueOf(4), integer2Property.getValue());
    }
  }

  @Test
  public void aborting() {
    try {
      propertyGroup.parseByOption("--group=-1,4");
      propertyGroup.abort();
    } catch (PropertyException e) {
      fail(e.getMessage());
    }
    assertEquals(Integer.valueOf(-2), integer1Property.getValue());
    assertEquals(Integer.valueOf(3), integer2Property.getValue());

    try {
      propertyGroup.parseByOption("--group=0,0");
      propertyGroup.abort();
    } catch (PropertyException e) {
      fail();
    }
    assertEquals(Integer.valueOf(-2), integer1Property.getValue());
    assertEquals(Integer.valueOf(3), integer2Property.getValue());
  }

  @Test
  public void strings() {
    assertEquals("-g, --group=(<int> | [<int>],[<int>])\n" +
                 "   group (currently = -2, 3)", propertyGroup.optionString(""));
    assertEquals("-h, --het=(<int> | [<int>],[[(false | true)],[<int>]])\n" +
                 "   a heterogenous group (currently = 0, false, 3)", heterogeneousPropertyGroup.optionString(""));
    assertEquals("-f, --big=(<int> | [<int>],[[<int>],[[<int>],[[<int>],[<int>]]]])\n" +
                 "   a big group (currently = 0, 0, 0, 0, 0)", bigPropertyGroup.optionString(""));
  }
}
