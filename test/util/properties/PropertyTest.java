package util.properties;

import static org.junit.Assert.*;

import org.eclipse.jdt.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;


public class PropertyTest {
  BooleanProperty booleanProperty = null;
  IntegerProperty integerProperty = null;

  IntegerProperty integer1Property = null;
  IntegerProperty integer2Property = null;
  PropertyGroup propertyGroup = null;
  PropertyGroup property2Group = null;
  PropertyGroup bigPropertyGroup = null;

  @Before
  public void setUp() throws Exception {
    this.booleanProperty = new BooleanProperty("bool", "boolean", "b", false);
    this.integerProperty = new IntegerProperty("int", "integer", "i", 0) {
                             public void check(@NonNull Integer value) throws PropertyException {
                               if (!(value >= 0))
                                 throw new PropertyException("Value must be greater or equal to zero");
                             }
                           };
  }

  @Test
  public void matching() {
    assertTrue(booleanProperty.matchesByOption("-b"));
    assertTrue(booleanProperty.matchesByOption("--bool"));
    assertTrue(booleanProperty.matchesByOption("  --bool "));
    assertFalse(booleanProperty.matchesByOption("-bool"));
    assertFalse(booleanProperty.matchesByOption("bool"));

    assertFalse(booleanProperty.matchesByOption("bool"));
    assertFalse(booleanProperty.matchesByOption("-bool"));
    assertFalse(booleanProperty.matchesByOption("--b"));

    assertTrue(booleanProperty.matchesByName("bool"));
    assertTrue(booleanProperty.matchesByName("bOOl"));
    assertFalse(booleanProperty.matchesByName("b"));
    assertFalse(booleanProperty.matchesByName("bol"));
    assertFalse(booleanProperty.matchesByName("-b"));
    assertFalse(booleanProperty.matchesByName("--bool"));

    assertTrue(booleanProperty.matchesByName("bool=true"));
    assertTrue(booleanProperty.matchesByName("bool = true"));
    assertTrue(booleanProperty.matchesByName("bool true"));
    assertTrue(booleanProperty.matchesByName("bool true  bool"));
    assertTrue(booleanProperty.matchesByName("  bool true  bool"));
    assertFalse(booleanProperty.matchesByName("bol=true"));
    assertFalse(booleanProperty.matchesByName(" bol = abc"));

    assertTrue(booleanProperty.matchesByOption("-b=abc"));
    assertTrue(booleanProperty.matchesByOption("--bool=abc"));
    assertTrue(booleanProperty.matchesByOption("--BooL=abc"));
    assertFalse(booleanProperty.matchesByOption("-B=abc"));
    assertFalse(booleanProperty.matchesByOption("bool=abc"));
    assertFalse(booleanProperty.matchesByOption("-- bool=abc"));

    assertTrue(integerProperty.matchesByOption("-i=abc"));
    assertTrue(integerProperty.matchesByOption("--int=abc"));
    assertFalse(integerProperty.matchesByOption("bool=abc"));
  }

  @Test
  public void parsing() {
    try {
      assertTrue(booleanProperty.parseByName("bool=true"));
      assertEquals(true, booleanProperty.getValueToCommit());
      assertTrue(booleanProperty.parseByName("bool=false"));
      assertEquals(false, booleanProperty.getValueToCommit());
      assertTrue(booleanProperty.parseByName(" bool =  true"));
      assertEquals(true, booleanProperty.getValueToCommit());
      assertTrue(booleanProperty.parseByName(" bool =  false"));
      assertEquals(false, booleanProperty.getValueToCommit());
      assertTrue(booleanProperty.parseByOption(" -b"));
      assertEquals(true, booleanProperty.getValueToCommit());
      assertTrue(booleanProperty.parseByOption(" --bool "));
      assertEquals(true, booleanProperty.getValueToCommit());
      assertFalse(booleanProperty.parseByName(" = false"));
      assertEquals(true, booleanProperty.getValueToCommit());
    } catch (PropertyException e) {
      fail(e.getMessage());
    }

    try {
      booleanProperty.parseByName(" bool ");
      fail();
    } catch (PropertyException e) {
    }
    try {
      booleanProperty.parseByName("  bool true");
      fail();
    } catch (PropertyException e) {
    }
    try {
      booleanProperty.parseByName("  bool true  test");
      fail();
    } catch (PropertyException e) {
    }
    try {
      booleanProperty.parseByName(" bool =  flse");
      fail();
    } catch (PropertyException e) {
    }
    try {
      booleanProperty.parseByName(" bool =");
      fail();
    } catch (PropertyException e) {
    }

    try {
      assertTrue(integerProperty.parseByName("int=0"));
      assertEquals(Integer.valueOf(0), integerProperty.getValueToCommit());
      assertTrue(integerProperty.parseByName("int=17"));
      assertEquals(Integer.valueOf(17), integerProperty.getValueToCommit());
      assertTrue(integerProperty.parseByName(" int =  -3"));
      assertEquals(Integer.valueOf(-3), integerProperty.getValueToCommit());
      assertTrue(integerProperty.parseByName(" int =  42"));
      assertEquals(Integer.valueOf(42), integerProperty.getValueToCommit());
      assertTrue(integerProperty.parseByOption("--int=-11"));
      assertEquals(Integer.valueOf(-11), integerProperty.getValueToCommit());
      assertTrue(integerProperty.parseByOption("  -i =  31"));
      assertEquals(Integer.valueOf(31), integerProperty.getValueToCommit());
      assertTrue(integerProperty.parseByOption(" -i"));
      assertEquals(Integer.valueOf(1), integerProperty.getValueToCommit());
      assertTrue(integerProperty.parseByOption(" --int"));
      assertEquals(Integer.valueOf(1), integerProperty.getValueToCommit());
      assertFalse(integerProperty.parseByName("=3"));
      assertEquals(Integer.valueOf(1), integerProperty.getValueToCommit());
    } catch (PropertyException e) {
      fail(e.getMessage());
    }

    try {
      integerProperty.parseByName("int true");
      fail();
    } catch (PropertyException e) {
    }

    try {
      integerProperty.parseByName("int 3  test");
      fail();
    } catch (PropertyException e) {
    }
    try {
      integerProperty.parseByName("  int true  test");
      fail();
    } catch (PropertyException e) {
    }
    try {
      integerProperty.parseByName(" int =  flse");
      fail();
    } catch (PropertyException e) {
    }
    try {
      integerProperty.parseByName(" int =  ,");
      fail();
    } catch (PropertyException e) {
    }
  }

  @Test
  public void checking() {
    try {
      booleanProperty.parseByOption("-b");
      booleanProperty.check();
    } catch (PropertyException pe) {
      fail(pe.getMessage());
    }

    try {
      integerProperty.parseByOption("-i=3");
      booleanProperty.check();
    } catch (PropertyException pe) {
      fail(pe.getMessage());
    }

    try {
      integerProperty.parseByOption("-i=-2");
      integerProperty.check();
      fail();
    } catch (PropertyException pe) {
    }

    try {
      integerProperty.parseByName("int=42");
      integerProperty.check();
    } catch (PropertyException pe) {
      fail();
    }
  }

  @Test
  public void committing() {
    try {
      booleanProperty.parseByOption("-b");
      booleanProperty.commit();
    } catch (PropertyException e) {
      fail(e.getMessage());
    }
    assertEquals(true, booleanProperty.getValue());

    try {
      integerProperty.parseByOption("-i=3");
      integerProperty.commit();
    } catch (PropertyException e) {
      fail(e.getMessage());
    }
    assertEquals(Integer.valueOf(3), integerProperty.getValue());

    try {
      integerProperty.parseByName("int=42");
      integerProperty.commit();
    } catch (PropertyException e) {
      fail(e.getMessage());
    }
    assertEquals(Integer.valueOf(42), integerProperty.getValue());

    try {
      integerProperty.parseByOption("-i=-2");
      integerProperty.commit();
      fail();
    } catch (PropertyException e) {
      assertEquals(Integer.valueOf(42), integerProperty.getValue());
    }
  }

  @Test
  public void aborting() {
    try {
      booleanProperty.parseByOption("-b=true");
      booleanProperty.abort();
    } catch (PropertyException e) {
      fail(e.getMessage());
    }
    assertEquals(false, booleanProperty.getValue());

    try {
      integerProperty.parseByOption("-i=3");
      integerProperty.abort();
    } catch (PropertyException e) {
      fail(e.getMessage());
    }
    assertEquals(Integer.valueOf(0), integerProperty.getValue());

    try {
      integerProperty.parseByName("int=42");
      integerProperty.abort();
    } catch (PropertyException e) {
      fail(e.getMessage());
    }
    assertEquals(Integer.valueOf(0), integerProperty.getValue());

    try {
      integerProperty.parseByOption("-i=-2");
      integerProperty.abort();
    } catch (PropertyException e) {
      fail(e.getMessage());
    }
    assertEquals(Integer.valueOf(0), integerProperty.getValue());
  }

  @Test
  public void strings() {
    assertEquals("-i, --int[=<int>] (no value means 1)\n" +
                 "   integer (currently = 0)", integerProperty.optionString(""));
  }
}
