package control;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class PropertiesTest {
  Properties properties = null;

  @Before
  public void setUp() throws Exception {
    this.properties = Properties.getDefault();
  }

  @Test
  public void test() {
    assertEquals(this.properties, this.properties.getCopy());
  }
}
