package uml.parser.ute;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.IOException;

import org.junit.Test;

import uml.UClass;
import uml.UModel;
import uml.UModelException;
import uml.parser.ModelReader;
import uml.statemachine.UStateMachine;

public class ModelReaderTest {
  @Test
  public void test() {
    String modelFileName = "test/uml/parser/ute/atm.ute";

    try {
      // util.Message.debug = true;
      util.Message.verbosity = util.Message.INFO;
      util.Message.debug = true;

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      ModelReader modelReader = ModelReader.create(modelInputStream);
      assertEquals(modelReader.getModelName(), "ATM");

      UModel uModel = modelReader.getModel();
      util.Message.info(uModel.declaration());

      UClass uClass = uModel.getClasses().stream().filter(uC -> uC.getName().equals("Bank")).findAny().get();
      UStateMachine uStateMachine = uClass.getStateMachine();
      assertFalse(uStateMachine == null);
    }
    catch (UModelException me) {
      fail("Could not parse ATM model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read ATM model: " + ioe.getMessage());
    }
  }
}
