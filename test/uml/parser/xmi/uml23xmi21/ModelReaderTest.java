package uml.parser.xmi.uml23xmi21;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.IOException;

import org.junit.Test;

import uml.UClass;
import uml.UModel;
import uml.UModelException;
import uml.parser.ModelReader;
import uml.statemachine.UStateMachine;
import uml.statemachine.semantics.UCompletionTree;

public class ModelReaderTest {
  @Test
  public void test() {
    String modelFileName = "test/uml/parser/xmi/uml23xmi21/atm.uml";

    try {
      util.Message.verbosity = util.Message.INFO;
      util.Message.debug = false;

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      ModelReader modelReader = ModelReader.create(modelInputStream);
      assertEquals(modelReader.getModelName(), "atm");

      UModel model = modelReader.getModel();
      util.Message.info(model.declaration());

      UClass uClass = model.getC1ass("Bank");
      assertNotNull(uClass);
      UStateMachine stateMachine = uClass.getStateMachine();
      assertNotNull(stateMachine);

      util.Message.info(UCompletionTree.create(stateMachine).toString());
    }
    catch (UModelException me) {
      fail("Could not parse ATM model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read ATM model: " + ioe.getMessage());
    }
  }
}
