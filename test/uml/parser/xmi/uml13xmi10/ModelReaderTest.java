package uml.parser.xmi.uml13xmi10;

import java.io.*;

import static org.junit.Assert.*;

import org.junit.Test;

import uml.UClass;
import uml.statemachine.UStateMachine;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class ModelReaderTest {
  /*
   * Test method for 'uml.parser.ModelReader.getModel()'
   */
  @Test
  public void getModel_XMI10() {
    String modelFileName = "test/uml/parser/xmi/uml13xmi10/argouml-0.15-test.xmi";

    try {
      util.Message.debug = true;
      util.Message.verbosity = util.Message.INFO;

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(modelInputStream);
      assertEquals(modelReader.getModelName(), "GRC");

      uml.UModel model = modelReader.getModel();
      assertEquals(model.getClasses().size(), 4);
      assertEquals(model.getCollaborations().size(), 1);

      UClass uClass = model.getC1ass("Control");
      assertNotNull(uClass);
      UStateMachine stateMachine = uClass.getStateMachine();
      assertNotNull(stateMachine);

      uml.UCollaboration collaboration = model.getCollaborations().iterator().next();
      assertEquals(collaboration.getObjects().size(), 4);
      assertEquals(collaboration.getConstraints().size(), 1);

      uml.UObject object = collaboration.getObject("ctl");
      assertNotNull(object);
    }
    catch (uml.UModelException me) {
      fail("Could not parse ArgoUML 0.15 model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read ArgoUML 0.15 model");
    }
  }

  /*
   * Test method for 'uml.parser.ModelReader.getModel()'
   */
  @Test
  public void getModel_XMI10_NSUML() {
    String modelFileName = "test/uml/parser/xmi/uml13xmi10/argouml-0.15-test.xmi";

    try {
      util.Message.debug = true;
      util.Message.verbosity = util.Message.INFO;

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(modelInputStream);
      assertEquals(modelReader.getModelName(), "GRC");

      uml.UModel model = modelReader.getModel();
      assertEquals(model.getClasses().size(), 4);
      assertEquals(model.getCollaborations().size(), 1);

      uml.UCollaboration collaboration = model.getCollaborations().iterator().next();
      assertEquals(collaboration.getObjects().size(), 4);
      assertEquals(collaboration.getConstraints().size(), 1);

      uml.UObject object = collaboration.getObject("ctl");
      assertNotNull(object);

      util.Message.info(model.declaration());
    }
    catch (uml.UModelException me) {
      fail("Could not parse ArgoUML 0.15 model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read ArgoUML 0.15 model");
    }
  }
}
