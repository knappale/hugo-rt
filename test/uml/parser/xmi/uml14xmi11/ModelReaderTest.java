package uml.parser.xmi.uml14xmi11;

import java.io.*;

import static org.junit.Assert.*;

import org.junit.Test;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class ModelReaderTest {
  /*
   * Test method for 'uml.parser.ModelReader.getModel()'
   */
  @Test
  public void getModel_XMI12() {
    String modelFileName = "test/uml/parser/xmi/uml14xmi11/poseidon-3.0.1-test.xmi";

    try {
      // util.Message.debug = true;
      util.Message.verbosity = util.Message.INFO;

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(modelInputStream);
      assertEquals(modelReader.getModelName(), "Test");

      uml.UModel model = modelReader.getModel();

      util.Message.info(model.declaration());
    }
    catch (uml.UModelException me) {
      fail("Could not parse GRC model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read GRC model: " + ioe.getMessage());
    }
  }
}
