package uml.ocl;

import static org.junit.Assert.*;

import org.junit.Test;

import util.Identifier;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class OExpressionTest {
  /**
   * Test coding of OCL expressions.
   */
  @Test
  public void testCoding() {
    String expected, actual;

    expected = "-1";
    actual = OExpression.intConst(-1).toString();
    assertEquals(expected, actual);

    expected = "-1";
    actual = OExpression.unary(OOperator.UMINUS, OExpression.intConst(1)).toString();
    assertEquals(expected, actual);

    expected = "1";
    actual = OExpression.unary(OOperator.UMINUS, OExpression.unary(OOperator.UMINUS, OExpression.intConst(1))).toString();
    assertEquals(expected, actual);

    expected = "q.x";
    actual = OExpression.id(Identifier.id("q"), Identifier.id("x")).toString();
    assertEquals(expected, actual);

    expected = "q.x < q.y";
    actual = OExpression.relational(OExpression.id(Identifier.id("q"), Identifier.id("x")),
                                    OOperator.LT, 
                                    OExpression.id(Identifier.id("q"), Identifier.id("y"))).toString();
    assertEquals(expected, actual);

    expected = "q.x >= q.y+1";
    actual = OExpression.relational(OExpression.id(Identifier.id("q"), Identifier.id("x")),
                                    OOperator.GEQ,
                                    OExpression.arithmetical(OExpression.id(Identifier.id("q"), Identifier.id("y")),
                                                             OOperator.ADD,
                                                             OExpression.intConst(1))).toString();
    assertEquals(expected, actual);

    expected = "-(1+2)";
    actual = OExpression.unary(OOperator.UMINUS,
                               OExpression.arithmetical(OExpression.intConst(1),
                                                        OOperator.ADD,
                                                        OExpression.intConst(2))).toString();
    assertEquals(expected, actual);

    expected = "1+2+3";
    actual = OExpression.arithmetical(OExpression.intConst(1),
                                      OOperator.ADD,
                                      OExpression.arithmetical(OExpression.intConst(2),
                                                               OOperator.ADD,
                                                               OExpression.intConst(3))).toString();
    assertEquals(expected, actual);

    expected = "(1-2)-3";
    actual = OExpression.arithmetical(OExpression.arithmetical(OExpression.intConst(1),
                                                             OOperator.SUB,
                                                             OExpression.intConst(2)),
                                      OOperator.SUB,
                                      OExpression.intConst(3)).toString();
    assertEquals(expected, actual);

    expected = "1-(2-3)";
    actual = OExpression.arithmetical(OExpression.intConst(1),
                                      OOperator.SUB,
                                      OExpression.arithmetical(OExpression.intConst(2),
                                                              OOperator.SUB,
                                                              OExpression.intConst(3))).toString();
    assertEquals(expected, actual);

    expected = "1-(2+3)";
    actual = OExpression.arithmetical(OExpression.intConst(1),
                                      OOperator.SUB,
                                      OExpression.arithmetical(OExpression.intConst(2),
                                                               OOperator.ADD,
                                                               OExpression.intConst(3))).toString();
    assertEquals(expected, actual);

    expected = "3*(2+1)";
    actual = OExpression.arithmetical(OExpression.intConst(3),
                                      OOperator.MULT,
                                      OExpression.arithmetical(OExpression.intConst(2),
                                                               OOperator.ADD,
                                                               OExpression.intConst(1))).toString();
    assertEquals(expected, actual);

    expected = "1+2*3";
    actual = OExpression.arithmetical(OExpression.intConst(1),
                                      OOperator.ADD,
                                      OExpression.arithmetical(OExpression.intConst(2),
                                                               OOperator.MULT,
                                                               OExpression.intConst(3))).toString();
    assertEquals(expected, actual);
  }

  /**
   * Test equality on expressions.
   */
  @Test
  public void testEquals() {
    assertFalse(OExpression.intConst(1).equals(OExpression.intConst(2)));
    assertTrue(OExpression.intConst(1).equals(OExpression.intConst(1)));
    assertTrue(OExpression.trueConst() == OExpression.trueConst());
  }
}
