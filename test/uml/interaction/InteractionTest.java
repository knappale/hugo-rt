package uml.interaction;

import java.util.*;

import static org.junit.Assert.*;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.Before;
import org.junit.Test;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class InteractionTest {
  private UInteraction interaction = null;

  @Before
  public void setUp() {
    uml.UModel model = new uml.UModel("test");
    uml.UClass class0 = new uml.UClass("Test1", model);
    model.addClass(class0);
    uml.UClass class1 = new uml.UClass("Test2", model);
    model.addClass(class1);
    uml.UOperation operation0 = new uml.UOperation("op", class0, false);
    class0.addOperation(operation0);
    uml.UOperation operation1 = new uml.UOperation("op", class1, false);
    class1.addOperation(operation1);
    
    uml.UCollaboration collaboration = new uml.UCollaboration("test", model);
    uml.UObject object0 = new uml.UObject("inst1", class0);
    collaboration.addObject(object0);
    uml.UObject object1 = new uml.UObject("inst2", class1);
    collaboration.addObject(object1);
    uml.interaction.UMessage message0 = new UMessage("msg0", object0, operation1, new ArrayList<>(), object1);
    uml.interaction.UMessage message1 = new UMessage("msg1", object1, operation0, new ArrayList<>(), object0);
    message1.addPredecessor(message0);
    uml.interaction.UMessage message2 = new UMessage("msg2", object0, operation1, new ArrayList<>(), object1);
    message2.addPredecessor(message1);
    uml.interaction.UMessage message3 = new UMessage("msg3", object0, operation1, new ArrayList<>(), object1);
    message3.addPredecessor(message2);
    Set<@NonNull UMessage> messagesSet = new HashSet<>();
    messagesSet.add(message0);
    messagesSet.add(message1);
    messagesSet.add(message2);
    messagesSet.add(message3);

    this.interaction = collaboration.addInteraction("test");
    this.interaction.addMessages(messagesSet);
  }

  /**
   * Test whether the set of messages before and after sorting is of same size.
   */
  @Test
  public void messageSorting() {
    assertTrue(this.interaction.getMessages().size() == this.interaction.getSortedMessages().size());
  }
}
