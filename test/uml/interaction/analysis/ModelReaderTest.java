package uml.interaction.analysis;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.IOException;

import org.junit.Test;

import uml.UModel;
import uml.UModelException;
import uml.parser.ModelReader;


public class ModelReaderTest {
  @Test
  public void test() {
    String modelFileName = "models/test/call.uml";

    try {
      util.Message.verbosity = util.Message.INFO;
      util.Message.debug = true;

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      ModelReader modelReader = ModelReader.create(modelInputStream);
      assertEquals(modelReader.getModelName(), "Model");

      UModel model = modelReader.getModel();
      util.Message.info(model.declaration());
    }
    catch (UModelException me) {
      fail("Could not parse Call model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read Call model: " + ioe.getMessage());
    }
  }
}
