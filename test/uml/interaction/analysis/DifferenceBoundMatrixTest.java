package uml.interaction.analysis;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.IOException;

import org.junit.Test;

import uml.UModelException;
import util.Formatter;
import util.properties.PropertyException;


public class DifferenceBoundMatrixTest {
  @Test
  public void translationTest() {
    var modelFileName = "test/uml/interaction/analysis/test2.ute";

    try {
      util.Message.debug = false;
      util.Message.verbosity = util.Message.INFO;
      var properties = control.Properties.getDefault().getCopy();
      properties.setPropertyByName("phaseBased", "true");
      properties.commit();
      control.Properties.setDefault(properties);

      var modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      var modelReader = uml.parser.ModelReader.create(modelInputStream);

      var uModel = modelReader.getModel();
      var uCollaboration = uModel.getCollaborations().stream().filter(uC -> uC.getName().equals("Request")).findAny().get();
      var uInteraction = uCollaboration.getInteractions().stream().filter(uI -> uI.getName().equals("Timing")).findAny().get();
      var matrices = new uml.interaction.analysis.Analysis(uInteraction).getMatrices();

      for (var matrix : matrices) {
        // util.Message.info("Difference bound matrix:\n" + matrix.toString());
        // util.Message.info("Canonical difference bound matrix:\n" + matrix.canonical().toString());
        util.Message.info("Inconsistencies:\n" + Formatter.set(matrix.getInconsistencies()));
      }
    }
    catch (PropertyException pe) {
      fail("Could not change properties: " + pe.getMessage());
    }
    catch (UModelException me) {
      fail("Could not parse UTE model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read UTE model");
    }
  }
}
