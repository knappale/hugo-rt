package uml.ida;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class AutomatonTest {
  @Before
  public void setUp() {
  }

  @Test
  public void testIsRecurringTriviallyNonRecurring1() {
    IAutomaton automaton = new IAutomaton();
    IState state1 = automaton.addState();
    automaton.setInitialState(state1);

    assertFalse(automaton.isRecurring());
  }

  @Test
  public void testIsRecurringTriviallyNonRecurring2() {
    IAutomaton automaton = new IAutomaton();
    IState state1 = automaton.addState();
    automaton.addRecurrenceState(state1);
    automaton.setInitialState(state1);

    assertFalse(automaton.isRecurring());    
  }

  @Test
  public void testIsRecurringTriviallyRecurring() {
    IAutomaton automaton = new IAutomaton();
    IState state1 = automaton.addState();
    automaton.setInitialState(state1);
    automaton.addRecurrenceState(state1);
    automaton.addTransition(state1, null, null, null, state1);    

    assertTrue(automaton.isRecurring());
  }

  @Test
  public void testIsRecurringRecurring() {
    IAutomaton automaton = new IAutomaton();
    IState state0 = automaton.addState("s0_");
    automaton.setInitialState(state0);
    IState state1 = automaton.addState("s1_");
    automaton.addRecurrenceState(state1);
    IState state2 = automaton.addState("s2_");
    IState state3 = automaton.addState("s3_");
    IState state4 = automaton.addState("s4_");
    automaton.addRecurrenceState(state4);
    IState state5 = automaton.addState("s5_");
    IState state6 = automaton.addState("s6_");
    IState state7 = automaton.addState("s7_");

    automaton.addTransition(state0, null, null, null, state1);
    automaton.addTransition(state0, null, null, null, state4);
    automaton.addTransition(state1, null, null, null, state2);
    automaton.addTransition(state2, null, null, null, state3);
    automaton.addTransition(state3, null, null, null, state2);
    automaton.addTransition(state4, null, null, null, state5);
    automaton.addTransition(state5, null, null, null, state6);
    automaton.addTransition(state5, null, null, null, state3);
    automaton.addTransition(state6, null, null, null, state7);
    automaton.addTransition(state7, null, null, null, state4);
    automaton.addTransition(state7, null, null, null, state5);

    assertTrue(automaton.isRecurring());
  }

  @Test
  public void testIsRecurringNotRecurring() {
    // Same automaton as above, but lacking transition from
    // state7 to state4
    IAutomaton automaton = new IAutomaton();
    IState state0 = automaton.addState("s0_");
    automaton.setInitialState(state0);
    IState state1 = automaton.addState("s1_");
    automaton.addRecurrenceState(state1);
    IState state2 = automaton.addState("s2_");
    IState state3 = automaton.addState("s3_");
    IState state4 = automaton.addState("s4_");
    automaton.addRecurrenceState(state4);
    IState state5 = automaton.addState("s5_");
    IState state6 = automaton.addState("s6_");
    IState state7 = automaton.addState("s7_");

    automaton.addTransition(state0, null, null, null, state1);
    automaton.addTransition(state0, null, null, null, state4);
    automaton.addTransition(state1, null, null, null, state2);
    automaton.addTransition(state2, null, null, null, state3);
    automaton.addTransition(state3, null, null, null, state2);
    automaton.addTransition(state4, null, null, null, state5);
    automaton.addTransition(state5, null, null, null, state6);
    automaton.addTransition(state5, null, null, null, state3);
    automaton.addTransition(state6, null, null, null, state7);
    automaton.addTransition(state7, null, null, null, state5);

    assertFalse(automaton.isRecurring());
  }
}
