package uml.statemachine.semantics;

import uml.UClass;
import uml.UModel;
import uml.UModelException;
import uml.parser.ModelReader;
import uml.statemachine.UStateMachine;
import util.properties.PropertyException;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import org.junit.Test;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class ConfigurationsTest {
  @Test
  public void testCreate() {
    String modelString =
      "model test {" +
      "  class Test {" +
      "    behaviour {" +
      "      states {" +
      "        initial Initial;" +
      "        concurrent Concurrent1 {" +
      "          region Region1 {" +
      "            concurrent Concurrent1_1 {" +
      "              composite Region1_1 {" +
      "                simple Simple1_1_1;" +
      "                simple Simple1_1_2;" +
      "                final Final1_1;" +
      "                Simple1_1_1 -> Simple1_1_2;" +
      "                Simple1_1_2 -> Final1_1;" +
      "              }" +
      "              composite Region1_2 {" +
      "                simple Simple1_2_1;" +
      "                simple Simple1_2_2;" +
      "                final Final1_2;" +
      "                Simple1_2_1 -> Simple1_2_2;" +
      "                Simple1_2_2 -> Final1_2;" +
      "              }" +
      "            }" +
      "            final Final1;" +
      "          }" +     
      "          region Region2 {" +
      "            final Final2;" +
      "          }" +
      "        }" +
      "        final Final;" +
      "      }" +
      "      transitions {" +
      "        Concurrent1 -> Final;" +
      "      }" +
      "    }" +
      "  }" +
      "}";

    try {
      ModelReader modelReader = ModelReader.create(new BufferedInputStream(new ByteArrayInputStream(modelString.getBytes())));
      UModel uModel = modelReader.getModel();
      UStateMachine uStateMachine = uModel.getClasses().iterator().next().getStateMachine();
      assertFalse(uStateMachine == null);
      assertEquals(29, uStateMachine.getConfigurations().size());
    }
    catch (UModelException me) {
      fail("Could not parse model: " + me.getMessage());
    }
  }

  @Test//(timeout=2000)
  public void testFlattening() {
    String modelFileName = "test/uml/parser/ute/atm.ute";

    try {
      util.Message.verbosity = util.Message.INFO;
      util.Message.debug = true;
      control.Properties properties = control.Properties.getDefault().getCopy();
      properties.setPropertyByName("fixedOrderRegions", "true");
      properties.setPropertyByName("fixedOrderTransitionFiring", "true");
      properties.commit();
      control.Properties.setDefault(properties);

      BufferedInputStream modelInputStream = new BufferedInputStream(new FileInputStream(modelFileName));
      ModelReader modelReader = ModelReader.create(modelInputStream);
      assertEquals(modelReader.getModelName(), "ATM");

      UModel uModel = modelReader.getModel();
      UClass uClass = uModel.getClasses().stream().filter(uC -> uC.getName().equals("ATM")).findAny().orElse(null);
      assertFalse(uClass == null);
      UStateMachine uStateMachine = uClass.getStateMachine();
      assertFalse(uStateMachine == null);

      for (UConfiguration sourceUConfiguration : uStateMachine.getConfigurations()) {
        System.out.print("Source: ");
        System.out.println(sourceUConfiguration);
        for (UConflictFreeSet outgoingUConflictFreeSet : sourceUConfiguration.getOutgoingMaximalConflictFrees()) {
          System.out.println(outgoingUConflictFreeSet);
          UConfiguration targetUConfiguration = outgoingUConflictFreeSet.getTarget(sourceUConfiguration);
          System.out.print("Target: ");
          System.out.println(targetUConfiguration);
        }
      }
    }
    catch (PropertyException pe) {
      fail("Could not change properties: " + pe.getMessage());
    }
    catch (UModelException me) {
      fail("Could not parse model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read model: " + ioe.getMessage());
    }
  }
}
