package uml;

import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.UVertex;
import uml.statemachine.semantics.UCompletionTree;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de">knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class CompletionTreeTest {
  private UStateMachine stateMachine = null;
  private Set<UState> completionStates = new HashSet<>();

  @Before
  public void setUp() throws Exception {
    String modelString =
      "model test {" +
      "  class Test {" +
      "    behaviour {" +
      "      states {" +
      "        initial Initial;" +
      "        concurrent TBC_Concurrent1 {" +
      "          region Region1 {" +
      "            concurrent Concurrent1_1 {" +
      "              composite Region1_1 {" +
      "                simple TBC_Simple1_1_1;" +
      "                simple TBC_Simple1_1_2;" +
      "                final Final1_1;" +
      "                TBC_Simple1_1_1 -> TBC_Simple1_1_2;" +
      "                TBC_Simple1_1_2 -> Final1_1;" +
      "              }" +
      "              composite Region1_2 {" +
      "                simple TBC_Simple1_2_1;" +
      "                simple TBC_Simple1_2_2;" +
      "                final Final1_2;" +
      "                TBC_Simple1_2_1 -> TBC_Simple1_2_2;" +
      "                TBC_Simple1_2_2 -> Final1_2;" +
      "              }" +
      "            }" +
      "            final Final1;" +
      "          }" +     
      "          region Region2 {" +
      "            final Final2;" +
      "          }" +
      "        }" +
      "        final Final;" +
      "      }" +
      "      transitions {" +
      "        TBC_Concurrent1 -> Final;" +
      "      }" +
      "    }" +
      "  }" +
      "}";

    uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(new java.io.BufferedInputStream(new java.io.ByteArrayInputStream(modelString.getBytes())));
    UModel model = modelReader.getModel();
    this.stateMachine = model.getClasses().iterator().next().getStateMachine();
    for (UVertex uVertex : this.stateMachine.getVertices()) {
      if (uVertex instanceof UState && uVertex.getName().startsWith("TBC_", 0)) {
        this.completionStates.add((UState)uVertex);
      }
    }
  }

  @Test
  public final void testCreate() {
    @SuppressWarnings("null")
    Set<UState> actual = new HashSet<>(UCompletionTree.create(this.stateMachine).getCompletionStates());
    Set<UState> expected = this.completionStates;
    assertEquals(expected, actual);
  }
}
