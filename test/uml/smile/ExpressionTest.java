package uml.smile;

import static org.junit.Assert.*;

import org.junit.Test;

import uml.UAction;
import uml.UClass;
import uml.UExpression;
import uml.UModel;
import uml.smile.translation.SSymbolTable;
import uml.statemachine.UEvent;
import uml.statemachine.UStateMachine;
import util.Sets;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class ExpressionTest {
  @Test
  public void testExpression() {
    SGuard expected = SGuard.literal(SLiteral.neq(SVariable.state("S1", Sets.singleton(SValue.emptyValue())), SConstant.empty()));
    SGuard actual = SGuard.neg(SGuard.literal(SLiteral.eq(SVariable.state("S1", Sets.singleton(SValue.emptyValue())), SConstant.empty())));
    assertEquals(expected, actual);
  }

  /**
   * Test coding of expressions.
   */
  @Test
  public void testCoding() {
    var expected = "!x";
    var actual = SGuard.neg(SGuard.literal(SLiteral.isTrue(SVariable.flag("x")))).toString();
    assertEquals(expected, actual);

    expected = "x";
    actual = SGuard.neg(SGuard.neg(SGuard.literal(SLiteral.isTrue(SVariable.flag("x"))))).toString();
    assertEquals(expected, actual);

    expected = "x && y";
    actual = SGuard.and(SGuard.literal(SLiteral.isTrue(SVariable.flag("x"))),
                        SGuard.literal(SLiteral.isTrue(SVariable.flag("y")))).toString();
    assertEquals(expected, actual);

    expected = "!x || !y";
    actual = SGuard.neg(SGuard.and(SGuard.literal(SLiteral.isTrue(SVariable.flag("x"))),
                                   SGuard.literal(SLiteral.isTrue(SVariable.flag("y"))))).toString();
    assertEquals(expected, actual);

    expected = "!x || y";
    actual = SGuard.neg(SGuard.and(SGuard.literal(SLiteral.isTrue(SVariable.flag("x"))),
                                   SGuard.neg(SGuard.literal(SLiteral.isTrue(SVariable.flag("y")))))).toString();
    assertEquals(expected, actual);

    expected = "true";
    actual = SGuard.or(SGuard.literal(SLiteral.eq(SVariable.state("s", Sets.singleton(SValue.emptyValue())), SConstant.empty())),
                       SGuard.literal(SLiteral.neq(SVariable.state("s", Sets.singleton(SValue.emptyValue())), SConstant.empty()))).toString();
    assertEquals(expected, actual);

    expected = "x";
    actual = SGuard.and(SGuard.literal(SLiteral.isTrue(SVariable.flag("x"))),
                        SGuard.or(SGuard.literal(SLiteral.isTrue(SVariable.flag("x"))),
                                  SGuard.literal(SLiteral.isTrue(SVariable.flag("y"))))).toString();
    assertEquals(expected, actual);

    expected = "x && y || z";
    actual = SGuard.or(SGuard.and(SGuard.literal(SLiteral.isTrue(SVariable.flag("x"))),
                                  SGuard.literal(SLiteral.isTrue(SVariable.flag("y")))),
                       SGuard.literal(SLiteral.isTrue(SVariable.flag("z")))).toString();
    assertEquals(expected, actual);

    expected = "!y && x || x && z || y && z"; // (!y || z) && (x || y) <->
                                              // (!y && (x || y)) || (z && (x || y) <->
                                              // !y && x || (x && z || y && z)
    actual = SGuard.and(SGuard.or(SGuard.literal(SLiteral.isTrue(SVariable.flag("x"))),
                                  SGuard.literal(SLiteral.isTrue(SVariable.flag("y")))),
                         SGuard.or(SGuard.literal(SLiteral.isFalse(SVariable.flag("y"))),
                                   SGuard.literal(SLiteral.isTrue(SVariable.flag("z"))))).toString();
    assertEquals(expected, actual);

    expected = "!y && z || x && y";
    actual = SGuard.or(SGuard.and(SGuard.literal(SLiteral.isTrue(SVariable.flag("x"))),
                                  SGuard.literal(SLiteral.isTrue(SVariable.flag("y")))),
                       SGuard.and(SGuard.literal(SLiteral.isFalse(SVariable.flag("y"))),
                                  SGuard.literal(SLiteral.isTrue(SVariable.flag("z"))))).toString();
    assertEquals(expected, actual);
  }

  @Test
  public void testEvaluation() {
    var uModel = new UModel("test");
    var uClass = new UClass("Test", uModel);
    var uStateMachine = new UStateMachine(uClass);
    var uRegionA = uStateMachine.addRegion("A");
    var uRegionB = uStateMachine.addRegion("B");
    var uStateA1 = uRegionA.addState("A1");
    var uStateA2 = uRegionA.addState("A2");
    var uStateB1 = uRegionB.addState("B1");

    var sSymbolTable = new SSymbolTable(uStateMachine, uModel.getProperties());
    var sMachine = new SMachine(sSymbolTable, SCommand.skip(), SCommand.skip(), SGuard.trueConst());
    var sVariable1 = sMachine.getStateVariable(uStateA1);
    var sVariable2 = sMachine.getStateVariable(uStateB1);

    var sValueA1 = SValue.constant(SConstant.vertex(uStateA1));
    var sValueA2 = SValue.constant(SConstant.vertex(uStateA2));
    
    var sState = SState.initial(sMachine).withAssignment(sVariable1, Sets.elements(sValueA1, sValueA2));
    var sGuard = SGuard.and(SGuard.literal(SLiteral.neq(sVariable1, sValueA1)), SGuard.literal(SLiteral.neq(sVariable1, sValueA2)));

    var expected = Sets.singleton(SValue.falseValue());
    var actual = sGuard.evaluate(sState);
    assertEquals(expected, actual);

    sState = SState.initial(sMachine).withAssignment(sVariable1, Sets.elements(sValueA1, sValueA2, SValue.emptyValue())).withAssignment(sVariable2, SValue.emptyValue());
    sGuard = SGuard.or(SGuard.literal(SLiteral.neq(sVariable1, SValue.emptyValue())), SGuard.literal(SLiteral.neq(sVariable2, SValue.emptyValue())));

    expected = Sets.elements(SValue.falseValue(), SValue.trueValue());
    actual = sGuard.evaluate(sState);
    assertEquals(expected, actual);
  }

  @Test
  public void testReduction() {
    var uModel = new UModel("test");
    var uClass = new UClass("Test", uModel);
    var uStateMachine = uClass.setStateMachine();
    var uRegionA = uStateMachine.addRegion("A");
    var uStateA1 = uRegionA.addState("A1");
    var uStateA2 = uRegionA.addState("A2");
    var uStateA3 = uRegionA.addState("A3");
    uRegionA.addTransition(uStateA1, uStateA2, UEvent.completion(uStateA1), UExpression.trueConst(), UAction.skip());

    var sSymbolTable = new SSymbolTable(uStateMachine, uModel.getProperties());
    var sMachine = new SMachine(sSymbolTable, SCommand.skip(), SCommand.skip(), SGuard.trueConst());
    var sVariable1 = sMachine.getStateVariable(uStateA1);

    var sValueA1 = SValue.constant(SConstant.vertex(uStateA1));
    var sValueA2 = SValue.constant(SConstant.vertex(uStateA2));
    var sValueA3 = SValue.constant(SConstant.vertex(uStateA3));
    
    var sState = SState.initial(sMachine).withAssignment(sVariable1, Sets.elements(sValueA1, sValueA2, sValueA3));
    var sGuard = SGuard.or(SGuard.and(SGuard.literal(SLiteral.eq(sVariable1, sValueA1)),
                                      SGuard.literal(SLiteral.isCompleted(uStateA1))),
                           SGuard.literal(SLiteral.neq(sVariable1, sValueA1)));

    // uStateA1 is not completed in the initial abstract state
    var expected = SGuard.literal(SLiteral.neq(sVariable1, sValueA1));
    var actual = sGuard.reduce(sState);
    assertEquals(expected, actual);
    
    sGuard = SGuard.and(SGuard.or(SGuard.literal(SLiteral.eq(sVariable1, sValueA1)),
                                  SGuard.literal(SLiteral.eq(sVariable1, sValueA2))),
                        SGuard.or(SGuard.literal(SLiteral.eq(sVariable1, sValueA1)),
                                  SGuard.literal(SLiteral.eq(sVariable1, sValueA2))));
    expected = SGuard.literal(SLiteral.neq(sVariable1, sValueA3));
    actual = sGuard.reduce(sState);
    assertEquals(expected, actual);
  }
}
