package uml.smile;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.Before;
import org.junit.Test;

import uml.UAttribute;
import uml.UClass;
import uml.UContext;
import uml.UExpression;
import uml.UModel;
import uml.UOperator;
import util.Identifier;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class StatementTest {
  private UClass uClass;

  @Before
  public void setUp() {
    UModel uModel = new UModel("test");
    UClass uClass = new UClass("Test", uModel);
    UAttribute uAttribute = new UAttribute("x", uModel.getInteger(), uClass);
    uClass.addAttribute(uAttribute);
    this.uClass = uClass;
  }

  /**
   * Test choice with else-branch translation
   */
  @Test
  public void testChoiceWithElseBranch() {
    UClass uClass = this.uClass; assert (uClass != null);
    UContext uContext = UContext.c1ass(uClass);

    SGuard guard1 = SGuard.literal(SLiteral.external(UExpression.relational(UExpression.id(Identifier.id("x")), UOperator.EQ, UExpression.intConst(1)), uContext));
    SGuard guard2 = SGuard.literal(SLiteral.external(UExpression.relational(UExpression.id(Identifier.id("x")), UOperator.NEQ, UExpression.intConst(2)), uContext));
    SBranch branch1 = SBranch.branch(guard1, SPrimitive.success());
    SBranch branch2 = SBranch.branch(guard2, SPrimitive.fail());
    SStatement else1 = SCommand.skip();
    @NonNull List<@NonNull SBranch> branches = new ArrayList<>();
    branches.add(branch1);
    branches.add(branch2);
    SStatement choice = SBranching.choice(branches, else1);
    assertEquals(choice, choice.getSimplified());

    String expected =
      "if\n" +
      ":: eval(x == 1) ->\n" +
      "   success();\n" +
      ":: eval(x != 2) ->\n" +
      "   fail();\n" +
      ":: else ->\n" +
      "   ;\n" +
      "fi;";
    String actual = choice.declaration();
    assertEquals(expected, actual);
  }

  /**
   * Test choice without else-branch
   */
  @Test
  public void testChoiceWithoutElseBranchTranslation() {
    UClass uClass = this.uClass; assert (uClass != null);
    UContext uContext = UContext.c1ass(uClass);

    SGuard guard1 = SGuard.literal(SLiteral.external(UExpression.relational(UExpression.id(Identifier.id("y")), UOperator.EQ, UExpression.intConst(1)), uContext));
    SGuard guard2 = SGuard.literal(SLiteral.external(UExpression.relational(UExpression.id(Identifier.id("x")), UOperator.NEQ, UExpression.intConst(2)), uContext));
    SBranch branch1 = SBranch.branch(guard1, SCommand.skip());
    SBranch branch2 = SBranch.branch(guard2, SCommand.skip());
    @NonNull List<@NonNull SBranch> branches = new ArrayList<>();
    branches.add(branch1);
    branches.add(branch2);
    SStatement choice = SBranching.choice(branches);
    assertEquals(choice, choice.getSimplified());

    String expected =
      "if\n" +
      ":: eval(x != 2) || eval(y == 1) ->\n" +
      "   ;\n" +
      "fi;";
    String actual = choice.declaration();
    assertEquals(expected, actual);
  }

}
