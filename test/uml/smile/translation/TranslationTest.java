package uml.smile.translation;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.IOException;

import org.junit.Test;

import uml.UClass;
import uml.UModel;
import uml.UModelException;
import uml.parser.ModelReader;
import uml.statemachine.UStateMachine;
import uml.statemachine.semantics.UCompoundTransition;
import util.properties.PropertyException;

public class TranslationTest {
  @Test
  public void readerTest() {
    String modelFileName = "test/uml/parser/ute/atm.ute";

    try {
      // util.Message.debug = true;
      util.Message.verbosity = util.Message.INFO;
      util.Message.debug = true;
      control.Properties properties = control.Properties.getDefault().getCopy();
      properties.setPropertyByName("fixedOrderRegions", "true");
      properties.setPropertyByName("fixedOrderTransitionFiring", "true");
      properties.commit();
      control.Properties.setDefault(properties);

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      ModelReader modelReader = ModelReader.create(modelInputStream);
      assertEquals("ATM", modelReader.getModelName());

      /*
      UModel uModel = modelReader.getModel();
      util.Message.info(uModel.declaration());

      UClass uClass = uModel.getClasses().stream().filter(uC -> uC.getName().equals("ATM")).findAny().get();
      UStateMachine uStateMachine = uClass.getStateMachine();
      assertFalse(uStateMachine == null);
      for (UCompoundTransition uCompound : uStateMachine.getCompounds())
        util.Message.info(uCompound.toString());

      util.Message.info(uStateMachine.getMachine(properties).declaration());
      */
    }
    catch (PropertyException pe) {
      fail("Could not change properties: " + pe.getMessage());
    }
    catch (UModelException me) {
      fail("Could not parse model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read model: " + ioe.getMessage());
    }
  }

  @Test
  public void deferTest() {
    String modelFileName = "test/uml/smile/translation/test.ute";

    try {
      // util.Message.debug = true;
      util.Message.verbosity = util.Message.INFO;
      util.Message.debug = true;
      control.Properties properties = control.Properties.getDefault().getCopy();
      properties.setPropertyByName("fixedOrderRegions", "true");
      properties.setPropertyByName("fixedOrderTransitionFiring", "true");
      properties.commit();
      control.Properties.setDefault(properties);

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      ModelReader modelReader = ModelReader.create(modelInputStream);
      assertEquals("Test", modelReader.getModelName());

      UModel uModel = modelReader.getModel();
      util.Message.info(uModel.declaration());

      UClass uClass = uModel.getClasses().stream().filter(uC -> uC.getName().equals("Test")).findAny().get();
      UStateMachine uStateMachine = uClass.getStateMachine();
      assertFalse(uStateMachine == null);
      for (UCompoundTransition uCompound : uStateMachine.getCompounds())
        util.Message.info(uCompound.toString());

      util.Message.info(uStateMachine.getMachine(properties).getFullStatement().declaration());
      util.Message.info(uStateMachine.getMachine(properties).getStatement().declaration());
    }
    catch (PropertyException pe) {
      fail("Could not change properties: " + pe.getMessage());
    }
    catch (UModelException me) {
      fail("Could not parse model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read model: " + ioe.getMessage());
    }
  }
}
