package uml.smile.translation;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import uml.UAction;
import uml.UAttribute;
import uml.UClass;
import uml.UExpression;
import uml.UModel;
import uml.UModelException;
import uml.parser.ModelReader;
import uml.smile.SBranch;
import uml.smile.SBranching;
import uml.smile.SCommand;
import uml.smile.SConstant;
import uml.smile.SControl;
import uml.smile.SGuard;
import uml.smile.SLiteral;
import uml.smile.SMachine;
import uml.smile.SState;
import uml.smile.SStatement;
import uml.smile.SValue;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.UPseudoState;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import uml.statemachine.semantics.UCompoundTransition;
import util.Sets;
import util.properties.PropertyException;


public class PartialEvaluationTest {
  private UClass uClass;

  @Before
  public void setUp() {
    UModel uModel = new UModel("test");
    UClass uClass = new UClass("Test", uModel);
    UAttribute uAttribute = new UAttribute("x", uModel.getInteger(), uClass);
    uClass.addAttribute(uAttribute);

    UStateMachine uStateMachine = uClass.setStateMachine();
    URegion uRegionA = uStateMachine.addRegion("A");
    UState uStateA1 = uRegionA.addState("A1");
    UState uStateA2 = uRegionA.addState("A2");
    UPseudoState uInitial = uRegionA.addPseudoState("I", UPseudoState.Kind.INITIAL);
    uRegionA.addTransition(uInitial, uStateA1, UEvent.pseudo(), UExpression.trueConst(), UAction.skip());
    uRegionA.addTransition(uStateA1, uStateA2, UEvent.completion(uStateA1), UExpression.trueConst(), UAction.skip());

    this.uClass = uClass;
  }

  @Test
  public void testStuckChoice() {
    UClass uClass = this.uClass; assert (uClass != null);
    UStateMachine uStateMachine = uClass.getStateMachine(); assert (uStateMachine != null);
    SMachine machine = uStateMachine.getMachine(control.Properties.getDefault());
    SStatement choice = SBranching.choice(Sets.singleton(SBranch.branch(SGuard.falseConst(), SCommand.skip())));

    SStatement expected = SCommand.await(SGuard.falseConst());
    SStatement actual = choice.partiallyEvaluate(SState.initial(machine));
    assertEquals(expected, actual);
  }

  @Test
  public void testStuckLoop() {
    UClass uClass = this.uClass; assert (uClass != null);
    UStateMachine uStateMachine = uClass.getStateMachine(); assert (uStateMachine != null);
    SMachine machine = uStateMachine.getMachine(control.Properties.getDefault());
    SStatement loop = SBranching.loop(Sets.singleton(SBranch.branch(SGuard.falseConst(), SCommand.skip())));

    UEvent uEvent = uStateMachine.getEvents().iterator().next(); assert (uEvent != null);
    SStatement expected = SCommand.await(SGuard.falseConst());
    SStatement actual = loop.partiallyEvaluate(SState.initial(machine).withEvent(uEvent));
    assertEquals(expected, actual);
  }

 @Test
  public void testFiniteLoop() {
    UClass uClass = this.uClass; assert (uClass != null);
    UStateMachine uStateMachine = uClass.getStateMachine(); assert (uStateMachine != null);
    SMachine machine = uStateMachine.getMachine(control.Properties.getDefault());
    SStatement loop = SBranching.loop(Sets.singleton(SBranch.branch(SGuard.trueConst(), SCommand.breakStm())));

    SStatement expected = SCommand.skip();
    SStatement actual = loop.partiallyEvaluate(SState.intermediate(machine));
    assertEquals(expected, actual);
  }

  @Test
  public void testInfiniteLoop() {
    UClass uClass = this.uClass; assert (uClass != null);
    UStateMachine uStateMachine = uClass.getStateMachine(); assert (uStateMachine != null);
    UCompoundTransition uTransition = uStateMachine.getCompounds().iterator().next(); assert (uTransition != null);
    SMachine machine = uStateMachine.getMachine(control.Properties.getDefault());
    SVariable variable = machine.getVariables().stream().filter(v -> v.getName().equals("A_transition")).findAny().orElse(null); assert(variable != null);
    SStatement loop = SBranching.loop(Sets.singleton(SBranch.branch(SGuard.literal(SLiteral.neq(variable, SConstant.empty())), SCommand.assign(variable, SConstant.empty()))), SCommand.assign(variable, SConstant.transition(uTransition)));
    UEvent uEvent = uStateMachine.getEvents().iterator().next(); assert (uEvent != null);

    SStatement expected = SBranching.loop(Sets.elements(SBranch.branch(SGuard.literal(SLiteral.eq(variable, SConstant.empty())), SCommand.assign(variable, SConstant.transition(uTransition))), SBranch.branch(SGuard.literal(SLiteral.eq(variable, SConstant.transition(uTransition))), SCommand.assign(variable, SConstant.empty()))));
    SStatement actual = loop.partiallyEvaluate(SState.intermediate(machine).withEvent(uEvent));
    assertEquals(expected, actual);
  }

  @Test//(timeout=2000)
  public void partialEvaluation() {
    // String modelFileName = "test/uml/smile/translation/test.ute";
    // String modelFileName = "test/uml/parser/ute/atm.ute";
    String modelFileName = "models/atm/atm.uml";

    try {
      // util.Message.debug = true;
      util.Message.verbosity = util.Message.INFO;
      util.Message.debug = true;
      control.Properties properties = control.Properties.getDefault().getCopy();
      properties.setPropertyByName("fixedOrderRegions", "true");
      properties.setPropertyByName("fixedOrderTransitionFiring", "true");
      properties.commit();
      control.Properties.setDefault(properties);

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      ModelReader modelReader = ModelReader.create(modelInputStream);
      assertEquals(modelReader.getModelName(), "atm");

      UModel uModel = modelReader.getModel();
      UClass uClass = uModel.getClasses().stream().filter(uC -> uC.getName().equals("ATM")).findAny().orElse(null);
      assertFalse(uClass == null);
      UStateMachine uStateMachine = uClass.getStateMachine();
      assertFalse(uStateMachine == null);

      SMachine sMachine = uml.smile.translation.STranslator.translateStateMachine(uStateMachine, properties);
      for (var uEvent : uStateMachine.getEvents()) {
        var reaction = sMachine.getReaction(uEvent);
        var suspenders = reaction.getPrimitives().stream().filter(p -> p.suspended() != null).collect(java.util.stream.Collectors.toSet());
        if (suspenders.isEmpty())
          continue;

        util.Message.info(util.Formatter.set(suspenders));
        var suspendFlag = sMachine.getSymbolTable().getSuspendFlag();
        var breakFlag = sMachine.getSymbolTable().getBreakFlag();
        var suspendedReaction = reaction.getCycle(suspendFlag);
        if (suspendedReaction != reaction)
          suspendedReaction = SControl.seq(suspendedReaction, SCommand.assign(suspendFlag, SValue.falseValue()));
        suspendedReaction = suspendedReaction.simplify(SState.intermediate(sMachine).withEvent(uEvent).withAssignment(suspendFlag, SValue.falseValue()));
        util.Message.info(suspendedReaction.declaration());
        for (var suspender : suspenders) {
          var resumption = reaction.getResumption(suspender, sMachine.getSymbolTable().getBreakFlag());
          if (resumption == null)
            continue;

          resumption = resumption.simplify(SState.intermediate(sMachine).withEvent(uEvent).withAssignment(breakFlag, SValue.falseValue()));
          var suspendedResumption = resumption.getCycle(suspendFlag);
          if (suspendedResumption != resumption)
            suspendedResumption = SControl.seq(suspendedResumption, SCommand.assign(suspendFlag, SValue.falseValue()));
          suspendedResumption = suspendedResumption.simplify(SState.intermediate(sMachine).withEvent(uEvent).withAssignment(suspendFlag, SValue.falseValue()));
          util.Message.info(suspendedResumption.declaration());
        }
      }

      /*
      UState uState = uStateMachine.getCompletionStates().stream().filter(uS -> uS.getName().equals("CardEntry")).findAny().orElse(null);
      assertFalse(uState == null);
      util.Message.info("Unsimplified\n" + sMachine.getMain());
      var simplified = sMachine.getMain().simplify(SState.intermediate(sMachine).withSomeEvent().withSomeState());
      util.Message.info("Simplified\n" + simplified);
      util.Message.info("Simplified with assignment\n" + sMachine.getMain().simplify(SState.intermediate(sMachine).withSomeEvent().withAssignment(sMachine.getStateVariable(uState), SValue.constant(SConstant.vertex(uState)))));
      */

      /*
      UEvent uEvent = UEvent.completion(uState);
      SState sState = SState.intermediate(sMachine).withEvent(uEvent);
      util.Message.info("Pre state\n" + sState);
      SStatement evaluatedSStatement = sMachine.getReaction(uEvent);
      util.Message.info("Partially evaluated Smile statement for event " + uEvent.getName() + "\n" + evaluatedSStatement.declaration());
      util.Message.info("Post state\n" + evaluatedSStatement.execute(sState));
      */
    }
    catch (PropertyException pe) {
      fail("Could not change properties: " + pe.getMessage());
    }
    catch (UModelException me) {
      fail("Could not parse model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read model: " + ioe.getMessage());
    }
  }
}
