package uml.smile.translation;

import static org.junit.Assert.*;

import org.junit.Test;

import uml.UAction;
import uml.UClass;
import uml.UExpression;
import uml.UModel;
import uml.smile.SCommand;
import uml.smile.SConstant;
import uml.smile.SGuard;
import uml.smile.SLiteral;
import uml.smile.SMachine;
import uml.smile.SState;
import uml.smile.SValue;
import uml.smile.SVariable;
import uml.statemachine.UEvent;
import uml.statemachine.URegion;
import uml.statemachine.UState;
import uml.statemachine.UStateMachine;
import util.Sets;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class ExpressionTest {
  @Test
  public void testReduction() {
    UModel uModel = new UModel("test");
    UClass uClass = new UClass("Test", uModel);
    UStateMachine uStateMachine = new UStateMachine(uClass);
    URegion uRegionA = uStateMachine.addRegion("A");
    UState uStateA1 = uRegionA.addState("A1");
    UState uStateA2 = uRegionA.addState("A2");
    uRegionA.addTransition(uStateA1, uStateA2, UEvent.completion(uStateA1), UExpression.trueConst(), UAction.skip());

    var sSymbolTable = new SSymbolTable(uStateMachine, uModel.getProperties());
    SVariable sVariable1 = sSymbolTable.getVertexVariable(uStateA1);
    SValue sValueA1 = SValue.constant(SConstant.vertex(uStateA1));
    SValue sValueA2 = SValue.constant(SConstant.vertex(uStateA2));
    SGuard sGuard = SGuard.or(SGuard.and(SGuard.literal(SLiteral.eq(sVariable1, sValueA1)),
                                         SGuard.literal(SLiteral.neg(SLiteral.isCompleted(uStateA1)))),
                              SGuard.literal(SLiteral.neq(sVariable1, sValueA2)));
    System.out.println(sGuard);

    var sMachine = new SMachine(sSymbolTable, SCommand.skip(), SCommand.skip(), sGuard);
    SState sState = SState.initial(sMachine).withAssignment(sVariable1, Sets.elements(sValueA1, sValueA2));

    SGuard expected = SGuard.literal(SLiteral.eq(sVariable1, sValueA1));
    SGuard actual = sGuard.reduce(sState);
    assertEquals(expected, actual);
  }
}
