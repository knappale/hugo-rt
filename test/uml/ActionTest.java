package uml;

import static org.junit.Assert.*;

import org.junit.Test;

import util.Identifier;


/**
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class ActionTest {
  /**
   * Test coding of actions.
   */
  @Test
  public void testCoding() {
    String expected, actual;
    
    expected = "if (!x) x = true; else x = false;";
    actual = UAction.conditional(UExpression.neg(UExpression.id(Identifier.id("x"))),
                                UAction.assign(Identifier.id("x"), UExpression.trueConst()),
                                UAction.assign(Identifier.id("x"), UExpression.falseConst())).toString();
    assertEquals(expected, actual);

    expected = "if (!x) { x = true; y = false; } else x = false;";
    actual = UAction.conditional(UExpression.neg(UExpression.id(Identifier.id("x"))),
                                UAction.seq(UAction.assign(Identifier.id("x"), UExpression.trueConst()),
                                           UAction.assign(Identifier.id("y"), UExpression.falseConst())),
                                UAction.assign(Identifier.id("x"), UExpression.falseConst())).toString();
    assertEquals(expected, actual);
    
    expected = "if (!x) x = true; else { x = false; y = true; }";
    actual = UAction.conditional(UExpression.neg(UExpression.id(Identifier.id("x"))),
                                UAction.assign(Identifier.id("x"), UExpression.trueConst()),
                                UAction.seq(UAction.assign(Identifier.id("x"), UExpression.falseConst()),
                                           UAction.assign(Identifier.id("y"), UExpression.trueConst()))).toString();
    assertEquals(expected, actual);
    
    expected = "if (!x) { x = true; y = false; } else { x = false; y = true; }";
    actual = UAction.conditional(UExpression.neg(UExpression.id(Identifier.id("x"))),
                                UAction.seq(UAction.assign(Identifier.id("x"), UExpression.trueConst()),
                                           UAction.assign(Identifier.id("y"), UExpression.falseConst())),
                                UAction.seq(UAction.assign(Identifier.id("x"), UExpression.falseConst()),
                                           UAction.assign(Identifier.id("y"), UExpression.trueConst()))).toString();
    assertEquals(expected, actual);
  }
}
