package uml;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import util.Identifier;

import static util.Objects.requireNonNull;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class ExpressionTest {
  private UModel model;

  @Before
  public void setUp() throws Exception {
    String modelString =
      "model test {" +
      "  class Test {" + 
      "    signature {" +
      "      static const a : int = 3;" +
      "      static const b : int[3] = { 1, 2, 3 };" +
      "      attr c : int = 1;" +
      "      static attr d : int[2] = { 2, 1 };" +
      "    }" +
      "  }" +
      "}";

    uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(new java.io.BufferedInputStream(new java.io.ByteArrayInputStream(modelString.getBytes())));
    this.model = modelReader.getModel();
  }

  /**
   * Test evaluation of expressions.
   */
  @Test
  public void testEvaluation() {
    UModel model = requireNonNull(this.model);
    UContext context = UContext.model(model);
    
    assertTrue(UExpression.minus(UExpression.intConst(1)).getIntegerValue(context) == -1);
    assertTrue(UExpression.minus(UExpression.intConst(-1)).getIntegerValue(context) == 1);
    assertTrue(UExpression.minus(UExpression.arithmetical(UExpression.intConst(1),
                                                          UOperator.ADD,
                                                          UExpression.intConst(2))).getIntegerValue(context) == -3);
    assertTrue(UExpression.arithmetical(UExpression.intConst(3),
                                        UOperator.MULT,
                                        UExpression.arithmetical(UExpression.intConst(2),
                                                                 UOperator.ADD,
                                                                 UExpression.intConst(1))).getIntegerValue(context) == 9);

    assertTrue(UExpression.id(Identifier.id(Identifier.id("Test"), "a")).getIntegerValue(context) == 3);
    assertTrue(UExpression.array(Identifier.id(Identifier.id("Test"), "b"),
                                 UExpression.intConst(1)).getIntegerValue(context) == 2);
    try {
      assertFalse(UExpression.id(Identifier.id(Identifier.id("Test"), "c")).getIntegerValue(context) == 1);
      assertFalse(UExpression.array(Identifier.id(Identifier.id("Test"), "d"),
                                    UExpression.intConst(1)).getIntegerValue(context) == 1);
    }
    catch (IllegalStateException ise) {
    }
  }

  /**
   * Test coding of expressions.
   */
  @Test
  public void testCoding() {
    String expected, actual;

    expected = "-1";
    actual = UExpression.intConst(-1).toString();
    assertEquals(expected, actual);

    expected = "-1";
    actual = UExpression.minus(UExpression.intConst(1)).toString();
    assertEquals(expected, actual);

    expected = "1";
    actual = UExpression.minus(UExpression.minus(UExpression.intConst(1))).toString();
    assertEquals(expected, actual);

    expected = "!x";
    actual = UExpression.neg(UExpression.id(Identifier.id("x"))).toString();
    assertEquals(expected, actual);

    expected = "x";
    actual = UExpression.neg(UExpression.neg(UExpression.id(Identifier.id("x")))).toString();
    assertEquals(expected, actual);

    expected = "!x || !y";
    actual = UExpression.neg(UExpression.and(UExpression.id(Identifier.id("x")),
                                           UExpression.id(Identifier.id("y")))).toString();
    assertEquals(expected, actual);

    expected = "!x || y";
    actual = UExpression.neg(UExpression.and(UExpression.id(Identifier.id("x")),
                                           UExpression.neg(UExpression.id(Identifier.id("y"))))).toString();
    assertEquals(expected, actual);

    expected = "x < y || x > y";
    actual = UExpression.or(UExpression.relational(UExpression.id(Identifier.id("x")), UOperator.LT, UExpression.id(Identifier.id("y"))),
                           UExpression.relational(UExpression.id(Identifier.id("x")), UOperator.GT, UExpression.id(Identifier.id("y")))).toString();
    assertEquals(expected, actual);

    expected = "x < y+1";
    actual = UExpression.relational(UExpression.id(Identifier.id("x")),
                                   UOperator.LT,
                                   UExpression.arithmetical(UExpression.id(Identifier.id("y")),
                                                           UOperator.ADD,
                                                           UExpression.intConst(1))).toString();
    assertEquals(expected, actual);

    expected = "-(1+2)";
    actual = UExpression.minus(UExpression.arithmetical(UExpression.intConst(1),
                                                      UOperator.ADD,
                                                      UExpression.intConst(2))).toString();
    assertEquals(expected, actual);

    expected = "1+2+3";
    actual = UExpression.arithmetical(UExpression.intConst(1),
                                     UOperator.ADD,
                                     UExpression.arithmetical(UExpression.intConst(2),
                                                             UOperator.ADD,
                                                             UExpression.intConst(3))).toString();
    assertEquals(expected, actual);

    expected = "(1-2)-3";
    actual = UExpression.arithmetical(UExpression.arithmetical(UExpression.intConst(1),
                                                             UOperator.SUB,
                                                             UExpression.intConst(2)),
                                     UOperator.SUB,
                                     UExpression.intConst(3)).toString();
    assertEquals(expected, actual);

    expected = "1-(2-3)";
    actual = UExpression.arithmetical(UExpression.intConst(1),
                                     UOperator.SUB,
                                     UExpression.arithmetical(UExpression.intConst(2),
                                                             UOperator.SUB,
                                                             UExpression.intConst(3))).toString();
    assertEquals(expected, actual);

    expected = "1-(2+3)";
    actual = UExpression.arithmetical(UExpression.intConst(1),
                                     UOperator.SUB,
                                     UExpression.arithmetical(UExpression.intConst(2),
                                                             UOperator.ADD,
                                                             UExpression.intConst(3))).toString();
    assertEquals(expected, actual);

    expected = "3*(2+1)";
    actual = UExpression.arithmetical(UExpression.intConst(3),
                                     UOperator.MULT,
                                     UExpression.arithmetical(UExpression.intConst(2),
                                                             UOperator.ADD,
                                                             UExpression.intConst(1))).toString();
    assertEquals(expected, actual);

    expected = "1+2*3";
    actual = UExpression.arithmetical(UExpression.intConst(1),
                                     UOperator.ADD,
                                     UExpression.arithmetical(UExpression.intConst(2),
                                                             UOperator.MULT,
                                                             UExpression.intConst(3))).toString();
    assertEquals(expected, actual);

    expected = "x && (x || y)";
    actual = UExpression.and(UExpression.id(Identifier.id("x")),
                             UExpression.or(UExpression.id(Identifier.id("x")),
                                            UExpression.id(Identifier.id("y")))).toString();
    assertEquals(expected, actual);

    expected = "(x && y) || x";
    actual = UExpression.or(UExpression.and(UExpression.id(Identifier.id("x")),
                                            UExpression.id(Identifier.id("y"))),
                            UExpression.id(Identifier.id("x"))).toString();
    assertEquals(expected, actual);
  }

  /**
   * Test disjunctive normal form.
   */
  @Test
  public void testDisjunctiveNormalForm() {
    String expected, actual;
    
    expected = "x || (x && y)";
    actual = UExpression.and(UExpression.id(Identifier.id("x")),
                             UExpression.or(UExpression.id(Identifier.id("x")),
                                            UExpression.id(Identifier.id("y")))).getDisjunctiveNormalForm().toString();
    assertEquals(expected, actual);

    expected = "(x && y) || x";
    actual = UExpression.or(UExpression.and(UExpression.id(Identifier.id("x")),
                                            UExpression.id(Identifier.id("y"))),
                            UExpression.id(Identifier.id("x"))).getDisjunctiveNormalForm().toString();
    assertEquals(expected, actual);
  }

  /**
   * Test equality on expressions.
   */
  @Test
  public void testEquals() {
    assertFalse(UExpression.intConst(1).equals(UExpression.intConst(2)));
    assertTrue(UExpression.intConst(1).equals(UExpression.intConst(1)));
    assertTrue(UExpression.trueConst() == UExpression.trueConst());
    assertTrue(UExpression.trueConst() == UExpression.or(UExpression.falseConst(), UExpression.trueConst()));
    assertTrue(UExpression.falseConst() == UExpression.and(UExpression.falseConst(), UExpression.trueConst()));
  }
}
