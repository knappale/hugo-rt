package promela;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class PMessageTypeTest {
  private PMessageType a;

  @Before
  public void setUp() {
    a = PMessageType.create().addPartChannel();
  }

  /**
   * Test equals().
   */
  @Test
  public void testEquals() {
    assertTrue(a.equals(a));
    assertTrue(a.equals(PMessageType.create().addPartChannel()));
    assertFalse(a.equals(PMessageType.create().addPartDataType(PDataType.bitType())));
  }
}

