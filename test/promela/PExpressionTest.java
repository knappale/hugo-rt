package promela;

import static org.junit.Assert.*;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.Test;


public class PExpressionTest {
  private @NonNull PConstant zero = PConstant.numeric(0);
  private @NonNull PConstant arraySize = PConstant.numeric(2);
  private @NonNull PDataType integer = PDataType.intType();
  private @NonNull PMessageType messageType = PMessageType.create().addPartDataType(integer);
  private @NonNull PExpression trueConstExp = PExpression.trueConst();
  private @NonNull PExpression falseConstExp = PExpression.falseConst();
  private @NonNull PExpression constantExp = PExpression.constant(zero);
  private @NonNull PExpression variableExp = PExpression.variable(PVariable.array("v", integer, arraySize).initialise(PExpression.constant(zero)));
  private @NonNull PExpression channelExp = PExpression.channel(PChannel.array("c", arraySize).initialise(zero, messageType));
  private @NonNull PExpression macroExp = PExpression.macro(PMacro.expression("zero", constantExp));
  private @NonNull PExpression conditionalExp = PExpression.conditional(constantExp, constantExp, constantExp);
  private @NonNull PExpression[] functionExps = { PExpression.len(trueConstExp),
                                                  PExpression.empty(trueConstExp),
                                                  PExpression.nempty(trueConstExp),
                                                  PExpression.nfull(trueConstExp),
                                                  PExpression.full(trueConstExp),
                                                  PExpression.eval(trueConstExp),
                                                  PExpression.enabled(trueConstExp),
                                                  PExpression.pc_value(trueConstExp) };
  private @NonNull PExpression bracketExp = PExpression.bracket(trueConstExp, trueConstExp);
  private @NonNull PExpression dotExp = PExpression.dot(trueConstExp, trueConstExp);
  private @NonNull PExpression[] runExps = { PExpression.run("p"),
                                             PExpression.run("p").addRunArg(trueConstExp),
                                             PExpression.run("p").addRunArg(trueConstExp).addRunArg(trueConstExp) };
  private @NonNull PExpression[] pollNoSideEffectExps = { PExpression.pollNoSideEffect(trueConstExp).addPollNoSideEffectArg(trueConstExp),
                                                          PExpression.pollNoSideEffect(trueConstExp).addPollNoSideEffectArg(trueConstExp).addPollNoSideEffectArg(trueConstExp) };
  private @NonNull PExpression[] randomPollNoSideEffectExps = { PExpression.randomPollNoSideEffect(trueConstExp).addPollNoSideEffectArg(trueConstExp),
                                                                PExpression.randomPollNoSideEffect(trueConstExp).addPollNoSideEffectArg(trueConstExp).addPollNoSideEffectArg(trueConstExp) };
  private @NonNull PExpression[] noChildExpOp_unaryExps = { PExpression.increment(conditionalExp),
                                                            PExpression.decrement(conditionalExp),
                                                            PExpression.unaryMinus(conditionalExp),
                                                            PExpression.bitComplement(conditionalExp),
                                                            PExpression.not(conditionalExp) };
  private @NonNull PExpression lowerChildOpPrior_unaryExp = PExpression.unaryMinus(PExpression.run("p"));
  private @NonNull PExpression[] leftNoChildExpOp_rightNoChildExpOp_binaryExps = { PExpression.binary(conditionalExp, POperator.MULT, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.DIV, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.MOD, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.PLUS, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.MINUS, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.SHL, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.SSHR, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.LT, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.LE, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.GT, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.GE, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.EQ, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.NEQ, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.AND, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.OR, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.XOR, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.CAND, conditionalExp),
                                                                                   PExpression.binary(conditionalExp, POperator.COR, conditionalExp) };
  private @NonNull PExpression leftLowerChildOpPrior_rightGreaterChildOpPrior_binaryExp = PExpression.binary(PExpression.run("p"), POperator.MULT, constantExp);
  private @NonNull PExpression leftEqualChildOpPriorNotEqualOp_rightEqualChildOpPriorEqualOp_binaryExp = PExpression.binary(PExpression.binary(constantExp, POperator.DIV, constantExp), POperator.MULT, PExpression.binary(constantExp, POperator.MULT, constantExp));
  private @NonNull PExpression[] expressions = { trueConstExp,
                                                 falseConstExp,
                                                 constantExp,
                                                 variableExp,
                                                 channelExp,
                                                 macroExp,
                                                 conditionalExp,
                                                 functionExps[0],
                                                 bracketExp,
                                                 dotExp,
                                                 runExps[1],
                                                 pollNoSideEffectExps[0],
                                                 randomPollNoSideEffectExps[0],
                                                 noChildExpOp_unaryExps[0],
                                                 leftNoChildExpOp_rightNoChildExpOp_binaryExps[0] };

  /**
   * Test coding of expression which represents the boolean constant
   * <CODE>true</CODE>.
   */
  @Test
  public void testTrueConstExpCode() {
    String expected = "true";
    String actual = trueConstExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of expression which represents the boolean constant
   * <CODE>false</CODE>.
   */
  @Test
  public void testFalseConstExpCode() {
    String expected = "false";
    String actual = falseConstExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of expression which represents access to constant.
   */
  @Test
  public void testConstantExpCode() {
    String expected = "0";
    String actual = constantExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of expression which represents access to variable.
   */
  @Test
  public void testVariableExpCode() {
    String expected = "v";
    String actual = variableExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of expression which represents access to channel.
   */
  @Test
  public void testChannelExpCode() {
    String expected = "c";
    String actual = channelExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of expression which represents macro.
   */
  @Test
  public void testMacroExpCode() {
    String expected = "zero";
    String actual = macroExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of expression which represents conditional.
   */
  @Test
  public void testConditionalExpCode() {
    String expected = "(0 -> 0 : 0)";
    String actual = conditionalExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of one expression which represents function
   * for each kind of function.
   */
  @Test
  public void testFunctionExpCode_allFunctions() {
    String[] s = { "len", "empty", "nempty", "nfull", "full", "eval", "enabled", "pc_value" };
    for (int i = 0; i < functionExps.length; i++) {
      String expected = s[i] + "(true)";
      String actual = functionExps[i].code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of expression which represents access to array.
   */
  @Test
  public void testBracketExpCode() {
    String expected = "true[true]";
    String actual = bracketExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of expression which represents access to structure.
   */
  @Test
  public void testDotExpCode() {
    String expected = "true.true";
    String actual = dotExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of expression which represents run-operation.
   */
  @Test
  public void testRunExpCode() {
    String[] s = { "p()", "p(true)", "p(true, true)" };
    String expected;
    String actual;
    for (int i = 0; i < runExps.length; i++) {
      expected = "run " + s[i];
      actual = runExps[i].code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of expression which represents poll-access to channel
   * without side-effect.
   */
  @Test
  public void testPollNoSideEffectExpCode() {
    String[] s = { "[true]", "[true,true]" };
    String expected;
    String actual;
    for (int i = 0; i < pollNoSideEffectExps.length; i++) {
      expected = "true?" + s[i];
      actual = pollNoSideEffectExps[i].code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of expression which represents random poll-access to
   * channel without side-effect.
   */
  @Test
  public void testRandomPollNoSideEffectExpCode() {
    String[] s = { "[true]", "[true,true]" };
    String expected;
    String actual;
    for (int i = 0; i < randomPollNoSideEffectExps.length; i++) {
      expected = "true??" + s[i];
      actual = randomPollNoSideEffectExps[i].code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of one expression which represents unary operation
   * for each kind of unary operation except parentheses,
   * having a child expression which has no operator.
   */
  @Test
  public void testUnaryExpCode_noOpChildExp() {
    String[] s = { "(0 -> 0 : 0)++", "(0 -> 0 : 0)--", "-(0 -> 0 : 0)", "~(0 -> 0 : 0)", "!(0 -> 0 : 0)" };
    for (int i = 0; i < noChildExpOp_unaryExps.length; i++) {
      String expected = s[i];
      String actual = noChildExpOp_unaryExps[i].code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of expression which represents unary operation
   * having a child expression whose operator priority is lower.
   */
  @Test
  public void testUnaryExpCode_lowerChildExpPrior() {
    String expected = "-(run p())";
    String actual = lowerChildOpPrior_unaryExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of one expression which represents binary operation
   * for each kind of binary operation having a left and a right child
   * expression which have both no operator.
   */
  @Test
  public void testBinaryExpCode_leftNoOpChildExp_rightNoOpChildExp() {
    String s[] = { "*", "/", "%", "+", "-", " << ", " >> ", " < ", " <= ", " > ", " >= ", " == ", " != ", "&", "|", "^", " && ", " || " };
    for (int i = 0; i < leftNoChildExpOp_rightNoChildExpOp_binaryExps.length; i++) {
      String expected = "(0 -> 0 : 0)" + s[i] + "(0 -> 0 : 0)";
      String actual = leftNoChildExpOp_rightNoChildExpOp_binaryExps[i].code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of expression which represents binary operation
   * having a left child expression whose operator priority is lower
   * and a right child expression whose operator priority is greater.
   */
  @Test
  public void testBinaryExpCode_leftLowerChildOpPrior_rightGreaterChildOpPrior() {
    String expected = "(run p())*0";
    String actual = leftLowerChildOpPrior_rightGreaterChildOpPrior_binaryExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of expression which represents binary operation
   * having a left child expression whose operator priority is equal
   * and whose operator is not equal and a right child expression
   * whose operator priority is equal and whose operator is equal.
   */
  @Test
  public void testBinaryExpCode_leftEqualChildOpPriorNotEqualOp_rightEqualChildOpPriorEqualOp() {
    String expected = "(0/0)*0*0";
    String actual = leftEqualChildOpPriorNotEqualOp_rightEqualChildOpPriorEqualOp_binaryExp.code();
    assertEquals(expected, actual);
  }

  /**
   * Test equals().
   */
  @Test
  public void testEquals() {
    for (int i = 0; i < expressions.length; i++) {
      assertTrue(expressions[i].equals(expressions[i]));
      for (int j = i + 1; j < expressions.length; j++) {
        assertFalse(expressions[i].equals(expressions[j]));
      }
    }
  }
}
