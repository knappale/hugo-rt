package promela;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class PConstantTest {
  private PConstant a;

  @Before
  public void setUp() {
    a = PConstant.numeric(0);
  }

  @Test
  public void equals() {
    assertTrue(a.equals(a));
    assertTrue(a.equals(PConstant.numeric(0)));
    assertFalse(a.equals(PConstant.trueConst()));
  }
}
