package promela;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class PSystemTest {
  private PSystem system;

  @Before
  public void setUp() {
    system = PSystem.create().addProcess(promela.PProcess.init(PStatement.skip()));
  }

  /**
   * Test system.
   */
  @Test
  public void testSystem() {
    String expected;
    String actual;

    expected = "";
    expected += "init {\n";
    expected += "  skip\n";
    expected += "}\n";
    actual = system.code();
    assertEquals(expected, actual);
  }

  /**
   * Test code with indentation of all lines.
   */
  @Test
  public void testLineIndentation() {
    String expected;
    String actual;
    String prefix = "  ";

    expected = "";
    expected += "  init {\n";
    expected += "    skip\n";
    expected += "  }\n";
    actual = system.code(prefix);
    assertEquals(expected, actual);
  }
}
