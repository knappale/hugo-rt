package promela;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


public class PProcessTest {
  private PProcess noParam_proctypePro;
  private PProcess oneParam_proctypePro;
  private PProcess twoParam_proctypePro;
  private PProcess initPro;
  private PProcess neverPro;
  private PProcess[] processes;

  @Before
  public void setUp() {
    PVariable v = PVariable.simple("v", PDataType.bitType());
    PChannel c = PChannel.simple("c");
    PStatement labeledStm = PStatement.label("l", PStatement.skip());
    PStatement skipStm = PStatement.skip();
    noParam_proctypePro = PProcess.proctype("p", new ArrayList<>(), new ArrayList<>(), labeledStm);
    oneParam_proctypePro = PProcess.proctype("p", new ArrayList<>(), new ArrayList<>(), skipStm).addParam(v);
    twoParam_proctypePro = PProcess.proctype("p", new ArrayList<>(), new ArrayList<>(), skipStm).addParam(v).addParam(c);
    initPro = PProcess.init(labeledStm);
    neverPro = PProcess.never(labeledStm);
    processes = new PProcess[5];
    processes[0] = noParam_proctypePro;
    processes[1] = oneParam_proctypePro;
    processes[2] = twoParam_proctypePro;
    processes[3] = initPro;
    processes[4] = neverPro;
  }

  /**
   * Test coding of user-defined processes.
   */
  @Test
  public void testProctypeProDec() {
    String expected;
    String actual;

    {
      expected = "";
      expected += "proctype p() {\n";
      expected += "  l:\n";
      expected += "  skip\n";
      expected += "}";
      actual = noParam_proctypePro.declaration();
      assertEquals(expected, actual);
    }

    {
      expected = "";
      expected += "proctype p(bit v) {\n";
      expected += "  skip\n";
      expected += "}";
      actual = oneParam_proctypePro.declaration();
      assertEquals(expected, actual);
    }

    {
      expected = "";
      expected += "proctype p(bit v; chan c) {\n";
      expected += "  skip\n";
      expected += "}";
      actual = twoParam_proctypePro.declaration();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of initial process.
   */
  @Test
  public void testInitProDec() {
    String expected;
    String actual;

    {
      expected = "";
      expected += "init {\n";
      expected += "  l:\n";
      expected += "  skip\n";
      expected += "}";
      actual = initPro.declaration();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of never-claims.
   */
  @Test
  public void testNeverProDec() {
    String expected;
    String actual;

    {
      expected = "";
      expected += "never {\n";
      expected += "  l:\n";
      expected += "  skip\n";
      expected += "}";
      actual = neverPro.declaration();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test declaration with indentation of inner lines.
   */
  @Test
  public void testInnerLineIndentation() {
    String expected;
    String actual;
    String prefix = "  ";

    {
      expected = "";
      expected += "proctype p() {\n";
      expected += "    l:\n";
      expected += "    skip\n";
      expected += "  }";
      actual = noParam_proctypePro.declaration(prefix);
      assertEquals(expected, actual);
    }

    {
      expected = "";
      expected += "init {\n";
      expected += "    l:\n";
      expected += "    skip\n";
      expected += "  }";
      actual = initPro.declaration(prefix);
      assertEquals(expected, actual);
    }

    {
      expected = "";
      expected += "never {\n";
      expected += "    l:\n";
      expected += "    skip\n";
      expected += "  }";
      actual = neverPro.declaration(prefix);
      assertEquals(expected, actual);
    }
  }

  /**
   * Test equals().
   */
  @Test
  public void testEquals() {
    for (int i = 0; i < processes.length; i++) {
      for (int j = i; j < processes.length; j++) {
        if (j == i || j < 3) {
          assertTrue(processes[i].equals(processes[j]));
        }
        else {
          assertFalse(processes[i].equals(processes[j]));
        }
      }
    }
  }
}
