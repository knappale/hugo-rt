package promela;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class PChannelTest {
  private PConstant arraySize;
  private PChannel simChn;
  private PChannel simChn_initialised_fullPartTest;
  private PChannel arrChn;
  private PChannel arrChn_initialised_dataTypeServesAsPart;
  private PChannel a;
  private PChannel b;

  @Before
  public void setUp() {
    var arraySize = (this.arraySize = PConstant.numeric(2));
    var bufferSize = PConstant.numeric(0);
    this.simChn = PChannel.simple("c");
    PMessageType messageType_fullPartTest = PMessageType.create().addPartChannel().addPartDataType(PDataType.bitType());
    simChn_initialised_fullPartTest = PChannel.simple("c").initialise(bufferSize, messageType_fullPartTest);
    arrChn = PChannel.array("c", arraySize);
    PMessageType messageType_dataTypeServesAsPart = PMessageType.create().addPartDataType(PDataType.bitType());
    arrChn_initialised_dataTypeServesAsPart = PChannel.array("c", arraySize).initialise(bufferSize, messageType_dataTypeServesAsPart);
    a = PChannel.simple("c");
    b = PChannel.array("c", arraySize);
  }

  /**
   * Test declaration of one simple channel.
   */
  @Test
  public void simChanDec() {
    String expected = "chan c";
    String actual = simChn.declaration();
    assertEquals(expected, actual);
  }

  /**
   * Test declaration of one initialised simple channel, full test of message
   * type's part.
   */
  @Test
  public void initialisedSimChanDec_fullPartTest() {
    String expected = "chan c = [0] of { chan, bit }";
    String actual = simChn_initialised_fullPartTest.declaration();
    assertEquals(expected, actual);
  }

  /**
   * Test declaration of one array channel.
   */
  @Test
  public void arrChanDec() {
    String expected = "chan c[2]";
    String actual = arrChn.declaration();
    assertEquals(expected, actual);
  }

  /**
   * Test declaration of one initialised array channel having a message type
   * which consists of a part that is created with a data type as its argument.
   */
  @Test
  public void initialisedArrChanDec_dataTypeServesAsPart() {
    String expected = "chan c[2] = [0] of { bit }";
    String actual = arrChn_initialised_dataTypeServesAsPart.declaration();
    assertEquals(expected, actual);
  }

  /**
   * Test equals()
   */
  @Test
  public void equals() {
    assertTrue(a.equals(a));
    assertTrue(a.equals(PChannel.simple("c")));
    assertFalse(a.equals(PChannel.simple("ch")));
    assertTrue(a.equals(b));
    assertTrue(b.equals(b));
    var arraySize = this.arraySize; assert arraySize != null;
    assertTrue(b.equals(PChannel.array("c", arraySize)));
    assertFalse(b.equals(PChannel.array("ch", arraySize)));
    assertTrue(b.equals(a));
  }
}