package promela;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import org.eclipse.jdt.annotation.NonNull;


public class PVariableTest {
  private @NonNull PConstant zero = PConstant.numeric(0);
  private @NonNull PConstant arraySize = PConstant.numeric(2);
  private @NonNull PDataType typeBit = PDataType.bitType();
  private @NonNull PDataType typeBool = PDataType.boolType();
  private @NonNull PVariable[] simVars = { PVariable.simple("a", PDataType.bitType()),
                                           PVariable.simple("a", PDataType.boolType()),
                                           PVariable.simple("a", PDataType.byteType()),
                                           PVariable.simple("a", PDataType.shortType()),
                                           PVariable.simple("a", PDataType.intType()),
                                           PVariable.simple("a", PDataType.mtype(Arrays.asList())),
                                           PVariable.simple("a", PDataType.typedef("Msg", Arrays.asList())) };
  private @NonNull PVariable simVar_initialised = PVariable.simple("a", typeBit).initialise(PExpression.constant(zero));
  private @NonNull PVariable arrVar = PVariable.array("a", typeBit, arraySize);
  private @NonNull PVariable arrVar_initialised = PVariable.array("a", typeBit, arraySize).initialise(PExpression.constant(zero));
  private @NonNull PVariable a = PVariable.simple("x", typeBit);
  private @NonNull PVariable b = PVariable.array("x", typeBit, arraySize);

  /*
   * Test declaration of one simple variable for each data type
   */
  @Test
  public void testSimVarDec_allDataTypes() {
    String[] s = {"bit", "bool", "byte", "short", "int", "mtype", "Msg"};
    for (int i = 0; i < simVars.length; i++) {
      String expected = s[i] + " a";
      String actual = simVars[i].declaration();
      assertEquals(expected, actual);
    }
  }

  /*
   * Test declaration of one initialised simple variable
   */
  @Test
  public void testInitialisedSimVarDec() {
    String expected = "bit a = 0";
    String actual = simVar_initialised.declaration();
    assertEquals(expected, actual);
  }

  /*
   * Test declaration of one array variable
   */
  @Test
  public void testArrVarDec() {
    String expected = "bit a[2]";
    String actual = arrVar.declaration();
    assertEquals(expected, actual);
  }

  /*
   * Test declaration of one initialised array variable
   */
  @Test
  public void testInitialisedArrVarDec() {
    String expected = "bit a[2] = 0";
    String actual = arrVar_initialised.declaration();
    assertEquals(expected, actual);
  }

  /*
   * Test equals()
   */
  @Test
  public void testEquals() {
    assertTrue(a.equals(a));
    assertTrue(a.equals(PVariable.simple("x", typeBit)));
    assertFalse(a.equals(PVariable.simple("y", typeBit)));
    assertTrue(a.equals(PVariable.array("x", typeBool, arraySize)));
    assertTrue(b.equals(b));
    assertTrue(b.equals(PVariable.array("x", typeBit, arraySize)));
    assertFalse(b.equals(PVariable.array("y", typeBit, arraySize)));
    assertTrue(b.equals(PVariable.simple("x", typeBool)));
  }
}
