package promela;

import static org.junit.Assert.*;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.Test;


public class PFormulaTest {
  private @NonNull PMacro trueMacro = PMacro.expression("t", PExpression.trueConst());
  private @NonNull PMacro falseMacro = PMacro.expression("f", PExpression.falseConst());

  @Test
  public void testTrue() {
    String expected;
    String actual;

    expected = "#define t true";
    actual = trueMacro.code();
    assertEquals(expected, actual);
    
    expected = "t";
    actual = PFormula.term(trueMacro).code();
    assertEquals(expected, actual);
  }

  @Test
  public void testFalse() {
    String expected;
    String actual;

    expected = "#define f false";
    actual = falseMacro.code();
    assertEquals(expected, actual);
    
    expected = "f";
    actual = PFormula.term(falseMacro).code();
    assertEquals(expected, actual);
  }

  @Test
  public void testAnd() {
    String expected = "(t) && (f)";
    String actual = PFormula.and(PFormula.term(trueMacro), PFormula.term(falseMacro)).code();
    assertEquals(expected, actual);
  }

  @Test
  public void testAndOr() {
    String expected = "((t) && (f)) || (f)";
    String actual = PFormula.or(PFormula.and(PFormula.term(trueMacro), PFormula.term(falseMacro)),
                               PFormula.term(falseMacro)).code();
    assertEquals(expected, actual);
  }

  @Test
  public void testFTrue() {
    String expected = "[] (t)";
    String actual = PFormula.always(PFormula.term(trueMacro)).code();
    assertEquals(expected, actual);
  }
}
