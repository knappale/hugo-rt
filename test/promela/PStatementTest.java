package promela;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class PStatementTest {
  private PStatement skipStm;
  private PStatement elseStm;
  private PStatement breakStm;
  private PStatement varDecStm;
  private PStatement chanDecStm;
  private PStatement expStm;
  private PStatement xrStm;
  private PStatement xsStm;
  private PStatement assertStm;
  private PStatement assignStm;
  private PStatement macroStm;
  private PStatement[] sendStms;
  private PStatement[] sortedSendStms;
  private PStatement[] recvStms;
  private PStatement[] randomRecvStms;
  private PStatement labeledStm;
  private PStatement gotoStm;
  private PStatement stmWithoutInner_atomicStm;
  private PStatement stmWithoutInner_d_stepStm;
  private PStatement sequenceStm;
  private PStatement blankSelStm;
  private PStatement[] oneBranchSelStms;
  private PStatement twoBranchesAndElseBranchSelStm;
  private PStatement blankRepStm;
  private PStatement oneBranchRepStm;
  private PStatement[] withoutInnerStms;
  private PStatement stmWithInner_atomicStm;
  private PStatement stmWithInner_d_stepStm;
  private PStatement[] withInnerStms;

  @Before
  public void setUp() {
    var zero = PConstant.numeric(0);
    var exp = PExpression.constant(zero);
    var skipStm = (this.skipStm = PStatement.skip());
    elseStm = PStatement.elseStm();
    breakStm = PStatement.breakStm();
    varDecStm = PStatement.varDecl(PVariable.simple("b", PDataType.bitType()));
    chanDecStm = PStatement.chanDecl(PChannel.simple("c"));
    expStm = PStatement.expression(exp);
    xrStm = PStatement.exclusiveRead(exp);
    xsStm = PStatement.exclusiveSend(exp);
    assertStm = PStatement.assertion(exp);
    assignStm = PStatement.assignment(exp, exp);
    macroStm = PStatement.macro(PMacro.constant("zero", zero));
    sendStms = new PStatement[2];
    sendStms[0] = PStatement.send(exp).addSendStmArg(exp);
    sendStms[1] = PStatement.send(exp).addSendStmArg(exp).addSendStmArg(exp);
    sortedSendStms = new PStatement[2];
    sortedSendStms[0] = PStatement.sortedSend(exp).addSendStmArg(exp);
    sortedSendStms[1] = PStatement.sortedSend(exp).addSendStmArg(exp).addSendStmArg(exp);
    recvStms = new PStatement[2];
    recvStms[0] = PStatement.receive(exp).addRecvStmArg(exp);
    recvStms[1] = PStatement.receive(exp).addRecvStmArg(exp).addRecvStmArg(exp);
    randomRecvStms = new PStatement[2];
    randomRecvStms[0] = PStatement.randomReceive(exp).addRecvStmArg(exp);
    randomRecvStms[1] = PStatement.randomReceive(exp).addRecvStmArg(exp).addRecvStmArg(exp);
    var labeledStm = (this.labeledStm = PStatement.label("l", PStatement.skip()));
    gotoStm = PStatement.gotoStm(labeledStm);
    stmWithoutInner_atomicStm = PStatement.atomic(skipStm);
    stmWithoutInner_d_stepStm = PStatement.d_step(skipStm);
    sequenceStm = PStatement.sequence(skipStm, skipStm);
    blankSelStm = PStatement.selection();
    oneBranchSelStms = new PStatement[3];
    oneBranchSelStms[0] = PStatement.selection().addBranch(labeledStm);
    oneBranchSelStms[1] = PStatement.selection().addBranch(PStatement.sequence(skipStm, labeledStm));
    oneBranchSelStms[2] = PStatement.selection().addBranch(PStatement.sequence(sequenceStm, skipStm));
    twoBranchesAndElseBranchSelStm = PStatement.selection().addBranch(skipStm).addBranch(skipStm).setElseBranch(skipStm);
    blankRepStm = PStatement.repetition();
    oneBranchRepStm = PStatement.repetition().addBranch(skipStm);
    withoutInnerStms = new PStatement[16];
    withoutInnerStms[0] = skipStm;
    withoutInnerStms[1] = varDecStm;
    withoutInnerStms[2] = chanDecStm;
    withoutInnerStms[3] = assignStm;
    withoutInnerStms[4] = expStm;
    withoutInnerStms[5] = assertStm;
    withoutInnerStms[6] = elseStm;
    withoutInnerStms[7] = breakStm;
    withoutInnerStms[8] = gotoStm;
    withoutInnerStms[9] = sendStms[0];
    withoutInnerStms[10] = sortedSendStms[0];
    withoutInnerStms[11] = recvStms[0];
    withoutInnerStms[12] = randomRecvStms[0];
    withoutInnerStms[13] = xrStm;
    withoutInnerStms[14] = xsStm;
    withoutInnerStms[15] = macroStm;
    stmWithInner_atomicStm = PStatement.atomic(labeledStm);
    stmWithInner_d_stepStm = PStatement.d_step(labeledStm);
    withInnerStms = new PStatement[5];
    withInnerStms[0] = labeledStm;
    withInnerStms[1] = stmWithInner_atomicStm;
    withInnerStms[2] = stmWithInner_d_stepStm;
    withInnerStms[3] = PStatement.selection().addBranch(skipStm).setElseBranch(skipStm);
    withInnerStms[4] = PStatement.repetition().addBranch(skipStm).setElseBranch(skipStm);
  }

  /**
   * Test coding of statement which represents <CODE>skip</CODE>.
   */
  @Test
  public void testSkipStmCode() {
    String expected = "skip";
    String actual = skipStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents <CODE>else</CODE>.
   */
  @Test
  public void testElseStmCode() {
    String expected = "else";
    String actual = elseStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents <CODE>break</CODE>.
   */
  @Test
  public void testBreakStmCode() {
    String expected = "break";
    String actual = breakStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a variable declaration.
   */
  @Test
  public void testVarDecStmCode() {
    String expected = "bit b";
    String actual = varDecStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a channel declaration.
   */
  @Test
  public void testChanDecStmCode() {
    String expected = "chan c";
    String actual = chanDecStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents expression.
   */
  @Test
  public void testExpStmCode() {
    String expected = "0";
    String actual = expStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents an exclusive read.
   */
  @Test
  public void testXrStmCode() {
    String expected = "xr 0";
    String actual = xrStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents an exclusive send.
   */
  @Test
  public void testXsStmCode() {
    String expected = "xs 0";
    String actual = xsStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents an assertion.
   */
  @Test
  public void testAssertStmCode() {
    String expected = "assert(0)";
    String actual = assertStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents an assignment.
   */
  @Test
  public void testAssignStmCode() {
    String expected = "0 = 0";
    String actual = assignStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a macro.
   */
  @Test
  public void testMacroStmCode() {
    String expected = "zero";
    String actual = macroStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a send.
   */
  @Test
  public void testSendStmCode() {
    String[] s = { "0!0", "0!0,0" };
    String expected;
    String actual;
    for (int i = 0; i < sendStms.length; i++) {
      expected = s[i];
      actual = sendStms[i].code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of statement which represents a sorted send.
   */
  @Test
  public void testSortedSendStmCode() {
    String[] s = { "0!!0", "0!!0,0" };
    String expected;
    String actual;
    for (int i = 0; i < sortedSendStms.length; i++) {
      expected = s[i];
      actual = sortedSendStms[i].code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of statement which represents a receive.
   */
  @Test
  public void testRecvStmCode() {
    String[] s = { "0?0", "0?0,0" };
    String expected;
    String actual;
    for (int i = 0; i < recvStms.length; i++) {
      expected = s[i];
      actual = recvStms[i].code();
      assertEquals(expected, actual);
    }
  }
  
  /**
   * Test coding of statement which represents a random receive.
   */
  @Test
  public void testRandomRecvStmCode() {
    String[] s = { "0??0", "0??0,0" };
    String expected;
    String actual;
    for (int i = 0; i < randomRecvStms.length; i++) {
      expected = s[i];
      actual = randomRecvStms[i].code();
      assertEquals(expected, actual);
    }
  }
  
  /**
   * Test coding of labeled statements.
   */
  @Test
  public void testLabeledStmCode() {
    String expected = "";
    expected += "l:\n";
    expected += "skip";
    String actual = labeledStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a <CODE>goto</CODE>.
   */
  @Test
  public void testGotoStmCode() {
    String expected = "goto l";
    String actual = gotoStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a <CODE>atomic</CODE> block.
   */
  @Test
  public void testAtomicStmCode() {
    String expected = "";
    expected += "atomic {\n";
    expected += "  skip\n";
    expected += "}";
    String actual = stmWithoutInner_atomicStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a <CODE>d_step</CODE> block.
   */
  @Test
  public void testD_stepStmCode()
  {
    String expected = "";
    expected += "d_step {\n";
    expected += "  skip\n";
    expected += "}";
    String actual = stmWithoutInner_d_stepStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a sequence.
   */
  @Test
  public void testSequenceStmCode() {
    String expected = "";
    expected += "skip";
    String actual = sequenceStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a blank selection.
   */
  @Test
  public void testBlankSelStmCode() {
    String expected = "";
    expected += "if\n";
    expected += ":: skip\n";
    expected += "fi";
    String actual = blankSelStm.code();
    assertEquals(expected, actual);
  }
  
  /**
   * Test coding of statement which represents a selection,
   * its branch under full test.
   */
  @Test
  public void testOneBranchSelStmCode() {
    String s[] = new String[oneBranchSelStms.length];
    for (int i = 0; i < oneBranchSelStms.length; i++) {
      s[i] = "";
    }
    s[0] += "if\n";      s[1] += "if\n";         s[2] += "if\n";
    s[0] += ":: l:\n";   s[1] += ":: l:\n";      s[2] += ":: skip\n";    
    s[0] += "   skip\n"; s[1] += "   skip\n";    s[2] += "fi";
    s[0] += "fi";        s[1] += "fi";
                
    for (int i = 0; i < oneBranchSelStms.length; i++) {
      String expected = s[i];
      String actual = oneBranchSelStms[i].code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of statement which represents a selection having
   * two branches and an else-branch.
   */
  @Test
  public void testTwoBranchesAndElseBranchSelStmCode() {
    String expected = "";
    expected += "if\n";
    expected += ":: skip\n";
    expected += ":: skip\n";
    expected += ":: else\n";
    expected += "fi";
    String actual = twoBranchesAndElseBranchSelStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a blank repetition.
   */
  @Test
  public void testBlankRepStmCode() {
    String expected = "";
    expected += "do\n";
    expected += ":: skip\n";
    expected += "od";
    String actual = blankRepStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test coding of statement which represents a repetition.
   */
  @Test
  public void testRepStmCode() {
    String expected = "";
    expected += "do\n";
    expected += ":: skip\n";
    expected += "od";
    String actual = oneBranchRepStm.code();
    assertEquals(expected, actual);
  }

  /**
   * Test code with indentation of inner lines.
   */
  @Test
  public void testInnerLineIndentation() {
    String expected;
    String actual;
    String prefix = "  ";

    // statements without inner lines
    for (int i = 0; i < withoutInnerStms.length; i++) {
      expected = withoutInnerStms[i].code();
      actual = withoutInnerStms[i].code(prefix);
      assertEquals(expected, actual);
    }

    // statements with inner lines
    {
      expected = "";
      expected += "l:\n";
      expected += "  skip";
      actual = labeledStm.code(prefix); // labeled statement
      assertEquals(expected, actual);
    }
    {
      expected = "";
      expected += "atomic {\n";
      expected += "    l:\n";
      expected += "    skip\n";
      expected += "  }";
      actual = stmWithInner_atomicStm.code(prefix); // atomic
      assertEquals(expected, actual);
    }
    {
      expected = "";
      expected += "d_step {\n";
      expected += "    l:\n";
      expected += "    skip\n";
      expected += "  }";
      actual = stmWithInner_d_stepStm.code(prefix); // d_step
      assertEquals(expected, actual);
    }
    {
      expected = "";
      expected += "skip";
      actual = sequenceStm.code(prefix); // sequence
      assertEquals(expected, actual);
    }
    {
      expected = "";
      expected += "if\n";
      expected += "  :: skip\n";
      expected += "  fi";
      actual = blankSelStm.code(prefix); // blank selection
      assertEquals(expected, actual);
    }
    {
      PStatement selStm = PStatement.selection().addBranch(PStatement.sequence(PStatement.label("l", PStatement.skip()), PStatement.skip())).setElseBranch(PStatement.skip());
      expected = "";
      expected += "if\n";
      expected += "  :: l:\n";
      expected += "     skip\n";
      expected += "  :: else\n";
      expected += "  fi";
      actual = selStm.code(prefix); // selection, prefixing of inner lines of branches under full test
      assertEquals(expected, actual);
    }
    {
      expected = "";
      expected += "do\n";
      expected += "  :: skip\n";
      expected += "  od";
      actual = blankRepStm.code(prefix); // blank repetition
      assertEquals(expected, actual);
    }
    {
      expected = "";
      expected += "do\n";
      expected += "  :: skip\n";
      expected += "  od";
      actual = oneBranchRepStm.code(prefix); // repetition
      assertEquals(expected, actual);
    }
  }

  /**
   * Test equals().
   */
  @Test
  public void testEquals() {
    PStatement[] statements = new PStatement[21];
    for (int i = 0; i < withoutInnerStms.length; i++) {
      statements[i] = withoutInnerStms[i];
    }
    for (int i = 0; i < withInnerStms.length; i++) {
      statements[withoutInnerStms.length + i] = withInnerStms[i];
    }
    for (int i = 0; i < statements.length; i++) {
      assertTrue(statements[i].equals(statements[i]));
      for (int j = i + 1; j < statements.length; j++) {
        assertFalse(statements[i].equals(statements[j]));
      }
    }
    assertTrue(PStatement.skip().equals(PStatement.sequence(PStatement.skip(), PStatement.skip())));
  }
}
