package promela;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;


public class PDataTypeTest {
  //private PDataType oneConst_mtypeDat;
  private PDataType twoConst_mtypeDat;
  private PDataType oneField_typedefDat;
  private PDataType twoField_typedefDat;
  private PDataType[] dataTypes;

  @Before
  public void setUp() {
    PConstant zero = PConstant.symbolic("zero");
    PConstant one = PConstant.symbolic("one");
    PVariable v = PVariable.simple("b", PDataType.bitType());
    PChannel c = PChannel.simple("c");
    // oneConst_mtypeDat = PDataType.mtype(Arrays.asList(zero));
    twoConst_mtypeDat = PDataType.mtype(Arrays.asList(zero, one));
    oneField_typedefDat = PDataType.typedef("Msg", Arrays.asList(v));
    twoField_typedefDat = PDataType.typedef("Msg", Arrays.asList(v, c));
    dataTypes = new PDataType[4];
    //dataTypes[0] = oneConst_mtypeDat;
    dataTypes[0] = twoConst_mtypeDat;
    dataTypes[1] = twoConst_mtypeDat;
    dataTypes[2] = oneField_typedefDat;
    dataTypes[3] = twoField_typedefDat;
  }

  /**
   * Test coding of mtype data types.
   */
  @Test
  public void testMtypeDatDec() {
    String expected;
    String actual;

    /*{
      expected = "mtype = {zero}";
      actual = oneConst_mtypeDat.declaration();
      assertEquals(expected, actual);
    }*/

    {
      expected = "mtype = {zero, one}";
      actual = twoConst_mtypeDat.declaration();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test coding of user-defined data types.
   */
  @Test
  public void testTypedefDatDec() {
    String expected;
    String actual;

    {
      expected = "";
      expected += "typedef Msg {\n";
      expected += "    bit b\n";
      expected += "}";
      actual = oneField_typedefDat.declaration();
      assertEquals(expected, actual);
    }

    {
      expected = "";
      expected += "typedef Msg {\n";
      expected += "    bit b;\n";
      expected += "    chan c\n";
      expected += "}";
      actual = twoField_typedefDat.declaration();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test declaration with indentation of inner lines.
   */
  public void testInnerLineIndentation() {
    String expected;
    String actual;
    String prefix = "    ";

    {
      expected = twoConst_mtypeDat.declaration();
      actual = twoConst_mtypeDat.declaration(prefix);
      assertEquals(expected, actual);
    }

    {
      expected = "";
      expected += "typedef Msg {\n";
      expected += "        bit b\n";
      expected += "    }";
      actual = oneField_typedefDat.declaration(prefix);
      assertEquals(expected, actual);
    }
  }

  /**
   * Test equals().
   */
  @Test
  public void testEquals() {
    for (int i = 0; i < dataTypes.length; i++) {
      for (int j = i; j < dataTypes.length; j++) {
        if (j == i || j == 1 && i == 0 || j == 3 && i == 2) {
          assertTrue(dataTypes[i].equals(dataTypes[j]));
        }
        else {
          assertFalse(dataTypes[i].equals(dataTypes[j]));
        }
      }
    }
  }
}