package promela;

import static org.junit.Assert.*;

import org.junit.Test;


public class PMacroTest {
  /**
   * Test macro which represents a constant.
   */
  @Test
  public void testConstantMac() {
    String expected = "#define ZERO_CONST 0";
    String actual = PMacro.constant("ZERO_CONST", PConstant.numeric(0)).code();
    assertEquals(expected, actual);
  }

  /**
   * Test macros which represent expressions.
   */
  @Test
  public void testExpressionMac() {
    String expected;
    String actual;

    {
      expected = "#define LEN_EXP len(true)";
      actual = PMacro.expression("LEN_EXP", PExpression.len(PExpression.trueConst())).code();
      assertEquals(expected, actual);
    }

    {
      expected = "#define TRUE_EXP true";
      actual = PMacro.expression("TRUE_EXP", PExpression.trueConst()).code();
      assertEquals(expected, actual);
    }

    {
      expected = "#define U_MIN_EXP (-true)";
      actual = PMacro.expression("U_MIN_EXP", PExpression.unaryMinus(PExpression.trueConst())).code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test macros which represent statements.
   */
  @Test
  public void testStatementMac() {
    String expected;
    String actual;

    {
      expected = "#define SKIP_STM skip";
      actual = PMacro.statement("SKIP_STM", PStatement.skip()).code();
      assertEquals(expected, actual);
    }

    {
      expected = "";
      expected += "#define LABELED_STM \\\n";
      expected += "    l: \\\n";
      expected += "    skip";
      actual = PMacro.statement("LABELED_STM", PStatement.label("l", PStatement.skip())).code();
      assertEquals(expected, actual);
    }
  }

  /**
   * Test code with indentation of inner lines.
   */
  @Test
  public void testInnerLineIndentation() {
    String expected;
    String actual;
    String prefix = "    ";

    {
      expected = "#define SKIP_STM skip";
      actual = PMacro.statement("SKIP_STM", PStatement.skip()).code(prefix);
      assertEquals(expected, actual);
    }

    {
      expected = "";
      expected += "#define LABELED_STM \\\n";
      expected += "        l: \\\n";
      expected += "        skip";
      actual = PMacro.statement("LABELED_STM", PStatement.label("l", PStatement.skip())).code(prefix);
      assertEquals(expected, actual);
    }
  }

  /**
   * Test equals().
   */
  @Test
  public void testEquals() {
    PExpression trueExp = PExpression.trueConst();
    PStatement skipStm = PStatement.skip();
    PStatement labeledStm = PStatement.label("l", PStatement.skip());
    PMacro[] macros = new PMacro[6];
    macros[0] = PMacro.constant("ZERO_CONST", PConstant.numeric(0));
    macros[1] = PMacro.expression("LEN_EXP", PExpression.len(trueExp));
    macros[2] = PMacro.expression("TRUE_EXP", trueExp);
    macros[3] = PMacro.expression("U_MIN_EXP", PExpression.unaryMinus(trueExp));
    macros[4] = PMacro.statement("SKIP_STM", skipStm);
    macros[5] = PMacro.statement("LABELED_STM", labeledStm);

    for (int i = 0; i < macros.length; i++) {
      assertTrue(macros[i].equals(macros[i]));
      for (int j = i + 1; j < macros.length; j++) {
        assertFalse(macros[i].equals(macros[j]));
      }
    }
  }
}
