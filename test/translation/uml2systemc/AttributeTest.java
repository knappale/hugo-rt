package translation.uml2systemc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class AttributeTest {
  @Before
  public void setUp() throws Exception {
    String modelString =
      "model callEventTest {" +
      "  class A {" +
      "    signature {" +
      "      attr b : B;" +
      "      attr i : int = 0;" +
      "      attr bu : boolean = false;" +
      "      attr s : String;" +
      "      reception finish();" +
      "    }" +
      "" +
      "    behaviour {" +
      "      states {" +
      "        initial init;" +
      "        simple BeforeCall;" +
      "        simple CallAcknowledged;" +
      "        final end;" +
      "      }" +
      "" +
      "      transitions {" +
      "        init -> BeforeCall {" +
      "        }" +
      "" +
      "        BeforeCall -> CallAcknowledged {" +
      "          effect b.mycall();" +
      "        }" +
      "" +
      "        CallAcknowledged -> end {" +
      "          guard true;" +
      "        }" +
      "      }"+
      "    }"+
      "  }"+
      ""+
      "  class B {"+
      "    signature {"+
      "      attr a : A;"+
      "      operation mycall();" +
      "    }" +
      "" +
      "    behaviour {" +
      "      states {" +
      "        initial init;" +
      "        simple sendf {" +
      "          defer mycall;" +
      "        }" +
      "        simple ackcall;" +
      "        final end;" +
      "      }" +
      "" +
      "      transitions {" +
      "        init -> sendf {" +
      "          guard true;" +
      "        }" +
      "" +
      "        sendf -> ackcall {" +
      "          trigger mycall;" +
      "        }" +
      "" +
      "        ackcall -> end {" +
      "        }" +
      "      }" +
      "    }" +
      "  }" +
      "}";

    uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(new java.io.BufferedInputStream(new java.io.ByteArrayInputStream(modelString.getBytes())));
    uml.UModel model = modelReader.getModel();
    uml.UCollaboration coll = model.getEmptyUCollaboration();
    cpp.CSystem code = translation.uml2systemc.UMLTranslator.translate(coll).getCPPCode();
    for (cpp.CClass c : code.getCClasses())
      System.out.println(c.getDeclaration());
  }

  @Test
  public void testTest () {
    assertTrue(true);
  }
}
