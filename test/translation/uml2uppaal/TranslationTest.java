package translation.uml2uppaal;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

import uml.UCollaboration;
import uml.UModel;
import uppaal.TSystem;


public class TranslationTest {
  @Test
  public void translationTest() {
    String modelFileName = "test/uml/parser/xmi/uml13xmi10/argouml-0.15-test.xmi";

    try {
      util.Message.debug = false;
      util.Message.verbosity = util.Message.INFO;

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(modelInputStream);

      UModel uModel = modelReader.getModel();
      UCollaboration uCollaboration = uModel.getCollaborations().iterator().next();
      TSystem tSystem = translation.uml2uppaal.UMLTranslator.translate(uCollaboration, null, new ArrayList<>());

      util.Message.info(tSystem.declaration());
    }
    catch (uml.UModelException me) {
      fail("Could not parse ArgoUML 0.15 model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read ArgoUML 0.15 model");
    }
  }

}
