package translation.uml2uppaal;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.Before;
import org.junit.Test;

import uml.UAttribute;
import uml.UClass;
import uml.UCollaboration;
import uml.UContext;
import uml.UExpression;
import uml.UModel;
import uml.UOperator;
import uml.smile.SBranch;
import uml.smile.SBranching;
import uml.smile.SCommand;
import uml.smile.SGuard;
import uml.smile.SLiteral;
import uml.smile.SValue;
import uml.smile.SVariable;
import util.Identifier;
import util.Sets;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class SmileTranslatorTest {
  private UCollaboration uCollaboration;
  private UClass uClass;

  @Before
  public void setUp() {
    UModel uModel = new UModel("test");
    UCollaboration uCollaboration = new UCollaboration("test", uModel);
    UClass uClass = new UClass("Test", uModel);
    UAttribute uAttribute = new UAttribute("x", uModel.getInteger(), uClass);
    uClass.addAttribute(uAttribute);
    uClass.setStateMachine();
    this.uCollaboration = uCollaboration;
    this.uClass = uClass;
  }

  /**
   * Test choice with else-branch translation.
   */
  @Test
  public void testChoiceWithElseBranchTranslation() {
    var uClass = this.uClass; assert uClass != null;
    var uContext = UContext.c1ass(uClass);
    var uStateMachine = uClass.getStateMachine(); assert uStateMachine != null;
    var uCollaboration = this.uCollaboration; assert uCollaboration != null;

    var moST = translation.uml2uppaal.ModelSymbolTable.collaboration(uCollaboration);
    var smST = new translation.uml2uppaal.StateMachine.SymbolTable(uStateMachine, moST);

    var guard1 = SGuard.literal(SLiteral.external(UExpression.relational(UExpression.id(Identifier.id("x")), UOperator.EQ, UExpression.intConst(1)), uContext));
    var guard2 = SGuard.literal(SLiteral.external(UExpression.relational(UExpression.id(Identifier.id("x")), UOperator.NEQ, UExpression.intConst(2)), uContext));
    var sVariable = SVariable.flag("f");
    var branch1 = SBranch.branch(guard1, SCommand.assign(sVariable, SValue.trueValue()));
    var branch2 = SBranch.branch(guard2, SCommand.assign(sVariable, SValue.falseValue()));
    var else1 = SCommand.skip();
    var branches = new ArrayList<@NonNull SBranch>();
    branches.add(branch1);
    branches.add(branch2);
    var choice = SBranching.choice(branches, else1);

    var subProcess = translation.uml2uppaal.SmileTranslator.translateStatement(choice, smST);
    subProcess.getEntry().setInitial(true);
    subProcess.optimise(Sets.empty());

    var expected =
      "process () {\n" +
      "  state\n" +
      "\n" +
      "  S0,\n" +
      "  S1;\n" +
      "\n" +
      "  commit S0,\n" +
      "         S1;\n" +
      "\n" +
      "  init S1;\n" +
      "\n" +
      "  trans\n" +
      "\n" +
      "  S1 -> S0 {\n" +
      "    guard x == 2 && x != 1;\n" +
      "  },\n" +
      "  S1 -> S0 {\n" +
      "    guard x == 1;\n" +
      "    assign f = true;\n" +
      "  },\n" +
      "  S1 -> S0 {\n" +
      "    guard x != 2;\n" +
      "    assign f = false;\n" +
      "  };\n" +
      "}";
    var actual = subProcess.toString();
    assertEquals(expected, actual);
  }

  /**
   * Test choice without else-branch translation.
   */
  @Test
  public void testChoiceWithoutElseBranchTranslation() {
    var uClass = this.uClass; assert (uClass != null);
    var uContext = UContext.c1ass(uClass);
    var uStateMachine = uClass.getStateMachine(); assert uStateMachine != null;
    var uCollaboration = this.uCollaboration; assert uCollaboration != null;

    var moST = translation.uml2uppaal.ModelSymbolTable.collaboration(uCollaboration);
    var smST = new translation.uml2uppaal.StateMachine.SymbolTable(uStateMachine, moST);

    var guard1 = SGuard.literal(SLiteral.external(UExpression.relational(UExpression.id(Identifier.id("x")), UOperator.EQ, UExpression.intConst(1)), uContext));
    var guard2 = SGuard.literal(SLiteral.external(UExpression.relational(UExpression.id(Identifier.id("x")), UOperator.NEQ, UExpression.intConst(2)), uContext));
    var sVariable = SVariable.flag("f");
    var branch1 = SBranch.branch(guard1, SCommand.assign(sVariable, SValue.trueValue()));
    var branch2 = SBranch.branch(guard2, SCommand.assign(sVariable, SValue.falseValue()));
    var branches = new ArrayList<@NonNull SBranch>();
    branches.add(branch1);
    branches.add(branch2);
    var choice = SBranching.choice(branches);

    var subProcess = translation.uml2uppaal.SmileTranslator.translateStatement(choice, smST);
    subProcess.getEntry().setInitial(true);
    subProcess.optimise(Sets.empty());

    var expected =
      "process () {\n" +
      "  state\n" +
      "\n" +
      "  S0,\n" +
      "  S1;\n" +
      "\n" +
      "  commit S0,\n" +
      "         S1;\n" +
      "\n" +
      "  init S1;\n" +
      "\n" +
      "  trans\n" +
      "\n" +
      "  S1 -> S0 {\n" +
      "    guard x == 1;\n" +
      "    assign f = true;\n" +
      "  },\n" +
      "  S1 -> S0 {\n" +
      "    guard x != 2;\n" +
      "    assign f = false;\n" +
      "  };\n" +
      "}";
    var actual = subProcess.toString();
    assertEquals(expected, actual);
  }
}
