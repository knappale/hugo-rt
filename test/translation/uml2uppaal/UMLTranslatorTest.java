package translation.uml2uppaal;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import uml.UAttribute;
import uml.UClass;
import uml.UCollaboration;
import uml.UModel;
import uppaal.builder.TNet;

import static util.Objects.requireNonNull;


/**
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public class UMLTranslatorTest {
  private UCollaboration uCollaboration;
  private UClass uClass;

  @Before
  public void setUp() {
    var uModel = new UModel("test");
    var uCollaboration = new UCollaboration("test", uModel);
    var uClass = new UClass("Test", uModel);
    var uAttribute = new UAttribute("x", uModel.getInteger(), uClass);
    uClass.addAttribute(uAttribute);
    uClass.setStateMachine();

    this.uCollaboration = uCollaboration;
    this.uClass = uClass;
  }

  @Test
  public void testAcknowledgement() {
    var uCollaboration = requireNonNull(this.uCollaboration);
    var uStateMachine = requireNonNull(this.uClass.getStateMachine());

    var moST = translation.uml2uppaal.ModelSymbolTable.collaboration(uCollaboration);
    var smST = new translation.uml2uppaal.StateMachine.SymbolTable(uStateMachine, moST);
    
    var acknowledgement = TNet.trans(smST.acknowledge());

    var expected = "Transition [sequence = " +
                     "[Synchronisation [ack[current.sender]!]], " +
                     "target = null]";
    var actual = acknowledgement.toString();
    assertEquals(expected, actual);
    // translation.uml2uppaal.ModelSymbolTable.destroy(this.collaboration);
  }
}
