package translation.ida2prism;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.IOException;

import org.junit.Test;

import uml.UCollaboration;
import uml.UModel;
import uml.UModelException;
import uml.ida.IAutomaton;
import uml.interaction.UInteraction;
import util.properties.PropertyException;
import prism.RSystem;


public class TranslationTest {
  @Test
  public void translationTest() {
    // String modelFileName = "test/uml/parser/ute/atm.ute";
    String modelFileName = "test/translation/ida2prism/bank.ute";

    try {
      util.Message.debug = false;
      util.Message.verbosity = util.Message.INFO;
      control.Properties properties = control.Properties.getDefault().getCopy();
      properties.setPropertyByName("phaseBased", "true");
      properties.commit();
      control.Properties.setDefault(properties);

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(modelInputStream);

      UModel uModel = modelReader.getModel();
      // UCollaboration uCollaboration = uModel.getUCollaborations().stream().filter(uC -> uC.getName().equals("Test")).findAny().get();
      // UInteraction uInteraction = uCollaboration.getInteractions().stream().filter(uI -> uI.getName().equals("always_get_money")).findAny().get();
      UCollaboration uCollaboration = uModel.getCollaborations().stream().filter(uC -> uC.getName().equals("scenario")).findAny().get();
      UInteraction uInteraction = uCollaboration.getInteractions().stream().filter(uI -> uI.getName().equals("protocol")).findAny().get();
      IAutomaton iAutomaton = uInteraction.getAutomaton().reachable();
      RSystem rSystem = translation.ida2prism.IdaTranslator.translate(iAutomaton, uInteraction.getMessageContext());

      util.Message.info(rSystem.declaration());
    }
    catch (PropertyException pe) {
      fail("Could not change properties: " + pe.getMessage());
    }
    catch (UModelException me) {
      fail("Could not parse UTE model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read UTE model");
    }
  }
}
