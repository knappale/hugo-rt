package translation.uml2java;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.IOException;

import org.junit.Test;

import javacode.JClass;
import javacode.JSystem;
import uml.UCollaboration;
import uml.UModel;
import uml.UModelException;
import uml.parser.ModelReader;
import util.properties.PropertyException;


public class TranslationTest {
  @Test
  public void testRunMethodBased() {
    // String modelFileName = "test/uml/smile/translation/test.ute";
    // String modelFileName = "test/uml/parser/ute/atm.ute";
    String modelFileName = "test/uml/parser/xmi/uml23xmi21/atm.uml";

    try {
      // util.Message.debug = true;
      util.Message.verbosity = util.Message.INFO;
      util.Message.debug = true;
      control.Properties properties = control.Properties.getDefault().getCopy();
      properties.setPropertyByName("fixedOrderRegions", "true");
      properties.setPropertyByName("fixedOrderTransitionFiring", "true");
      properties.commit();
      control.Properties.setDefault(properties);

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      ModelReader modelReader = ModelReader.create(modelInputStream);
      assertEquals(modelReader.getModelName(), "atm");

      UModel uModel = modelReader.getModel();
      UCollaboration uCollaboration = uModel.getCollaborations().stream().filter(uC -> uC.getName().equals("setup")).findAny().get();
      assertFalse(uCollaboration == null);

      JSystem javaCode = translation.uml2java.UMLTranslator.translate(uCollaboration);
      for (JClass jClass : javaCode.getJClasses())
        util.Message.info(jClass.declaration());
    }
    catch (PropertyException pe) {
      fail("Could not change properties: " + pe.getMessage());
    }
    catch (UModelException me) {
      fail("Could not parse model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read model: " + ioe.getMessage());
    }
  }

  @Test
  public void testEventBased() {
    // String modelFileName = "test/uml/smile/translation/test.ute";
    String modelFileName = "test/uml/parser/ute/atm.ute";

    try {
      // util.Message.debug = true;
      util.Message.verbosity = util.Message.INFO;
      util.Message.debug = true;
      control.Properties properties = control.Properties.getDefault().getCopy();
      properties.setPropertyByName("fixedOrderRegions", "true");
      properties.setPropertyByName("fixedOrderTransitionFiring", "true");
      properties.setPropertyByName("eventBased", "true");
      properties.commit();
      control.Properties.setDefault(properties);

      BufferedInputStream modelInputStream = new BufferedInputStream(new java.io.FileInputStream(modelFileName));
      ModelReader modelReader = ModelReader.create(modelInputStream);
      assertEquals(modelReader.getModelName(), "ATM");

      UModel uModel = modelReader.getModel();
      UCollaboration uCollaboration = uModel.getCollaborations().stream().filter(uC -> uC.getName().equals("Test")).findAny().get();
      assertFalse(uCollaboration == null);

      JSystem javaCode = translation.uml2java.UMLTranslator.translate(uCollaboration);
      for (JClass jClass : javaCode.getJClasses())
        util.Message.info(jClass.declaration());
    }
    catch (PropertyException pe) {
      fail("Could not change properties: " + pe.getMessage());
    }
    catch (UModelException me) {
      fail("Could not parse model: " + me.getMessage());
    }
    catch (IOException ioe) {
      fail("Could not read model: " + ioe.getMessage());
    }
  }
}
