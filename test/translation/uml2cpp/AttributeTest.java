package translation.uml2cpp;

import static org.junit.Assert.*;

import org.junit.Test;

import cpp.CClass;
import cpp.CSystem;
import uml.UCollaboration;
import uml.UModel;


public class AttributeTest {
  @Test
  public void attributeTest() throws Exception {
    String modelString =
    "model callEventTest {"+
    "  class A {"+
    "    signature{"+
    "      attr b:B;"+
    "      attr i:int=0;"+
    "      attr bu:boolean=false;"+
    "      attr s:String;"+
    "      reception finish();"+
    "    }"+
    "    behaviour {"+
    "      states {"+
    "        initial init;"+
    "        simple BeforeCall;"+
    "        simple CallAcknowledged;"+
    "        final end;      "+
    "      }"+
    "      transitions {"+
    "        init -> BeforeCall {"+
    "        }"+
    " "+
    "        BeforeCall -> CallAcknowledged {"+
    "        effect b.mycall();"+
    "        }"+
    "        "+
    "        CallAcknowledged -> end {"+
    "          guard true;"+
    "        }"+
    "        "+
    "      "+
    "      }"+
    "    }"+
    "    "+
    "    "+
    "  }"+
    "  "+
    "  class B {"+
    "    signature {"+
    "      attr a:A;"+
    "      operation mycall(); "+
    "    }"+
    "    behaviour {"+
    "      states {"+
    "        initial init ;"+
    "        simple sendf {"+
    "          defer mycall;"+
    "        }       "+
    "        simple ackcall;"+
    "        final end;"+
    "      }"+
    "      "+
    "      transitions{"+
    "        init -> sendf {"+
    "          guard true;"+
    "        }"+
    "        sendf -> ackcall {"+
    "          trigger mycall;"+
    "        }"+
    "        ackcall -> end {"+
    "        }"+
    "      }"+
    "    }"+
    "    "+
    "  }"+
    "}";

    uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(new java.io.BufferedInputStream(new java.io.ByteArrayInputStream(modelString.getBytes())));
    UModel uModel = modelReader.getModel();
    UCollaboration uCollaboration = uModel.getEmptyUCollaboration();
    CSystem cCode = translation.uml2cpp.UMLTranslator.translate(uCollaboration);
    for (CClass cClass : cCode.getCClasses()) {
      System.out.println("*** Header for class " + cClass.getName());
      System.out.println(cClass.getDeclaration());
      System.out.println("*** Definitions for class " + cClass.getName());
      System.out.println(cClass.getDefinitions());
    }
    assertTrue(true);
  }
}
