package translation.uml2cpp;

import java.io.*;

import static org.junit.Assert.*;

import org.junit.Test;


public class SystemTest {
  private String mainString =
    "int main () {\n" +
    "  eventqueue::configdebug(log4cpp::Priority::DEBUG);\n" +
    "  ATM * a=new ATM();\n" +
    "  Bank * b=new Bank();\n" +
    "  pthread_t t1,t2;\n" +
    "  a->setBank(b);\n" +
    "  b->setAtm(a);\n" +
    "  pthread_create(&t1,NULL,runatm,a);\n" +
    "  pthread_create(&t2,NULL,runbank,b);\n" +
    "  pthread_join(t1,NULL);\n" +
    "  pthread_join(t2,NULL);" +
    "}";
  private String modelString =
    "model max {\n"+
    "  class A {\n"+
    "    signature{\n"+
    "      attr b:B;\n"+
    "      reception asig();\n"+
    "    }\n"+
    "    behaviour {\n"+
    "      states {\n"+
    "        initial init;\n"+
    "        simple med;\n"+
    "        final end;      \n"+
    "      }\n"+
    "      transitions {\n"+
    "        init -> med {\n"+
    "        }\n"+
    "        med -> med {\n"+
    "          trigger asig;\n"+
    "          effect b.bsig();\n"+
    "        }\n"+
    "        med -> end {\n"+
    "          guard false;\n" +
    "          effect b.term();\n"+
    "        }\n"+
    "      }\n"+
    "    }\n"+
    "    \n"+
    "    \n"+
    "  }\n"+
    "  \n"+
    "  class B {\n"+
    "    signature {\n"+
    "      attr a:A;\n"+
    "      reception term();\n" +
    "      reception bsig();\n"+
    "    }\n"+
    "    behaviour {\n"+
    "      states {\n"+
    "        initial init ;\n"+
    "        simple med;\n"+
    "        final end;\n"+
    "      }\n"+
    "      \n"+
    "      transitions{\n"+
    "        init -> med {\n"+
    "          effect a.asig();\n"+
    "        }\n"+
    "        med -> med {\n"+
    "          trigger bsig;\n"+
    "          effect a.asig();\n"+
    "        }\n"+
    "        med ->end {\n" +
    "          trigger term;\n" +
    "          guard false;\n" +
    "        }\n" +
    "      }\n"+
    "    }\n"+
    "  }\n" +
    "  collaboration test {\n" +
    "    object a:A {\n" +
    "      b=b;" +
    "    }\n" +
    "    object b:B {\n" +
    "      a=a;" +
    "    }\n" +
    "  }\n"+
    "}";

  // Currently not executed
  public void setUp() throws Exception {
    File f= new File("models/ccs.ute");
    System.out.println(modelString);
    uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(new BufferedInputStream(new FileInputStream(f)));
    //uml.parser.ModelReader modelReader = uml.parser.ModelReader.create(new BufferedInputStream(new ByteArrayInputStream(modelString().getBytes())));
    uml.UModel model = modelReader.getModel();
    uml.UCollaboration coll = model.getEmptyUCollaboration();
    // StringBuffer forwards = new StringBuffer();
    StringBuffer declarations = new StringBuffer();
    StringBuffer implementations = new StringBuffer();
    cpp.CSystem code=translation.uml2cpp.UMLTranslator.translate(coll);
    try (PrintWriter header=new PrintWriter(new FileOutputStream(new File("../cpp-hugo-test/tmp.hh")));) {
	//out.println("#include \"/home/max/dipl/soft/systemc/include/systemc.h\"");
      for (cpp.CClass c:code.getCClasses()){
        // forward.append(cpp.CKeywords.CLASS +" "+c.getName()+";\n");
        declarations.append(c.getDeclaration());
        declarations.append("\n");
        implementations.append(c.getDefinitions());
      }
      for (cpp.CMethod m: code.getMethods()) {
        declarations.append(m.declaration()+"\n");
        implementations.append(m.statement("", ""));
      }
      header.println("#include \"eventqueue.h\"");
      // header.println(forward.toString()+"\n");
      header.println(declarations.toString()+"\n");
      header.close();
    }
    try (PrintWriter def=new PrintWriter(new FileOutputStream(new File("../cpp-hugo-test/tmp.cpp")));) {
      def.println("#include \"tmp.hh\"");
      def.println("#include \"eventqueue.h\"");
      def.println(implementations.toString()+"\n");
      def.println(mainString);
      def.close();
    }
    
    //System.out.println(model.declaration());
    //this.stateMachine = model.getClasses().iterator().next().getStateMachine();
    
  }
  
  @Test
  public void testTest() {
    assertTrue(true);
  }
}
